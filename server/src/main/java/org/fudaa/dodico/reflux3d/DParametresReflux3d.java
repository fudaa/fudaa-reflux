/**
 * @file         DParametresReflux3d.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 14:45:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.reflux3d;
import java.io.*;
import java.util.*;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.calcul.*;
import org.fudaa.dodico.corba.reflux3d.*;
import org.fudaa.dodico.fortran.*;
/**
 * Les parametres Reflux3d.
 *
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 14:45:55 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class DParametresReflux3d
  extends DParametres
  implements IParametresReflux3d,IParametresReflux3dOperations {
  // Num�ro de version. A GENERALISER
  //  private static String version_="5.0";
  private String racine_;
  // Racine du nom des fichiers param�tres
  //  private String            nomFichier_=null;
  // Donn�es .INP
  private SParametresR3dINP paramsINP_;
  // Solutions initiales
  private SParametresR3dINI paramsINI_;
  // C.L. non stationnaires
  private SParametresR3dCL paramsCL_;
  // Booleen de pr�sence de conditions limites transitoires.
  //  private boolean bCLTrans_=false;
  /**
   * Construit un ensemble de parametres. Les attributs doivent dans ce cas
   * etre OBLIGATOIREMENT specifies par les methodes appropriees avant toute
   * manipulation.
   */
  public DParametresReflux3d() {
    super();
  }
  // Object
  /**
   * Cree un clone de l'ensemble de parametres.
   * @return l'ensemble de parametres clone de classe CParametresReflux
   */
  public Object clone() {
    return new DParametresReflux3d();
  }
  /**
   * Compare cet ensemble de parametres a un autre.
   * @param _o ensemble a comparer avec cet ensemble
   * @return true si les ensembles sont identiques.false sinon
   */
  public boolean equals(final Object _o) {
    boolean r= false;
    if(((org.omg.CORBA.Object)_o)._is_a("IParametresReflux3d")) {
      final IParametresReflux3d q= IParametresReflux3dHelper.narrow((org.omg.CORBA.Object)_o);
      r=
        (paramsINP_.equals(q.parametresINP())
          && paramsINI_.equals(q.parametresINI())
          && paramsCL_.equals(q.parametresCL()));
    }
    return r;
  }
  
  public int hashCode() {
    return paramsCL_.hashCode()+3*paramsINI_.hashCode()+13*paramsCL_.hashCode();
  }
  /**
   * Convertit cet ensemble de parametres en un objet String.
   * @return la chaine correspondante a cet ensemble de parametres
   */
  public String toString() {
    return "DParametresReflux3d()";
  }
  // Interface IParametresReflux3d
  /**
   * D�finit la racine du projet. Cette racine sert de noms � tous les fichiers
   * du calcul. Cette racine n'est constitu�e que du nom du fichier sans
   * extension et sans chemin.
   *
   * Chaque fois que la racine est modifi�e, les r�sultats associ�s doivent
   * �tre relus.
   *
   * @param _rac La racine du projet.
   */
  public void setRacine(final String _rac, final IResultatsReflux3d _r) {
    if (!_rac.equals(racine_)) {
      _r.clear();
    }
    racine_= _rac;
  }
  /**
   * Retourne la racine des fichiers de calcul.
   *
   * @return Racine.
   */
  public String getRacine() {
    return racine_;
  }
  /**
   * Retourne la racine des noms de fichiers parametres.
   * @return La racine des noms.
   */
  //  public String fichier()
  //  { return nomFichier_; }
  /**
   * Affecte la racine des noms de fichiers parametres.
   * @param _fichier La racine du nom.
   */
  //  public void fichier(String _fichier)
  //  {
  //    nomFichier_=_fichier;
  //  }
  /**
   * Retourne les donn�es du .INP de cet ensemble de param�tres.
   * @return Les donn�es.
   */
  public SParametresR3dINP parametresINP() {
    return paramsINP_;
  }
  /**
   * Modifie les donn�es du .INP de cet ensemble de param�tres.
   * @param _params Les nouvelles donn�es du .INP.
   */
  public void parametresINP(final SParametresR3dINP _params) {
    paramsINP_= _params;
  }
  /**
   * Retourne les solutions initiales de cet ensemble de param�tres.
   * @return Les donn�es.
   */
  public SParametresR3dINI parametresINI() {
    return paramsINI_;
  }
  /**
   * Retourne si les donn�es comportent des conditions limites de type
   * transitoires.
   *
   * @return <i>true</i> Le projet contient des CL transitoires.
   *         <i>false</i> Pas de CL transitoires.
   */
  public boolean bCLTransitoires() {
    return (parametresCL() != null);
  }
  /**
   * Modifie les solutions initiales de cet ensemble de param�tres.
   * @param _params les nouvelles solutions initiales.
   */
  public void parametresINI(final SParametresR3dINI _params) {
    paramsINI_= _params;
  }
  /**
   * Retourne les conditions limites transitoires de cet ensemble de param�tres.
   * @return Les conditions limites.
   */
  public SParametresR3dCL parametresCL() {
    return paramsCL_;
  }
  /**
   * Modifie les conditions initiales transitoires de cet ensemble de param�tres.
   * @param _params Les nouvelles conditions.
   */
  public void parametresCL(final SParametresR3dCL _params) {
    paramsCL_= _params;
  }
  // M�thodes locales
  /**
   * Ecriture des param�tres sur fichiers.
   *
   * @param _fichiers Nom des fichiers.
   * @param _params Parametres � �crire.
   */
  public static void ecritSurFichiers(
    final String _fichiers,
    final IParametresReflux3d _params) {
    ecritParametresReflux3dINP(_fichiers, _params);
    ecritParametresReflux3dINI(_fichiers, _params);
    if (_params.bCLTransitoires()) {
      ecritParametresReflux3dCL(_fichiers, _params);
    }
    ecritData(new File(_fichiers + ".dat"), _params);
  }
  /**
   * Ecrit les donn�es du fichiers .INP � partir des param�tres stock�s.
   * @param _nomFichier Le nom du fichier .INP (sans extension) contenant les
   *        donn�es
   * @param _params Param�tres stock�es
   */
  private static void ecritParametresReflux3dINP(
    final String _nomFichier,
    final IParametresReflux3d _params) {
    // Format
    int[] fmt;
    // Incr�mentation
    int i;
    int j;
    // Parametres INP
    final SParametresR3dINP paramsINP= _params.parametresINP();
    // Nombre de champs deja stockes avant �criture de l'enregistrement
    int nombreFields= 0;
    // Nombre de P.E. par groupe de P.E.
    final int nbMaxPEGroupe= 16;
    // Fichier .inp
    FortranWriter file= null;
    try {
      // Ouverture du fichier
      file= new FortranWriter(new FileWriter(_nomFichier + ".inp"));
      // Pas de chaines limit�es par des apostrophes en format libre
      file.setStringQuoted(false);
      //------------------------------------------------------------------------
      //---  BLOC 'DATA'  ------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('DATA')
      file.stringField(0, "DATA");
      file.writeFields();
      //------------------------------------------------------------------------
      //---  BLOC 'COOR'  ------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('COOR',<nb dimensions=3>,<nb noeuds>,
      //         <nb noeuds sur la verticale>,<nb termes de la base>);
      file.stringField(0, "COOR");
      file.intField(1, 3);
      file.intField(2, paramsINP.coordonnees.length);
      file.intField(3, paramsINP.nbNoeudsVerticale);
      file.intField(4, paramsINP.nbMaxiFonctions);
      fmt= new int[] { 5, 5, 5, 5, 5 };
      file.writeFields(fmt);
      // Coordonn�es des noeuds (<X>,<Y>,<Z>)
      fmt= new int[] { 10, 10, 10 };
      for (i= 0; i < paramsINP.coordonnees.length; i++) {
        file.floatField(0, paramsINP.coordonnees[i].x);
        file.floatField(1, paramsINP.coordonnees[i].y);
        file.floatField(2, paramsINP.coordonnees[i].z);
        file.writeFields(fmt);
      }
      //------------------------------------------------------------------------
      //---  BLOC 'ELEM'  ------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('ELEM',<nb total d'�l�ments>,<nb maxi de noeuds par �l�ment>)
      file.stringField(0, "ELEM");
      file.intField(1, paramsINP.connectivites.length);
      file.intField(2, paramsINP.nbMaxiNoeudsElement);
      fmt= new int[] { 5, 5, 5 };
      file.writeFields(fmt);
      // Connectivit�s
      fmt= new int[] { 5, 5, 5, 5, 5, 5 };
      for (i= 0; i < paramsINP.connectivites.length; i++) {
        for (j= 0; j < paramsINP.connectivites[i].iNoeuds.length; j++) {
          file.intField(j, paramsINP.connectivites[i].iNoeuds[j] + 1);
        }
        file.writeFields(fmt);
      }
      //------------------------------------------------------------------------
      //---  BLOC 'TPEL'  ------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('TPEL')
      file.stringField(0, "TPEL");
      fmt= new int[] { 5 };
      file.writeFields(fmt);
      // Type des �l�ments
      fmt= new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
      nombreFields= 0;
      for (i= 0; i < paramsINP.typesElements.length; i++) {
        if (nombreFields == fmt.length) {
          file.writeFields(fmt);
          nombreFields= 0;
        }
        file.intField(nombreFields, paramsINP.typesElements[i]);
        nombreFields++;
      }
      file.writeFields(fmt);
      //------------------------------------------------------------------------
      //---  BLOC 'NFN'  -------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('NFN')
      file.stringField(0, "NFN");
      fmt= new int[] { 5 };
      file.writeFields(fmt);
      // Fonction (<base fonctionnelle>,<nb fonctions/noeuds>)
      fmt= new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
      nombreFields= 0;
      for (i= 0; i < paramsINP.typesElements.length; i++) {
        if (nombreFields == fmt.length) {
          file.writeFields(fmt);
          nombreFields= 0;
        }
        file.intField(nombreFields++, paramsINP.fonctionsNoeuds[i].iBase + 1);
        file.intField(nombreFields++, paramsINP.fonctionsNoeuds[i].nbFonctions);
      }
      file.writeFields(fmt);
      //------------------------------------------------------------------------
      //---  BLOC 'DLPN'  ------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('DLPN')
      file.stringField(0, "DLPN");
      fmt= new int[] { 5 };
      file.writeFields(fmt);
      // Nombre de DDL par noeuds
      fmt= new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
      nombreFields= 0;
      for (i= 0; i < paramsINP.nbsDLNoeuds.length; i++) {
        if (nombreFields == fmt.length) {
          file.writeFields(fmt);
          nombreFields= 0;
        }
        file.intField(nombreFields, paramsINP.nbsDLNoeuds[i]);
        nombreFields++;
      }
      file.writeFields(fmt);
      //------------------------------------------------------------------------
      //---  BLOC 'COND'  ------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('COND')
      file.stringField(0, "COND");
      fmt= new int[] { 5 };
      file.writeFields(fmt);
      // par condition limite
      for (i= 0; i < paramsINP.conditionsLimites.length; i++) {
        // Codes (<0:Inutile, 1:Non-stat, 2:Stat>)
        file.intField(0, paramsINP.conditionsLimites[i].codes[0]);
        file.intField(1, paramsINP.conditionsLimites[i].codes[1]);
        file.intField(2, paramsINP.conditionsLimites[i].codes[2]);
        fmt= new int[] { 5, 5, 5 };
        file.writeFields(fmt);
        // Valeurs
        file.floatField(0, paramsINP.conditionsLimites[i].valeurs[0]);
        file.floatField(1, paramsINP.conditionsLimites[i].valeurs[1]);
        file.floatField(2, paramsINP.conditionsLimites[i].valeurs[2]);
        fmt= new int[] { 10, 10, 10 };
        file.writeFields(fmt);
        // Noeuds support (termin�s par 0)
        fmt= new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
        nombreFields= 0;
        for (j= 0;
          j < paramsINP.conditionsLimites[i].iNoeuds.length + 1;
          j++) {
          if (nombreFields == fmt.length) {
            file.writeFields(fmt);
            nombreFields= 0;
          }
          // 0 de fin
          if (j == paramsINP.conditionsLimites[i].iNoeuds.length) {
            file.intField(nombreFields, 0);
          // Num�ro de noeud
          } else {
            file.intField(
              nombreFields,
              paramsINP.conditionsLimites[i].iNoeuds[j] + 1);
          }
          nombreFields++;
        }
        file.writeFields(fmt);
      }
      String blanc =CtuluLibString.ESPACE;
      // 1 ligne blanche
      file.stringField(0, blanc);
      file.writeFields();
      //------------------------------------------------------------------------
      //---  BLOC 'PRND'  ------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('PRND',<nombre de prop. nodales par noeuds=2>)
      file.stringField(0, "PRND");
      file.intField(1, 2);
      fmt= new int[] { 5, 5 };
      file.writeFields(fmt);
      // Valeurs
      fmt= new int[] { 10, 10, 10, 10, 10, 10, 10, 10 };
      nombreFields= 0;
      for (i= 0; i < paramsINP.propsNoeuds.length; i++) {
        if (nombreFields == fmt.length) {
          file.writeFields(fmt);
          nombreFields= 0;
        }
        // B.M. Un peu alambiqu� j'en conviens, mais j'ai pas trouv� mieux
        // pour l'instant pour �viter les problemes de valeurs qui ne tiennent
        // pas sur un champs de longueur donn�e
        file.floatField(
          nombreFields,
          Math.floor(paramsINP.propsNoeuds[i].valeurs[0] * 10000) / 10000);
        nombreFields++;
        file.floatField(
          nombreFields,
          Math.floor(paramsINP.propsNoeuds[i].valeurs[1] * 10000) / 10000);
        nombreFields++;
      }
      file.writeFields(fmt);
      //------------------------------------------------------------------------
      //---  BLOC 'PREL'  ------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('PREL',<nombre maxi de prel/groupe>)
      file.stringField(0, "PREL");
      file.intField(1, nbMaxPEGroupe);
      fmt= new int[] { 5, 5 };
      file.writeFields(fmt);
      // Groupe de PREL
      // Valeurs
      fmt= new int[] { 10, 10, 10, 10, 10, 10, 10, 10 };
      nombreFields= 0;
      for (i= 0; i < paramsINP.groupePE.valeurs.length; i++) {
        if (nombreFields == fmt.length) {
          file.writeFields(fmt);
          nombreFields= 0;
        }
        file.floatField(nombreFields, paramsINP.groupePE.valeurs[i]);
        nombreFields++;
      }
      file.writeFields(fmt);
      // 1 ligne blanche
      //      file.stringField(0," ");
      //      file.writeFields();
      //------------------------------------------------------------------------
      //---  BLOC 'TEMP'  ------------------------------------------------------
      //------------------------------------------------------------------------
      // Entete ('TEMP')
      file.stringField(0, "TEMP");
      file.writeFields();
      // Solutions initiales sur fichier
      // (<nom du fichier>,<num�ro d'unit� logique=9>)
      file.stringField(0, blanc);
      file.stringField(1, new File(_nomFichier).getName() + ".ini");
      file.intField(2, 9);
      fmt= new int[] { 5, 10, 5 };
      file.writeFields(fmt);
      // Solutions initiales constantes (non utilis�)
      file.stringField(0, blanc);
      file.writeFields();
      // Masses volumiques sur fichier .ro (non utilis�)
      // (<nom du fichier>,<num�ro d'unit� logique=0>)
      file.stringField(0, blanc);
      file.stringField(1, new File(_nomFichier).getName() + ".ro");
      file.intField(2, 0);
      fmt= new int[] { 5, 10, 5 };
      file.writeFields(fmt);
      // Masses volumiques (toutes=0)
      file.stringField(0, blanc);
      for (i= 0; i < paramsINP.nbMaxiFonctions; i++) {
        file.doubleField(i + 1, 0);
      }
      fmt=
        new int[] {
          5,
          10,
          10,
          10,
          10,
          10,
          10,
          10,
          10,
          10,
          10,
          10,
          10,
          10,
          10,
          10 };
      file.writeFields(fmt);
      // Conditions limites transitoires
      // (<nom du fichier>,<num�ro d'unit� logique=0 ou 10>)
      file.stringField(0, blanc);
      file.stringField(1, new File(_nomFichier).getName() + ".cl");
      file.intField(2, _params.bCLTransitoires() ? 10 : 0);
      fmt= new int[] { 5, 10, 5 };
      file.writeFields(fmt);
      // Indices (<convection>,<frottement>,<pression>)
      file.intField(0, paramsINP.groupePT.convection);
      file.intField(1, paramsINP.groupePT.frottement);
      file.intField(2, paramsINP.groupePT.pression);
      fmt= new int[] { 5, 5, 5 };
      file.writeFields(fmt);
      // Indices (<schema>,<calcul matrice>,<symetrie=1>)
      file.intField(0, paramsINP.groupePT.schemaResolution);
      file.intField(1, paramsINP.groupePT.calculMatrice);
      file.intField(2, 1);
      fmt= new int[] { 5, 5, 5 };
      file.writeFields(fmt);
      // Indices (<correction=0>,<inutilis�=0>)
      file.intField(0, 0);
      file.intField(1, 0);
      fmt= new int[] { 5, 5 };
      file.writeFields(fmt);
      // Int�gration en temps (<nb pas de temps>,<nb max. d'it�rations>,
      // <pas d'impression>)
      file.intField(0, paramsINP.groupePT.nbPasDeTemps);
      file.intField(1, paramsINP.groupePT.nbMaxIterations);
      file.intField(2, paramsINP.groupePT.pasImpression);
      fmt= new int[] { 5, 5, 5 };
      file.writeFields(fmt);
      // Int�gration en temps (<valeur pdt>,<pr�cision Newton-Raphson>)
      file.doubleField(0, paramsINP.groupePT.valeurPasDeTemps);
      file.doubleField(1, paramsINP.groupePT.precisionNR);
      fmt= new int[] { 10, 10 };
      file.writeFields(fmt);
      // 1 ligne blanche
      //      file.stringField(0," ");
      //      file.writeFields();
      //------------------------------------------------------------------------
      //---  INSTRUCTION 'STOP'  -----------------------------------------------
      //------------------------------------------------------------------------
      file.stringField(0, "STOP");
      file.writeFields();
      // Fermeture du fichier
      file.flush();
      file.close();
    } catch (final IOException ex) {
      // CDodico.exception(this, ex);
      ex.printStackTrace();
    }
  }
  /**
   * Ecrit les solutions initiales sur le fichier .ini � partir des param�tres
   * stock�s. Le format du fichier .ini utilis� par Reflux 3D ressemble au
   * fichier .sol.
   *
   * @param _nomFichier Le nom du fichier .ini (sans extension) contenant les
   *        solutions initiales
   * @param _params Solutions initiales stock�es
   */
  private static void ecritParametresReflux3dINI(
    final String _nomFichier,
    final IParametresReflux3d _params) {
    int[] fmt;
    FortranWriter file;
    final SParametresR3dINI paramsINI= _params.parametresINI();
    //    SParametresR3dINP paramsINP=_params.parametresINP();
    try {
      // Ouverture du fichier
      file= new FortranWriter(new FileWriter(_nomFichier + ".ini"));
      // Pas de chaines limit�es par des apostrophes en format libre
      file.setStringQuoted(false);
      // Ecriture des solutions initiales
      for (int i= 0; i < paramsINI.hs.length; i++) {
        // Ligne 1 noeud (<iNoeud>,<x=0>,<y=0>,<z=0>,<ht=0>,<hs>,<u1>,<u2>,..)
        file.intField(0, i + 1);
        file.doubleField(1, 0);
        file.doubleField(2, 0);
        file.doubleField(3, 0);
        file.doubleField(4, 0);
        file.doubleField(5, paramsINI.hs[i]);
        for (int j= 0; j < paramsINI.u[i].length; j++) {
          file.doubleField(6 + j, paramsINI.u[i][j]);
        }
        fmt= new int[6 + paramsINI.u[i].length];
        fmt[0]= 5;
        for (int j= 1; j < fmt.length; j++) {
          fmt[j]= 12;
        }
        file.writeFields(fmt);
        // Ligne 2 noeud (<v1>,<v2>,...,<w1>,...)
        file.stringField(0, " ");
        for (int j= 0; j < paramsINI.v[i].length; j++) {
          file.doubleField(1 + j, paramsINI.v[i][j]);
        }
        for (int j= 0; j < paramsINI.w[i].length; j++) {
          file.doubleField(1 + paramsINI.v[i].length + j, paramsINI.w[i][j]);
        }
        fmt= new int[1 + paramsINI.v[i].length + paramsINI.w[i].length];
        fmt[0]= 5;
        for (int j= 1; j < fmt.length; j++) {
          fmt[j]= 12;
        }
        file.writeFields(fmt);
      }
      // Fermeture du fichier
      file.close();
    } catch (final IOException ex) {
      // CDodico.exception(this, ex);
      ex.printStackTrace();
      return;
    }
  }
  /**
   * Ecrit les conditions limites transitoires sur fichier .cl.
   * @param _nomFichier Le nom du fichier .clv (sans extension) contenant les
   *        conditions limites
   * @param _params Conditions limites transitoires stock�es
   */
  private static void ecritParametresReflux3dCL(
    final String _nomFichier,
    final IParametresReflux3d _params) {
    int[] fmt;
    FortranWriter file;
    final SParametresR3dConditionLimite[][] cls=
      _params.parametresCL().conditionsLimites;
    try {
      // Ouverture du fichier
      file= new FortranWriter(new FileWriter(_nomFichier + ".cl"));
      // Pas de chaines limit�es par des apostrophes en format libre
      file.setStringQuoted(false);
      // Pour chaque pas de temps
      for (int i= 0; i < cls.length; i++) {
        int nombreFields;
        // Pour chaque condition limite du pas de temps
        for (int j= 0; j < cls[i].length; j++) {
          // Codes (<0:Inutile, 1:Non-stat, 2:Stat>)
          fmt= new int[cls[i][j].codes.length];
          for (int k= 0; k < cls[i][j].codes.length; k++) {
            fmt[k]= 5;
            file.intField(k, cls[i][j].codes[k]);
          }
          file.writeFields(fmt);
          // Valeurs
          fmt= new int[cls[i][j].codes.length];
          for (int k= 0; k < cls[i][j].codes.length; k++) {
            fmt[k]= 10;
            file.floatField(k, cls[i][j].valeurs[k]);
          }
          file.writeFields(fmt);
          // Noeuds support (termin�s par 0)
          fmt= new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
          nombreFields= 0;
          for (int k= 0; k < cls[i][j].iNoeuds.length + 1; k++) {
            if (nombreFields == fmt.length) {
              file.writeFields(fmt);
              nombreFields= 0;
            }
            // 0 de fin
            if (k == cls[i][j].iNoeuds.length) {
              file.intField(nombreFields, 0);
            // Num�ro de noeud
            } else {
              file.intField(nombreFields, cls[i][j].iNoeuds[k] + 1);
            }
            nombreFields++;
          }
          file.writeFields(fmt);
        }
        // Ligne blanche pour fin de pas de temps
        file.writeFields();
      }
      // Fermeture du fichier
      file.close();
    } catch (final IOException ex) {
      // CDodico.exception(this, ex);
      ex.printStackTrace();
      return;
    }
  }
  /**
   * Ecrit les donn�es non stock�es mais n�cessaires � la lecture des fichiers
   * r�sultats.
   *
   * @param _file Le nom du fichier .dat
   */
  private static void ecritData(final File _file, final IParametresReflux3d _params) {
    final SParametresR3dINP parINP= _params.parametresINP();
    FileOutputStream file= null;
    try {
      // Ouverture du fichier
      file= new FileOutputStream(_file);
      // Propri�t�s
      final Properties prs= new Properties();
      prs.setProperty("nbNoeuds", "" + parINP.nbFonctionsNoeuds.length);
      for (int i= 0; i < parINP.nbFonctionsNoeuds.length; i++) {
        prs.setProperty(
          "nbTermesNoeud[" + i + "]",
          "" + parINP.nbFonctionsNoeuds[i]);
      }
      prs.store(file, "Donn�es diverses");
      file.close();
    } catch (final IOException ex) {
      // CDodico.exception(this, ex);
      ex.printStackTrace();
    }
  }
}
