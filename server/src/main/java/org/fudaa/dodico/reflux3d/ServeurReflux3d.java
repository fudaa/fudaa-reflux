/*
 * @file         ServeurReflux3d.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 14:45:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.reflux3d;
import java.util.Date;

import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.objet.CDodico;
/**
 * Un serveur Reflux 3D.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 14:45:55 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public final class ServeurReflux3d {
  
  private ServeurReflux3d(){}
  public static void main(final String[] _args) {
    final String nom=
      (_args.length > 0
        ? _args[0]
        : CDodico.generateName("::reflux3d::ICalculReflux3d"));
    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    //Cette M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculReflux3d.class));
    System.out.println("Reflux3d server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}
