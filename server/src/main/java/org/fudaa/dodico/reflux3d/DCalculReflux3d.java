/**
 * @file         DCalculReflux3d.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 14:45:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.reflux3d;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.PrintStream;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.reflux3d.ICalculReflux3d;
import org.fudaa.dodico.corba.reflux3d.ICalculReflux3dOperations;
import org.fudaa.dodico.corba.reflux3d.IParametresReflux3d;
import org.fudaa.dodico.corba.reflux3d.IParametresReflux3dHelper;
import org.fudaa.dodico.corba.reflux3d.IResultatsReflux3d;
import org.fudaa.dodico.corba.reflux3d.IResultatsReflux3dHelper;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
/**
 * Classe Calcul de Reflux3d.
 *
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 14:45:55 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class DCalculReflux3d
  extends DCalcul
  implements ICalculReflux3d,ICalculReflux3dOperations {
  private String traceExecution_= "";
  private boolean succes_;
  /**
   * 
   */
  public DCalculReflux3d() {
    super();
  }
  public Object clone() {
    return new DCalculReflux3d();
  }
  public String toString() {
    return "DCalculReflux3d()";
  }
  public String description() {
    return "Reflux3d, serveur de calcul pour l'hydrodynamique: "
      + super.description();
  }
  public String traceExecution() {
    return traceExecution_;
  }
  public boolean estOK() {
    return succes_;
  }
  /**
   * Controle que le serveur est op�rationnel. Le serveur est op�rationnel
   * si l'ex�cutable est disponible.
   */
  public boolean estOperationnel() {
    boolean op;
    final String os= System.getProperty("os.name");
    final String path= cheminServeur();
    // On ne teste que sur l'existence du fichier de lancement, l'ex�cutable
    // ayant un nom difficile � controler.
    if (os.startsWith("Windows")) {
      op= new File(path + "launcher.bat").exists();
    } else {
      op= new File(path + "launcher.sh").exists();
    }
    return op;
  }
  /**
   * Retourne si une reprise de calcul est possible. Ceci
   * implique que les fichiers r�sultats .vno existe.
   *
   * @param _racine Racine des fichiers calcul sans path.
   */
  public boolean accepteRepriseCalcul(final String _racine, final IConnexion _c) {
    final IParametresReflux3d params= IParametresReflux3dHelper.narrow(parametres(_c));
    if (params == null) {
      return false;
    }
    final String path= cheminServeur();
    File fTest;
    // Controle sur le fichier .vno.
    fTest= new File(path + params.getRacine() + ".vno");
    if (!fTest.exists()) {
      message("Fichier " + fTest.getPath() + " inexistant");
      return false;
    }
    return true;
  }
  /**
   * Affiche un message du serveur.
   *
   * @param _m Message.
   */
  public static void message(final String _m) {
    System.out.println("ServeurReflux3d : " + _m);
  }
  public void calcul(final IConnexion _c) {
    succes_= false;
    traceExecution_= "";
    if (!verifieConnexion(_c)) {
      return;
    }
    final IParametresReflux3d params= IParametresReflux3dHelper.narrow(parametres(_c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    final IResultatsReflux3d results= IResultatsReflux3dHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
    }
    log(_c, "lancement du calcul");
    final String fichier= params.getRacine();
    final String os= System.getProperty("os.name");
    final String path= cheminServeur();
    try {
      // Ecriture des fichiers
      DParametresReflux3d.ecritSurFichiers(path + fichier, params);
      message("Ex�cution de reflux3d pour " + fichier);
      String[] cmd;
      int icmd= 0;
      if (os.startsWith("Windows")) {
        cmd= new String[4];
        cmd[icmd++]= path + "launcher.bat";
        cmd[icmd++]= fichier;
        if (path.indexOf(':') != -1) {
          // lettre de l'unite (ex: "C:")
          cmd[icmd++]= path.substring(0, path.indexOf(':') + 1);
          // chemin du serveur
          cmd[icmd++]= path.substring(path.indexOf(':') + 1);
        } else {
          // si pas de lettre dans le chemin
          cmd[icmd++]= "fake_cmd";
          cmd[icmd++]= path;
        }
      } else {
        cmd= new String[2];
        cmd[icmd++]= path + "launcher.sh";
        cmd[icmd++]= fichier;
      }
      final CExec ex= new CExec();
      ex.setCommand(cmd);
      //      Process p=Runtime.getRuntime().exec(cmd);
      //      BufferedReader errs=new BufferedReader(new InputStreamReader(p.getErrorStream()));
      //      BufferedReader outs=new BufferedReader(new InputStreamReader(p.getInputStream()));
      //
      //      p.waitFor();
      final ByteArrayOutputStream outs= new ByteArrayOutputStream();
      final ByteArrayOutputStream errs= new ByteArrayOutputStream();
      ex.setOutStream(new PrintStream(outs));
      ex.setErrStream(new PrintStream(errs));
      ex.exec();
      final String sErr= errs.toString();
      //      String sOut=outs.toString();
      final int etat= ex.getProcess().exitValue();
      //      int c;
      //      while ((c=is.read())!=-1) sResult+=(char)c;
      // Succ�s du calcul si aucune erreur et fichiers r�sultats non vides
      if (etat == 0
        && new File(path + fichier + ".sol").length() != 0
        && new File(path + fichier + ".vno").length() != 0
        && new File(path + fichier + ".sl").length() != 0
        && new File(path + fichier + ".s").length() != 0) {
        succes_= true;
      } else {
        succes_= false;
      }
      // Trace d'ex�cution
      final StringBuffer traceBuf= new StringBuffer();
      if (new File(path + fichier + ".out").exists()) {
        String line;
        final LineNumberReader fout=
          new LineNumberReader(new FileReader(path + fichier + ".out"));
        while ((line= fout.readLine()) != null) {
          traceBuf.append(line).append("\n");
        }
      }
      //      traceBuf.append(sOut);
      if (!succes_) {
        traceBuf.append(sErr);
      }
      traceExecution_= traceBuf.toString();
      log(_c, "calcul termin�");
      if (succes_) {
        message("Fin du calcul");
      } else {
        message("Erreurs : " + sErr);
      }
    } catch (final Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
}
