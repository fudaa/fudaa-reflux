/**
 * @file         DResultatsReflux3d.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 14:45:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.reflux3d;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;
import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.corba.reflux3d.IParametresReflux3d;
import org.fudaa.dodico.corba.reflux3d.IResultatsReflux3d;
import org.fudaa.dodico.corba.reflux3d.IResultatsReflux3dOperations;
import org.fudaa.dodico.corba.reflux3d.SResultatsEtapeR3dSOL;
import org.fudaa.dodico.corba.reflux3d.SResultatsEtapeR3dVNO;
import org.fudaa.dodico.corba.reflux3d.SResultatsLigneR3dSOL;
import org.fudaa.dodico.corba.reflux3d.SResultatsLigneR3dVNO;
import org.fudaa.dodico.corba.reflux3d.SResultatsR3dSOL;
import org.fudaa.dodico.corba.reflux3d.SResultatsR3dVNO;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.objet.CDodico;
/**
 * Les resultats Reflux3d.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 14:45:55 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class DResultatsReflux3d
  extends DResultats
  implements IResultatsReflux3d,IResultatsReflux3dOperations {
  private IParametresReflux3d params_;
  // Nom du fichier correspondant aux r�sultats
  //  private String nomFichier_=null;
  // Nombre de noeuds du domaine calcul�
  private int nbNds_;
  // Nombre de fonctions par noeud pour la lecture du fichier .vno
  private int[] nbFctsNds_;
  // R�sultats VNO
  private SResultatsR3dVNO rVNO_;
  // R�sultats SOL
  private SResultatsR3dSOL rSOL_;
  /**
   * Construit un ensemble de r�sultats.
   */
  //  public CResultatsReflux3d()
  //  {
  //    super();
  //  }
  /**
   * Construit un ensemble de r�sultats.
   */
  public DResultatsReflux3d() {
    super();
    //    nomFichier_=_nomFichier;
    //    nbNds_  =_nbNds;
    //    nbFctsNds_ =_nbFctsNds;
  }
  public void parametres(final IParametresReflux3d _p) {
    params_= _p;
  }
  // Object
  /**
   * Cree un clone de l'ensemble de r�sultats.
   * @return l'ensemble de r�sultats clone de classe CResultatsReflux
   */
  public Object clone() {
    final DResultatsReflux3d r= new DResultatsReflux3d();
    r.parametres(params_);
    return r;
  }
  /**
   * Convertit cet ensemble de r�sultats en un objet String.
   * @return la chaine correspondante a cet ensemble de parametres
   */
  public String toString() {
    return "DResultatsReflux3d()";
  }
  // Interface IResultatsReflux3d
  public void clear() {
    nbNds_= 0;
    nbFctsNds_= null;
    rVNO_= null;
    rSOL_= null;
  }
  /**
   * Retourne les r�sultats calcul�s du .vno sous forme de structure.
   * @return La structure contenant les valeurs pour tous les noeuds � toutes
   *         les �tapes.
   */
  public SResultatsR3dVNO resultatsVNO() {
    final String racine= params_.getRacine();
    final String path=
      System.getProperty("user.dir")
        + File.separator
        + "serveurs"
        + File.separator
        + "reflux3d"
        + File.separator;
    // On lit les r�sultats sur fichier s'ils ne sont pas d�ja stock�s
    if (rVNO_ == null) {
      try {
        // Lecture du fichier de data
        litData(new File(path + racine + ".dat"));
        // Lecture du fichier des r�sultats
        final File fsov= new File(path + racine + ".vno");
        rVNO_= litResultatsReflux3dVNO(fsov, nbNds_, nbFctsNds_);
      } catch (final IOException _exc) {
        DCalculReflux3d.message(_exc.getMessage());
        //       CDodico.exception(this,_exc);
        return null;
      }
    }
    return rVNO_;
  }
  /**
   * Retourne les r�sultats calcul�s du .sol sous forme de structure.
   * @return La structure contenant les valeurs pour tous les noeuds � toutes
   *         les �tapes.
   */
  public SResultatsR3dSOL resultatsSOL() {
    final String racine= params_.getRacine();
    final String path=
      System.getProperty("user.dir")
        + File.separator
        + "serveurs"
        + File.separator
        + "reflux3d"
        + File.separator;
    // On lit les r�sultats sur fichier s'ils ne sont pas d�ja stock�s
    if (rSOL_ == null) {
      try {
        // Lecture du fichier de data
        litData(new File(path + racine + ".dat"));
        // Lecture du fichier des r�sultats
        rSOL_= litResultatsReflux3dSOL(path + racine, nbNds_);
      } catch (final IOException _exc) {
        CDodico.exception(this, _exc);
        return null;
      }
    }
    return rSOL_;
  }
  /**
   * Retourne les r�sultats calcul�s � une �tape sous forme de structure
   * @param instant Valeur du temps pour lequel on demande les r�sultats
   * @return La structure contenant les valeurs pour tous les noeuds � un
   *         instant donn� (null si aucune valeur n'a �t� calcul�e � cet instant)
   */
  //  public SResultatsEtapeReflux resultatsEtapeReflux(int instant)
  //  {
  //    // On lit les r�sultats sur fichier s'ils ne sont pas d�j� stock�s
  //    if (this.resultats == null)
  //     this.resultats = litResultatsReflux(nomFichier_,nbNds_);
  //
  //    // Recherche du pas de temps
  //    for (int i=0; i<this.resultats.etapes.length; i++) {
  //      // Pas de temps trouv�
  //      if (this.resultats.etapes[i].instant == instant) {
  //        return this.resultats.etapes[i];
  //      }
  //    }
  //
  //    // Pas de temps non trouv�
  //    CDodico.exception(this,"Etape non trouv�e pour le pas de temps : "+instant);
  //    return null;
  //  }
  /**
   * Retourne les r�sultats calcul�s pour un noeud � une �tape sous forme de
   * structure
   * @param instant Valeur du temps pour lequel on demande les r�sultats
   * @param numeroNoeud Numero du noeud pour lequel on demande les r�sultats
   * @return La structure contenant les valeurs pour un noeud � un instant
   *         donn� (null si aucune valeur n'a �t� calcul�e � cet instant)
   */
  //  public SResultatsLigneReflux resultatsLigneReflux
  //                             (int instant, int numeroNoeud)
  //  {
  //    SResultatsLigneReflux ligne = null;
  //
  //    // Recup�ration de l'�tape
  //    SResultatsEtapeReflux etape = resultatsEtapeReflux(instant);
  //
  //    if (etape != null) {
  //      try {
  //        // R�cup�ration des r�sultats du noeud
  //        ligne = etape.lignes[numeroNoeud];
  //      }
  //      catch (Exception ex) {
  //        CDodico.exception(this,"Noeud "+numeroNoeud+
  //                               " non trouv� au pas de temps "+instant);
  //      }
  //    }
  //    return ligne;
  //  }
  /**
   * Lit les resultats du calcul par Reflux 3d.
   * @param _nomFichier Le nom du fichier .sol (sans extension) contenant les
   *        r�sultats
   * @param _nbNds Nombre de noeuds du domaine calcul�.
   * @return La structure contenant les valeurs pour tous les noeuds � toutes
   *         les �tapes
   */
  private static SResultatsR3dSOL litResultatsReflux3dSOL(
    final String _nomFichier,
    final int _nbNds) {
    SResultatsR3dSOL r;
    // Fichier de lecture
    FortranReader file;
    // Stockage temporaire des �tapes
    final Vector etapes= new Vector();
    try {
      // Ouverture du fichier des solutions
      file= new FortranReader(new FileReader(_nomFichier + ".sol"));
    } catch (final FileNotFoundException ex) {
      CDodico.exception(
        "Erreur d'ouverture du fichier " + _nomFichier + ".sol");
      return null;
    }
    try {
      // Valeur du pas de temps lu sur fichier
      //      double temps;
      // Nombre de valeurs � lire sur le fichier
      int nbValeurs= 0;
      // Formats
      int[] fmt;
      // Lecture des �tapes (-999)
      while (true) {
        try {
          fmt= new int[] { 5 };
          file.readFields(fmt);
        }
        // Fin du fichier => Plus d'�tapes
        catch (final EOFException ex) {
          break;
        }
        if (file.intField(0) != -999) {
          throw new IOException();
        }
        // Cr�ation d'une nouvelle �tape
        final SResultatsEtapeR3dSOL etape= new SResultatsEtapeR3dSOL();
        etape.lignes= new SResultatsLigneR3dSOL[_nbNds];
        // (' ===== PAS NUMERO :',<pas de temps>,' ITERATION NUMERO :',
        //  <numero d'it�ration>
        fmt= new int[] { 19, 5, 19, 5 };
        file.readFields(fmt);
        // Pas de valeur de temps : instant=pdt.
        etape.instant= file.intField(2);
        DCalculReflux3d.message("SOL Pas       : " + file.intField(2));
        DCalculReflux3d.message("SOL Iteration : " + file.intField(4));
        // Seulement lors de la premi�re �tape
        if (etapes.size() < 1) {
          // (-1,<nombre de valeurs>)
          fmt= new int[] { 5, 5 };
          file.readFields(fmt);
          if (file.intField(0) != -1) {
            throw new IOException();
          }
          nbValeurs= file.intField(1);
          DCalculReflux3d.message("SOL Colonnes : " + nbValeurs);
        }
        // Cr�ation des lignes de l'�tape
        for (int i= 0; i < _nbNds; i++) {
          etape.lignes[i]= new SResultatsLigneR3dSOL();
          etape.lignes[i].valeurs= new double[nbValeurs];
        }
        // Lecture des lignes (<numero de noeud>,<valeurs>)
        fmt= new int[nbValeurs + 1];
        fmt[0]= 5;
        for (int i= 1; i < nbValeurs + 1; i++) {
          fmt[i]= 12;
        }
        for (int i= 0; i < _nbNds; i++) {
          file.readFields(fmt);
          etape.lignes[i].numNoeud= file.intField(0);
          for (int j= 0; j < nbValeurs; j++) {
            etape.lignes[i].valeurs[j]= file.doubleField(j + 1);
          }
        }
        etapes.addElement(etape);
      }
      // Fermeture du fichier
      file.close();
    } catch (final IOException e) {
      CDodico.exception("Erreur de lecture ligne " + file.getLineNumber());
      return null;
    }
    // Fin du fichier => Stockage des �tapes dans la structure ad�quate
    r= new SResultatsR3dSOL(new SResultatsEtapeR3dSOL[etapes.size()]);
    etapes.toArray(r.etapes);
    return r;
  }
  /**
   * Lit les donn�es non stock�es sur les fichiers r�sultats mais n�cessaires
   * � la lecture des fichiers r�sultats.
   * @param _file Le nom du fichier .dat
   */
  private void litData(final File _file) throws IOException {
    FileInputStream file= null;
    // Ouverture du fichier
    file= new FileInputStream(_file);
    // Propri�t�s
    final Properties prs= new Properties();
    prs.load(file);
    file.close();
    // Remplissage des variables
    nbNds_= Integer.parseInt(prs.getProperty("nbNoeuds", "0"));
    nbFctsNds_= new int[nbNds_];
    for (int i= 0; i < nbNds_; i++) {
      nbFctsNds_[i]=
        Integer.parseInt(prs.getProperty("nbTermesNoeud[" + i + "]", "1"));
    }
  }
  /**
   * Lit les resultats du calcul par Reflux 3d.
   * @param _file Le fichier de solutions contenant les r�sultats aux noeuds. Le
   *              fichier peut avoir n'importe quelle extension.
   * @param _nbNds Nombre de noeuds sur le fichier des solutions. Ce nombre
   *               devrait correspondre au nombre de noeuds du maillage du
   *               projet.
   * @param _nbFctsNds Nombre de fonctions par noeud.
   *
   * @return La structure contenant les valeurs pour tous les noeuds � toutes
   *         les �tapes.
   * @exception FileNotFoundException Fichier non trouv�.
   * @exception IOException           Erreur de lecture sur le fichier.
   */
  public static SResultatsR3dVNO litResultatsReflux3dVNO(
    final File _file,
    final int _nbNds,
    final int[] _nbFctsNds)
    throws IOException {
    SResultatsR3dVNO r;
    // Fichier de lecture
    FortranReader file;
    // Stockage temporaire des �tapes
    final Vector etapes= new Vector();
    try {
      // Ouverture du fichier des solutions
      file= new FortranReader(new FileReader(_file));
    } catch (final FileNotFoundException ex) {
      throw new FileNotFoundException("Erreur d'ouverture du fichier " + _file);
    }
    try {
      // Valeur du pas de temps lu sur fichier
      //      double temps;
      // Nombre de valeurs � lire sur le fichier
      //      int nbValeurs = 0;
      // Formats
      int[] fmt;
      int[] fmt2;
      // Lecture des �tapes (-999)
      while (true) {
        try {
          fmt= new int[] { 5 };
          file.readFields(fmt);
        }
        // Fin du fichier => Plus d'�tapes
        catch (final EOFException ex) {
          break;
        }
        if (file.intField(0) != -999) {
          throw new IOException();
        }
        // Cr�ation d'une nouvelle �tape
        final SResultatsEtapeR3dVNO etape= new SResultatsEtapeR3dVNO();
        etape.lignes= new SResultatsLigneR3dVNO[_nbNds];
        // (' ===== PAS NUMERO :',<pas de temps>,' ITERATION NUMERO :',
        //  <numero d'it�ration>
        fmt= new int[] { 19, 5, 20, 5 };
        file.readFields(fmt);
        // Pas de valeur de temps : instant=pdt.
        etape.instant= file.intField(1);
        DCalculReflux3d.message("VNO Pas       : " + file.intField(1));
        DCalculReflux3d.message("VNO Iteration : " + file.intField(3));
        // Seulement lors de la premi�re �tape
        if (etapes.size() < 1) {
          // (-1,<nombre de valeurs (inutilis�)>)
          fmt= new int[] { 5, 5 };
          file.readFields(fmt);
          if (file.intField(0) != -1) {
            throw new IOException();
          //          nbValeurs = file.intField(1);
          }
        }
        // Cr�ation des lignes de l'�tape
        for (int i= 0; i < _nbNds; i++) {
          etape.lignes[i]= new SResultatsLigneR3dVNO();
          etape.lignes[i].valeurs= new double[5 + _nbFctsNds[i] * 3];
        }
        // Lecture des valeurs aux noeuds
        fmt= new int[6 + 20];
        fmt[0]= 5;
        for (int i= 1; i < fmt.length; i++) {
          fmt[i]= 12;
        }
        fmt2= new int[1 + 20 * 2];
        fmt2[0]= 5;
        for (int i= 1; i < fmt2.length; i++) {
          fmt2[i]= 12;
        }
        for (int i= 0; i < _nbNds; i++) {
          //Ligne 1 (<numero de noeud>,<valeurs>)
          file.readFields(fmt);
          etape.lignes[i].numNoeud= file.intField(0);
          for (int j= 0; j < _nbFctsNds[i] + 5; j++) {
            etape.lignes[i].valeurs[j]= file.doubleField(j + 1);
          }
          // Ligne 2 (<valeurs suite>)
          file.readFields(fmt2);
          for (int j= 0; j < _nbFctsNds[i] * 2; j++) {
            etape.lignes[i].valeurs[j + _nbFctsNds[i] + 5]=
              file.doubleField(j + 1);
          }
        }
        etapes.addElement(etape);
      }
      // Fermeture du fichier
      file.close();
    } catch (final IOException e) {
      throw new IOException(
        "Erreur de lecture sur fichier "
          + _file
          + " ligne "
          + file.getLineNumber());
    }
    // Fin du fichier => Stockage des �tapes dans la structure ad�quate
    r= new SResultatsR3dVNO(new SResultatsEtapeR3dVNO[etapes.size()]);
    etapes.toArray(r.etapes);
    return r;
  }
}
