/**
 * @creation 2000-02-16
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux;

import java.util.Date;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Une classe serveur pour Reflux.
 * @version $Revision: 1.10 $ $Date: 2006-09-19 14:45:56 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public final class ServeurReflux {

  private ServeurReflux() {

  }

  /**
   * @param _args args[0], si defini, sera utilise pour le nom du serveur.
   */
  public static void main(final String[] _args){
    final String nom = (_args.length > 0 ? _args[0] : CDodico.generateName("::reflux::ICalculReflux"));
    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    //Cete M�tthode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculReflux.class));
    System.out.println("Reflux server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}