/**
 * @creation     1998-04-16
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux;

import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.reflux.ICalculReflux;
import org.fudaa.dodico.corba.reflux.ICalculRefluxOperations;
import org.fudaa.dodico.corba.reflux.IParametresReflux;
import org.fudaa.dodico.corba.reflux.IParametresRefluxHelper;
import org.fudaa.dodico.corba.reflux.IResultatsReflux;
import org.fudaa.dodico.corba.reflux.IResultatsRefluxHelper;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CDodico;

/**
 * Classe Calcul de Reflux.
 * 
 * @version $Revision: 1.10 $ $Date: 2006-09-19 14:45:56 $ by $Author: deniger $
 * @author Bertrand Marchand
 */
public class DCalculReflux extends DCalcul implements ICalculReflux, ICalculRefluxOperations {
  /**
   * Fin des extensions pour les fichiers de calcul. Pour la courantologie, les fichiers ont une extension qui se
   * termine par "v" (.sov,.siv, etc.).
   */
  private final String[] endExts_ = { "v", "v", "t" };
  // private boolean[] impression=null;
  private String traceExecution_ = "";
  private boolean succes_;

  /**
   * Constructeur par defaut.
   */
  public DCalculReflux() {
    super();
  }

  public Object clone() {
    return new DCalculReflux();
  }

  public String toString() {
    return "DCalculReflux()";
  }

  public String description() {
    return "Reflux, serveur de calcul pour l'hydrodynamique: " + super.description();
  }

  /**
   * Affecte les booleens d'impression pour les informations du calcul.
   * 
   * @param impression Booleens d'impression
   */
  // public void impression(boolean[] impression)
  // { this.impression = impression; }
  //
  /**
   * Retourne les booleens d'impression pour les informations du calcul.
   * 
   * @return Booleens d'impression
   */
  // public boolean[] impression()
  // {
  // return this.impression;
  // }
  public String traceExecution() {
    return traceExecution_;
  }

  public boolean estOK() {
    return succes_;
  }

  /**
   * Controle que le serveur est op�rationnel. Le serveur est op�rationnel si l'ex�cutable est disponible.
   */
  public boolean estOperationnel() {
    boolean op;
    final String os = System.getProperty("os.name");
    final String path = cheminServeur();
    // On ne teste que sur l'existence du fichier de lancement, l'ex�cutable
    // ayant un nom difficile � controler.
    if (os.startsWith("Windows")) {
      op = new File(path + "launcher.bat").exists();
    } else {
      op = new File(path + "launcher.sh").exists();
    }
    return op;
  }

  /**
   * Affiche un message du serveur.
   * 
   * @param _m Message.
   */
  public static void message(final String _m) {
    System.out.println("ServeurReflux2d : " + _m);
  }

  /**
   * Clear results before running a new analysis. Important for a thread waiting for results of analysis.
   */
  public void clear(final IConnexion _c) {
    succes_ = false;
    traceExecution_ = "";
    final IResultatsReflux results = IResultatsRefluxHelper.narrow(resultats(_c));
    if (results != null) {
      results.clear();
    }
  }

  public void calcul(final IConnexion _c) {
    succes_ = false;
    traceExecution_ = "";
    if (!verifieConnexion(_c)) {
      return;
    }
    final IParametresReflux params = IParametresRefluxHelper.narrow(parametres(_c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    final IResultatsReflux results = IResultatsRefluxHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
      return;
    }
    results.parametres(params);
    log(_c, "lancement du calcul");
    final String fichier = params.getRacine();
    final String os = System.getProperty("os.name");
    final String path = cheminServeur();
    try {
      // Ecriture des fichiers
      DParametresReflux.ecritSurFichiers(path + fichier, params);
      message("Ex�cution de reflux2d pour " + fichier);
      String[] cmd;
      int icmd = 0;
      if (os.startsWith("Windows")) {
        cmd = new String[4];
        cmd[icmd++] = path + "launcher.bat";
        cmd[icmd++] = fichier;
        if (path.indexOf(':') != -1) {
          // lettre de l'unite (ex: "C:")
          cmd[icmd++] = path.substring(0, path.indexOf(':') + 1);
          // chemin du serveur
          cmd[icmd++] = path.substring(path.indexOf(':') + 1);
        } else {
          // si pas de lettre dans le chemin
          cmd[icmd++] = "fake_cmd";
          cmd[icmd++] = path;
        }
      } else {
        cmd = new String[2];
        cmd[icmd++] = path + "launcher.sh";
        cmd[icmd++] = fichier;
      }
      // Suppress old results file
      new File(path + fichier + ".sov").delete();
      results.startReader(); // Launch asynchronous reader of results file.
      final Process p = Runtime.getRuntime().exec(cmd);
      final InputStreamReader is = new InputStreamReader(p.getErrorStream());
      p.waitFor();
      results.stopReader(); // Stop asynchronous reader
      String sResult = "";
      int co;
      while ((co = is.read()) != -1) {
        sResult += (char) co;
      }
      // Succ�s du calcul si aucune erreur et fichier r�sultat existant
      final int tpPb = params.parametresINP().typeProbleme;
      if (sResult.equals("") && new File(path + fichier + ".so" + endExts_[tpPb]).length() != 0) {
        succes_ = true;
      } else {
        succes_ = false;
      }
      // Trace d'ex�cution
      final StringBuffer traceBuf = new StringBuffer();
      if (new File(path + fichier + ".out").exists()) {
        String line;
        final LineNumberReader fout = new LineNumberReader(new FileReader(path + fichier + ".out"));
        try {
          while ((line = fout.readLine()) != null) {
            traceBuf.append(line).append("\n");
          }
        } finally {
          fout.close();
        }
      }
      traceBuf.append(sResult);
      traceExecution_ = traceBuf.toString();
      if (succes_) {
        message("Fin du calcul");
      } else {
        message("Erreurs : " + sResult);
      }
      log(_c, "calcul termin�");
    } catch (final Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
}
