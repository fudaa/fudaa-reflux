/**
 * @file         DResultatsReflux.java
 * @creation     1998-09-10
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.reflux;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.fudaa.dodico.corba.reflux.IParametresReflux;
import org.fudaa.dodico.corba.reflux.IResultatsReflux;
import org.fudaa.dodico.corba.reflux.IResultatsRefluxOperations;
import org.fudaa.dodico.corba.reflux.SResultatsEtapeReflux;
import org.fudaa.dodico.corba.reflux.SResultatsLigneReflux;
import org.fudaa.dodico.corba.reflux.SResultatsReflux;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.objet.CDodico;
/**
 * Les resultats Reflux.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 14:45:56 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class DResultatsReflux
  extends DResultats
  implements IResultatsReflux,IResultatsRefluxOperations {
  /** Parametres de calcul associ�s. Visibilite adaptee pour un
   * un usage dans les inner classes. Si variable privee
   * la JVM cree des accesseurs prives.*/
  IParametresReflux params_;
  /** Vector of steps. */
  List vsteps_;
  /** Results reader. */
  private ResultsReader resReader_;
  /** Index in steps for iterator. */
  private int icptSteps_;
  /** If false, there is no more step to come.  */
  boolean iteratorFilled_= true;
  /**
   * Construit un ensemble de r�sultats.
   */
  public DResultatsReflux() {
    super();
    vsteps_= new Vector();
  }
  public void parametres(final IParametresReflux _p) {
    params_= _p;
  }
  // Object
  /**
   * Cree un clone de l'ensemble de r�sultats.
   * @return l'ensemble de r�sultats clone de classe CResultatsReflux
   */
  public Object clone() {
    final DResultatsReflux r= new DResultatsReflux();
    r.parametres(params_);
    return r;
  }
  /**
   * Convertit cet ensemble de r�sultats en un objet String.
   * @return la chaine correspondante a cet ensemble de parametres
   */
  public String toString() {
    return "DResultatsReflux()";
  }
  // IResultatsReflux
  public void clear() {
    vsteps_.clear();
    iteratorFilled_= false;
    //    resultats_=null;
  }
  /**
   * Initialise iterator for results.
   */
  public void initIterator() {
    icptSteps_= 0;
  }
  /**
   * Is there a new step in file since last call to method next().
   * <p>
   * Warning : Results comes as soon as the result file is filled. The method
   * can return <code>false</code> an then  <code>true</code>.
   *
   * @see #isIteratorFilled()
   */
  public boolean hasIteratorNextStep() {
    return icptSteps_ < vsteps_.size();
  }
  /**
   * Get step in file.
   */
  public SResultatsEtapeReflux nextIteratorStep() {
    return (SResultatsEtapeReflux)vsteps_.get(icptSteps_++);
  }
  /**
   * Is iterator filled or is there some other steps to come ?
   */
  public boolean isIteratorFilled() {
    return iteratorFilled_;
  }
  /**
   * Start reader.
   * <p>
   * Important : This method is public to be internally accessible. Don't
   * use it.
   */
  public void startReader() {
    resReader_= new ResultsReader(this);
    resReader_.start();
  }
  /**
   * Stop is in demand for reader. This method waits until results are all read
   * from file.
   * <p>
   * Important : This method is public to be internally accessible. Don't
   * use it.
   */
  public void stopReader() {
    if (resReader_ == null) {
      return;
    }
    resReader_.stopNeeded();
    try {
      resReader_.join();
    } // Wait until thread is dead.
    catch (final InterruptedException _exc) {} // Should not append.
  }
  /**
   * Retourne les r�sultats calcul�s sous forme de structure.
   * @return La structure contenant les valeurs pour tous les noeuds � toutes
   *         les �tapes
   */
  public SResultatsReflux resultatsReflux() {
    return new SResultatsReflux(
      (SResultatsEtapeReflux[])vsteps_.toArray(new SResultatsEtapeReflux[0]));
  }
  /**
   * Retourne les r�sultats calcul�s � une �tape sous forme de structure.
   * @param _instant Valeur du temps pour lequel on demande les r�sultats
   * @return La structure contenant les valeurs pour tous les noeuds � un
   *         instant donn� (null si aucune valeur n'a �t� calcul�e � cet instant)
   */
  public SResultatsEtapeReflux resultatsEtapeReflux(final long _instant) {
    // Recherche du pas de temps
    for (int i= 0; i < vsteps_.size(); i++) {
      final SResultatsEtapeReflux step= (SResultatsEtapeReflux)vsteps_.get(i);
      if (step.instant == _instant) {
        return step; // Pas de temps trouv�
      }
    }
    // Pas de temps non trouv�
    CDodico.exception(
      this,
      "Etape non trouv�e pour le pas de temps : " + _instant);
    return null;
  }
  /**
   * Retourne les r�sultats calcul�s pour un noeud � une �tape sous forme de
   * structure.
   * @param _instant Valeur du temps pour lequel on demande les r�sultats
   * @param _numeroNoeud Numero du noeud pour lequel on demande les r�sultats
   * @return La structure contenant les valeurs pour un noeud � un instant
   *         donn� (null si aucune valeur n'a �t� calcul�e � cet instant)
   */
  public SResultatsLigneReflux resultatsLigneReflux(
    final long _instant,
    final int _numeroNoeud) {
    SResultatsLigneReflux ligne= null;
    // Recup�ration de l'�tape
    final SResultatsEtapeReflux etape= resultatsEtapeReflux(_instant);
    if (etape != null) {
      try {
        // R�cup�ration des r�sultats du noeud
        ligne= etape.lignes[_numeroNoeud];
      } catch (final Exception ex) {
        CDodico.exception(
          this,
          "Noeud " + _numeroNoeud + " non trouv� au pas de temps " + _instant);
      }
    }
    return ligne;
  }
  /**
   * Ecriture des r�sultats sur fichiers.
   *
   * @param _fichier Nom du fichier .sol sans extension.
   * @param _res R�sultats � �crire.
   */
  public static void ecritSurFichiers(final File _fichier, final SResultatsReflux _res) {
    ecritResultatsRefluxSOL(_fichier, _res);
  }
  /**
   * Ecriture des r�sultats sur fichier .sol.
   *
   * @param _fichier Nom du fichier sans extension
   * @param _params Parametres � �crire.
   */
  private static void ecritResultatsRefluxSOL(
    final File _fichier,
    final SResultatsReflux _res) {
    int[] fmt;
    try {
      final FortranWriter file= new FortranWriter(new FileWriter(_fichier));
      // Pas de chaines limit�es par des apostrophes en format libre
      file.setStringQuoted(false);
      final SResultatsEtapeReflux[] etapes= _res.etapes;
      for (int ipas= 0; ipas < etapes.length; ipas++) {
        // " -999"
        file.stringField(0, " -999");
        file.writeFields();
        // " ===== PAS NUMERO :",<pas>," ITERATION NUMERO :",<iter=1>," TEMPS ",<temps>
        fmt= new int[] { 19, 5, 19, 5, 19, 12 };
        file.stringField(0, " ===== PAS NUMERO :");
        file.intField(1, ipas + 1);
        file.stringField(2, " ITERATION NUMERO :");
        file.intField(3, 1);
        file.stringField(4, "    TEMPS          ");
        file.floatField(5, etapes[ipas].instant);
        file.writeFields(fmt);
        final int nbValeurs= etapes[0].lignes[0].valeurs.length;
        // "   -1",<nombre de r�sultats> (si 1er pas de temps)
        if (ipas == 0) {
          fmt= new int[] { 5, 5 };
          file.intField(0, -1);
          file.intField(1, nbValeurs);
          file.writeFields(fmt);
        }
        // Valeurs des r�sultats
        fmt= new int[nbValeurs + 1];
        fmt[0]= 5;
        for (int i= 0; i < nbValeurs; i++) {
          fmt[i + 1]= 12;
        }
        for (int i= 0; i < etapes[ipas].lignes.length; i++) {
          file.intField(0, i + 1);
          for (int j= 0; j < etapes[ipas].lignes[i].valeurs.length; j++) {
            file.doubleField(j + 1, etapes[ipas].lignes[i].valeurs[j]);
          }
          file.writeFields(fmt);
        }
      }
      file.flush();
      file.close();
    } catch (final IOException ex) {
      CDodico.exception(DResultatsReflux.class, ex);
    }
  }
  /**
   * Lecture des r�sultats sur fichiers.
   *
   * @param _fichier Nom du fichier r�sultats .sol sans extension
   * @param _nbNoeuds Nombre de noeuds du fichier.
   * @return resultats lus.
   * @see #litResultatsReflux(File, int)
   * @exception FileNotFoundException Fichier non trouv�.
   * @exception IOException           Erreur de lecture sur le fichier.
   */
  public static SResultatsReflux litSurFichiers(final File _fichier, final int _nbNoeuds)
    throws IOException {
    return litResultatsReflux(_fichier, _nbNoeuds);
  }
  /**
   * Lit les resultats du calcul par Reflux. Cette m�thode peut �tre appel�e
   * aussi bien par le serveur de calcul que par l'application cliente (pour
   * lire des fichiers r�sultats locaux).
   *
   * @param _file  Le fichier de solutions contenant les r�sultats. Le fichier
   *               peut avoir n'importe quelle extension.
   * @param _nbNds Nombre de noeuds sur le fichier des solutions. Ce nombre
   *               devrait correspondre au nombre de noeuds du maillage du
   *               projet.
   *
   * @return La structure contenant les valeurs pour tous les noeuds � toutes
   *         les �tapes.
   * @exception FileNotFoundException Fichier non trouv�.
   * @exception IOException           Erreur de lecture sur le fichier.
   */
  public static SResultatsReflux litResultatsReflux(final File _file, final int _nbNds)
    throws IOException {
    final List vsteps= new Vector();
    final ResultsReader reader= new ResultsReader();
    readSteps(_file, _nbNds, vsteps, reader);
    // Fin du fichier => Stockage des �tapes dans la structure ad�quate
    final SResultatsReflux resultats=
      new SResultatsReflux(new SResultatsEtapeReflux[vsteps.size()]);
    vsteps.toArray(resultats.etapes);
    return resultats;
  }
  /**
   * Read step synchronously or asynchronously.
   *
   * @param _file   Le fichier de solutions contenant les r�sultats. Le fichier
   *                peut avoir n'importe quelle extension.
   * @param _nbNds  Nombre de noeuds sur le fichier des solutions. Ce nombre
   *                devrait correspondre au nombre de noeuds du maillage du
   *                projet.
   * @param _vsteps Vector of steps to fill.
   */
  static void readSteps(
    final File _file,
    final int _nbNds,
    final List _vsteps,
    final ResultsReader _reader)
    throws IOException {
    // Fichier de lecture
    FortranReader file= null;
    //      // Stockage des �tapes
    //      Vector etapes = new Vector();
    _vsteps.clear();
    try {
      // Ouverture du fichier des solutions
      file= new FortranReader(new FileReader(_file));
      // Valeur du pas de temps lu sur fichier
      //      double temps;
      // Nombre de valeurs � lire sur le fichier
      int nbValeurs= 0;
      // Formats
      int[] fmt;
      // Lecture des �tapes (-999)
      for (;;) {
        try {
          while (!file.ready() && _reader.waitForResults_) {
            try {
              Thread.sleep(100);
            } catch (final InterruptedException e) {}
          }
          fmt= new int[] { 5 };
          file.readFields(fmt);
        }
        // Fin du fichier => Plus d'�tapes
        catch (final EOFException ex) {
          break;
        }
        if (file.intField(0) != -999) {
          throw new IOException();
        }
        // Cr�ation d'une nouvelle �tape
        final SResultatsEtapeReflux etape= new SResultatsEtapeReflux();
        etape.lignes= new SResultatsLigneReflux[_nbNds];
        // (' ===== PAS NUMERO :',<pas de temps>,' ITERATION NUMERO :',
        //  <numero d'it�ration>,'TEMPS',<valeur de temps>)
        fmt= new int[] { 19, 5, 20, 5, 18, 12 };
        while (!file.ready() && _reader.waitForResults_) {
          try {
            Thread.sleep(100);
          } catch (final InterruptedException e) {}
        }
        file.readFields(fmt);
        DCalculReflux.message(
          "Fichier r�sultats Pas: "
            + file.intField(1)
            + " Iteration: "
            + file.intField(3)
            + " Temps: "
            + file.doubleField(5));
        //        if (file.stringField(4).equals("TEMPS"))
        etape.instant= file.doubleField(5);
        //        else etape.instant=0;
        // Seulement lors de la premi�re �tape
        if (_vsteps.size() < 1) {
          // (-1,<nombre de valeurs>)
          fmt= new int[] { 5, 5 };
          while (!file.ready() && _reader.waitForResults_) {
            try {
              Thread.sleep(100);
            } catch (final InterruptedException e) {}
          }
          file.readFields(fmt);
          if (file.intField(0) != -1) {
            throw new IOException();
          }
          nbValeurs= file.intField(1);
          DCalculReflux.message("Fichier r�sultats Colonnes : " + nbValeurs);
        }
        // Cr�ation des lignes de l'�tape
        for (int i= 0; i < _nbNds; i++) {
          etape.lignes[i]= new SResultatsLigneReflux();
          etape.lignes[i].valeurs= new double[nbValeurs];
        }
        // Lecture des lignes (<numero de noeud>,<valeurs>)
        fmt= new int[nbValeurs + 1];
        fmt[0]= 5;
        for (int i= 1; i < nbValeurs + 1; i++) {
          fmt[i]= 12;
        }
        for (int i= 0; i < _nbNds; i++) {
          while (!file.ready() && _reader.waitForResults_) {
            try {
              Thread.sleep(100);
            } catch (final InterruptedException e) {}
          }
          file.readFields(fmt);
          etape.lignes[i].numeroNoeud= file.intField(0);
          for (int j= 0; j < nbValeurs; j++) {
            etape.lignes[i].valeurs[j]= file.doubleField(j + 1);
          }
        }
        _vsteps.add(etape);
      }
      // Fermeture du fichier
      file.close();
    } catch (final FileNotFoundException _exc) {
      throw new FileNotFoundException("Erreur d'ouverture du fichier " + _file);
    } catch (final IOException e) {
      throw new IOException(
        "Erreur de lecture sur fichier "
          + _file
          + " ligne "
          + (file==null?0:file.getLineNumber()));
    }
  }
  /**
   * A reader for asynchronous read the results. When a new step is written
   * in results file, it is load in vector of steps.
   */
  private static class ResultsReader extends Thread {
    /** Dernier caract�re des extensions pour les fichiers de calcul. */
    private DResultatsReflux results_;
    /**Laisser la visibite en l'etat car ce champ est utilise par
     * une innner classe : optimisation (evite de creer des accesseurs
     * 'synthetise'). 
     */
    boolean waitForResults_;
    /** Nombre de noeuds du domaine calcul� (nombre d'enregistrements pour chaque �tape). */
    private int nbNds_;
    /** Type de probl�me (sert notamment pour l'extension du fichier de r�sultat). */
    private int tpPb_;
    /**
     * @param _res le resultat a modifier
     */
    public ResultsReader(final DResultatsReflux _res) {
      results_= _res;
    }
    /**
     * ne fait rien.
     */
    public ResultsReader() {}
    public void run() {
      waitForResults_= true;
      results_.iteratorFilled_= false;
      final String racine= results_.params_.getRacine();
      final String path=
        System.getProperty("user.dir")
          + File.separator
          + "serveurs"
          + File.separator
          + "reflux"
          + File.separator;
      try {
        // Read informations in file .dat
        litData(new File(path + racine + ".dat"));
      } catch (final IOException _exc) {
        System.out.println(
          "Erreur de lecture du fichier " + new File(path + racine + ".dat"));
      }
      // Wait for result file exist.
      final File fsol= new File(path + racine + ".so" + DParametresReflux.EXTENSIONS[tpPb_]);
      while (!fsol.exists() && waitForResults_) {
        try {
          sleep(100);
        } catch (final InterruptedException e) {}
      }
      try {
        readSteps(fsol, nbNds_, results_.vsteps_, this);
      } catch (final IOException _exc) {
        System.out.println(_exc.getMessage());
      }
      // Notify the is no more results to come
      results_.iteratorFilled_= true;
    }
    /**
     * Lit les donn�es non stock�es sur les fichiers r�sultats mais n�cessaires
     * � la lecture des fichiers r�sultats.
     * @param _file Le nom du fichier .dat
     */
    private void litData(final File _file) throws IOException {
      FileInputStream file= null;
      // Ouverture du fichier
      file= new FileInputStream(_file);
      // Propri�t�s
      final Properties prs= new Properties();
      prs.load(file);
      file.close();
      // Remplissage des variables
      nbNds_= Integer.parseInt(prs.getProperty("nbNoeuds", "0"));
      tpPb_= Integer.parseInt(prs.getProperty("tpProbleme", "0"));
    }
    /**
     * Ask the reader to stop when analysis is finished.
     */
    public void stopNeeded() {
      waitForResults_= false;
    }
  }
}
