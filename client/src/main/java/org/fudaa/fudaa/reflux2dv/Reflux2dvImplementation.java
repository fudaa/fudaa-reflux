/*
 * Cr�� le 1 f�vr. 2005
 *
 * TODO Pour changer le mod�le de ce fichier g�n�r�, allez � :
 * Fen�tre - Pr�f�rences - Java - Style de code - Mod�les de code
 */
package org.fudaa.fudaa.reflux2dv;

import java.util.Map;

import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPreferences;

import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.reflux.RefluxResource;

/**
 * @author clavreul TODO Pour changer le mod�le de ce commentaire de type g�n�r�, allez � : Fen�tre - Pr�f�rences - Java
 *         - Style de code - Mod�les de code
 */
public class Reflux2dvImplementation extends FudaaImplementation {

  protected static final BuInformationsSoftware isSoft_ = new BuInformationsSoftware();
  static {
    isSoft_.name = "Reflux2dv";
    isSoft_.version = "0.01";
    isSoft_.date = "28-aout-2005";
    isSoft_.rights = "Tous droits r�serv�s. CETMEF (c)1999,2000,2001";
    isSoft_.contact = "patrick.gomi@equipement.gouv.fr";
    isSoft_.license = "GPL2";
    isSoft_.languages = "fr,en";
    isSoft_.logo = RefluxResource.REFLUX.getIcon("vag_logo");
    isSoft_.banner = RefluxResource.REFLUX.getIcon("vag_banner");
    isSoft_.http = "http://www.utc.fr/fudaa/vag/";
    isSoft_.update = "http://www.fudaa.org/distrib/deltas/vag/";
    isSoft_.man = FudaaLib.LOCAL_MAN + "vag/";
    isSoft_.authors = new String[] { "Axel von Arnim" };
    isSoft_.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa", "Christophe Delhorbe", "Mickael Rubens" };
    isSoft_.documentors = new String[] { "Patrick Gomi" };
    isSoft_.testers = new String[] { "Patrick Gomi" };
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.commun.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    // TODO Raccord de m�thode auto-g�n�r�
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.commun.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    // TODO Raccord de m�thode auto-g�n�r�
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.commun.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(Map _r) {
  // TODO Raccord de m�thode auto-g�n�r�

  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.commun.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
  // TODO Raccord de m�thode auto-g�n�r�

  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.commun.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    // TODO Raccord de m�thode auto-g�n�r�
    return null;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isSoft_;
  }

}
