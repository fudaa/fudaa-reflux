/*
 * @file         PRBoucleTemps.java
 * @creation     2000-03-02
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;

import java.util.Vector;

/**
 * Une classe d�finissant un groupe d'objets.
 *
 * @version      $Revision: 1.2 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxGroupe {
  private int num_;
  private Object[] objs_;

  /**
   * Cr�ation d'un groupe avec un num�ro donn� unique.
   */
  public RefluxGroupe(Object[] _objs) {
    objs_=_objs;
  }

  /**
   * Retourne les objets du groupe.
   */
  public Object[] getObjects() {
    return objs_;
  }

  /**
   * Affecte les objets au groupe.
   */
  public void setObjects(Object[] _objs) {
    objs_=_objs;
  }

  /**
   * Affecte le num�ro au groupe.
   */
  public void setNum(int i) { num_=i; }

  /**
   * Retourne le num�ro du groupe.
   */
  public int getNum() { return num_; }

  /**
   * Retourne un num�ro de groupe non encore affect�. (A partir de 1).
   */
  public static int getLastNumber(Vector _grps) {
    int num=0;
    for (int i=0; i<_grps.size(); i++) {
      num=Math.max(((RefluxGroupe)_grps.get(i)).num_,num);
    }
    return num+1;
  }
}
