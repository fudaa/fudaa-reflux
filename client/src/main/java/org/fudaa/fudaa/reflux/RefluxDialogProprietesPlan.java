/*
 * @file         RefondeDialogDomainePoreux.java
 * @creation     2003-02-10
 * @modification $Date: 2007-01-19 13:14:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;

import org.fudaa.fudaa.commun.impl.FudaaDialog;

/**
 * Affiche/modifie les propri�t�s d'un domaine poreux.
 *
 * @version      $Id: RefluxDialogProprietesPlan.java,v 1.4 2007-01-19 13:14:36 deniger Exp $
 * @author       Bertrand Marchand
 */
public class RefluxDialogProprietesPlan extends FudaaDialog {
  TitledBorder bdpnValeurAval;
  /** Domaine poreux. */
  private RefluxPlan plan_;
  /** Projet. */
  private PRProjet prj_;
  /** Retourne le bouton activ�. */
  public JButton reponse;
  private JPanel pnGroupe = new JPanel();
  private JLabel lbGroupe = new JLabel();
  private JComboBox coGroupe = new JComboBox();
  JPanel pnExtremites = new JPanel();
  JTextField tfValeurAmont = new JTextField();
  JRadioButton rbMultiplanAmont = new JRadioButton();
  JPanel pnValeurAmont = new JPanel();
  JLabel lbValeurAmont = new JLabel();
  JComboBox coMultiplanAmont = new JComboBox();
  BuGridLayout lyExtremites = new BuGridLayout();
  TitledBorder bdpnValeurAmont;
  JRadioButton rbFixeAmont = new JRadioButton();
  JRadioButton rbFixeAval = new JRadioButton();
  JLabel lbValeurAval = new JLabel();
  JComboBox coMultiplanAval = new JComboBox();
  JRadioButton rbMultiplanAval = new JRadioButton();
  JTextField tfValeurAval = new JTextField();
  JPanel pnValeurAval = new JPanel();
  ButtonGroup bgAmont = new ButtonGroup();
  ButtonGroup bgAval = new ButtonGroup();
  //  private JTextField[] tfXPlanPt=new JTextField[3];
  //  private JTextField[] tfYPlanPt=new JTextField[3];
  //  private JTextField[] tfZPlanPt=new JTextField[3];
  //  private JCheckBox[] cbZPlanPt=new JCheckBox[3];
//  RefondePnPointPlan[] pnPlanPt= new RefondePnPointPlan[3];
//  RefondePnPointPlan pnPlanPt1= new RefondePnPointPlan();
//  RefondePnPointPlan pnPlanPt2= new RefondePnPointPlan();
//  RefondePnPointPlan pnPlanPt3= new RefondePnPointPlan();
  /**
   * Cr�ation d'un dialogue sans parent.
   */
  public RefluxDialogProprietesPlan() {
    this(null);
  }
  /**
   * Cr�ation d'un dialogue avec parent.
   */
  public RefluxDialogProprietesPlan(Frame _parent) {
    super(_parent);
    try {
      jbInit();
      this.pack();
    } catch (Exception e) {
      e.printStackTrace();
    }
//    pnPlanPt[0]= pnPlanPt1;
//    pnPlanPt[1]= pnPlanPt2;
//    pnPlanPt[2]= pnPlanPt3;
    //    tfXPlanPt[0]=tfXPlanPt1;
    //    tfXPlanPt[1]=tfXPlanPt2;
    //    tfXPlanPt[2]=tfXPlanPt3;
    //
    //    tfYPlanPt[0]=tfYPlanPt1;
    //    tfYPlanPt[1]=tfYPlanPt2;
    //    tfYPlanPt[2]=tfYPlanPt3;
    //
    //    tfZPlanPt[0]=tfZPlanPt1;
    //    tfZPlanPt[1]=tfZPlanPt2;
    //    tfZPlanPt[2]=tfZPlanPt3;
    //
    //    cbZPlanPt[0]=cbZPlanPt1;
    //    cbZPlanPt[1]=cbZPlanPt2;
    //    cbZPlanPt[2]=cbZPlanPt3;
  }
  private void jbInit() throws Exception {
    bdpnValeurAmont = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),"Ligne extr�mit� amont n�");
    pnExtremites.setLayout(lyExtremites);
    pnValeurAmont.setBorder(bdpnValeurAmont);
    lyExtremites.setColumns(1);
    lyExtremites.setFlowMode(false);
    lyExtremites.setHfilled(true);
    lyExtremites.setVgap(5);
    rbFixeAmont.setText("Fix�");
    rbFixeAmont.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        rbAmont_itemStateChanged(e);
      }
    });
    rbFixeAval.setText("Fix�");
    rbFixeAval.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        rbAval_itemStateChanged(e);
      }
    });
    lbValeurAval.setText("Niveau d\'eau :");
    coMultiplanAval.setPreferredSize(new Dimension(70, 21));
    rbMultiplanAval.setText("Issu du multiplan");
    rbMultiplanAval.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
    rbMultiplanAval.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        rbAval_itemStateChanged(e);
      }
    });
    bdpnValeurAval= new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),"Ligne extr�mit� aval n�");
    tfValeurAval.setPreferredSize(new Dimension(70, 21));
    tfValeurAval.setText("");
    pnValeurAval.setBorder(bdpnValeurAval);
    pnExtremites.add(pnValeurAmont, null);
    this.setTitle("Propri�t�s du multiplan");
//    pnPlan.add(pnPlanPt1, null);
//    pnPlan.add(pnPlanPt2, null);
//    pnPlan.add(pnPlanPt3, null);
//    pnPlanPt1.setTitle("Premier point");
//    pnPlanPt2.setTitle("Deuxi�me point");
//    pnPlanPt3.setTitle("Troisi�me point");
    lbGroupe.setText("Groupe d'�l�ments associ� :");
    coGroupe.setPreferredSize(new Dimension(70, 21));
    tfValeurAmont.setText("");
    tfValeurAmont.setPreferredSize(new Dimension(70, 21));
    rbMultiplanAmont.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        rbAmont_itemStateChanged(e);
      }
    });
    rbMultiplanAmont.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
    rbMultiplanAmont.setText("Issu du multiplan");
    lbValeurAmont.setText("Niveau d\'eau :");
    coMultiplanAmont.setPreferredSize(new Dimension(70, 21));
    this.getContentPane().add(pnGroupe,  BorderLayout.NORTH);
    pnGroupe.add(lbGroupe, null);
    pnGroupe.add(coGroupe, null);
    this.getContentPane().add(pnExtremites, BorderLayout.CENTER);
    pnValeurAmont.add(lbValeurAmont, null);
    pnValeurAmont.add(rbFixeAmont, null);
    pnValeurAmont.add(tfValeurAmont, null);
    pnValeurAmont.add(rbMultiplanAmont, null);
    pnValeurAmont.add(rbMultiplanAmont, null);
    pnValeurAmont.add(coMultiplanAmont, null);
    pnValeurAval.add(lbValeurAval, null);
    pnValeurAval.add(rbFixeAval, null);
    pnValeurAval.add(tfValeurAval, null);
    pnValeurAval.add(rbMultiplanAval, null);
    pnValeurAval.add(rbMultiplanAval, null);
    pnValeurAval.add(coMultiplanAval, null);
    pnExtremites.add(pnValeurAval, null);
    bgAmont.add(rbFixeAmont);
    bgAmont.add(rbMultiplanAmont);
    bgAval.add(rbFixeAval);
    bgAval.add(rbMultiplanAval);
  }
  //  private void cbZPlanPt1_itemStateChanged(ItemEvent e) {
  //    tfZPlanPt1.setEnabled(!cbZPlanPt1.isSelected());
  //  }
  //
  //  private void cbZPlanPt2_itemStateChanged(ItemEvent e) {
  //    tfZPlanPt2.setEnabled(!cbZPlanPt2.isSelected());
  //  }
  //
  //  private void cbZPlanPt3_itemStateChanged(ItemEvent e) {
  //    tfZPlanPt3.setEnabled(!cbZPlanPt3.isSelected());
  //  }
  protected void btOkActionPerformed(ActionEvent _evt) {
    if (majBDD()) {
      reponse= (JButton)_evt.getSource();
      super.btOkActionPerformed(_evt);
    }
  }
  /**
   * Mise � jour de la base de donn�es.
   *
   * @return <i>true</i> : La mise � jour s'est bien pass�e.
   */
  private boolean majBDD() {
    Vector pls=prj_.getPlans();
    Vector grps=prj_.getGroupes();

    boolean amontFixe;
    double valAmont=0;
    RefluxPlan plAmont=null;
    boolean avalFixe;
    double valAval=0;
    RefluxPlan plAval=null;
    RefluxGroupe grp=null;

    try {
      if (coGroupe.getSelectedIndex()!=0) {
        int nmGp=Integer.parseInt((String)coGroupe.getSelectedItem());
        for (int i=0; i<grps.size(); i++) {
          RefluxGroupe gp=(RefluxGroupe)grps.get(i);
          if (gp.getNum()==nmGp) {
            grp=gp;
            break;
          }
        }
      }

      if (rbFixeAmont.isSelected()) {
        amontFixe=true;
        valAmont=Double.parseDouble(tfValeurAmont.getText());
      }
      else {
        amontFixe=false;
        if (coMultiplanAmont.getSelectedIndex()!=0) {
          int nmPl=Integer.parseInt((String)coMultiplanAmont.getSelectedItem());
          for (int i=0; i<pls.size(); i++) {
            RefluxPlan pl=(RefluxPlan)pls.get(i);
            if (pl.getNum()==nmPl) {
              plAmont=pl;
              break;
            }
          }
        }
      }

      if (rbFixeAval.isSelected()) {
        avalFixe=true;
        valAval=Double.parseDouble(tfValeurAval.getText());
      }
      else {
        avalFixe=false;
        if (coMultiplanAval.getSelectedIndex()!=0) {
          int nmPl=Integer.parseInt((String)coMultiplanAval.getSelectedItem());
          for (int i=0; i<pls.size(); i++) {
            RefluxPlan pl=(RefluxPlan)pls.get(i);
            if (pl.getNum()==nmPl) {
              plAval=pl;
              break;
            }
          }
        }
      }

      plan_.setGroupe(grp);
      plan_.setHAutomatiqueAmont(!amontFixe);
      plan_.setHAmont(valAmont);
      plan_.setPlanAmont(plAmont);
      plan_.setHAutomatiqueAval(!avalFixe);
      plan_.setHAval(valAval);
      plan_.setPlanAval(plAval);
    }
    catch (NumberFormatException _exc) {
      new BuDialogError(
        null,
        RefluxImplementation.informationsSoftware(),
        "Un des param�tres n'a pas un format valide")
        .activate();
      return false;
    }
    return true;
  }
  /**
   * Affectation du plan � modifier.
   * @param _pl Plan.
   */
  public void setPlan(RefluxPlan _pl) {
    plan_= _pl;
  }
  /**
   * Retourne le domaine poreux affich�.
   * @return Le domaine poreux �ventuellement modifi�.
   */
//  public RefondeDomainePoreux getDomainePoreux() {
//    return domaine_;
//  }
  /**
   * Affectation de la g�om�trie (necessaire pour savoir si les points du
   * domaine poreux sont hors g�om�trie ou non.
   * @param _prj Le projet refonde.
   */
  public void setProjet(PRProjet _prj) {
    prj_= _prj;
  }
  /**
   * Visualisation de la fenetre. Surcharg�.
   */
  public void show() {
    Vector grps=prj_.getGroupes();
    coGroupe.removeAllItems();
    coGroupe.addItem("");
    for (int i=0; i<grps.size(); i++) {
      coGroupe.addItem(""+((RefluxGroupe)grps.get(i)).getNum());
    }
    Vector pls=prj_.getPlans();
    coMultiplanAmont.removeAllItems();
    coMultiplanAmont.addItem("");
    for (int i=0; i<pls.size(); i++) {
      RefluxPlan pl=(RefluxPlan)pls.get(i);
      if (pl!=plan_) coMultiplanAmont.addItem(""+pl.getNum());
    }
    coMultiplanAval.removeAllItems();
    coMultiplanAval.addItem("");
    for (int i=0; i<pls.size(); i++) {
      RefluxPlan pl=(RefluxPlan)pls.get(i);
      if (pl!=plan_) coMultiplanAval.addItem(""+pl.getNum());
    }

    if (plan_!=null) {
      if (plan_.getGroupe()!=null) coGroupe.setSelectedItem(""+plan_.getGroupe().getNum());
      setTitle("Propri�t�s du multiplan n� "+plan_.getNum());

      bdpnValeurAmont.setTitle("Ligne extr�mit� amont n� "+plan_.getSegments()[0].getNum());
      if (!plan_.isHAutomatiqueAmont()) {
        tfValeurAmont.setText(""+plan_.getHAmont());
        rbFixeAmont.setSelected(true);
      }
      else {
        coMultiplanAmont.setSelectedIndex(0);
        if (plan_.getPlanAmont()!=null) coMultiplanAmont.setSelectedItem(""+plan_.getPlanAmont().getNum());
        rbMultiplanAmont.setSelected(true);
      }

      bdpnValeurAval.setTitle("Ligne extr�mit� aval n� "+plan_.getSegments()[plan_.getSegments().length-1].getNum());
      if (!plan_.isHAutomatiqueAval()) {
        tfValeurAval.setText(""+plan_.getHAval());
        rbFixeAval.setSelected(true);
      }
      else {
        coMultiplanAval.setSelectedIndex(0);
        if (plan_.getPlanAval()!=null) coMultiplanAval.setSelectedItem(""+plan_.getPlanAval().getNum());
        rbMultiplanAval.setSelected(true);
      }
    }
//    if (domaine_ != null) {
//      GrPoint[] pts= domaine_.getPoints();
//      GrPoint[] ptsPlan= domaine_.getPointsPlan();
//      // Propri�t�s
//      tfPorosite.setText("" + domaine_.porosite_);
//      tfFrottement.setText("" + domaine_.coefFrottement_);
//      tfPermeabilite.setText("" + domaine_.permeabilite_);
//      tfCm.setText("" + domaine_.coefMasse_);
//      // Limites
//      tfXmin.setText("" + pts[0].x);
//      tfYmin.setText("" + pts[0].y);
//      tfXmax.setText("" + pts[1].x);
//      tfYmax.setText("" + pts[1].y);
//      // Points du plan
//      for (int i= 0; i < 3; i++) {
//        pnPlanPt[i].setProjet(prj_);
//        pnPlanPt[i].setPoint(ptsPlan[i]);
//        pnPlanPt[i].setZAutomatique(domaine_.isZAutomatique(i));
//      }
//    }
    super.show();
  }

  void rbAmont_itemStateChanged(ItemEvent e) {
    if (e.getStateChange()==ItemEvent.DESELECTED) return;
    tfValeurAmont.setEnabled(rbFixeAmont.isSelected());
    coMultiplanAmont.setEnabled(rbMultiplanAmont.isSelected());
  }

  void rbAval_itemStateChanged(ItemEvent e) {
    if (e.getStateChange()==ItemEvent.DESELECTED) return;
    tfValeurAval.setEnabled(rbFixeAval.isSelected());
    coMultiplanAval.setEnabled(rbMultiplanAval.isSelected());
  }


  /**
   * Pour test de la boite de dialogue.
   */
/*  public static void main(String[] _args) {
    try {
      UIManager.setLookAndFeel(
        "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
    } catch (Exception _exc) {}
    RefondeDomainePoreux dm= new RefondeDomainePoreux();
    GrPoint[] pts= new GrPoint[2];
    pts[0]= new GrPoint(0, 0, 0);
    pts[1]= new GrPoint(10, 25, 0);
    dm.setPoints(pts);
    dm.setPointsPlan(
      new GrPoint[] {
        new GrPoint(1, 2, 3),
        new GrPoint(4, 5, 6),
        new GrPoint(7, 8, 9)});
    dm.setZAutomatique(false, 0);
    dm.setZAutomatique(true, 1);
    RefondeDialogProprietesPoreux di= new RefondeDialogProprietesPoreux();
    di.setDomainePoreux(dm);
    di.show();
    System.exit(0);
  }*/
}
