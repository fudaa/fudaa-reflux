/*
 * @file         PRBoucleTemps.java
 * @creation     2000-03-02
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;

import java.util.Vector;

import org.fudaa.ctulu.gis.CtuluLibGeometrie;

/**
 * Une classe d�finissant un plan.
 *
 * @version      $Revision: 1.4 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxPlan {
  private int num_;
  private RefluxSegment[] sgs_;
  private double[][] pentesXY_;
  private RefluxGroupe gp_;
  private RefluxPlan planAutoAval_;  // Le plan pour le calcul de hauteur automatique segment aval
  private RefluxPlan planAutoAmont_; // Le plan pour le calcul de hauteur automatique segment amont

  /**
   * Cr�ation d'un plan avec un num�ro donn� unique.
   */
  public RefluxPlan(RefluxSegment[] _sgs) {
    setSegments(_sgs);
  }

  /**
   * Retourne les segments du plan.
   */
  public RefluxSegment[] getSegments() {
    return sgs_;
  }

  /**
   * Affecte les objets au plan. Les pentes sont � recalculer.
   */
  public void setSegments(RefluxSegment[] _sgs) {
    sgs_=_sgs;
    pentesXY_=null;
  }

  /**
   * Retourne le segment amont.
   */
  public RefluxSegment getSegmentAmont() {
    return sgs_==null ? null:sgs_[0];
  }

  /**
   * Retourne le segment aval.
   */
  public RefluxSegment getSegmentAval() {
    return sgs_==null ? null:sgs_[sgs_.length-1];
  }

  /**
   * Affecte le groupe d'�l�ments au plan.
   */
  public void setGroupe(RefluxGroupe _gp) {
    gp_=_gp;
  }

  /**
   * Retourne le groupe d'�l�ments du plan.
   */
  public RefluxGroupe getGroupe() {
    return gp_;
  }

  /**
   * D�fini l'automaticit� du calcul du h amont.
   */
  public void setHAutomatiqueAmont(boolean _b) {
    sgs_[0].setHAutomatique(_b);
  }

  /**
   * Est ce que le h amont est calcul� automatiquement ?
   */
  public boolean isHAutomatiqueAmont() { return sgs_[0].isHAutomatique(); }

  /**
   * D�fini la hauteur en h amont.
   */
  public void setHAmont(double _h) {
    sgs_[0].setHauteur(_h);
  }

  /**
   * Retourne la hauteur en h amont.
   */
  public double getHAmont() {
    return sgs_[0].getHauteur();
  }

  /**
   * D�finit le plan de calcul automatique amont. Peut �tre null.
   */
  public void setPlanAmont(RefluxPlan _pl) {
    planAutoAmont_=_pl;
  }

  /**
   * Retourne le plan de calcul automatique amont.
   */
  public RefluxPlan getPlanAmont() {
    return planAutoAmont_;
  }

  /**
   * D�fini l'automaticit� du calcul du h aval.
   */
  public void setHAutomatiqueAval(boolean _b) {
    sgs_[sgs_.length-1].setHAutomatique(_b);
  }

  /**
   * Est ce que le h aval est calcul� automatiquement ?
   */
  public boolean isHAutomatiqueAval() { return sgs_[sgs_.length-1].isHAutomatique(); }

  /**
   * D�fini la hauteur en h amont.
   */
  public void setHAval(double _h) {
    sgs_[sgs_.length-1].setHauteur(_h);
  }

  /**
   * Retourne la hauteur en h amont.
   */
  public double getHAval() {
    return sgs_[sgs_.length-1].getHauteur();
  }

  /**
   * D�finit le plan de calcul automatique aval. Peut �tre null.
   */
  public void setPlanAval(RefluxPlan _pl) {
    planAutoAval_=_pl;
  }

  /**
   * Retourne le plan de calcul automatique aval.
   */
  public RefluxPlan getPlanAval() {
    return planAutoAval_;
  }

  /**
   * Affecte le num�ro au plan.
   */
  public void setNum(int i) { num_=i; }

  /**
   * Retourne le num�ro du plan.
   */
  public int getNum() { return num_; }

  /**
   * Retourne un num�ro de plan non encore affect�. (A partir de 1).
   */
  public static int getLastNumber(Vector _pls) {
    int num=0;
    for (int i=0; i<_pls.size(); i++) {
      num=Math.max(((RefluxPlan)_pls.get(i)).num_,num);
    }
    return num+1;
  }

  /**
   * Tri sur les plans pour qu'ils soient toujours dans un ordre de d�pendance.
   * Ceci pour un calcul des h extr�mit�s.
   * Ne teste pas la validit� des plans (controle que le groupe est bien
   * existant, etc).
   *
   * @return Un vecteur de plans tri�s pour qu'un plan d'index i ne puisse
   *         d�pendre que d'un plan d'index &lt;i.
   * @throws IllegalArgumentException En cas de plan non d�fini correctement ou de
   *         d�pendances en boucle.
   */
  public static Vector triPlans(Vector _pls) {
    Vector pls=(Vector)_pls.clone();
    Vector plsTri=new Vector();
    boolean okAmont;
    boolean okAval;

    while (pls.size()!=0) {
      boolean okTri=false;
      for (int i=pls.size()-1; i>=0; i--) {
        RefluxPlan pl=(RefluxPlan)pls.get(i);
        okAmont=true;
        if (pl.isHAutomatiqueAmont()) {
          if (pl.getPlanAmont()==null) {
            throw new IllegalArgumentException("Le multiplan n� "+pl.getNum()+" n'est pas totalement d�fini.");
//            System.out.println("Le multiplan n� "+pl.getNum()+" n'est pas totalement d�fini.");
//            return new Vector();
          }
          /*else */okAmont=plsTri.contains(pl.getPlanAmont());
        }
        okAval=true;
        if (pl.isHAutomatiqueAval()) {
          if (pl.getPlanAval()==null) {
            throw new IllegalArgumentException("Le multiplan n� "+pl.getNum()+" n'est pas totalement d�fini.");
//            System.out.println("Le multiplan n� "+pl.getNum()+" n'est pas totalement d�fini.");
//            return new Vector();
          }
          /*else */okAval=plsTri.contains(pl.getPlanAval());
        }
        if (okAmont && okAval) {
          pls.remove(i);
          plsTri.add(pl);
          okTri=true;
        }
      }
      if (!okTri) {
        String nums="";
        for (int i=0; i<pls.size(); i++) nums+=((RefluxPlan)pls.get(i)).getNum()+" ";
        throw new IllegalArgumentException("Les multiplans ( "+nums+") ont une d�pendance en boucle");
//        System.out.println("Les multiplans ( "+nums+") ont une d�pendance en boucle");
//        return new Vector();
      }
    }
    return plsTri;
  }

  /**
   * Calcul les pentes suivant X et Y en fonction d'un indice de segment et du
   * point issu par le milieu du segment suivant. Chaque segment doit avoir un
   * h affect�.
   */
  private void initPentesXY() {
    pentesXY_=new double[sgs_.length-1][2];

    for (int i=0; i<sgs_.length-1; i++) {
      double[][] ptsPlan=new double[3][3];
      ptsPlan[0][0]=sgs_[i].o[0];
      ptsPlan[0][1]=sgs_[i].o[1];
      ptsPlan[0][2]=sgs_[i].getHauteur();
      ptsPlan[1][0]=sgs_[i].e[0];
      ptsPlan[1][1]=sgs_[i].e[1];
      ptsPlan[1][2]=sgs_[i].getHauteur();
      ptsPlan[2][0]=(sgs_[i+1].e[0]+sgs_[i+1].e[0])/2;
      ptsPlan[2][1]=(sgs_[i+1].e[1]+sgs_[i+1].e[1])/2;
      ptsPlan[2][2]=sgs_[i+1].getHauteur();

      pentesXY_[i][0]=
        -((ptsPlan[1][1] - ptsPlan[0][1]) * (ptsPlan[2][2] - ptsPlan[0][2])
         -(ptsPlan[1][2] - ptsPlan[0][2]) * (ptsPlan[2][1] - ptsPlan[0][1]))
        /((ptsPlan[1][0] - ptsPlan[0][0]) * (ptsPlan[2][1] - ptsPlan[0][1])
         -(ptsPlan[1][1] - ptsPlan[0][1]) * (ptsPlan[2][0] - ptsPlan[0][0]));
      pentesXY_[i][1]=
         ((ptsPlan[1][0] - ptsPlan[0][0]) * (ptsPlan[2][2] - ptsPlan[0][2])
         -(ptsPlan[1][2] - ptsPlan[0][2]) * (ptsPlan[2][0] - ptsPlan[0][0]))
        /((ptsPlan[1][0] - ptsPlan[0][0]) * (ptsPlan[2][1] - ptsPlan[0][1])
         -(ptsPlan[1][1] - ptsPlan[0][1]) * (ptsPlan[2][0] - ptsPlan[0][0]));
    }
  }

  /**
   * Calcul de vitesses vx, vy approximatives pour un noeud x,y � ht total donn�
   * pour le segment donn� (plan auquel il appartient dans le multiplan).
   * (On prend un chezy=50)
   * @param _ht Hauteurs totale au point.
   * @param _x Coordonn�e x du point
   * @param _y Coordonn�e y du point
   * @param _trs Le noeud est ou non de transit.
   * @return r[0] Vitesse suivant x, r[1] Vitesse suivant y
   */
  public double[] calculeVitesses(int _iseg, double _ht, double _x, double _y,
                                  boolean _trs) {

    double[] vxy;
    double sss;
    double[][] pentesXY=getPentesXY(); // Pour etre s�r de l'initialisation.

    vxy= new double[2];
    double sx=pentesXY[_iseg][0];
    double sy=pentesXY[_iseg][1];
    sss= Math.sqrt(sx*sx + sy*sy);
    // Si la pente sur X et Y est nulle => Vitesses nulles
    if (sss < 1.e-10) {
      vxy[0]= 0;
      vxy[1]= 0;
    }
    // Si la pente est differente de 0
    else {
      // Noeud de transit ou noeud sec => vitesses nulles
      if (_trs || _ht <= 0.) {
        vxy[0]= 0;
        vxy[1]= 0;
      }
      // Noeud mouille
      else {
        vxy[0]= -sx * Math.sqrt(_ht / sss) * 50.;
        vxy[1]= -sy * Math.sqrt(_ht / sss) * 50.;
      }
    }
    return vxy;
  }

  /**
   * Recherche de l'indice du segment dans le vecteur des segments contenant le
   * point X,Y.
   * @return -1 : Pas de segment trouv�. Il y en a toujours 1 finallement.
   */
  public int getIndiceSegment(double _x, double _y) {
    double[] o2=sgs_[0].o;
    double[] e2=sgs_[0].e;
    for (int i=1; i<sgs_.length; i++) {
      double[] o1=o2;
      double[] e1=e2;
      o2=sgs_[i].o;
      e2=sgs_[i].e;
      double ue1o1=e1[0]-o1[0];
      double ve1o1=e1[1]-o1[1];
      double ue2o1=e2[0]-o1[0];
      double ve2o1=e2[1]-o1[1];
      double uo2o1=o2[0]-o1[0];
      double vo2o1=o2[1]-o1[1];
      double ue2o2=e2[0]-o2[0];
      double ve2o2=e2[1]-o2[1];
      double upo1=_x-o1[0];
      double vpo1=_y-o1[1];
      double upo2=_x-o2[0];
      double vpo2=_y-o2[1];
      if ((ue1o1*ve2o1-ve1o1*ue2o1)*(ue2o1*vo2o1-ve2o1*uo2o1)>=0) { // Lignes dans le m�me sens
        if ((ue1o1*vpo1-ve1o1*upo1)*(ue2o2*vpo2-ve2o2*upo2)<0) return i-1;
      }
      else { // lignes invers�es
        if ((ue1o1*vpo1-ve1o1*upo1)*(ue2o2*vpo2-ve2o2*upo2)>=0) return i-1;
      }
    }

    // On suppose alors que la distance au segment min ou max fait foi.
    int iend=sgs_.length-1;
    if (CtuluLibGeometrie.distanceFromSegment(sgs_[0].o[0],sgs_[0].o[1],sgs_[0].e[0],sgs_[0].e[1],_x,_y)<
        CtuluLibGeometrie.distanceFromSegment(sgs_[iend].o[0],sgs_[iend].o[1],sgs_[iend].e[0],sgs_[iend].e[1],_x,_y))
      return 0;
    /*else */return iend-1;
  }

  /**
   * Retourne les pentes suivant X et Y respectives pour chacun des plans du
   * multiplan et dans l'ordre.
   */
  public double[][] getPentesXY() {
    if (pentesXY_==null) initPentesXY();
    return pentesXY_;
  }

  /**
   * Retourne le h pour un X,Y fix�. On ne connait pas le plan d'appartenance.
   * @param _x double
   * @param _y double
   * @return double
   */
  public double calculeHauteur(double _x, double _y) {
    int iseg=getIndiceSegment(_x,_y);
//    if (iseg==-1) System.out.println("Ce cas ne devrait pas se produire");
    return calculeHauteur(iseg,_x,_y);
  }

  /**
   * Retourne le h pour un X,Y fix� dans le segment d'indice donn�.
   */
  public double calculeHauteur(int _iseg, double _x, double _y) {
    double[][] pentesXY=getPentesXY(); // Pour etre s�r de l'initialisation.

    double h;
    h=pentesXY[_iseg][0]*(_x-sgs_[_iseg].o[0])+
      pentesXY[_iseg][1]*(_y-sgs_[_iseg].o[1])+
      sgs_[_iseg].getHauteur();

//    System.out.println("h : "+h+" calcul� depuis segments n� "+sgs_[_iseg].getNum()+" "+sgs_[_iseg+1].getNum());
    return h;
  }

  /**
   * Calcule les h interm�diaires non fix�s pour un plan dont les extr�mit�s sont
   * fix�es ou calcul�es. Ceci remet les pentes � null.
   */
  public void calculeHIntermediaires() {
    double sentre2fixe=0;
    int ifixe1=0;
    double x2m=(sgs_[ifixe1].o[0]+sgs_[ifixe1].e[0])/2;
    double y2m=(sgs_[ifixe1].o[1]+sgs_[ifixe1].e[1])/2;

    for (int i=1; i<sgs_.length; i++) {
      // Calcul du s entre 2 fixe.
      double x1m=x2m;
      double y1m=y2m;
      x2m=(sgs_[i].o[0]+sgs_[i].e[0])/2;
      y2m=(sgs_[i].o[1]+sgs_[i].e[1])/2;
      sentre2fixe+=Math.sqrt((x2m-x1m)*(x2m-x1m)+(y2m-y1m)*(y2m-y1m));
      // Recherche du segment suivant � h fix�. (Le dernier est consid�r� comme fix�).
      if (!sgs_[i].isHAutomatique() || i==sgs_.length-1) {
        // Pour tous les segments interm�diaires, calcul du h.
        double hfixe1=sgs_[ifixe1].getHauteur();
        double hfixe2=sgs_[i].getHauteur();
        x2m=(sgs_[ifixe1].o[0]+sgs_[ifixe1].e[0])/2;
        y2m=(sgs_[ifixe1].o[1]+sgs_[ifixe1].e[1])/2;
        double s=0;
        for (int j=ifixe1+1; j<i; j++) {
          x1m=x2m;
          y1m=y2m;
          x2m=(sgs_[j].o[0]+sgs_[j].e[0])/2;
          y2m=(sgs_[j].o[1]+sgs_[j].e[1])/2;
          s+=Math.sqrt((x2m-x1m)*(x2m-x1m)+(y2m-y1m)*(y2m-y1m));
          double h=hfixe1+(hfixe2-hfixe1)*s/sentre2fixe;
          sgs_[j].setHauteur(h);
        }
        ifixe1=i;
        sentre2fixe=0;
        x2m=(sgs_[ifixe1].o[0]+sgs_[ifixe1].e[0])/2;
        y2m=(sgs_[ifixe1].o[1]+sgs_[ifixe1].e[1])/2;
      }
    }

    pentesXY_=null;
  }
}
