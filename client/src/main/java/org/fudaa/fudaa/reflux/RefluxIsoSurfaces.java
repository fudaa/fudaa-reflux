/*
 * @file         RefluxIsoSurfaces.java
 * @creation     1999-01-11
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
/**
 * Trace d'iso-surfaces.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Guillaume Desnoix , Bertrand Marchand
 */
public class RefluxIsoSurfaces {
  RefluxPaletteCouleur pal_;
  public RefluxIsoSurfaces(RefluxPaletteCouleur _pal) {
    pal_= _pal;
  }
  public void draw(Graphics _g, Polygon _p, double[] _v) {
    drawX(_g, _p, _v);
  }
  private void drawX(Graphics _g, Polygon _p, double[] _v) {
    if (_p.npoints == 3)
      draw3(_g, _p, _v);
    else if (_p.npoints == 4)
      draw4(_g, _p, _v);
    else if (_p.npoints >= 5)
      draw5(_g, _p, _v);
  }
  private void draw5(Graphics _g, Polygon _p, double[] _v) {
    Polygon p1= new Polygon();
    Polygon p2= new Polygon();
    double[] v1= new double[3];
    double[] v2= new double[_p.npoints - 1];
    p1.addPoint(_p.xpoints[0], _p.ypoints[0]);
    p1.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p1.addPoint(_p.xpoints[_p.npoints - 1], _p.ypoints[_p.npoints - 1]);
    v1[0]= _v[0];
    v1[1]= _v[1];
    v1[2]= _v[_p.npoints - 1];
    for (int i= 1; i < _p.npoints; i++) {
      p2.addPoint(_p.xpoints[i], _p.ypoints[i]);
      v2[i - 1]= _v[i];
    }
    draw3(_g, p1, v1);
    drawX(_g, p2, v2);
  }
  private void draw4(Graphics _g, Polygon _p, double[] _v) {
    Polygon p1= new Polygon();
    Polygon p2= new Polygon();
    p1.addPoint(_p.xpoints[0], _p.ypoints[0]);
    p1.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p1.addPoint(_p.xpoints[3], _p.ypoints[3]);
    p2.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p2.addPoint(_p.xpoints[2], _p.ypoints[2]);
    p2.addPoint(_p.xpoints[3], _p.ypoints[3]);
    double[] v1= new double[3];
    double[] v2= new double[3];
    v1[0]= _v[0];
    v1[1]= _v[1];
    v1[2]= _v[3];
    v2[0]= _v[1];
    v2[1]= _v[2];
    v2[2]= _v[3];
    draw3(_g, p1, v1);
    draw3(_g, p2, v2);
  }
  /**
   * Principe :
   * 1. On remplit le triangle avec la couleur correspondante � la valeur du
   *    sommet moyen (sommet dont la valeur n'est ni min ni max sur ce triangle).
   * 2. On remplit des triangles d�finis avec le sommet valeur max et les 2
   *    intersections entre les cotes du triangle et les limites des niveaux sup.
   *    au niveau moyen
   * 3. On remplit des triangles d�finis avec le sommet valeur min et les 2
   *    intersections entre les cotes du triangle et les limites des niveaux inf.
   *    au niveau moyen
   */
  private void draw3(Graphics _g, Polygon _p, double[] _v) {
    double[] vniv= pal_.getNiveaux();
    // Le dernier niveau n'a pas de couleur correspondante
    Color[] cniv= pal_.getCouleurs();
    int[] xp= new int[5];
    int[] yp= new int[5];
    int np;
    double vmin= Math.min(_v[0], Math.min(_v[1], _v[2]));
    double vmax= Math.max(_v[0], Math.max(_v[1], _v[2]));
    double bmin= vniv[0];
    double bmax= vniv[vniv.length - 1];
    if (vmin > bmax || vmax < bmin)
      return;
    //int      pn=_p.npoints;
    int[] px= _p.xpoints;
    int[] py= _p.ypoints;
    double d;
    int nmin= 0;
    if (vmin == _v[1])
      nmin= 1;
    if (vmin == _v[2])
      nmin= 2;
    int nmax= 0;
    if (vmax == _v[1] && nmin != 1)
      nmax= 1;
    if (vmax == _v[2] && nmin != 2)
      nmax= 2;
    int nmoy= 0;
    while ((nmoy == nmin) || (nmoy == nmax))
      nmoy++;
    double vmoy= _v[nmoy];
    int imin;
    for (imin= vniv.length - 1; imin > -1; imin--)
      if (vmin > vniv[imin])
        break;
    imin= Math.max(imin, 0);
    int imax;
    for (imax= 0; imax < vniv.length; imax++)
      if (vmax < vniv[imax])
        break;
    imax= Math.min(imax, vniv.length - 1);
    int imoy= -1;
    for (int i= 0; i <= vniv.length - 1; i++)
      if ((vniv[i] <= vmoy) && (vmoy < vniv[i + 1])) {
        imoy= i;
        break;
      }
    // D�termination des 2 points d'intersection (ou du point) du premier
    // niveau
    int[] xpp= new int[2];
    int[] ypp= new int[2];
    int npp= 0;
    if (bmin <= vmin) { // 1pt
      xpp[0]= px[nmin];
      ypp[0]= py[nmin];
      npp= 1;
    } else { // 2pts
      d= (bmin - _v[nmoy]) / (_v[nmin] - _v[nmoy]);
      xpp[0]= (int) (d * px[nmin] + (1. - d) * px[nmoy]);
      ypp[0]= (int) (d * py[nmin] + (1. - d) * py[nmoy]);
      d= (bmin - _v[nmin]) / (_v[nmax] - _v[nmin]);
      xpp[1]= (int) (d * px[nmax] + (1. - d) * px[nmin]);
      ypp[1]= (int) (d * py[nmax] + (1. - d) * py[nmin]);
      npp= 2;
    }
    // D�termination des 2 points d'intersection (ou du point) du dernier
    // niveau
    int[] xpd= new int[2];
    int[] ypd= new int[2];
    int npd= 0;
    if (bmax >= vmax) { // 1pt
      xpd[0]= px[nmax];
      ypd[0]= py[nmax];
      npd= 1;
    } else { // 2pts
      d= (bmax - _v[nmin]) / (_v[nmax] - _v[nmin]);
      xpd[0]= (int) (d * px[nmax] + (1. - d) * px[nmin]);
      ypd[0]= (int) (d * py[nmax] + (1. - d) * py[nmin]);
      d= (bmax - _v[nmoy]) / (_v[nmax] - _v[nmoy]);
      xpd[1]= (int) (d * px[nmax] + (1. - d) * px[nmoy]);
      ypd[1]= (int) (d * py[nmax] + (1. - d) * py[nmoy]);
      npd= 2;
    }
    // Niveau (s'il y en a un) incluant le point de valeur moyenne
    if (bmin < vmoy && vmoy < bmax) {
      np= 0;
      for (int i= 0; i < npp; i++) {
        xp[np]= xpp[i];
        yp[np]= ypp[i];
        np++;
      }
      for (int i= 0; i < npd; i++) {
        xp[np]= xpd[i];
        yp[np]= ypd[i];
        np++;
      }
      xp[np]= px[nmoy];
      yp[np]= py[nmoy];
      np++;
      _g.setColor(cniv[imoy]);
      _g.fillPolygon(xp, yp, np);
      _g.drawPolygon(xp, yp, np);
    }
    // Niveaux au dessus du niveau de valeur moyenne
    if (vmoy < bmax) {
      np= 0;
      for (int i= 0; i < npd; i++) {
        xp[np]= xpd[i];
        yp[np]= ypd[i];
        np++;
      }
      int j= imin;
      if (imoy != -1)
        j= imoy + 1;
      for (int i= j; i < imax; i++) {
        d= (vniv[i] - _v[nmoy]) / (_v[nmax] - _v[nmoy]);
        xp[np]= (int) (d * px[nmax] + (1. - d) * px[nmoy]);
        yp[np]= (int) (d * py[nmax] + (1. - d) * py[nmoy]);
        d= (vniv[i] - _v[nmin]) / (_v[nmax] - _v[nmin]);
        xp[np + 1]= (int) (d * px[nmax] + (1. - d) * px[nmin]);
        yp[np + 1]= (int) (d * py[nmax] + (1. - d) * py[nmin]);
        _g.setColor(cniv[i]);
        _g.fillPolygon(xp, yp, np + 2);
        _g.drawPolygon(xp, yp, np + 2);
      }
    }
    // Niveaux en dessous du niveau de valeur moyenne
    if (vmoy > bmin) {
      np= 0;
      for (int i= 0; i < npp; i++) {
        xp[np]= xpp[i];
        yp[np]= ypp[i];
        np++;
      }
      int j= imax;
      if (imoy != -1)
        j= imoy;
      for (int i= j; i > imin; i--) {
        d= (vniv[i] - _v[nmin]) / (_v[nmax] - _v[nmin]);
        xp[np]= (int) (d * px[nmax] + (1. - d) * px[nmin]);
        yp[np]= (int) (d * py[nmax] + (1. - d) * py[nmin]);
        d= (vniv[i] - _v[nmoy]) / (_v[nmin] - _v[nmoy]);
        xp[np + 1]= (int) (d * px[nmin] + (1. - d) * px[nmoy]);
        yp[np + 1]= (int) (d * py[nmin] + (1. - d) * py[nmoy]);
        _g.setColor(cniv[i]);
        _g.fillPolygon(xp, yp, np + 2);
        _g.drawPolygon(xp, yp, np + 2);
      }
    }
  }
}
