/*
 * @file         PRSolutionsInitiales.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import org.fudaa.ebli.geometrie.GrNoeud;
/**
 * Une classe de stockage des solutions initiales pour un noeud.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRSolutionsInitiales {
  /**
   * Valeurs.
   */
  private double[] vals_= new double[4];
  /**
   * Noeud associ�.
   */
  private GrNoeud support_;
  /**
   * Construit une solution sans attributs. Les attributs doivent dans ce cas
   * etre OBLIGATOIREMENT specifies par les methodes appropriees avant toute
   * manipulation.
   */
  public PRSolutionsInitiales() {
    support(null);
  }
  /**
   * Construit une solution initiale avec des valeurs.
   */
  public PRSolutionsInitiales(GrNoeud _nd, double[] _vals) {
    support(_nd);
    vals_= _vals;
  }
  /**
   * Construit une solution initiale pour de la courantologie 2d.
   * @deprecated
   */
  public PRSolutionsInitiales(GrNoeud _nd, double _u, double _v, double _h) {
    this(_nd, new double[] { _u, _v, 0, _h });
    //    this(_nd,new double[]{_u},new double[]{_v},new double[1],_h);
  }
  /**
   * Construit une solution initiales pour de la courantologie 3d.
   * @deprecated
   */
  public PRSolutionsInitiales(
    GrNoeud _nd,
    double[] _u,
    double[] _v,
    double[] _w,
    double _h) {
    support(_nd);
    vals_= new double[_u.length + _v.length + _w.length + 1];
    System.arraycopy(_u, 0, vals_, 0, _u.length);
    System.arraycopy(_v, 0, vals_, _u.length, _v.length);
    System.arraycopy(_w, 0, vals_, _u.length + _v.length, _w.length);
    vals_[_u.length + _v.length + _w.length]= _h;
  }
  /**
   * Retourne les valeurs.
   * @return Les valeurs de solutions.
   */
  public double[] valeurs() {
    return vals_;
  }
  /**
   * D�finit les valeurs.
   * @param _vals Valeurs.
   */
  public void valeurs(double[] _vals) {
    vals_= _vals;
  }
  /**
   * Retourne la hauteur (courantologie).
   * @deprecated
   */
  public double hauteur() {
    return vals_[vals_.length - 1];
  }
  /**
   * D�finit la hauteur (courantologie).
   * @deprecated
   */
  public void hauteur(double _h) {
    vals_[vals_.length - 1]= _h;
  }
  /**
   * Retourne les vitesses en x (courantologie).
   * @deprecated
   */
  public double[] u() {
    double[] u= new double[(vals_.length - 1) / 3];
    System.arraycopy(vals_, 0, u, 0, u.length);
    return u;
  }
  /**
   * D�finit les vitesses en x (courantologie).
   * @deprecated
   */
  public void u(double[] _u) {
    System.arraycopy(_u, 0, vals_, 0, _u.length);
  }
  /**
   * Retourne les vitesses en y (courantologie).
   * @deprecated
   */
  public double[] v() {
    double[] v= new double[(vals_.length - 1) / 3];
    System.arraycopy(vals_, v.length, v, 0, v.length);
    return v;
  }
  /**
   * D�finit les vitesses en y (courantologie).
   * @deprecated
   */
  public void v(double[] _v) {
    System.arraycopy(_v, 0, vals_, _v.length, _v.length);
  }
  /**
   * Retourne les vitesses en z (courantologie).
   * @deprecated
   */
  public double[] w() {
    double[] w= new double[(vals_.length - 1) / 3];
    System.arraycopy(vals_, w.length * 2, w, 0, w.length);
    return w;
  }
  /**
   * D�finit les vitesses en z (courantologie).
   * @deprecated
   */
  public void w(double[] _w) {
    System.arraycopy(_w, 0, vals_, _w.length * 2, _w.length);
  }
  /**
   * Retourne le support geometrique.
   * @return le support (= null si pas de support)
   */
  public GrNoeud support() {
    return support_;
  }
  /**
   * Definit le supports geometrique.
   * @param _support le support.
   */
  public void support(GrNoeud _support) {
    support_= _support;
  }
  /**
   * Initialise la solution initiale pour un ht=0 (courantologie).
   * @deprecated
   */
  public void initialiseHt0() {
    for (int i= 0; i < vals_.length; i++)
      vals_[i]= 0;
    //    h_=0;
    //    for (int i=0; i<u_.length; i++) u_[i]=0;
    //    for (int i=0; i<v_.length; i++) v_[i]=0;
    //    for (int i=0; i<w_.length; i++) w_[i]=0;
  }
}