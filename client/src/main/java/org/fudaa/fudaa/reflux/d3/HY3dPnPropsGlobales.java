/*
 * @file         HY3dPnPropsGlobales.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d3;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuGridLayout;

import org.fudaa.dodico.corba.mesure.IEvolutionConstante;

import org.fudaa.dodico.objet.UsineLib;

import org.fudaa.fudaa.reflux.PRPnPropsGlobales;
import org.fudaa.fudaa.reflux.PRPropriete;
/**
 * Un panel comportant les propriétés physiques globales spécifiques Reflux 3d
 * courantologie.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class HY3dPnPropsGlobales extends PRPnPropsGlobales {
  //public class HY3dPnPropsGlobales extends JPanel {
  BuGridLayout lyThis= new BuGridLayout();
  JLabel lbViscoCine= new JLabel();
  JTextField tfViscoCine= new JTextField();
  JLabel lbViscoHoriz= new JLabel();
  JTextField tfViscoHoriz= new JTextField();
  JLabel lbFrotFond= new JLabel();
  JTextField tfFrotFond= new JTextField();
  JLabel lbRugoFond= new JLabel();
  JTextField tfRugoFond= new JTextField();
  JLabel lbCstVK= new JLabel();
  JTextField tfCstVK= new JTextField();
  JLabel lbAjust= new JLabel();
  JTextField tfAjust= new JTextField();
  JLabel lbFrotBord= new JLabel();
  JTextField tfFrotBord= new JTextField();
  /**
   * Sauvegarde des propriétés.
   */
  private double[] oldPrps_= new double[7];
  /**
   * Création du panel.
   */
  public HY3dPnPropsGlobales() {
    try {
      jbInit();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  /**
   * Méthode d'implémentation de l'interface.
   */
  private void jbInit() throws Exception {
    lbViscoCine.setText("Viscosité cinématique de l'eau :");
    lbViscoHoriz.setText("Viscosité horizontale constante :");
    lbFrotFond.setText("Coef. de frottement au fond :");
    lbRugoFond.setText("Rugosité au fond :");
    lbCstVK.setText("Constante de Von Karmann :");
    lbAjust.setText("Coef. d'ajustement :");
    lbFrotBord.setText("Coef. de frottement sur parois :");
    lbViscoCine.setHorizontalAlignment(SwingConstants.RIGHT);
    lbViscoHoriz.setHorizontalAlignment(SwingConstants.RIGHT);
    lbFrotFond.setHorizontalAlignment(SwingConstants.RIGHT);
    lbRugoFond.setHorizontalAlignment(SwingConstants.RIGHT);
    lbCstVK.setHorizontalAlignment(SwingConstants.RIGHT);
    lbAjust.setHorizontalAlignment(SwingConstants.RIGHT);
    lbFrotBord.setHorizontalAlignment(SwingConstants.RIGHT);
    tfViscoCine.setPreferredSize(new Dimension(150, 21));
    tfViscoHoriz.setPreferredSize(new Dimension(150, 21));
    tfFrotFond.setPreferredSize(new Dimension(150, 21));
    tfRugoFond.setPreferredSize(new Dimension(150, 21));
    tfCstVK.setPreferredSize(new Dimension(150, 21));
    tfAjust.setPreferredSize(new Dimension(150, 21));
    tfFrotBord.setPreferredSize(new Dimension(150, 21));
    lyThis.setColumns(2);
    lyThis.setHgap(5);
    lyThis.setVgap(5);
    this.setLayout(lyThis);
    this.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.add(lbViscoCine, null);
    this.add(tfViscoCine, null);
    this.add(lbViscoHoriz, null);
    this.add(tfViscoHoriz, null);
    this.add(lbFrotFond, null);
    this.add(tfFrotFond, null);
    this.add(lbRugoFond, null);
    this.add(tfRugoFond, null);
    this.add(lbCstVK, null);
    this.add(tfCstVK, null);
    this.add(lbAjust, null);
    this.add(tfAjust, null);
    this.add(lbFrotBord, null);
    this.add(tfFrotBord, null);
  }
  /**
   * Initialisation avec les propriétés globales.
   *
   * @param _prps Les propriétés.
   */
  public void setProprietes(PRPropriete[] _prps) {
    for (int i= 0; i < oldPrps_.length; i++)
      oldPrps_[i]= ((IEvolutionConstante)_prps[i].evolution()).constante();
    tfViscoCine.setText("" + oldPrps_[0]);
    tfViscoHoriz.setText("" + oldPrps_[1]);
    tfFrotFond.setText("" + oldPrps_[2]);
    tfRugoFond.setText("" + oldPrps_[3]);
    tfCstVK.setText("" + oldPrps_[4]);
    tfAjust.setText("" + oldPrps_[5]);
    tfFrotBord.setText("" + oldPrps_[6]);
  }
  /*
   * Retourne les propriétés physiques globales.
   * @return Les propriétés dans l'ordre.
   */
  public PRPropriete[] getProprietes() {
    PRPropriete[] r= new PRPropriete[7];
    try {
      oldPrps_[0]= Double.parseDouble(tfViscoCine.getText());
    } catch (NumberFormatException _exc) {}
    try {
      oldPrps_[1]= Double.parseDouble(tfViscoHoriz.getText());
    } catch (NumberFormatException _exc) {}
    try {
      oldPrps_[2]= Double.parseDouble(tfFrotFond.getText());
    } catch (NumberFormatException _exc) {}
    try {
      oldPrps_[3]= Double.parseDouble(tfRugoFond.getText());
    } catch (NumberFormatException _exc) {}
    try {
      oldPrps_[4]= Double.parseDouble(tfCstVK.getText());
    } catch (NumberFormatException _exc) {}
    try {
      oldPrps_[5]= Double.parseDouble(tfAjust.getText());
    } catch (NumberFormatException _exc) {}
    try {
      oldPrps_[6]= Double.parseDouble(tfFrotBord.getText());
    } catch (NumberFormatException _exc) {}
    for (int i= 0; i < oldPrps_.length; i++) {
      IEvolutionConstante evol=UsineLib.findUsine().creeMesureEvolutionConstante();
/*        IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
      evol.constante(oldPrps_[i]);
      r[i]= new PRPropriete(-1, evol, new Object[0]);
    }
    return r;
  }
}
