/*
 * @file         PRCalqueNormale.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import org.fudaa.ebli.calque.BCalqueSymbole;
/**
 * Un calque de trace des normales sur les noeuds de bord.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRCalqueNormale extends BCalqueSymbole {
  //private PRProjet        probleme_;
  //private ListeGrContour   noeuds_;
  //private GrBoite          boite_;         // Boite englobante des noeuds du calque
  private int taille_= 15;
  public PRCalqueNormale() {
    super();
    setDestructible(false);
    //    noeuds_=new ListeGrContour();
    //    boite_=null;
  }
  // Icon
  /**
   * Dessin de l'icone.
   * @param _c composant dont l'icone peut deriver des proprietes
   * (couleur, ...). Ce parametre peut etre <I>null</I>. Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  public void paintIcon(Component _c, Graphics _g, int _x, int _y) {
    super.paintIcon(_c, _g, _x, _y);
    //boolean attenue=isAttenue();
    int w= getIconWidth() - 1;
    int h= getIconHeight() - 1;
    Image i= RefluxResource.REFLUX.getImage("normales");
    if (i != null) {
      int wi= i.getWidth(this);
      int hi= i.getHeight(this);
      int r= Math.max(wi / w, hi / h);
      _g.drawImage(i, _x + 1, _y + 1, wi / r - 1, hi / r - 1, _c);
    }
  }
  /**
   * Trac� des normales.
   */
  public void paintComponent(Graphics _g) {
    // D�finition de la taille des symboles
    if (!isRapide()) {
      for (int i= 0; i < symboles_.nombre(); i++)
        symboles_.renvoie(i).echelle_= taille_;
    }
    super.paintComponent(_g);
  }
  /**
   * D�finition de la taille en pixels de toutes les normales du calque.
   * Cette taille est normalement donn�e pour chaque normale.
   *
   * @param _taille Taille des normales en pixels
   */
  public void setTailleNormales(int _taille) {
    taille_= _taille;
  }
  /**
   * Retoune la taille des normales en pixels.
   */
  public int getTaillenormales() {
    return taille_;
  }
}
