/*
 * @file         Reflux.java
 * @creation     1998-06-25
 * @modification $Date: 2007-01-19 13:14:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import javax.swing.JFrame;

import com.diaam.lgpl.ts.TerminalStandard;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuSplashScreen;

import org.fudaa.dodico.corba.dunes.ICalculDunes;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.olb.ICalculOlb;
import org.fudaa.dodico.corba.reflux.ICalculReflux;
import org.fudaa.dodico.corba.reflux3d.ICalculReflux3d;

import org.fudaa.fudaa.commun.impl.FudaaCommandLineParser;
/**
 * Point d'entr�e de l'application Reflux.
 *
 * @version      $Revision: 1.10 $ $Date: 2007-01-19 13:14:36 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public final class Reflux {
  

  public static ICalculReflux SERVEUR_REFLUX;
  public static ICalculOlb SERVEUR_OLB;
  public static ICalculDunes SERVEUR_DUNES;
  public static ICalculReflux3d SERVEUR_REFLUX3D;
  public static IConnexion CONNEXION_REFLUX;
  public static IConnexion CONNEXION_OLB;
  public static IConnexion CONNEXION_DUNES;
  public static IConnexion CONNEXION_REFLUX3D;
//  public static IPersonne PERSONNE= null;
  
  private Reflux() {}
  /**
   * Ex�cution de l'application.
   */
  public static void main(String[] _args) {
    // Lecture des flags
    FudaaCommandLineParser flags= new FudaaCommandLineParser();
    _args= flags.parse(_args);
    if (_args.length > 0) {
      System.err.println("The flag " + _args[0] + " is unknown");
      System.err.println("Flags: " + flags.flagTotalText());
      System.exit(1);
    }
    // Affichage de la version et sortie
    if (flags.version_) {
      String ver= RefluxImplementation.informationsSoftware().version;
      String date= RefluxImplementation.informationsSoftware().date;
      System.out.println("Fudaa-Reflux, version " + ver + " du " + date);
      System.exit(0);
    }
    System.out.println("Client Reflux");
    // Console syst�me
    if (!flags.noLog_) {
      String ver= RefluxImplementation.informationsSoftware().version;
      TerminalStandard ts= new TerminalStandard();
      ts.activeStandards();
      JFrame fts= new JFrame("Console syst�me");
      fts.setContentPane(ts);
      fts.pack();
      fts.show();
      String wlcmsg=
        "******************************************************************************\n"
          + "*                           Bienvenue dans Reflux "
          + ver
          + "                       *\n"
          + "*                           ---------------------------                      *\n"
          + "* Ceci est la console texte. Elle affiche tous les messages systeme:         *\n"
          + "* erreurs, taches en cours. Consultez-la regulierement pour savoir           *\n"
          + "* si le programme est actif, si une erreur s'est produite, ...               *\n"
          + "* En cas d'erreur, joignez son contenu (enregistre dans le fichier ts.log)   *\n"
          + "* au mail de notification de bogue, ceci nous aidera a comprendre.           *\n"
          + "******************************************************************************\n\n";
      System.out.println(wlcmsg);
    }
    // Preferences
    /*BuInformationsSoftware il=*/ RefluxImplementation.informationsSoftware();
    BuPreferences.BU.applyLookAndFeel();
    // Network Check
    //    FudaaNetworkChecker nc=new FudaaNetworkChecker(il);
    //    nc.check();
    // Update
    //    if(!flags.no_update) {
    //      FudaaUpdate update=new FudaaUpdate(il);
    //      update.setInstallMode(flags.jar_update?FudaaUpdate.JAR:FudaaUpdate.CLASS);
    //      if(update.scanForUpdate()) {
    //	BuRegistry.register(update);
    //	boolean r=update.startUpdate();
    //	BuRegistry.unregister(update);
    //	if (r) return;
    //      }
    //    }
    // Splash screen
    BuSplashScreen ss=
      new BuSplashScreen(
        RefluxImplementation.informationsSoftware(),
        3000,
        new String[][] { BuLib.SWING_CLASSES, BuLib.BU_CLASSES });
    if (!flags.noSplash_)
      ss.start();
    ss.setProgression(0);
    /*
        if (!flags.no_server) {

          // Reflux 2D
          ss.setText("Acces au serveur de calcul Reflux 2D...");
          PRCalculReflux2d.initialiser(!flags.no_corba);
          ss.setProgression(20);

          // Reflux 3D
          ss.setText("Acces au serveur de calcul Reflux 3D...");
          PRCalculReflux3d.initialiser(!flags.no_corba);
          ss.setProgression(30);

    //    if(!flags.no_corba&&!flags.no_server)
    //     // Recherche du serveur
    //     RefluxImplementation.SERVEUR_REFLUX=ICalculRefluxHelper.narrow(
    //      CDodico.findServerByInterface("::reflux::ICalculReflux",4000));
    //
    //
    //    if(!flags.no_server)
    //    if(RefluxImplementation.SERVEUR_REFLUX==null) {
    //
    //      ss.setText("Lancement d'un serveur Reflux local...");
    //      System.err.print("Lancement d'un serveur Reflux local...");
    //
    //      {
    //	ss.setProgression(30);
    //	ss.setText("Initialisation du serveur local...");
    //        // "un-serveur-reflux-local"
    //	RefluxImplementation.SERVEUR_REFLUX=new CCalculReflux();
    //	CDodico.getBOA().obj_is_ready(RefluxImplementation.SERVEUR_REFLUX);
    //	System.err.println("OK");
    //      }
    //      // else System.err.println("# introuvable.");
    //    }
        }
    */
    ss.setProgression(40);
    ss.setText("Cr�ation de l'application...");
    RefluxApplication app= new RefluxApplication();
    ss.setProgression(60);
    // new AlmaScreenCopy(app);
    // ss.setProgression(70);
    ss.setText("Initialisation de l'application...");
    app.init();
    ss.setProgression(100);
    try {
      Thread.sleep(500);
    } catch (Exception ex) {}
    ss.setVisible(false);
    ss.dispose();
    app.start();
  }
}
