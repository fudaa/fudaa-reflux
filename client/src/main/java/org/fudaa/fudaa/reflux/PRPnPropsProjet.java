/*
 * @file         PRPnPropsProjet.java
 * @creation     2001-03-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuGridLayout;
/**
 * Un panel comportant les propri�t�s du projet.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRPnPropsProjet extends JPanel {
  BuGridLayout lyThis= new BuGridLayout();
  JLabel lbType= new JLabel();
  JLabel lbTypeVal= new JLabel();
  JLabel lbNbNds= new JLabel();
  JLabel lbNbNdsVal= new JLabel();
  JLabel lbNbEls= new JLabel();
  JLabel lbNbElsVal= new JLabel();
  /**
   * Sauvegarde des propri�t�s.
   */
  /**
   * Cr�ation du panel.
   */
  public PRPnPropsProjet() {
    try {
      jbInit();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  /**
   * M�thode d'impl�mentation de l'interface.
   */
  private void jbInit() throws Exception {
    JSeparator js= new JSeparator(SwingConstants.HORIZONTAL);
    JLabel lbDummy= new JLabel();
    lbType.setText("Type du projet:");
    lbNbNds.setText("Nombre de noeuds:");
    lbNbEls.setText("Nombre d'�l�ments:");
    lbType.setHorizontalAlignment(SwingConstants.RIGHT);
    lbNbNds.setHorizontalAlignment(SwingConstants.RIGHT);
    lbNbEls.setHorizontalAlignment(SwingConstants.RIGHT);
    lbTypeVal.setBorder(BorderFactory.createLoweredBevelBorder());
    lbNbNdsVal.setBorder(BorderFactory.createLoweredBevelBorder());
    lbNbElsVal.setBorder(BorderFactory.createLoweredBevelBorder());
    lbNbNdsVal.setHorizontalAlignment(SwingConstants.RIGHT);
    lbNbElsVal.setHorizontalAlignment(SwingConstants.RIGHT);
    lyThis.setColumns(2);
    lyThis.setHgap(5);
    lyThis.setVgap(5);
    this.setLayout(lyThis);
    this.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.add(lbType, null);
    this.add(lbTypeVal, null);
    this.add(lbDummy, null);
    this.add(js, null);
    this.add(lbNbNds, null);
    this.add(lbNbNdsVal, null);
    this.add(lbNbEls, null);
    this.add(lbNbElsVal, null);
  }
  /**
   * Affectation du projet sur lequel avoir les informations.
   *
   * @param _prj Le projet.
   */
  public void setProjet(PRProjet _prj) {
    lbTypeVal.setText(Definitions.projetToString(_prj.type()));
    lbNbNdsVal.setText("" + _prj.maillage().noeuds().length);
    lbNbElsVal.setText("" + _prj.maillage().elements().length);
  }
}