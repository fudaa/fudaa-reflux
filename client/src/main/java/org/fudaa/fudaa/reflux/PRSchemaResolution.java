/*
 * @file         PRSchemaResolution.java
 * @creation     1998-06-08
 * @modification $Date: 2003-11-25 10:14:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
/**
 * Une classe de d�finition d'un sch�ma de r�solution avec ou sans coefficient.
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:14:06 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRSchemaResolution {
  /**
   * Type Stationnaire.
   */
  public final static int STATIONNAIRE= 0;
  /**
   * Type Euler.
   */
  public final static int EULER= 1;
  /**
   * Type Lax Wendroff.
   */
  public final static int LAX_WENDROFF= 2;
  /**
   * Type Kawahara.
   */
  public final static int KAWAHARA= 3;
  /**
   * Type du sch�ma.
   */
  private int type_= 0;
  /**
   * Coefficient.
   */
  private double coef_= 0;
  /**
   * Constructeur par d�faut.
   */
  public PRSchemaResolution() {
    this(Definitions.getTypesSchema()[0]);
  }
  /**
   * Constructeur avec le type donn� avec le coefficient par d�faut.
   * @param _type Type.
   */
  public PRSchemaResolution(int _type) {
    this(_type, Definitions.getDefaultCoefSchema(_type));
  }
  /**
   * Constructeur pour les sch�mas avec coefficient.
   * @param _type Type.
   * @param _coef Coeffocient.
   */
  public PRSchemaResolution(int _type, double _coef) {
    type(_type);
    coefficient(_coef);
  }
  /**
   * Attribut le sch�ma.
   * @param _type Le sch�ma de r�solution.
   */
  public void type(int _type) {
    this.type_= _type;
  }
  /**
   * Retourne le sch�ma.
   * @return Le sch�ma de r�solution.
   */
  public int type() {
    return this.type_;
  }
  /**
   * Attribut le coefficient pour le sch�ma.
   * @param _coef Le coefficient pour le sch�ma.
   */
  public void coefficient(double _coef) {
    this.coef_= _coef;
  }
  /**
   * Attribut le coefficient pour le sch�ma.
   * @return  Le coefficient pour le sch�ma.
   */
  public double coefficient() {
    return this.coef_;
  }
}
