/*
 * @file         HY2dProjet.java
 * @creation     2001-01-08
 * @modification $Date: 2007-01-19 13:14:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d2;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import com.memoire.bu.BuDialogError;

import org.fudaa.dodico.corba.mesure.IEvolution;
import org.fudaa.dodico.corba.mesure.IEvolutionConstante;
import org.fudaa.dodico.corba.mesure.IEvolutionSerieIrreguliere;
import org.fudaa.dodico.corba.planification.ISegmentationTemps;
import org.fudaa.dodico.corba.planification.ISegmentationTempsHelper;
import org.fudaa.dodico.corba.reflux.SResultatsEtapeReflux;
import org.fudaa.dodico.corba.reflux.SResultatsReflux;

import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.reflux.DResultatsReflux;

import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;

import org.fudaa.fudaa.commun.conversion.FudaaMaillageCORELEBTH;
import org.fudaa.fudaa.reflux.*;
/**
 * Un projet Reflux 2D.
 *
 * @version      $Revision: 1.13 $ $Date: 2007-01-19 13:14:38 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class HY2dProjet extends PRProjet {
  private static final int CA_VITX= 0;
  private static final int CA_VITY= 1;
  private static final int CA_HAUT= 2;
  private static final int CA_FROT= 3;
  private static final int CA_DEBI= 4;
  private static HY2dInterfaceCalcul interface_= null;
  private static double[][] pointsSI= new double[3][3];
  // Points de solutions initiales
  private static double[][] valeursSI;
  // Valeurs de solutions initiales pour tous les noeuds
  private static int numeroTypeProbleme; // Numero de type de probleme
  private static int nbNoeudsBord; // Nombre de noeuds de bord
  private static int[] impression= new int[9]; // Indices d'impression
  private static double[] coefPond= new double[9];
  // Coefficients de pond�ration
  // Bords
  private static int nbDefBords; // Nombre de d�finitions de bord
  private static int[] defBordsTypeTemp;
  // Type temporel (1 : Stationnaire, 2 : Transitoire)
  private static double[] defBordsValeur;
  // Valeur (valeur r�elle ou num�ro de courbe associ�e)
  private static int[] carBordsNoeuds; // Numeros des noeuds de bords ordonn�s
  private static int[][] carBords;
  // Caract�ristiques de bord (pointeurs sur les d�finitions)
  private static int[] natBords;
  // Nature des bords (1: Ouvert (d�bit,vitesse), 2: Ferm� (avec ou sans frot.), 3: Ouvert libre)
  // Fonds
  private static int nbDefFond; // Nombre de d�finitions de fond
  private static int[] defFondTypeTemp;
  // Type temporel (1 : Stationnaire, 2 : Transitoire)
  private static double[] defFondValeur;
  // Valeur (valeur r�elle ou num�ro de courbe associ�e)
  private static int[][] carFond;
  // Caract�ristiques de fond (pointeurs sur les d�finitions)
  // Groupes de pas de temps
  private static int nbGroupesPT; // Nombre de groupes de pas de temps
  private static double[] groupesPTDebut; // Debut du temps
  private static double[] groupesPTFin; // Fin du temps
  private static double[] groupesPTDT; // Delta T
  private static double[] groupesPTCoefSchema; // Coefficient du sch�ma
  private static int[] groupesPTMethode; // M�thode
  private static double[][] groupesPTCoefMethode; // Coefficients de la m�thode
  private static int[] groupesPTfrequence;
  // Fr�quence de stockage des r�sultats
  // Normales sur les noeuds de bord
  private static int[] normalesNoeuds; // Num�ros des noeuds
  private static double[] normalesValeurs; // Valeurs
  private static int[] numGrpElements;
  private static int[][] numElements;

  private static int[] numMultiplans;
  private static int[] numMultiplansGrp;
  private static int[][] numMultiplansLignes;
  private static double[][][] p1MultiplansLignes;
  private static double[][][] p2MultiplansLignes;
  private static int[][] autoMultiplansLignes;
  private static double[][] valMultiplansLignes; // Si auto, num plan associ�.


  /**
   * Initialisation du projet depuis les fichiers .bth/.cor/.ele./[.crb] de
   * nom sp�cifi�.
   *
   * @param _racine Le nom de la racine des fichiers sans extension.
   * @param _params Param�tres de cr�ation du projet. Non utilis� dans ce type
   *                de projet.
   *
   * @exception FileNotFoundException Un fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   */
  public void initialiser(String _racine, Object[] _params)
    throws IOException {
    RefluxMaillage maillage;
    // Racine des fichiers
    racineFichiers(_racine);
    // Lecture du maillage
    maillage= lireMaillage(_racine);
    maillage(maillage);
    // Tentative de lecture du fichier des courbes s'il y en a un
    try {
      //      _operation.operation="Lecture des courbes";
      lireFichierCRB(_racine + ".crb");
    } catch (FileNotFoundException _exc) {}
    // Points d�finissant le plan des solutions initiales
    //    getMainPanel().setProgression(50);
//    {
//      GrBoite boite= new GrBoite();
//      GrNoeud[] noeuds= maillage().noeuds();
//      GrPoint[] ptsSI= new GrPoint[3];
//      GrPoint pmin;
//      GrPoint pmax;
//      for (int i= 0; i < noeuds.length; i++)
//        boite.ajuste(noeuds[i].point);
//      pmin= boite.o;
//      pmax= boite.e;
//      ptsSI[0]= new GrPoint(pmin.x, pmin.y, pmax.z + 0.2);
//      ptsSI[1]= new GrPoint(pmax.x, pmin.y, pmax.z + 0.2);
//      ptsSI[2]= new GrPoint(pmax.x, pmax.y, pmax.z + 0.2);
//      modeleCalcul().pointsSI(ptsSI);
//    }

    // Solutions initiales (a partir du plan)
    //    getMainPanel().setProgression(55);
    //    initialiserConditionsInitiales();
    {
      GrNoeud[] noeuds= maillage().noeuds();
      double z=Double.NEGATIVE_INFINITY;
      for (int i= 0; i < noeuds.length; i++) z=Math.max(z,noeuds[i].point_.z_);
      initialiserSolutionsInitiales(new double[]{z+0.2});
    }
    // Normales en chaque noeuds des contours
    //    getMainPanel().setProgression(60);
    initialiserNormales();
    //Nature des bords
    //    getMainPanel().setProgression(65);
    initialiserNatureBords();
    //Nature des fonds
    //    getMainPanel().setProgression(70);
    initialiserNatureFonds();
  }

  /**
   * Chargement du projet depuis les fichiers .pre/.bth/.cor/.ele./[.crb] de
   * nom sp�cifi�.
   *
   * @param _racine Le nom de la racine des fichiers sans extension.
   * @exception FileNotFoundException Un fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   */
  public void charger(String _racine) throws IOException {
    String racine; // Racine des noms de fichier (sans extension)
    RefluxMaillage maillage; // Maillage issu des .bth/.cor/.ele
    GrNoeud[] noeuds;
    GrElement[] elements;
    Hashtable nd2Bord;
    int nbNoeudArete= 0;
    // Nombre de noeuds par ar�te (ce nombre est constant pour tous les elements,
    // les versions avant 5.0 ne supportant pas des elements diff�rents)
    int nbAretes= 0;
    racine= _racine;
    // Controle que les fichiers existent et qu'ils sont ouvrables en lecture
    String extension[]= { ".pre" };
    File file;
    for (int i= 0; i < extension.length; i++) {
      file= new File(racine + extension[i]);
      if (!file.exists() || !file.canRead())
        throw new FileNotFoundException(racine + extension[i]);
    }
    // Racine des fichiers
    racineFichiers(racine);
    // Lecture du maillage
    maillage= lireMaillage(racine);
    maillage(maillage);
    // Lecture du fichier .pre contenant la sauvegarde des informations
    lireFichierPRE(racine + extension[0]);
    // Lecture du fichier .crb
    try {
      lireFichierCRB(racine + ".crb");
    }
    // FileNotFoundException => On controlera + tard la coh�rence .pre/.crb
    catch (FileNotFoundException _exc) {}
    // Lecture du fichier des solutions
    if (new File(racine + ".sov").exists())
      readResults(racine);
    noeuds= maillage.noeuds();
    elements= maillage.elements();
    // Association 1er GrNoeud extremit� de GrElement -> GrElement
    {
      GrElement[][] aretes= maillage().aretesContours();
      for (int i= 0; i < aretes.length; i++)
        nbAretes += aretes[i].length;
      nbNoeudArete= aretes[0][0].noeuds_.length;
      nd2Bord= new Hashtable(nbAretes > 0 ? nbAretes : 1);
      for (int i= 0; i < aretes.length; i++)
        for (int j= 0; j < aretes[i].length; j++)
          nd2Bord.put(aretes[i][j].noeuds_[0], aretes[i][j]);
    }
    Hashtable num2El= new Hashtable();
    {
      GrElement[] els= maillage().elements();
      for (int i= 0; i < els.length; i++)
        num2El.put(new Integer(i),els[i]);
    }
    //--------------------------------------------------------------------------
    // Type du probleme
    //--------------------------------------------------------------------------
    switch (numeroTypeProbleme) {
      case 1 :
        type(PRProjet.VIT_NU_CON);
        break;
      case 2 :
        type(PRProjet.VIT_NU_LMG);
        break;
    }
    //--------------------------------------------------------------------------
    // Points d�finissant le plan des solutions initiales
    //--------------------------------------------------------------------------
//    {
//      GrPoint[] points= new GrPoint[3];
//      for (int i= 0; i < points.length; i++)
//        points[i]= new GrPoint(pointsSI[i]);
//      modeleCalcul().pointsSI(points);
//    }
    //--------------------------------------------------------------------------
    // Nature des �l�ments de fond => Tous HYDRO_FOND ou HYDRO_FOND_LMG
    //--------------------------------------------------------------------------
    {
      PRNature natureFonds= null;
      if (type() == PRProjet.VIT_NU_CON)
        natureFonds= new PRNature(PRNature.FOND, elements);
      else if (type() == PRProjet.VIT_NU_LMG)
        natureFonds= new PRNature(PRNature.FOND_LMG, elements);
      mdlPrp_.naturesFonds().addElement(natureFonds);
    }
    //--------------------------------------------------------------------------
    // Propri�t�s des �l�ments de fond
    //--------------------------------------------------------------------------
    {
      IEvolutionConstante evolCst; // Evolution constante
      PRPropriete[][] tPE= new PRPropriete[nbDefFond][numeroTypeProbleme + 2];
      // Stockage temporaire des propri�t�s
      int[] type; // Type des propri�t�s
      if (numeroTypeProbleme == 1)
        type=
          new int[] {
            PRPropriete.VISCOSITE,
            PRPropriete.RUGOSITE,
            PRPropriete.PERTE_CHARGE };
      else
        type=
          new int[] {
            PRPropriete.VISCOSITE,
            PRPropriete.RUGOSITE,
            PRPropriete.ALPHA_LONGUEUR_MELANGE,
            PRPropriete.PERTE_CHARGE };
      int iDef; // Indice de d�finition
      int nbPE= 0; // Nombre de propri�t�s cr��es
      // Cr�ation des propri�t�s �l�mentaires et affectation des �l�ments
      for (int i= 0; i < elements.length; i++) {
        for (int j= 0; j < numeroTypeProbleme + 2; j++) {
          iDef= carFond[i][j] - 1;
          // Une d�finition existe pour cet �l�ment
          if (iDef != -1) {
            if (tPE[iDef][j] == null) {
              tPE[iDef][j]= new PRPropriete(type[j], null, new Vector());
              nbPE++;
            }
            tPE[iDef][j].supports().add(elements[i]);
          }
        }
      }
      // Initialisation des propri�t�s cr��es et transfert dans le tableau des
      // propri�t�s
      mdlPrp_.proprietesElements(new PRPropriete[nbPE]);
      nbPE= 0;
      for (int i= 0; i < nbDefFond; i++) {
        for (int j= 0; j < numeroTypeProbleme + 2; j++) {
          if (tPE[i][j] != null) {
            // Propri�t� stationnaire
            if (defFondTypeTemp[i] == 1) {
              evolCst=UsineLib.findUsine().creeMesureEvolutionConstante();
/*                IEvolutionConstanteHelper.narrow(
                  new DEvolutionConstante().tie());*/
              evolCst.constante(defFondValeur[i]);
              tPE[i][j].evolution(evolCst);
            }
            // Propri�t� transitoire
            else {
              // Probleme de coh�rence du fichier .crb par rapport au fichier .pre
              if (defFondValeur[i] <= 0.
                || defFondValeur[i] > evolutions().length) {
                throw new IOException(
                  "Probl�me de coh�rence entre le fichier "
                    + ".crb et le fichier .pre");
              }
              tPE[i][j].evolution(evolutions()[(int)defFondValeur[i] - 1]);
            }
            mdlPrp_.proprietesElements()[nbPE]= tPE[i][j];
            nbPE++;
          }
        }
      }
    }
    //--------------------------------------------------------------------------
    // Nature des ar�tes de bord
    //--------------------------------------------------------------------------
    {
      Vector natures= new Vector(nbAretes);
      final int[] caVit= new int[] { CA_VITX, CA_VITY };
      GrNoeud noeud;
      GrElement arete;
      int type;
      int iDef;
      int numNoeud;
      for (int i= 0; i < nbNoeudsBord; i += nbNoeudArete - 1) {
        switch (natBords[i]) {
          case 1 : // Ouvert (avec d�bit, vitesse ou sans rien)
            // Recherche de d�bit
            boolean debit= true;
            for (int j= 0; j < nbNoeudArete; j++) {
              if (carBords[(i + j) % nbNoeudsBord][CA_DEBI] == 0) {
                debit= false;
                break;
              }
            }
            // Recherche de vitesse non nulle sur un des noeuds
            boolean vitesse= false;
            VITESSE : for (int j= 0; j < nbNoeudArete; j++) {
              for (int k= 0; k < caVit.length; k++) {
                iDef= carBords[(i + j) % nbNoeudsBord][caVit[k]] - 1;
                if (iDef != -1
                  && (defBordsTypeTemp[iDef] == 2
                    || defBordsValeur[iDef] != 0.)) {
                  vitesse= true;
                  break VITESSE;
                }
              }
            }
            if (debit)
              type= PRNature.BORD_OUVERT_DEBIT;
            else if (vitesse)
              type= PRNature.BORD_OUVERT_VITESSE;
            else
              type= PRNature.BORD_OUVERT;
            break;
          case 2 : // Ferm� (avec ou sans frottement)
            boolean frottement= (carBords[i][CA_FROT] != 0);
            if (frottement)
              type= PRNature.BORD_FERME_FROTTEMENT;
            else
              type= PRNature.BORD_FERME;
            break;
          case 3 : // Ouvert libre
            type= PRNature.BORD_OUVERT_LIBRE;
            break;
          default : // On le consid�re ferm�
            type= PRNature.BORD_FERME;
            break;
        }
        numNoeud= carBordsNoeuds[i];
        //        noeud   =(GrNoeud)numToNoeud.get(new Integer(numNoeud));
        noeud= noeuds[numNoeud];
        arete= (GrElement)nd2Bord.get(noeud);
        natures.addElement(new PRNature(type, new GrElement[] { arete }));
      }
      mdlPrp_.naturesBords(natures);
    }
    //--------------------------------------------------------------------------
    // Propri�t�s de frottement des ar�tes de bord
    //--------------------------------------------------------------------------
    {
      // Evolution constante
      IEvolutionConstante evolCst= null;
      // Stockage temporaire des C.L.
      PRPropriete[][] tPA= new PRPropriete[nbDefBords][1];
      // Type des P.A.
      int[] type= new int[] { PRPropriete.FROTTEMENT_PAROI };
      // Indice de d�finition
      int iDef;
      // Nombre de P.A. cr��es
      int nbPA= 0;
      // Numero du 1er noeud extr�mit� d'arete courante
      int numNoeudArete;
      // 1er noeuds d'arete
      GrNoeud noeud= null;
      // Cr�ation des P.A. et affectation des ar�tes
      for (int i= 0; i < nbNoeudsBord; i += nbNoeudArete - 1) {
        numNoeudArete= carBordsNoeuds[i];
        for (int j= 0; j < 1; j++) {
          iDef= carBords[i][j + 3] - 1;
          // Une d�finition existe pour cette ar�te
          if (iDef != -1) {
            if (tPA[iDef][j] == null) {
              tPA[iDef][j]= new PRPropriete(type[j], null, new Vector());
              nbPA++;
            }
            //            noeud=(GrNoeud)numToNoeud.get(new Integer(numNoeudArete));
            noeud= noeuds[numNoeudArete];
            tPA[iDef][j].supports().add(nd2Bord.get(noeud));
          }
        }
      }
      // Initialisation des P.A. cr��es et transfert dans le tableau des
      // propri�t�s
      mdlPrp_.proprietesAretes(new PRPropriete[nbPA]);
      nbPA= 0;
      for (int i= 0; i < nbDefBords; i++) {
        for (int j= 0; j < 1; j++) {
          if (tPA[i][j] != null) {
            // P.A. stationnaire
            if (defBordsTypeTemp[i] == 1) {
              evolCst=UsineLib.findUsine().creeMesureEvolutionConstante();
/*                IEvolutionConstanteHelper.narrow(
                  new DEvolutionConstante().tie());*/
              evolCst.constante(defBordsValeur[i]);
              tPA[i][j].evolution(evolCst);
            }
            // P.A. transitoire
            else {
              // Probleme de coh�rence du fichier .crb par rapport au fichier .pre
              if (defBordsValeur[i] <= 0.
                || defBordsValeur[i] > evolutions().length) {
                throw new IOException(
                  "Probl�me de coh�rence entre le fichier "
                    + ".crb et le fichier .pre");
              }
              tPA[i][j].evolution(evolutions()[(int)defBordsValeur[i] - 1]);
            }
            mdlPrp_.proprietesAretes()[nbPA]= tPA[i][j];
            nbPA++;
          }
        }
      }
    }
    //--------------------------------------------------------------------------
    // Conditions limites
    //--------------------------------------------------------------------------
    {
      // Evolution constante
      IEvolutionConstante evolCst= null;
      // Stockage temporaire des C.L.
      PRConditionLimite[][] tCL= new PRConditionLimite[nbDefBords][3];
      // Type des C.L.
      int[] type=
        new int[] {
          PRConditionLimite.VITESSE_X,
          PRConditionLimite.VITESSE_Y,
          PRConditionLimite.HAUTEUR };
      // Indice de d�finition
      int iDef;
      // Nombre de C.L. cr��es
      int nbCL= 0;
      // Numero du noeud de bord courant
      int numNoeudBord= 0;
      // Cr�ation des conditions limites et affectation des noeuds
      for (int i= 0; i < nbNoeudsBord; i++) {
        numNoeudBord= carBordsNoeuds[i];
        for (int j= 0; j < 3; j++) {
          iDef= carBords[i][j] - 1;
          // Une d�finition existe pour ce noeud
          if (iDef != -1) {
            if (tCL[iDef][j] == null) {
              tCL[iDef][j]= new PRConditionLimite(type[j], null, new Vector());
              nbCL++;
            }
            //            tCL[iDef][j].geometries().ajoute((GrNoeud) numToNoeud.get(new Integer(numNoeudBord)));
            tCL[iDef][j].supports().add(noeuds[numNoeudBord]);
          }
        }
      }
      // Initialisation des C.L. cr��es et transfert dans le tableau des C.L.
      conditionsLimites(new PRConditionLimite[nbCL]);
      nbCL= 0;
      for (int i= 0; i < nbDefBords; i++) {
        for (int j= 0; j < 3; j++) {
          if (tCL[i][j] != null) {
            // C.L. stationnaire
            if (defBordsTypeTemp[i] == 1) {
              evolCst=UsineLib.findUsine().creeMesureEvolutionConstante();
/*                IEvolutionConstanteHelper.narrow(
                  new DEvolutionConstante().tie());*/
              evolCst.constante(defBordsValeur[i]);
              tCL[i][j].evolution(evolCst);
            }
            // C.L. transitoire
            else {
              // Probleme de coh�rence du fichier .crb par rapport au fichier .pre
              if (defBordsValeur[i] <= 0.
                || defBordsValeur[i] > evolutions().length) {
                throw new IOException(
                  "Probl�me de coh�rence entre le fichier "
                    + ".crb et le fichier .pre");
              }
              tCL[i][j].evolution(evolutions()[(int)defBordsValeur[i] - 1]);
            }
            conditionsLimites()[nbCL]= tCL[i][j];
            nbCL++;
          }
        }
      }
    }
    //--------------------------------------------------------------------------
    // Sollicitations
    //--------------------------------------------------------------------------
    {
      // Evolution constante
      IEvolutionConstante evolCst= null;
      // Stockage temporaire des Soll
      PRSollicitation[][] tS= new PRSollicitation[nbDefBords][1];
      // Type des Soll
      int[] type= new int[] { PRSollicitation.DEBIT };
      // Indice de d�finition
      int iDef;
      // Nombre de Soll cr��es
      int nbS= 0;
      // Numero du noeud de bord courant
      int numNoeudBord= 0;
      // Cr�ation des sollicitations et affectation des noeuds
      for (int i= 0; i < nbNoeudsBord; i++) {
        numNoeudBord= carBordsNoeuds[i];
        for (int j= 0; j < 1; j++) {
          iDef= carBords[i][j + 4] - 1;
          // Une d�finition existe pour ce noeud
          if (iDef != -1) {
            if (tS[iDef][j] == null) {
              tS[iDef][j]= new PRSollicitation(type[j], null, new Vector());
              nbS++;
            }
            //            tS[iDef][j].geometries().ajoute((GrNoeud) numToNoeud.get(new Integer(numNoeudBord)));
            tS[iDef][j].supports().add(noeuds[numNoeudBord]);
          }
        }
      }
      // Initialisation des Soll cr��es et transfert dans le tableau des
      // sollicitations
      sollicitations(new PRSollicitation[nbS]);
      nbS= 0;
      for (int i= 0; i < nbDefBords; i++) {
        for (int j= 0; j < 1; j++) {
          if (tS[i][j] != null) {
            // Sollicitation stationnaire
            if (defBordsTypeTemp[i] == 1) {
              evolCst=UsineLib.findUsine().creeMesureEvolutionConstante();
/*                IEvolutionConstanteHelper.narrow(
                  new DEvolutionConstante().tie());*/
              evolCst.constante(defBordsValeur[i]);
              tS[i][j].evolution(evolCst);
            }
            // Sollicitation transitoire
            else {
              // Probleme de coh�rence du fichier .crb par rapport au fichier .pre
              if (defBordsValeur[i] <= 0.
                || defBordsValeur[i] > evolutions().length) {
                throw new IOException(
                  "Probl�me de coh�rence entre le fichier "
                    + ".crb et le fichier .pre");
              }
              tS[i][j].evolution(evolutions()[(int)defBordsValeur[i] - 1]);
            }
            sollicitations()[nbS]= tS[i][j];
            nbS++;
          }
        }
      }
    }
    //--------------------------------------------------------------------------
    // Groupes de pas de temps
    //--------------------------------------------------------------------------
    {
      PRGroupePT[] grpPTs;
      // Boucle en temps
      PRBoucleTemps temps= null;
      // Sch�ma de r�solution
      PRSchemaResolution schema= null;
      // Type des sch�mas
      //      int typeSchema = 0;
      // M�thode de r�solution
      PRMethodeResolution methode= null;
      // Types de m�thodes (fonction du probleme)
      int typeMethode[][]=
        {
          {
            PRMethodeResolution.LINEAIRE,
            PRMethodeResolution.NEWTON_RAPHSON,
            PRMethodeResolution.NEWTON_RAPHSON_BCD,
            PRMethodeResolution.SELECTED_LUMPING,
            PRMethodeResolution.SELECTED_LUMPING_BCD },
          {
          PRMethodeResolution.NEWTON_RAPHSON_LMG }
      };
      double[] coefs;
      // Groupes de pas de temps
      grpPTs= new PRGroupePT[nbGroupesPT];
      for (int i= 0; i < nbGroupesPT; i++) {
        // Boucle en temps
        // Attention : Pour le cas stationnaire, on r�ajuste pour avoir un
        // pas de temps de 1.
        if (groupesPTDebut[i]==groupesPTFin[i])
          groupesPTFin[i]+=groupesPTDT[i];
        temps=
          new PRBoucleTemps(groupesPTDebut[i], groupesPTFin[i], groupesPTDT[i]);
        // M�thode
        methode= new PRMethodeResolution();
        methode.type(typeMethode[numeroTypeProbleme - 1][groupesPTMethode[i]]);
        coefs= groupesPTCoefMethode[i];
        methode.coefficients(coefs);
        // Sch�ma stationnaire si nb de pas de temps = 1
        if ((groupesPTFin[i] - groupesPTDebut[i]) / groupesPTDT[i] < 2)
          schema= new PRSchemaResolution(PRSchemaResolution.STATIONNAIRE);
        // Sch�ma d'Euler
        else if (
          methode.type() == PRMethodeResolution.LINEAIRE
            || methode.type() == PRMethodeResolution.NEWTON_RAPHSON
            || methode.type() == PRMethodeResolution.NEWTON_RAPHSON_BCD
            || methode.type() == PRMethodeResolution.NEWTON_RAPHSON_LMG)
          schema=
            new PRSchemaResolution(
              PRSchemaResolution.EULER,
              groupesPTCoefSchema[i]);
        // Sch�ma de Kawahara
        else if (
          methode.type() == PRMethodeResolution.SELECTED_LUMPING
            || methode.type() == PRMethodeResolution.SELECTED_LUMPING_BCD)
          schema=
            new PRSchemaResolution(
              PRSchemaResolution.KAWAHARA,
              groupesPTCoefSchema[i]);
        // Groupe de pas
        grpPTs[i]=
          new PRGroupePT(temps, schema, methode, groupesPTfrequence[i]);
      }
      modeleCalcul().groupesPT(grpPTs);
    }
    //--------------------------------------------------------------------------
    // Normales
    //--------------------------------------------------------------------------
    {
      PRNormale[] normales= new PRNormale[nbNoeudsBord];
      //      mdlPrp_.noeudsNormales.clear();
      GrNoeud support;
      //      PRNormale normale;
      for (int i= 0; i < nbNoeudsBord; i++) {
        //        support = (GrNoeud) numToNoeud.get(new Integer(normalesNoeuds[i]));
        support= noeuds[normalesNoeuds[i]];
        normales[i]= new PRNormale(support, normalesValeurs[i]);
        //        mdlPrp_.noeudsNormales.put(support,normales[i]);
      }
      mdlPrp_.normales(normales);
    }
    //--------------------------------------------------------------------------
    // Coefficients de pond�ration + Indices d'impression
    //--------------------------------------------------------------------------
    {
      Object[] params= new Object[coefPond.length + impression.length];
      // Coefficients
      for (int i= 0; i < coefPond.length; i++)
        params[i]= new Double(coefPond[i]);
      //      System.arraycopy(coefPond,0,params,0,coefPond.length);
      // Indices d'impression (a la suite).
      for (int i= 0; i < impression.length; i++)
        params[coefPond.length + i]= new Double(impression[i] - 1);
      modeleCalcul().parametresCalcul(params);
    }
    //    {
    //       boolean[] bImpression = new boolean[9];
    //       for (int i=0; i<bImpression.length; i++) bImpression[i] = (impression[i] == 2);
    //       modeleCalcul().impressions(bImpression);
    //    }
    //
    //    //--------------------------------------------------------------------------
    //    // Coefficients de pond�ration des termes de la matrice
    //    //--------------------------------------------------------------------------
    //
    //    {
    //       modeleCalcul().coefficientsCalcul(coefPond);
    //    }
    //    return probleme;

    //--------------------------------------------------------------------------
    // Groupes d'�l�ments pour multiplans
    //--------------------------------------------------------------------------
    getGroupes().clear();
    for (int i=0; i<numGrpElements.length; i++) {
      Object[] els=new Object[numElements[i].length];
      for (int j=0; j<numElements[i].length; j++) {
        els[j]=(GrElement)num2El.get(new Integer(numElements[i][j]-1));
      }
      RefluxGroupe gp=new RefluxGroupe(els);
      gp.setNum(numGrpElements[i]);
      addGroupe(gp);
    }

    //--------------------------------------------------------------------------
    // Multiplans
    //--------------------------------------------------------------------------
    {
      getPlans().clear();
      getSegments().clear();
      for (int i=0; i<numMultiplans.length; i++) {
        RefluxSegment[] sgs=new RefluxSegment[numMultiplansLignes[i].length];
        for (int j=0; j<numMultiplansLignes[i].length; j++) {
          sgs[j]=new RefluxSegment(p1MultiplansLignes[i][j],p2MultiplansLignes[i][j]);
          sgs[j].setNum(numMultiplansLignes[i][j]);
          sgs[j].setHAutomatique(autoMultiplansLignes[i][j]==1);
          sgs[j].setHauteur(valMultiplansLignes[i][j]);
          addSegment(sgs[j]);
        }
        RefluxPlan pl=new RefluxPlan(sgs);
        pl.setNum(numMultiplans[i]);
        for (int j=0; j<groupes_.size(); j++) {
          RefluxGroupe gp=(RefluxGroupe)groupes_.get(j);
          if (gp.getNum()==numMultiplansGrp[i]) {
            pl.setGroupe(gp);
            break;
          }
        }
        addPlan(pl);
      }

      // Mise a jour des plans de d�pendance.
      Vector plans=getPlans();
      for (int i=0; i<plans.size(); i++) {
        RefluxPlan pl=(RefluxPlan)plans.get(i);
        if (pl.isHAutomatiqueAmont()) {
          int numpl=(int)pl.getSegments()[0].getHauteur();
          for (int j=0; j<plans.size(); j++) {
            RefluxPlan pldep=(RefluxPlan)plans.get(j);
            if (pldep.getNum()==numpl) {
              pl.setPlanAmont(pldep);
              break;
            }
          }
        }
        if (pl.isHAutomatiqueAval()) {
          int numpl=(int)pl.getSegments()[pl.getSegments().length-1].getHauteur();
          for (int j=0; j<plans.size(); j++) {
            RefluxPlan pldep=(RefluxPlan)plans.get(j);
            if (pldep.getNum()==numpl) {
              pl.setPlanAval(pldep);
              break;
            }
          }
        }
      }
    }

    //--------------------------------------------------------------------------
    // Solutions initiales
    //--------------------------------------------------------------------------
    {
      PRSolutionsInitiales[] sis= new PRSolutionsInitiales[valeursSI.length];
      // Version > 5.11
      if (valeursSI[0].length == 3) {
        for (int i= 0;
          i < valeursSI.length;
          i++) //         sis[i]=new PRSolutionsInitiales(noeuds[i],valeursSI[i][0],
          //                                         valeursSI[i][1],valeursSI[i][2]);
          sis[i]= new PRSolutionsInitiales(noeuds[i], valeursSI[i]);
      }

      // Version <= 5.11 => Calcul des vitesses approximatives vx et vy � partir
      // des ht lues et du multiplan de r�f�rence
      else {
        // hauteurs lues
        double[] hts = new double[valeursSI.length];
        for (int i=0; i<valeursSI.length; i++) hts[i]=valeursSI[i][0];
        // Noeuds de transits pour les ht lues.
        boolean[] nTrs=getNoeudsTransit(hts);
        // Calcul des vitesses pour le seul multiplan
        RefluxPlan pl=(RefluxPlan)getPlans().get(0);
        // Le seul indice de segment est le segment 0 pour tous les noeuds.
        int iseg=0;
//        // Calcul de la pente du plan de r�f�rence suivant x et y.
//        double[] s= modeleCalcul().getPenteXYPlanSI();
        // Vitesses approximatives.
//        double[][] vxy= calculVitesses(hts, nTrs, s[0], s[1]);
        for (int i=0; i<valeursSI.length; i++) {
          double[] vxy=pl.calculeVitesses(iseg, hts[i], noeuds[i].point_.x_,
                                          noeuds[i].point_.y_, nTrs[i]);
          sis[i]=new PRSolutionsInitiales(noeuds[i],new double[]{vxy[0],vxy[1],hts[i]});
        }
      }
      modeleCalcul().solutionsInitiales(sis);
    }
  }
  /**
   * Lecture d'un maillage depuis les fichiers noeuds, bathy et �l�ments de
   * maillage.
   * @param _fichier Nom du fichier maillage. Le maillage est contenu
   *                 dans les _fichier.cor, _fichier.bth et _fichier.ele
   * @exception FileNotFoundException Un fichier de maillage n'est pas trouv�
   * @exception IOException Une erreur de lecture s'est produite
   * @return L'objet maillage
   */
  public static RefluxMaillage lireMaillage(String _fichier)
    throws IOException {
    RefluxMaillage r= null;
    try {
      GrMaillageElement mail= FudaaMaillageCORELEBTH.lire(new File(_fichier));
      r= new RefluxMaillage(mail.elements(), mail.noeuds());
    } catch (FileNotFoundException _exc) {
      throw new FileNotFoundException(
        "Erreur d'ouverture de " + _exc.getMessage());
    } catch (IOException _exc) {
      throw new IOException("Erreur de lecture sur " + _exc.getMessage());
    }
    // Controle des �lements lus. Ils doivent tous etre T6.
    GrElement[] els= r.elements();
    for (int i= 0; i < els.length; i++)
      if (els[i].type_ != GrElement.T6)
        throw new IOException("Seuls les �l�ments T6 sont autoris�s pour ce type de projet");
    return r;
  }
  /**
   * Lecture des r�sultats
   */
  private void readResults(String _racPath) throws IOException {
    SResultatsReflux sres=
      DResultatsReflux.litSurFichiers(
        new File(_racPath + ".sov"),
        maillage().noeuds().length);
    PRResultats res= new PRResultats();
    for (int i= 0; i < sres.etapes.length; i++) {
      res.addStep(HY2dInterfaceCalcul.fromStruct(sres.etapes[i]));
    }
    resultats(res);
  }
  /**
   * Ecriture des r�sultats sur le fichier de racine d�sign�e.
   * @param _racPath La racine du fichier avec le path.
   */
  private void writeResults(String _racPath) throws IOException {
    PRResultats res= resultats();
    SResultatsEtapeReflux[] steps= new SResultatsEtapeReflux[res.getNbSteps()];
    for (int i= 0; i < steps.length; i++) {
      steps[i]= HY2dInterfaceCalcul.toStruct(res.getStep(i));
    }
    DResultatsReflux.ecritSurFichiers(
      new File(_racPath + ".sov"),
      new SResultatsReflux(steps));
  }
  /**
   * Lecture des informations contenues dans le fichier .pre de nom specifie et
   * stockage
   * @param _nomFichier Nom du fichier a lire
   *
   * @exception FileNotFoundException Le fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   */
  private void lireFichierPRE(String _nomFichier)
    throws FileNotFoundException, IOException {
    int[] fmt;
    // Nombre d'�l�ments du maillage
    int nbElements= maillage().elements().length;
    // Nombre de noeuds du maillage
    int nbNoeuds= maillage().noeuds().length;
    String version;
    // Nombre de noeuds sur les contours
    {
      nbNoeudsBord= 0;
      GrNoeud[][] noeudsContours= maillage().noeudsContours();
      for (int i= 0; i < noeudsContours.length; i++)
        nbNoeudsBord += noeudsContours[i].length;
    }
    FortranReader file= null;
    try {
      // Ouverture du fichier
      file= new FortranReader(new FileReader(_nomFichier));
      file.setBlankZero(true);
      // Num�ro de version (seules les versions > 4.0 sont autoris�es)
      fmt= new int[] { 12, 10 };
      file.readFields(fmt);
      version= file.stringField(1);
      System.out.println(_nomFichier + " version : " + version);
      if (version.compareTo("4.0") < 0) {
        throw new Exception(
          "Erreur de lecture sur "
            + _nomFichier
            + "\nLe format du fichier est de version"
            + file.stringField(1)
            + ".\nSeules les versions > � 4.0 sont autoris�es");
      }
      // Type de probleme (1 : Courantologie et 2 : Courantologie+LMG autoris�s)
      fmt= new int[] { 5 };
      file.readFields(fmt);
      numeroTypeProbleme= file.intField(0);
      if (numeroTypeProbleme != 1 && numeroTypeProbleme != 2) {
        throw new Exception(
          "Erreur de lecture sur "
            + _nomFichier
            + "\nLe probl�me n'est pas de type courantologique.");
      }
      if (version.compareTo("5.15")<0) {
        // Commentaire
        file.readFields();
        // Points du plan de solutions initiales
        fmt= new int[] { 15, 15, 15 };
        for (int i= 0; i < 3; i++) {
          file.readFields(fmt);
          for (int j= 0; j < 3; j++)
            pointsSI[i][j]= file.doubleField(j);
        }

        // 1 groupe pour tous les �lements, 1 multiplan recalcul�.

        numGrpElements=new int[]{1};
        numElements=new int[1][nbElements];
        for (int i=0; i<nbElements; i++) numElements[0][i]=i+1;

        numMultiplans=new int[]{1};
        numMultiplansGrp=new int[]{1};
        numMultiplansLignes=new int[][]{{1,2}};
        p1MultiplansLignes=new double[1][2][2];
        p2MultiplansLignes=new double[1][2][2];
        autoMultiplansLignes=new int[][]{{0,0}};
        valMultiplansLignes=new double[1][2];

        // Calcul des points du plan
        int imil=-1;
        int imax=-1;
        int imin=-1;
        boolean bcal=false;
        if (pointsSI[0][2]==pointsSI[1][2]) { imil=2; imin=0; imax=1; }
        if (pointsSI[1][2]==pointsSI[2][2]) { imil=0; imin=1; imax=2; }
        if (pointsSI[2][2]==pointsSI[0][2]) { imil=1; imin=2; imax=0; }
        if (pointsSI[0][2]>pointsSI[1][2] && pointsSI[1][2]>pointsSI[2][2]) { imil=1; imin=2; imax=0; bcal=true; }
        if (pointsSI[1][2]>pointsSI[2][2] && pointsSI[2][2]>pointsSI[0][2]) { imil=2; imin=0; imax=1; bcal=true; }
        if (pointsSI[2][2]>pointsSI[0][2] && pointsSI[0][2]>pointsSI[1][2]) { imil=0; imin=1; imax=2; bcal=true; }
        if (pointsSI[0][2]<pointsSI[1][2] && pointsSI[1][2]<pointsSI[2][2]) { imil=1; imin=0; imax=2; bcal=true; }
        if (pointsSI[1][2]<pointsSI[2][2] && pointsSI[2][2]<pointsSI[0][2]) { imil=2; imin=1; imax=0; bcal=true; }
        if (pointsSI[2][2]<pointsSI[0][2] && pointsSI[0][2]<pointsSI[1][2]) { imil=0; imin=2; imax=1; bcal=true; }

        if (bcal) { // Calcul de la position du point �quivalent au z imil.
          double x=pointsSI[imin][0]+(pointsSI[imil][2]-pointsSI[imin][2])*(pointsSI[imax][0]-pointsSI[imin][0])/(pointsSI[imax][2]-pointsSI[imin][2]);
          double y=pointsSI[imin][1]+(pointsSI[imil][2]-pointsSI[imin][2])*(pointsSI[imax][1]-pointsSI[imin][1])/(pointsSI[imax][2]-pointsSI[imin][2]);
          double z=pointsSI[imil][2];
          p1MultiplansLignes[0][0][0]=pointsSI[imil][0];
          p1MultiplansLignes[0][0][1]=pointsSI[imil][1];
          p2MultiplansLignes[0][0][0]=x;
          p2MultiplansLignes[0][0][1]=y;
          valMultiplansLignes[0][0]=z;

          if (z>(pointsSI[imin][2]+pointsSI[imax][2])/2) imil=imin;
          else                                           imil=imax;
        }
        else {
          p1MultiplansLignes[0][0][0]=pointsSI[imin][0];
          p1MultiplansLignes[0][0][1]=pointsSI[imin][1];
          p2MultiplansLignes[0][0][0]=pointsSI[imax][0];
          p2MultiplansLignes[0][0][1]=pointsSI[imax][1];
          valMultiplansLignes[0][0]=pointsSI[imin][2];
        }

        // Les 2 points formant la 2ieme ligne sont confondus.
        p1MultiplansLignes[0][1][0]=pointsSI[imil][0];
        p1MultiplansLignes[0][1][1]=pointsSI[imil][1];
        p2MultiplansLignes[0][1][0]=pointsSI[imil][0];
        p2MultiplansLignes[0][1][1]=pointsSI[imil][1];
        valMultiplansLignes[0][1]=pointsSI[imil][2];
      }

      // Commentaire
      file.readFields();
      // Valeurs des solutions initiales (VX,VY,HT) sur les noeuds
      boolean buvh= !(version.compareTo("5.12") < 0);
      valeursSI= new double[nbNoeuds][buvh ? 3 : 1];
      fmt= new int[] { 5, 15, 15, 15 };
      for (int i= 0; i < nbNoeuds; i++) {
        file.readFields(fmt);
        for (int j= 0; j < valeursSI[i].length; j++)
          valeursSI[i][j]= file.doubleField(1 + j);
      }
      // Commentaire
      file.readFields();
      // Nombre de d�finitions sur les bords
      fmt= new int[] { 5 };
      file.readFields(fmt);
      nbDefBords= file.intField(0);
      // D�finitions de bords (<type temporel>,<valeur/num�ro de courbe associ�e>)
      defBordsTypeTemp= new int[nbDefBords];
      defBordsValeur= new double[nbDefBords];
      fmt= new int[] { 5, 12 };
      for (int i= 0; i < nbDefBords; i++) {
        file.readFields(fmt);
        defBordsTypeTemp[i]= file.intField(0);
        defBordsValeur[i]= file.doubleField(1);
      }
      // Commentaire
      file.readFields();
      // Caract�ristiques sur les noeuds de bord
      carBords= new int[nbNoeudsBord][5];
      natBords= new int[nbNoeudsBord];
      carBordsNoeuds= new int[nbNoeudsBord];
      fmt= new int[] { 5, 5, 5, 5, 5, 5, 5 };
      for (int i= 0; i < nbNoeudsBord; i++) {
        file.readFields(fmt);
        // Num�ros des noeuds
        carBordsNoeuds[i]= file.intField(0) - 1;
        // Nature des noeuds
        natBords[i]= file.intField(1);
        // Caract�ristiques des noeuds
        for (int j= 0; j < 5; j++)
          carBords[i][j]= file.intField(j + 2);
      }
      // Commentaire
      file.readFields();
      // Nombre de d�finitions sur le fond
      fmt= new int[] { 5 };
      file.readFields(fmt);
      nbDefFond= file.intField(0);
      // D�finitions de fond (<type temporel>,<valeur/num�ro de courbe associ�e>)
      defFondTypeTemp= new int[nbDefFond];
      defFondValeur= new double[nbDefFond];
      fmt= new int[] { 5, 12 };
      for (int i= 0; i < nbDefFond; i++) {
        file.readFields(fmt);
        defFondTypeTemp[i]= file.intField(0);
        defFondValeur[i]= file.doubleField(1);
      }
      // Commentaire
      file.readFields();
      // Caract�ristiques sur les �l�ments de fond
      carFond= new int[nbElements][numeroTypeProbleme + 2];
      fmt= new int[] { 5, 5, 5, 5, 5 };
      for (int j= 0; j < nbElements; j++) {
        file.readFields(fmt);
        for (int i= 0; i < numeroTypeProbleme + 2; i++)
          carFond[j][i]= file.intField(i + 1);
      }
      // Commentaire
      file.readFields();
      // Description de l'�chelle de temps (on saute)
      file.readFields();
      // Nombre de groupes de pas de temps
      fmt= new int[] { 5 };
      file.readFields(fmt);
      nbGroupesPT= file.intField(0);
      // Boucle sur les groupes de pas de temps
      groupesPTDebut= new double[nbGroupesPT];
      groupesPTFin= new double[nbGroupesPT];
      groupesPTDT= new double[nbGroupesPT];
      groupesPTCoefSchema= new double[nbGroupesPT];
      groupesPTMethode= new int[nbGroupesPT];
      groupesPTCoefMethode= new double[nbGroupesPT][];
      groupesPTfrequence= new int[nbGroupesPT];
      for (int i= 0; i < nbGroupesPT; i++) {
        fmt= new int[] { 15, 15, 15, 15, 5 };
        file.readFields(fmt);
        groupesPTDebut[i]= file.doubleField(0);
        groupesPTFin[i]= file.doubleField(1);
        groupesPTDT[i]= file.doubleField(2);
        groupesPTCoefSchema[i]= file.doubleField(3);
        groupesPTMethode[i]= file.intField(4) - 1;
        // Nombre de coefficients par m�thode par type de probleme
        int[][] nbCoefMethode;
        if (version.compareTo("5.10") < 0)
          nbCoefMethode= new int[][] { { 0, 4, 4, 4, 4 }, { 4 }};
        else
          nbCoefMethode= new int[][] { { 0, 3, 5, 0, 2 }, { 3 }};

        // Coefficients des m�thodes
        groupesPTCoefMethode[i]=
          new double[nbCoefMethode[numeroTypeProbleme - 1][groupesPTMethode[i]]];
        int cpt= 0;
        fmt= new int[] { 15, 15, 15, 15 };
        COEFS : while (true) {
          file.readFields(fmt);
          for (int k= 0; k < 4; k++) {
            if (cpt == groupesPTCoefMethode[i].length) {
              groupesPTfrequence[i]= (int)file.doubleField(k);
              break COEFS;
            }
            groupesPTCoefMethode[i][cpt]= file.doubleField(k);
            cpt++;
          }
        }

        if (version.compareTo("5.10") < 0) {
          if (numeroTypeProbleme == 1) {
            if (groupesPTMethode[i] == 0) // LINEAIRE
              groupesPTCoefMethode[i]= new double[] {};
            else if (groupesPTMethode[i] == 1) // NEWTON
              groupesPTCoefMethode[i]=
                new double[] {
                  groupesPTCoefMethode[i][0],
                  groupesPTCoefMethode[i][1],
                  groupesPTCoefMethode[i][3] };
            else if (groupesPTMethode[i] == 2) // NEWTON_BCD
              groupesPTCoefMethode[i]=
                new double[] {
                  groupesPTCoefMethode[i][0],
                  groupesPTCoefMethode[i][1],
                  groupesPTCoefMethode[i][2],
                  groupesPTCoefMethode[i][3],
                  1000.0 };
            else if (groupesPTMethode[i] == 3) // SELUMP
              groupesPTCoefMethode[i]= new double[] {};
            else if (groupesPTMethode[i] == 4) // SELUMP_BCD
              groupesPTCoefMethode[i]=
                new double[] { groupesPTCoefMethode[i][2], 1000.0 };
          }
          else {
            if (groupesPTMethode[i] == 0) // NEWTON_LMG
              groupesPTCoefMethode[i]=
                new double[] {
                  groupesPTCoefMethode[i][0],
                  groupesPTCoefMethode[i][1],
                  groupesPTCoefMethode[i][3] };
          }
        }
      }

      // Commentaire
      file.readFields();

      // Normales sur les noeuds de bord
      normalesNoeuds= new int[nbNoeudsBord];
      normalesValeurs= new double[nbNoeudsBord];
      fmt= new int[] { 5, 15 };
      for (int i= 0; i < nbNoeudsBord; i++) {
        file.readFields(fmt);
        normalesNoeuds[i]= file.intField(0) - 1;
        // Valeurs en degr�s
        normalesValeurs[i]= file.doubleField(1) * 180. / Math.PI;
      }

      // Commentaire
      file.readFields();
      // Indices d'impression
      fmt= new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5 };
      file.readFields(fmt);
      for (int i= 0; i < impression.length; i++) {
        impression[i]= file.intField(i);
      }

      // Commentaire
      file.readFields();
      // Coefficients de pond�ration
      fmt= new int[] { 15, 15, 15, 15 };
      int cpt= 0;
      while (cpt < 9) {
        file.readFields(fmt);
        for (int k= 0; k < 4 && cpt < 9; k++) {
          coefPond[cpt]= file.doubleField(k);
          cpt++;
        }
      }

      // Avant cette version, pas de groupes d'�l�ments et pas de multiplans.
//      if (version.compareTo("5.15")<0) {
//        numGrpElements=new int[0];  // Les autres variables n'ont pas besoin d'�tre initialis�es.
//        numMultiplans=new int[0];
//      }
      if (version.compareTo("5.15")>=0) {
        // Commentaire
        file.readFields();
        // Nombre de groupes d'�l�ments
        fmt= new int[]{5};
        file.readFields(fmt);
        numGrpElements=new int[file.intField(0)];
        numElements=new int[numGrpElements.length][];
        // Num�ros des �l�ments (16 par ligne)
        for (int i=0; i<numGrpElements.length; i++) {
          fmt=new int[]{5,5};
          file.readFields(fmt);
          numGrpElements[i]=file.intField(0);
          numElements[i]=new int[file.intField(1)];
          int j=0;
          fmt= new int[] {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
          while (j<numElements[i].length) {
            if (j%16==0) file.readFields(fmt);
            numElements[i][j]=file.intField(j%16);
            j++;
          }
        }

        // Commentaire
        file.readFields();
        // Nombre de multiplans
        fmt=new int[]{5};
        file.readFields(fmt);
        numMultiplans=new int[file.intField(0)];
        numMultiplansGrp=new int[file.intField(0)];
        numMultiplansLignes=new int[file.intField(0)][];
        p1MultiplansLignes=new double[file.intField(0)][][];
        p2MultiplansLignes=new double[file.intField(0)][][];
        autoMultiplansLignes=new int[file.intField(0)][];
        valMultiplansLignes=new double[file.intField(0)][];
        for (int i=0; i<numMultiplans.length; i++) {
          fmt=new int[]{5,5,5};
          file.readFields(fmt);
          numMultiplans[i]=file.intField(0);
          numMultiplansLignes[i]=new int[file.intField(1)];
          p1MultiplansLignes[i]=new double[file.intField(1)][2];
          p2MultiplansLignes[i]=new double[file.intField(1)][2];
          autoMultiplansLignes[i]=new int[file.intField(1)];
          valMultiplansLignes[i]=new double[file.intField(1)];
          numMultiplansGrp[i]=file.intField(2);
          for (int j=0; j<numMultiplansLignes[i].length; j++) {
            fmt=new int[]{5,15,15,15,15,5,15};
            file.readFields(fmt);
            numMultiplansLignes[i][j]=file.intField(0);
            p1MultiplansLignes[i][j][0]=file.doubleField(1);
            p1MultiplansLignes[i][j][1]=file.doubleField(2);
            p2MultiplansLignes[i][j][0]=file.doubleField(3);
            p2MultiplansLignes[i][j][1]=file.doubleField(4);
            autoMultiplansLignes[i][j]=file.intField(5);
            valMultiplansLignes[i][j]=file.doubleField(6);
          }
        }
      }
    }
    catch (IOException _exc) {
      throw new IOException(
        "Erreur de lecture sur " + _nomFichier + " ligne " + file.getLineNumber());
    }
    catch (NumberFormatException _exc) {
      _exc.printStackTrace();
      throw new IOException(
        "Erreur de lecture sur " + _nomFichier + " ligne " + file.getLineNumber());
    }
    catch (Exception _exc) {
      throw new IOException(_exc.getMessage());
    }
    finally {
      // Fermeture du fichier
      if (file != null)
        file.close();
    }
  }

/**
 * Lecture des informations contenues dans le fichier .crb des courbes transitoires
 * de nom specifie et stockage dans les objets IEvolutionSerieIrreguliere
 *
 * @param _nomFichier Nom du fichier a lire
 * @exception FileNotFoundException Le fichier est introuvable
 * @exception IOException Une erreur de lecture s'est produite
 */
private void lireFichierCRB(String _nomFichier)
  throws FileNotFoundException, IOException {
  int[] fmt;
  String nomCourbe; // Nom de courbe
  int nbPoints; // Nombre de points de courbe
  long[] x; // Abscisses de la courbe
  double[] y; // Ordonn�es de la courbe
  IEvolutionSerieIrreguliere evolution; // Evolution en cours de cr�ation
  IEvolutionSerieIrreguliere[] evolutions; // Evolutions lues
  Vector evolTmp; // Vecteur temporaire des �volutions
  FortranReader file= null;
  try {
    // Ouverture du fichier
    file= new FortranReader(new FileReader(_nomFichier));
    // Num�ro de version (seules les versions > 4.0 sont autoris�es)
    fmt= new int[] { 12, 10 };
    file.readFields(fmt);
    if (file.stringField(1).compareTo("4.0") < 0) {
      throw new Exception(
        "Erreur de lecture sur "
          + _nomFichier
          + "\nLe format du fichier est de version"
          + file.stringField(1)
          + ".\nSeules les versions > � 4.0 sont autoris�es");
    }
    // Boucle jusqu'� fin de fichier
    evolTmp= new Vector();
    while (file.ready()) {
      fmt= new int[] { 20, 1, 5 };
      try {
        file.readFields(fmt);
      } catch (EOFException exc) {
        break;
      }
      if (file.stringField(0).substring(0, 1).equals("*"))
        continue;
      // Nom de la courbe
      nomCourbe= file.stringField(0);
      // Nombre de points de la courbe
      nbPoints= file.intField(2);
      x= new long[nbPoints];
      y= new double[nbPoints];
      // Boucle sur les points
      fmt= new int[] { 10, 1, 10 };
      for (int i= 0; i < nbPoints;) {
        file.readFields(fmt);
        if (file.stringField(0).substring(0, 1).equals("*"))
          continue;
        x[i]= (long)file.doubleField(0);
        y[i]= file.doubleField(2);
        i++;
      }
      // Cr�ation de l'�volution s�rie irr�guli�re
      evolution=UsineLib.findUsine().creeMesureEvolutionSerieIrreguliere();
/*        IEvolutionSerieIrreguliereHelper.narrow(
          new DEvolutionSerieIrreguliere().tie());*/
      ISegmentationTemps dst= UsineLib.findUsine().creePlanificationSegmentationTemps();
      dst.modifieInstants(x);
      evolution.instants(ISegmentationTempsHelper.narrow(dst));
      evolution.serie(y);
      associe(evolution, nomCourbe);
      evolTmp.addElement(evolution);
    }
  } catch (FileNotFoundException exc) {
    throw new FileNotFoundException(_nomFichier);
  } catch (IOException exc) {
    throw new IOException("Erreur de lecture sur " + _nomFichier);
  } catch (NumberFormatException exc) {
    throw new IOException("Erreur de lecture sur " + _nomFichier);
  } catch (Exception _exc) {
    throw new IOException(_exc.getMessage());
  } finally {
    // Fermeture du fichier
    if (file != null)
      file.close();
  }
  evolutions= new IEvolutionSerieIrreguliere[evolTmp.size()];
  evolTmp.copyInto(evolutions);
  evolutions(evolutions);
}
/**
 * Enregistrement du projet sur le fichier .pre de nom specifie.
 * La nouvelle version de Fudaa-Reflux garde (provisoirement)
 * le format de la version 4.0. Cependant, un fichier de version 5.0 ne
 * pourra pas �tre relu par une version 4.0
 *
 * @param _nomFichier Nom du fichier de sauvegarde.
 * @exception FileNotFoundException Le fichier est introuvable
 * @exception IOException Une erreur de lecture s'est produite
 */
public void enregistrer(String _nomFichier) throws IOException {
  prjToPRE();
  FortranWriter file; // Fichier d'�criture
  int fmt[];
  // Nombre d'�l�ments du maillage
  int nbElements= maillage().elements().length;
  // Nombre de noeuds du maillage
  int nbNoeuds= maillage().noeuds().length;
  // Nombre de coefficients par m�thode par type de probleme
  //    final int nbCoefMethode[][]={{0,4,4,4,4},{4}};
  // Nombre de noeuds sur les contours
  {
    GrNoeud[][] noeudsContours= maillage().noeudsContours();
    nbNoeudsBord= 0;
    for (int i= 0; i < noeudsContours.length; i++)
      nbNoeudsBord += noeudsContours[i].length;
  }
  file= null;
  try {
    // Ouverture du fichier
    file= new FortranWriter(new FileWriter(_nomFichier));
    // Num�ro de version courante
    String version= RefluxImplementation.informationsSoftware().version;
    fmt= new int[] { 12, version.length()};
    file.stringField(0, "*!$ Version ");
    file.stringField(1, version);
    file.writeFields(fmt);
    // Type de probleme (1 : Courantologie 2D, 2 : Courantologie 2D+LMG)
    fmt= new int[] { 5 };
    file.intField(0, numeroTypeProbleme);
    file.writeFields(fmt);
    // Commentaire
//    fmt= new int[] { 31 };
//    file.stringField(0, "* POINTS DE SOLUTIONS INITIALES");
//    file.writeFields(fmt);
//    // Points de solutions initiales
//    fmt= new int[] { 15, 15, 15 };
//    for (int i= 0; i < 3; i++) {
//      for (int j= 0; j < 3; j++)
//        file.floatField(j, pointsSI[i][j]);
//      file.writeFields(fmt);
//    }
    // Commentaire
    fmt= new int[] { 42 };
    file.stringField(0, "* SOL. INITIALES (VX,VY,HT) SUR LES NOEUDS");
    file.writeFields(fmt);
    // Valeurs des solutions initiales (VX,VY,HT) sur les noeuds
    fmt= new int[] { 5, 15, 15, 15 };
    for (int i= 0; i < nbNoeuds; i++) {
      file.intField(0, i + 1);
      for (int j= 0; j < 3; j++)
        file.floatField(1 + j, valeursSI[i][j]);
      file.writeFields(fmt);
    }
    // Commentaire
    fmt= new int[] { 23 };
    file.stringField(0, "* DEFINITIONS DES BORDS");
    file.writeFields(fmt);
    // Nombre de d�finitions sur les bords
    fmt= new int[] { 5 };
    file.intField(0, nbDefBords);
    file.writeFields(fmt);
    // D�finitions de bords (<type temporel>,<valeur/num�ro de courbe associ�e>)
    fmt= new int[] { 5, 12 };
    for (int i= 0; i < nbDefBords; i++) {
      file.intField(0, defBordsTypeTemp[i]);
      if (defBordsTypeTemp[i] == 1)
        file.floatField(1, defBordsValeur[i]);
      else
        file.intField(1, (int)defBordsValeur[i]);
      file.writeFields(fmt);
    }
    // Commentaire
    fmt= new int[] { 15 };
    file.stringField(0, "* NOEUD DE BORD");
    file.writeFields(fmt);
    // Caract�ristiques sur les noeuds de bord
    fmt= new int[] { 5, 5, 5, 5, 5, 5, 5 };
    for (int i= 0; i < nbNoeudsBord; i++) {
      // Num�ros des noeuds
      file.intField(0, carBordsNoeuds[i]);
      // Nature des noeuds
      file.intField(1, natBords[i]);
      // Caract�ristiques des noeuds
      for (int j= 0; j < 5; j++)
        file.intField(j + 2, carBords[i][j]);
      file.writeFields(fmt);
    }
    // Commentaire
    fmt= new int[] { 23 };
    file.stringField(0, "* DEFINITIONS DES FONDS");
    file.writeFields(fmt);
    // Nombre de d�finitions sur le fond
    fmt= new int[] { 5 };
    file.intField(0, nbDefFond);
    file.writeFields(fmt);
    // D�finitions de fond (<type temporel>,<valeur/num�ro de courbe associ�e>)
    fmt= new int[] { 5, 12 };
    for (int i= 0; i < nbDefFond; i++) {
      file.intField(0, defFondTypeTemp[i]);
      if (defFondTypeTemp[i] == 1)
        file.floatField(1, defFondValeur[i]);
      else
        file.intField(1, (int)defFondValeur[i]);
      file.writeFields(fmt);
    }
    // Commentaire
    fmt= new int[] { 17 };
    file.stringField(0, "* ELEMENT DU FOND");
    file.writeFields(fmt);
    // Caract�ristiques sur les �l�ments de fond
    fmt= new int[] { 5, 5, 5, 5, 5 };
    for (int j= 0; j < nbElements; j++) {
      file.intField(0, j + 1);
      for (int i= 0; i < numeroTypeProbleme + 2; i++)
        file.intField(i + 1, carFond[j][i]);
      file.writeFields(fmt);
    }
    // Commentaire
    fmt= new int[] { 21 };
    file.stringField(0, "* INTERVALLE DE TEMPS");
    file.writeFields(fmt);
    // Description de l'�chelle de temps (Valeurs par d�faut, inutile)
    fmt= new int[] { 15, 15, 15 };
    file.floatField(0, 0.);
    file.floatField(1, 1000.);
    file.floatField(2, 100.);
    file.writeFields(fmt);
    // Nombre de groupes de pas de temps
    fmt= new int[] { 5 };
    file.intField(0, nbGroupesPT);
    file.writeFields(fmt);
    // Boucle sur les groupes de pas de temps
    for (int i= 0; i < nbGroupesPT; i++) {
      fmt= new int[] { 15, 15, 15, 15, 5 };
      file.floatField(0, groupesPTDebut[i]);
      file.floatField(1, groupesPTFin[i]);
      file.floatField(2, groupesPTDT[i]);
      file.floatField(3, groupesPTCoefSchema[i]);
      file.intField(4, groupesPTMethode[i] + 1);
      file.writeFields(fmt);
      // Coefficients des m�thodes + fr�quence de stockage
      fmt= new int[] { 15, 15, 15, 15 };
      int cpt= 0;
      for (int j= 0; j < groupesPTCoefMethode[i].length; j++) {
        file.floatField(cpt, groupesPTCoefMethode[i][j]);
        cpt++;
        if (cpt == 4) {
          file.writeFields(fmt);
          cpt= 0;
        }
      }
      file.floatField(cpt, groupesPTfrequence[i]);
      file.writeFields(fmt);
    }
    // Commentaire
    fmt= new int[] { 19 };
    file.stringField(0, "* NORMALE PAR NOEUD");
    file.writeFields(fmt);
    // Normales sur les noeuds de bord
    fmt= new int[] { 5, 15 };
    for (int i= 0; i < nbNoeudsBord; i++) {
      file.intField(0, normalesNoeuds[i]);
      // Valeurs en radian
      file.floatField(1, normalesValeurs[i] * Math.PI / 180.);
      file.writeFields(fmt);
    }
    // Commentaire
    fmt= new int[] { 22 };
    file.stringField(0, "* INDICES D'IMPRESSION");
    file.writeFields(fmt);
    // Indices d'impression
    fmt= new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5 };
    for (int i= 0; i < impression.length; i++)
      file.intField(i, impression[i]);
    file.writeFields(fmt);
    // Commentaire
    fmt= new int[] { 28 };
    file.stringField(0, "* COEFFICIENT DE PONDERATION");
    file.writeFields(fmt);
    // Coefficients de pond�ration
    fmt= new int[] { 15, 15, 15, 15 };
    int cpt= 0;
    while (cpt < 9) {
      for (int k= 0; k < 4 && cpt < 9; k++) {
        file.floatField(k, coefPond[cpt]);
        cpt++;
      }
      file.writeFields(fmt);
    }

    // Commentaire
    fmt=new int[]{49};
    file.stringField(0,"* GROUPES D'ELEMENTS POUR LES SOLUTIONS INITIALES");
    file.writeFields(fmt);
    // <Nombre de groupes>
    fmt=new int[]{5};
    file.intField(0,numGrpElements.length);
    file.writeFields(fmt);
    // Boucle sur les groupes
    for (int i=0; i<numGrpElements.length; i++) {
      // <Num�ro de groupe>,<nombre d'�lements>
      fmt=new int[]{5,5};
      file.intField(0,numGrpElements[i]);
      file.intField(1,numElements[i].length);
      file.writeFields(fmt);

      // <Num�ros des �l�ments (16 par ligne)>
      int j=0;
      fmt=new int[]{5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
      while (j<numElements[i].length) {
        file.intField(j%16,numElements[i][j]);
        if ((j+1)%16==0 || j==numElements[i].length-1) file.writeFields(fmt);
        j++;
      }
    }

    // Commentaire
    fmt=new int[]{41};
    file.stringField(0,"* MULTIPLANS POUR LES SOLUTIONS INITIALES");
    file.writeFields(fmt);
    // <nombre de multiplans>
    fmt=new int[]{5};
    file.intField(0,numMultiplans.length);
    file.writeFields(fmt);

    // Boucle sur les multiplans
    for (int i=0; i<numMultiplans.length; i++) {
      // <numero de multiplan>,<nb lignes>,<num groupe>
      fmt=new int[]{5,5,5};
      file.intField(0,numMultiplans[i]);
      file.intField(1,numMultiplansLignes[i].length);
      file.intField(2,numMultiplansGrp[i]);
      file.writeFields(fmt);

      // Boucle sur chaque segment
      for (int j=0; j<numMultiplansLignes[i].length; j++) {
        // <num segment>,<xo>,<yo>,<xe>,<ye>,<h; 0:Fix�, 1:Auto>,<valeur h ou num plan si auto>
        fmt=new int[]{5,15,15,15,15,5,15};
        file.intField(0,numMultiplansLignes[i][j]);
        file.floatField(1,p1MultiplansLignes[i][j][0]);
        file.floatField(2,p1MultiplansLignes[i][j][1]);
        file.floatField(3,p2MultiplansLignes[i][j][0]);
        file.floatField(4,p2MultiplansLignes[i][j][1]);
        file.intField(5,autoMultiplansLignes[i][j]);
        file.floatField(6,valMultiplansLignes[i][j]);
        file.writeFields(fmt);
      }
    }
  }
  catch (IOException exc) {
    throw new IOException("Erreur d'�criture sur " + _nomFichier);
  } catch (NumberFormatException exc) {
    throw new IOException("Erreur d'�criture sur " + _nomFichier);
  } finally {
    // Fermeture du fichier
    file.close();
  }
  // Ecriture du fichier des solutions
  String racPath= _nomFichier.substring(0, _nomFichier.lastIndexOf(".pre"));
  new File(racPath + ".sov").delete();
  // Suppress old file if there is no results.
  if (hasResultats())
    writeResults(racPath);
}
/**
 * Transfert des informations du projet vers une structure
 * compatible avec le fichier .pre
 *
 * Remarque importante : Dans la version 5.0, le fichier .pre ne diff�re pas
 * structurellement du format 4.0. Cependant, un fichier de version 5.0 ne
 * pourra pas �tre relu sous la version 4.0 car l'ordre d'�criture des noeuds
 * de bord version 4.0 correspond � un ordre bien pr�cis (ordre suivant lequel
 * les bords sont d�termin�s), ordre qui n'est pas respect� par la version 5.0
 */
private void prjToPRE() {
  Hashtable nd2Num= new Hashtable();
  {
    GrNoeud[] nds= maillage().noeuds();
    for (int i= 0; i < nds.length; i++)
      nd2Num.put(nds[i], new Integer(i));
  }
  Hashtable el2Num= new Hashtable();
  {
    GrElement[] els= maillage().elements();
    for (int i= 0; i < els.length; i++)
      el2Num.put(els[i], new Integer(i));
  }
  //--------------------------------------------------------------------------
  // Type du probleme
  //--------------------------------------------------------------------------
  if (type() == PRProjet.VIT_NU_CON)
    numeroTypeProbleme= 1;
  else if (type() == PRProjet.VIT_NU_LMG)
    numeroTypeProbleme= 2;
  //--------------------------------------------------------------------------
  // Solutions initiales
  //--------------------------------------------------------------------------
  {
    PRSolutionsInitiales[] sis= modeleCalcul().solutionsInitiales();
    valeursSI= new double[sis.length][];
    for (int i= 0; i < sis.length; i++)
      valeursSI[i]= sis[i].valeurs();
    //      valeursSI=new double[sis.length][3];
    //      for (int i=0; i<sis.length; i++) {
    //        valeursSI[i][0]=sis[i].u()[0];
    //        valeursSI[i][1]=sis[i].v()[0];
    //        valeursSI[i][2]=sis[i].hauteur();
    //      }
  }
  //--------------------------------------------------------------------------
  // Points d�finissant le plan des conditions initiales
  //--------------------------------------------------------------------------
//  {
//    GrPoint[] pts= modeleCalcul().pointsSI();
//    for (int i= 0; i < pts.length; i++)
//      pointsSI[i]= new double[] { pts[i].x, pts[i].y, pts[i].z };
//  }
  //--------------------------------------------------------------------------
  // D�finitions + caract�ristiques des �l�ments de fond
  //--------------------------------------------------------------------------
  {
    PRPropriete[] pEs= mdlPrp_.proprietesElements();
    IEvolution[] courbes= evolutions();
    int nbElements= maillage().elements().length;
    Object[] supports;
    int nCar;
    Hashtable crb2Num= new Hashtable(courbes.length > 0 ? courbes.length : 1);
    for (int i= 0; i < courbes.length; i++)
      crb2Num.put(courbes[i], new Integer(i + 1));
    nbDefFond= pEs.length;
    defFondTypeTemp= new int[nbDefFond];
    defFondValeur= new double[nbDefFond];
    carFond= new int[nbElements][numeroTypeProbleme + 2];
    for (int i= 0; i < nbElements; i++)
      for (int j= 0; j < numeroTypeProbleme + 2; j++)
        carFond[i][j]= 0;
    // D�termination de la d�finition correspondant � la propri�t�
    for (int i= 0; i < pEs.length; i++) {
      supports= pEs[i].supports().toArray();
      if (pEs[i].evolution() instanceof IEvolutionConstante) {
        defFondTypeTemp[i]= 1;
        defFondValeur[i]= ((IEvolutionConstante)pEs[i].evolution()).constante();
      } else {
        defFondTypeTemp[i]= 2;
        defFondValeur[i]=
          ((Integer)crb2Num.get(pEs[i].evolution())).doubleValue();
      }
      if (numeroTypeProbleme == 1) {
        if (pEs[i].type() == PRPropriete.VISCOSITE)
          nCar= 0;
        else if (pEs[i].type() == PRPropriete.RUGOSITE)
          nCar= 1;
        else
          nCar= 2;
      } else {
        if (pEs[i].type() == PRPropriete.VISCOSITE)
          nCar= 0;
        else if (pEs[i].type() == PRPropriete.RUGOSITE)
          nCar= 1;
        else if (pEs[i].type() == PRPropriete.ALPHA_LONGUEUR_MELANGE)
          nCar= 2;
        else
          nCar= 3;
      }
      // El�ments concern�s par cette propri�t�
      for (int j= 0; j < supports.length; j++) {
        carFond[((Integer)el2Num.get(supports[j])).intValue()][nCar]= i + 1;
      }
    }
  }
  //--------------------------------------------------------------------------
  // D�finitions + natures + caract�ristiques de bord
  //--------------------------------------------------------------------------
  {
    PRPropriete[] pAs= mdlPrp_.proprietesAretes();
    PRConditionLimite[] cLs= conditionsLimites();
    PRSollicitation[] sOs= sollicitations();
    IEvolution[] courbes= evolutions();
    GrNoeud[][] ndsBords= maillage().noeudsContours();
    Vector naturesBords= mdlPrp_.naturesBords();
    Object[] supports;
    GrElement arete;
    PRNature nature;
    int nbNoeudsBordTmp;
    int nCar;
    int index;
    Hashtable nd2Ind;
    nbNoeudsBordTmp= 0;
    for (int i= 0; i < ndsBords.length; i++)
      nbNoeudsBordTmp += ndsBords[i].length;
    Hashtable crb2Num= new Hashtable(courbes.length > 0 ? courbes.length : 1);
    for (int i= 0; i < courbes.length; i++)
      crb2Num.put(courbes[i], new Integer(i + 1));
    nbDefBords= pAs.length + cLs.length + sOs.length;
    defBordsTypeTemp= new int[nbDefBords];
    defBordsValeur= new double[nbDefBords];
    carBords= new int[nbNoeudsBordTmp][5];
    carBordsNoeuds= new int[nbNoeudsBordTmp];
    natBords= new int[nbNoeudsBordTmp];
    for (int i= 0; i < nbNoeudsBordTmp; i++)
      for (int j= 0; j < 5; j++)
        carBords[i][j]= 0;
    // Num�ro des noeuds de bord +
    // noeud->Index de carBordsNoeuds/carBords/natBords
    nd2Ind= new Hashtable();
    nbNoeudsBordTmp= 0;
    for (int i= 0; i < ndsBords.length; i++) {
      for (int j= 0; j < ndsBords[i].length; j++) {
        carBordsNoeuds[nbNoeudsBordTmp]=
          ((Integer)nd2Num.get(ndsBords[i][j])).intValue() + 1;
        nd2Ind.put(ndsBords[i][j], new Integer(nbNoeudsBordTmp));
        nbNoeudsBordTmp++;
      }
    }
    nbDefBords= 0;
    // D�termination de la nature des bords
    for (Enumeration e= naturesBords.elements(); e.hasMoreElements();) {
      nature= (PRNature)e.nextElement();
      arete= (GrElement)nature.supports().get(0);
      index= ((Integer)nd2Ind.get(arete.noeuds_[0])).intValue();
      if (nature.type() == PRNature.BORD_FERME
        || nature.type() == PRNature.BORD_FERME_FROTTEMENT)
        natBords[index]= 2;
      else if (nature.type() == PRNature.BORD_OUVERT_LIBRE)
        natBords[index]= 3;
      else // Tous les autres types ouverts.
        natBords[index]= 1;
    }
    // D�termination de la d�finition correspondant � la propri�t�
    for (int i= 0; i < pAs.length; i++) {
      supports= pAs[i].supports().toArray();
      if (pAs[i].evolution() instanceof IEvolutionConstante) {
        defBordsTypeTemp[nbDefBords]= 1;
        defBordsValeur[nbDefBords]=
          ((IEvolutionConstante)pAs[i].evolution()).constante();
      } else {
        defBordsTypeTemp[nbDefBords]= 2;
        defBordsValeur[nbDefBords]=
          ((Integer)crb2Num.get(pAs[i].evolution())).doubleValue();
      }
      nCar= CA_FROT;
      // Noeuds concern�s par cette propri�t�
      for (int j= 0; j < supports.length; j++) {
        arete= (GrElement)supports[j];
        index= ((Integer)nd2Ind.get(arete.noeuds_[0])).intValue();
        carBords[index][nCar]= nbDefBords + 1;
      }
      nbDefBords++;
    }
    // D�termination de la d�finition correspondant � la condition limite
    for (int i= 0; i < cLs.length; i++) {
      supports= cLs[i].supports().toArray();
      if (cLs[i].evolution() instanceof IEvolutionConstante) {
        defBordsTypeTemp[nbDefBords]= 1;
        defBordsValeur[nbDefBords]=
          ((IEvolutionConstante)cLs[i].evolution()).constante();
      } else {
        defBordsTypeTemp[nbDefBords]= 2;
        defBordsValeur[nbDefBords]=
          ((Integer)crb2Num.get(cLs[i].evolution())).doubleValue();
      }
      if (cLs[i].type() == PRConditionLimite.VITESSE_X)
        nCar= CA_VITX;
      else if (cLs[i].type() == PRConditionLimite.VITESSE_Y)
        nCar= CA_VITY;
      else
        nCar= CA_HAUT;
      // Noeuds concern�s par cette condition limite
      for (int j= 0; j < supports.length; j++) {
        index= ((Integer)nd2Ind.get(supports[j])).intValue();
        carBords[index][nCar]= nbDefBords + 1;
      }
      nbDefBords++;
    }
    // D�termination de la d�finition correspondant � la sollicitation
    for (int i= 0; i < sOs.length; i++) {
      supports= sOs[i].supports().toArray();
      if (sOs[i].evolution() instanceof IEvolutionConstante) {
        defBordsTypeTemp[nbDefBords]= 1;
        defBordsValeur[nbDefBords]=
          ((IEvolutionConstante)sOs[i].evolution()).constante();
      } else {
        defBordsTypeTemp[nbDefBords]= 2;
        defBordsValeur[nbDefBords]=
          ((Integer)crb2Num.get(sOs[i].evolution())).doubleValue();
      }
      nCar= CA_DEBI;
      // Noeuds concern�s par cette sollicitation
      for (int j= 0; j < supports.length; j++) {
        index= ((Integer)nd2Ind.get(supports[j])).intValue();
        carBords[index][nCar]= nbDefBords + 1;
      }
      nbDefBords++;
    }
  }
  //--------------------------------------------------------------------------
  // Groupes de pas de temps
  //--------------------------------------------------------------------------
  {
    PRGroupePT[] gPTs= modeleCalcul().groupesPT();
    PRBoucleTemps temps;
    PRSchemaResolution schema;
    PRMethodeResolution methode;
    double[] coefs;
    nbGroupesPT= gPTs.length;
    groupesPTDebut= new double[nbGroupesPT];
    groupesPTFin= new double[nbGroupesPT];
    groupesPTDT= new double[nbGroupesPT];
    groupesPTCoefSchema= new double[nbGroupesPT];
    groupesPTMethode= new int[nbGroupesPT];
    groupesPTCoefMethode= new double[nbGroupesPT][];
    groupesPTfrequence= new int[nbGroupesPT];
    for (int i= 0; i < gPTs.length; i++) {
      temps= gPTs[i].temps();
      schema= gPTs[i].schema();
      methode= gPTs[i].methode();
      coefs= methode.coefficients();
      groupesPTDebut[i]= temps.debut();
      groupesPTFin[i]= temps.fin();
      groupesPTDT[i]= temps.pas();
      groupesPTCoefSchema[i]= schema.coefficient();
      if (methode.type() == PRMethodeResolution.LINEAIRE)
        groupesPTMethode[i]= 0;
      else if (methode.type() == PRMethodeResolution.NEWTON_RAPHSON)
        groupesPTMethode[i]= 1;
      else if (methode.type() == PRMethodeResolution.NEWTON_RAPHSON_BCD)
        groupesPTMethode[i]= 2;
      else if (methode.type() == PRMethodeResolution.SELECTED_LUMPING)
        groupesPTMethode[i]= 3;
      else if (methode.type() == PRMethodeResolution.SELECTED_LUMPING_BCD)
        groupesPTMethode[i]= 4;
      else if (methode.type() == PRMethodeResolution.NEWTON_RAPHSON_LMG)
        groupesPTMethode[i]= 0;
      groupesPTCoefMethode[i]= coefs;
      groupesPTfrequence[i]= gPTs[i].frequenceStockage();
    }
  }
  //--------------------------------------------------------------------------
  // Normales
  //--------------------------------------------------------------------------
  {
    PRNormale[] normales= mdlPrp_.normales();
    GrNoeud[][] noeuds= maillage().noeudsContours();
    GrNoeud support;
    int nbNoeudsBordTmp;
    int index;
    Hashtable nd2Ind;
    nbNoeudsBordTmp= 0;
    for (int i= 0; i < noeuds.length; i++)
      nbNoeudsBordTmp += noeuds[i].length;
    normalesNoeuds= new int[nbNoeudsBordTmp];
    normalesValeurs= new double[nbNoeudsBordTmp];
    // Num�ro des noeuds de bord + noeud->Index de
    // normalesNoeuds/normalesValeurs
    nd2Ind= new Hashtable();
    nbNoeudsBordTmp= 0;
    for (int i= 0; i < noeuds.length; i++) {
      for (int j= 0; j < noeuds[i].length; j++) {
        normalesNoeuds[nbNoeudsBordTmp]=
          ((Integer)nd2Num.get(noeuds[i][j])).intValue() + 1;
        nd2Ind.put(noeuds[i][j], new Integer(nbNoeudsBordTmp));
        nbNoeudsBordTmp++;
      }
    }
    // Valeurs des normales pour chaque noeud
    for (int i= 0; i < normales.length; i++) {
      support= normales[i].support();
      index= ((Integer)nd2Ind.get(support)).intValue();
      normalesValeurs[index]= normales[i].valeur();
    }
  }
  //--------------------------------------------------------------------------
  // Coefficients de pond�ration + indices d'impression
  //--------------------------------------------------------------------------
  {
    Object[] params= modeleCalcul().parametresCalcul();
    // Coefficients
    for (int i= 0; i < coefPond.length; i++)
      coefPond[i]= ((Double)params[i]).doubleValue();
    //      System.arraycopy(params,0,coefPond,0,coefPond.length);
    // Indices d'impression (a la suite).
    for (int i= 0; i < impression.length; i++)
      impression[i]= ((Double)params[coefPond.length + i]).intValue() + 1;
  }

  //--------------------------------------------------------------------------
  // Groupes d'�l�ments
  //--------------------------------------------------------------------------
  {
    Vector grps=getGroupes();
    numGrpElements=new int[grps.size()];
    numElements=new int[grps.size()][];
    for (int i=0; i<grps.size(); i++) {
      RefluxGroupe gp=(RefluxGroupe)grps.get(i);
      numGrpElements[i]=gp.getNum();
      Object[] els=gp.getObjects();
      numElements[i]=new int[els.length];
      for (int j=0; j<els.length; j++) {
        numElements[i][j]=((Integer)el2Num.get(els[j])).intValue()+1;
      }
    }
  }

  //--------------------------------------------------------------------------
  // Multiplans
  //--------------------------------------------------------------------------
  {
    Vector plans=getPlans();
    numMultiplans=new int[plans.size()];
    numMultiplansGrp=new int[plans.size()];
    numMultiplansLignes=new int[plans.size()][];
    p1MultiplansLignes=new double[plans.size()][][];
    p2MultiplansLignes=new double[plans.size()][][];
    autoMultiplansLignes=new int[plans.size()][];
    valMultiplansLignes=new double[plans.size()][];

    for (int i=0; i<plans.size(); i++) {
      RefluxPlan pl=(RefluxPlan)plans.get(i);
      numMultiplans[i]=pl.getNum();
      numMultiplansGrp[i]=pl.getGroupe()==null ? 0:pl.getGroupe().getNum();

      RefluxSegment[] sgs=pl.getSegments();
      numMultiplansLignes[i]=new int[sgs.length];
      p1MultiplansLignes[i]=new double[sgs.length][2];
      p2MultiplansLignes[i]=new double[sgs.length][2];
      autoMultiplansLignes[i]=new int[sgs.length];
      valMultiplansLignes[i]=new double[sgs.length];
      for (int j=0; j<sgs.length; j++) {
        numMultiplansLignes[i][j]=sgs[j].getNum();
        p1MultiplansLignes[i][j]=sgs[j].o;
        p2MultiplansLignes[i][j]=sgs[j].e;
        autoMultiplansLignes[i][j]=sgs[j].isHAutomatique()? 1:0;
        if (!sgs[j].isHAutomatique())
          valMultiplansLignes[i][j]=sgs[j].getHauteur();
        else if (j==0)              // Num�ro du plan amont associ�
          valMultiplansLignes[i][j]=pl.getPlanAmont()==null ? 0:pl.getPlanAmont().getNum();
        else if (j==sgs.length-1)   // Num�ro du plan aval associ�
          valMultiplansLignes[i][j]=pl.getPlanAval()==null ? 0:pl.getPlanAval().getNum();
      }
    }
  }
}
/**
 * Retourne la classe interface de calcul associ� au projet.
 *
 * @return L'interface de calcul.
 */
public PRInterfaceCalcul getInterfaceCalcul() {
  if (interface_ == null) {
    interface_= new HY2dInterfaceCalcul();
  }
  return interface_;
}
/**
 * Cr�e les solutions initiales aux noeuds � partir des tableaux pass�s.
 * @param _vx La vitesse x en chaque noeud dans l'ordre des noeuds.
 * @param _vy La vitesse y en chaque noeud dans l'ordre des noeuds.
 * @param _ne Le niveau d'eau en chaque noeud dans l'ordre des noeuds.
 */
public void creeSolutionsInitiales(double[] _vx, double[] _vy, double[] _ne) {
  PRSolutionsInitiales[] sis;
  GrNoeud[] nds= maillage().noeuds();
  sis= new PRSolutionsInitiales[nds.length];
  for (int i= 0; i < nds.length; i++) {
    //      sis[i]=new PRSolutionsInitiales(nds[i],_vx[i],_vy[i],_ne[i]);
    sis[i]=new PRSolutionsInitiales(nds[i], new double[] { _vx[i], _vy[i], _ne[i] });
  }
  mdlCal_.solutionsInitiales(sis);
}
/**
 * Controle et mise � jour des solutions initiales si le nombre de termes
 * u,v,w a chang�. Pas d'action.
 */
public void controleNbTermesNoeuds() {}
/**
 * Initialise les solutions initiales en chaque noeud avec des valeurs
 * globales.
 *
 * @param _valGlobs Valeurs globales.
 *        _valGlobs[0]: Niveau d'eau global.
 */
public void initialiserSolutionsInitiales(double[] _valGlobs) {
  //double sx= 0;
  //double sy= 0;
  double[] hts;
  double[] vx;
  double[] vy;
  GrNoeud[] nds= maillage().noeuds();
  hts=new double[nds.length];
  vx=new double[nds.length];
  vy=new double[nds.length];
  for (int i= 0; i < nds.length; i++) {
    GrPoint pt= nds[i].point_;
    hts[i]= _valGlobs[0] - pt.z_;
    vx[i]=0;
    vy[i]=0;
  }
//  initialiserSolutionsInitiales(sx, sy, hts);
  creeSolutionsInitiales(vx,vy,hts);
}
/**
 * Initialise les solutions initiales en chaque noeud � partir du plan d�finit
 * par 3 points.
 */
public void initialiserSolutionsInitiales() {
  // Controle que les multiplans sont correctement d�finis.
  Vector pls=getPlans();
  for (int i=0; i<pls.size(); i++) {
    RefluxPlan pl=(RefluxPlan)pls.get(i);
    if (pl.getGroupe()==null) {
      new BuDialogError(null, RefluxImplementation.informationsSoftware(),
                        "Le plan n� "+pl.getNum()+
                        " n'est associ� � aucun groupe d'�l�ment").activate();
      return;
    }
    if (pl.isHAutomatiqueAmont() && pl.getPlanAmont()==null ||
        pl.isHAutomatiqueAval()  && pl.getPlanAval()==null) {
      new BuDialogError(null, RefluxImplementation.informationsSoftware(),
                        "Le plan n� "+pl.getNum()+
                        " n'est pas enti�rement d�finit").activate();
      return;
    }
  }

  Vector plsTri=null;
  try {
    plsTri=RefluxPlan.triPlans(getPlans());
  }
  catch (IllegalArgumentException _exc) {
    new BuDialogError(null,RefluxImplementation.informationsSoftware(),_exc.getMessage()).activate();
    return;
  }

  for (int i=0; i<plsTri.size(); i++) {
    RefluxPlan pl=(RefluxPlan)plsTri.get(i);
    if (pl.isHAutomatiqueAmont()) {
      RefluxPlan plDep=pl.getPlanAmont();
      RefluxSegment sg=pl.getSegmentAmont();
      double xmil=(sg.o[0]+sg.e[0])/2;
      double ymil=(sg.o[1]+sg.e[1])/2;
      double h=plDep.calculeHauteur(xmil,ymil);  // A priori si le point est bien entre 2 segments
      pl.setHAmont(h);
    }
    if (pl.isHAutomatiqueAval()) {
      RefluxPlan plDep=pl.getPlanAval();
      RefluxSegment sg=pl.getSegmentAval();
      double xmil=(sg.o[0]+sg.e[0])/2;
      double ymil=(sg.o[1]+sg.e[1])/2;
      double h=plDep.calculeHauteur(xmil,ymil);  // A priori si le point est bien entre 2 segments
      pl.setHAval(h);
    }
    pl.calculeHIntermediaires(); // Pour obtenir un h partout sur le multiplan
  }

  // Initialisation des solutions initiales (par d�faut h � bathym�trie, vx=0, vy=0).
  GrNoeud[] nds=maillage().noeuds();
  double[] hts=new double[nds.length];
  double[] vx=new double[nds.length];
  double[] vy=new double[nds.length];
  for (int i=0; i<hts.length; i++) hts[i]=0;

  Hashtable hnd2Ind=new Hashtable();
  Hashtable hnd2Pl=new Hashtable();   // Stockage du plan dans lequel est le noeud
  Hashtable hnd2Iseg=new Hashtable(); // Stockage du segment du plan dans lequel est le noeud.
  for (int i=0; i<nds.length; i++) hnd2Ind.put(nds[i],new Integer(i));

  // Calcul des hauteurs totales des noeuds pour chaque multiplan.
  for (int i=0; i<plsTri.size(); i++) {
    RefluxPlan pl=(RefluxPlan)plsTri.get(i);
    RefluxGroupe gp=pl.getGroupe();
    Object[] els=gp.getObjects();
    // Regroupement de tous les noeuds de tous les �l�ments concern�s par le plan
    HashSet hnds=new HashSet();
    for (int j=0; j<els.length; j++) {
      GrNoeud[] ndsEle=((GrElement)els[j]).noeuds_;
      for (int k=0; k<ndsEle.length; k++) {
        hnds.add(ndsEle[k]);
      }
    }
    GrNoeud[] ndsPl=(GrNoeud[])hnds.toArray(new GrNoeud[0]);
    for (int j=0; j<ndsPl.length; j++) {
      GrPoint pt=ndsPl[j].point_;
      int iseg=pl.getIndiceSegment(pt.x_,pt.y_);
      double h=pl.calculeHauteur(iseg,pt.x_,pt.y_);
      hnd2Pl.put(ndsPl[j],pl);
      hnd2Iseg.put(ndsPl[j],new Integer(iseg));
      hts[((Integer)hnd2Ind.get(ndsPl[j])).intValue()]=h-pt.z_;
    }
  }

  // Noeuds de transit (calcul�s globalement au maillage)
  boolean[] ntrs=getNoeudsTransit(hts);

  // Attention : Le calcul des vitesses implique de connaitre le plan dans lequel
  // est le ht du noeud. Donc � nouveau recherche des noeuds du plan � consid�rer
  // pour retrouver dans quel plan, etc. Peut �tre stocker plutot la pente pour
  // chaque noeud...

  // Pour chaque noeud concern�, calcul des vitesses.
  for (Enumeration e=hnd2Pl.keys(); e.hasMoreElements();) {
    GrNoeud nd=(GrNoeud)e.nextElement();
    RefluxPlan pl=(RefluxPlan)hnd2Pl.get(nd);
    int iseg=((Integer)hnd2Iseg.get(nd)).intValue();
    int ind=((Integer)hnd2Ind.get(nd)).intValue();
    double[] vxy=pl.calculeVitesses(iseg,hts[ind],nd.point_.x_,nd.point_.y_,ntrs[ind]);
    vx[ind]=vxy[0];
    vy[ind]=vxy[1];
  }

  creeSolutionsInitiales(vx,vy,hts);
//  GrPoint[] ptsSI= mdlCal_.pointsSI();
//  if (ptsSI == null || maillage() == null)
//    return;
//  double[] hts;
//  GrNoeud[] nds= maillage().noeuds();
//  // Calcul de la pente suivant X et Y du plan des S.I.
//  double[] s= mdlCal_.getPenteXYPlanSI();
//  // Initialisation des hauteur totales pour tous les noeuds
//  // a partir du plan de r�f�rence
//  hts= new double[nds.length];
//  for (int i= 0; i < nds.length; i++) {
//    GrPoint pt= nds[i].point;
//    hts[i]=
//      s[0] * (pt.x - ptsSI[0].x)
//        + s[1] * (pt.y - ptsSI[0].y)
//        + ptsSI[0].z
//        - pt.z;
//  }
//  initialiserSolutionsInitiales(s[0], s[1], hts);
}
/**
 * Initialise les solutions initiales � partir de la pente suivant x et y et
 * des hauteurs.
 */
//private void initialiserSolutionsInitiales(
//  double _sx,
//  double _sy,
//  double[] _hts) {
//  // Noeuds de transit
//  boolean[] nTrs= getNoeudsTransit(_hts);
//  // Calcul des vitesses pour chaque noeud (On prend un chezy=50)
//  double[][] vxy= calculVitesses(_hts, nTrs, _sx, _sy);
//  creeSolutionsInitiales(vxy[0], vxy[1], _hts);
//}
}
