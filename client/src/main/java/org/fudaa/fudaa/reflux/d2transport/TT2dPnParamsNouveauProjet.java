/*
 * @file         TT2dPnParamsNouveauProjet.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d2transport;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuGridLayout;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.fudaa.reflux.PRPnParamsNouveauProjet;
/**
 * Un panel comportant les propri�t�s de cr�ation d'un projet de type
 * transport.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class TT2dPnParamsNouveauProjet extends PRPnParamsNouveauProjet {
  //public class TT2dPnParamsNouveauProjet extends JPanel {
  JLabel lbProjet= new JLabel();
  JTextField tfProjet= new JTextField();
  BuGridLayout lyThis= new BuGridLayout();
  JPanel pnProjet= new JPanel();
  JPanel pnCreation= new JPanel();
  BuGridLayout lyCreation= new BuGridLayout();
  JPanel pnPrjHydro= new JPanel();
  BuGridLayout lyPrjHydro= new BuGridLayout();
  JButton btProjet= new JButton();
  TitledBorder bdCreation;
  JFileChooser diFcProjet_;
  JFileChooser diFcMailT3_;
  JFileChooser diFcPrjHydro_;
  JRadioButton rbPrjHydro= new JRadioButton();
  JTextField tfPrjHydro= new JTextField();
  JButton btPrjHydro= new JButton();
  JPanel pnOptionCree= new JPanel();
  BuGridLayout lyOptionCree= new BuGridLayout();
  JRadioButton rbTransT64T3= new JRadioButton();
  JRadioButton rbTransT61T3= new JRadioButton();
  Border bdOptionCree;
  JPanel pnMailT3= new JPanel();
  BuGridLayout lyMailT3= new BuGridLayout();
  JTextField tfMailT3= new JTextField();
  JRadioButton rbMailT3= new JRadioButton();
  JButton btMailT3= new JButton();
  private final static String[] extsPrj= new String[] { "pre" };
  private final static String[] extsPrjHydro=
    new String[] { "pre", "cor", "ele", "bth" };
  private final static String[] extsMailT3=
    new String[] { "cor", "ele", "bth" };
  /**
   * Cr�ation du panel.
   */
  public TT2dPnParamsNouveauProjet() {
    try {
      jbInit();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  /**
   * M�thode d'impl�mentation de l'interface.
   */
  private void jbInit() throws Exception {
    ButtonGroup bgCreation= new ButtonGroup();
    ButtonGroup bgOption= new ButtonGroup();
    // Projet � cr�er
    lbProjet.setToolTipText("");
    lbProjet.setText("Nom du projet � cr�er:");
    tfProjet.setPreferredSize(new Dimension(250, 21));
    btProjet.setText("...");
    btProjet.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        btProjet_actionPerformed(_evt);
      }
    });
    pnProjet.add(lbProjet);
    pnProjet.add(tfProjet);
    pnProjet.add(btProjet);
    // Cr�ation depuis un maillage T3 existant
    rbMailT3.setText("Maillage T3 existant");
    rbMailT3.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbTrans_itemStateChanged(_evt);
      }
    });
    bgCreation.add(rbMailT3);
    tfMailT3.setPreferredSize(new Dimension(250, 21));
    btMailT3.setText("...");
    btMailT3.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        btMailT3_actionPerformed(_evt);
      }
    });
    lyMailT3.setHgap(5);
    lyMailT3.setVfilled(false);
    lyMailT3.setCfilled(false);
    pnMailT3.setLayout(lyMailT3);
    pnMailT3.add(rbMailT3);
    pnMailT3.add(tfMailT3);
    pnMailT3.add(btMailT3);
    // Maillage transport � cr�er
    rbPrjHydro.setText("Projet courantologie");
    rbPrjHydro.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbTrans_itemStateChanged(_evt);
      }
    });
    bgCreation.add(rbPrjHydro);
    tfPrjHydro.setPreferredSize(new Dimension(250, 21));
    btPrjHydro.setText("...");
    btPrjHydro.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        btPrjHydro_actionPerformed(_evt);
      }
    });
    lyPrjHydro.setHgap(5);
    lyPrjHydro.setVfilled(false);
    lyPrjHydro.setCfilled(false);
    pnPrjHydro.setLayout(lyPrjHydro);
    pnPrjHydro.add(rbPrjHydro);
    pnPrjHydro.add(tfPrjHydro);
    pnPrjHydro.add(btPrjHydro);
    // Panneau d'options pour le maillage transport cr��
    rbTransT64T3.setText(
      "D�composition des T6 du maillage courantologie en 4 T3");
    rbTransT64T3.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbTrans_itemStateChanged(_evt);
      }
    });
    bgOption.add(rbTransT64T3);
    rbTransT61T3.setText(
      "D�composition des T6 du maillage courantologie en T3");
    rbTransT61T3.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbTrans_itemStateChanged(_evt);
      }
    });
    bgOption.add(rbTransT61T3);
    bdOptionCree= BorderFactory.createEmptyBorder(0, 20, 0, 0);
    lyOptionCree.setColumns(1);
    pnOptionCree.setLayout(lyOptionCree);
    pnOptionCree.setBorder(bdOptionCree);
    pnOptionCree.add(rbTransT64T3);
    pnOptionCree.add(rbTransT61T3);
    bdCreation=
      new TitledBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
        "Cr�� depuis");
    lyCreation.setColumns(1);
    pnCreation.setLayout(lyCreation);
    pnCreation.setBorder(bdCreation);
    pnCreation.add(pnPrjHydro);
    pnCreation.add(pnOptionCree);
    pnCreation.add(pnMailT3);
    lyThis.setColumns(1);
    lyThis.setHgap(5);
    lyThis.setVgap(5);
    this.setLayout(lyThis);
    this.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.add(pnCreation);
    this.add(pnProjet);
    BuFileFilter flt;
    // Dialogue de saisie du nom du projet � cr�er.
    flt= new BuFileFilter("pre", "Projet");
    diFcProjet_= new JFileChooser();
    diFcProjet_.setFileHidingEnabled(true);
    diFcProjet_.setCurrentDirectory(new File(System.getProperty("user.dir")));
    diFcProjet_.setMultiSelectionEnabled(false);
    diFcProjet_.addChoosableFileFilter(flt);
    diFcProjet_.setFileFilter(flt);
    // Dialogue d'ouverture des fichiers de maillage T3.
    flt= new BuFileFilter(new String[] { "cor", "ele", "bth" }, "Maillage T3");
    diFcMailT3_= new JFileChooser();
    diFcMailT3_.setFileHidingEnabled(true);
    diFcMailT3_.setCurrentDirectory(new File(System.getProperty("user.dir")));
    diFcMailT3_.setMultiSelectionEnabled(false);
    diFcMailT3_.addChoosableFileFilter(flt);
    diFcMailT3_.setFileFilter(flt);
    // Dialogue de s�lection du projet de courantologie.
    flt= new BuFileFilter("pre", "Projet");
    diFcPrjHydro_= new JFileChooser();
    diFcPrjHydro_.setFileHidingEnabled(true);
    diFcPrjHydro_.setCurrentDirectory(new File(System.getProperty("user.dir")));
    diFcPrjHydro_.setMultiSelectionEnabled(false);
    flt= new BuFileFilter(new String[] { "cor", "ele", "bth" }, "Maillage T6");
    diFcPrjHydro_.addChoosableFileFilter(flt);
    flt= new BuFileFilter("pre", "Projet");
    diFcPrjHydro_.addChoosableFileFilter(flt);
    diFcPrjHydro_.setFileFilter(flt);
    rbMailT3.setSelected(true);
    rbPrjHydro.setSelected(true);
    rbTransT61T3.setSelected(true);
  }
  /**
   * Retourne les param�tres de cr�ation du projet sous forme d'objets.
   * @return Les param�tres.
   * <pre>
   *   0 : String   Racine du projet courantologie ou du maillage T3 existant
   *   1 : Integer  0 : Cr�ation du maillage transport depuis le maillage
   *                courantologie, 1 : Existant
   *   2 : Integer  Mode pour la cr�ation 0 : 1T6->1T3, 1 : 1T6->4T3
   * </pre>
   */
  public Object[] getParametres() {
    Object[] r= new Object[3];
    if (rbPrjHydro.isSelected())
      r[0]= getRacineFichier(tfPrjHydro.getText(), extsPrjHydro);
    else
      r[0]= getRacineFichier(tfMailT3.getText(), extsMailT3);
    r[1]= rbPrjHydro.isSelected() ? new Integer(0) : new Integer(1);
    r[2]= rbTransT61T3.isSelected() ? new Integer(0) : new Integer(1);
    return r;
  }
  /**
   * Retourne la racine du nom du projet.
   * @return La racine sans extension.
   */
  public String getRacineProjet() {
    return getRacineFichier(tfProjet.getText(), extsPrj);
  }
  /**
   * Retourne la racine d'un fichier s�lectionn�.
   *
   * @param _fich Nom du fichier s�lectionn�.
   * @param _exts Les extensions possibles sans les ".".
   * @return La racine du fichier sans extension.
   */
  private String getRacineFichier(String _fich, String[] _exts) {
    String r= _fich;
    for (int i= 0; i < _exts.length; i++) {
      if (r.endsWith(CtuluLibString.DOT + _exts[i])) {
        r= r.substring(0, r.lastIndexOf(CtuluLibString.DOT + _exts[i]));
        break;
      }
    }
    return r;
  }
  //----------------------------------------------------------------------------
  // Ev�nements
  //----------------------------------------------------------------------------
  /**
   * D�clench� quand un radio bouton du panneau Maillage transport est
   * s�lectionn�.
   */
  void rbTrans_itemStateChanged(ItemEvent _evt) {
    if (_evt.getSource() == rbMailT3) {
      boolean b= _evt.getStateChange() == ItemEvent.SELECTED;
      tfMailT3.setEnabled(b);
      btMailT3.setEnabled(b);
    }
    if (_evt.getSource() == rbPrjHydro) {
      boolean b= _evt.getStateChange() == ItemEvent.SELECTED;
      tfPrjHydro.setEnabled(b);
      btPrjHydro.setEnabled(b);
      rbTransT61T3.setEnabled(b);
      rbTransT64T3.setEnabled(b);
      tfProjet.setEnabled(b);
      btProjet.setEnabled(b);
    }
    if (_evt.getStateChange() == ItemEvent.SELECTED) {
      if (_evt.getSource() == rbMailT3) {
        String fich= getRacineFichier(tfMailT3.getText(), extsMailT3);
        tfProjet.setText(fich + ".pre");
      } else {
        String fich= getRacineFichier(tfPrjHydro.getText(), extsPrjHydro);
        tfProjet.setText(fich + "_t.pre");
      }
    }
  }
  /**
   * D�clench� quand le bouton de s�lection du nom du fichier projet est activ�.
   */
  void btProjet_actionPerformed(ActionEvent _evt) {
    diFcProjet_.setSelectedFile(new File(tfProjet.getText()));
    int ret= diFcProjet_.showOpenDialog(this);
    if (ret == JFileChooser.APPROVE_OPTION) {
      tfProjet.setText("" + diFcProjet_.getSelectedFile());
    }
  }
  /**
   * D�clench� quand le bouton de s�lection de fichiers de maillage T3 est
   * activ�.
   */
  void btMailT3_actionPerformed(ActionEvent _evt) {
    int ret= diFcMailT3_.showOpenDialog(this);
    if (ret == JFileChooser.APPROVE_OPTION) {
      String fich=
        getRacineFichier("" + diFcMailT3_.getSelectedFile(), extsMailT3);
      tfMailT3.setText(fich);
      tfProjet.setText(fich + ".pre");
    }
  }
  /**
   * D�clench� quand le bouton de s�lection du projet de courantologie est
   * activ�.
   */
  void btPrjHydro_actionPerformed(ActionEvent _evt) {
    int ret= diFcPrjHydro_.showOpenDialog(this);
    if (ret == JFileChooser.APPROVE_OPTION) {
      String fich=
        getRacineFichier("" + diFcPrjHydro_.getSelectedFile(), extsPrjHydro);
      tfPrjHydro.setText(fich + ".pre");
      tfProjet.setText(fich + "_t.pre");
    }
  }
}