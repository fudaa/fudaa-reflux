/*
 * @file         RefluxDialog.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuHorizontalLayout;
/**
 * Un dialogue g�n�rique duquel les dialogues doivent d�river.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxDialog extends JDialog {
  public static final int OK_CANCEL_OPTION= 0;
  public static final int OK_APPLY_OPTION= 1;
  public static final int OK_CANCEL_APPLY_OPTION= 2;
  public JButton OK_BUTTON= new JButton();
  public JButton CANCEL_BUTTON= new JButton();
  public JButton APPLY_BUTTON= new JButton();
  protected JPanel pnAffichage= new JPanel();
  protected int option;
  private JPanel pnAction= new JPanel();
  private BuHorizontalLayout lyAction= new BuHorizontalLayout();
  private JPanel pnAction1= new JPanel();
  private BorderLayout lyThis= new BorderLayout();
  private BorderLayout lyAffichage= new BorderLayout();
  //private Frame parent_= null;
  /**
   * Cr�ation d'une fen�tre dialogue avec centrage sur l'�cran.
   */
  public RefluxDialog() {
    this(null);
  }
  /**
   * Cr�ation d'une fen�tre dialogue avec centrage sur le Frame _parent.
   */
  public RefluxDialog(Frame _parent) {
    this(_parent, OK_CANCEL_OPTION);
  }
  /**
   * Cr�ation d'une fen�tre dialogue avec centrage sur le Frame _parent et
   * affectation des boutons d'action (_option).
   */
  public RefluxDialog(Frame _parent, int _option) {
    super(_parent, true);
    // Pour recentrage de la fen�tre � l'�cran
    //parent_= _parent;
    OK_BUTTON.setText("Continuer");
    OK_BUTTON.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        OK_BUTTON_actionPerformed(e);
      }
    });
    CANCEL_BUTTON.setText("Annuler");
    CANCEL_BUTTON.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CANCEL_BUTTON_actionPerformed(e);
      }
    });
    APPLY_BUTTON.setText("Appliquer");
    APPLY_BUTTON.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        APPLY_BUTTON_actionPerformed(e);
      }
    });
    pnAffichage.setLayout(lyAffichage);
    pnAffichage.setBorder(new EmptyBorder(5, 5, 5, 5));
    lyAction.setHgap(5);
    lyAction.setVfilled(false);
    pnAction.setLayout(lyAction);
    pnAction1.add(pnAction, null);
    this.getContentPane().setLayout(lyThis);
    this.getContentPane().add(pnAction1, BorderLayout.SOUTH);
    this.getContentPane().add(pnAffichage, BorderLayout.CENTER);
    setOption(_option);
  }
  /**
   * D�finition des buttons actions � afficher (OK, CANCEL, APPLIQUER).
   */
  public void setOption(int _option) {
    option= _option;
    pnAction.removeAll();
    switch (_option) {
      case OK_CANCEL_OPTION :
        pnAction.add(OK_BUTTON, null);
        pnAction.add(CANCEL_BUTTON, null);
        break;
      case OK_APPLY_OPTION :
        pnAction.add(OK_BUTTON, null);
        pnAction.add(APPLY_BUTTON, null);
        break;
      case OK_CANCEL_APPLY_OPTION :
        pnAction.add(OK_BUTTON, null);
        pnAction.add(CANCEL_BUTTON, null);
        pnAction.add(APPLY_BUTTON, null);
        break;
    }
    pack();
  }
  public int getOption() {
    return option;
  }
  /**
   * Affichage de la boite de dialogue avec un recentrage.
   */
  public void show() {
    // Calcul de la position de la fenetre
    Dimension dParent;
    Point pParent;
    Dimension dThis= this.getPreferredSize();
    Point pThis= new Point();
    /* BM 19/02/2003 : la fenetre se d�cale vers la gauche a chaque affichage ???
       La taille du parent change a chaque affichage.
       => On ne tient plus compte du parent, on centre sur l'�cran.

        if (parent_ == null) {
          dParent = Toolkit.getDefaultToolkit().getScreenSize();
          pParent = new Point(0,0);
        }
        else {
         dParent = parent_.getPreferredSize();
         pParent = parent_.getLocation();
        }
    */
    dParent= Toolkit.getDefaultToolkit().getScreenSize();
    pParent= new Point(0, 0);
    pThis.x= pParent.x + (dParent.width - dThis.width) / 2;
    pThis.y= pParent.y + (dParent.height - dThis.height) / 2;
    this.setLocation(pThis);
    super.show();
  }
  /**
   * Ajout d'un listener aux boutons action du dialog (OK, ANNULER, APPLIQUER). Le
   * bouton press� est obtenu par getSource().
   */
  public void addActionListener(ActionListener _listener) {
    OK_BUTTON.addActionListener(_listener);
    CANCEL_BUTTON.addActionListener(_listener);
    APPLY_BUTTON.addActionListener(_listener);
  }
  /**
   * Suppression d'un listener aux boutons action du dialog (OK, ANNULER, APPLIQUER).
   * @see #addActionListener(ActionListener)
   */
  public void removeActionListener(ActionListener _listener) {
    OK_BUTTON.removeActionListener(_listener);
    CANCEL_BUTTON.removeActionListener(_listener);
    APPLY_BUTTON.removeActionListener(_listener);
  }
  /**
   * Bouton "Ok" press�, effacage du dialog, traitement dans le listener du dialog.
   */
  void OK_BUTTON_actionPerformed(ActionEvent _evt) {
    actionOK();
    dispose();
  }
  /**
   * Bouton "Apply" press�, traitement dans le listener du dialog.
   */
  void APPLY_BUTTON_actionPerformed(ActionEvent _evt) {
    actionApply();
  }
  /**
   * Bouton "annuler" press�, effacage du dialog, traitement dans le listener du dialog.
   */
  void CANCEL_BUTTON_actionPerformed(ActionEvent _evt) {
    actionCancel();
    dispose();
  }
  /**
   * M�thode action pour le bouton OK � surcharger. Pas d�faut, la m�thode
   * appelle la m�thode actionApply() qui doit �tre surcharg�e.
   */
  protected void actionOK() {
    actionApply();
  }
  /**
   * M�thode action pour le bouton APPLY � surcharger.
   */
  protected void actionApply() {}
  /**
   * M�thode action pour le bouton CANCEL � surcharger.
   */
  protected void actionCancel() {}
  /*  public static void main(String[] argv) {
      RefluxDialogNormale d = new RefluxDialogNormale();
      d.affiche(new Double(10.));
      System.exit(0);
    } */
}
