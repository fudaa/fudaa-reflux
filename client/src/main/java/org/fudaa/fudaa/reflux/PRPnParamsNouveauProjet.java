/*
 * @file         PRPnParamsNouveauProjet.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import javax.swing.JPanel;
/**
 * Un panel comportant les parametres de cr�ation du projet.
 * Les param�tres de cr�ation de projet sont stock�s dans des objets. Chaque
 * type de projet g�re les param�tres qu'il recoit.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public abstract class PRPnParamsNouveauProjet extends JPanel {
  /**
   * Retourne la racine du projet.
   */
  public abstract String getRacineProjet();
  /*
   * Retourne les parametres de cr�ation du projet.
   * @return Les parametres dans l'ordre de chaque projet.
   */
  public abstract Object[] getParametres();
}
