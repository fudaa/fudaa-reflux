/*

 * @file         RefondeFillePost.java

 * @creation     1999-08-09

 * @modification $Date: 2006-11-14 09:08:09 $

 * @license      GNU General Public License 2

 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne

 * @mail         devel@fudaa.org

 */
package org.fudaa.fudaa.reflux;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuPopupButton;

import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;

import org.fudaa.fudaa.commun.trace2d.BPaletteCouleurPlage;
import org.fudaa.fudaa.commun.trace2d.BPanneauOptionsIso;
import org.fudaa.fudaa.commun.trace2d.BPanneauPaletteCouleurPlage;
import org.fudaa.fudaa.commun.trace2d.BPanneauSelectedStep;
import org.fudaa.fudaa.commun.trace2d.ZModeleChangeListener;
import org.fudaa.fudaa.commun.trace2d.ZModeleValeur;
/**

 * Une fenetre interne affichant les calques de post traitement sans int�raction

 * possible.

 *

 * @version      $Id: RefluxFillePost.java,v 1.22 2006-11-14 09:08:09 deniger Exp $

 * @author       Bertrand Marchand

 */
public class RefluxFillePost
  extends ZEbliFilleCalques
  implements RefluxDataChangeListener, ZSelectionListener,TreeSelectionListener {
  /** Num�ro de fen�tre, incr�ment� � chaque cr�ation. Ce num�ro ne sert que

      dans la liste de fen�tres, pour les diff�rentier. */
  private static int numero_;
  /** Table des calques r�sultats. */
  private Vector cqsResultats_= new Vector();
  /** Calque des noeuds. */
  private ZCalquePoint cqNds_;
  /** Calque des l�gendes. */
  private BCalqueLegende cqLeg_;
  /** Tools sp�cifiques de la fen�tre. */
  private JComponent[] btTools_;
  /** panneau de palette de couleur. */
  private BPanneauPaletteCouleurPlage pnPal_;
  /** Panneau d'options de trac� iso. */
  private BPanneauOptionsIso pnIso_;
  /** Panneau de selection du temps courant. */
  private BPanneauSelectedStep pnStep_;
  /** Panneau d'information sur l'objet s�lectionn�. */
  private BPanneauInformation pnInfo_;
  /** Le maillage permet de visualiser les r�sultats. */
  //private RefluxMaillage maillage_=null;
  /** Le calque actif (s�lectionn� dans l'arbre des calques). */
  private BCalque cqActif_;
  /** Projet non private pour etre accessible depuis les inner-classes. */
  PRProjet prj_;

  private static class RefluxSpecificPanel extends ZEbliCalquesPanel{

    public RefluxSpecificPanel(){
      super(null);
    }



    protected synchronized void buildButtonGroupStandard(){
      //ne fait rien
    }
  }
  /**

   * Cr�ation d'une fen�tre de gestion des calques sans pr�occupation de

   * la vue associ�e.

   */
  //  public RefluxFillePost(BArbreCalque _ac) {
  //    this( _ac);
  //  }
  /**

   * Cr�ation d'une fen�tre de gestion des calques sans pr�occupation de

   * la vue associ�e.

   */
  public RefluxFillePost(BArbreCalque _ac) {
    super(new RefluxSpecificPanel(),null,null/**_vc,_ac,
    true, true, false*/);
    //BCalque cqRoot=getVueCalque().getCalque();
    //le calque des donnees.
    //    BCalque cqRoot=gcDonnees_;
    addInternalFrameListener(_ac);
    // Suppression du calque par d�faut
    //inutile : pas de calque par defaut (ou plus ...).
    //    BCalque[] cqs=cqRoot.getTousCalques();
    //    for (int i=0; i<cqs.length; i++)
    //     if (cqs[i].getName()==null) cqs[i].detruire();
    // Calque l�gende
    cqLeg_= new BCalqueLegende();
    cqLeg_.setName("cqLegende");
    cqLeg_.setTitle("L�gende");
    cqLeg_.setForeground(Color.blue);
    //    cqRoot.add(cqLeg_);
    pn_.addCalque(cqLeg_);
    // Calque des noeuds
    cqNds_= new ZCalquePoint();
    cqNds_.setName("cqNoeud");
    cqNds_.setTitle("Noeuds");
    cqNds_.setIconModel(0, new TraceIconModel(TraceIcon.CROIX, 2,Color.GREEN));
    cqNds_.setForeground(Color.green.darker());
    cqNds_.addSelectionListener(this);
    //    cqRoot.add(cqNds_);
    addCalque(cqNds_);
    BGroupeCalque gc=new BGroupeCalque();
    gc.setName("cqInitiales");
    gc.setTitle("Valeurs initiales");
    addCalque(gc);
    // Tools sp�cifiques de la fen�tre
    //Changement fred : normalement on doit pouvoir se passer de l'arbre et
    //ne s'occuper que du model.
    _ac.setModel(getArbreCalqueModel());
    CalqueGuiHelper.CALQUE_OUTILS.addPropertyChangeListener(
      getArbreCalqueModel());
    buildTools();
    getVueCalque().setBackground(Color.white);
    getVueCalque().setPreferredSize(new Dimension(550, 400));
    setTitle("Fen�tre post " + (++numero_));
    setFrameIcon(RefluxResource.REFLUX.getIcon("post"));
    //Initialise dans le constructeur
    //    setBoutonRepereVisible(true);
    //    setSelectionVisible(true);
    //    setBoutonsStandardVisible(false);
    setClosable(true);
    //setToolsInSpecificBar(true);
    pack();
    _ac.setRootVisible(false);
    //_ac.setModel(getArbreCalqueModel());
    _ac.addTreeSelectionListener(this);
  }
  /**

   * Pour donner un nom � la vue calque, nom utilis� dans la fenetre de mise en

   * page pour l'affichage en mode rapide des vues.

   */
  public void setTitle(String _title) {
    super.setTitle(_title);
    getVueCalque().setName(_title);
  }
  /**

   * Initialisation avec le projet.

   */
  public void initialise(PRProjet _prj) {
    // Cr�ation du super maillage. Le maillage est cens� �tre optimis� � ce
    // niveau et donc compatible avec des r�sultats de calcul.
    // Les noeuds sont rang�s dans l'ordre de leur num�rotation, les �l�ments
    // dans n'importe quel ordre.
    //    superMail_=new GrMaillage();
    //
    //    GrMaillageElement mail=RefondeMaillage.creeSuperMaillage(_prj);
    //    GrNoeud[]         nds=mail.noeuds();
    //    GrElement[]       els=mail.elements();
    //    Hashtable         hInd2Nd=new Hashtable();
    //    for (int i=0; i<nds.length; i++)
    //     hInd2Nd.put(new Integer(((RefondeNoeudData)nds[i].data()).numero-1),nds[i]);
    //
    //    // Noeuds dans l'ordre de leur num�rotation
    //    for (int i=0; i<nds.length; i++)
    //     superMail_.noeuds.ajoute(((GrNoeud)hInd2Nd.get(new Integer(i))).point);
    //
    //    // Connectivit�s
    //    for (int i=0; i<els.length; i++) {
    //      GrNoeud[] ndsEl=els[i].noeuds;
    //      int[] conn=new int[ndsEl.length];
    //      for (int j=0; j<ndsEl.length; j++)
    //       conn[j]=((RefondeNoeudData)ndsEl[j].data()).numero-1;
    //      superMail_.connectivites.add(conn);
    //    }
    prj_= _prj;
    cqNds_.setModele(new RefluxModeleNoeud(prj_.maillage()));
    // Cr�ation des calques de r�sultat
    clearResultats();
    if (_prj.hasResultats()) {
      PRResultats res= _prj.resultats();
      pnStep_.clearSteps();
      for (int i= 0; i < res.getNbSteps(); i++)
        pnStep_.addStep(new Double(res.getStep(i).t));
      ajouteVecteur();
      PRResultats.Etape step= res.getStep(0);
      for (int i= 0; i < step.getNbColonnes(); i++) {
        ajouteResultat(step.getColonne(i).type);
      }
    }
    _prj.addDataChangeListener(this);
    if (Definitions.estPlanReferenceOK()) ajouteVecteurInit();
    ajouteBathyInit();
    if (Definitions.estPlanReferenceOK()) ajouteProfondeurInit();
    if (Definitions.estPlanReferenceOK()) ajouteNEInit();
  }

  /**
   * Suppression des calques de r�sultats.
   */
  public void clearResultats() {
    for (int i= 0; i < cqsResultats_.size(); i++)
       ((BCalque)cqsResultats_.get(i)).detruire();
    cqsResultats_.clear();
  }

  /**
   * Ajout de la bathymetrie comme calque de r�sultat.
   */
  public void ajouteBathyInit() {
    // Calque de bathymetrie
    class ModeleValeurBathy implements ZModeleValeur {
      double[] vals_;
      public ModeleValeurBathy(double[] _vals) {
        vals_= _vals;
      }
      public int nbValeurs() {
        return vals_.length;
      }
      public double valeur(int i) {
        return vals_[i];
      }
      public double getMin() {
        double r= Double.POSITIVE_INFINITY;
        for (int i= 0; i < vals_.length; i++)
          r= Math.min(r, vals_[i]);
        return r;
      }
      public double getMax() {
        double r= Double.NEGATIVE_INFINITY;
        for (int i= 0; i < vals_.length; i++)
          r= Math.max(r, vals_[i]);
        return r;
      }
      public void addModelChangeListener(ZModeleChangeListener _list) {} // Model doesn't change
      public void removeModelChangeListener(ZModeleChangeListener _list) {} // Model doesn't change
    }
    RefluxMaillage mail= prj_.maillage();
    double[] z= new double[mail.nombreNoeuds()];
    for (int i= 0; i < z.length; i++)
      z[i]= mail.noeud(i).point_.z_;
    //    BCalque cqRoot=getVueCalque().getCalque();
    ZCalqueIso cq= new ZCalqueIso();
    cq.setLegende(cqLeg_);
    //    cq.setPaletteCouleur(new BPaletteCouleurSimple());
    cq.setModeleMaillage(mail);
    cq.setName("cq" + "Bathy init");
    cq.setTitle("Bathy init");
    cq.setModeleValeurs(new ModeleValeurBathy(z));
    cq.setContour(false);
    cq.setIsolignes(false);
    cq.setSurface(false);
    cq.setVisible(false);
    getDonneesCalque().getCalqueParTitre("Valeurs initiales").add(cq);
    //    cqRoot.add(cq);
//    addCalque(cq);
  }

  public void ajouteNEInit() {
    // Calque de bathymetrie
    class ModeleValeurNE implements ZModeleValeur {
      PRSolutionsInitiales[] vals_;
      public ModeleValeurNE(PRSolutionsInitiales[] _sis) {
        vals_= _sis;
      }
      public int nbValeurs() {
        return vals_.length;
      }
      public double valeur(int i) {
        double[] v=vals_[i].valeurs();
        return v[v.length-1]+prj_.maillage().point(i).z_;
      }
      public double getMin() {
        double r= Double.POSITIVE_INFINITY;
        for (int i= 0; i < vals_.length; i++) {
          double[] v=vals_[i].valeurs();
          r= Math.min(r, v[v.length-1]+prj_.maillage().point(i).z_);
        }
        return r;
      }
      public double getMax() {
        double r= Double.NEGATIVE_INFINITY;
        for (int i= 0; i < vals_.length; i++) {
          double[] v=vals_[i].valeurs();
          r= Math.max(r, v[v.length-1]+prj_.maillage().point(i).z_);
        }
        return r;
      }
      public void addModelChangeListener(ZModeleChangeListener _list) {} // Model doesn't change
      public void removeModelChangeListener(ZModeleChangeListener _list) {} // Model doesn't change
    }

    RefluxMaillage mail= prj_.maillage();
    double[] z= new double[mail.nombreNoeuds()];
    for (int i= 0; i < z.length; i++)
      z[i]= mail.noeud(i).point_.z_;
    //    BCalque cqRoot=getVueCalque().getCalque();
    ZCalqueIso cq= new ZCalqueIso();
    cq.setLegende(cqLeg_);
    //    cq.setPaletteCouleur(new BPaletteCouleurSimple());
    cq.setModeleMaillage(mail);
    cq.setName("cq" + "Niveau d'eau init");
    cq.setTitle("Niveau d'eau init");
    cq.setModeleValeurs(new ModeleValeurNE(prj_.modeleCalcul().solutionsInitiales()));
    cq.setContour(false);
    cq.setIsolignes(false);
    cq.setSurface(false);
    cq.setVisible(false);
    //    cqRoot.add(cq);
    getDonneesCalque().getCalqueParTitre("Valeurs initiales").add(cq);
//    addCalque(cq);
  }

  public void ajouteProfondeurInit() {
    // Calque de bathymetrie
    class ModeleValeurProfondeur implements ZModeleValeur {
      PRSolutionsInitiales[] vals_;
      public ModeleValeurProfondeur(PRSolutionsInitiales[] _sis) {
        vals_= _sis;
      }
      public int nbValeurs() {
        return vals_.length;
      }
      public double valeur(int i) {
        double[] v=vals_[i].valeurs();
        return v[v.length-1];
      }
      public double getMin() {
        double r= Double.POSITIVE_INFINITY;
        for (int i= 0; i < vals_.length; i++) {
          double[] v=vals_[i].valeurs();
          r = Math.min(r, v[v.length-1]);
        }
        return r;
      }
      public double getMax() {
        double r= Double.NEGATIVE_INFINITY;
        for (int i= 0; i < vals_.length; i++) {
          double[] v=vals_[i].valeurs();
          r = Math.max(r, v[v.length-1]);
        }
        return r;
      }
      public void addModelChangeListener(ZModeleChangeListener _list) {} // Model doesn't change
      public void removeModelChangeListener(ZModeleChangeListener _list) {} // Model doesn't change
    }

    RefluxMaillage mail= prj_.maillage();
    double[] z= new double[mail.nombreNoeuds()];
    for (int i= 0; i < z.length; i++)
      z[i]= mail.noeud(i).point_.z_;
    //    BCalque cqRoot=getVueCalque().getCalque();
    ZCalqueIso cq= new ZCalqueIso();
    cq.setLegende(cqLeg_);
    //    cq.setPaletteCouleur(new BPaletteCouleurSimple());
    cq.setModeleMaillage(mail);
    cq.setName("cq" + "Profondeurs init");
    cq.setTitle("Profondeurs init");
    cq.setModeleValeurs(new ModeleValeurProfondeur(prj_.modeleCalcul().solutionsInitiales()));
    cq.setContour(false);
    cq.setIsolignes(false);
    cq.setSurface(false);
    cq.setVisible(false);
    //    cqRoot.add(cq);
    getDonneesCalque().getCalqueParTitre("Valeurs initiales").add(cq);
//    addCalque(cq);
  }

  /**
   * Ajoute les solutions initiales sous forme de vecteur.
   */
  public void ajouteVecteurInit() {
    BCalque cqRoot=getDonneesCalque();
    String nom="Vitesses init";
    if (cqRoot.getCalqueParTitre(nom) != null) return;
    RefluxModeleSIVecteur mdSI=
      new RefluxModeleSIVecteur(prj_.modeleCalcul().solutionsInitiales());
    ZCalqueVecteur cq= new ZCalqueVecteur();
    //    cq.setPaletteCouleur(new BPaletteCouleurSimple());
    cq.setModeleMaillage(prj_.maillage());
    cq.setName("cq" + nom);
    cq.setTitle(nom);
    cq.setModeleVecteur(mdSI);
    cq.setVisible(false);
    cq.setLegende(cqLeg_);
    cqsResultats_.add(cq);
    //    cqRoot.add(cq);
    getDonneesCalque().getCalqueParTitre("Valeurs initiales").add(cq);
//    addCalque(cq);
  }

  /**
   * Ajout d'un r�sultat. Le r�sultat n'est ajout� que s'il n'existe pas d�j� un
   * calque de m�me nom.
   */
  public void ajouteResultat(int _type) {
    //    BCalque cqRoot=getVueCalque().getCalque();
    BCalque cqRoot= getDonneesCalque();
    String nom= Definitions.resultToString(_type);
    if (cqRoot.getCalqueParTitre(nom) != null)
      return;
    RefluxModeleResultats mdRes=
      new RefluxModeleResultats(prj_.resultats(), _type);
    mdRes.setSelectedStep(pnStep_.getSelectedStep());
    ZCalqueIso cq= new ZCalqueIso();
    //    cq.setPaletteCouleur(new BPaletteCouleurSimple());
    cq.setModeleMaillage(prj_.maillage());
    cq.setName("cq" + nom);
    cq.setTitle(nom);
    cq.setModeleValeurs(mdRes);
    cq.setContour(false);
    cq.setIsolignes(false);
    cq.setSurface(false);
    cq.setVisible(false);
    cq.setLegende(cqLeg_);
    cqsResultats_.add(cq);
    //    cqRoot.add(cq);
    addCalque(cq);
  }

  /**
   * Ajout d'une r�pr�sentation sous forme de vecteurs. Le r�sultat n'est ajout�
   * que s'il n'existe pas d�j� un calque de m�me nom.
   */
  public void ajouteVecteur() {
    //    BCalque cqRoot=getVueCalque().getCalque();
    BCalque cqRoot= getDonneesCalque();
    String nom= "Vecteurs";
    if (cqRoot.getCalqueParTitre(nom) != null)
      return;
    RefluxModeleResultatsVecteur mdRes=
      new RefluxModeleResultatsVecteur(prj_.resultats());
    mdRes.setSelectedStep(pnStep_.getSelectedStep());
    ZCalqueVecteur cq= new ZCalqueVecteur();
    //    cq.setPaletteCouleur(new BPaletteCouleurSimple());
    cq.setModeleMaillage(prj_.maillage());
    cq.setName("cq" + nom);
    cq.setTitle(nom);
    cq.setModeleVecteur(mdRes);
    cq.setVisible(false);
    cq.setLegende(cqLeg_);
    cqsResultats_.add(cq);
    //    cqRoot.add(cq);
    addCalque(cq);
  }
  public String[] getEnabledActions() {
    return super.getEnabledActions();
  }
  public String[] getDisabledActions() {
    return super.getDisabledActions();
  }
  /**

   * Outils sp�cifiques de la fen�tre.

   */
  public JComponent[] getSpecificTools() {
    JComponent[] cpssuper= super.getSpecificTools();
    // La valeur de DesktopPane n'est valid�e qu'une fois la fen�tre ajout�e
    // au desktop. En th�orie, il faudrait faire le traitement ci dessous
    // une fois la fen�tre ajout�e. En pratique, on le fait dans la m�thode
    // getSpecificTools() appel�e chaque fois que la fen�tre est rendue active.
    // B.M. 20.08.2001
    try {
      for (int i= 0; i < btTools_.length; i++)
        if (btTools_[i] != null)
          ((BuPopupButton)btTools_[i]).setDesktop((BuDesktop)getDesktopPane());
    } catch (ClassCastException e) {}
    JComponent[] cps= new JComponent[cpssuper.length + btTools_.length];
    System.arraycopy(cpssuper, 0, cps, 0, cpssuper.length);
    System.arraycopy(btTools_, 0, cps, cpssuper.length, btTools_.length);
    return cps;
  }
  /**

   * Ev�nement sur l'arbre (changement d'activation de calque).

   */
  public void valueChanged(TreeSelectionEvent _evt) {
//    super.valueChanged(_evt);
    try {
      cqActif_= (BCalque)_evt.getNewLeadSelectionPath().getLastPathComponent();
      //      updateTools(cqActif_);
      //      if (btTools_.length>0 ) btTools_[0].doLayout();
      if (cqActif_ instanceof ZCalqueIso) {
        // Mise a jour du panneau de palette.
        pnPal_.setPalette(((ZCalqueIso)cqActif_).getPalette());
        pnPal_.setBorneMin(((ZCalqueIso)cqActif_).getModeleValeur().getMin());
        pnPal_.setBorneMax(((ZCalqueIso)cqActif_).getModeleValeur().getMax());
        pnPal_.setPanneauTailleEnabled(false);
      } else if (cqActif_ instanceof ZCalqueVecteur) {
        // Mise a jour du panneau de palette.
        pnPal_.setPalette(((ZCalqueVecteur)cqActif_).getPalette());
        pnPal_.setBorneMin(
          ((ZCalqueVecteur)cqActif_).getModeleVecteur().getMinNorme());
        pnPal_.setBorneMax(
          ((ZCalqueVecteur)cqActif_).getModeleVecteur().getMaxNorme());
        pnPal_.setPanneauTailleEnabled(true);
      } else {
        pnPal_.setPalette(new BPaletteCouleurPlage());
        pnPal_.setBorneMin(pnPal_.getPalette().getMinPalette());
        pnPal_.setBorneMax(pnPal_.getPalette().getMaxPalette());
      }
    } catch (NullPointerException e) {}
  }
  /**

   * Modification des propri�t�s de calque courant.

   */
  public void layerPropertyChange(PropertyChangeEvent _evt) {
    if (_evt.getPropertyName().equals("palette")) {
      if (cqActif_ instanceof ZCalqueIso) {
        ((ZCalqueIso)cqActif_).setPalette(pnPal_.getPalette());
        cqActif_.repaint();
      } else if (cqActif_ instanceof ZCalqueVecteur) {
        ((ZCalqueVecteur)cqActif_).setPalette(pnPal_.getPalette());
        cqActif_.repaint();
      }
    } else if (_evt.getPropertyName().equals("optionsIso")) {
      if (cqActif_ instanceof ZCalqueIso) {
        ((ZCalqueIso)cqActif_).setIsolignes(pnIso_.isIsolignesSelected());
        ((ZCalqueIso)cqActif_).setIsosurfaces(pnIso_.isIsosurfacesSelected());
        ((ZCalqueIso)cqActif_).setContour(pnIso_.isMaillageSelected());
        cqActif_.repaint();
      } else if (cqActif_ instanceof ZCalqueVecteur) {
        ((ZCalqueVecteur)cqActif_).setMaillageVisible(
          pnIso_.isMaillageSelected());
        cqActif_.repaint();
      }
    }
  }
  /**

   * Modification des propri�t�s de fenetre.

   */
  public void windowPropertyChange(PropertyChangeEvent _evt) {
    if (_evt.getPropertyName().equals("selectedStep")) {
      for (int i= 0; i < cqsResultats_.size(); i++) {
        BCalque cq= (BCalque)cqsResultats_.get(i);
        if (cq instanceof ZCalqueIso) {
          RefluxModeleResultats mdRes=
            (RefluxModeleResultats) ((ZCalqueIso)cq).getModeleValeur();
          mdRes.setSelectedStep(pnStep_.getSelectedStep());
          if (cq == cqActif_) {
            pnPal_.setPalette(((ZCalqueIso)cqActif_).getPalette());
            pnPal_.setBorneMin(
              ((ZCalqueIso)cqActif_).getModeleValeur().getMin());
            pnPal_.setBorneMax(
              ((ZCalqueIso)cqActif_).getModeleValeur().getMax());
          }
        } else {
          RefluxModeleResultatsVecteur mdRes=
            (RefluxModeleResultatsVecteur) ((ZCalqueVecteur)cq)
              .getModeleVecteur();
          mdRes.setSelectedStep(pnStep_.getSelectedStep());
          if (cq == cqActif_) {
            pnPal_.setPalette(((ZCalqueVecteur)cqActif_).getPalette());
            pnPal_.setBorneMin(
              ((ZCalqueVecteur)cqActif_).getModeleVecteur().getMinNorme());
            pnPal_.setBorneMax(
              ((ZCalqueVecteur)cqActif_).getModeleVecteur().getMaxNorme());
          }
        }
      }
      getVueCalque().repaint();
    }
  }
  // >>> ZSelectionListener ----------------------------------------------------
  public void selectionChanged(ZSelectionEvent _evt) {
    // Info sur le noeud
    if (_evt.getSource() == cqNds_) {
      //EbliListeSelection sel=cqNds_.modeleSelection();
      ZModeleDonnees md= cqNds_.modeleDonnees();
      int[] isels= cqNds_.getSelectedIndex();
      if (cqNds_.isSelectionEmpty()) {
        pnInfo_.setInfo("- Pas de s�lection -");
      } else if (isels.length == 1) {
        int idNd= isels[0];
        GrNoeud nd= (GrNoeud)md.getObject(idNd);
        double x= nd.point_.x_;
        double y= nd.point_.y_;
        String sinfo=
          "** Objet : Noeud\n"
            + "Num�ro : "
            + (idNd + 1)
            + "\n"
            + "X : "
            + (float)x
            + "\n"
            + "Y : "
            + (float)y;
        if (prj_.hasResultats()) {
          PRResultats res= prj_.resultats();
          Double t= pnStep_.getSelectedStep();
          PRResultats.Etape stp= res.getStepAtTime(t.doubleValue());
          if (stp != null) {
            sinfo += "\n\n** R�sultats � t=" + (float)stp.t;
            for (int i= 0; i < stp.getNbColonnes(); i++) {
              sinfo += "\n"
                + Definitions.resultToString(stp.getColonne(i).type)
                + " : "
                + (float)stp.getColonne(i).valeurs[idNd];
            }
          }
        }
        pnInfo_.setInfo(sinfo);
      } else {
        pnInfo_.setInfo("- S�lection multiple -");
      }
    }
  }
  // <<< ZSelectionListener ----------------------------------------------------
  // >>> RefluxDataChangeListener ----------------------------------------------
  public void dataChanged(RefluxDataChangeEvent _evt) {
    if (_evt.type == RefluxDataChangeEvent.RESULTS_CLEARED) {
      pnStep_.clearSteps();
      clearResultats();
    } else if (_evt.type == RefluxDataChangeEvent.RESULT_ADDED) {
      PRResultats res= (PRResultats)_evt.getSource();
      pnStep_.addStep(new Double(res.getStep(res.getNbSteps() - 1).t));
      if (res.getNbSteps() == 1) { // Add layers of results
        ajouteVecteur();
        PRResultats.Etape step= res.getStep(0);
        for (int i= 0; i < step.getNbColonnes(); i++) {
          ajouteResultat(step.getColonne(i).type);
        }
        getArbreCalqueModel().refresh();
      }
    }
  }
  // <<< RefluxDataChangeListener ----------------------------------------------
  /**

   * Cr�ation de la barre d'outils avec les outils de tous les calques.

   */
  private void buildTools() {
    Vector vcps= new Vector();
    BCalque[] cqs= { new BCalqueLegende()};
    for (int i= 0; i < cqs.length; i++) {
      JComponent[] cps= CalqueGuiHelper.CALQUE_OUTILS.getSpecificTools(cqs[i]);
      for (int j= 0; j < cps.length; j++)
        if (!vcps.contains(cps[j])) {
          vcps.addElement(cps[j]);
        }
    }
    // Separator
    vcps.add(null);
    // Panneau de palette de couleurs
    pnPal_= new BPanneauPaletteCouleurPlage();
    pnPal_.addPropertyChangeListener(new PropertyChangeListener() {
      public void propertyChange(PropertyChangeEvent _evt) {
        layerPropertyChange(_evt);
      }
    });
    BuPopupButton btPal=
      new BuPopupButton(EbliResource.EBLI.getString("palette"), pnPal_);
    btPal.setMargin(new Insets(0, 0, 0, 0));
    btPal.setActionCommand("DEFINIRPALETTE");
    btPal.setToolTipText("Palette de couleurs");
    btPal.setIcon(EbliResource.EBLI.getIcon("palettecouleur"));
    btPal.setPaletteResizable(true);
    vcps.add(btPal);
    // Panneau d'options de trac� d'isocouleurs
    pnIso_= new BPanneauOptionsIso();
    pnIso_.addPropertyChangeListener(new PropertyChangeListener() {
      public void propertyChange(PropertyChangeEvent _evt) {
        layerPropertyChange(_evt);
      }
    });
    BuPopupButton btIso=
      new BuPopupButton(EbliResource.EBLI.getString("trac� iso"), pnIso_);
    btIso.setMargin(new Insets(0, 0, 0, 0));
    btIso.setActionCommand("OPTIONSISO");
    btIso.setToolTipText("Options de trac� iso");
    btIso.setIcon(EbliResource.EBLI.getIcon("surface"));
    vcps.add(btIso);
    // Panneau de selection de pas
    pnStep_= new BPanneauSelectedStep();
    pnStep_.addPropertyChangeListener(new PropertyChangeListener() {
      public void propertyChange(PropertyChangeEvent _evt) {
        windowPropertyChange(_evt);
      }
    });
    BuPopupButton btStep= new BuPopupButton("Pas", pnStep_);
    btStep.setMargin(new Insets(0, 0, 0, 0));
    btStep.setActionCommand("SELECTIONNERPAS");
    btStep.setToolTipText("Selection du pas de temps");
    btStep.setIcon(RefluxResource.REFLUX.getIcon("temps"));
    vcps.add(btStep);
    // Separator
    vcps.add(null);
    // Panneau d'information
    pnInfo_= new BPanneauInformation();
    pnInfo_.setInfo("- Pas de s�lection -");
    BuPopupButton btInfo= new BuPopupButton("Information", pnInfo_);
    btInfo.setMargin(new Insets(0, 0, 0, 0));
    btInfo.setActionCommand("INFORMATION");
    btInfo.setToolTipText("Information sur l'objet s�lectionn�");
    btInfo.setIcon(RefluxResource.REFLUX.getIcon("information"));
    btInfo.setPaletteResizable(true);
    vcps.add(btInfo);
    btTools_= (JComponent[])vcps.toArray(new JComponent[0]);
    for (int i= 0; i < btTools_.length; i++)
      if (btTools_[i] != null)
        btTools_[i].setEnabled(true);
  }
}
