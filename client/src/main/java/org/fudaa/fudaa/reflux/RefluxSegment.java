/*
 * @file         PRBoucleTemps.java
 * @creation     2000-03-02
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;

import java.util.Vector;

/**
 * Une classe d�finissant un segment.
 *
 * @version      $Revision: 1.3 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxSegment {
  private int num_;
  public double[] o=new double[2];
  public double[] e=new double[2];
  public double h_;
  public boolean hauto_=true; // Le h est calcul� automatiquement en fonction des autres segments
                               // ou d'un autre multiplan.

  /**
   * Cr�ation d'un segment avec un num�ro donn� unique.
   */
  public RefluxSegment(double[] _o, double[] _e) {
    o=_o;
    e=_e;
  }

  /**
   * Fixe la hauteur sur la ligne (calcul�e ou donn�e par l'utilisateur).
   */
  public void setHauteur(double _h) {
    h_=_h;
  }

  /**
   * Retourne la hauteur sur le segment.
   */
  public double getHauteur() {
    return h_;
  }

  /**
   * D�fini l'automaticit� du calcul de la hauteur.
   */
  public void setHAutomatique(boolean _b) { hauto_=_b; }

  /**
   * Le calcul de h est-il automatique ?
   */
  public boolean isHAutomatique() { return hauto_; }

  /**
   * Affecte le num�ro au segment.
   */
  public void setNum(int i) { num_=i; }

  /**
   * Retourne le num�ro du segment.
   */
  public int getNum() { return num_; }

  /**
   * Retourne un num�ro de segment non encore affect�. (A partir de 1).
   */
  public static int getLastNumber(Vector _sgs) {
    int num=0;
    for (int i=0; i<_sgs.size(); i++) {
      num=Math.max(((RefluxSegment)_sgs.get(i)).num_,num);
    }
    return num+1;
  }
}
