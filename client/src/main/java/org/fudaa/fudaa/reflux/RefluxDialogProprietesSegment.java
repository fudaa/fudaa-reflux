/*
 * @file         RefondeDialogDomainePoreux.java
 * @creation     2003-02-10
 * @modification $Date: 2007-01-19 13:14:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuDialogError;

import org.fudaa.fudaa.commun.impl.FudaaDialog;

/**
 * Affiche/modifie les propri�t�s d'un domaine poreux.
 * 
 * @version $Id: RefluxDialogProprietesSegment.java,v 1.5 2007-01-19 13:14:36 deniger Exp $
 * @author Bertrand Marchand
 */
public class RefluxDialogProprietesSegment extends FudaaDialog {
  Border bdpnProprietes;
  Border bdpnPoint2;
  Border bdpnPoint1;
  Border border1;
  TitledBorder titledBorder1;
  Border border2;
  Border border3;
  TitledBorder titledBorder2;
  Border border4;
  Border border5;
  TitledBorder titledBorder3;
  Border border6;
  Border border7;
  /** Domaine poreux. */
  private RefluxSegment sg_;
  /** Projet */
  // private PRProjet prj_= null;
  /** Retourne le bouton activ�. */
  public JButton reponse;
  private JPanel pnValeur = new JPanel();
  private JLabel lbValeur = new JLabel();
  private JCheckBox cbAutomatique = new JCheckBox();
  private JTextField tfValeur = new JTextField();

  // private JTextField[] tfXPlanPt=new JTextField[3];
  // private JTextField[] tfYPlanPt=new JTextField[3];
  // private JTextField[] tfZPlanPt=new JTextField[3];
  // private JCheckBox[] cbZPlanPt=new JCheckBox[3];
  // RefondePnPointPlan[] pnPlanPt= new RefondePnPointPlan[3];
  // RefondePnPointPlan pnPlanPt1= new RefondePnPointPlan();
  // RefondePnPointPlan pnPlanPt2= new RefondePnPointPlan();
  // RefondePnPointPlan pnPlanPt3= new RefondePnPointPlan();
  /**
   * Cr�ation d'un dialogue sans parent.
   */
  public RefluxDialogProprietesSegment() {
    this(null);
  }

  /**
   * Cr�ation d'un dialogue avec parent.
   */
  public RefluxDialogProprietesSegment(Frame _parent) {
    super(_parent);
    try {
      jbInit();
    } catch (Exception e) {
      e.printStackTrace();
    }
    // pnPlanPt[0]= pnPlanPt1;
    // pnPlanPt[1]= pnPlanPt2;
    // pnPlanPt[2]= pnPlanPt3;
    // tfXPlanPt[0]=tfXPlanPt1;
    // tfXPlanPt[1]=tfXPlanPt2;
    // tfXPlanPt[2]=tfXPlanPt3;
    //
    // tfYPlanPt[0]=tfYPlanPt1;
    // tfYPlanPt[1]=tfYPlanPt2;
    // tfYPlanPt[2]=tfYPlanPt3;
    //
    // tfZPlanPt[0]=tfZPlanPt1;
    // tfZPlanPt[1]=tfZPlanPt2;
    // tfZPlanPt[2]=tfZPlanPt3;
    //
    // cbZPlanPt[0]=cbZPlanPt1;
    // cbZPlanPt[1]=cbZPlanPt2;
    // cbZPlanPt[2]=cbZPlanPt3;
  }

  private void jbInit() throws Exception {
    bdpnPoint1 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), "Premier point saisi"), BorderFactory.createEmptyBorder(5, 5, 5, 5));
    bdpnPoint2 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), "Deuxi�me point saisi"), BorderFactory.createEmptyBorder(5, 5, 5, 5));
    border1 = BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140));
    titledBorder1 = new TitledBorder(border1, "Premier point");
    border2 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), "Deuxi�me point"), BorderFactory.createEmptyBorder(2, 5, 2, 5));
    border3 = BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140));
    titledBorder2 = new TitledBorder(border3, "Premier point");
    border4 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), "Premier point"), BorderFactory.createEmptyBorder(2, 5, 2, 5));
    border5 = BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140));
    titledBorder3 = new TitledBorder(border5, "Deuxi�me point");
    border6 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), "Troisi�me point"), BorderFactory.createEmptyBorder(2, 5, 2, 5));
    border7 = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    this.setTitle("Propri�t�s de la ligne");
    // pnPlan.add(pnPlanPt1, null);
    // pnPlan.add(pnPlanPt2, null);
    // pnPlan.add(pnPlanPt3, null);
    bdpnProprietes = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    // pnPlanPt1.setTitle("Premier point");
    // pnPlanPt2.setTitle("Deuxi�me point");
    // pnPlanPt3.setTitle("Troisi�me point");
    lbValeur.setText("Valeur :");
    cbAutomatique.setHorizontalTextPosition(SwingConstants.LEFT);
    cbAutomatique.setText("Automatique :");
    cbAutomatique.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
    cbAutomatique.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(ItemEvent _e) {
        cbAutomatique_itemStateChanged(_e);
      }
    });
    tfValeur.setPreferredSize(new Dimension(70, 21));
    tfValeur.setText("");
    this.getContentPane().add(pnValeur, BorderLayout.NORTH);
    pnValeur.add(lbValeur, null);
    pnValeur.add(tfValeur, null);
    pnValeur.add(cbAutomatique, null);
    this.pack();
  }

  // private void cbZPlanPt1_itemStateChanged(ItemEvent e) {
  // tfZPlanPt1.setEnabled(!cbZPlanPt1.isSelected());
  // }
  //
  // private void cbZPlanPt2_itemStateChanged(ItemEvent e) {
  // tfZPlanPt2.setEnabled(!cbZPlanPt2.isSelected());
  // }
  //
  // private void cbZPlanPt3_itemStateChanged(ItemEvent e) {
  // tfZPlanPt3.setEnabled(!cbZPlanPt3.isSelected());
  // }
  protected void btOkActionPerformed(ActionEvent _evt) {
    if (majBDD()) {
      reponse = (JButton) _evt.getSource();
      super.btOkActionPerformed(_evt);
    }
  }

  /**
   * Mise � jour de la base de donn�es.
   * 
   * @return <i>true</i> : La mise � jour s'est bien pass�e.
   */
  private boolean majBDD() {
    try {
      if (!cbAutomatique.isSelected()) {
        sg_.setHauteur(Double.parseDouble(tfValeur.getText()));
      }
      sg_.setHAutomatique(cbAutomatique.isSelected());
    } catch (NumberFormatException _exc) {
      new BuDialogError(null, RefluxImplementation.informationsSoftware(), "Un des param�tres n'a pas un format valide")
          .activate();
      return false;
    }
    return true;
  }

  /**
   * Affectation du segment � modifier.
   * 
   * @param _sg Segment.
   */
  public void setSegment(RefluxSegment _sg) {
    sg_ = _sg;
  }

  /**
   * Retourne le domaine poreux affich�.
   * 
   * @return Le domaine poreux �ventuellement modifi�.
   */
  // public RefondeDomainePoreux getDomainePoreux() {
  // return domaine_;
  // }
  /**
   * FD: prj_ ne sert a rien Affectation de la g�om�trie (necessaire pour savoir si les points du domaine poreux sont
   * hors g�om�trie ou non.
   * 
   * @param _prj Le projet refonde.
   */
  public void setProjet(PRProjet _prj) {
  // prj_= _prj;
  }

  /**
   * Visualisation de la fenetre. Surcharg�.
   */
  public void show() {
    if (sg_ != null) {
      if (!sg_.isHAutomatique()) {
        tfValeur.setText("" + sg_.getHauteur());
      } else {
        tfValeur.setText("** Calcul� **");
      }
      cbAutomatique.setSelected(sg_.isHAutomatique());
      setTitle("Propri�t�s de la ligne " + sg_.getNum());
    }
    /*
     * Vector grps=prj_.getGroupes(); coGroupe.removeAllItems(); coGroupe.addItem(""); for (int i=0; i<grps.size();
     * i++) { coGroupe.addItem(""+((RefluxGroupe)grps.get(i)).getNum()); } if (plan_.getGroupe()!=null)
     * coGroupe.setSelectedItem(""+plan_.getGroupe().getNum());
     */
    // if (domaine_ != null) {
    // GrPoint[] pts= domaine_.getPoints();
    // GrPoint[] ptsPlan= domaine_.getPointsPlan();
    // // Propri�t�s
    // tfPorosite.setText("" + domaine_.porosite_);
    // tfFrottement.setText("" + domaine_.coefFrottement_);
    // tfPermeabilite.setText("" + domaine_.permeabilite_);
    // tfCm.setText("" + domaine_.coefMasse_);
    // // Limites
    // tfXmin.setText("" + pts[0].x);
    // tfYmin.setText("" + pts[0].y);
    // tfXmax.setText("" + pts[1].x);
    // tfYmax.setText("" + pts[1].y);
    // // Points du plan
    // for (int i= 0; i < 3; i++) {
    // pnPlanPt[i].setProjet(prj_);
    // pnPlanPt[i].setPoint(ptsPlan[i]);
    // pnPlanPt[i].setZAutomatique(domaine_.isZAutomatique(i));
    // }
    // }
    super.show();
  }

  void cbAutomatique_itemStateChanged(ItemEvent _e) {
    tfValeur.setEnabled(!cbAutomatique.isSelected());
  }

  /**
   * Pour test de la boite de dialogue.
   */
  /*
   * public static void main(String[] _args) { try { UIManager.setLookAndFeel(
   * "com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); } catch (Exception _exc) {} RefondeDomainePoreux dm= new
   * RefondeDomainePoreux(); GrPoint[] pts= new GrPoint[2]; pts[0]= new GrPoint(0, 0, 0); pts[1]= new GrPoint(10, 25,
   * 0); dm.setPoints(pts); dm.setPointsPlan( new GrPoint[] { new GrPoint(1, 2, 3), new GrPoint(4, 5, 6), new GrPoint(7,
   * 8, 9)}); dm.setZAutomatique(false, 0); dm.setZAutomatique(true, 1); RefondeDialogProprietesPoreux di= new
   * RefondeDialogProprietesPoreux(); di.setDomainePoreux(dm); di.show(); System.exit(0); }
   */
}
