/*
 * @file         HY2dInterfaceCalcul.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d2;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.calcul.ICalcul;
import org.fudaa.dodico.corba.mesure.IEvolution;
import org.fudaa.dodico.corba.mesure.IEvolutionConstante;
import org.fudaa.dodico.corba.mesure.IEvolutionSerieIrreguliere;
import org.fudaa.dodico.corba.reflux.*;

import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.reflux.DParametresReflux;
import org.fudaa.dodico.reflux.DResultatsReflux;

import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;

import org.fudaa.fudaa.reflux.*;
/**
 * Transfert du projet vers les structures Reflux courantologie et calcul.
 *
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class HY2dInterfaceCalcul extends PRInterfaceCalcul {
  /**
   * Serveur de calcul.
   */
  private static ICalcul serveur_;
  /**
   * Cr�ation d'une interface de calcul.
   */
  public HY2dInterfaceCalcul() {
    if (serveur_ == null)
      serveur_= Reflux.SERVEUR_REFLUX;
  }
  /**
   * Appel au serveur de calcul Reflux 2D.
   *
   * @param _prj Projet.
   * @exception IllegalArgumentException Erreur de l'application. Des donn�es
   *            sont incoh�rentes ou non initialis�es
   */
  public void calculer(PRProjet _prj) {
    ((ICalculReflux)serveur_).clear(Reflux.CONNEXION_REFLUX);
    class Reader extends Thread {
      private IResultatsReflux ires_;
      private PRResultats res_;
      public Reader(IResultatsReflux _ires, PRResultats _res) {
        ires_= _ires;
        res_= _res;
      }
      public void run() {
        res_.removeAll();
        ires_.initIterator();
        while (ires_.hasIteratorNextStep() || !ires_.isIteratorFilled()) {
          if (ires_.hasIteratorNextStep()) {
            res_.addStep(fromStruct(ires_.nextIteratorStep()));
          } else
            try {
              sleep(100);
            } catch (InterruptedException e) {}
        }
      }
    }
    IResultatsReflux res=
      IResultatsRefluxHelper.narrow(
        serveur_.resultats(Reflux.CONNEXION_REFLUX));
    new Reader(res, _prj.resultats()).start();
    // Transfert vers l'objet parametres
    versParametresReflux(_prj);
    // Calcul
    serveur_.calcul(Reflux.CONNEXION_REFLUX);
  }
  /**
   * Transfert des donn�es du projet vers les fichiers d'entr�es de Reflux.
   *
   * @param _prj Projet � transf�rer sur fichiers d'entr�e.
   * @param _rac Racine des fichiers avec le chemin.
   */
  public void versFichiersEntree(PRProjet _prj, File _rac) throws IOException {
    IParametresReflux par=
      IParametresRefluxHelper.narrow(
        serveur_.parametres(Reflux.CONNEXION_REFLUX));
    String racine;
    String path= _rac.getPath();
    String[] exts= Definitions.getExtFichiersDonnees();
    // Controle de l'extension pass�e
    racine= path;
    for (int i= 0; i < exts.length; i++)
      if (path.endsWith(exts[i])) {
        racine= path.substring(0, path.lastIndexOf(CtuluLibString.DOT+ exts[i]));
        break;
      }
    // Transfert des donn�es vers un objet adapt�
    versParametresReflux(_prj);
    // Ecriture du fichier des parametres
    DParametresReflux.ecritSurFichiers(racine, par);
  }
  /**
   * R�cup�ration des solutions finales depuis un fichier local. Les solutions
   * finales sont les solutions du dernier pas de temps.
   *
   * @param _prj Le projet.
   * @param _file Fichier des solutions finales.
   * @return Les solutions finales.
   */
  public PRSolutionsInitiales[] getSolutionsFinales(PRProjet _prj, File _file)
    throws IOException {
    PRSolutionsInitiales[] r;
    SResultatsReflux res;
    SResultatsEtapeReflux pasRes;
    int nbPas;
    GrMaillageElement mail= _prj.maillage();
    GrNoeud[] nds= mail.noeuds();
    res= DResultatsReflux.litResultatsReflux(_file, nds.length);
    if (res == null || res.etapes == null || res.etapes.length <= 0)
      throw new IOException("Le fichier r�sultat n'est pas compatible avec le projet courant");
    nbPas= res.etapes.length;
    pasRes= res.etapes[nbPas - 1];
    if (pasRes.lignes.length != nds.length
      || pasRes.lignes[0].valeurs.length < 8)
      throw new IOException(
        "Le fichier " + _file + " n'est pas compatible avec le projet courant");
    r= new PRSolutionsInitiales[nds.length];
    for (int i= 0; i < pasRes.lignes.length; i++) {
      SResultatsLigneReflux ndRes= pasRes.lignes[i];
      double u= ndRes.valeurs[3];
      double v= ndRes.valeurs[4];
      double h= ndRes.valeurs[7];
      r[i]= new PRSolutionsInitiales(nds[i], new double[] { u, v, h });
    }
    //    for (int i=0; i<r.length; i++) {
    //      System.out.println("Noeud : "+i+" "+r[i].valeur()+" "+_prj.conditionsInitiales()[i].valeur());
    //    }
    return r;
  }
  /**
   * R�cup�ration des solutions finales sur le serveur. Les solutions finales
   * sont les solutions du dernier pas de temps.
   *
   * @param _prj Le projet.
   * @return Les solutions finales.
   */
  public PRSolutionsInitiales[] getSolutionsFinales(PRProjet _prj)
    throws IOException {
    PRSolutionsInitiales[] r;
    IResultatsReflux ires;
    SResultatsReflux res;
    SResultatsEtapeReflux pasRes;
    int nbPas;
    GrMaillageElement mail= _prj.maillage();
    GrNoeud[] nds= mail.noeuds();
    IParametresReflux pr=
      IParametresRefluxHelper.narrow(
        serveur_.parametres(Reflux.CONNEXION_REFLUX));
    ires=
      IResultatsRefluxHelper.narrow(
        serveur_.resultats(Reflux.CONNEXION_REFLUX));
    res= ires.resultatsReflux();
    pr.setRacine(new File(_prj.racineFichiers()).getName(), ires);
    if (res == null || res.etapes == null || res.etapes.length <= 0)
      throw new IOException(
        "Serveur de calcul : Un des fichiers r�sultats est manquant\n"
          + "ou n'est pas compatible avec le projet courant");
    nbPas= res.etapes.length;
    pasRes= res.etapes[nbPas - 1];
    if (pasRes.lignes.length != nds.length
      || pasRes.lignes[0].valeurs.length < 8)
      throw new IOException(
        "Serveur de calcul : Un des fichiers r�sultats est manquant\n"
          + "ou n'est pas compatible avec le projet courant");
    r= new PRSolutionsInitiales[nds.length];
    for (int i= 0; i < pasRes.lignes.length; i++) {
      SResultatsLigneReflux ndRes= pasRes.lignes[i];
      double u= ndRes.valeurs[3];
      double v= ndRes.valeurs[4];
      double h= ndRes.valeurs[7];
      r[i]= new PRSolutionsInitiales(nds[i], new double[] { u, v, h });
    }
    return r;
  }
  /**
   * Create a PRResults.Etape from SResultatsEtapeReflux. Norm is added.
   */
  public static PRResultats.Etape fromStruct(SResultatsEtapeReflux _step) {
    PRResultats.Etape r= null;
    final int[] tpResults=
      {
        PRResultats.COORD_X,
        PRResultats.COORD_Y,
        PRResultats.BATHYMETRIE,
        PRResultats.VITESSE_X,
        PRResultats.VITESSE_Y,
        -1,
        PRResultats.NIVEAU_EAU,
        PRResultats.PROFONDEUR,
        -1 };
    PRResultats.Etape.Colonne[] cols= new PRResultats.Etape.Colonne[7];
    int nbCols= 0;
    for (int j= 0; j < 9; j++) {
      if (tpResults[j] == -1)
        continue; // On passe les colonnes sans r�sultats.
      double[] vals= new double[_step.lignes.length];
      for (int k= 0; k < vals.length; k++)
        vals[k]= _step.lignes[k].valeurs[j];
      cols[nbCols++]= new PRResultats.Etape.Colonne(tpResults[j], vals);
    }
    r= new PRResultats.Etape(_step.instant, cols);
    // Add vitesse norm
    PRResultats.Etape.Colonne vx= r.getColonneOfType(PRResultats.VITESSE_X);
    PRResultats.Etape.Colonne vy= r.getColonneOfType(PRResultats.VITESSE_Y);
    PRResultats.Etape.Colonne norm=
      new PRResultats.Etape.Colonne(
        PRResultats.NORME,
        new double[vx.valeurs.length]);
    for (int j= 0; j < norm.valeurs.length; j++) {
      norm.valeurs[j]=
        Math.sqrt(
          vx.valeurs[j] * vx.valeurs[j] + vy.valeurs[j] * vy.valeurs[j]);
    }
    r.add(norm);
    return r;
  }
  /**
   * Create a SResultatsEtapeReflux from PRResults.Etape. Norm is suppressed
   * and 2 null columns are added to be compatible with standard result files.
   */
  public static SResultatsEtapeReflux toStruct(PRResultats.Etape _step) {
    final int[] tpResults=
      {
        PRResultats.COORD_X,
        PRResultats.COORD_Y,
        PRResultats.BATHYMETRIE,
        PRResultats.VITESSE_X,
        PRResultats.VITESSE_Y,
        -1,
        PRResultats.NIVEAU_EAU,
        PRResultats.PROFONDEUR,
        -1 };
    PRResultats.Etape.Colonne[] cols=
      new PRResultats.Etape.Colonne[tpResults.length];
    int nbLines= 0;
    for (int i= 0; i < cols.length; i++) {
      if (tpResults[i] != -1)
        cols[i]= _step.getColonneOfType(tpResults[i]);
      if (cols[i] != null)
        nbLines= cols[i].valeurs.length;
    }
    SResultatsLigneReflux[] lines= new SResultatsLigneReflux[nbLines];
    for (int il= 0; il < nbLines; il++) {
      double[] vals= new double[tpResults.length];
      for (int ic= 0; ic < tpResults.length; ic++)
        if (cols[ic] != null)
          vals[ic]= cols[ic].valeurs[il];
      lines[il]= new SResultatsLigneReflux(il + 1, vals);
    }
    return new SResultatsEtapeReflux(_step.t, lines);
  }
  /**
   * Retourne les r�sultats nodaux.
   *
   * @param _prj Le projet.
   * @param _racine La racine des fichiers du projet.
   * @return Les r�sultats nodaux.
   */
  public PRResultats getResultats(PRProjet _prj, File _racine)
    throws IOException {
    SResultatsReflux res;
    SResultatsEtapeReflux pasRes;
    int nbPas;
    //int nbRes;
    int nbCols;
    final int[] tpResults=
      {
        PRResultats.COORD_X,
        PRResultats.COORD_Y,
        PRResultats.BATHYMETRIE,
        PRResultats.VITESSE_X,
        PRResultats.VITESSE_Y,
        -1,
        PRResultats.NIVEAU_EAU,
        PRResultats.PROFONDEUR,
        -1 };
    GrMaillageElement mail= _prj.maillage();
    GrNoeud[] nds= mail.noeuds();
    File file= new File(_racine.getPath() + ".sov");
    res= DResultatsReflux.litResultatsReflux(file, nds.length);
    nbPas= res.etapes.length;
    pasRes= res.etapes[nbPas - 1];
    if (pasRes.lignes.length != nds.length
      || (/*nbRes= */pasRes.lignes[0].valeurs.length) < 8)
      throw new IOException(
        "Le fichier " + file + " n'est pas compatible avec le projet courant");
    // Stockage des valeurs dans le format ad�quat.
    PRResultats.Etape[] etapes= new PRResultats.Etape[res.etapes.length];
    for (int i= 0; i < res.etapes.length; i++) {
      PRResultats.Etape.Colonne[] cols= new PRResultats.Etape.Colonne[7];
      nbCols= 0;
      for (int j= 0; j < 9; j++) {
        if (tpResults[j] == -1)
          continue; // On passe les colonnes sans r�sultats.
        double[] vals= new double[res.etapes[i].lignes.length];
        for (int k= 0; k < vals.length; k++)
          vals[k]= res.etapes[i].lignes[k].valeurs[j];
        cols[nbCols++]= new PRResultats.Etape.Colonne(tpResults[j], vals);
      }
      etapes[i]= new PRResultats.Etape(res.etapes[i].instant, cols);
      // Add vitesse norm
      PRResultats.Etape.Colonne vx=
        etapes[i].getColonneOfType(PRResultats.VITESSE_X);
      PRResultats.Etape.Colonne vy=
        etapes[i].getColonneOfType(PRResultats.VITESSE_Y);
      PRResultats.Etape.Colonne norm=
        new PRResultats.Etape.Colonne(
          PRResultats.NORME,
          new double[vx.valeurs.length]);
      for (int j= 0; j < norm.valeurs.length; j++) {
        norm.valeurs[j]=
          Math.sqrt(
            vx.valeurs[j] * vx.valeurs[j] + vy.valeurs[j] * vy.valeurs[j]);
      }
      etapes[i].add(norm);
    }
    return new PRResultats(etapes);
  }
  /**
   * Retourne si le serveur a pu �tre joint.
   * @return <i>true</i> Le serveur est pr�sent.
   *         <i>false</i> Le serveur est inexistant
   */
  public boolean serveurExiste() {
    return serveur_ != null && ((ICalculReflux)serveur_).estOperationnel();
  }
  /**
   * Retourne si le calcul s'est bien d�roul�.
   * @return <i>true</i> : Ok, <i>false</i> : Probl�me.
   */
  public boolean calculEstOK() {
    return serveurExiste() ? ((ICalculReflux)serveur_).estOK() : false;
  }
  /**
   * Retourne la trace d'ex�cution du calcul.
   * @return La chaine d'ex�cution.
   */
  public String calculTraceExecution() {
    return serveurExiste() ? ((ICalculReflux)serveur_).traceExecution() : null;
  }
  /**
   * Transfert de la BDD vers un objet parametres adapt� au calcul.
   * @param _prj
   */
  private void versParametresReflux(PRProjet _prj)
    throws IllegalArgumentException {
    IParametresReflux par=
      IParametresRefluxHelper.narrow(
        serveur_.parametres(Reflux.CONNEXION_REFLUX));
    IResultatsReflux res=
      IResultatsRefluxHelper.narrow(
        serveur_.resultats(Reflux.CONNEXION_REFLUX));
    // Racine des fichiers calcul
    par.setRacine((new File(_prj.racineFichiers())).getName(), res);
    GrMaillageElement mail= _prj.maillage();
    PRModeleCalcul mdlCal= _prj.modeleCalcul();
    PRModeleProprietes mdlPrp= _prj.modeleProprietes();
    PRSollicitation[] sos= _prj.sollicitations();
    // Nombre de noeuds du probleme
    int nbNoeuds= mail.noeuds().length;
    // Nombre d'�l�ments du probleme
    int nbElements= mail.elements().length;
    // Nombre de propri�t�s �l�mentaires pour les �l�ments de fond suivant le pb
    //    int nbPEFond = 0;
    //    if      (type()==VIT_NU_CON) nbPEFond=3;
    //    else if (type()==VIT_NU_LMG) nbPEFond=4;
    int nbPEFond= 4;
    double calDeb;
    double calFin;
    Hashtable nd2Num= new Hashtable();
    {
      GrNoeud[] nds= mail.noeuds();
      for (int i= 0; i < nds.length; i++)
        nd2Num.put(nds[i], new Integer(i));
    }
    Hashtable el2Num= new Hashtable();
    {
      GrElement[] els= mail.elements();
      for (int i= 0; i < els.length; i++)
        el2Num.put(els[i], new Integer(i));
    }
    PRGroupePT[] grpPTs= mdlCal.groupesPT();
    //--------------------------------------------------------------------------
    //
    //      Transfert des informations du probleme en parametres REFLUX
    //
    //--------------------------------------------------------------------------
    SParametresRefluxINP paramINP= new SParametresRefluxINP();
    // Parametres INP
    //---  D�termination de la dur�e du calcul pour controle de l'�tendue des
    //     courbes non stationnaires
    calDeb= Double.POSITIVE_INFINITY;
    calFin= Double.NEGATIVE_INFINITY;
    if (grpPTs[0].schema().type() != PRSchemaResolution.STATIONNAIRE) {
      calDeb= grpPTs[0].temps().debut() + grpPTs[0].temps().pas();
      calFin= grpPTs[grpPTs.length - 1].temps().fin();
    }
    //    for (int i=0; i<grpPTs.length; i++) {
    //      if (grpPTs[i].schema().type()!=PRSchemaResolution.STATIONNAIRE) {
    //        calDeb=Math.min(calDeb,grpPTs[i].temps().debut());
    //        calFin=Math.max(calFin,grpPTs[i].temps().fin());
    //      }
    //    }
    //--------------------------------------------------------------------------
    //---  Definition des groupes de PREL / Numeros de groupes  ----------------
    //---  de PREL par �l�ment / Type des �l�ments / connectivit�s  ------------
    //--------------------------------------------------------------------------
    {
      GrElement[] elements= mail.elements();
      //GrNoeud[]     noeuds  =mail.noeuds();
      GrElement[][] bords= mail.aretesContours();
      PRPropriete[] gPE; // Groupe de P.E. courant
      // Table d'indexage direct element => Propri�t�s �l�mentaires
      PRPropriete[][] elementsPE= new PRPropriete[nbElements][nbPEFond];
      int[] connectivite;
      double[] valeurs; // Valeurs des P.E.
      int cpt; // Compteur
      Vector natsBds= mdlPrp.naturesBords();
      PRPropriete[] prpsBds= mdlPrp.proprietesAretes();
      Vector rePREL= new Vector();
      // Groupes de P.E. (temporaire : contient des gPE)
      Vector reCONN= new Vector(nbElements); // Connectivit� des �lements
      Vector reNGPE= new Vector(nbElements);
      // Num�ros de groupe de P.E. par �l�ment
      Vector reTPEL= new Vector(nbElements); // Types des �l�ments
      GrNoeud[] nds;
      PRNature nature;
      //      IEvolutionConstante
      //                courbe;
      Hashtable bordToNature; // IArete->PRNature
      Hashtable bordToPA; // IArete->PRPropriete
      PRPropriete // Propri�t� d'ar�te
      pA;
      Object[] supports;
      // Cr�ation d'une propri�t� �l�mentaire bidon = 0
      IEvolutionConstante crb=UsineLib.findUsine().creeMesureEvolutionConstante();
/*        IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
      crb.constante(0);
      PRPropriete peZero=
        new PRPropriete(PRPropriete.PERTE_CHARGE, crb, new Vector());
      //---  Elements de fond  -------------------------------------------------
      //--- Remplissage de la table d'indexage direct
      {
        PRPropriete[] pe= mdlPrp.proprietesElements();
        int indice;
        for (int i= 0; i < pe.length; i++) {
          supports= pe[i].supports().toArray();
          for (int j= 0; j < supports.length; j++) {
            switch (pe[i].type()) {
              case PRPropriete.VISCOSITE :
                indice= 0;
                break;
              case PRPropriete.RUGOSITE :
                indice= 1;
                break;
              case PRPropriete.ALPHA_LONGUEUR_MELANGE :
                indice= 2;
                break;
              case PRPropriete.PERTE_CHARGE :
                indice= 3;
                break;
              default :
                indice= 0;
                break;
            }
            // Remplissage de la table
            elementsPE[((Integer)el2Num.get(supports[j])).intValue()][indice]=
              pe[i];
          }
        }
      }
      //--- Propri�t� bidon pour le bon d�roulement de la suite des op�rations
      for (int i= 0; i < nbElements; i++) {
        if (_prj.type() == PRProjet.VIT_NU_CON)
          elementsPE[i][2]= peZero;
        if (elementsPE[i][3] == null)
          elementsPE[i][3]= peZero;
      }
      //--- Controle que tous les �l�ments ont bien leurs propri�t�s affect�es
      for (int i= 0; i < nbElements; i++) {
        for (int j= 0; j < nbPEFond; j++) {
          if (elementsPE[i][j] == null)
            throw new IllegalArgumentException(
              "L'�l�ment "
                + (i + 1)
                + " a une propri�t� �l�mentaire non affect�e");
        }
      }
      //--- Cr�ation des groupes de propri�t�s
      ELEMENTS : for (int i= 0; i < nbElements; i++) {
        // Recherche d'un groupe de P.E. existant
        GPE : for (int j= 0; j < rePREL.size(); j++) {
          gPE= (PRPropriete[])rePREL.elementAt(j);
          for (int k= 0; k < nbPEFond; k++) {
            if (((IEvolutionConstante)gPE[k].evolution()).constante()
              != ((IEvolutionConstante)elementsPE[i][k].evolution()).constante())
              continue GPE;
          }
          // Groupe trouv� => On affecte son num�ro � l'�l�ment
          reNGPE.addElement(new Integer(j + 1));
          continue ELEMENTS;
        }
        // Groupe inexistant => Nouveau groupe
        gPE= new PRPropriete[nbPEFond];
        for (int k= 0; k < nbPEFond; k++)
          gPE[k]= elementsPE[i][k];
        rePREL.addElement(gPE);
        // Affectation du num�ro de groupe de P.E.
        reNGPE.addElement(new Integer(rePREL.size()));
      }
      //--- Types des �l�ments de fond
      for (int i= 0; i < nbElements; i++) {
        if (mail.elements()[i].type_ == GrElement.T6) {
          reTPEL.addElement(new Integer(5));
        } else
          throw new IllegalArgumentException(
            "Le type d'�l�ment "
              + mail.elements()[i].type_
              + "n'est pas trait� dans Reflux");
      }
      //---  Connectivit�s
      for (int i= 0; i < elements.length; i++) {
        nds= elements[i].noeuds_;
        connectivite= new int[nds.length];
        for (int j= 0; j < nds.length; j++)
          connectivite[j]= ((Integer)nd2Num.get(nds[j])).intValue();
        reCONN.addElement(connectivite);
      }
      //---  Elements de bord  -------------------------------------------------
      // Bord->Nature
      bordToNature= new Hashtable(natsBds.size() > 0 ? natsBds.size() : 1);
      for (Enumeration e= natsBds.elements(); e.hasMoreElements();) {
        nature= (PRNature)e.nextElement();
        bordToNature.put(nature.supports().get(0), nature);
      }
      // Bord->Propri�t� d'ar�te
      bordToPA= new Hashtable();
      for (int i= 0; i < prpsBds.length; i++) {
        pA= prpsBds[i];
        supports= pA.supports().toArray();
        for (int j= 0; j < supports.length; j++)
          bordToPA.put(supports[j], pA);
      }
      //---  R�cup�ration des informations
      for (int i= 0; i < bords.length; i++) {
        for (int j= 0; j < bords[i].length; j++) {
          nature= (PRNature)bordToNature.get(bords[i][j]);
          nds= bords[i][j].noeuds_;
          connectivite= new int[nds.length];
          for (int k= 0; k < nds.length; k++)
            connectivite[k]= ((Integer)nd2Num.get(nds[k])).intValue();
          // Element de type d�bit
          if (nature.type() == PRNature.BORD_OUVERT_DEBIT) {
            reCONN.addElement(connectivite);
            reTPEL.addElement(new Integer(7));
            reNGPE.addElement(new Integer(0));
          }
          // Element de type vitesse / ouvert libre
          else if (
            nature.type() == PRNature.BORD_OUVERT_VITESSE
              || nature.type() == PRNature.BORD_OUVERT_LIBRE) {
            reCONN.addElement(connectivite);
            reTPEL.addElement(new Integer(6));
            reNGPE.addElement(new Integer(0));
          }
          // Element de type frottement
          else if (nature.type() == PRNature.BORD_FERME_FROTTEMENT) {
            pA= (PRPropriete)bordToPA.get(bords[i][j]);
            if (pA == null)
              throw new IllegalArgumentException(
                "Le bord "
                  + i
                  + " de nature FERME_FROTTEMENT ne "
                  + "comporte pas de valeur de propriete d'arete");
            // Recherche d'un groupe de P.E. existant (1 P.E.)
            boolean found= false;
            int k;
            for (k= 0; k < rePREL.size(); k++) {
              gPE= (PRPropriete[])rePREL.elementAt(k);
              if (gPE.length == 1 && gPE[0] == pA) {
                found= true;
                break;
              }
            }
            // Groupe trouv� => On affecte son num�ro � l'�l�ment
            if (found)
              reNGPE.addElement(new Integer(k + 1));
            // Groupe inexistant => Nouveau groupe
            else {
              gPE= new PRPropriete[1];
              gPE[0]= pA;
              rePREL.addElement(gPE);
              reNGPE.addElement(new Integer(rePREL.size()));
            }
            reCONN.addElement(connectivite);
            reTPEL.addElement(new Integer(8));
          }
        }
      }
      //---  Propri�t� �l�mentaire suppl�mentaire pour les bancs  --------------
      //     couvrants/d�couvrants
      for (int i= 0; i < grpPTs.length; i++) {
        PRMethodeResolution meth= grpPTs[i].methode();
        if (meth.type() == PRMethodeResolution.NEWTON_RAPHSON_BCD
          || meth.type() == PRMethodeResolution.SELECTED_LUMPING_BCD) {
          double[] coefs= meth.coefficients();
          IEvolutionConstante evol=UsineLib.findUsine().creeMesureEvolutionConstante();
/*            IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
          PRPropriete pe= new PRPropriete();
          if (meth.type() == PRMethodeResolution.NEWTON_RAPHSON_BCD)
            evol.constante(coefs[4]);
          else
            evol.constante(coefs[1]);
          pe.type(PRPropriete.VISCOSITE);
          pe.evolution(evol);
          rePREL.add(new PRPropriete[] { pe, peZero, peZero, peZero });
          break;
        }
      }
      //---  Remplissage de la structure  --------------------------------------
      // Groupes de PE
      cpt= 0;
      paramINP.groupesPE= new SParametresRefluxGroupePE[rePREL.size()];
      for (Enumeration e= rePREL.elements(); e.hasMoreElements();) {
        gPE= (PRPropriete[])e.nextElement();
        valeurs= new double[gPE.length];
        for (int j= 0; j < gPE.length; j++)
          valeurs[j]= ((IEvolutionConstante)gPE[j].evolution()).constante();
        paramINP.groupesPE[cpt++]= new SParametresRefluxGroupePE(valeurs);
      }
      // Numero de groupes de PE
      cpt= 0;
      paramINP.numeroGroupePE= new int[reNGPE.size()];
      for (Enumeration e= reNGPE.elements(); e.hasMoreElements();)
        paramINP.numeroGroupePE[cpt++]= ((Integer)e.nextElement()).intValue();
      // Types des �l�ments
      cpt= 0;
      paramINP.typesElement= new int[reTPEL.size()];
      for (Enumeration e= reTPEL.elements(); e.hasMoreElements();)
        paramINP.typesElement[cpt++]= ((Integer)e.nextElement()).intValue();
      // Connectivit�s
      cpt= 0;
      paramINP.connectivites= new SParametresRefluxConnectivite[reCONN.size()];
      for (Enumeration e= reCONN.elements(); e.hasMoreElements();)
        paramINP.connectivites[cpt++]=
          new SParametresRefluxConnectivite((int[])e.nextElement());
    }
    // Type de projet
    if (_prj.type() == PRProjet.VIT_NU_CON)
      paramINP.typeProbleme= 0;
    else if (_prj.type() == PRProjet.VIT_NU_LMG)
      paramINP.typeProbleme= 1;
    // Coordonn�es des noeuds. La renum�rotation/optimisation doit avoir �t�
    // effectu�e si on ne veut pas de d�passement de tableau.
    // B.M. Il serait peut �tre judicieux de controler si ca a �t� fait.
    {
      GrNoeud[] noeuds= mail.noeuds();
      //double[] coordonnees = new double[3];
      paramINP.coordonnees= new SParametresRefluxCoordonnees[noeuds.length];
      for (int i= 0; i < noeuds.length; i++) {
        GrPoint pt= noeuds[i].point_;
        //        paramINP.coordonnees[noeuds[i].numero()] =
        paramINP.coordonnees[i]=
          new SParametresRefluxCoordonnees(pt.x_, pt.y_, pt.z_);
      }
    }
    // Nombre de degr�s de libert� par noeuds
    {
      GrElement[] elements= mail.elements();
      int[] nombresDLN= new int[nbNoeuds];
      GrNoeud[] noeudsSommets;
      GrNoeud[] noeudsMilieux;
      for (int i= 0; i < elements.length; i++) {
        noeudsSommets= elements[i].noeudsSommets();
        for (int j= 0; j < noeudsSommets.length; j++)
          nombresDLN[((Integer)nd2Num.get(noeudsSommets[j])).intValue()]= 3;
        noeudsMilieux= elements[i].noeudsMilieux();
        for (int j= 0; j < noeudsMilieux.length; j++)
          nombresDLN[((Integer)nd2Num.get(noeudsMilieux[j])).intValue()]= 2;
      }
      paramINP.nombresDLN= nombresDLN;
    }
    // Conditions limites
    {
      PRConditionLimite[] cLs= _prj.conditionsLimites();
      SParametresRefluxConditionLimite[] refluxCLs=
        new SParametresRefluxConditionLimite[cLs.length];
      int[] codes= null;
      int code= 0;
      double[] valeurs= null;
      double valeur= 0;
      Vector supports= null;
      int[] tmpNumSupports= null;
      int[] numSupports= null;
      GrNoeud noeud= null;
      int nbSupports;
      int nbCLs= 0;
      // Pr�sence de conditions limites transitoires
      paramINP.bCLTransitoires= false;
      // Pour chaque C.L.
      for (int i= 0; i < cLs.length; i++) {
        // Pour chaque support
        supports= cLs[i].supports();
        tmpNumSupports= new int[supports.size()];
        nbSupports= 0;
        for (int j= 0; j < supports.size(); j++) {
          // Le support n'est pas un noeud => On le passe
          if (!(supports.get(j) instanceof GrNoeud))
            continue;
          noeud= (GrNoeud)supports.get(j);
          // Dans le cas d'une condition limite hauteur
          if (cLs[i].type() == PRConditionLimite.HAUTEUR) {
            // Noeud milieu => On le passe
            if (paramINP.nombresDLN[((Integer)nd2Num.get(noeud)).intValue()]
              == 2)
              continue;
          }
          tmpNumSupports[nbSupports]= ((Integer)nd2Num.get(noeud)).intValue();
          nbSupports++;
        }
        // Si aucun noeud support => On passe
        if (nbSupports == 0)
          continue;
        numSupports= new int[nbSupports];
        System.arraycopy(tmpNumSupports, 0, numSupports, 0, nbSupports);
        // Codes / valeurs stationnaires
        if (cLs[i].evolution() instanceof IEvolutionConstante) {
          code= 2;
          valeur= ((IEvolutionConstante)cLs[i].evolution()).constante();
        }
        // Codes / valeurs transitoires
        else {
          code= 1;
          valeur= 0.;
          double crbDeb= cLs[i].evolution().debut();
          double crbFin= cLs[i].evolution().fin();
          if (crbDeb > calDeb || crbFin < calFin)
            throw new IllegalArgumentException(
              "La courbe '"
                + _prj.getName(cLs[i].evolution())
                + "' n'est pas assez �tendue\n"
                + "Courbe: {"
                + crbDeb
                + ";"
                + crbFin
                + "} ; Calcul: {"
                + calDeb
                + ";"
                + calFin
                + "}");
          paramINP.bCLTransitoires= true;
        }
        if (cLs[i].type() == PRConditionLimite.VITESSE_X) {
          codes= new int[] { code, 0, 0 };
          valeurs= new double[] { valeur, 0., 0. };
        } else if (cLs[i].type() == PRConditionLimite.VITESSE_Y) {
          codes= new int[] { 0, code, 0 };
          valeurs= new double[] { 0., valeur, 0. };
        } else if (cLs[i].type() == PRConditionLimite.HAUTEUR) {
          codes= new int[] { 0, 0, code };
          valeurs= new double[] { 0., 0., valeur };
        }
        // Condition limite Reflux
        refluxCLs[nbCLs]=
          new SParametresRefluxConditionLimite(codes, valeurs, numSupports);
        nbCLs++;
      }
      // Remplissage du tableau des conditions limites
      //      if (nbCLs != 0) {
      paramINP.conditionsLimites= new SParametresRefluxConditionLimite[nbCLs];
      System.arraycopy(refluxCLs, 0, paramINP.conditionsLimites, 0, nbCLs);
      //      }
    }
    // Propri�t�s nodales
    {
      int code= 0;
      double valeur= 0;
      paramINP.proprietesNodales=
        new SParametresRefluxProprieteNodale[nbNoeuds];
      GrNoeud support= null;
      Vector supports= null;
      PRNormale[] normales= mdlPrp.normales();
      // Pr�sence de propri�t�s nodales transitoires
      paramINP.bPNTransitoires= false;
      // Cr�ation des propri�t�s
      for (int i= 0; i < nbNoeuds; i++) {
        paramINP.proprietesNodales[i]=
          new SParametresRefluxProprieteNodale(
            new int[] { 0, 0 },
            new double[] { 0., 0. });
      }
      // Initialisation de la 1�re propri�t� nodale (normale)
      for (int i= 0; i < normales.length; i++) {
        //        if (! (this.normales_[i].support() instanceof GrNoeud)) continue;
        support= normales[i].support();
        int num= ((Integer)nd2Num.get(support)).intValue();
        paramINP.proprietesNodales[num].code[0]= 2;
        paramINP.proprietesNodales[num].valeur[0]= normales[i].valeur();
      }
      // Initialisation de la 2�me propri�t� nodale (d�bit lin�aire)
      for (int i= 0; i < sos.length; i++) {
        // On ne retient que les sollicitations de type debit
        if (sos[i].type() != PRSollicitation.DEBIT)
          continue;
        supports= sos[i].supports();
        for (int j= 0; j < supports.size(); j++) {
          // On ne traite que les supports de type GrNoeud
          if (!(supports.get(j) instanceof GrNoeud))
            continue;
          support= (GrNoeud)supports.get(j);
          // Codes / valeurs stationnaires
          if (sos[i].evolution() instanceof IEvolutionConstante) {
            code= 2;
            valeur= ((IEvolutionConstante)sos[i].evolution()).constante();
          }
          // Codes / valeurs transitoires
          else {
            code= 1;
            valeur= 0.;
            if (sos[i].evolution().debut() > calDeb
              || sos[i].evolution().fin() < calFin)
              throw new IllegalArgumentException(
                "La courbe '"
                  + _prj.getName(sos[i].evolution())
                  + "' n'est pas assez �tendue");
            paramINP.bPNTransitoires= true;
          }
          int num= ((Integer)nd2Num.get(support)).intValue();
          paramINP.proprietesNodales[num].code[1]= code;
          paramINP.proprietesNodales[num].valeur[1]= valeur;
        }
      }
    }
    //--- Nombre maxi de noeuds par element
    paramINP.nombreMaxiNoeudsElement= 0;
    for (int i= 0; i < paramINP.connectivites.length; i++) {
      paramINP.nombreMaxiNoeudsElement=
        Math.max(
          paramINP.nombreMaxiNoeudsElement,
          paramINP.connectivites[i].numeroNoeud.length);
    }
    //--- Sollicitations r�parties
    paramINP.bSollicitationsReparties= false;
    for (int i= 0; i < sos.length; i++) {
      if (sos[i].type() == PRSollicitation.DEBIT) {
        paramINP.bSollicitationsReparties= true;
        break;
      }
    }
    //--- Groupes de pas de temps
    {
      // Nombre de pas de temps
      int reNbPT;
      // Valeur du pas de temps
      double rePas;
      // Type du sch�ma
      int reSchema;
      // Coefficient du sch�ma
      double reCoefSchema;
      // Type de m�thode
      int reMethode;
      // Coefficient de relaxation
      double reMethodeRelaxation;
      // Coefficient de pr�cision NW/R
      double reMethodePrecisionNR;
      // Coefficient de pr�cision BCD
      double reMethodePrecisionBCD;
      // Nombre max d'it�rations
      int reMethodeNbMaxIterations;
      // Coefficients de contribution
      double[] reCoefCont= new double[9];
      // Fr�quence de stockage
      int reFrequence;
      double[] coefs;
      paramINP.groupesPT= new SParametresRefluxGroupePT[grpPTs.length];
      // Coefficients de pond�ration.
      Object[] paramsCalcul= mdlCal.parametresCalcul();
      for (int i= 0; i < reCoefCont.length; i++)
        reCoefCont[i]= ((Double)paramsCalcul[i]).doubleValue();
      //      System.arraycopy(mdlCal.parametresCalcul(),0,reCoefCont,0,reCoefCont.length);
      for (int i= 0; i < grpPTs.length; i++) {
        if (grpPTs[i].schema().type() == PRSchemaResolution.STATIONNAIRE) {
          reNbPT= 1;
          rePas= 1;
        } else {
          reNbPT= grpPTs[i].temps().nbPas();
          rePas= grpPTs[i].temps().pas();
        }
        reSchema= grpPTs[i].schema().type();
        reCoefSchema= grpPTs[i].schema().coefficient();
        reMethode= grpPTs[i].methode().type();
        //        reCoefCont  =coefsCalcul_;
        reFrequence= grpPTs[i].frequenceStockage();
        // Param�tres de la m�thode
        coefs= grpPTs[i].methode().coefficients();
        if (grpPTs[i].methode().type() == PRMethodeResolution.NEWTON_RAPHSON) {
          reMethodeRelaxation= coefs[0];
          reMethodePrecisionNR= coefs[1];
          reMethodePrecisionBCD= 0.005;
          // Inutile, mais initialis�>0 (sinon erreur Reflux)
          reMethodeNbMaxIterations= (int)coefs[2];
        } else if (
          grpPTs[i].methode().type()
            == PRMethodeResolution.NEWTON_RAPHSON_LMG) {
          reMethodeRelaxation= coefs[0];
          reMethodePrecisionNR= coefs[1];
          reMethodePrecisionBCD= 0.005;
          // Inutile, mais initialis�>0 (sinon erreur Reflux)
          reMethodeNbMaxIterations= (int)coefs[2];
        } else if (
          grpPTs[i].methode().type()
            == PRMethodeResolution.NEWTON_RAPHSON_BCD) {
          reMethodeRelaxation= coefs[0];
          reMethodePrecisionNR= coefs[1];
          reMethodePrecisionBCD= coefs[2];
          reMethodeNbMaxIterations= (int)coefs[3];
        } else if (
          grpPTs[i].methode().type()
            == PRMethodeResolution.SELECTED_LUMPING_BCD) {
          reMethodeRelaxation= 1;
          // Inutile, mais initialis�>0 (sinon erreur Reflux)
          reMethodePrecisionNR= 0.001;
          // Inutile, mais initialis�>0 (sinon erreur Reflux)
          reMethodePrecisionBCD= coefs[0];
          reMethodeNbMaxIterations= 2;
          // Inutile, mais initialis�>0 (sinon erreur Reflux)
        } else { // SELECTED_LUMPING
          reMethodeRelaxation= 1;
          // Inutile, mais initialis�>0 (sinon erreur Reflux)
          reMethodePrecisionNR= 0.001;
          // Inutile, mais initialis�>0 (sinon erreur Reflux)
          reMethodePrecisionBCD= 0.05;
          // Inutile, mais initialis�>0 (sinon erreur Reflux)
          reMethodeNbMaxIterations= 2;
          // Inutile, mais initialis�>0 (sinon erreur Reflux)
        }
        // Initialisation du groupe
        paramINP.groupesPT[i]= new SParametresRefluxGroupePT();
        paramINP.groupesPT[i].nombrePasDeTemps= reNbPT;
        paramINP.groupesPT[i].valeurPasDeTemps= rePas;
        paramINP.groupesPT[i].schemaResolution= reSchema;
        paramINP.groupesPT[i].coefficientSchema= reCoefSchema;
        paramINP.groupesPT[i].methodeResolution= reMethode;
        paramINP.groupesPT[i].relaxation= reMethodeRelaxation;
        paramINP.groupesPT[i].precisionNR= reMethodePrecisionNR;
        paramINP.groupesPT[i].precisionBCD= reMethodePrecisionBCD;
        paramINP.groupesPT[i].nombreMaxIterations= reMethodeNbMaxIterations;
        paramINP.groupesPT[i].coefficientContribution= reCoefCont;
        paramINP.groupesPT[i].frequenceImpression= reFrequence;
      }
      paramINP.t0= grpPTs[0].temps().debut();
    }
    //--- Pr�sence des blocs VENT, CRAD, TRAN
    paramINP.bCntRadiations=
      paramINP.groupesPT[0].coefficientContribution[7] == 1;
    paramINP.bVent= paramINP.groupesPT[0].coefficientContribution[8] == 1;
    paramINP.bTransport= false;
    //--- Indices d'impression
    {
      boolean[] impres= new boolean[9];
      Object[] paramsCalcul= mdlCal.parametresCalcul();
      for (int i= 0; i < impres.length; i++)
        impres[i]= ((Double)paramsCalcul[i + 9]).doubleValue() == 1;
      paramINP.impression= impres;
    }
    //--------------------------------------------------------------------------
    //---  Remplissage de la structure de calcul  ------------------------------
    //--------------------------------------------------------------------------
    // Solutions initiales
    SParametresRefluxSI paramSI= remplirParametresRefluxSIV(_prj, paramINP);
    par.parametresSI(paramSI);
    // Conditions limites non stationnaires
    if (paramINP.bCLTransitoires) {
      SParametresRefluxCL paramCL= remplirParametresRefluxCLV(_prj, paramINP);
      par.parametresCL(paramCL);
    }
    // Propri�t�s nodales non stationnaires
    if (paramINP.bPNTransitoires) {
      SParametresRefluxPN paramPN= remplirParametresRefluxPNV(_prj, paramINP);
      par.parametresPN(paramPN);
    }
    par.parametresINP(paramINP);
    //    par.fichier((new File(_prj.racineFichiers())).getName());
  }
  /**
   * Remplissage de la structure pour le fichier .SIV.
   */
  private static SParametresRefluxSI remplirParametresRefluxSIV(
    PRProjet _prj,
    SParametresRefluxINP _paramINP) {
    SParametresRefluxSI paramSI= new SParametresRefluxSI();
    GrMaillageElement mail= _prj.maillage();
    PRSolutionsInitiales[] sis= _prj.modeleCalcul().solutionsInitiales();
    GrNoeud[] nds= mail.noeuds();
    //GrElement[] els=mail.elements();
    boolean[] nTrs;
    double[] ne= new double[nds.length];
    double[] valeursSI= new double[nds.length * 3];
    int nbValeursSI;
    int[] nombresDLN= _paramINP.nombresDLN;
    double[] hts= new double[sis.length];
    // Noeuds de transit
    //    for (int i=0; i<sis.length; i++) hts[i]=sis[i].hauteur();
    for (int i= 0; i < sis.length; i++)
      hts[i]= sis[i].valeurs()[2];
    nTrs= _prj.getNoeudsTransit(hts);
    //--------------------------------------------------------------------------
    //-------  Calcul des niveaux d'eau  ---------------------------------------
    //--------------------------------------------------------------------------
    for (int i= 0; i < nds.length; i++) {
      // Noeud sec => niveau d'eau = bathy
      if (!nTrs[i] && hts[i] < 0.) {
        ne[i]= nds[i].point_.z_;
      }
      // Noeud de transit ou noeud mouille
      else {
        ne[i]= nds[i].point_.z_ + hts[i];
      }
    }
    //--------------------------------------------------------------------------
    //-------  Remplissage de la table des solutions initiales  ----------------
    //--------------------------------------------------------------------------
    nbValeursSI= 0;
    for (int i= 0; i < nds.length; i++) {
      valeursSI[nbValeursSI++]= sis[i].valeurs()[0];
      valeursSI[nbValeursSI++]= sis[i].valeurs()[1];
      if (nombresDLN[i] == 3)
        valeursSI[nbValeursSI++]= ne[i];
    }
    paramSI.valeurs= new double[nbValeursSI];
    System.arraycopy(valeursSI, 0, paramSI.valeurs, 0, nbValeursSI);
    return paramSI;
  }
  /**
   * Remplissage de la structure pour le fichier .CLV.
   */
  private static SParametresRefluxCL remplirParametresRefluxCLV(
    PRProjet _prj,
    SParametresRefluxINP _paramINP) {
    SParametresRefluxCL paramCL= new SParametresRefluxCL();
    PRConditionLimite[] cLs= _prj.conditionsLimites();
    GrMaillageElement mail= _prj.maillage();
    Object[] supports;
    //GrNoeud             noeud;
    Vector dDLTransitoire= new Vector();
    int nbPTs;
    int nbDDLs;
    int nddl;
    SParametresRefluxLigneTransitoire[] pTs;
    double valeurPT;
    double dtReel; // Decalage de la valeur du pas de temps du fait
    // que Reflux ne sait pas prendre en compte un t0.
    IEvolutionSerieIrreguliere courbe;
    Hashtable nd2Num= new Hashtable();
    {
      GrNoeud[] nds= mail.noeuds();
      for (int i= 0; i < nds.length; i++)
        nd2Num.put(nds[i], new Integer(i));
    }
    IEvolution[][] dlts= new IEvolution[mail.noeuds().length][3];
    for (int i= 0; i < dlts.length; i++)
      for (int j= 0; j < 3; j++)
        dlts[i][j]= null;
    //--------------------------------------------------------------------------
    //---  Stockage de la courbe pour chaque DDL transitoires  -----------------
    //--------------------------------------------------------------------------
    for (int i= 0; i < cLs.length; i++) {
      if (cLs[i].evolution() instanceof IEvolutionConstante)
        continue;
      // Pour chaque support
      supports= cLs[i].supports().toArray();
      for (int j= 0; j < supports.length; j++) {
        // Le support n'est pas un noeud => On le passe
        if (!(supports[j] instanceof GrNoeud))
          continue;
        int num= ((Integer)nd2Num.get(supports[j])).intValue();
        // Dans le cas d'une condition limite hauteur
        if (cLs[i].type() == PRConditionLimite.HAUTEUR) {
          // Noeud milieu => On le passe
          if (_paramINP.nombresDLN[num] == 2)
            continue;
        }
        if (cLs[i].type() == PRConditionLimite.VITESSE_X)
          nddl= 0;
        else if (cLs[i].type() == PRConditionLimite.VITESSE_Y)
          nddl= 1;
        else
          nddl= 2;
        dlts[num][nddl]= cLs[i].evolution();
        //        dDLTransitoire.addElement(cLs[i].evolution());
      }
    }
    for (int i= 0; i < dlts.length; i++)
      for (int j= 0; j < 3; j++)
        if (dlts[i][j] != null)
          dDLTransitoire.add(dlts[i][j]);
    //--------------------------------------------------------------------------
    //---  Remplissage des valeurs de DDL pour chaque pas de temps  ------------
    //--------------------------------------------------------------------------
    nbPTs= 0;
    for (int i= 0; i < _paramINP.groupesPT.length; i++)
      nbPTs += _paramINP.groupesPT[i].nombrePasDeTemps;
    pTs= new SParametresRefluxLigneTransitoire[nbPTs];
    nbPTs= 0;
    valeurPT= 0;
    dtReel= _paramINP.t0;
    for (int i= 0; i < _paramINP.groupesPT.length; i++) {
      for (int j= 0; j < _paramINP.groupesPT[i].nombrePasDeTemps; j++) {
        pTs[nbPTs]= new SParametresRefluxLigneTransitoire();
        valeurPT += _paramINP.groupesPT[i].valeurPasDeTemps;
        pTs[nbPTs].valeurPasDeTemps= valeurPT;
        pTs[nbPTs].valeursDegresLibertes= new double[dDLTransitoire.size()];
        // Interpolation sur la courbe au temps t=valeurPT+dtReel
        nbDDLs= 0;
        for (Enumeration e= dDLTransitoire.elements(); e.hasMoreElements();) {
          courbe= (IEvolutionSerieIrreguliere)e.nextElement();
          pTs[nbPTs].valeursDegresLibertes[nbDDLs]=
            courbe.valeur((int) (valeurPT + dtReel));
          nbDDLs++;
        }
        nbPTs++;
      }
    }
    paramCL.pasDeTemps= pTs;
    return paramCL;
  }
  /**
   * Remplissage de la structure pour le fichier .PNV.
   */
  private static SParametresRefluxPN remplirParametresRefluxPNV(
    PRProjet _prj,
    SParametresRefluxINP _paramINP) {
    SParametresRefluxPN paramPN= new SParametresRefluxPN();
    PRSollicitation[] sOs= _prj.sollicitations();
    Object[] supports;
    GrNoeud noeud;
    GrNoeud[] noeuds= _prj.maillage().noeuds();
    Hashtable dDLTransitoire= new Hashtable();
    int nbPTs;
    int nbDDLs;
    SParametresRefluxLigneTransitoire[] pTs;
    double valeurPT;
    double dtReel; // Decalage de la valeur du pas de temps du fait
    // que Reflux ne sait pas prendre en compte un t0.
    IEvolutionSerieIrreguliere courbe;
    //--------------------------------------------------------------------------
    //---  Stockage de la courbe pour chaque DDL transitoires  -----------------
    //--------------------------------------------------------------------------
    for (int i= 0; i < sOs.length; i++) {
      if (sOs[i].type() != PRSollicitation.DEBIT)
        continue;
      if (sOs[i].evolution() instanceof IEvolutionConstante)
        continue;
      // Pour chaque support
      supports= sOs[i].supports().toArray();
      for (int j= 0; j < supports.length; j++) {
        // Le support n'est pas un noeud => On le passe
        if (!(supports[j] instanceof GrNoeud))
          continue;
        noeud= (GrNoeud)supports[j];
        dDLTransitoire.put(noeud, sOs[i].evolution());
      }
    }
    //--------------------------------------------------------------------------
    //---  Remplissage des valeurs de DDL pour chaque pas de temps  ------------
    //--------------------------------------------------------------------------
    nbPTs= 0;
    for (int i= 0; i < _paramINP.groupesPT.length; i++)
      nbPTs += _paramINP.groupesPT[i].nombrePasDeTemps;
    pTs= new SParametresRefluxLigneTransitoire[nbPTs];
    nbPTs= 0;
    valeurPT= 0;
    dtReel= _paramINP.t0;
    for (int i= 0; i < _paramINP.groupesPT.length; i++) {
      for (int j= 0; j < _paramINP.groupesPT[i].nombrePasDeTemps; j++) {
        pTs[nbPTs]= new SParametresRefluxLigneTransitoire();
        valeurPT += _paramINP.groupesPT[i].valeurPasDeTemps;
        pTs[nbPTs].valeurPasDeTemps= valeurPT;
        pTs[nbPTs].valeursDegresLibertes= new double[dDLTransitoire.size()];
        // Interpolation sur la courbe au temps t=valeurPT+dtReel
        nbDDLs= 0;
        for (int k= 0; k < noeuds.length; k++) {
          courbe= (IEvolutionSerieIrreguliere)dDLTransitoire.get(noeuds[k]);
          if (courbe != null) {
            pTs[nbPTs].valeursDegresLibertes[nbDDLs]=
              courbe.valeur((int) (valeurPT + dtReel));
            nbDDLs++;
          }
        }
        nbPTs++;
      }
    }
    paramPN.pasDeTemps= pTs;
    return paramPN;
  }
}
