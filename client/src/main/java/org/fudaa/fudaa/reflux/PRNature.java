/*
 * @file         PRNature.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.util.Arrays;
import java.util.Vector;
/**
 * Une classe nature pour les bords ou les fonds.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRNature {
  public static final int INDEFINI= 0;
  public static final int FOND= 1;
  public static final int FOND_LMG= 2;
  public static final int BORD_OUVERT= 3;
  public static final int BORD_OUVERT_DEBIT= 4;
  public static final int BORD_OUVERT_VITESSE= 5;
  public static final int BORD_FERME= 6;
  public static final int BORD_FERME_FROTTEMENT= 7;
  public static final int FOND_3D= 8;
  public static final int BORD_OUVERT_DEBIT_SOL= 9;
  public static final int BORD_OUVERT_LIBRE= 10;
  public static final int FOND_NON_ERODABLE= 11;
  public static final int BORD_OUVERT_FLUX_CONC= 12;
  // Type de la nature
  private int type_;
  // Supports geometriques associes
  private Vector supports_;
  /**
   * Construit une nature sans attributs. Les attributs doivent dans ce cas
   * etre OBLIGATOIREMENT specifies par les methodes appropriees avant toute
   * manipulation de la nature.
   */
  public PRNature() {
    type(-1);
    supports(new Vector());
  }
  /**
   * Construit une nature.
   * @param type le type de la nature
   * @param supports les supports geometriques associes
   */
  public PRNature(int type, Vector supports) {
    type(type);
    supports(supports);
  }
  /**
   * Construit une nature.
   * @param type le type de la nature
   * @param supports les supports geometriques associes
   */
  public PRNature(int type, Object[] supports) {
    type(type);
    supports(supports);
  }
  /**
   * Retourne le type de cette nature.
   * @return le type
   */
  public int type() {
    return type_;
  }
  /**
   * Modifie le type de cette nature.
   * @param _type le type de la nature
   */
  public void type(int _type) {
    type_= _type;
  }
  /**
   * Retourne les supports geometriques associes a cette nature.
   * @return les supports geometriques (= null si pas de supports)
   */
  public Vector supports() {
    return supports_;
  }
  /**
   * Remplace les supports geometriques associes a cette nature.
   * @param _supports les supports associes
   */
  public void supports(Vector _supports) {
    supports_= _supports;
  }
  /**
   * Remplace les supports geometriques associes a cette nature.
   * @param _supports les supports associes
   */
  public void supports(Object[] _supports) {
    supports_= new Vector(Arrays.asList(_supports));
  }
}
