/*
 * @file         RefluxGrBord.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Point;
import java.util.Arrays;
import java.util.HashSet;

import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrContour;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
/**
 * Une classe d'affichage d'un groupe.
 *
 * @version      $Revision: 1.4 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */

public class RefluxGrGroupe extends GrObjet implements GrContour {
  public RefluxGroupe grp_;
  public RefluxGrGroupe(RefluxGroupe _grp) {
    grp_=_grp;
  }

  public GrPoint[] contour() {
    HashSet hs=new HashSet();
    Object[] objs=grp_.getObjects();
    for (int i=0; i<objs.length; i++) {
      if (objs[i] instanceof GrContour) {
        GrContour ct=(GrContour)objs[i];
        hs.addAll(Arrays.asList(ct.contour()));
      }
    }
    return (GrPoint[])hs.toArray(new GrPoint[0]);
  }

  public boolean estSelectionne(GrMorphisme _ecran, double _dist, Point _pt) {
    Object[] objs=grp_.getObjects();
    for (int i=0; i<objs.length; i++) {
      if (objs[i] instanceof GrContour) {
        if (((GrContour)objs[i]).estSelectionne(_ecran, _dist, _pt)) return true;
      }
    }
    return false;
  }

  public GrBoite boite() {
    GrBoite boiteObj;
    GrBoite boite=new GrBoite();
    Object[] objs=grp_.getObjects();

    for (int i= 0; i < objs.length; i++) {
      if (objs[i] instanceof GrObjet) {
        GrObjet gr=(GrObjet)objs[i];
        boiteObj= gr.boite();
        boite.ajuste(boiteObj.o_);
        boite.ajuste(boiteObj.e_);
      }
    }
    return boite;
  }

  public RefluxGroupe getGroupe() {
    return grp_;
  }
}
