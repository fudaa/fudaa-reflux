/*
 * @file         HY3dPnSaisieValeurPEBase.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d3;
import java.awt.BorderLayout;

import javax.swing.JComboBox;

import org.fudaa.fudaa.reflux.PRPnSaisieValeurPE;
/**
 * Un composant de saisie de valeur de propri�t� de type BASE. Ce composant est
 * un JComboBox.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class HY3dPnSaisieValeurPEBase extends PRPnSaisieValeurPE {
  /**
   * Le composant contenu dans le panneau.
   */
  private JComboBox co_= new JComboBox();
  /**
   * Sauvegarde de la valeur de propri�t�.
   */
  private double oldPrps_= 0;
  /**
   * Cr�ation du composant.
   */
  public HY3dPnSaisieValeurPEBase() {
    super();
    co_.addItem("Polynomiale");
    co_.addItem("Mixte");
    co_.addItem("Trigonom�trique complexe");
    setLayout(new BorderLayout());
    add(co_, "Center");
  }
  /**
   * Initialisation avec la valeur de propri�t�.
   * @param _val La valeur de la propri�t�.
   */
  public void setValeur(double _val) {
    oldPrps_= _val;
    if (Double.isNaN(oldPrps_))
      co_.setSelectedIndex(-1); // Mixte
    else
      co_.setSelectedIndex((int)oldPrps_);
  }
  /**
   * Retourne la valeur de la propri�t�. La valeur peut �tre Double.NaN, c'est
   * � dire mixte.
   *
   * @return La valeur.
   */
  public double getValeur() {
    oldPrps_= co_.getSelectedIndex();
    if (oldPrps_ == -1)
      oldPrps_= Double.NaN;
    return oldPrps_;
  }
  /**
   * Pour rendre actif/non les composants du panneau.
   */
  public void setEnabled(boolean _b) {
    super.setEnabled(_b);
    co_.setEnabled(_b);
  }
}