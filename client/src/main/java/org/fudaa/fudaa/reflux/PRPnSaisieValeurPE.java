/*
 * @file         PRPnSaisieValeurPE.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import javax.swing.JPanel;
/**
 * Un composant de saisie de valeur de propri�t� �l�mentaire. Cette classe doit
 * �tte d�riv�e.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public abstract class PRPnSaisieValeurPE extends JPanel {
  /**
   * Initialisation avec la valeur de propri�t�. La valeur Double.NaN
   * correspond � une valeur mixte. Le composant doit refl�ter cet �tat, par
   * exemple en affichant rien pour un JTextField.
   *
   * @param _val La valeur de la propri�t�.
   */
  public abstract void setValeur(double _val);
  /**
   * Retourne la valeur de la propri�t�. La valeur peut �tre Double.NaN, c'est
   * � dire mixte.
   *
   * @return La valeur.
   */
  public abstract double getValeur();
}
