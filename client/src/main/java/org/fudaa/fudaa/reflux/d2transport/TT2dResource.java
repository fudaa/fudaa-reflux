/*
 * @file         TT2dResource.java
 * @creation     2001-04-12
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d2transport;
import com.memoire.bu.BuResource;
/**
 * Une classes de gestion des ressources pour les projets Reflux 2d transport.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class TT2dResource extends BuResource {
  public final static TT2dResource TT2D= new TT2dResource();
  // Nombre de paramètres de calcul
  public static int nbParsCalcul= 0;
  // Paramètres de calcul.
  public static final int CONVEC_ZF= nbParsCalcul++;
  public static final int CONVEC_C= nbParsCalcul++;
  public static final int DIFFUSION= nbParsCalcul++;
  public static final int DEBIT_REF= nbParsCalcul++;
  public static final int PROF_REF= nbParsCalcul++;
  public static final int CONC_REF= nbParsCalcul++;
  public static final int EFFET_PENTE= nbParsCalcul++;
  public static final int ND_ERODABLE= nbParsCalcul++;
  public static final int HMIN_CALCUL= nbParsCalcul++;
  public static final int FORMULE= nbParsCalcul++;
  public static final int PRJ_HOULE= nbParsCalcul++;
  public static final int CAS_CHAMP= nbParsCalcul++;
  public static final int CAS_MAREE= nbParsCalcul++;
  public static final int PERIODE= nbParsCalcul++;
  public static final int ICOURBE= nbParsCalcul++;
  public static final int PRJ_MAREE_45= nbParsCalcul++;
  public static final int PRJ_MAREE_90= nbParsCalcul++;
  public static final int T0_HYDRO= nbParsCalcul++;
  // Nombre de propriétés globales
  public static int nbPropsGlob= 0;
  // Propriétés globales.
  public static final int DENS_EAU= nbPropsGlob++;
  public static final int DENS_SEDI= nbPropsGlob++;
  public static final int DIFFU_LONGI= nbPropsGlob++;
  public static final int DIFFU_TRAN= nbPropsGlob++;
  public static final int POROSITE= nbPropsGlob++;
  public static final int SHIELDS= nbPropsGlob++;
  public static final int COEF_PSI= nbPropsGlob++;
  public static final int VISCO_DYN= nbPropsGlob++;
  // Position des valeurs dans les objets solutions initiales.
  public static final int CONCENTR= 0;
  public static final int COTE_Z= 1;
}
