/*
 * @file         PRProjet.java
 * @creation     1999-06-25
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.fudaa.dodico.corba.mesure.IEvolution;

import org.fudaa.dodico.fortran.FortranReader;

import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;
/**
 * Une classe projet.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public abstract class PRProjet {
  // Types de projets
  /** Type courantologie 2D. */
  public final static int VIT_NU_CON= 0;
  /** Type courantologie 2D + longueur de m�lange. */
  public final static int VIT_NU_LMG= 1;
  /** Type courantologie 3D. */
  public final static int VIT_3D= 2;
  /** Type transport total 2D. */
  public final static int TRANS_TOT_2D= 3;
  /** Type transport en suspension 2D. */
  public final static int TRANS_SUS_2D= 4;
  // Type du projet.
  private int type_= VIT_NU_CON;
  // Maillage
  private RefluxMaillage maillage_;
  // Evolutions
  private IEvolution[] evolutions_;
  // Racine des fichiers du probleme
  private String racineFichiers_;
  /** Noms des objets corba. */
  private Hashtable nomsIObjets_;
  // Conditions limites
  private PRConditionLimite[] conditionsLimites_;
  // Sollicitations
  private PRSollicitation[] sollicitations_;
  // Modification des courbes
  public boolean modCourbes;
  /** Mod�le de propri�t�s physiques. */
  protected PRModeleProprietes mdlPrp_;
  /** Mod�le de calcul. */
  protected PRModeleCalcul mdlCal_;
  /** Groupes. */
  protected Vector groupes_;
  /** Segments de solinit. */
  protected Vector sgs_;
  /** Multi plans. */
  protected Vector plans_;


  /** R�sultats. */
  protected PRResultats results_;
  /** Listeners for modifications in project. */
  protected HashSet listeners_= new HashSet();
  public PRProjet() {
    nomsIObjets_= new Hashtable();
    evolutions_= new IEvolution[0];
    conditionsLimites_= new PRConditionLimite[0];
    sollicitations_= new PRSollicitation[0];
    groupes_=new Vector();
    sgs_=new Vector();
    plans_=new Vector();
    mdlPrp_= new PRModeleProprietes();
    mdlCal_= new PRModeleCalcul();
    resultats(new PRResultats());
    // Racine des fichiers du probleme
    racineFichiers("reflux");
    // Type de probleme
    type(RefluxResource.typeProjet);
  }
  public void racineFichiers(String _racine) {
    this.racineFichiers_= _racine;
  }
  public String racineFichiers() {
    return this.racineFichiers_;
  }
  public void maillage(RefluxMaillage _maillage) {
    this.maillage_= _maillage;
  }
  public RefluxMaillage maillage() {
    return this.maillage_;
  }
  public void evolutions(IEvolution[] _evolutions) {
    this.evolutions_= _evolutions;
  }
  public IEvolution[] evolutions() {
    return this.evolutions_;
  }
  public void conditionsLimites(PRConditionLimite[] _conditionsLimites) {
    this.conditionsLimites_= _conditionsLimites;
  }
  public PRConditionLimite[] conditionsLimites() {
    return this.conditionsLimites_;
  }
  public void sollicitations(PRSollicitation[] _sollicitations) {
    this.sollicitations_= _sollicitations;
  }
  public PRSollicitation[] sollicitations() {
    return this.sollicitations_;
  }
  public void type(int _type) {
    this.type_= _type;
  }
  public int type() {
    return this.type_;
  }

  /**
   * Ajoute un nouveau groupe.
   */
  public void addGroupe(RefluxGroupe _grp) {
//    int num=RefluxGroupe.getLastNumber(groupes_);
//    _grp.setNum(num);
    groupes_.add(_grp);
  }

  /**
   * Supprime un groupe.
   * @param _grp PRModeleCalcul
   */
  public void removeGroupe(RefluxGroupe _grp) {
    groupes_.remove(_grp);
  }

  /**
   * Renvoie les groupes.
   */
  public Vector getGroupes() { return groupes_; }

  /**
   * Ajoute un nouveau segment.
   */
  public void addSegment(RefluxSegment _sg) {
//    int num=RefluxSegment.getLastNumber(sgs_);
//    _sg.setNum(num);
    sgs_.add(_sg);
  }

  /**
   * Supprime un segment.
   * @param _sg PRModeleCalcul
   */
  public void removeSegment(RefluxSegment _sg) {
    sgs_.remove(_sg);
  }

  /**
   * Renvoie les segments.
   */
  public Vector getSegments() { return sgs_; }

  /**
   * Ajoute un nouveau plan.
   */
  public void addPlan(RefluxPlan _plan) {
//    int num=RefluxPlan.getLastNumber(plans_);
//    _plan.setNum(num);
    plans_.add(_plan);
  }

  /**
   * Supprime un plan.
   * @param _plan PRModeleCalcul
   */
  public void removePlan(RefluxPlan _plan) {
    plans_.remove(_plan);
  }

  /**
   * Renvoie les plans.
   */
  public Vector getPlans() { return plans_; }

  /**
   * Affecte le mod�le de calcul au projet.
   * @param _mdl Mod�le de calcul.
   */
  public void modeleCalcul(PRModeleCalcul _mdl) {
    mdlCal_= _mdl;
  }
  /**
   * Retourne le mod�le de calcul du projet.
   * @return Le mod�le.
   */
  public PRModeleCalcul modeleCalcul() {
    return mdlCal_;
  }
  /**
   * Affecte le mod�le de propri�t�s.
   */
  public void modeleProprietes(PRModeleProprietes _mdl) {
    mdlPrp_= _mdl;
  }
  /**
   * Retourne le mod�le de propri�t�s.
   */
  public PRModeleProprietes modeleProprietes() {
    return mdlPrp_;
  }
  /**
   * Affecte les r�sultats.
   */
  public void resultats(PRResultats _res) {
    results_= _res;
    results_.setProject(this);
    fireDataChange(
      new RefluxDataChangeEvent(this, RefluxDataChangeEvent.RESULTS_CHANGED));
  }
  /**
   * Retourne les r�sultats. Si les r�sultats n'existent pas, ils sont charg�s
   * depuis le fichier de m�me racine que le projet.
   *
   * Cette m�thode n'est pas appel�e au moment du chargement du projet pour le
   * moment car les r�sultats sont rarement utilis�s.
   *
   * @return Les r�sultats
   */
  public PRResultats resultats() /*throws IOException*/ {
    //    if (results_==null) results_=new PRResultats();
    //     results_=getInterfaceCalcul().getResultats(this,new File(racineFichiers_));
    return results_;
  }
  public void addDataChangeListener(RefluxDataChangeListener _listener) {
    listeners_.add(_listener);
  }
  public void removeDataChangeListener(RefluxDataChangeListener _listener) {
    listeners_.remove(_listener);
  }
  protected void fireDataChange(RefluxDataChangeEvent _evt) {
    for (Iterator i= listeners_.iterator(); i.hasNext();) {
      ((RefluxDataChangeListener)i.next()).dataChanged(_evt);
    }
  }
  public boolean hasResultats() {
    return results_.getNbSteps() > 0;
  }
  /**
   * Donnee un nom � un objet corba.
   */
  public void associe(Object _iObjet, String _nom) {
    nomsIObjets_.put(_iObjet, _nom);
  }
  /**
   * Retourne le nom d'un objet corba.
   */
  public String getName(Object _iObjet) {
    return (String)nomsIObjets_.get(_iObjet);
  }
  /**
   * Initialise les solutions initiales en chaque noeud � partir du plan d�finit
   * par 3 points
   */
  //  public void initialiserSolutionsInitiales() {
  //    GrPoint[] ptsSI=mdlCal_.pointsSI();
  //    if (ptsSI==null || maillage_==null) return;
  //
  //    double[]  ht;
  //    boolean[] nTrs;
  //
  //    GrNoeud[] nds=maillage_.noeuds();
  //
  //    // Calcul de la pente suivant X et Y du plan des S.I.
  //    double[] s=mdlCal_.getPenteXYPlanSI();
  //
  //    // Initialisation des hauteur totales pour tous les noeuds
  //    // a partir du plan de r�f�rence
  //
  //    ht=new double[nds.length];
  //
  //    for(int i=0;i<nds.length;i++) {
  //      GrPoint pt=nds[i].point;
  //
  //      ht[i]=s[0]*(pt.x-ptsSI[0].x)+
  //            s[1]*(pt.y-ptsSI[0].y)+
  //            ptsSI[0].z-pt.z;
  //    }
  //
  //    // Noeuds de transit
  //    nTrs=getNoeudsTransit(ht);
  //
  //    // Calcul des vitesses pour chaque noeud (On prend un chezy=50)
  //    double[][] vxy=calculVitesses(ht,nTrs,s[0],s[1]);
  //
  //    creeSolutionsInitiales(vxy[0],vxy[1],ht);
  //  }
  /**
   * Calcul de vitesses vx, vy approximatives pour chaque noeud du maillage
   * a partir des hauteurs totales et du plan de r�f�rence (On prend un
   * chezy=50).
   * @param _hts Hauteurs totales sur les noeuds.
   * @param _nTrs Les noeuds ou non de transit.
   * @param _sx  Pente suivant x
   * @param _sy  Pente suivant y
   * @return r[0][]: Vitesses suivant x, r[1][]: Vitesses suivant y
   */
  protected double[][] calculVitesses(
    double[] _hts,
    boolean[] _nTrs,
    double _sx,
    double _sy) {
    double[][] vxy;
    double sss;
    vxy= new double[2][_hts.length];
    sss= Math.sqrt(_sx * _sx + _sy * _sy);
    // Si la pente sur X et Y est nulle => Vitesses nulles
    if (sss < 1.e-10) {
      for (int i= 0; i < _hts.length; i++) {
        vxy[0][i]= 0;
        vxy[1][i]= 0;
      }
    }
    // Si la pente est differente de 0
    else {
      for (int i= 0; i < _hts.length; i++) {
        // Noeud de transit ou noeud sec => vitesses nulles
        if (_nTrs[i] || _hts[i] <= 0.) {
          vxy[0][i]= 0;
          vxy[1][i]= 0;
        }
        // Noeud mouille
        else {
          vxy[0][i]= -_sx * Math.sqrt(_hts[i] / sss) * 50.;
          vxy[1][i]= -_sy * Math.sqrt(_hts[i] / sss) * 50.;
        }
      }
    }
    return vxy;
  }
  /**
   * Determination des noeuds de transit.
   *
   * Si au moins 1 des noeuds de l'element est mouille et au moins 1
   * est sec => Element de transit. Les noeuds non mouilles sont transit
   *
   * @param _hts Hauteurs totales sur les noeuds.
   * @return <i>true</i> noeud de transit, <i>false</i> sinon.
   */
  public boolean[] getNoeudsTransit(double[] _hts) {
    boolean[] nTrs;
    boolean[] nSecs= new boolean[3];
    GrElement[] els= maillage().elements();
    GrNoeud[] nds= maillage().noeuds();
    // Table de correspondance Noeud->Indice
    Hashtable hnd2Ind= new Hashtable();
    for (int i= 0; i < nds.length; i++)
      hnd2Ind.put(nds[i], new Integer(i));
    nTrs= new boolean[nds.length];
    for (int i= 0; i < nds.length; i++)
      nTrs[i]= false;
    for (int i= 0; i < els.length; i++) {
      GrNoeud[] ndsEl= els[i].noeuds_;
      // Etat des noeuds de coins (sec/mouille)
      for (int j= 0; j < 3; j++) {
        int iNd= ((Integer)hnd2Ind.get(ndsEl[j])).intValue();
        nSecs[j]= _hts[iNd] <= 0.;
      }
      // Un des noeuds de coin n'est pas dans le meme etat => Element de
      // transit
      for (int j= 0; j < 2; j++) {
        if (nSecs[j] == nSecs[j + 1])
          continue;
        // Noeuds transit
        nTrs[((Integer)hnd2Ind.get(ndsEl[(j * 2 + 5) % 6])).intValue()]= true;
        nTrs[((Integer)hnd2Ind.get(ndsEl[j * 2])).intValue()]= true;
        nTrs[((Integer)hnd2Ind.get(ndsEl[j * 2 + 3])).intValue()]= true;
        //        nTrs[((Integer)hnd2Ind.get(ndsEl[j*2+1]    )).intValue()]=true;
        break;
      }
    }
    return nTrs;
  }
  /**
   * Initialisation des normales pour un maillage donn�.
   */
  public void initialiserNormales() {
    if (maillage_ == null)
      return;
    GrNoeud[][] noeuds= maillage_.noeudsContours();
    GrNoeud noeud, prec, suiv;
    int nbNoeuds;
    double valeur;
    //    double[]   cNoeud, cPrec, cSuiv;
    double xalpha1, yalpha1, alpha1;
    double xalpha2, yalpha2, alpha2;
    PRNormale[] normales;
    nbNoeuds= 0;
    for (int i= 0; i < noeuds.length; i++)
      nbNoeuds += noeuds[i].length;
    normales= new PRNormale[nbNoeuds];
    nbNoeuds= 0;
    for (int i= 0; i < noeuds.length; i++) {
      prec= noeuds[i][noeuds[i].length - 1];
      noeud= noeuds[i][0];
      for (int j= 0; j < noeuds[i].length; j++) {
        suiv= noeuds[i][(j + 1) % noeuds[i].length];
        GrPoint ptP= prec.point_;
        GrPoint ptN= noeud.point_;
        GrPoint ptS= suiv.point_;
        // Angle defini par le vecteur (noeud,prec)
        xalpha1= ptP.x_ - ptN.x_;
        yalpha1= ptP.y_ - ptN.y_;
        if (xalpha1 != 0 || yalpha1 != 0)
          alpha1= Math.atan2(yalpha1, xalpha1);
        else
          alpha1= 0;
        // Angle defini par le vecteur (noeud,suiv)
        xalpha2= ptS.x_ - ptN.x_;
        yalpha2= ptS.y_ - ptN.y_;
        if (xalpha2 != 0 || yalpha2 != 0)
          alpha2= Math.atan2(yalpha2, xalpha2);
        else
          alpha2= 0;
        // Angle de la normale
        valeur= (alpha1 + alpha2) / 2 * 180 / Math.PI;
        if (alpha2 - alpha1 <= 0)
          valeur += 180;
        normales[nbNoeuds]= new PRNormale(noeud, valeur);
        prec= noeud;
        noeud= suiv;
        nbNoeuds++;
      }
    }
    mdlPrp_.normales(normales);
  }
  /**
   * Initialisation de la nature des bords.
   */
  public void initialiserNatureBords() {
    if (maillage_ == null)
      return;
    GrElement[][] aretes= maillage_.aretesContours();
    int nbAretes= 0;
    for (int i= 0; i < aretes.length; i++)
      nbAretes += aretes[i].length;
    Vector natures= new Vector(nbAretes);
    PRNature nature;
    for (int i= 0; i < aretes.length; i++) {
      for (int j= 0; j < aretes[i].length; j++) {
        nature=
          new PRNature(
            Definitions.getDefautTypeBord(),
            new GrElement[] { aretes[i][j] });
        natures.addElement(nature);
      }
    }
    mdlPrp_.naturesBords(natures);
  }
  /**
   * Initialisation de la nature des fonds (1 nature pour tous les fonds).
   */
  public void initialiserNatureFonds() {
    if (maillage_ == null)
      return;
    GrElement[] elements= maillage_.elements();
    Vector natures= new Vector();
    PRNature nature;
    nature= new PRNature(Definitions.getDefautTypeFond(), elements);
    natures.addElement(nature);
    mdlPrp_.naturesFonds(natures);
  }
  /**
   * Retourne la classe interface de calcul associ� au projet.
   *
   * @return L'interface de calcul.
   */
  public abstract PRInterfaceCalcul getInterfaceCalcul();
  /**
   * Nouveau projet. Le projet est cr�� depuis plusieurs fichiers
   * qui doivent forcement exister.
   *
   * @param _tpPrj  Le type de projet.
   * @param _racine Racine du projet sans extension.
   * @param _params Les param�tres eventuels de cr�ation du projet.
   *
   * @exception FileNotFoundException Un fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   * @return Le projet.
   */
  public static PRProjet nouveau(int _tpPrj, String _racine, Object[] _params)
    throws IOException {
    PRProjet r;
    //*** A voir : A mettre dans le projet correspondant
    // Controle que les fichiers existent et qu'ils sont ouvrables en lecture
    //    File   file;
    //    for (int i=0; i<exts.length; i++) {
    //      file=new File(racine+exts[i]);
    //      if (!file.exists() || !file.canRead())
    //       throw new FileNotFoundException(racine+exts[i]);
    //    }
    // Definition du type de projet. Ceci est n�cessaire � la cr�ation de toutes
    // les classes UI et BDD utilis�es par le projet.
    RefluxResource.typeProjet= _tpPrj;
    r= PRFabrique.getFabrique().creeProjet();
    r.initialiser(_racine, _params);
    return r;
  }
  /**
   * Ouverture du projet depuis un fichier .pre. Les fichiers associ�s au projet
   * .bth/.cor/.ele/[.crb] doivent �tre pr�sents dans le m�me r�pertoire que
   * le fichier .pre.
   *
   * @param _nomFichier Nom des fichiers a lire int�grant le chemin complet
   *        (l'extension est autoris�e et peut valoir ".pre")
   *
   * @exception FileNotFoundException Un fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   * @return Le projet de type d�fini dans le fichier .pre.
   */
  public static PRProjet ouvrir(String _nomFichier) throws IOException {
    PRProjet r;
    // Suppression de l'extension .pre eventuelle du nom du fichier.
    String racine= _nomFichier.substring(0, _nomFichier.lastIndexOf(".pre"));
    // Controle que les fichiers existent et qu'ils sont ouvrables en lecture
    String[] extension= { ".pre" };
    File file;
    for (int i= 0; i < extension.length; i++) {
      file= new File(racine + extension[i]);
      if (!file.exists() || !file.canRead())
        throw new FileNotFoundException(racine + extension[i]);
    }
    // Ouverture du fichier .pre pour lecture du type de projet.
    RefluxResource.typeProjet= getTypeProjet(racine + ".pre");
    // Lecture du projet.
    r= PRFabrique.getFabrique().creeProjet();
    r.charger(racine);
    return r;
  }
  /**
   * Retourne le type de projet stock� dans le fichier projet.
   *
   * @param _nomFichier Nom du fichier .pre avec extension.
   * @return Le type de projet.
   */
  private static int getTypeProjet(String _nomFichier) throws IOException {
    int r;
    FortranReader file= null;
    int[] fmt;
    String version;
    try {
      // Ouverture du fichier
      file= new FortranReader(new FileReader(_nomFichier));
      file.setBlankZero(true);
      // Num�ro de version (seules les versions > 4.0 sont autoris�es)
      fmt= new int[] { 12, 10 };
      file.readFields(fmt);
      version= file.stringField(1);
      if (version.compareTo("4.0") < 0) {
        throw new Exception(
          "Erreur de lecture sur "
            + _nomFichier
            + "\nLe format du fichier est de version"
            + file.stringField(1)
            + ".\nSeules les versions > � 4.0 sont autoris�es");
      }
      // Type de projet stock�
      fmt= new int[] { 5 };
      file.readFields(fmt);
      r= file.intField(0) - 1;
      if (r != PRProjet.VIT_NU_CON
        && r != PRProjet.VIT_NU_LMG
        && r != PRProjet.VIT_3D
        && r != PRProjet.TRANS_TOT_2D
        && r != PRProjet.TRANS_SUS_2D) {
        throw new Exception(
          "Erreur de lecture sur "
            + _nomFichier
            + "\nLe type de projet est inconnu.");
      }
      return r;
    } catch (IOException _exc) {
      throw new IOException("Erreur de lecture sur " + _nomFichier);
    } catch (NumberFormatException _exc) {
      throw new IOException("Erreur de lecture sur " + _nomFichier);
    } catch (Exception _exc) {
      _exc.printStackTrace();
      throw new IOException(_exc.getMessage());
    } finally {
      // Fermeture du fichier
      if (file != null)
        file.close();
    }
  }
  /**
   * Initialisation du projet.
   * @param _racine Nom de la racine du projet.
   * @param _params Param�tres eventuels de cr�ation du projet.
   */
  public abstract void initialiser(String _racine, Object[] _params)
    throws IOException;
  /**
   * Chargement du projet depuis les fichiers .pre/.bth/.cor/.ele./[.crb].
   * @param _nomFichier Nom du fichier de lecture.
   */
  public abstract void charger(String _nomFichier) throws IOException;
  /**
   * Enregistrement du projet sur fichier .pre.
   * @param _nomFichier Nom du fichier de sauvegarde.
   */
  public abstract void enregistrer(String _nomFichier) throws IOException;
  /**
   * Initialise les solutions initiales en chaque noeud avec des valeurs
   * globales.
   *
   * @param _valGlobs Valeurs globales.
   */
  public abstract void initialiserSolutionsInitiales(double[] _valGlobs);
  /**
   * Initialise les solutions initiales par d�faut en chaque noeud � partir des
   * multiplans.
   */
  public abstract void initialiserSolutionsInitiales();
  /**
   * Cr�e les solutions initiales aux noeuds � partir des tableaux pass�s.
   * @param _vx La vitesse x en chaque noeud dans l'ordre des noeuds.
   * @param _vy La vitesse y en chaque noeud dans l'ordre des noeuds.
   * @param _ne Le niveau d'eau en chaque noeud dans l'ordre des noeuds.
   */
  public abstract void creeSolutionsInitiales(
    double[] _vx,
    double[] _vy,
    double[] _ne);
  /**
   * Controle et mise � jour des solutions initiales si le nombre de termes
   * u,v,w a chang�.
   */
  public abstract void controleNbTermesNoeuds();
}
