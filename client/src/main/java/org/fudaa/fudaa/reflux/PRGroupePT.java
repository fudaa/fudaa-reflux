/*
 * @file         PRGroupePT.java
 * @creation     1998-06-08
 * @modification $Date: 2005-08-16 14:01:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
/**
 * Une classe d�finissant un groupe de pas de temps (au sens REFLUX du terme).
 *
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 14:01:54 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRGroupePT {
  /**
   * D�finition du temps
   */
  private PRBoucleTemps temps_= null;
  /**
   * Sch�ma utilis�
   */
  private PRSchemaResolution schema_= null;
  /**
   * M�thode de r�solution et coefficients
   */
  private PRMethodeResolution methode_= null;
  /**
   * Fr�quence de stockage des r�sultats (par d�faut, 1). Elle n'est pas
   * utilis�e si le sch�ma de r�solution est STATIONNAIRE (Reflux 2D)
   */
  private int frequenceStockage_= 1;
  /**
   * Contruction d'un groupe a partir d'un groupe. Copie r�cursive.
   * @param groupe Le groupe � copier.
   */
  public PRGroupePT(PRGroupePT groupe) {
    PRBoucleTemps temps=
      new PRBoucleTemps(
        groupe.temps().debut(),
        groupe.temps().fin(),
        groupe.temps().pas());
    PRSchemaResolution schema=
      new PRSchemaResolution(
        groupe.schema().type(),
        groupe.schema().coefficient());
    PRMethodeResolution methode=
      new PRMethodeResolution(
        groupe.methode().type(),
        (double[])groupe.methode().coefficients().clone());
    temps(temps);
    schema(schema);
    methode(methode);
    frequenceStockage(groupe.frequenceStockage());
  }
  /**
   * Construction d'un groupe par d�faut.
   */
  public PRGroupePT() {
    this(
      new PRBoucleTemps(),
      new PRSchemaResolution(),
      new PRMethodeResolution(),
      1);
  }
  /**
   * Construction d'un groupe.
   *
   * @param _temps D�finition du temps.
   * @param _schema Sch�ma de r�solution.
   * @param _methode M�thode de r�solution.
   * @param _frequenceStockage La fr�quence d'impresssion des r�sultats.
   */
  public PRGroupePT(
    PRBoucleTemps _temps,
    PRSchemaResolution _schema,
    PRMethodeResolution _methode,
    int _frequenceStockage) {
    temps(_temps);
    schema(_schema);
    methode(_methode);
    frequenceStockage(_frequenceStockage);
  }
  public Object clone() {
    return new PRGroupePT(temps_, schema_, methode_, frequenceStockage_);
  }
  public void schema(PRSchemaResolution _schema) {
    this.schema_= _schema;
  }
  public PRSchemaResolution schema() {
    return this.schema_;
  }
  public void temps(PRBoucleTemps _temps) {
    this.temps_= _temps;
  }
  public PRBoucleTemps temps() {
    return this.temps_;
  }
  public void methode(PRMethodeResolution _methode) {
    this.methode_= _methode;
  }
  public PRMethodeResolution methode() {
    return this.methode_;
  }
  /**
   * D�finit la fr�quence de stockage des r�sultats (en nombre de pas de
   * temps).<BR>
   * Cette fr�quence n'est pas utilis�e si le sch�ma est stationnaire.
   * @param _frequence La fr�quence de stockage
   */
  public void frequenceStockage(int _frequence) {
    this.frequenceStockage_= _frequence;
  }
  /**
   * Retourne la fr�quence de stockage des r�sultats (en nombre de pas de
   * temps).<BR>
   * Cette fr�quence n'est pas utilis�e si le sch�ma est stationnaire.
   * @return _fr�quence La fr�quence de stockage
   */
  public int frequenceStockage() {
    return this.frequenceStockage_;
  }
}