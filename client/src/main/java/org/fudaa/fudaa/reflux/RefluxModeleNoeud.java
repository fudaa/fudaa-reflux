package org.fudaa.fudaa.reflux;
import com.memoire.bu.BuTable;

import org.fudaa.ctulu.table.CtuluTable;

import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZModelePoint;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
public final class RefluxModeleNoeud implements ZModelePoint {
  /** Maillage associ�. */
  private GrMaillageElement maillage_;
  /**
   * Cr�ation du mod�le � partir du maillage.
   */
  public RefluxModeleNoeud(GrMaillageElement _maillage) {
    maillage_= _maillage;
  }
  /// >>> ZModeleDonnees -------------------------------------------------------
  public GrBoite getDomaine() {
    return maillage_.boite();
  }
  public boolean isDonneesBoiteAvailable() {
    return false;
  }


  public double getX(int _i){
    return  maillage_.noeud(_i).point_.x_;
  }
  public double getY(int _i){
    return  maillage_.noeud(_i).point_.y_;
  }
  public BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer){
    final BuTable table=new CtuluTable();
    table.setModel(new ZCalquePoint.DefaultTableModel(this));
    return table;
  }
  public void fillWithInfo(InfoData _d,ZCalqueAffichageDonneesInterface _layer){}
  public boolean isValuesTableAvailable(){
    return true;
  }
  public boolean isDonneesBoiteTimeAvailable() {
    return false;
  }
  public void getDomaine(GrBoite _bt) {
    _bt.ajuste(getDomaine());
    return;
  }
  public int getNombre() {
    return maillage_.noeuds().length;
  }
  /**
   * L'objet d'index i. Lors d'une selection, cet index permet de faire le lien
   * entre le modele et la liste de s�lection sur ce mod�le.
   */
  public  Object getObject(int _ind) {
    return maillage_.noeud(_ind);
  }
  /// <<< ZModeleDonnees -------------------------------------------------------
  /// >>> ZModelePoint ---------------------------------------------------------
  public  boolean point(GrPoint _p, int _i, boolean _force) {
    //    if (_i<0 || _i>=maillage_.noeuds().length) return false;
    GrPoint pt= maillage_.noeud(_i).point_;
    _p.x_= pt.x_;
    _p.y_= pt.y_;
    return true;
  }

}