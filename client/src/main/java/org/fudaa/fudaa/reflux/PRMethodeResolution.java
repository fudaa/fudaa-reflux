/*
 * @file         PRMethodeResolution.java
 * @creation     1998-06-08
 * @modification $Date: 2003-11-25 10:14:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
/**
 * Une classe de d�finition d'une m�thode de r�solution avec ou sans
 * coefficients.
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:14:05 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRMethodeResolution {
  /**
   * Type Lin�aire.
   */
  public final static int LINEAIRE= 0;
  /**
   * Type Newton Raphson.
   */
  public final static int NEWTON_RAPHSON= 1;
  /**
   * Type Newton Raphson avec bancs couvrants/d�couvrants.
   */
  public final static int NEWTON_RAPHSON_BCD= 2;
  /**
   * Type Newton Raphson avec longueur de m�lange.
   */
  public final static int NEWTON_RAPHSON_LMG= 3;
  /**
   * Type Selected Lumping.
   */
  public final static int SELECTED_LUMPING= 4;
  /**
   * Type Selected Lumping avec bancs couvrants/d�couvrants.
   */
  public final static int SELECTED_LUMPING_BCD= 5;
  /**
   * Type Lax Wendroff.
   */
  public final static int LAX_WENDROFF= 6;
  /**
   * Type de la m�thode.
   */
  private int type_= 0;
  /**
   * Coefficients de la m�thode.
   */
  private double[] coefs_= null;
  /**
   * Constructeur avec type par d�faut et coefficients par d�faut.
   */
  public PRMethodeResolution() {
    this(Definitions.getTypesMethode(Definitions.getTypesSchema()[0])[0]);
  }
  /**
   * Constructeur avec le type de la m�thode et des coefficients par d�faut.
   * @param _type Type de la m�thode.
   */
  public PRMethodeResolution(int _type) {
    this(_type, Definitions.getDefaultCoefsMethode(_type));
  }
  /**
   * Constructeur avec coefficients initialis�s.
   * @param _type Type de la m�thode.
   * @param _coefs Coefficients de la m�thode.
   */
  public PRMethodeResolution(int _type, double[] _coefs) {
    this.type(_type);
    this.coefficients(_coefs);
  }
  /**
   * Attribut la m�thode
   * @param _type La m�thode de r�solution
   */
  public void type(int _type) {
    this.type_= _type;
  }
  /**
   * Retourne la m�thode
   * @return La m�thode de r�solution
   */
  public int type() {
    return this.type_;
  }
  /**
   * Attribut les coefficients pour la m�thode
   * @param _coefs Les coefficients pour la m�thode
   */
  public void coefficients(double[] _coefs) {
    this.coefs_= _coefs;
  }
  /**
   * Retourne les coefficients pour la m�thode
   * @return  Les coefficients pour la m�thode
   */
  public double[] coefficients() {
    return this.coefs_;
  }
}
