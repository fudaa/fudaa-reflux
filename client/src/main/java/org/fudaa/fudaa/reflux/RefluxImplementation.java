/*

 * @file         RefluxImplementation.java

 * @creation     1998-06-17

 * @modification $Date: 2007-01-19 13:14:36 $

 * @license      GNU General Public License 2

 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne

 * @mail         devel@fudaa.org

 */
package org.fudaa.fudaa.reflux;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.dunes.ICalculDunesHelper;
import org.fudaa.dodico.corba.olb.ICalculOlbHelper;
import org.fudaa.dodico.corba.reflux.ICalculRefluxHelper;
import org.fudaa.dodico.corba.reflux3d.ICalculReflux3dHelper;

import org.fudaa.dodico.dunes.DCalculDunes;
import org.fudaa.dodico.olb.DCalculOlb;
import org.fudaa.dodico.reflux.DCalculReflux;
import org.fudaa.dodico.reflux3d.DCalculReflux3d;

import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.commun.EbliCalquesPreferencesPanel;

import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * Classe principale d'impl�mentation de Reflux. Commune � l'application et � l'applet.
 * 
 * @version $Revision: 1.19 $ $Date: 2007-01-19 13:14:36 $ by $Author: deniger $
 * @author Bertrand Marchand
 */
public class RefluxImplementation extends FudaaImplementation {
  // Fenetre principale
  PRFilleCalques fnCalques_ ;
  // Fenetre de nouveau projet
  // private PRDialogNouveauProjet dialogNouveauPrj_ ;
  // Projet
  public static PRProjet projet ;
  // Arbre des calques
  private BArbreCalque arbre_;
  // Boite de dialogue de chargement/sauvegarde des fichiers du projet. Cette
  // boite est toujours la m�me afin de conserver le contexte (nom du dernier
  // fichier ouvert, emplacement, etc.).
  private JFileChooser diPrj ;
  // private BuPreferencesFrame preferences_;
  private BuHelpFrame aide_;
  private BuMenuBar mb;
  private BuToolBar tb;
  private BuTaskView taches_;
  // Informations sur Reflux
  public final static BuInformationsSoftware isReflux_ = new BuInformationsSoftware();
  static {
    isReflux_.name = "Reflux";
    isReflux_.version = "5.15";
    isReflux_.date = "04-Oct-2004";
    isReflux_.contact = "patrick.gomi@equipement.gouv.fr";
    isReflux_.license = "GPL2";
    // isReflux_.logo =RefluxResource.REFLUX.getIcon("reflux-logo");
    isReflux_.rights = "1998-2000 GHN\n2000-2004 DeltaCAD";
    isReflux_.logo = null;
    isReflux_.banner = RefluxResource.REFLUX.getIcon("reflux-banner");
    isReflux_.ftp = null;
    isReflux_.http = "http://www.deltacad.fr";
    isReflux_.man = "http://www.utc.fr/fudaa/manuels";
    isReflux_.update = "http://www.utc.fr/fudaa/deltas/";
    isReflux_.authors = new String[] { "Bertrand Marchand" };
    isReflux_.contributors = null;
    isReflux_.testers = null;
    BuPrinter.INFO_LOG = isReflux_;
  }

  public RefluxImplementation() {
    projet = PRFabrique.getFabrique().creeProjet();
  }

  public void init() {
    super.init();
    // Contr�le de la date de validit�
    /*
     * if (RefluxLock.isTrue()) { new BuDialogError(this, getInformationsSoftware(), "La date d'�valution est maintenant
     * d�pass�e.\n Veuillez vous adresser "+ "au G.H.N.\n � l'adresse e-mail "+isReflux_.contact).activate();
     * System.exit(1); }
     */
    try {
      // Titre de l'application
      // BuInformationsSoftware is=getInformationsSoftware();
      changeTitle(null);
      // Menu standard
      mb = getApp().getMainMenuBar();
      mb.addMenu(buildMenuProprietes());
      mb.addMenu(buildMenuCalcul());
      // B.M. En attendant que BuCommonImplementation soit coh�rent
      mb.removeActionListener(this);
      mb.addActionListener(this);
      // Menu "Fen�tres" : Rajout d'un item "Nouvelle fen�tre"
      JMenu mn = mb.getMenu("MENU_FENETRES");
      BuSeparator se = new BuSeparator("Fen�tres");
      BuMenuItem mi = new BuMenuItem(RefluxResource.REFLUX.getIcon("post"), "Nouvelle fen�tre de post");
      mi.setActionCommand("NOUVELLEFENETRE");
      mi.setName("mi" + mi.getActionCommand());
      mi.setHorizontalTextPosition(SwingConstants.RIGHT);
      mi.addActionListener((BuMenu) mn);
      mi.setEnabled(false);
      mn.add(se, 1);
      mn.add(mi, 2);
      BuMenu m;
      m = (BuMenu) mb.getMenu("EXPORTER");
      m.addMenuItem("Fichier de sauvegarde (.pre)...", "EXPORTERPRE", true);
      m.addMenuItem("Fichiers de maillage Opthyca (.opn,.ope,.opf)...", "EXPORTEROPT", true);
      m.addMenuItem("Fichiers de donn�es Reflux...", "EXPORTERREFLUXCALCUL", true);
      m = (BuMenu) mb.getMenu("IMPORTER");
      m.addMenuItem("Solutions initiales...", "IMPORTERSOLINIT", true);
      // Barre d'outils standard
      tb = getApp().getMainToolBar();
      // B.M. En attendant que BuCommonImplementation soit coh�rent
      tb.removeActionListener(this);
      tb.addActionListener(this);
      tb.addSeparator();
      tb.addToolButton("Connecter", "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
      tb.addToolButton("Calculer", "CALCULER", false);
      // if (RefluxVariables.debug) tb.addToolButton("","TEST",true);
      // Activabilit� des commandes
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("EXPORTER", true);
      setEnabledForAction("IMPORTER", true);
      setEnabledForAction("EXPORTERPRE", false);
      setEnabledForAction("EXPORTEROPT", false);
      setEnabledForAction("EXPORTERREFLUXCALCUL", false);
      setEnabledForAction("IMPORTERSOLINIT", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("PREFERENCE", true);
      removeAction("VISIBLE_LEFTCOLUMN");
      removeAction("ASSISTANT");
      // R�cup�ration du panel principal
      BuMainPanel mp = getApp().getMainPanel();
      BuColumn rc = mp.getRightColumn();
      // mp.getLeftColumn().setBorder(null);
      // mp.getLeftColumn().setVisible(false);
      // --- Colonne de droite
      BuScrollPane sp;
      // Assistant
      // assistant_=new BuAssistant();
      // rc.addToggledComponent(BuResource.BU.getString("Assistant"),
      // "ASSISTANT",assistant_,this);
      // Vue des taches
      taches_ = new BuTaskView();
      sp = new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      rc.addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", sp, this);
      // Arbre des calques
      arbre_ = new BArbreCalque();
      sp = new BuScrollPane(arbre_);
      sp.setSize(150, 150);
      rc.addToggledComponent("Calques", "CALQUE", sp, this);
      // mp.setLogo(is.logo);
      mp.setTaskView(taches_);
      // Application des pr�f�rences
      BuPreferences.BU.applyOn(this);
      // Cr�ation de la boite de dialogue de chargement/enregistrement de fichier
      if (!(getApp() instanceof BuApplet)) {
        diPrj = new JFileChooser();
        diPrj.setFileHidingEnabled(true);
        diPrj.setCurrentDirectory(new File(System.getProperty("user.dir")));
        diPrj.setMultiSelectionEnabled(false);
      }
    } catch (Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
  }

  /**
   * rendu synchronized pour eviter des problemes de rafraichissement au lancement.
   */
  public synchronized void start() {
    super.start();
    BuMainPanel mp = this.getApp().getMainPanel();
    mp.getLeftColumn().setVisible(false);
    // Affichage de la fenetre principale
    // BGroupeCalque gc=new BGroupeCalque();
    // gc.setName("Calques");
    // BVueCalque vc=new BVueCalque();
    // vc.setCalque(gc);
    // vc.setTaskView(taches_);
    // arbre_.setCalque(gc);
    fnCalques_ = new PRFilleCalques(getApp(), arbre_);
    fnCalques_.getVueCalque().setTaskView(taches_);
    addInternalFrame(fnCalques_);
    // fnCalques_.setSize(getMainPanel().getDesktop().getSize());
    // fnCalques_.setLocation(0,0);
    fnCalques_.setVisible(false);
    // Pour afficher tous les calques dans l'arbre des calques.
    arbre_.refresh();
    PRPreferences.REFLUX.applyOn(this);
    // mp.doLayout();
    // mp.validate();
    // cmdConnecter();
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isReflux_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isReflux_;
  }

  private void changeTitle(String _racineProbleme) {
    BuInformationsSoftware is = getInformationsSoftware();
    String title = is.name + " " + is.version;
    if (_racineProbleme != null) title += " - " + _racineProbleme;
    setTitle(title);
  }

  /**
   * Suppression du desktop des fenetres de post traitement et de mise en page.
   */
  private void clearDeskTop() {
    // Suppression des anciennes fenetres de post.
    JInternalFrame[] frames = getAllInternalFrames();
    for (int i = frames.length - 1; i >= 0; i--)
      if (frames[i] instanceof RefluxFillePost) removeInternalFrame(frames[i]);
    getMainPanel().getDesktop().repaint();
  }

  // ------------------------------------------------------------------------------
  // --- ACTIONS ------------------------------------------------------------------
  // ------------------------------------------------------------------------------
  public void actionPerformed(ActionEvent _evt) {
    String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    if (action.equals("CREER")) cmdNouveauProjet();
    else if (action.equals("OUVRIR")) cmdOuvrir();
    else if (action.equals("ENREGISTRER")) cmdEnregistrer();
    else if (action.equals("EXPORTERPRE")) cmdExporterPRE();
    else if (action.equals("EXPORTEROPT")) cmdExporterOPT();
    else if (action.equals("EXPORTERREFLUXCALCUL")) cmdExporterREFCalcul();
    else if (action.equals("IMPORTERSOLINIT")) cmdImporterSolInit();
    /*
     * else if (action.equals("PREFERENCE")) cmdPreferences();
     */
    else if (action.equals("CALCULER")) cmdExecuterReflux();
    else if (action.equals("REPRENDRECALCUL")) cmdReprendreCalcul();
    else if (action.equals("AFFICHERTRACEEXE")) cmdAfficherTraceExe();
    else if (action.equals("PARAMETRESCALCUL")) cmdParametresCalcul();
    else if (action.equals("INTEGRATIONTEMPS")) cmdIntegrationTemps();
    // else if (action.equals("TEST" )) cmdTest();
    else if (action.equals("PROPSGLOBALES")) cmdPropsGlobales();
    else if (action.equals("PROPRIETE")) cmdPropsProjet();
    else if (action.equals("SOLINIGLOB")) cmdSolIniGlob();
    else if (action.equals("NOUVELLEFENETRE")) cmdNouvelleFenetre();
    // else if (action.equals("DEPLACEMENTPOINT"))
    // fnCalques_.cmdDeplacementPoint();
    else if (action.equals("PROPELEM")) fnCalques_.cmdProprietesFonds();
    else if (action.equals("DEFINITIONBORD")) fnCalques_.cmdDefinitionBord();
    else if (action.equals("DEFINITIONCARBORD")) fnCalques_.cmdDefinitionCaracteristiques();
    // else if (action.equals("INITMARES"))
    // fnCalques_.cmdInitialisationMares();
    else if (action.equals("CALSOLINI")) fnCalques_.cmdCalculSolutionsInitiales();
    else if (action.equals("SUPPRESSIONMARE")) fnCalques_.cmdSuppressionMare();
    else if (action.equals("ASSISTANT") || action.equals("TACHE") || action.equals("CALQUE")) {
      BuColumn rc = getMainPanel().getRightColumn();
      rc.toggleComponent(action);
      setCheckedForAction(action, rc.isToggleComponentVisible(action));
    } else
      super.actionPerformed(_evt);
  }

  /**
   * Affichage d'un URL dans le navigateur.
   */
  public void displayURL(String _url) {
    // Aide locale
    if (!"remote".equals(System.getProperty("net.access.man"))) {
      _url = "file:" + System.getProperty("user.dir") + "/manuels/reflux/index.html";
      // _url="file:///users/bmarchan/reflux50/projets/manuels/reflux/index.htm";
      System.out.println(_url);
    }
    // En interne
    if (BuPreferences.BU.getStringProperty("browser.internal").equals("true")) {
      if (aide_ == null) aide_ = new BuHelpFrame();
      addInternalFrame(aide_);
      aide_.setDocumentUrl(_url);
    }
    // En externe
    else {
      if (_url == null) {
        BuInformationsSoftware il = getInformationsSoftware();
        _url = il.http;
      }
      BuBrowserControl.displayURL(_url);
    }
  }

  /**
   * Commande de cr�ation d'un nouveau projet.
   */
  public void cmdNouveauProjet() {
    final BuCommonImplementation app = this;
    PRPnNouveauProjet pnNewPrj = new PRPnNouveauProjet();
    PRDialogPanneau di = new PRDialogPanneau(getFrame(), pnNewPrj, "Nouveau projet") {
      public void actionApply() {
        PRPnNouveauProjet pn = (PRPnNouveauProjet) getPanneauPrincipal();
        final int tpPrj = pn.getTypeProjet();
        final String racine = pn.getRacineProjet();
        final Object[] params = pn.getParametres();
        RefluxTacheOperation op = new RefluxTacheOperation(app, "Cr�ation d'un nouveau projet") {
          public void act() {
            oprNouveauPrj(this, tpPrj, racine, params);
          }
        };
        op.start();
      }
    };
    di.show();
    // dialogNouveauPrj_ = new PRDialogNouveauProjet();
    // dialogNouveauPrj_.addActionListener(new ActionListener() {
    // public void actionPerformed(ActionEvent _evt) {
    // dialogNouveauPrj_actionPerformed(_evt);
    // }
    // });
    // dialogNouveauPrj_.show();
  }

  // ---------------------------------------------------------------------------
  // --- Gestion des �venement provenant de la fenetre de dialogue nouveau pb
  // ---------------------------------------------------------------------------
  // private void dialogNouveauPrj_actionPerformed(ActionEvent _evt) {
  // if (_evt.getSource() == dialogNouveauPrj_.OK_BUTTON) {
  // RefluxTacheOperation op=new RefluxTacheOperation(this,"Cr�ation d'un nouveau projet") {
  // public void act() { oprNouveauPrj(this); }
  // };
  // op.start();
  // }
  // }
  /**
   * Op�ration de cr�ation d'un nouveau projet.
   * 
   * @param _tpPrj Type de projet
   * @param _racine La racine du projet.
   * @param _params Les parametres �ventuels du projet.
   */
  public void oprNouveauPrj(RefluxTacheOperation _operation, int _tpPrj, String _racine, Object[] _params) {
    // File fichier=dialogNouveauPrj_.getFichierSelectionne();
    // String path =fichier.getPath();
    // GrElement[] elements;
    // int tpPrj=dialogNouveauPrj_.getTypeProbleme();
    // RefluxResource.typeProjet=dialogNouveauPrj_.getTypeProbleme();
    // _operation.operation="Lecture du maillage";
    // getMainPanel().setMessage("Lecture du maillage depuis "+fichier+"...");
    getMainPanel().setMessage("Cr�ation du projet depuis " + _racine + "...");
    getMainPanel().setProgression(0);
    // projet=new PRProjet();
    try {
      clearDeskTop();
      getMainPanel().setProgression(10);
      projet = PRProjet.nouveau(_tpPrj, _racine, _params);
      changeTitle(projet.racineFichiers());
      getMainPanel().setProgression(80);
      fnCalques_.initialise(projet);
      fnCalques_.zoomEtendu();
      fnCalques_.setVisible(true);
      // diTemps_.initialise(projet);
      getMainPanel().setProgression(100);
      // Activabilit� des commandes
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("EXPORTERPRE", true);
      setEnabledForAction("EXPORTEROPT", true);
      setEnabledForAction("EXPORTERREFLUXCALCUL", true);
      setEnabledForAction("IMPORTERSOLINIT", true);
      setEnabledForAction("PARAMETRESCALCUL", true);
      setEnabledForAction("INTEGRATIONTEMPS", true);
      setEnabledForAction("CALCULER", projet.getInterfaceCalcul().serveurExiste());
      setEnabledForAction("REPRENDRECALCUL", projet.getInterfaceCalcul().serveurExiste());
      setEnabledForAction("PROPSGLOBALES", projet.modeleProprietes().proprietesGlobales().length != 0);
      setEnabledForAction("SOLINIGLOB", Definitions.getTypesSolutionsInitiales().length != 0);
      setEnabledForAction("GROUPER", Definitions.getTypesSolutionsInitiales().length != 0);
      setEnabledForAction("PROPRIETE", true);
      if (!projet.getInterfaceCalcul().serveurExiste()) new BuDialogWarning(this, getInformationsSoftware(),
          "Le serveur de calcul pour ce type de projet n'est pas pr�sent." + "\nVous ne pourrez pas passer de calcul.")
          .activate();
    } catch (FileNotFoundException ex) {
      new BuDialogError(this, getInformationsSoftware(), "Le fichier " + ex.getMessage() + " n'existe pas").activate();
      projet = PRFabrique.getFabrique().creeProjet();
    } catch (IOException ex) {
      new BuDialogError(this, getInformationsSoftware(), ex.getMessage()).activate();
      projet = PRFabrique.getFabrique().creeProjet();
    } finally {
      getMainPanel().setMessage("");
      getMainPanel().setProgression(0);
      valideActions();
    }
  }

  /**
   * Commande d'ouverture d'un projet existant.
   */
  public void cmdOuvrir() {
    BuFileFilter flt = new BuFileFilter("pre", "Projet");
    // diPrj.updateUI();
    diPrj.resetChoosableFileFilters();
    diPrj.addChoosableFileFilter(flt);
    diPrj.setFileFilter(flt);
    diPrj.setSelectedFile(new File(new File(projet.racineFichiers()).getName() + ".pre"));
    int returnVal = diPrj.showOpenDialog(getFrame());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      new BuTaskOperation(this, "Ouverture d'un projet") {
        public void act() {
          oprOuvrir();
        }
      }.start();
    }
  }

  /*
   * Op�ration d'ouverture d'un projet.
   */
  public void oprOuvrir() {
    File fichier = diPrj.getSelectedFile();
    // javax.swing.filechooser.FileFilter filtre =diPrj.getFileFilter();
    String nomFichier = fichier.getName();
    String path = fichier.getPath();
    // GrElement[] elements;
    // --------------------------------------------------------------------------
    // --- Ouverture depuis des fichiers ascii (.cor/.ele/.bth/.pre)
    // --- correspondants � une version inf�rieure � 4.0
    // --------------------------------------------------------------------------
    getMainPanel().setMessage("Chargement du projet depuis " + fichier + "...");
    getMainPanel().setProgression(0);
    try {
      if (!nomFichier.endsWith(".pre")) return;
      clearDeskTop();
      getMainPanel().setProgression(10);
      projet = PRProjet.ouvrir(path);
      getMainPanel().setProgression(20);
      changeTitle(projet.racineFichiers());
      getMainPanel().setProgression(70);
      fnCalques_.initialise(projet);
      fnCalques_.zoomEtendu();
      fnCalques_.setVisible(true);
      // diTemps_.initialise(projet);
      getMainPanel().setProgression(100);
      // Activabilit� des commandes
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("EXPORTERPRE", true);
      setEnabledForAction("EXPORTEROPT", true);
      setEnabledForAction("EXPORTERREFLUXCALCUL", true);
      setEnabledForAction("IMPORTERSOLINIT", true);
      setEnabledForAction("PARAMETRESCALCUL", true);
      setEnabledForAction("INTEGRATIONTEMPS", true);
      setEnabledForAction("CALCULER", projet.getInterfaceCalcul().serveurExiste());
      setEnabledForAction("REPRENDRECALCUL", projet.getInterfaceCalcul().serveurExiste());
      setEnabledForAction("PROPSGLOBALES", projet.modeleProprietes().proprietesGlobales().length != 0);
      setEnabledForAction("SOLINIGLOB", Definitions.getTypesSolutionsInitiales().length != 0);
      setEnabledForAction("GROUPER", Definitions.getTypesSolutionsInitiales().length != 0);
      setEnabledForAction("PROPRIETE", true);
      if (!projet.getInterfaceCalcul().serveurExiste()) new BuDialogWarning(this, getInformationsSoftware(),
          "Le serveur de calcul pour ce type de projet n'est pas pr�sent." + "\nVous ne pourrez pas passer de calcul.")
          .activate();
    } catch (FileNotFoundException ex) {
      ex.printStackTrace();
      new BuDialogError(this, getInformationsSoftware(), "Le fichier " + ex.getMessage() + " n'existe pas").activate();
      projet = PRFabrique.getFabrique().creeProjet();
    } catch (IOException ex) {
      ex.printStackTrace();
      new BuDialogError(this, getInformationsSoftware(), ex.getMessage()).activate();
      projet = PRFabrique.getFabrique().creeProjet();
    } finally {
      getMainPanel().setMessage("");
      getMainPanel().setProgression(0);
      valideActions();
    }
  }

  /**
   * Enregistrement du projet courant.
   */
  public void cmdEnregistrer() {
    new BuTaskOperation(this, "Enregistrement du projet") {
      public void act() {
        oprEnregistrer();
      }
    }.start();
  }

  /*
   * Op�ration d'enregistrement du projet courant
   */
  public void oprEnregistrer() {
    try {
      String path;
      File fichier;
      // ------------------------------------------------------------------------
      // --- Enregistrement du fichier projet .pre
      // ------------------------------------------------------------------------
      path = projet.racineFichiers() + ".pre";
      fichier = new File(path);
      getMainPanel().setMessage("Enregistrement du projet sur " + fichier + "...");
      getMainPanel().setProgression(0);
      getMainPanel().setProgression(10);
      // this.setEnabledForAction("CALCULER",false);
      // Fichier.enregistre(path, projet);
      projet.enregistrer(path);
      getMainPanel().setProgression(90);
      // this.setEnabledForAction("CALCULER",(SERVEUR_REFLUX != null));
      getMainPanel().setProgression(100);
      // ------------------------------------------------------------------------
      // --- Enregistrement du fichier des courbes .crb
      // ------------------------------------------------------------------------
      if (!projet.modCourbes) return;
      path = projet.racineFichiers() + ".crb";
      fichier = new File(path);
      getMainPanel().setMessage("Enregistrement des courbes sur " + fichier + "...");
      getMainPanel().setProgression(0);
      getMainPanel().setProgression(10);
      // this.setEnabledForAction("CALCULER",false);
      new PRExporte(getMainPanel(), projet).refluxCourbes(fichier);
      getMainPanel().setProgression(90);
      // this.setEnabledForAction("CALCULER",(SERVEUR_REFLUX != null));
      getMainPanel().setProgression(100);
      projet.modCourbes = false;
    } catch (FileNotFoundException _ex) {
      new BuDialogError(this, getInformationsSoftware(), "Le fichier " + _ex.getMessage() + " n'existe pas").activate();
    } catch (IOException ex) {
      new BuDialogError(this, getInformationsSoftware(), ex.getMessage()).activate();
    } finally {
      getMainPanel().setMessage("");
      getMainPanel().setProgression(0);
      valideActions();
    }
  }

  /**
   * Exportation du fichier .pre sous un nom diff�rent du nom courant.
   */
  public void cmdExporterPRE() {
    BuFileFilter flt = new BuFileFilter("pre", "Projet");
    // diPrj.updateUI();
    diPrj.resetChoosableFileFilters();
    diPrj.addChoosableFileFilter(flt);
    diPrj.setFileFilter(flt);
    diPrj.setSelectedFile(new File(new File(projet.racineFichiers()).getName() + ".pre"));
    int returnVal = diPrj.showSaveDialog((BuApplication) getApp());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      // Si existence du fichier, demande � l'utilisateur pour continuer.
      if (diPrj.getSelectedFile().exists()) {
        BuDialogConfirmation diMes = new BuDialogConfirmation(this, isReflux_, "Le fichier " + diPrj.getSelectedFile()
            + " existe d�j�.\n" + "Voulez vous continuer et l'�craser ?");
        if (diMes.activate() != JOptionPane.YES_OPTION) return;
      }
      new BuTaskOperation(this, "Exportation des donn�es") {
        public void act() {
          oprExporterPRE();
        }
      }.start();
    }
  }

  /*
   * Op�ration d'exportation du fichier .pre
   */
  public void oprExporterPRE() {
    File fichier = diPrj.getSelectedFile();
    // javax.swing.filechooser.FileFilter filtre =diPrj.getFileFilter();
    // String nomFichier=fichier.getName();
    String path = fichier.getPath();
    getMainPanel().setMessage("Exportation des donn�es sur " + fichier + "...");
    getMainPanel().setProgression(0);
    try {
      getMainPanel().setProgression(10);
      this.setEnabledForAction("CALCULER", false);
      projet.enregistrer(path);
      getMainPanel().setProgression(90);
      this.setEnabledForAction("CALCULER", projet.getInterfaceCalcul().serveurExiste());
      getMainPanel().setProgression(100);
    } catch (FileNotFoundException ex) {
      new BuDialogError(this, getInformationsSoftware(), "Le fichier " + ex.getMessage() + " n'existe pas").activate();
    } catch (IOException ex) {
      new BuDialogError(this, getInformationsSoftware(), ex.getMessage()).activate();
    } finally {
      getMainPanel().setMessage("");
      getMainPanel().setProgression(0);
      valideActions();
    }
  }

  /**
   * Exportation du maillage vers Opthyca.
   */
  public void cmdExporterOPT() {
    BuFileFilter flt = new BuFileFilter(new String[] { "opn", "ope", "opf" }, "Maillage Opthyca");
    // diPrj.updateUI();
    diPrj.resetChoosableFileFilters();
    diPrj.addChoosableFileFilter(flt);
    diPrj.setFileFilter(flt);
    diPrj.setSelectedFile(new File(new File(projet.racineFichiers()).getName() + ".opn"));
    int returnVal = diPrj.showSaveDialog((BuApplication) getApp());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      // Si existence du fichier, demande � l'utilisateur pour continuer.
      if (diPrj.getSelectedFile().exists()) {
        BuDialogConfirmation diMes = new BuDialogConfirmation(this, isReflux_, "Le fichier " + diPrj.getSelectedFile()
            + " existe d�j�.\n" + "Voulez vous continuer et l'�craser ?");
        if (diMes.activate() != JOptionPane.YES_OPTION) return;
      }
      new BuTaskOperation(this, "Exportation des donn�es vers Opthyca") {
        public void act() {
          oprExporterOPT();
        }
      }.start();
    }
  }

  /*
   * Op�ration d'exportation du maillage vers Opthyca
   */
  public void oprExporterOPT() {
    File fichier = diPrj.getSelectedFile();
    // javax.swing.filechooser.FileFilter filtre =diPrj.getFileFilter();
    // String nomFichier=fichier.getName();
    // String path =fichier.getPath();
    getMainPanel().setMessage("Exportation des donn�es sur " + fichier + "...");
    getMainPanel().setProgression(0);
    try {
      getMainPanel().setProgression(10);
      new PRExporte(getMainPanel(), projet).opthycaMaillage(fichier);
      getMainPanel().setProgression(100);
    } catch (FileNotFoundException _ex) {
      new BuDialogError(this, getInformationsSoftware(), "Le fichier " + _ex.getMessage() + " n'existe pas").activate();
    } catch (IOException ex) {
      new BuDialogError(this, getInformationsSoftware(), ex.getMessage()).activate();
    } finally {
      getMainPanel().setMessage("");
      getMainPanel().setProgression(0);
      valideActions();
    }
  }

  /**
   * Importation des solutions initiales depuis un fichier solutions.
   */
  public void cmdImporterSolInit() {
    BuFileFilter flt = new BuFileFilter(Definitions.getExtFichiersResultats(), "Solutions Reflux");
    // diPrj.updateUI();
    diPrj.resetChoosableFileFilters();
    diPrj.addChoosableFileFilter(flt);
    diPrj.setFileFilter(flt);
    int returnVal = diPrj.showOpenDialog((BuApplication) getApp());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      new BuTaskOperation(this, "Importation des solutions intiales") {
        public void act() {
          oprImporterSolInit();
        }
      }.start();
    }
  }

  /*
   * Op�ration d'importation des solutions initiales.
   */
  public void oprImporterSolInit() {
    File fichier = diPrj.getSelectedFile();
    // javax.swing.filechooser.FileFilter filtre =diPrj.getFileFilter();
    // String nomFichier=fichier.getName();
    // String path =fichier.getPath();
    getMainPanel().setMessage("Importation des solutions initiales depuis " + fichier + "...");
    getMainPanel().setProgression(0);
    try {
      getMainPanel().setProgression(10);
      new PRImport(projet).refluxSolutionsInitiales(fichier);
      getMainPanel().setProgression(100);
      fnCalques_.getVueCalque().repaint();
    } catch (IOException ex) {
      new BuDialogError(this, getInformationsSoftware(), ex.getMessage()).activate();
    } finally {
      getMainPanel().setMessage("");
      getMainPanel().setProgression(0);
      valideActions();
    }
  }

  /**
   * Exportation des donn�es vers Reflux.
   */
  public void cmdExporterREFCalcul() {
    BuFileFilter flt = new BuFileFilter(Definitions.getExtFichiersDonnees(), "Donn�es Reflux");
    // diPrj.updateUI();
    diPrj.resetChoosableFileFilters();
    diPrj.addChoosableFileFilter(flt);
    diPrj.setFileFilter(flt);
    diPrj.setSelectedFile(new File(new File(projet.racineFichiers()).getName() + ".inp"));
    int returnVal = diPrj.showSaveDialog((BuApplication) getApp());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      // Si existence du fichier, demande � l'utilisateur pour continuer.
      if (diPrj.getSelectedFile().exists()) {
        BuDialogConfirmation diMes = new BuDialogConfirmation(this, isReflux_, "Le fichier " + diPrj.getSelectedFile()
            + " existe d�j�.\n" + "Voulez vous continuer et l'�craser ?");
        if (diMes.activate() != JOptionPane.YES_OPTION) return;
      }
      new BuTaskOperation(this, "Exportation des donn�es vers Reflux") {
        public void act() {
          oprExporterREFCalcul();
        }
      }.start();
    }
  }

  /**
   * Op�ration d'exportation des donn�es vers Reflux.
   */
  public void oprExporterREFCalcul() {
    File fichier = diPrj.getSelectedFile();
    // javax.swing.filechooser.FileFilter filtre =diPrj.getFileFilter();
    // String nomFichier=fichier.getName();
    // String path =fichier.getPath();
    getMainPanel().setMessage("Exportation des donn�es sur " + fichier + "...");
    getMainPanel().setProgression(0);
    try {
      getMainPanel().setProgression(10);
      new PRExporte(getMainPanel(), projet).refluxDonnees(fichier);
      getMainPanel().setProgression(100);
    } catch (IllegalArgumentException _exc) {
      new BuDialogError(this, getInformationsSoftware(), _exc.getMessage()).activate();
    } catch (FileNotFoundException ex) {
      new BuDialogError(this, getInformationsSoftware(), "Le fichier " + ex.getMessage() + " n'existe pas").activate();
    } catch (IOException ex) {
      new BuDialogError(this, getInformationsSoftware(), ex.getMessage()).activate();
    } finally {
      getMainPanel().setMessage("");
      getMainPanel().setProgression(0);
      valideActions();
    }
  }

  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuLookPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new EbliCalquesPreferencesPanel());
    _prefs.add(new PRPnPreferences(this));

  }

  /**
   * Ex�cution de reflux.
   */
  public void cmdExecuterReflux() {
    new RefluxTacheOperation(this, "Ex�cution de Reflux") {
      // new BuTaskOperation(this,"Ex�cution de Reflux") {
      public void act() {
        oprExecuterReflux();
      }
    }.start();
  }

  /**
   * T�che d'ex�cution de Reflux.
   */
  public void oprExecuterReflux() {
    try {
      main_panel_.setMessage("Ex�cution de Reflux en t�che de fond...");
      main_panel_.setProgression(10);
      setEnabledForAction("CALCULER", false);
      setEnabledForAction("REPRENDRECALCUL", false);
      projet.getInterfaceCalcul().calculer(projet);
    } catch (IllegalArgumentException _exc) {
      new BuDialogError(this, getInformationsSoftware(), _exc.getMessage()).activate();
      return;
    } finally {
      main_panel_.setMessage("");
      main_panel_.setProgression(0);
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("REPRENDRECALCUL", true);
    }
    String msg;
    if (projet.getInterfaceCalcul().calculEstOK()) msg = "Le calcul de Reflux s'est termin� avec succes";
    else
      msg = "Le calcul de Reflux s'est termin� avec des erreurs";
    new BuDialogMessage(this, getInformationsSoftware(), msg).activate();
    cmdAfficherTraceExe();
    setEnabledForAction("AFFICHERTRACEEXE", true);
    valideActions();
  }

  /**
   * Afficher la trace d'�x�cution.
   */
  private void cmdAfficherTraceExe() {
    BuLabelMultiLine lb = new BuLabelMultiLine(projet.getInterfaceCalcul().calculTraceExecution());
    lb.setFont(new Font("Monospaced", Font.PLAIN, 10));
    BuDialogMessage di = new BuDialogMessage(this, getInformationsSoftware(), lb);
    di.setTitle("Trace d'ex�cution de Reflux");
    di.setResizable(true);
    di.activate();
  }

  /**
   * Reprendre le calcul. 1. Importation des solutions initiales depuis fichier de calcul serveur. 2. Affichage d'un
   * message pour poursuite.
   */
  private void cmdReprendreCalcul() {
    PRModeleCalcul mdlCal = projet.modeleCalcul();
    PRSolutionsInitiales[] sis;
    try {
      sis = projet.getInterfaceCalcul().getSolutionsFinales(projet);
      mdlCal.solutionsInitiales(sis);
      new BuDialogMessage(this, getInformationsSoftware(),
          "Les solutions du pr�c�dent calcul sont � pr�sent r�cup�r�es.\n"
              + "Vous pouvez modifier les donn�es et relancer le calcul.").activate();
      fnCalques_.getVueCalque().repaint();
    } catch (IOException ex) {
      new BuDialogError(this, getInformationsSoftware(), ex.getMessage()).activate();
    }
  }

  /**
   * Parametres du calcul.
   */
  public void cmdParametresCalcul() {
    PRPnParamsCalcul pnPC = PRFabrique.getFabrique().creePnParamsCalcul();
    pnPC.setProjet(projet);
    pnPC.setParametres(projet.modeleCalcul().parametresCalcul());
    PRDialogPanneau di = new PRDialogPanneau(getFrame(), pnPC, "Param�tres du calcul") {
      public void actionApply() {
        PRPnParamsCalcul pn = (PRPnParamsCalcul) getPanneauPrincipal();
        projet.modeleCalcul().parametresCalcul(pn.getParametres());
      }
    };
    di.show();
  }

  /**
   * Integration en temps.
   */
  public void cmdIntegrationTemps() {
    PRPnTemps pnTps = new PRPnTemps();
    pnTps.setGroupesPT(projet.modeleCalcul().groupesPT());
    PRDialogPanneau di = new PRDialogPanneau(getFrame(), pnTps, "Int�gration en temps") {
      public void actionApply() {
        PRPnTemps pn = (PRPnTemps) getPanneauPrincipal();
        projet.modeleCalcul().groupesPT(pn.getGroupesPT());
      }
    };
    pnTps.setParent(di);
    di.show();
  }

  /**
   * Propri�t�s globales. S'il existe des propri�t�s globales pour le type de calcul, la commande est active.
   */
  public void cmdPropsGlobales() {
    PRPnPropsGlobales pnPG = PRFabrique.getFabrique().creePnPropsGlobales();
    pnPG.setProprietes(projet.modeleProprietes().proprietesGlobales());
    PRDialogPanneau di = new PRDialogPanneau(getFrame(), pnPG, "Propri�t�s globales") {
      public void actionApply() {
        PRPnPropsGlobales pn = (PRPnPropsGlobales) getPanneauPrincipal();
        projet.modeleProprietes().proprietesGlobales(pn.getProprietes());
      }
    };
    di.show();
  }

  /**
   * Propri�t�s du projet.
   */
  public void cmdPropsProjet() {
    PRPnPropsProjet pnPP = new PRPnPropsProjet();
    pnPP.setProjet(projet);
    PRDialogPanneau di = new PRDialogPanneau(getFrame(), pnPP, "Propri�t�s du projet");
    // {
    // public void actionApply() {
    // PRPnPropsGlobales pn=(PRPnPropsGlobales)getPanneauPrincipal();
    // projet.modeleProprietes().proprietesGlobales(pn.getProprietes());
    // }
    // };
    di.show();
  }

  /**
   * Initialisation des solutions initiales avec une valeur globale.
   */
  public void cmdSolIniGlob() {
    PRPnValeursGlobalesSI pnSI = new PRPnValeursGlobalesSI();
    pnSI.setTypesSI(Definitions.getTypesSolutionsInitiales());
    PRDialogPanneau di = new PRDialogPanneau(getFrame(), pnSI, "Solutions initiales globales") {
      public void actionApply() {
        PRPnValeursGlobalesSI pn = (PRPnValeursGlobalesSI) getPanneauPrincipal();
        projet.initialiserSolutionsInitiales(pn.getValeursGlobales());
        fnCalques_.getVueCalque().repaint();
      }
    };
    di.show();
  }

  /**
   * Cr�er une nouvelle fen�tre de post traitement.
   */
  private void cmdNouvelleFenetre() {
    RefluxFillePost fn = new RefluxFillePost(arbre_);
    fn.initialise(projet);
    fn.getVueCalque().setTaskView(taches_);
    addInternalFrame(fn);
    fn.restaurer();
    fn.setClosable(true);
    // fnsCalques_.add(fn);
  }

  /**
   * Active ou non les actions en fonction de l'�tat de l'application.
   */
  private void valideActions() {
    boolean b;
    b = projet != null;
    setEnabledForAction("NOUVELLEFENETRE", b);
  }

  // ------------------------------------------------------------------------------
  // --- MENUS --------------------------------------------------------------------
  // ------------------------------------------------------------------------------
  // Le menu "Calcul"
  private BuMenu buildMenuCalcul() {
    BuMenu r = new BuMenu("Calcul", "CALCUL");
    r.addMenuItem("Param�tres du calcul...", "PARAMETRESCALCUL", BuResource.BU.getIcon("parametre"), false);
    r.addMenuItem("Int�gration en temps...", "INTEGRATIONTEMPS", RefluxResource.REFLUX.getIcon("temps"), false);
    r.addSeparator();
    r.addMenuItem("Ex�cuter Reflux", "CALCULER", false);
    r.addMenuItem("Reprendre le calcul", "REPRENDRECALCUL", false);
    r.addMenuItem("Afficher trace d'ex�cution...", "AFFICHERTRACEEXE", BuResource.BU.getIcon("texte"), false);
    r.addSeparator("Solutions initiales");
    r.addMenuItem("Solutions initiales globales...", "SOLINIGLOB", false);
    r.addMenuItem("Solutions initiales par multiplans", "CALSOLINI", RefluxResource.REFLUX.getIcon("calsolini"), false);
    r.addMenuItem("Supprimer une zone inond�e", "SUPPRESSIONMARE", RefluxResource.REFLUX.getIcon("supzonesmouillees"),
        false);
    // r.addMenuItem(
    // "D�placer un point du plan de r�f�rence...",
    // "DEPLACEMENTPOINT",
    // RefluxResource.REFLUX.getIcon("deplacementpoint"),
    // false);
    return r;
  }

  // Le menu "Propri�t�s"
  private BuMenu buildMenuProprietes() {
    BuMenu r = new BuMenu("Propri�t�s", "PROPRIETES");
    r.addMenuItem("Propri�t�s des fonds...", "PROPELEM", RefluxResource.REFLUX.getIcon("proprietes"), false);
    r.addMenuItem("Propri�t�s des bords...", "DEFINITIONBORD", RefluxResource.REFLUX.getIcon("naturesbord"), false);
    r.addMenuItem("Propri�t�s globales...", "PROPSGLOBALES", RefluxResource.REFLUX.getIcon("propsglobales"), false);
    r.addSeparator();
    r.addMenuItem("Caract�ristiques de bord...", "DEFINITIONCARBORD",
        RefluxResource.REFLUX.getIcon("caracteristiques"), false);
    return r;
  }

  public void exit() {
    closeConnexions();
    super.exit();
  }

  public void finalize() {
    closeConnexions();
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return PRPreferences.REFLUX;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
    Reflux.CONNEXION_REFLUX = null;
    Reflux.SERVEUR_REFLUX = null;
    Reflux.CONNEXION_DUNES = null;
    Reflux.SERVEUR_DUNES = null;
    Reflux.CONNEXION_REFLUX3D = null;
    Reflux.SERVEUR_REFLUX3D = null;
    Reflux.CONNEXION_OLB = null;
    Reflux.SERVEUR_OLB = null;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(Reflux.SERVEUR_OLB, Reflux.CONNEXION_OLB);
    FudaaDodicoTacheConnexion c1 = new FudaaDodicoTacheConnexion(Reflux.SERVEUR_DUNES, Reflux.CONNEXION_DUNES);
    FudaaDodicoTacheConnexion c2 = new FudaaDodicoTacheConnexion(Reflux.SERVEUR_REFLUX, Reflux.CONNEXION_REFLUX);
    FudaaDodicoTacheConnexion c3 = new FudaaDodicoTacheConnexion(Reflux.SERVEUR_REFLUX3D, Reflux.CONNEXION_REFLUX3D);
    return new FudaaDodicoTacheConnexion[] { c, c1, c2, c3 };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculOlb.class, DCalculDunes.class, DCalculReflux.class, DCalculReflux3d.class };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(Map _r) {
    FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculReflux.class);
    Reflux.CONNEXION_REFLUX = c.getConnexion();
    Reflux.SERVEUR_REFLUX = ICalculRefluxHelper.narrow(c.getTache());
    c = (FudaaDodicoTacheConnexion) _r.get(DCalculReflux3d.class);
    Reflux.CONNEXION_REFLUX3D = c.getConnexion();
    Reflux.SERVEUR_REFLUX3D = ICalculReflux3dHelper.narrow(c.getTache());
    c = (FudaaDodicoTacheConnexion) _r.get(DCalculOlb.class);
    Reflux.CONNEXION_OLB = c.getConnexion();
    Reflux.SERVEUR_OLB = ICalculOlbHelper.narrow(c.getTache());
    c = (FudaaDodicoTacheConnexion) _r.get(DCalculDunes.class);
    Reflux.CONNEXION_DUNES = c.getConnexion();
    Reflux.SERVEUR_DUNES = ICalculDunesHelper.narrow(c.getTache());
  }
}
