package org.fudaa.fudaa.reflux;
import java.util.EventObject;
public class RefluxDataChangeEvent extends EventObject {
  /** All results steps remove. */
  public static final int RESULTS_CLEARED= 0;
  /** Add a step to results. */
  public static final int RESULT_ADDED= 1;
  /** Change results. */
  public static final int RESULTS_CHANGED= 2;
  public int type;
  public RefluxDataChangeEvent(Object _source, int _type) {
    super(_source);
    type= _type;
  }
}