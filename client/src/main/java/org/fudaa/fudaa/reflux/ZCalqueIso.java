/*
 * @file         ZCalqueIso.java
 * @creation     1999-08-10
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Rectangle;

import org.fudaa.ebli.calque.BCalqueCarte;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.ZModeleMaillage;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;

import org.fudaa.fudaa.commun.trace2d.BPaletteCouleurPlage;
import org.fudaa.fudaa.commun.trace2d.TraceIsoLignesPlages;
import org.fudaa.fudaa.commun.trace2d.TraceIsoSurfacesPlages;
import org.fudaa.fudaa.commun.trace2d.ZModeleChangeEvent;
import org.fudaa.fudaa.commun.trace2d.ZModeleChangeListener;
import org.fudaa.fudaa.commun.trace2d.ZModeleValeur;

/**
 * Un calque de trace de cartes avec un nouvel algorithme de trace.
 *
 * @version      $Id: ZCalqueIso.java,v 1.12 2006-09-19 15:11:53 deniger Exp $
 * @author       Bertrand Marchand , Bertrand Marchand
 */
public class ZCalqueIso extends BCalqueCarte implements ZModeleChangeListener {
  BPaletteCouleurPlage pal_;
  private GrBoite boite_;
  /** Le modele de maillage. */
  private ZModeleMaillage maillage_;
  private ZModeleValeur valeurs_;
  /**
   * Contructeur du calque.
   */
  public ZCalqueIso() {
    super();
    pal_= new BPaletteCouleurPlage();
    pal_.setTitre("");
    pal_.setSousTitre("");
    pal_.setNbPlages(10);
  }
  public void setPalette(BPaletteCouleurPlage _pal) {
    pal_= _pal;
    construitLegende();
  }
  public BPaletteCouleurPlage getPalette() {
    return pal_;
  }
  /**
   * + Reinitialisation de la palette.
   */
  /*  public void setValeurs(double[] _vals) {
      super.setValeurs(_vals);

      double vmin=Double.POSITIVE_INFINITY;
      double vmax=Double.NEGATIVE_INFINITY;
      for (int i=0; i<_vals.length; i++) {
        vmin=Math.min(vmin,_vals[i]);
        vmax=Math.max(vmax,_vals[i]);
      }

      pal_.setMinPalette(vmin);
      pal_.setMaxPalette(vmax);
      pal_.ajustePlages();
      pal_.ajusteLegendes();

      construitLegende();
    }*/
  /**
   * Affectation du modele de maillage.
   * @param _maillage Le modele.
   */
  public void setModeleMaillage(ZModeleMaillage _maillage) {
    ZModeleMaillage vp= maillage_;
    maillage_= _maillage;
    boite_= null;
    firePropertyChange("maillage", vp, maillage_);
    repaint();
  }
  /**
   * Retourne le modele de maillage.
   * @return Le modele.
   */
  public ZModeleMaillage getModeleMaillage() {
    return maillage_;
  }
  /**
   * Affectation du modele de valeurs.
   * @param _modele Le modele
   */
  public void setModeleValeurs(ZModeleValeur _modele) {
    //ZModeleValeur vp=valeurs_;
    //    if (valeurs_!=null) valeurs_.removeModelChangeListener(this);
    valeurs_= _modele;
    //    if (valeurs_!=null) valeurs_.addModelChangeListener(this);
    modelChanged(null);
  }
  /**
   * Retourne le modele de valeurs.
   * @return Le modele.
   */
  public ZModeleValeur getModeleValeur() {
    return valeurs_;
  }
  // >>> ZModeleChangeListener  ------------------------------------------------
  public void modelChanged(ZModeleChangeEvent _evt) {
    // Mise � jour de la palette en fonction des valeurs (d�sactiv� pour le chgt de pas de temps).
    pal_.setMinPalette(valeurs_.getMin());
    pal_.setMaxPalette(valeurs_.getMax());
    pal_.ajustePlages();
    pal_.ajusteLegendes();
    construitLegende();
  }
  // <<< ZModeleChangeListener  ------------------------------------------------
  /**
   * Construction de la legende. En fait, affecte la legende du calque au calque
   * d'affichage des legendes.
   */
  protected void construitLegende() {
    BCalqueLegende cqLg= getLegende();
    if (cqLg == null)
      return;
    cqLg.enleve(this);
    //    p.setOpaque(false);
    //    cqLg.ajoute(this, paletteLeg, p);
    cqLg.ajoute(this, pal_);
    pal_.repaint();
  }

  // Icon
  public void paintIcon(Component _c, Graphics _g, int _x, int _y) {
    //    super.paintIcon(_c,_g,_x,_y);
    _g.translate(_x, _y);
    boolean attenue= isAttenue();
    int w= getIconWidth();
    int h= getIconHeight();
    Color fg= getForeground();
    Color bg= getBackground();
    if (attenue)
      fg= attenueCouleur(fg);
    if (attenue)
      bg= attenueCouleur(bg);
    {
      Color c;
      c= Color.red;
      if (attenue)
        c= attenueCouleur(c);
      _g.setColor(c);
      _g.fillRect(1, 1, w - 1, h - 1);
      c= Color.yellow;
      if (attenue)
        c= attenueCouleur(c);
      _g.setColor(c);
      _g.fillOval(3, 3, w - 5, h - 5);
      c= Color.blue;
      if (attenue)
        c= attenueCouleur(c);
      _g.setColor(c);
      _g.fillOval(7, 7, w - 14, h - 14);
    }
    _g.translate(-_x, -_y);
  }
  // Paint
  public void paintComponent(Graphics _g) {
    boolean attenue= isAttenue();
    boolean rapide= isRapide();
    int i, j;
    if (boite_ == null)
      boite_= getModeleMaillage().getDomaine();
    GrMorphisme versEcran= getVersEcran();
    Polygon pecr= boite_.enPolygoneXY().applique(versEcran).polygon();
    Rectangle clip= _g.getClipBounds();
    if (clip == null)
      clip= new Rectangle(0, 0, getWidth(), getHeight());
    if (clip.intersects(pecr.getBounds())) {
      if (rapide) {
        Color c;
        c= pal_.getCouleurAutres();
        if (attenue)
          c= attenueCouleur(c);
        _g.setColor(c);
        _g.drawPolygon(pecr);
      } else {
        Color fg= getForeground();
        Color bg= getBackground();
        if (attenue)
          fg= attenueCouleur(fg);
        if (attenue)
          bg= attenueCouleur(bg);
        // Palette background
        BPaletteCouleurPlage pbg= (BPaletteCouleurPlage)pal_.clone();
        pbg.propagerCouleurs(bg, bg);
        BPaletteCouleurPlage pisl= pal_;
        if (getIsosurfaces())
          pisl= pbg;
        TraceIsoLignesPlages isol= new TraceIsoLignesPlages(pisl);
        TraceIsoSurfacesPlages isos= new TraceIsoSurfacesPlages(pal_);
        int n= maillage_.nombrePolygones();
        //        double[] vals=getValeurs();
        boolean isIsoSurfacesVisible= getIsosurfaces();
        boolean isIsoLignesVisible= getIsolignes();
        boolean isContoursVisible= getContour();
        for (i= 0; i < n; i++) {
          Polygon p= maillage_.polygone(i).applique(versEcran).polygon();
          if (!clip.intersects(p.getBounds()))
            continue;
          // Trace des isosurfaces / Isolignes (dans la couleur du fond si les isocouleurs sont
          // tracees).
          if (isIsoSurfacesVisible || isIsoLignesVisible) {
            int[] noeuds= maillage_.connectivites(i);
            int m= noeuds.length;
            double[] v= new double[m];
            for (j= 0; j < m; j++)
              v[j]= valeurs_.valeur(noeuds[j]);
            if (isIsoSurfacesVisible)
              isos.draw(_g, p, v);
            if (isIsoLignesVisible)
              isol.draw(_g, p, v);
          }
          // Trace des contours (dans la couleur du trace).
          if (isContoursVisible) {
            _g.setColor(fg);
            _g.drawPolygon(p);
          }
        }
      }
    }
  }
  public GrBoite getDomaine() {
    GrBoite r= super.getDomaine();
    if (maillage_ != null) {
      GrBoite b= maillage_.getDomaine();
      if (r == null)
        r= b;
      else
        r= r.union(b);
    }
    return r;
  }
}
