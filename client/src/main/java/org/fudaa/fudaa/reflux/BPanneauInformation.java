/*
 * @file         BPanneauSelectedStep.java
 * @creation     1999-07-09
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
/**
 * Un panneau d'information sur un objet.
 *
 * @version      $Id: BPanneauInformation.java,v 1.6 2006-09-19 15:11:53 deniger Exp $
 * @author       Bertrand Marchand
 */
public class BPanneauInformation extends JPanel {
  boolean selEventSent_;
  BorderLayout lyThis= new BorderLayout();
  JScrollPane spInfo= new JScrollPane();
  JTextArea taInfo= new JTextArea();
  /**
   * Constructeur.
   */
  public BPanneauInformation() {
    super();
    jbInit();
  }
  /**
   * D�finition de l'IU.
   */
  public void jbInit() {
    this.setLayout(lyThis);
    this.setPreferredSize(new Dimension(150, 215));
    taInfo.setFont(new java.awt.Font("SansSerif", 0, 10));
    taInfo.setEditable(false);
    this.add(spInfo, BorderLayout.CENTER);
    spInfo.getViewport().add(taInfo, null);
  }
  /**
   * Texte � afficher. Le texte peut comporter des sauts de lignes, sous forme
   * de "\n" dans le String.
   *
   * @param _texte Le texte.
   */
  public void setInfo(String _texte) {
    taInfo.setText(_texte);
  }
  /**
   * Retourne le texte affich�.
   */
  public String getInfo() {
    return taInfo.getText();
  }
  /**
   * Pour test.
   */
  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(
        "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
    } catch (Exception _exc) {}
    JFrame f= new JFrame("Information");
    BPanneauInformation pn= new BPanneauInformation();
    pn.setInfo(
      "** Objet : Noeud\n"
        + "Num�ro : 621\n"
        + "X : "
        + (float) (343.434326)
        + "\n"
        + "Y : "
        + (float) (10.0)
        + "\n"
        + "\n"
        + "** R�sultats � t="
        + (float) (3600.0)
        + "\n"
        + "Coordonn�e X : "
        + (float) (343.43)
        + "\n"
        + "Coordonn�e Y : "
        + (float) (10.0)
        + "\n"
        + "Bathym�trie : "
        + (float) (13.23)
        + "\n"
        + "Vitesse X : "
        + (float) (2.2271)
        + "\n"
        + "Vitesse Y : "
        + (float) (1.5937E-7)
        + "\n"
        + "Niveau d'eau : "
        + (float) (15.286)
        + "\n"
        + "Profondeur : "
        + (float) (1.3161)
        + "\n"
        + "Norme vitesse : "
        + (float) (2.227100000000006));
    f.getContentPane().add(pn, BorderLayout.CENTER);
    f.pack();
    f.show();
    //    }
  }
}
