/*
 * @file         RefluxGrBord.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import org.fudaa.ebli.geometrie.GrPolygone;
/**
 * Une classe d'affichage d'un segment.
 *
 * @version      $Revision: 1.4 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */

public class RefluxGrPlan extends GrPolygone {
  public RefluxPlan pl_;

  public RefluxGrPlan(RefluxPlan _pl) {
    RefluxSegment[] sgs=_pl.getSegments();
    for (int i=0; i<sgs.length; i++) {
      RefluxSegment sg=sgs[i];
      sommets_.ajoute((sg.e[0]+sg.o[0])/2.,(sg.e[1]+sg.o[1])/2.,0);
      if (i!=sgs.length) sommets_.ajoute(sg.e[0],sg.e[1],0);
    }
    for (int i=sgs.length-1; i>=0; i--) {
      RefluxSegment sg=sgs[i];
      sommets_.ajoute(sg.o[0],sg.o[1],0);
      sommets_.ajoute((sg.e[0]+sg.o[0])/2.,(sg.e[1]+sg.o[1])/2.,0);
    }
    pl_=_pl;
  }

  public RefluxPlan getPlan() {
    return pl_;
  }
}
