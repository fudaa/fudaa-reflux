/*
 * @file         RefluxGrBord.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Color;
import java.awt.Point;

import org.fudaa.ebli.geometrie.GrContour;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.trace.TraceLigne;
/**
 * Une classe d'affichage d'un bord en fonction de sa nature.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class RefluxGrBord implements GrContour {
  public GrPolyligne ligne;
  private int typeTrait_;
  private Color couleur_;
  public RefluxGrBord() {
    this(Color.black, TraceLigne.LISSE);
  }
  public RefluxGrBord(Color _couleur, int _typeTrait) {
    typeTrait_= _typeTrait;
    couleur_= _couleur;
    ligne= new GrPolyligne();
  }
  /**
   * Accesseurs couleur.
   */
  public void setCouleur(Color _couleur) {
    couleur_= _couleur;
  }
  public Color getCouleur() {
    return couleur_;
  }
  /**
   * Accesseur type de trait.
   */
  public void setTypeTrait(int _typeTrait) {
    typeTrait_= _typeTrait;
  }
  public int getTypeTrait() {
    return typeTrait_;
  }
  /**
   * Points de saisie.
   */
  public GrPoint[] contour() {
    return ligne.contour();
  }
  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * @see GrContour
   *
   * @author b.marchand Le 26/10/2000
   */
  public final boolean estSelectionne(
    GrMorphisme _ecran,
    double _dist,
    Point _pt) {
    if (ligne != null)
      return ligne.estSelectionne(_ecran, _dist, _pt);
    return false;
  }
}