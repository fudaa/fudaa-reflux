/*
 * @file         RefluxGrBord.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import org.fudaa.ebli.geometrie.GrPolyligne;
/**
 * Une classe d'affichage d'un segment.
 *
 * @version      $Revision: 1.4 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */

public class RefluxGrSegment extends GrPolyligne {
  public RefluxSegment sg_;
  public RefluxGrSegment(RefluxSegment _sg) {
    sg_=_sg;
    sommets_.ajoute(_sg.o[0],_sg.o[1],0);
    sommets_.ajoute(_sg.e[0],_sg.e[1],0);
  }

  public RefluxSegment getSegment() {
    return sg_;
  }
}
