/*
 * @file         HY3dFabrique.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d3;
import org.fudaa.fudaa.reflux.PRFabrique;
import org.fudaa.fudaa.reflux.PRPnParamsCalcul;
import org.fudaa.fudaa.reflux.PRPnPropsGlobales;
import org.fudaa.fudaa.reflux.PRPnSaisieValeurPE;
import org.fudaa.fudaa.reflux.PRPnSaisieValeurPEDefaut;
import org.fudaa.fudaa.reflux.PRProjet;
import org.fudaa.fudaa.reflux.PRPropriete;
/**
 * Une fabrique pour toutes les sp�cificit�s Reflux 3D courantologie.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class HY3dFabrique extends PRFabrique {
  /**
   * Cr�ation d'un projet.
   */
  public PRProjet creeProjet() {
    return new HY3dProjet();
  }
  /**
   * Cr�ation d'un panel de parametres de calcul.
   */
  public PRPnParamsCalcul creePnParamsCalcul() {
    return new HY3dPnParamsCalcul();
  }
  /**
   * Cr�ation d'un panel de propri�t�s physiques globales.
   */
  public PRPnPropsGlobales creePnPropsGlobales() {
    return new HY3dPnPropsGlobales();
  }
  /**
   * Cr�ation d'un panel d'�dition de groupe de propri�t� pour un type de
   * groupe de propri�t�s.
   *
   * @param _tpNat Le type de groupe de PE.
   */
  //  public PRPnGPEEditor creePnGPEEditor(int _tpNat) {
  //    switch (_tpNat) {
  //      default:
  //        return new PRPnGPEDefaultEditor(_tpNat);
  //
  //      case PRNature.HYDRO_FOND_3D:
  //        return new HY3dPnGPEFond3DEditor();
  //    }
  //  }
  /**
   * Cr�ation d'un composant de saisie de valeur d'une propri�t� �l�mentaire.
   *
   * @param _tpPrp Le type de propri�t�.
   */
  public PRPnSaisieValeurPE creeCpSaisieValeurPE(int _tpPrp) {
    switch (_tpPrp) {
      default :
        return new PRPnSaisieValeurPEDefaut();
      case PRPropriete.BASE :
        return new HY3dPnSaisieValeurPEBase();
    }
  }
}