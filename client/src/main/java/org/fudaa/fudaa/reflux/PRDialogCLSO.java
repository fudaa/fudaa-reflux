/*
 * @file         PRDialogCLSO.java
 * @creation     1999-06-28
 * @modification $Date: 2007-01-19 13:14:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.memoire.bu.BuCheckBox3States;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.mesure.IEvolution;
import org.fudaa.dodico.corba.mesure.IEvolutionConstante;
import org.fudaa.dodico.corba.mesure.IEvolutionSerieIrreguliere;

import org.fudaa.dodico.objet.UsineLib;

import org.fudaa.ebli.geometrie.GrNoeud;

import org.fudaa.fudaa.commun.dodico.Profil1D2D;
import org.fudaa.fudaa.commun.impl.FudaaDialog;
/**
 * Une boite de dialog de saisie des conditions limites + sollicitations pour
 * les noeuds s�lectionn�s.
 *
 * @version      $Revision: 1.10 $ $Date: 2007-01-19 13:14:36 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRDialogCLSO extends FudaaDialog {
  BuGridLayout lyCL= new BuGridLayout();
  BuCheckBox3States[] cbImpose;
  BuCheckBox3States[] cbTransitoire;
  BuTextField[] tfValeur;
  JComboBox[] coCourbe;
  int nbCar;
  boolean modCourbe;
  PRProjet projet_;
  Object[] selects_= new Object[0];
  public PRDialogCLSO() {
    this(null);
  }
  public PRDialogCLSO(Frame _parent) {
    super(_parent, OK_CANCEL_APPLY_OPTION);
    /**Jbuilder**/
    jbInit();
  }
  public void jbInit() { /**JBuilder**/
    lyCL.setVfilled(false);
    lyCL.setVgap(5);
    lyCL.setHgap(5);
    lyCL.setXAlign(0.0f);
    lyCL.setHfilled(false);
    pnAffichage_.setLayout(lyCL);
    setModal(false);
    //    setModal(true);
    setTitle("Conditions limites et sollicitations");
    pack();
    setSize(getPreferredSize());
  }
  /**
   * Association du projet.
   */
  public void setProjet(PRProjet _projet) {
    projet_= _projet;
    modCourbe= false;
    initInterface();
  }
  /**
   * Mise a jour de la boite de dialogue avec les objets s�lectionn�s.
   */
  public void setSelection(Object[] _selects) {
    if (_selects == null
      || _selects.length == 0
      || !(_selects[0] instanceof GrNoeud)) {
      setNoSelection();
      return;
    }
    selects_= _selects;
    int[] cLImpose;
    int[] cLStat;
    Double[] cLValeur;
    IEvolution[] cLCourbe;
    int[] sOImpose;
    int[] sOStat;
    Double[] sOValeur;
    IEvolution[] sOCourbe;
    int cLImposeDefaut= BuCheckBox3States.STATE_DESELECTED;
    int cLStatDefaut= BuCheckBox3States.STATE_DESELECTED;
    Double cLValeurDefaut= new Double(0.0);
    int sOImposeDefaut= BuCheckBox3States.STATE_DESELECTED;
    int sOStatDefaut= BuCheckBox3States.STATE_DESELECTED;
    Double sOValeurDefaut= new Double(0.0);
    int impose;
    int stationnaire;
    Double valeur;
    IEvolution courbe;
    Vector supports;
    PRConditionLimite[] cond= projet_.conditionsLimites();
    PRSollicitation[] soll= projet_.sollicitations();
    PRConditionLimite cLCourante;
    PRSollicitation sOCourante;
    int[] typesCond= Definitions.getTypesConditions();
    int[] typesSoll= Definitions.getTypesSollicitations();
    Hashtable[] noeudToCond;
    Hashtable[] noeudToSoll;
    // Cr�ation des tables noeud->Cond, noeud->Soll
    noeudToCond= new Hashtable[typesCond.length];
    for (int i= 0; i < typesCond.length; i++) {
      noeudToCond[i]= new Hashtable();
      for (int j= 0; j < cond.length; j++) {
        if (cond[j].type() == typesCond[i]) {
          supports= cond[j].supports();
          for (int k= 0; k < supports.size(); k++)
            noeudToCond[i].put(supports.get(k), cond[j]);
        }
      }
    }
    noeudToSoll= new Hashtable[typesSoll.length];
    for (int i= 0; i < typesSoll.length; i++) {
      noeudToSoll[i]= new Hashtable();
      for (int j= 0; j < soll.length; j++) {
        if (soll[j].type() == typesSoll[i]) {
          supports= soll[j].supports();
          for (int k= 0; k < supports.size(); k++)
            noeudToSoll[i].put(supports.get(k), soll[j]);
        }
      }
    }
    // Recherche du niveau d'�quivalence des conditions de noeuds
    // s�lectionn�s
    impose= cLImposeDefaut;
    stationnaire= cLStatDefaut;
    valeur= cLValeurDefaut;
    courbe= null;
    cLImpose= new int[typesCond.length];
    cLStat= new int[typesCond.length];
    cLValeur= new Double[typesCond.length];
    cLCourbe= new IEvolution[typesCond.length];
    for (int j= 0; j < typesCond.length; j++) {
      // Pour le cas ou rien n'est s�lectionn�
      cLImpose[j]= impose;
      cLStat[j]= stationnaire;
      cLValeur[j]= valeur;
      cLCourbe[j]= courbe;
      for (int i= 0; i < _selects.length; i++) {
        cLCourante= (PRConditionLimite)noeudToCond[j].get(_selects[i]);
        if (cLCourante == null) {
          impose= BuCheckBox3States.STATE_DESELECTED;
        } else {
          impose= BuCheckBox3States.STATE_SELECTED;
          if (cLCourante.evolution() instanceof IEvolutionConstante) {
            stationnaire= BuCheckBox3States.STATE_DESELECTED;
            valeur=
              new Double(
                ((IEvolutionConstante)cLCourante.evolution()).constante());
          } else {
            stationnaire= BuCheckBox3States.STATE_SELECTED;
            courbe= cLCourante.evolution();
          }
        }
        // Premier �l�ment
        if (i == 0) {
          cLImpose[j]= impose;
          cLStat[j]= stationnaire;
          cLValeur[j]= valeur;
          cLCourbe[j]= courbe;
        }
        // Les autres
        else {
          if (cLImpose[j] != impose) {
            cLImpose[j]= BuCheckBox3States.STATE_MIXED;
          } else if (cLImpose[j] == BuCheckBox3States.STATE_SELECTED) {
            if (cLStat[j] != stationnaire) {
              cLStat[j]= BuCheckBox3States.STATE_MIXED;
            } else if (cLStat[j] == BuCheckBox3States.STATE_SELECTED) {
              if (cLCourbe[j] != courbe) {
                cLCourbe[j]= null; // Etat mixte
              }
            } else {
              if (cLValeur[j] != null && !cLValeur[j].equals(valeur)) {
                cLValeur[j]= null; // Etat mixte
              }
            }
          }
        }
      }
    }
    // Recherche du niveau d'�quivalence des sollicitations de noeuds
    // s�lectionn�s
    impose= sOImposeDefaut;
    stationnaire= sOStatDefaut;
    valeur= sOValeurDefaut;
    courbe= null;
    sOImpose= new int[typesSoll.length];
    sOStat= new int[typesSoll.length];
    sOValeur= new Double[typesSoll.length];
    sOCourbe= new IEvolution[typesSoll.length];
    for (int j= 0; j < typesSoll.length; j++) {
      // Pour le cas ou rien n'est s�lectionn�
      sOImpose[j]= impose;
      sOStat[j]= stationnaire;
      sOValeur[j]= valeur;
      sOCourbe[j]= courbe;
      for (int i= 0; i < _selects.length; i++) {
        sOCourante= (PRSollicitation)noeudToSoll[j].get(_selects[i]);
        if (sOCourante == null) {
          impose= BuCheckBox3States.STATE_DESELECTED;
        } else {
          impose= BuCheckBox3States.STATE_SELECTED;
          if (sOCourante.evolution() instanceof IEvolutionConstante) {
            stationnaire= BuCheckBox3States.STATE_DESELECTED;
            valeur=
              new Double(
                ((IEvolutionConstante)sOCourante.evolution()).constante());
          } else {
            stationnaire= BuCheckBox3States.STATE_SELECTED;
            courbe= sOCourante.evolution();
          }
        }
        // Premier �l�ment
        if (i == 0) {
          sOImpose[j]= impose;
          sOStat[j]= stationnaire;
          sOValeur[j]= valeur;
          sOCourbe[j]= courbe;
        }
        // Les autres
        else {
          if (sOImpose[j] != impose) {
            sOImpose[j]= BuCheckBox3States.STATE_MIXED;
          } else if (sOImpose[j] == BuCheckBox3States.STATE_SELECTED) {
            if (sOStat[j] != stationnaire) {
              sOStat[j]= BuCheckBox3States.STATE_MIXED;
            } else if (sOStat[j] == BuCheckBox3States.STATE_SELECTED) {
              if (sOCourbe[j] != courbe) {
                sOCourbe[j]= null; // Etat mixte
              }
            } else {
              if (sOValeur[j] != null && !sOValeur[j].equals(valeur)) {
                sOValeur[j]= null; // Etat mixte
              }
            }
          }
        }
      }
    }
    // Initialisation de l'interface
    //    initInterface();
    // Mise � jour de l'interface avec les valeurs d�termin�es
    nbCar= 0;
    for (int i= 0; i < typesCond.length; i++) {
      cbImpose[i].setEnabled(true);
      cbImpose[nbCar].setState(cLImpose[i]);
      if (cLImpose[i] == BuCheckBox3States.STATE_SELECTED) {
        cbTransitoire[nbCar].setState(cLStat[i]);
        if (cLStat[i] == BuCheckBox3States.STATE_SELECTED)
          changeCourbe(coCourbe[nbCar], cLCourbe[i]);
        else if (cLStat[i] == BuCheckBox3States.STATE_DESELECTED)
          changeValeur(tfValeur[nbCar], cLValeur[i]);
      }
      nbCar++;
    }
    for (int i= 0; i < typesSoll.length; i++) {
      cbImpose[nbCar].setEnabled(true);
      cbImpose[nbCar].setState(sOImpose[i]);
      if (sOImpose[i] == BuCheckBox3States.STATE_SELECTED) {
        cbTransitoire[nbCar].setState(sOStat[i]);
        if (sOStat[i] == BuCheckBox3States.STATE_SELECTED)
          changeCourbe(coCourbe[nbCar], sOCourbe[i]);
        else if (sOStat[i] == BuCheckBox3States.STATE_DESELECTED)
          changeValeur(tfValeur[nbCar], sOValeur[i]);
      }
      nbCar++;
    }
    btApply_.setEnabled(true);
    btOk_.setEnabled(true);
  }
  /**
   * Mise a jour de la boite de dialogue avec aucune s�lection.
   */
  public void setNoSelection() {
/*    int nbCar=
      Definitions.getTypesConditions().length
        + Definitions.getTypesSollicitations().length;*/
    for (int i= 0; i < nbCar; i++) {
      cbImpose[i].setEnabled(false);
      cbImpose[i].setState(BuCheckBox3States.STATE_DESELECTED);
      cbTransitoire[i].setState(BuCheckBox3States.STATE_DESELECTED);
      changeValeur(tfValeur[i], new Double(0));
    }
    btApply_.setEnabled(false);
    btOk_.setEnabled(false);
  }
  // ---------------------------------------------------------------------------
  // Initialisation de l'interface
  // ---------------------------------------------------------------------------
  private void initInterface() {
    int[] typesCond;
    int[] typesSoll;
    String[] nomCar;
    JPanel pnValeur;
    typesCond= Definitions.getTypesConditions();
    typesSoll= Definitions.getTypesSollicitations();
    nomCar= new String[typesCond.length + typesSoll.length];
    nbCar= 0;
    for (int i= 0; i < typesCond.length; i++)
      nomCar[nbCar++]= Definitions.conditionToString(typesCond[i]);
    for (int i= 0; i < typesSoll.length; i++)
      nomCar[nbCar++]= Definitions.sollicitationToString(typesSoll[i]);
    pnAffichage_.removeAll();
    cbImpose= new BuCheckBox3States[nbCar];
    cbTransitoire= new BuCheckBox3States[nbCar];
    tfValeur= new BuTextField[nbCar];
    coCourbe= new JComboBox[nbCar];
    for (int i= 0; i < nbCar; i++) {
      // Cas sp�cial : La hauteur qui peut avoir une courbe en provenance de LIDO
      if (nomCar[i]
        .equals(Definitions.conditionToString(PRConditionLimite.HAUTEUR)))
        cbImpose[i]= new DnDCheckBox();
      else
        cbImpose[i]= new BuCheckBox3States();
      cbImpose[i].setText(nomCar[i]);
      cbImpose[i].setState(BuCheckBox3States.STATE_DESELECTED);
      cbImpose[i].addItemListener(new ItemListener() {
        public void itemStateChanged(ItemEvent _evt) {
          caracteristique_itemStateChanged(_evt);
        }
      });
      pnAffichage_.add(cbImpose[i]);
      cbTransitoire[i]= new BuCheckBox3States();
      cbTransitoire[i].setText("Transitoire");
      cbTransitoire[i].setState(BuCheckBox3States.STATE_DESELECTED);
      cbTransitoire[i].setEnabled(false);
      cbTransitoire[i].addItemListener(new ItemListener() {
        public void itemStateChanged(ItemEvent _evt) {
          cas_itemStateChanged(_evt);
        }
      });
      pnAffichage_.add(cbTransitoire[i]);
      tfValeur[i]= new BuTextField();
      tfValeur[i].setText("0.0");
      tfValeur[i].setPreferredSize(new Dimension(70, 19));
      tfValeur[i].setEnabled(false);
      coCourbe[i]= new JComboBox();
      coCourbe[i].setEnabled(false);
      coCourbe[i].addItemListener(new ItemListener() {
        public void itemStateChanged(ItemEvent _evt) {
          courbe_itemStateChanged(_evt);
        }
      });
      pnValeur= new JPanel();
      pnValeur.setLayout(new CardLayout());
      pnValeur.add(
        tfValeur[i],
        new Integer(BuCheckBox3States.STATE_DESELECTED).toString());
      pnValeur.add(
        coCourbe[i],
        new Integer(BuCheckBox3States.STATE_SELECTED).toString());
      pnAffichage_.add(pnValeur);
    }
    initListesCourbes();
    pack();
    setSize(getPreferredSize());
  }
  // ---------------------------------------------------------------------------
  // Initialisation des listes de courbe
  // ---------------------------------------------------------------------------
  private void initListesCourbes() {
    IEvolution[] courbes= projet_.evolutions();
    String[] nomCourbes;
    nomCourbes= new String[courbes.length];
    for (int i= 0; i < courbes.length; i++)
      nomCourbes[i]= projet_.getName(courbes[i]);
    //    if (nomCourbes.length==0) return;
    for (int i= 0; i < coCourbe.length; i++) {
      coCourbe[i].removeAllItems();
      coCourbe[i].addItem("<mixte>");
      for (int k= 0; k < nomCourbes.length; k++)
        coCourbe[i].addItem(nomCourbes[k]);
    }
  }
  // ---------------------------------------------------------------------------
  // Ajout d'une courbe aux listes de courbe
  // ---------------------------------------------------------------------------
  private void addCourbe(String _name) {
    for (int i= 0; i < coCourbe.length; i++) {
      coCourbe[i].addItem(_name);
    }
  }
  // ---------------------------------------------------------------------------
  // Changement d'�tat d'une caract�ristique (Libre/Impos�/Mixte)
  // ---------------------------------------------------------------------------
  private void changeCar(BuCheckBox3States _cbImpose, int _state) {
    int i;
    for (i= 0; i < nbCar; i++)
      if (_cbImpose == cbImpose[i])
        break;
    switch (_state) {
      // Cas Impos�
      case BuCheckBox3States.STATE_SELECTED :
        setEnabledCas(cbTransitoire[i], true);
        break;
        // Cas libre/mixte => Pas de modification possible de la propri�t�
      case BuCheckBox3States.STATE_DESELECTED :
      case BuCheckBox3States.STATE_MIXED :
        setEnabledCas(cbTransitoire[i], false);
        break;
    }
  }
  // ---------------------------------------------------------------------------
  // Changement d'�tat du cas (Stationnaire/Transitoire/Mixte)
  // ---------------------------------------------------------------------------
  private void changeCas(BuCheckBox3States _cbTransitoire, int _state) {
    // Pas de courbes, pas de changement d'�tat
    if (_state == BuCheckBox3States.STATE_SELECTED
      && projet_.evolutions().length == 0) {
      _cbTransitoire.setState(BuCheckBox3States.STATE_DESELECTED);
      System.out.println(
        "Erreur : Pas de courbes dans la base de donnees. "
          + "Impossible de definir une caract�ristique "
          + "transitoire");
      return;
    }
    int i;
    for (i= 0; i < nbCar; i++)
      if (_cbTransitoire == cbTransitoire[i])
        break;
    switch (_state) {
      // Cas transitoire/ Cas non stationnaire
      case BuCheckBox3States.STATE_SELECTED :
      case BuCheckBox3States.STATE_DESELECTED :
        JPanel pn= (JPanel)coCourbe[i].getParent();
        CardLayout ly= (CardLayout)pn.getLayout();
        ly.show(pn, new Integer(_state).toString());
        tfValeur[i].setEnabled(true);
        coCourbe[i].setEnabled(true);
        break;
        // Cas mixte => On ne change rien
      case BuCheckBox3States.STATE_MIXED :
        tfValeur[i].setEnabled(false);
        coCourbe[i].setEnabled(false);
        break;
    }
  }
  // ---------------------------------------------------------------------------
  // Changement de la valeur d'une caract�ristique
  // ---------------------------------------------------------------------------
  private void changeValeur(JTextField _tfValeur, Double _valeur) {
    // Valeur donn�e
    if (_valeur != null)
      _tfValeur.setText(_valeur.toString());
    // Valeur nulle => Cas mixte : Texte vide
    else
      _tfValeur.setText("");
  }
  // ---------------------------------------------------------------------------
  // Changement de la courbe associ�e � une caract�ristique
  // ---------------------------------------------------------------------------
  private void changeCourbe(JComboBox _coCourbe, IEvolution _courbe) {
    // Courbe donn�e
    if (_courbe != null)
      _coCourbe.setSelectedItem(projet_.getName(_courbe));
    // Valeur nulle => Cas mixte
    else
      _coCourbe.setSelectedItem("<mixte>");
  }
  // ---------------------------------------------------------------------------
  // Autorisation de changement d'�tat de cas
  // ---------------------------------------------------------------------------
  private void setEnabledCas(
    BuCheckBox3States _cbTransitoire,
    boolean _select) {
    int i;
    for (i= 0; i < nbCar; i++)
      if (_cbTransitoire == cbTransitoire[i])
        break;
    cbTransitoire[i].setEnabled(_select);
    tfValeur[i].setEnabled(_select);
    coCourbe[i].setEnabled(_select);
    if (_select
      && _cbTransitoire.getState() == BuCheckBox3States.STATE_MIXED) {
      tfValeur[i].setEnabled(false);
      coCourbe[i].setEnabled(false);
    }
  }
  /**
   * Bouton "Ok" press�.
   */
  protected void btOkActionPerformed(ActionEvent _evt) {
    if (modCourbe)
      projet_.modCourbes= true;
    super.btOkActionPerformed(_evt);
  }
  /**
   * Bouton "Appliquer" press�.
   */
  protected void btApplyActionPerformed(ActionEvent _evt) {
    if (modCourbe)
      projet_.modCourbes= true;
    super.btApplyActionPerformed(_evt);
  }
  // ---------------------------------------------------------------------------
  // -------  Events interfaces  -----------------------------------------------
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  // Changement d'�tat de caract�ristique
  // ---------------------------------------------------------------------------
  void caracteristique_itemStateChanged(ItemEvent _evt) {
    BuCheckBox3States cb= (BuCheckBox3States)_evt.getSource();
    changeCar(cb, cb.getState());
  }
  // ---------------------------------------------------------------------------
  // Changement d'�tat Stationnaire/Transitoire
  // ---------------------------------------------------------------------------
  void cas_itemStateChanged(ItemEvent _evt) {
    BuCheckBox3States cb= (BuCheckBox3States)_evt.getSource();
    changeCas(cb, cb.getState());
  }
  // ---------------------------------------------------------------------------
  // Changement de courbe
  // ---------------------------------------------------------------------------
  void courbe_itemStateChanged(ItemEvent _evt) {
    JComboBox co= (JComboBox)_evt.getSource();
    if (co.getSelectedItem() == _evt.getItem()) {}
  }
  /**
   * Retourne les conditions limites supprim�es.
   * @return les conditions limites supprim�es. Si aucune condition n'a
   *         �t� supprim�e, le tableau en retour est vide
   */
  public PRConditionLimite[] getConditionsSupprimees() {
    int[] typesCL= Definitions.getTypesConditions();
    PRConditionLimite[] cLs= new PRConditionLimite[typesCL.length];
    PRConditionLimite[] r;
    int nbCL= 0;
    for (int i= 0; i < typesCL.length; i++) {
      if (cbImpose[i].getState() != BuCheckBox3States.STATE_DESELECTED)
        continue;
      // Cr�ation de la condition
      cLs[nbCL]= new PRConditionLimite(typesCL[i], null, new Vector());
      nbCL++;
    }
    r= new PRConditionLimite[nbCL];
    System.arraycopy(cLs, 0, r, 0, nbCL);
    return r;
  }
  /**
   * Retourne les conditions limites affect�es.
   * @return les conditions limites affect�es. Si aucune condition n'a
   *         �t� affect�e, le tableau en retour est vide
   */
  public PRConditionLimite[] getConditions() {
    int[] typesCond;
    IEvolution[] courbes= projet_.evolutions();
    Vector vctCL= new Vector();
    IEvolution evolution;
    typesCond= Definitions.getTypesConditions();
    for (int i= 0; i < typesCond.length; i++) {
      // On passe cette propri�t� si elle n'est pas s�lectionn�e ou mixte
      if (cbImpose[i].getState() != BuCheckBox3States.STATE_SELECTED)
        continue;
      if (cbTransitoire[i].getState() == BuCheckBox3States.STATE_MIXED)
        continue;
      if (cbTransitoire[i].getState() == BuCheckBox3States.STATE_DESELECTED
        && tfValeur[i].getText().equals(""))
        continue;
      if (cbTransitoire[i].getState() == BuCheckBox3States.STATE_SELECTED
        && coCourbe[i].getSelectedIndex() == 0)
        continue;
      // Cr�ation de la condition
      if (cbTransitoire[i].getState() == BuCheckBox3States.STATE_SELECTED) {
        evolution= courbes[coCourbe[i].getSelectedIndex() - 1];
      } else {
        evolution=UsineLib.findUsine().creeMesureEvolutionConstante();
/*          IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
        double constante= new Double(tfValeur[i].getText()).doubleValue();
        ((IEvolutionConstante)evolution).constante(constante);
      }
      vctCL.addElement(
        new PRConditionLimite(typesCond[i], evolution, selects_));
    }
    PRConditionLimite[] tabCL= new PRConditionLimite[vctCL.size()];
    vctCL.copyInto(tabCL);
    return tabCL;
  }
  /**
   * Retourne les sollicitations supprim�es.
   * @return les sollicitations supprim�es. Si aucune sollicitation n'a
   *         �t� supprim�e, le tableau en retour est vide
   */
  public PRSollicitation[] getSollicitationsSupprimees() {
    int[] typesSO= Definitions.getTypesSollicitations();
    PRSollicitation[] sOs= new PRSollicitation[typesSO.length];
    PRSollicitation[] r;
    int j= Definitions.getTypesConditions().length;
    int nbSO= 0;
    for (int i= 0; i < typesSO.length; i++) {
      if (cbImpose[j].getState() != BuCheckBox3States.STATE_DESELECTED)
        continue;
      // Cr�ation de la condition
      sOs[nbSO]= new PRSollicitation(typesSO[i], null, new Vector());
      nbSO++;
    }
    r= new PRSollicitation[nbSO];
    System.arraycopy(sOs, 0, r, 0, nbSO);
    return r;
  }
  /**
   * Retourne les sollicitations affect�es.
   * @return les sollicitations affect�es. Si aucune sollicitation n'a
   *         �t� affect�e, le tableau en retour est vide
   */
  public PRSollicitation[] getSollicitations() {
    int[] typesSoll;
    IEvolution[] courbes= projet_.evolutions();
    Vector vctSO= new Vector();
    IEvolution evolution;
    typesSoll= Definitions.getTypesSollicitations();
    int j= Definitions.getTypesConditions().length;
    for (int i= 0; i < typesSoll.length; i++) {
      // On passe cette propri�t� si elle n'est pas s�lectionn�e ou mixte
      if (cbImpose[j].getState() != BuCheckBox3States.STATE_SELECTED)
        continue;
      if (cbTransitoire[j].getState() == BuCheckBox3States.STATE_MIXED)
        continue;
      if (cbTransitoire[j].getState() == BuCheckBox3States.STATE_DESELECTED
        && tfValeur[j].getText().equals(""))
        continue;
      if (cbTransitoire[j].getState() == BuCheckBox3States.STATE_SELECTED
        && coCourbe[j].getSelectedIndex() == 0)
        continue;
      // Cr�ation de la condition
      if (cbTransitoire[j].getState() == BuCheckBox3States.STATE_SELECTED) {
        evolution= courbes[coCourbe[j].getSelectedIndex() - 1];
      } else {
        evolution=UsineLib.findUsine().creeMesureEvolutionConstante();
/*          IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
        double constante= new Double(tfValeur[j].getText()).doubleValue();
        ((IEvolutionConstante)evolution).constante(constante);
      }
      vctSO.addElement(new PRSollicitation(typesSoll[i], evolution, selects_));
      j++;
    }
    PRSollicitation[] tabSO= new PRSollicitation[vctSO.size()];
    vctSO.copyInto(tabSO);
    return tabSO;
  }
  /**
   * Traitement du Drop sur un checkBox ou un TextField ou un ComboBox.
   */
  public void dndDrop(DropTargetDropEvent _dtde) {
    try {
      System.err.println("dndDrop");
      Transferable transferable= _dtde.getTransferable();
      // Seuls les profils 1D2D sont accept�s
      DataFlavor dfP1D2D=
        new DataFlavor(
          new Profil1D2D().getClass(),
          DataFlavor.javaJVMLocalObjectMimeType);
      if (transferable.isDataFlavorSupported(dfP1D2D)) {
        _dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
        Profil1D2D p1D2D= (Profil1D2D)transferable.getTransferData(dfP1D2D);
        IEvolution courbe= prf2Crb(p1D2D);
        if (!(courbe instanceof IEvolutionConstante)) {
          EXIST : {
            // Si la courbe existe d�j�
            IEvolution[] crbs= projet_.evolutions();
            String nmCrb= projet_.getName(courbe);
            for (int i= 0; i < crbs.length; i++) {
              if (projet_.getName(crbs[i]).equals(nmCrb)) {
                courbe= crbs[i];
                break EXIST;
              }
            }
            IEvolution[] crbsNew= new IEvolution[crbs.length + 1];
            for (int i= 0; i < crbs.length; i++)
              crbsNew[i]= crbs[i];
            crbsNew[crbs.length]= courbe;
            projet_.evolutions(crbsNew);
            modCourbe= true;
            addCourbe(projet_.getName(courbe));
          }
        }
        // Mise a jour de l'interface
        BuCheckBox3States cb=
          (BuCheckBox3States) ((DropTarget)_dtde.getSource()).getComponent();
        for (int i= 0; i < nbCar; i++) {
          if (cb == cbImpose[i]) {
            cb.setState(BuCheckBox3States.STATE_SELECTED);
            if (courbe instanceof IEvolutionConstante) {
              cbTransitoire[i].setState(BuCheckBox3States.STATE_DESELECTED);
              changeValeur(
                tfValeur[i],
                new Double(((IEvolutionConstante)courbe).constante()));
            } else {
              cbTransitoire[i].setState(BuCheckBox3States.STATE_SELECTED);
              changeCourbe(coCourbe[i], courbe);
            }
            break;
          }
        }
        _dtde.getDropTargetContext().dropComplete(true);
      } else {
        _dtde.rejectDrop();
      }
    } catch (IOException _exc) {
      //      exception.printStackTrace();
      System.err.println("Exception" + _exc.getMessage());
      _dtde.rejectDrop();
    } catch (UnsupportedFlavorException _exc) {
      //      ufException.printStackTrace();
      System.err.println("Exception" + _exc.getMessage());
      _dtde.rejectDrop();
    }
  }
  // Cr�ation d'une courbe depuis le profil r�cup�r�
  private IEvolution prf2Crb(Profil1D2D _profil) {
    double[][] pts= _profil.getLimnigramme();
    String name= "Lido - " + _profil.getName();
    long[] ptsX= new long[pts.length];
    for (int i= 0; i < pts.length; i++)
      ptsX[i]= (long)pts[i][0];
    double[] ptsY= new double[pts.length];
    for (int i= 0; i < pts.length; i++)
      ptsY[i]= pts[i][1];
    if (_profil.isRegimePermanent()) {
      IEvolutionConstante courbe=UsineLib.findUsine().creeMesureEvolutionConstante();
/*        IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
      courbe.constante(ptsY[0]);
      return courbe;
    }
    IEvolutionSerieIrreguliere courbe=UsineLib.findUsine().creeMesureEvolutionSerieIrreguliere();
    /*        IEvolutionSerieIrreguliereHelper.narrow(
     new DEvolutionSerieIrreguliere().tie());*/
    courbe.instants().modifieInstants(ptsX);
    courbe.serie(ptsY);
    projet_.associe(courbe, name);
    return courbe;
  }
  // Une classe CheckBox g�rant le Drag&Drop
  class DnDCheckBox extends BuCheckBox3States implements DropTargetListener {
    DropTarget dropTarget_=
      new DropTarget(this, DnDConstants.ACTION_COPY_OR_MOVE, this);
    public void dragEnter(DropTargetDragEvent _dtde) {
      if (this.isEnabled())
        _dtde.acceptDrag(DnDConstants.ACTION_COPY_OR_MOVE);
      else
        _dtde.rejectDrag();
    }
    public void dragExit(DropTargetEvent _dte) {}
    public void dragOver(DropTargetDragEvent _dtde) {}
    public void drop(DropTargetDropEvent _dtde) {
      dndDrop(_dtde);
    }
    public void dropActionChanged(DropTargetDragEvent _dtde) {}
  }
}
