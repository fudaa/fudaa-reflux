/*
 * @file         PRPnPreferences.java
 * @creation     2001-03-07
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLib;
/**
 * Un panneau de preferences pour Reflux.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRPnPreferences
  extends BuAbstractPreferencesPanel
  implements ActionListener {
  PRPreferences options_;
  BuCommonImplementation imp_;
  JTextField tfTaille;
  public String getTitle() {
    return "Divers";
  }
  // Constructeur
  public PRPnPreferences(BuCommonImplementation _imp) {
    super();
    options_= PRPreferences.REFLUX;
    imp_= _imp;
    BuGridLayout ly= new BuGridLayout(2, 5, 5, false, false);
    JPanel p= new JPanel();
    p.setBorder(new TitledBorder("Divers"));
    p.setLayout(ly);
    p.add(new JLabel("Taille des normales (en pixels):"));
    tfTaille= new JTextField();
    tfTaille.setPreferredSize(new Dimension(70, 21));
    p.add(tfTaille);
    setLayout(new BorderLayout());
    setBorder(new EmptyBorder(5, 5, 5, 5));
    add("Center", p);
    BuLib.computeMnemonics(this);
    updateComponents();
  }
  // Evenements
  public void actionPerformed(ActionEvent _evt) {
    JComponent source= (JComponent)_evt.getSource();
    System.err.println("SOURCE=" + source.getName());
    System.err.println("ACTION=" + _evt.getActionCommand());
    // chBrowser.setEnabled(cbExterne.isSelected());
  }
  // Methodes publiques
  public boolean isPreferencesValidable() {
    return true;
  }
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
  }
  public boolean isPreferencesApplyable() {
    return true;
  }
  public void applyPreferences() {
    fillTable();
    options_.applyOn(imp_);
  }
  public boolean isPreferencesCancelable() {
    return true;
  }
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }
  // Methodes privees
  private void fillTable() {
    options_.putStringProperty(
      "fudaa_reflux.taille_normales",
      tfTaille.getText());
  }
  private void updateComponents() {
    tfTaille.setText(
      options_.getStringProperty("fudaa_reflux.taille_normales"));
  }
}
