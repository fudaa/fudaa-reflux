/*
 * @file         RefluxResource.java
 * @creation     1999-01-11
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import com.memoire.bu.BuResource;
/**
 * Une classes de gestion des ressources Reflux.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxResource extends BuResource {
  public final static RefluxResource REFLUX= new RefluxResource();
  /**
   * Type de projet. Le type de projet sert � d�finir les sp�cificit�s
   * interfaces et les sp�cificit�s base de donn�es.
   *
   * @see PRProjet#VIT_NU_CON
   * @see PRProjet#VIT_NU_LMG
   * @see PRProjet#VIT_3D
   */
  public static int typeProjet= PRProjet.VIT_NU_CON;
}
