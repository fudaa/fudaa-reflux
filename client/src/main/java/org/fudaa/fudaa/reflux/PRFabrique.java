/*
 * @file         PRFabrique.java
 * @creation     2001-01-08
 * @modification $Date: 2005-08-16 14:00:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
//import org.fudaa.fudaa.reflux.d2.*;
//import org.fudaa.fudaa.reflux.d3.*;
/**
 * Une fabrique g�n�rale.
 *
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 14:00:33 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public abstract class PRFabrique {
  /**
   * Retourne la fabrique pour le type de calcul.
   *
   * On ne passe pas par une instanciation classique par "new" des differentes
   * fabriques pour des problemes de compilation crois�e entre paquetages.
   */
  public static PRFabrique getFabrique() {
    String nm;
    Class cl;
    PRFabrique fa= null;
    switch (RefluxResource.typeProjet) {
      default :
      case PRProjet.VIT_NU_CON :
      case PRProjet.VIT_NU_LMG :
        nm= "org.fudaa.fudaa.reflux.d2.HY2dFabrique";
        break;
      case PRProjet.VIT_3D :
        nm= "org.fudaa.fudaa.reflux.d3.HY3dFabrique";
        break;
      case PRProjet.TRANS_SUS_2D :
      case PRProjet.TRANS_TOT_2D :
        nm= "org.fudaa.fudaa.reflux.d2transport.TT2dFabrique";
        break;
    }
    // Instanciation de la classe fabrique.
    try {
      cl= Class.forName(nm);
      fa= (PRFabrique)cl.newInstance();
    } catch (ClassNotFoundException _exc) {
      _exc.printStackTrace();
    } catch (IllegalAccessException _exc) {
      _exc.printStackTrace();
    } catch (InstantiationException _exc) {
      _exc.printStackTrace();
    }
    return fa;
  }
  /**
   * Cr�ation d'un projet.
   */
  public abstract PRProjet creeProjet();
  /**
   * Cr�ation d'un panel de parametres de calcul.
   */
  public abstract PRPnParamsCalcul creePnParamsCalcul();
  /**
   * Cr�ation d'un panel de propri�t�s physiques globales.
   */
  public abstract PRPnPropsGlobales creePnPropsGlobales();
  /**
   * Cr�ation d'un composant de saisie de valeur d'une propri�t� �l�mentaire.
   *
   * @param _tpPrp Le type de propri�t�.
   */
  public abstract PRPnSaisieValeurPE creeCpSaisieValeurPE(int _tpPrp);
  /**
   * Cr�ation d'un panneau de param�tres de nouveau projet.
   */
  public PRPnParamsNouveauProjet creePnParamsNouveauProjet() {
    return new PRPnParamsNouveauProjetDefaut();
  }
}