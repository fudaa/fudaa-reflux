/*
 * @file         RefluxPaletteCouleur.java
 * @creation     1999-01-11
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Color;
/**
 * Une palette de couleur simple pour les isosurfaces.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class RefluxPaletteCouleur {
  double[] niveaux_;
  Color[] couleurs_;
  public RefluxPaletteCouleur() {
    niveaux_= new double[] { 0.0001, 1.e10 };
    couleurs_= new Color[] { new Color(0, 150, 255), Color.black };
  }
  public double[] getNiveaux() {
    return niveaux_;
  }
  public Color[] getCouleurs() {
    return couleurs_;
  }
}