/*
 * @file         RefluxApplication.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import com.memoire.bu.BuApplication;
/**
 * Application Reflux (parall�le � Applet).
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class RefluxApplication extends BuApplication {
  public RefluxApplication() {
    super();
    setImplementation(new RefluxImplementation());
  }
}
