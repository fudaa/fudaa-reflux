/*
 * @file         PRPnMethode.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.util.Hashtable;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuGridLayout;
/**
 * Un panneau pour la saisie de la m�thode et de ses coefficients. Les m�thodes
 * propos�es dans le panneau d�pendent du sch�ma.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRPnMethode extends JPanel {
  private JComboBox coMethode_= new JComboBox();
  private JPanel pnCoefsMethode_= new JPanel();
  private CardLayout lyCoefsMethode_= new CardLayout();
  /**
   * Pour retrouver le panel de coefficients � partir de son type de m�thode.
   */
  private Hashtable hTp2PnCoefs_;
  /**
   * Cr�ation du panneau de saisie de m�thode.
   */
  public PRPnMethode() {
    super();
    try {
      jbInit();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  /**
   * D�finition de l'interface graphique.
   */
  private void jbInit() throws Exception {
    JLabel laMethode= new JLabel();
    EtchedBorder bdThis= new EtchedBorder();
    BorderLayout lyThis= new BorderLayout();
    JPanel pnMethode= new JPanel();
    FlowLayout lyMethode= new FlowLayout();
    int[] tpsMethode= Definitions.getTypesMethode();
    hTp2PnCoefs_= new Hashtable(tpsMethode.length);
    laMethode.setText("M�thode");
    coMethode_.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        coMethode__itemStateChanged(e);
      }
    });
    lyCoefsMethode_.setHgap(3);
    lyCoefsMethode_.setVgap(3);
    pnCoefsMethode_.setLayout(lyCoefsMethode_);
    // Panneaux de coefficients. On construit les panneaux de toutes les
    // m�thodes possibles pour que la m�thode panneau.getPreferredSize()
    // retourne d'embl�e la taille maxi pour tous les composants.
    for (int i= 0; i < tpsMethode.length; i++) {
      JPanel pnCoefs= new JPanel();
      String[] nmsCoefs= Definitions.getNomsCoefsMethode(tpsMethode[i]);
      double[] vlsCoefs= Definitions.getDefaultCoefsMethode(tpsMethode[i]);
      pnCoefs.setLayout(new BuGridLayout(2, 3, 3));
      for (int j= 0; j < nmsCoefs.length; j++) {
        JLabel lb;
        JTextField tf;
        lb= new JLabel(nmsCoefs[j]);
        tf= new JTextField("" + vlsCoefs[j]);
        tf.setPreferredSize(new Dimension(100, 21));
        pnCoefs.add(lb);
        pnCoefs.add(tf);
      }
      pnCoefsMethode_.add(pnCoefs, Definitions.methodeToString(tpsMethode[i]));
      hTp2PnCoefs_.put(new Integer(tpsMethode[i]), pnCoefs);
    }
    pnMethode.setLayout(lyMethode);
    pnMethode.add(laMethode, null);
    pnMethode.add(coMethode_, null);
    this.setLayout(lyThis);
    this.setBorder(bdThis);
    this.add(pnMethode, BorderLayout.NORTH);
    this.add(pnCoefsMethode_, BorderLayout.CENTER);
  }
  /**
   * D�finition du type de sch�ma. L'appel � cette m�thode d�finit les m�thodes
   * propos�es dans le panneau et r�initialise tout le panneau (y compris les
   * coefficients des m�thodes).
   *
   * @param _tpSchema Type de sch�ma.
   */
  public void setTypeSchema(int _tpSchema) {
    // Initialisation des param�tres des panneaux.
    setDefaultCoefficients();
    int[] tpsMethode= Definitions.getTypesMethode(_tpSchema);
    coMethode_.removeAllItems();
    for (int i= 0; i < tpsMethode.length; i++) {
      coMethode_.addItem(Definitions.methodeToString(tpsMethode[i]));
    }
  }
  /**
   * Initialisation avec la m�thode. Le type de la m�thode est suppos� �tre
   * autoris� pour le type de sch�ma.
   *
   * @param _methode La m�thode de r�solution.
   */
  public void setMethode(PRMethodeResolution _methode) {
    JPanel pnCoefs= (JPanel)hTp2PnCoefs_.get(new Integer(_methode.type()));
    double[] coefs= _methode.coefficients();
    for (int i= 0; i < coefs.length; i++)
       ((JTextField)pnCoefs.getComponent(i * 2 + 1)).setText("" + coefs[i]);
    // Selection de la m�thode
    coMethode_.setSelectedItem(Definitions.methodeToString(_methode.type()));
  }
  /**
   * Retourne la m�thode.
   * @return La m�thode
   */
  PRMethodeResolution getMethode() {
    PRMethodeResolution r;
    int tpMethode=
      Definitions.stringToMethode((String)coMethode_.getSelectedItem());
    double[] coefs= Definitions.getDefaultCoefsMethode(tpMethode);
    JPanel pnCoefs= (JPanel)hTp2PnCoefs_.get(new Integer(tpMethode));
    for (int i= 0; i < coefs.length; i++) {
      try {
        coefs[i]=
          Double.parseDouble(
            ((JTextField)pnCoefs.getComponent(i * 2 + 1)).getText());
      } catch (NumberFormatException _exc) {}
    }
    r= new PRMethodeResolution(tpMethode, coefs);
    return r;
  }
  /**
   * Initialise les coefficients des diff�rentes m�thodes du panneau avec leurs
   * valeurs par defaut.
   */
  private void setDefaultCoefficients() {
    int[] tpsMethode= Definitions.getTypesMethode();
    for (int i= 0; i < tpsMethode.length; i++) {
      JPanel pnCoefs= (JPanel)hTp2PnCoefs_.get(new Integer(tpsMethode[i]));
      double[] coefs= Definitions.getDefaultCoefsMethode(tpsMethode[i]);
      for (int j= 0; j < coefs.length; j++)
         ((JTextField)pnCoefs.getComponent(j * 2 + 1)).setText("" + coefs[j]);
    }
  }
  /**
   * M�thode appel�e lors d'un �venement ItemEvent sur coMethode_.
   */
  void coMethode__itemStateChanged(ItemEvent _evt) {
    lyCoefsMethode_.show(pnCoefsMethode_, (String)_evt.getItem());
  }
}
