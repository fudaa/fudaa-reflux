/*
 * @file         PRFilleCalques.java
 * @creation     1998-06-17b
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JMenu;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuPopupButton;
import com.memoire.bu.BuToggleButton;

import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.dessin.DeTrait;
import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.geometrie.GrSymbole;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrSymbole;
import org.fudaa.ebli.trace.TraceSurface;

/**
 * Une classe affichant les calques et permettant l'int�ration avec
 * l'utilisateur.
 *
 * @version      $Id: PRFilleCalques.java,v 1.13 2006-09-19 15:11:53 deniger Exp $
 * @author       Bertrand Marchand
 */
public class PRFilleCalques extends EbliFilleCalques {
  BuButton btCaracFond;
  BuButton btModifNormale;
//  BuButton btModifPoint;
//  BuButton btInitMares;
  BuButton btSupMare;
  BuButton btNaturesBord;
  BuButton btCaracBord;
  BuPopupButton btInfo;
  BuButton btGroupe;
  BuButton btSupGroupe;
  BuToggleButton btSegment;
  BuButton btSupSegment;
  BuButton btPlan;
  BuButton btSupPlan;
  BuButton btPropPlan;
  BuButton btPropSegment;
  BuButton btCalSolInit;
//  BuButton btCalHInter;
  BuMenu mnVoir;
  RefluxCalqueElement cqElement= new RefluxCalqueElement();
//  RefluxCalquePtSI cqPtSI= new RefluxCalquePtSI();
  RefluxCalqueSI cqSI= new RefluxCalqueSI();
  RefluxCalqueBord cqBord= new RefluxCalqueBord();
  RefluxCalqueNoeud cqNoeud= new RefluxCalqueNoeud();
  PRCalqueNormale cqNormale= new PRCalqueNormale();
  RefluxCalqueGroupe cqGrp_=new RefluxCalqueGroupe();
  BCalqueFormeInteraction cqFormeI_;
  BCalquePolyligne cqSegment_;
  BCalquePolygone cqPlan_;
  private BGroupeCalque rootCalque_= new BGroupeCalque();
  private BGroupeCalque gcSI_= new BGroupeCalque();
  private boolean siOK_= true;
  PRDialogNormale diNormale;
  PRDialogPE diPE;
  PRDialogCLSO diCLSO;
//  RefluxDialogPtSI diPointSI;
  RefluxDialogPA diPA;
  private HashSet enabledActions_= new HashSet();
  //  private Frame parent_;
  private BuCommonInterface app_;
  private Object[] iSelects_= new Object[0]; // Objets s�lectionn�s
  private BCalque cqActif_; // Calque actif au moment de la s�lection
  private VecteurGrContour objets_;
  private PRProjet projet_;
  Hashtable grToI; // Association Gr*** -> I***
  Hashtable bordToNature; // Association bord -> PRNature
  Hashtable el2Nature; // Association element -> PRNature
  //BArbreCalque ac_;
  /** Panneau d'information sur l'objet s�lectionn�. */
  private BPanneauInformation pnInfo_;

  public PRFilleCalques(BuCommonInterface _app /*, BVueCalque _vc*/
  , BArbreCalque _arbre) {
    //super(_vc);
    addInternalFrameListener(_arbre);
    //    parent_=_parent;
    app_= _app;
    //ac_=_arbre;
    grToI= new Hashtable();
    bordToNature= new Hashtable();
    el2Nature= new Hashtable();
    /**JBuilder    jbInit();
      }

      private void jbInit() throws Exception {  JBuilder**/
    //    ButtonGroup   bGroup;
    rootCalque_.setName("Calques");
    this.setClosable(false);
    this.setTitle("Fen�tre principale");
    cqElement.setName("El�ments");
    cqNoeud.setName("Noeuds");
    cqNormale.setName("Normales");
    cqNormale.setVisible(false);
    cqGrp_.setName("Groupes d'�l�ments");
    // Lignes plans pour solutions initiales
    cqSegment_= new BCalquePolyligne();
    cqSegment_.setForeground(new Color(255, 0, 0));
//    cqSegment_.setTypeTrait(TraceLigne.UNIFORME);
    cqSegment_.setTitle("Lignes plans");
    cqSegment_.setName("cqSEGMENT");
    // Calque dessin interaction
    cqFormeI_= new BCalqueFormeInteraction(cqSegment_);
    cqFormeI_.setName("cqDESSIN-I");
    cqFormeI_.setTitle("D-Interaction");
    cqFormeI_.setGele(true);
    cqFormeI_.setTypeForme(DeForme.TRAIT);
    cqFormeI_.addFormeEventListener(new FormeEventListener() {
      public void formeSaisie(FormeEvent _evt) {
        cqFormeI_formeSaisie(_evt);
      }
    });
    cqPlan_= new BCalquePolygone();
    cqPlan_.setForeground(new Color(200, 0, 0));
    cqPlan_.setBackground(new Color(200,0,0,75));
    cqPlan_.setTypeSurface(TraceSurface.UNIFORME);
    cqPlan_.setTitle("Multiplans");
    cqPlan_.setName("cqPLAN");
//    cqPoint_.setTitle("points");
//    cqPoint_.setName("cqPOINT");
//    ZModeleStatiquePoint mod=new ZModeleStatiquePoint(new GrPoint[]{new GrPoint()});
//    cqPoint_.modele(mod);
//    //le calque berge
//    calqueBerge = new ZCalqueLongPolyligne();
//    calqueBerge.setName("cqBerge");
//    calqueBerge.setForeground(Color.GREEN);
//    rootCalque_.add(calqueBerge);
//    //le modele pour les berges
//    mdBerges = new ZModeleMultiPointSelection();
//    //les points supports pour la construction de la berge
//    mdBerges.setTarget(cqPoint_.modele());
//    calqueBerge.modele(mdBerges);
//    //le calque d'interaction pour les berges
//    ZCalquePolySelectionInteraction cqBergeI = new ZCalquePolySelectionInteraction();
//    cqBergeI.setName("cqIBuildPoly");
//    cqBergeI.setModel(mdBerges, calqueBerge);
//    cqBergeI.setGele(true);
//    rootCalque_.add(cqBergeI);
//    rootCalque_.add(cqPoint_);
    cqBord.setName("Bords");

//    rootCalque_.add(cqFormeI_);
    gcSI_.setName("Solutions initiales");
    gcSI_.setVisible(false);
    gcSI_.add(cqPlan_);
    gcSI_.add(cqSegment_);
    gcSI_.add(cqGrp_);
    gcSI_.add(cqSI);
    gcSI_.add(cqFormeI_);
//    gcSI_.add(cqPtSI);
    rootCalque_.add(cqElement);
    rootCalque_.add(cqNoeud);
    rootCalque_.add(cqNormale);
    rootCalque_.add(cqBord);
    cqSI.setName("Zones inond�es");
//    cqPtSI.setName("Points du plan");
    getVueCalque().setPreferredSize(new Dimension(550, 400));
    getVueCalque().setBackground(Color.white);
    buildTools();
    associeBoutonCalqueInteraction(cqFormeI_, btSegment);
    setCalque(rootCalque_);
    this.setResizable(true);
    this.setIconifiable(true);
    this.setMaximizable(true);
    setBoutonsStandardVisible(false);
    this.getContentPane().add(getVueCalque());
    this.pack();
    //--- Menus sp�cifiques
    mnVoir= (BuMenu)buildMenuVoir();
  }
  public String[] getEnabledActions() {
//    return (String[])enabledActions_.toArray(new String[0]);
    return super.getEnabledActions();
  }
  public String[] getDisabledActions() {
    return super.getDisabledActions();
  }
  public JMenu[] getSpecificMenus() {
    JMenu[] mnPere= super.getSpecificMenus();
    JMenu[] r= new JMenu[mnPere.length + 1];
    for (int i= 0; i < mnPere.length; i++)
      r[i]= mnPere[i];
    r[mnPere.length + 0]= mnVoir;
    return r;
  }
  public JComponent[] getSpecificTools() {
    JComponent[] cpssuper= super.getSpecificTools();
    JComponent[] r= new JComponent[24];
    int n= 0;
    //    r[n++]=btSelectionPonctuelle;
    //    r[n++]=btSelectionRectangle;
    //    r[n++]=btSelectionPolygone;
    //    r[n++]=null;
    r[n++]= btCaracFond;
    r[n++]= null;
    //    r[n++]=btVisuNormales;
    r[n++]= btModifNormale;
    r[n++]= null;
//    r[n++]= btModifPoint;
//    r[n++]= btInitMares;
    r[n++]= btNaturesBord;
    r[n++]= btCaracBord;
    r[n++]= null;
    r[n++]= btInfo;
    if (getDesktopPane()!=null)
      btInfo.setDesktop((BuDesktop)getDesktopPane());
    r[n++]=null;
    r[n++]=btGroupe;
    r[n++]=btSupGroupe;
    r[n++]=btSegment;
    r[n++]=btSupSegment;
    r[n++]=btPropSegment;
    r[n++]=null;
    r[n++]=btPlan;
    r[n++]=btSupPlan;
    r[n++]=btPropPlan;
    r[n++]=null;
    r[n++]=btCalSolInit;
    r[n++]= btSupMare;
//    r[n++]=btCalHInter;
    JComponent[] cps= new JComponent[cpssuper.length + r.length];
    System.arraycopy(cpssuper, 0, cps, 0, cpssuper.length);
    System.arraycopy(r, 0, cps, cpssuper.length, r.length);
    return cps;
  }
  /**
   * M�thode pour g�rer �galement les actions des boutons de la barre sp�cifique,
   * la m�thode standard de Bu ne le faisant pas.
   */
  private void setEnabledForAction(String _cmd, boolean _etat) {
    app_.setEnabledForAction(_cmd, _etat);
    // Pour les boutons de la barre
    JComponent[] cps= getSpecificTools();
    for (int i= 0; i < cps.length; i++) {
      if (cps[i] instanceof AbstractButton) {
        AbstractButton bt= (AbstractButton)cps[i];
        if (_cmd.equals(bt.getActionCommand()))
          bt.setEnabled(_etat);
      }
    }
    if (_etat)
      enabledActions_.add(_cmd);
    else
      enabledActions_.remove(_cmd);
  }
  // Cr�ation du menu Voir
  private JMenu buildMenuVoir() {
    BuMenu r;
    r= new BuMenu("Voir", "mnVOIR");
    r.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });
    r.addCheckBox(
      "Num�ro des noeuds",
      "NUMNOEUDS",
      RefluxResource.REFLUX.getIcon("numnoeuds"),
      true,
      false);
    r.addCheckBox(
      "Num�ro des �l�ments",
      "NUMELEMENTS",
      RefluxResource.REFLUX.getIcon("numelements"),
      true,
      false);
    return r;
  }
  void this_actionPerformed(ActionEvent _evt) {
    String action= _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    if (action.equals("ROTATIONNORMALE"))
      cmdRotationNormale();
    else if (action.equals("NUMNOEUDS"))
      cmdVoirNumNoeuds(((AbstractButton)_evt.getSource()).isSelected());
    else if (action.equals("NUMELEMENTS"))
      cmdVoirNumElements(((AbstractButton)_evt.getSource()).isSelected());
    else if (action.equals("GROUPER"))
      cmdCreerGroupe();
    else if (action.equals("SUPGROUPE"))
      cmdSupprimerGroupe();
    else if (action.equals("CREERSEGMENT"))
      cmdCreerSegment();
    else if (action.equals("SUPSEGMENT"))
      cmdSupprimerSegment();
    else if (action.equals("PROPSEGMENT"))
      cmdProprietesSegment();
    else if (action.equals("CREERPLAN"))
      cmdCreerPlan();
    else if (action.equals("SUPPLAN"))
      cmdSupprimerPlan();
    else if (action.equals("PROPPLAN"))
      cmdProprietesPlan();
    else if (action.equals("CALSOLINI"))
      cmdCalculSolutionsInitiales();
//    else if (action.equals("CALHINTER"))
//      cmdCalculHIntermediaires();
    else
      System.out.println("Action inconnue");
  }

  /**
   * Calcul des h interm�diaires
   */
//  private void cmdCalculHIntermediaires() {
//    try {
//      ((RefluxGrPlan)iSelects_[0]).getPlan().calculHIntermediaires();
//      RefluxSegment[] sgs=((RefluxGrPlan)iSelects_[0]).getPlan().getSegments();
//      for (int i=0; i<sgs.length; i++) {
//        System.out.println("Sg "+sgs[i].getNum()+": h: "+sgs[i].getHauteur());
//      }
//    }
//    catch (Exception _exc) {
//      _exc.printStackTrace();
//    }
//  }

  /**
   * Calcul des solutions initiales � partir des multiplans d�finis.
   */
  public void cmdCalculSolutionsInitiales() {
    projet_.initialiserSolutionsInitiales();
    getVueCalque().repaint();
  }

  /**
   * Cr�ation d'une ligne plan. On vide juste la s�lection pour ne pas avoir
   * d'effet de bord.
   */
  private void cmdCreerSegment() {
    if (iSelects_ != null && iSelects_.length != 0)
      videSelection();
  }

  /**
   * Cr�ation d'une ligne plan. On vide juste la s�lection pour ne pas avoir
   * d'effet de bord.
   */
  private void cmdSupprimerSegment() {
    if (new BuDialogConfirmation(app_,RefluxImplementation.informationsSoftware(),
      "Etes vous s�r de vouloir supprimer la(les) "+iSelects_.length+" ligne(s) s�lectionn�e(s) ?")
      .activate()==1) return;

    for (int i=0; i<iSelects_.length; i++) {
      projet_.removeSegment(((RefluxGrSegment)iSelects_[i]).getSegment());
      cqSegment_.enleve((RefluxGrSegment)iSelects_[i]);
    }
    videSelection();
  }

  /**
   * Propri�t� d'un segment.
   */
  private void cmdProprietesSegment() {
    RefluxSegment sg= ((RefluxGrSegment)iSelects_[0]).getSegment();
    RefluxDialogProprietesSegment diSegment=
      new RefluxDialogProprietesSegment(app_.getFrame());
    diSegment.setProjet(projet_);
    diSegment.setSegment(sg);
    diSegment.show();
    if (diSegment.reponse == diSegment.btOk_) {
      getVueCalque().repaint();
    }

  }

  /**
   * Cr�ation d'une ligne plan. On vide juste la s�lection pour ne pas avoir
   * d'effet de bord.
   */
  private void cmdCreerPlan() {
    RefluxSegment[] sgs=new RefluxSegment[iSelects_.length];
    for (int i=0; i<sgs.length; i++) sgs[i]=((RefluxGrSegment)iSelects_[i]).getSegment();
    RefluxPlan plan=new RefluxPlan(sgs);
    plan.setNum(RefluxPlan.getLastNumber(projet_.getPlans()));
    RefluxGrPlan gr=new RefluxGrPlan(plan);
    projet_.addPlan(plan);
    cqPlan_.ajoute(gr);
    getVueCalque().repaint();
  }

  /**
   * Cr�ation d'une plan multiple. On vide juste la s�lection pour ne pas avoir
   * d'effet de bord.
   */
  private void cmdSupprimerPlan() {
    if (new BuDialogConfirmation(app_,RefluxImplementation.informationsSoftware(),
      "Etes vous s�r de vouloir supprimer le(s) "+iSelects_.length+" plan(s) s�lectionn�(s) ?")
      .activate()==1) return;

    Vector pls=projet_.getPlans();

    for (int i=0; i<iSelects_.length; i++) {
      RefluxPlan pl=((RefluxGrPlan)iSelects_[i]).getPlan();
      projet_.removePlan(pl);
      cqPlan_.enleve((RefluxGrPlan)iSelects_[i]);

      // Suppression des plans d�pendants du plan supprim�.
      for (int j=0; j<pls.size(); j++) {
        RefluxPlan plDep=(RefluxPlan)pls.get(j);
        if (plDep.isHAutomatiqueAmont() && plDep.getPlanAmont()==pl) plDep.setPlanAmont(null);
        if (plDep.isHAutomatiqueAval()  && plDep.getPlanAval()==pl)  plDep.setPlanAval(null);
      }
    }
    videSelection();
  }

  /**
   * Propri�t� d'un plan.
   */
  private void cmdProprietesPlan() {
    RefluxPlan pl= ((RefluxGrPlan)iSelects_[0]).getPlan();
    RefluxDialogProprietesPlan diPlan=
      new RefluxDialogProprietesPlan(app_.getFrame());
    diPlan.setProjet(projet_);
    diPlan.setPlan(pl);
    diPlan.show();
    if (diPlan.reponse == diPlan.btOk_) {
      getVueCalque().repaint();
    }
  }

  /**
   * Modification des propri�t�s de fond.
   */
  public void cmdProprietesFonds() {
    if (diPE != null && diPE.isShowing())
      return;
    diPE= new PRDialogPE(app_.getFrame());
    diPE.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        diPE_actionPerformed(_evt);
      }
    });
    diPE.setProjet(projet_);
    diPE.setSelection(cqActif_ != cqElement ? new Object[0] : iSelects_);
    diPE.show();
  }
  //----------------------------------------------------------------------------
  // Mise a jour des propri�t�s + natures de fond
  //----------------------------------------------------------------------------
  void diPE_actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() != diPE.OK_BUTTON
      && _evt.getSource() != diPE.APPLY_BUTTON)
      return;
    PRModeleProprietes mdlPrp= projet_.modeleProprietes();
    int typeNat;
    PRPropriete[] pes;
    PRPropriete[] pePbs= mdlPrp.proprietesElements();
    PRNature nature;
    Vector vctpePbs;
    //-------  Nature  ---------------------------------------------------------
    typeNat= diPE.getTypeNature();
    // Nature <mixte> => Pas de modification
    if (typeNat == -1)
      return;
    // Suppression des natures des �l�ments s�lectionn�s
    for (int i= 0; i < iSelects_.length; i++) {
      nature= (PRNature)el2Nature.get(iSelects_[i]);
      nature.supports().remove(iSelects_[i]);
    }
    // Nouvelle nature pour les supports s�lectionn�s
    nature= new PRNature(typeNat, iSelects_);
    mdlPrp.naturesFonds().addElement(nature);
    // Mise a jour de la table Element->Nature
    for (int i= 0; i < iSelects_.length; i++) {
      el2Nature.put(iSelects_[i], nature);
    }
    //-------  Propri�t�s �l�mentaires  ----------------------------------------
    pes= diPE.getProprietes();
    // Suppression dans les propri�t�s d'arete du pb des �l�ments s�lectionn�s
    for (int i= 0; i < pes.length; i++) {
      for (int j= 0; j < pePbs.length; j++) {
        if (pePbs[j].type() == pes[i].type()) {
          for (int k= 0; k < iSelects_.length; k++) {
            pePbs[j].supports().remove(iSelects_[k]);
          }
        }
      }
    }
    // Ajout des nouvelles propri�t�s cr��es dans le probleme
    vctpePbs= new Vector(pePbs.length);
    for (int i= 0; i < pePbs.length; i++)
      vctpePbs.addElement(pePbs[i]);
    for (int i= 0; i < pes.length; i++) {
      pes[i].supports(iSelects_);
      vctpePbs.addElement(pes[i]);
    }
    pePbs= new PRPropriete[vctpePbs.size()];
    vctpePbs.copyInto(pePbs);
    mdlPrp.proprietesElements(pePbs);
    initCalqueBord();
    getVueCalque().repaint();
    // Mise � jour des solutions initiales si le nombre de termes sur les
    // �l�ments a �t� chang�.
    projet_.controleNbTermesNoeuds();
  }
  /**
   * Rotation des normales des noeuds de bord s�lectionn�s.
   */
  private void cmdRotationNormale() {
    if (diNormale != null && diNormale.isShowing())
      return;
    diNormale= new PRDialogNormale(app_.getFrame());
    diNormale.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        diNormale_actionPerformed(_evt);
      }
    });
    diNormale.setProjet(projet_);
    diNormale.setSelection(
      cqActif_ != cqNormale && cqActif_ != cqNoeud ? new Object[0] : iSelects_);
    diNormale.show();
  }
  void diNormale_actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() == diNormale.OK_BUTTON
      || _evt.getSource() == diNormale.APPLY_BUTTON) {
      PRModeleProprietes mdlPrp= projet_.modeleProprietes();
      PRNormale normale;
      Double angle;
      if ((angle= diNormale.getAngle()) != null) {
        for (int i= 0; i < iSelects_.length; i++) {
          normale= mdlPrp.normale((GrNoeud)iSelects_[i]);
          if (normale != null) {
            normale.valeur(angle.doubleValue());
          }
        }
        reinitCalqueNormales();
        getVueCalque().repaint();
      }
    }
  }
  /**
   * D�placement d'un point de d�finition du plan des SI
   */
//  public void cmdDeplacementPoint() {
//    if (diPointSI != null && diPointSI.isShowing())
//      return;
//    diPointSI= new RefluxDialogPtSI(app_.getFrame());
//    diPointSI.addActionListener(new ActionListener() {
//      public void actionPerformed(ActionEvent _evt) {
//        diPointSI_actionPerformed(_evt);
//      }
//    });
//    diPointSI.setSelection(cqActif_ != cqPtSI ? new Object[0] : iSelects_);
//    diPointSI.show();
//  }
  /**
   * Mise a jour du GrPoint et du IPoint correspondant
   */
//  private void diPointSI_actionPerformed(ActionEvent _evt) {
//    if (_evt.getSource() != diPointSI.OK_BUTTON
//      && _evt.getSource() != diPointSI.APPLY_BUTTON)
//      return;
//    GrPoint pt= (GrPoint)objets_.renvoie(0);
//    double[] coor= diPointSI.getCoordonnees();
//    pt.x= coor[0];
//    pt.y= coor[1];
//    pt.z= coor[2];
//    getVueCalque().repaint();
//  }
  /**
   * R�initialisation des zones inond�es
   */
//  public void cmdInitialisationMares() {
//    projet_.initialiserSolutionsInitiales();
//    getVueCalque().repaint();
//  }
  /**
   * Suppression d'une zone inond�e (ht = 0 sur les noeuds s�lectionn�s).
   */
  public void cmdSuppressionMare() {
    GrNoeud noeud;
    PRSolutionsInitiales si;
    // ht � 0 sur les noeuds s�lectionn�s
    for (int i= 0; i < objets_.nombre(); i++) {
      noeud= (GrNoeud)objets_.renvoie(i);
      si= projet_.modeleCalcul().solutionInitiale(noeud);
      double[] vals= si.valeurs();
      if (vals[vals.length - 1] > 0) {
        //        si.initialiseHt0();
        for (int j= 0; j < vals.length; j++)
          vals[j]= 0;
        si.valeurs(vals);
      }
    }
    // Ajustement des ht des noeuds milieux pour qu'ils soient au milieu des
    // ht des noeuds de coin
    GrElement[] elements= projet_.maillage().elements();
    GrElement[] aretes;
    GrNoeud[] noeuds;
    PRSolutionsInitiales[] sis= new PRSolutionsInitiales[3];
    for (int i= 0; i < elements.length; i++) {
      aretes= elements[i].aretes();
      for (int j= 0; j < aretes.length; j++) {
        noeuds= aretes[j].noeuds_;
        if (noeuds.length == 3) {
          for (int k= 0; k < noeuds.length; k++)
            sis[k]= projet_.modeleCalcul().solutionInitiale(noeuds[k]);
          double[][] vs= new double[3][];
          for (int k= 0; k < 3; k++)
            vs[k]= sis[k].valeurs();
          vs[1][vs[1].length - 1]=
            (vs[0][vs[0].length - 1] + vs[2][vs[2].length - 1]) / 2;
          sis[1].valeurs(vs[1]);
          //          sis[1].hauteur((sis[0].hauteur()+sis[2].hauteur())/2);
        }
      }
    }
    getVueCalque().repaint();
  }
  /**
   * D�finition des nature de bord.
   */
  public void cmdDefinitionBord() {
    diPA= new RefluxDialogPA(app_.getFrame());
    diPA.initialise(projet_, iSelects_);
    diPA.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        diPA_actionPerformed(_evt);
      }
    });
    diPA.show();
  }
  //----------------------------------------------------------------------------
  // Mise a jour des propri�t�s + natures de bord
  //----------------------------------------------------------------------------
  void diPA_actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() != diPA.OK_BUTTON
      && _evt.getSource() != diPA.APPLY_BUTTON)
      return;
    PRModeleProprietes mdlPrp= projet_.modeleProprietes();
    int typeNat;
    PRPropriete[] pes;
    PRPropriete[] pePbs= mdlPrp.proprietesAretes();
    Vector vctpePbs;
    //-------  Nature  ---------------------------------------------------------
    typeNat= diPA.getTypeNature();
    // Nature <mixte> => Pas de modification
    if (typeNat == -1)
      return;
    // Modification des types des natures des �l�ments s�lectionn�s
    for (int i= 0; i < iSelects_.length; i++)
       ((PRNature)bordToNature.get(iSelects_[i])).type(typeNat);
    //-------  Propri�t�s �l�mentaires  ----------------------------------------
    pes= diPA.getProprietes();
    // Suppression dans les propri�t�s d'arete du pb des �l�ments s�lectionn�s
    for (int i= 0; i < pes.length; i++) {
      for (int j= 0; j < pePbs.length; j++) {
        if (pePbs[j].type() == pes[i].type()) {
          for (int k= 0; k < iSelects_.length; k++) {
            pePbs[j].supports().remove(iSelects_[k]);
          }
        }
      }
    }
    // Ajout des nouvelles propri�t�s cr��es dans le probleme
    vctpePbs= new Vector(pePbs.length);
    for (int i= 0; i < pePbs.length; i++)
      vctpePbs.addElement(pePbs[i]);
    for (int i= 0; i < pes.length; i++) {
      pes[i].supports(iSelects_);
      vctpePbs.addElement(pes[i]);
    }
    pePbs= new PRPropriete[vctpePbs.size()];
    vctpePbs.copyInto(pePbs);
    mdlPrp.proprietesAretes(pePbs);
    initCalqueBord();
    getVueCalque().repaint();
  }
  /**
   * D�finition des caract�ristiques de bord.
   */
  public void cmdDefinitionCaracteristiques() {
    if (diCLSO != null && diCLSO.isShowing())
      return;
    diCLSO= new PRDialogCLSO(app_.getFrame());
    diCLSO.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        diCLSO_actionPerformed(_evt);
      }
    });
    diCLSO.setProjet(projet_);
    diCLSO.setSelection(
      cqActif_ != cqNoeud && cqActif_ != cqNormale ? new Object[0] : iSelects_);
    diCLSO.show();
  }
  /**
   * Visualisation des num�ros de noeuds.
   */
  public void cmdVoirNumNoeuds(boolean _visible) {
    cqNoeud.setVisibleNumber(_visible);
    getVueCalque().repaint();
  }
  /**
   * Visualisation des num�ros d'�l�ments.
   */
  public void cmdVoirNumElements(boolean _visible) {
    cqElement.setVisibleNumber(_visible);
    getVueCalque().repaint();
  }

  /**
   * Cr�ation d'une ligne.
   */
  public void cqFormeI_formeSaisie(FormeEvent _evt) {
    DeTrait lg= (DeTrait)_evt.getForme();
    GrSegment sg= (GrSegment)lg.getGeometrie();
    RefluxSegment rsg=new RefluxSegment(new double[]{sg.o_.x_,sg.o_.y_},new double[]{sg.e_.x_,sg.e_.y_});
    rsg.setNum(RefluxSegment.getLastNumber(projet_.getSegments()));
    RefluxGrSegment gr=new RefluxGrSegment(rsg);
//    gr.sommets.ajoute(sg.o);
//    gr.sommets.ajoute(sg.e);
    projet_.addSegment(rsg);
    cqSegment_.ajoute(gr);
    getVueCalque().repaint();
    // D�sactivation du bouton et gel du calque.
    btSegment.setSelected(false);
    cqFormeI_.setGele(true);
  }

  //----------------------------------------------------------------------------
  // Mise a jour des caract�ristiques de bord
  //----------------------------------------------------------------------------
  void diCLSO_actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() != diCLSO.btOk_
      && _evt.getSource() != diCLSO.btApply_)
      return;
    Vector supports;
    Vector noeudsSelects= new Vector();
    PRConditionLimite[] cLs;
    PRConditionLimite[] cLsSup;
    PRConditionLimite[] cLPbs= projet_.conditionsLimites();
    Vector vctCLPbs;
    PRSollicitation[] sOs;
    PRSollicitation[] sOsSup;
    PRSollicitation[] sOPbs= projet_.sollicitations();
    Vector vctSOPbs;
    //-------  Noeuds de bord s�lectionn�s (seuls habilit�s � supporter des  ---
    //-------  caract�ristiques de bord)  --------------------------------------
    {
      Hashtable isNoeudBord= new Hashtable();
      GrNoeud[][] noeudsBord= projet_.maillage().noeudsContours();
      Object noeud;
      for (int i= 0; i < noeudsBord.length; i++)
        for (int j= 0; j < noeudsBord[i].length; j++)
          isNoeudBord.put(noeudsBord[i][j], noeudsBord[i][j]);
      for (int i= 0; i < iSelects_.length; i++)
        if ((noeud= isNoeudBord.get(iSelects_[i])) != null)
          noeudsSelects.addElement(noeud);
    }
    //-------  Conditions limites  ---------------------------------------------
    supports= new Vector(noeudsSelects);
    cLs= diCLSO.getConditions();
    cLsSup= diCLSO.getConditionsSupprimees();
    // Suppression dans les conditions du pb des noeuds s�lectionn�s
    for (int j= 0; j < cLPbs.length; j++) {
      for (int i= 0; i < cLs.length; i++) {
        if (cLPbs[j].type() == cLs[i].type()) {
          for (int k= 0; k < supports.size(); k++) {
            cLPbs[j].supports().remove(supports.get(k));
          }
        }
      }
      for (int i= 0; i < cLsSup.length; i++) {
        if (cLPbs[j].type() == cLsSup[i].type()) {
          for (int k= 0; k < supports.size(); k++) {
            cLPbs[j].supports().remove(supports.get(k));
          }
        }
      }
    }
    // Ajout des nouvelles conditions cr��es dans le probleme
    vctCLPbs= new Vector(cLPbs.length);
    for (int i= 0; i < cLPbs.length; i++)
      if (!cLPbs[i].supports().isEmpty())
        vctCLPbs.addElement(cLPbs[i]);
    for (int i= 0; i < cLs.length; i++) {
      cLs[i].supports(supports);
      vctCLPbs.addElement(cLs[i]);
    }
    cLPbs= new PRConditionLimite[vctCLPbs.size()];
    vctCLPbs.copyInto(cLPbs);
    projet_.conditionsLimites(cLPbs);
    //-------  Sollicitations  -------------------------------------------------
    supports= new Vector(noeudsSelects);
    sOs= diCLSO.getSollicitations();
    sOsSup= diCLSO.getSollicitationsSupprimees();
    // Suppression dans les sollicitations du pb des noeuds s�lectionn�s
    for (int j= 0; j < sOPbs.length; j++) {
      for (int i= 0; i < sOs.length; i++) {
        if (sOPbs[j].type() == sOs[i].type()) {
          for (int k= 0; k < supports.size(); k++) {
            sOPbs[j].supports().remove(supports.get(k));
          }
        }
      }
      for (int i= 0; i < sOsSup.length; i++) {
        if (sOPbs[j].type() == sOsSup[i].type()) {
          for (int k= 0; k < supports.size(); k++) {
            sOPbs[j].supports().remove(supports.get(k));
          }
        }
      }
    }
    // Ajout des nouvelles sollicitations cr��es dans le probleme
    vctSOPbs= new Vector(sOPbs.length);
    for (int i= 0; i < sOPbs.length; i++)
      if (!sOPbs[i].supports().isEmpty())
        vctSOPbs.addElement(sOPbs[i]);
    for (int i= 0; i < sOs.length; i++) {
      sOs[i].supports(supports);
      vctSOPbs.addElement(sOs[i]);
    }
    sOPbs= new PRSollicitation[vctSOPbs.size()];
    vctSOPbs.copyInto(sOPbs);
    projet_.sollicitations(sOPbs);
  }

  /**
   * Cr�ation d'un groupe sur des �l�ments.
   */
  public void cmdCreerGroupe() {
    RefluxGroupe grp=new RefluxGroupe(iSelects_);
    grp.setNum(RefluxGroupe.getLastNumber(projet_.getGroupes()));
    projet_.addGroupe(grp);
    cqGrp_.add(grp);
//    getVueCalque().repaint();
    videSelection();
  }

  /**
   * Suppression d'un groupe d'�l�ments. Autorise � supprimer des groupes sur
   * les plans.
   */
  public void cmdSupprimerGroupe() {
    if (new BuDialogConfirmation(app_,RefluxImplementation.informationsSoftware(),
      "Etes vous s�r de vouloir supprimer le(s) "+iSelects_.length+" groupe(s) s�lectionn�(s) ?")
      .activate()==1) return;

    Vector pls=projet_.getPlans();
    for (int i=0; i<iSelects_.length; i++) {
      RefluxGroupe gp=((RefluxGrGroupe)iSelects_[i]).getGroupe();
      projet_.removeGroupe(gp);
      cqGrp_.remove(gp);

      for (int j=0; j<pls.size(); j++) {
        RefluxPlan pl=(RefluxPlan)pls.get(j);
        if (pl.getGroupe()==gp) pl.setGroupe(null);
      }
    }
    videSelection();
  }

  /**
   * Initialisation de la fenetre avec le projet.
   */
  public void initialise(PRProjet _prj) {
    projet_= _prj;
    GrElement[] elements= projet_.maillage().elements();
    //GrNoeud[] noeuds= projet_.maillage().noeuds();
    GrNoeud[][] ndsCntr= projet_.maillage().noeudsContours();
    //PRSolutionsInitiales[] sis=projet_.modeleCalcul().solutionsInitiales();
//    GrPoint[] ptSI= projet_.modeleCalcul().pointsSI();
    PRModeleProprietes mdlPrp= projet_.modeleProprietes();
    Vector natBords= mdlPrp.naturesBords();
    Vector natEls= mdlPrp.naturesFonds();
    PRNature nature;
    grToI.clear();
    bordToNature.clear();
    el2Nature.clear();
    for (int i= 0; i < natBords.size(); i++)
      bordToNature.put(
        ((PRNature)natBords.elementAt(i)).supports().get(0),
        natBords.elementAt(i));
    for (int i= 0; i < natEls.size(); i++) {
      nature= (PRNature)natEls.elementAt(i);
      Vector supports= nature.supports();
      for (int j= 0; j < supports.size(); j++)
        el2Nature.put(supports.get(j), nature);
    }
    cqElement.removeAll();
    for (int i= 0; i < elements.length; i++) {
      cqElement.add(elements[i]);
      grToI.put(elements[i], elements[i]);
    }
    cqNoeud.removeAll();
//    GrPoint[] pts=new GrPoint[noeuds.length];
//    for (int i=0; i<pts.length; i++) pts[i]=noeuds[i].point;
//    ZModeleStatiquePoint mod=new ZModeleStatiquePoint(pts);
//    cqPoint_.modele(mod);
//    mdBerges.setTarget(cqPoint_.modele());
    for (int i= 0; i < ndsCntr.length; i++) {
      for (int j= 0; j < ndsCntr[i].length; j++) {
        cqNoeud.add(ndsCntr[i][j]);
        grToI.put(ndsCntr[i][j], ndsCntr[i][j]);
      }
    }
    cqNormale.reinitialise();
    for (int i= 0; i < ndsCntr.length; i++) {
      for (int j= 0; j < ndsCntr[i].length; j++) {
        PRNormale norm= mdlPrp.normale(ndsCntr[i][j]);
        GrPoint pt= ndsCntr[i][j].point_;
        GrSymbole s=
          new GrSymbole(GrSymbole.FLECHE, pt, norm.valeur() + 90, 15.);
        cqNormale.ajoute(s);
        grToI.put(s, ndsCntr[i][j]);
      }
    }
//    cqPtSI.reinitialise();
//    for (int i= 0; i < ptSI.length; i++) {
//      cqPtSI.ajoute(ptSI[i]);
//      grToI.put(ptSI[i], ptSI[i]);
//    }
    initCalqueBord();
    cqSI.setMaillage(projet_.maillage());
    cqSI.setProbleme(projet_);

    { // Calque des groupes d'�l�ments
      cqGrp_.removeAll();
      Vector grps=projet_.getGroupes();
      for (int i=0; i<grps.size(); i++) cqGrp_.add((RefluxGroupe)grps.get(i));
    }

    { // Calque des lignes de multiplans
      cqSegment_.reinitialise();
      Vector sgs=projet_.getSegments();
      for (int i=0; i<sgs.size(); i++) cqSegment_.ajoute(new RefluxGrSegment((RefluxSegment)sgs.get(i)));
    }

    { // Calque des lignes de multiplans
      cqPlan_.reinitialise();
      Vector plans=projet_.getPlans();
      for (int i=0; i<plans.size(); i++) cqPlan_.ajoute(new RefluxGrPlan((RefluxPlan)plans.get(i)));
    }

    setEnabledForAction("DEFINITIONCARBORD", true);
    setEnabledForAction("ROTATIONNORMALE", true);
    setEnabledForAction("PROPELEM", true);
    setEnabledForAction("IMPRIMER", true);
    // Cas des solutions initiales
    siOK_=Definitions.estPlanReferenceOK();
    if (siOK_) rootCalque_.add(gcSI_,0);
    else       rootCalque_.remove(gcSI_);
    setEnabledForAction("CALSOLINI",siOK_);
    setEnabledForAction("CREERSEGMENT",siOK_);
//    setEnabledForAction("INITMARES", siOK_);
//    setEnabledForAction("DEPLACEMENTPOINT", siOK_);
    getArbreCalqueModel().refresh();
    // Les fen�tres non modales disparaissent
    if (diCLSO != null) {
      diCLSO.dispose();
      diCLSO= null;
    }
    if (diNormale != null) {
      diNormale.dispose();
      diNormale= null;
    }
//    if (diPointSI != null) {
//      diPointSI.dispose();
//      diPointSI= null;
//    }
    if (diPE != null) {
      diPE.dispose();
      diPE= null;
    }
    // On vide la s�lection
    videSelection();
  }
  /**
   * Recentrage du BVueCalque en fonction des calques visibles.
   */
  public void zoomEtendu() {
    getVueCalque().changeRepere(this, getVueCalque().getCalque().getDomaine());
  }
  /**
   * R�initialisation du calque des normales.
   */
  private void reinitCalqueNormales() {
    VecteurGrSymbole smbs= cqNormale.getSymboles();
    for (int i= 0; i < smbs.nombre(); i++) {
      GrSymbole s= smbs.renvoie(i);
      GrNoeud nd= (GrNoeud)grToI.get(s);
      PRNormale norm= projet_.modeleProprietes().normale(nd);
      s.setRotationZInitiale(norm.valeur() + 90);
    }
  }
  // ---------------------------------------------------------------------------
  // Initialisation du calque des bords � partir du pb
  // ---------------------------------------------------------------------------
  private void initCalqueBord() {
    int type;
    int typeTrait;
    GrNoeud[] noeudsBord;
    GrElement[][] bords= projet_.maillage().aretesContours();
    RefluxGrBord[] grBords;
    int nbBords;
    nbBords= 0;
    for (int i= 0; i < bords.length; i++)
      for (int j= 0; j < bords[i].length; j++)
        nbBords += bords[i].length;
    grBords= new RefluxGrBord[nbBords];
    cqBord.reinitialise();
    int l= 0;
    for (int i= 0; i < bords.length; i++) {
      for (int j= 0; j < bords[i].length; j++) {
        type= ((PRNature)bordToNature.get(bords[i][j])).type();
        typeTrait= Definitions.getTypeTrait(type);
        grBords[l]= new RefluxGrBord(Color.black, typeTrait);
        noeudsBord= bords[i][j].noeuds_;
        for (int k= 0; k < noeudsBord.length; k++) {
          grBords[l].ligne.sommets_.ajoute(noeudsBord[k].point_);
        }
        cqBord.ajoute(grBords[l]);
        grToI.put(grBords[l], bords[i][j]);
        l++;
      }
    }
  }
  /**
   * R�cup�ration des objets s�lectionn�s.
   */
  public void selectedObjects(SelectionEvent _evt) {
    objets_= _evt.getObjects();
    cqActif_= (BCalque)_evt.getSource();
    boolean etat;
    // Stockage des I*** correspondants
    iSelects_= new Object[objets_.nombre()];
    if (cqActif_==cqGrp_) {
      for (int i=0; i<objets_.nombre(); i++) {
        iSelects_[i]=objets_.renvoie(i);
      }
    }
    else if (cqActif_==cqSegment_) {
      for (int i=0; i<objets_.nombre(); i++) {
        iSelects_[i]=objets_.renvoie(i);
//        System.out.println(((RefluxGrSegment)iSelects_[i]).getSegment().getNum());
      }
    }
    else if (cqActif_==cqPlan_) {
      for (int i=0; i<objets_.nombre(); i++) {
        iSelects_[i]=objets_.renvoie(i);
      }
    }
    else {
      for (int i= 0; i < objets_.nombre(); i++)
        iSelects_[i]= grToI.get(objets_.renvoie(i));
    }
    // Activation ou non des boutons
    // Bouton suppression d'une mare
    etat= siOK_ && objets_.nombre() > 0 && cqActif_ == cqSI;
    setEnabledForAction("SUPPRESSIONMARE", etat);
    // Bouton modification des natures de bord
    etat= objets_.nombre() > 0 && cqActif_ == cqBord;
    setEnabledForAction("DEFINITIONBORD", etat);
    // Cr�ation de groupe sur �l�ments
    etat= objets_.nombre() > 0 && cqActif_==cqElement && Definitions.estPlanReferenceOK();
    setEnabledForAction("GROUPER",etat);
    etat= objets_.nombre() > 0 && cqActif_==cqGrp_;
    setEnabledForAction("SUPGROUPE",etat);
    etat= objets_.nombre() > 0 && cqActif_==cqSegment_;
    setEnabledForAction("SUPSEGMENT",etat);
    etat= objets_.nombre()==1 && cqActif_==cqSegment_;
    setEnabledForAction("PROPSEGMENT",etat);
    etat= objets_.nombre() > 1 && cqActif_==cqSegment_;
    setEnabledForAction("CREERPLAN",etat);
    etat= objets_.nombre() > 0 && cqActif_==cqPlan_;
    setEnabledForAction("SUPPLAN",etat);
    etat= objets_.nombre()== 1 && cqActif_==cqPlan_;
    setEnabledForAction("PROPPLAN",etat);
//    etat= objets_.nombre()== 1 && cqActif_==cqPlan_;
//    setEnabledForAction("CALHINTER",etat);
    // Mise � jour des infos pour l'objet s�lectionn�.
    updateInfo(iSelects_);
    // Mise a jour des fenetres non modales
    if (diCLSO != null)
      diCLSO.setSelection(
        cqActif_ != cqNoeud
          && cqActif_ != cqNormale ? new Object[0] : iSelects_);
    if (diNormale != null)
      diNormale.setSelection(
        cqActif_ != cqNormale
          && cqActif_ != cqNoeud ? new Object[0] : iSelects_);
//    if (diPointSI != null)
//      diPointSI.setSelection(cqActif_ != cqPtSI ? new Object[0] : iSelects_);
    if (diPE != null)
      diPE.setSelection(cqActif_ != cqElement ? new Object[0] : iSelects_);
  }

  /**
   * Mise � jour du panneau d'information suivant le type d'objet.
   * @param _objs Object[]
   */
  private void updateInfo(Object[] _objs) {
    String sinfo;
    if (_objs.length==0) {
      sinfo="- Pas de s�lection -";
    }
    else if (_objs.length>1) {
      sinfo="Objets s�lectionn�s : "+_objs.length;
    }
    else if (_objs[0] instanceof GrNoeud) {
      GrNoeud nd=(GrNoeud)_objs[0];
      // Indice
      int ind=projet_.maillage().indice(nd);
      // Coordonn�es
      double x= nd.point_.x_;
      double y= nd.point_.y_;
      sinfo=
        "** Objet : Noeud\n"
          + "Num�ro : "
          + (ind + 1)
          + "\n"
          + "X : "
          + (float)x
          + "\n"
          + "Y : "
          + (float)y;
    }
    else if (_objs[0] instanceof GrElement) {
      GrElement el=(GrElement)_objs[0];
      // Indice
      int ind=projet_.maillage().indice(el);
      if (ind==-1) { // Element de bord, probablement => Pas d'infos.
        sinfo="Pas d'information";
      }
      else {
        // Connectivit�s
        int[] conns=projet_.maillage().connectivites(ind);
        sinfo=
            "** Objet : Element\n"
            +"Num�ro : "
            +(ind+1)
            +"\n"
            +"Noeuds :\n ";
        for (int i=0; i<conns.length; i++)sinfo+=(conns[i]+1)+" ";
      }
    }
    else if (_objs[0] instanceof RefluxGrGroupe) {
      RefluxGrGroupe gp=(RefluxGrGroupe)_objs[0];
      int ind=gp.getGroupe().getNum();
      sinfo=
          "** Objet : Groupe\n"
          + "Num�ro : "
          + (ind)
          + "\n Nombre d'�l�ments : "
          + gp.getGroupe().getObjects().length;
    }
    else if (_objs[0] instanceof RefluxGrSegment) {
      RefluxGrSegment gp=(RefluxGrSegment)_objs[0];
      int ind=gp.getSegment().getNum();
      sinfo=
          "** Objet : Ligne de plan\n"
          + "Num�ro : "
          + (ind)
          + "\nHauteur : "
          + (gp.getSegment().isHAutomatique() ? "Auto, valeur calcul�e : ":"")+gp.getSegment().getHauteur();
    }
    else if (_objs[0] instanceof RefluxGrPlan) {
      RefluxPlan pl=((RefluxGrPlan)_objs[0]).getPlan();
      RefluxSegment[] sgs=pl.getSegments();
      int ind=pl.getNum();
      sinfo=
          "** Objet : Multiplan\n"
          + "Num�ro : "
          + (ind)
          + "\nGroupe associ� : "
          + (pl.getGroupe()==null ? "Aucun":""+pl.getGroupe().getNum())
          + "\nLigne amont n�"+sgs[0].getNum()+
            (pl.isHAutomatiqueAmont()? " Hauteur auto, plan : "+(pl.getPlanAmont()==null ? "Aucun":""+pl.getPlanAmont().getNum()):" Hauteur fixe : "+pl.getHAmont())
          + "\nLigne aval n�"+sgs[sgs.length-1].getNum()+
            (pl.isHAutomatiqueAval()? " Hauteur auto, plan : "+(pl.getPlanAval()==null ? "Aucun":""+pl.getPlanAval().getNum()):" Hauteur fixe : "+pl.getHAval());
    }
    else {
      sinfo="Pas d'information";
    }
    pnInfo_.setInfo(sinfo);
  }

  /**
   * Construction de la barre de boutons.
   */
  private void buildTools() {

    // Propri�t�s de fond des �l�ments
    btCaracFond= new BuButton();
    btCaracFond.setMargin(new Insets(0, 0, 0, 0));
    btCaracFond.setName("tbPROPRIETESELEMENTAIRES");
    btCaracFond.setActionCommand("PROPELEM");
    btCaracFond.setIcon(RefluxResource.REFLUX.getIcon("proprietes"));
    btCaracFond.setToolTipText("Modification des propri�t�s des fonds");
    setEnabledForAction("PROPELEM", false);
    btCaracFond.addActionListener(app_);

    // Modification des normales
    btModifNormale= new BuButton();
    btModifNormale.setMargin(new Insets(0, 0, 0, 0));
    btModifNormale.setName("tbNORMALES");
    btModifNormale.setActionCommand("ROTATIONNORMALE");
    btModifNormale.setIcon(RefluxResource.REFLUX.getIcon("rotationnormale"));
    btModifNormale.setToolTipText("Modification des normales");
    setEnabledForAction("ROTATIONNORMALE", false);
    //    btModifNormale.setEnabled(false);
    btModifNormale.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // D�placement des points d�finissant le plan des SI
//    btModifPoint= new BuButton();
//    btModifPoint.setMargin(new Insets(0, 0, 0, 0));
//    btModifPoint.setName("tbPOINTSSI");
//    btModifPoint.setActionCommand("DEPLACEMENTPOINT");
//    btModifPoint.setIcon(RefluxResource.REFLUX.getIcon("deplacementpoint"));
//    btModifPoint.setToolTipText(
//      "Positionnement d'un point d�finissant le plan des SI");
//    setEnabledForAction("DEPLACEMENTPOINT", false);
//    btModifPoint.addActionListener(app_);
//
//    // Reinitialisation des zones inond�es
//    btInitMares= new BuButton();
//    btInitMares.setMargin(new Insets(0, 0, 0, 0));
//    btInitMares.setName("tbINITMARES");
//    btInitMares.setActionCommand("INITMARES");
//    btInitMares.setIcon(RefluxResource.REFLUX.getIcon("zonesmouillees"));
//    btInitMares.setToolTipText("R�initialisation des zones inond�es");
//    setEnabledForAction("INITMARES", false);
//    btInitMares.addActionListener(app_);

    // Suppression d'une mare
    btSupMare= new BuButton();
    btSupMare.setMargin(new Insets(0, 0, 0, 0));
    btSupMare.setName("tbSUPPRESSIONMARE");
    btSupMare.setActionCommand("SUPPRESSIONMARE");
    btSupMare.setIcon(RefluxResource.REFLUX.getIcon("supzonesmouillees"));
    btSupMare.setToolTipText("Suppression d'une zone inond�e");
    setEnabledForAction("SUPPRESSIONMARE", false);
    btSupMare.addActionListener(app_);

    // D�finition des natures de bord
    btNaturesBord= new BuButton();
    btNaturesBord.setMargin(new Insets(0, 0, 0, 0));
    btNaturesBord.setName("tbDEFINITIONBORD");
    btNaturesBord.setActionCommand("DEFINITIONBORD");
    btNaturesBord.setIcon(RefluxResource.REFLUX.getIcon("naturesbord"));
    btNaturesBord.setToolTipText("D�finition des natures de bord");
    setEnabledForAction("DEFINITIONBORD", false);
    btNaturesBord.addActionListener(app_);

    // D�finition des CL + Soll de bord
    btCaracBord= new BuButton();
    btCaracBord.setMargin(new Insets(0, 0, 0, 0));
    btCaracBord.setName("tbDEFINITIONCARBORD");
    btCaracBord.setActionCommand("DEFINITIONCARBORD");
    btCaracBord.setIcon(RefluxResource.REFLUX.getIcon("caracteristiques"));
    btCaracBord.setToolTipText("D�finition des caract�ristiques de bord");
    setEnabledForAction("DEFINITIONCARBORD", false);
    btCaracBord.addActionListener(app_);

    // Panneau d'information
    pnInfo_= new BPanneauInformation();
    pnInfo_.setInfo("- Pas de s�lection -");
    btInfo= new BuPopupButton("Information", pnInfo_);
    btInfo.setMargin(new Insets(0, 0, 0, 0));
    btInfo.setActionCommand("INFORMATION");
    btInfo.setToolTipText("Information sur l'objet s�lectionn�");
    btInfo.setIcon(RefluxResource.REFLUX.getIcon("information"));
    btInfo.setPaletteResizable(true);

    // Grouper les objets
    btGroupe= new BuButton();
    btGroupe.setMargin(new Insets(0, 0, 0, 0));
    btGroupe.setName("tbGROUPER");
    btGroupe.setActionCommand("GROUPER");
    btGroupe.setIcon(RefluxResource.REFLUX.getIcon("grouper"));
    btGroupe.setToolTipText("Grouper les �l�ments");
    setEnabledForAction("GROUPER", false);
    btGroupe.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // Supprimer un groupe
    btSupGroupe= new BuButton();
    btSupGroupe.setMargin(new Insets(0, 0, 0, 0));
    btSupGroupe.setName("tbSUPGROUPE");
    btSupGroupe.setActionCommand("SUPGROUPE");
    btSupGroupe.setIcon(RefluxResource.REFLUX.getIcon("supgroupe"));
    btSupGroupe.setToolTipText("Supprimer le(s) groupe(s) s�lectionn�(s)");
    setEnabledForAction("SUPGROUPE", false);
    btSupGroupe.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // Cr�ation d'un segment
    btSegment=
      new BuToggleButton(RefluxResource.REFLUX.getIcon("creersegment"));
    btSegment.setToolTipText("Cr�ation d'une ligne de plan");
    btSegment.setActionCommand("CREERSEGMENT");
    setEnabledForAction("CREERSEGMENT", false);
    btSegment.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // Suppression d'un segment
    btSupSegment=
      new BuButton(RefluxResource.REFLUX.getIcon("supsegment"));
    btSupSegment.setToolTipText("Supprimer la(les) ligne(s) de plan s�lectionn�e(s)");
    btSupSegment.setActionCommand("SUPSEGMENT");
    setEnabledForAction("SUPSEGMENT", false);
    btSupSegment.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // Propri�t� d'un segment
    btPropSegment=
      new BuButton(RefluxResource.REFLUX.getIcon("propsegment"));
    btPropSegment.setToolTipText("Propri�t�s d'une ligne de plan");
    btPropSegment.setActionCommand("PROPSEGMENT");
    setEnabledForAction("PROPSEGMENT", false);
    btPropSegment.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // Cr�ation d'un plan
    btPlan=
      new BuButton(RefluxResource.REFLUX.getIcon("creerplan"));
    btPlan.setToolTipText("Cr�ation d'un multiplan");
    btPlan.setActionCommand("CREERPLAN");
    setEnabledForAction("CREERPLAN", false);
    btPlan.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // Suppression d'un plan
    btSupPlan=
      new BuButton(RefluxResource.REFLUX.getIcon("supplan"));
    btSupPlan.setToolTipText("Supprimer le(s) multiplan(s) s�lectionn�(s)");
    btSupPlan.setActionCommand("SUPPLAN");
    setEnabledForAction("SUPPLAN", false);
    btSupPlan.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // Propri�t� d'un plan
    btPropPlan=
      new BuButton(RefluxResource.REFLUX.getIcon("propplan"));
    btPropPlan.setToolTipText("Propri�t�s d'un multiplan");
    btPropPlan.setActionCommand("PROPPLAN");
    setEnabledForAction("PROPPLAN", false);
    btPropPlan.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // Cr�ation des solutions initiales par multiplan
    btCalSolInit=
      new BuButton(RefluxResource.REFLUX.getIcon("calsolini"));
    btCalSolInit.setToolTipText("Calcul des solutions initiales par multiplans");
    btCalSolInit.setActionCommand("CALSOLINI");
    setEnabledForAction("CALSOLINI", false);
    btCalSolInit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        this_actionPerformed(_evt);
      }
    });

    // Cr�ation des solutions initiales par multiplan
//    btCalHInter=
//      new BuButton(RefluxResource.REFLUX.getIcon("calhinter"));
//    btCalHInter.setToolTipText("Calcul des h interm�diaires");
//    btCalHInter.setActionCommand("CALHINTER");
//    setEnabledForAction("CALHINTER", false);
//    btCalHInter.addActionListener(new ActionListener() {
//      public void actionPerformed(ActionEvent _evt) {
//        this_actionPerformed(_evt);
//      }
//    });
  }
}
