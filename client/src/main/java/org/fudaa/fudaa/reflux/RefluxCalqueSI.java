/*
 * @file         RefluxCalqueSI.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
// B.M.
// Remarque : Ce calque fait intervenir plusieurs objets Dodico mais qui ne sont
// pas forcement trac�s. Par exemple, pour repr�senter les "zones inond�es", il
// est n�cessaire de faire une interpolation sur les �l�ments, mais ceux ci ne
// sont pas repr�sent�s. Le calque ne suit pas (pour l'instant) le sch�ma des
// autres calques qui visualisent tous les objets qu'on y ajoute. De m�me, les
// objets ajout�s seront plut�t de type I que de type Gr puisqu'on ne les
// visualise pas.
package org.fudaa.fudaa.reflux;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Rectangle;

import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.VecteurGrContour;
/**
 * Un calque d'affichage des solutions initiales.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxCalqueSI extends BCalqueAffichage {
  PRProjet prj_;
  GrMaillageElement maillage_;
  GrBoite boite_;
  VecteurGrContour noeuds_= new VecteurGrContour();
  public RefluxCalqueSI() {
    super();
    setDestructible(false);
  }
  // Paint
  public void paintComponent(Graphics _g) {
    if (prj_ == null || prj_.maillage() == null)
      return;
    GrMaillageElement maillage= prj_.maillage();
    //boolean attenue    =isAttenue();
    boolean rapide= isRapide();
    int i, j;
    GrMorphisme versEcran= getVersEcran();
    //TraceGeometrie tg = new TraceGeometrie(_g,versEcran);
    //Polygon   pecr=getDomaine().enPolygoneXY().applique(versEcran).polygon();
    Rectangle clip= _g.getClipBounds();
    if (clip == null)
      clip= new Rectangle(0, 0, getWidth(), getHeight());
    //    if(clip.intersects(pecr.getBounds())) {
    if (rapide) {} else {
      // Affichage des zones inond�es
      GrElement[] elements= maillage.elements();
      //PRSolutionsInitiales[] sis=prj_.modeleCalcul().solutionsInitiales();
      //GrElement[]           element =maillage.elements();
      GrNoeud[] ndsEle;
      RefluxIsoSurfaces iso= new RefluxIsoSurfaces(new RefluxPaletteCouleur());
      //double[]             coor=new double[3];
      double[] valpt;
      GrPolygone pg= new GrPolygone();
      Polygon p;
      for (i= 0; i < elements.length; i++) {
        ndsEle= elements[i].noeuds_;
        valpt= new double[ndsEle.length];
        pg.sommets_.vide();
        for (j= 0; j < ndsEle.length; j++) {
          pg.sommets_.ajoute(ndsEle[j].point_);
          //            valpt[j]=prj_.modeleCalcul().solutionInitiale(ndsEle[j]).hauteur();
          double[] vals=
            prj_.modeleCalcul().solutionInitiale(ndsEle[j]).valeurs();
          valpt[j]= vals[vals.length - 1];
        }
        p= pg.applique(versEcran).polygon();
        iso.draw(_g, p, valpt);
      }
    }
    super.paintComponent(_g);
  }
  public void setMaillage(GrMaillageElement _maillage) {
    boite_= null;
    maillage_= _maillage;
    noeuds_= new VecteurGrContour();
    if (maillage_ != null)
      noeuds_.tableau(maillage_.noeuds());
  }
  public GrMaillageElement getMaillage() {
    return maillage_;
  }
  public void setProbleme(PRProjet _prj) {
    prj_= _prj;
  }
  public PRProjet getProbleme() {
    return prj_;
  }
  /**
   * Liste des objets s�lectionnables (normales).
   */
  public VecteurGrContour contours() {
    return noeuds_;
  }
  /**
   * Boite englobante des objets contenus dans le calque.
   * @return Boite englobante. Si le calque est vide,
   * la boite englobante retourn�e est <I>null</I>
   */
  public GrBoite getDomaine() {
    if (boite_ == null && noeuds_.nombre() > 0) {
      boite_= new GrBoite();
      for (int i= 0; i < noeuds_.nombre(); i++) {
        boite_.ajuste(((GrNoeud)noeuds_.renvoie(i)).point_);
      }
    }
    return boite_;
  }
}
