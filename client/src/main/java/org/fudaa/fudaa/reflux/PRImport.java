/*
 * @file         PRImport.java
 * @creation     2001-02-05
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.io.File;
import java.io.IOException;
/**
 * Une classe pour l'importation de format vers le projet.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRImport {
  /**
   * Le projet d'importation.
   */
  private PRProjet projet_;
  /**
   * Cr�ation de la classe avec le projet.
   * @param _prj Le projet sur lequel importer.
   */
  public PRImport(PRProjet _prj) {
    projet_= _prj;
  }
  /**
   * Importation des solutions initiales depuis les fichiers Reflux 2D ou 3D.
   * @param _file Fichiers de stockage des solutions initiales.
   */
  public void refluxSolutionsInitiales(File _file) throws IOException {
    PRSolutionsInitiales[] sis;
    sis= projet_.getInterfaceCalcul().getSolutionsFinales(projet_, _file);
    projet_.modeleCalcul().solutionsInitiales(sis);
  }
}