/*
 * @file         PRBoucleTemps.java
 * @creation     2000-03-02
 * @modification $Date: 2003-11-25 10:14:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
/**
 * Une classe d�finissant une boucle de temps.
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:14:05 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRBoucleTemps {
  private double debut_= 0;
  private double fin_= 1;
  private int nbPas_= 10;
  /**
   * Cr�ation d'une boucle de temps par d�faut
   */
  public PRBoucleTemps() {}
  /**
   * Cr�ation d'une boucle de temps avec un nombre de pas
   */
  public PRBoucleTemps(double _debut, double _fin, int _nbPas) {
    debut_= _debut;
    fin_= _fin;
    nbPas_= _nbPas;
  }
  /**
   * Cr�ation d'une boucle de temps avec une valeur de pas
   */
  public PRBoucleTemps(double _debut, double _fin, double _pas) {
    debut_= _debut;
    fin_= _fin;
    nbPas_= (int)Math.round((fin_ - debut_) / _pas);
  }
  /**
   * D�finition du d�but du temps
   */
  public void debut(double _debut) {
    debut_= _debut;
  }
  /**
   * Retourne le d�but du temps
   */
  public double debut() {
    return debut_;
  }
  /**
   * D�finition de la fin du temps
   */
  public void fin(double _fin) {
    fin_= _fin;
  }
  /**
   * Retourne la fin du temps
   */
  public double fin() {
    return fin_;
  }
  /**
   * D�finition du nombre de pas de temps
   */
  public void nbPas(int _nbPas) {
    nbPas_= _nbPas;
  }
  /**
   * Retourne le nombre de pas de temps
   */
  public int nbPas() {
    return nbPas_;
  }
  /**
   * Retourne la valeur du pas de temps
   */
  public double pas() {
    double pas= 0;
    if (nbPas_ == 0.)
      throw new ArithmeticException("Nombre de pas de temps=0");
    pas= (fin_ - debut_) / nbPas_;
    return pas;
  }
}