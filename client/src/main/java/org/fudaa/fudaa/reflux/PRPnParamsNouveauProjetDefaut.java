/*
 * @file         PRPnParamsNouveauProjetDefaut.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.memoire.bu.BuFileFilter;
/**
 * Un panneau pour la saisie de la m�thode et de ses coefficients. Les m�thodes
 * propos�es dans le panneau d�pendent du sch�ma.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRPnParamsNouveauProjetDefaut extends PRPnParamsNouveauProjet {
  JLabel lbFichiers= new JLabel();
  JTextField tfFichiers= new JTextField();
  JButton btFichiers= new JButton();
  JFileChooser diFc_;
  FlowLayout lyThis= new FlowLayout();
  /**
   * Pour retrouver le panel de coefficients � partir de son type de m�thode.
   */
  /**
   * Cr�ation du panneau de saisie de m�thode.
   */
  public PRPnParamsNouveauProjetDefaut() {
    super();
    try {
      jbInit();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  /**
   * D�finition de l'interface graphique.
   */
  private void jbInit() throws Exception {
    lbFichiers.setText("Fichiers de maillage:");
    tfFichiers.setPreferredSize(new Dimension(250, 21));
    btFichiers.setText("...");
    btFichiers.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        btFichiers_actionPerformed(_evt);
      }
    });
    this.setLayout(lyThis);
    this.add(lbFichiers);
    this.add(tfFichiers);
    this.add(btFichiers);
    BuFileFilter flt;
    // Dialogue d'ouverture des fichiers du projet.
    flt=
      new BuFileFilter(
        new String[] { "cor", "ele", "bth" },
        "Fichiers de maillage");
    diFc_= new JFileChooser();
    diFc_.setFileHidingEnabled(true);
    diFc_.setCurrentDirectory(new File(System.getProperty("user.dir")));
    diFc_.setMultiSelectionEnabled(false);
    diFc_.addChoosableFileFilter(flt);
    diFc_.setFileFilter(flt);
  }
  /**
   * Retourne les param�tres de cr�ation du projet sous forme d'objets.
   * @return Les param�tres. <i>null</i>
   */
  public Object[] getParametres() {
    return null;
  }
  /**
   * Retourne la racine du nom du projet.
   */
  public String getRacineProjet() {
    String r;
    r= tfFichiers.getText();
    String[] exts= { ".bth", ".cor", ".ele" };
    for (int i= 0; i < exts.length; i++) {
      if (r.endsWith(exts[i])) {
        r= r.substring(0, r.lastIndexOf(exts[i]));
        break;
      }
    }
    return r;
  }
  //----------------------------------------------------------------------------
  // Ev�nements
  //----------------------------------------------------------------------------
  /**
   * D�clench� quand le bouton de s�lection de fichiers de maillage pour le
   * projet est activ�.
   */
  void btFichiers_actionPerformed(ActionEvent _evt) {
    int ret= diFc_.showOpenDialog(this);
    if (ret == JFileChooser.APPROVE_OPTION) {
      tfFichiers.setText("" + diFc_.getSelectedFile());
    }
  }
}
