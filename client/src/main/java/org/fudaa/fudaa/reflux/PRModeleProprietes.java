/*
 * @file         PRModeleProprietes.java
 * @creation     2001-01-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.util.Hashtable;
import java.util.Vector;

import org.fudaa.ebli.geometrie.GrNoeud;
/**
 * Classe mod�le de propri�t�s physiques du projet. Le mod�le de propri�t�s
 * contient les propri�t�s globales, les propri�t�s �l�mentaires et les
 * propri�t�s nodales.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRModeleProprietes {
  /**
   * Propri�t�s globales du mod�le.
   */
  private PRPropriete[] prpsGlob_= Definitions.getDefaultProprietesGlobales();
  /**
   * Natures des fonds.
   */
  private Vector natsFds_= new Vector();
  /**
   * Propri�t�s de fond du mod�le.
   */
  private PRPropriete[] prpsEls_= new PRPropriete[0];
  /**
   * Natures des bords (une nature par bord). La nature du bord n'est jamais
   * chang�e, seul son type est modifi�.
   */
  private Vector natsBds_= new Vector();
  /**
   * Propri�t�s des bords (une propri�t� peut avoir plusieurs ar�tes pour
   * support).
   */
  private PRPropriete[] prpsBds_= new PRPropriete[0];
  /**
   * Normales aux noeuds.
   */
  private PRNormale[] normales_= new PRNormale[0];
  /**
   * Hashtable des noeuds pointants sur les normales.
   */
  private Hashtable hnd2Norm= new Hashtable();
  /**
   * Cr�ation d'un mod�le de propri�t�s.
   */
  public PRModeleProprietes() {}
  /**
   * Affecte les propri�t�s globales au mod�le.
   * @param _prps Les propri�t�s.
   */
  public void proprietesGlobales(PRPropriete[] _prps) {
    prpsGlob_= _prps;
  }
  /**
   * Retourne les propri�t�s globales du mod�le.
   * @return Les propri�t�s.
   */
  public PRPropriete[] proprietesGlobales() {
    return prpsGlob_;
  }
  /**
   * Affecte les propri�t�s �l�mentaires au mod�le.
   * @param _prps Les propri�t�s.
   */
  public void proprietesElements(PRPropriete[] _prps) {
    prpsEls_= _prps;
  }
  /**
   * Retourne les propri�t�s �l�mentaires du mod�le.
   * @return Les propri�t�s.
   */
  public PRPropriete[] proprietesElements() {
    return prpsEls_;
  }
  /**
   * Affecte les propri�t�s d'aretes au mod�le.
   * @param _prps Les propri�t�s.
   */
  public void proprietesAretes(PRPropriete[] _prps) {
    prpsBds_= _prps;
  }
  /**
   * Retourne les propri�t�s d'aretes du mod�le.
   * @return Les propri�t�s.
   */
  public PRPropriete[] proprietesAretes() {
    return prpsBds_;
  }
  /**
   * Affecte les natures de fond au mod�le.
   * @param _natures Les natures.
   */
  public void naturesFonds(Vector _natures) {
    natsFds_= _natures;
  }
  /**
   * Retourne les natures de fond du mod�le.
   * @return Les natures.
   */
  public Vector naturesFonds() {
    return natsFds_;
  }
  /**
   * Affecte les natures de bord au mod�le.
   * @param _natures Les natures.
   */
  public void naturesBords(Vector _natures) {
    natsBds_= _natures;
  }
  /**
   * Retourne les natures de bord du mod�le.
   * @return Les natures.
   */
  public Vector naturesBords() {
    return natsBds_;
  }
  /**
   * Affecte les normales aux noeuds.
   * @param _normales Les normales.
   */
  public void normales(PRNormale[] _normales) {
    normales_= _normales;
    hnd2Norm.clear();
    for (int i= 0; i < normales_.length; i++) {
      hnd2Norm.put(normales_[i].support(), normales_[i]);
    }
  }
  /**
   * Retourne les normales sur les noeuds.
   * @return Les normales.
   */
  public PRNormale[] normales() {
    return normales_;
  }
  /**
   * Retourne la normale pour un noeud donn�.
   *
   * @param _nd
   * @return La normale ou <code>null</code> si le noeud n'est pas un noeud de
   *         bord.
   */
  public PRNormale normale(GrNoeud _nd) {
    return (PRNormale)hnd2Norm.get(_nd);
  }
}