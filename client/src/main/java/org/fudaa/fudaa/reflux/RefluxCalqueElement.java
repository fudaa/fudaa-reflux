/*
 * @file         RefluxCalqueElement.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.Hashtable;

import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.trace.TraceGeometrie;
/**
 * Un calque d'affichage des �l�ments.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class RefluxCalqueElement extends BCalqueAffichage {
  private VecteurGrContour elements_;
  private GrBoite boite_;
  private Hashtable el2Num_;
  private boolean visibleNumber_;
  public RefluxCalqueElement() {
    super();
    setDestructible(false);
    elements_= new VecteurGrContour();
    boite_= null;
    visibleNumber_= false;
    el2Num_= null;
  }
  /**
   * Trac� des �l�ments.
   */
  public void paintComponent(Graphics _g) {
    if (elements_.nombre() > 0) {
      int i;
      Polygon p;
      int num;
      GrElement ele;
      GrPoint bary;
      if (el2Num_ == null) {
        el2Num_= new Hashtable();
        GrElement[] els= RefluxImplementation.projet.maillage().elements();
        for (i= 0; i < els.length; i++) {
          el2Num_.put(els[i], new Integer(i));
        }
      }
      GrMorphisme versEcran= getVersEcran();
      TraceGeometrie tg= new TraceGeometrie( versEcran);
      Polygon pecr= getDomaine().enPolygoneXY().applique(versEcran).polygon();
      Rectangle clip= _g.getClipBounds();
      if (clip == null)
        clip= new Rectangle(0, 0, getWidth(), getHeight());
      if (clip.intersects(pecr.getBounds())) {
        if (isRapide()) {
          Color c= Color.black;
          _g.setColor(isAttenue() ? attenueCouleur(c) : c);
          _g.drawPolygon(pecr);
        } else {
          // Affichage des �l�ments
          int n= elements_.nombre();
          for (i= 0; i < n; i++) {
            ele= (GrElement)elements_.renvoie(i);
            p= ele.applique(versEcran).polygon();
            //            for (j=0; j<p.length; j++) {
            if (clip.intersects(p.getBounds())) {
              Color c= Color.black;
              _g.setColor(isAttenue() ? attenueCouleur(c) : c);
              _g.drawPolygon(p);
              //                _g.drawPolygon(p[j]);
            }
            //            }
            // Visibilit� des num�ros
            if (!isRapide() && visibleNumber_) {
              bary= ele.barycentre();
              num= ((Integer)el2Num_.get(ele)).intValue() + 1;
              tg.dessineTexte(( Graphics2D) _g,"" + num, bary, isRapide());
            }
          }
        }
      }
    }
    super.paintComponent(_g);
  }
  /**
   * Ajout d'un <I>GrElement</I> au calque.
   */
  public void add(GrElement _element) {
    elements_.ajoute(_element);
    boite_= null;
    el2Num_= null;
  }
  /**
   * Suppression d'un <I>GrElement</I> du calque.
   */
  public void remove(GrElement _element) {
    elements_.enleve(_element);
    boite_= null;
    el2Num_= null;
  }
  /**
   * Suppression de tous les <I>GrElement</I>.
   */
  public void removeAll() {
    elements_.vide();
    boite_= null;
    el2Num_= null;
  }
  /**
   * Visibilit� des num�ros d'objets.
   */
  public void setVisibleNumber(boolean _visible) {
    visibleNumber_= _visible;
  }
  /**
   * Liste des �l�ments s�lectionnables.
   */
  public VecteurGrContour contours() {
    return elements_;
  }
  /**
   * Boite englobante des objets contenus dans le calque.
   * @return Boite englobante. Si le calque est vide,
   * la boite englobante retourn�e est <I>null</I>
   */
  public GrBoite getDomaine() {
    GrBoite boiteElement;
    if (boite_ == null && elements_.nombre() > 0) {
      boite_= new GrBoite();
      for (int i= 0; i < elements_.nombre(); i++) {
        boiteElement= ((GrElement)elements_.renvoie(i)).boite();
        boite_.ajuste(boiteElement.o_);
        boite_.ajuste(boiteElement.e_);
      }
    }
    return boite_;
  }
}
