/*
 * @file         TT2dPnPropsGlobales.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d2transport;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuGridLayout;

import org.fudaa.dodico.corba.mesure.IEvolutionConstante;
import org.fudaa.dodico.corba.mesure.IEvolutionConstanteHelper;

import org.fudaa.dodico.mesure.DEvolutionConstante;

import org.fudaa.fudaa.reflux.PRPnPropsGlobales;
import org.fudaa.fudaa.reflux.PRPropriete;
/**
 * Un panel comportant les propri�t�s physiques globales sp�cifiques Reflux 2d
 * transport.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class TT2dPnPropsGlobales extends PRPnPropsGlobales {
  //public class TT2dPnPropsGlobales extends JPanel {
  BuGridLayout lyThis= new BuGridLayout();
  JLabel lbDiffuLongi= new JLabel();
  JTextField tfDiffuLongi= new JTextField();
  JLabel lbDiffuTrans= new JLabel();
  JTextField tfDiffuTrans= new JTextField();
  JLabel lbPorosite= new JLabel();
  JTextField tfPorosite= new JTextField();
  JLabel lbShields= new JLabel();
  JTextField tfShields= new JTextField();
  JLabel lbPsi= new JLabel();
  JTextField tfPsi= new JTextField();
  JLabel lbViscoDyn= new JLabel();
  JTextField tfViscoDyn= new JTextField();
  /**
   * Sauvegarde des propri�t�s.
   */
  private double[] oldPrps_= new double[TT2dResource.nbPropsGlob];
  JLabel lbDensO= new JLabel();
  JTextField tfDensO= new JTextField();
  JLabel lbDensSed= new JLabel();
  JTextField tfDensSed= new JTextField();
  /**
   * Cr�ation du panel.
   */
  public TT2dPnPropsGlobales() {
    try {
      jbInit();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  /**
   * M�thode d'impl�mentation de l'interface.
   */
  private void jbInit() throws Exception {
    lbDensO.setText("Densit� de l'eau:");
    lbDensSed.setText("Densit� de s�diment:");
    lbDiffuLongi.setText("Diffusion longitudinale:");
    lbDiffuTrans.setText("Diffusion transversale:");
    lbPorosite.setText("Porosit� de granula:");
    lbShields.setText("Param�tre de Shields:");
    lbPsi.setText("Coefficient Psi:");
    lbViscoDyn.setText("Viscosit� dynamique:");
    lbDensO.setHorizontalAlignment(SwingConstants.RIGHT);
    lbDensSed.setHorizontalAlignment(SwingConstants.RIGHT);
    lbDiffuLongi.setHorizontalAlignment(SwingConstants.RIGHT);
    lbDiffuTrans.setHorizontalAlignment(SwingConstants.RIGHT);
    lbPorosite.setHorizontalAlignment(SwingConstants.RIGHT);
    lbShields.setHorizontalAlignment(SwingConstants.RIGHT);
    lbPsi.setHorizontalAlignment(SwingConstants.RIGHT);
    lbViscoDyn.setHorizontalAlignment(SwingConstants.RIGHT);
    tfDensO.setPreferredSize(new Dimension(150, 21));
    tfDensSed.setPreferredSize(new Dimension(150, 21));
    tfDiffuLongi.setPreferredSize(new Dimension(150, 21));
    tfDiffuTrans.setPreferredSize(new Dimension(150, 21));
    tfPorosite.setPreferredSize(new Dimension(150, 21));
    tfShields.setPreferredSize(new Dimension(150, 21));
    tfPsi.setPreferredSize(new Dimension(150, 21));
    tfViscoDyn.setPreferredSize(new Dimension(150, 21));
    lyThis.setColumns(2);
    lyThis.setHgap(5);
    lyThis.setVgap(5);
    this.setLayout(lyThis);
    this.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.add(lbDensO, null);
    this.add(tfDensO, null);
    this.add(lbDensSed, null);
    this.add(tfDensSed, null);
    this.add(lbDiffuLongi, null);
    this.add(tfDiffuLongi, null);
    this.add(lbDiffuTrans, null);
    this.add(tfDiffuTrans, null);
    this.add(lbPorosite, null);
    this.add(tfPorosite, null);
    this.add(lbShields, null);
    this.add(tfShields, null);
    this.add(lbPsi, null);
    this.add(tfPsi, null);
    this.add(lbViscoDyn, null);
    this.add(tfViscoDyn, null);
  }
  /**
   * Initialisation avec les propri�t�s globales.
   *
   * @param _prps Les propri�t�s.
   */
  public void setProprietes(PRPropriete[] _prps) {
    for (int i= 0; i < oldPrps_.length; i++)
      oldPrps_[i]= ((IEvolutionConstante)_prps[i].evolution()).constante();
    tfDensO.setText("" + oldPrps_[TT2dResource.DENS_EAU]);
    tfDensSed.setText("" + oldPrps_[TT2dResource.DENS_SEDI]);
    tfDiffuLongi.setText("" + oldPrps_[TT2dResource.DIFFU_LONGI]);
    tfDiffuTrans.setText("" + oldPrps_[TT2dResource.DIFFU_TRAN]);
    tfPorosite.setText("" + oldPrps_[TT2dResource.POROSITE]);
    tfShields.setText("" + oldPrps_[TT2dResource.SHIELDS]);
    tfPsi.setText("" + oldPrps_[TT2dResource.COEF_PSI]);
    tfViscoDyn.setText("" + oldPrps_[TT2dResource.VISCO_DYN]);
  }
  /*
   * Retourne les propri�t�s physiques globales.
   * @return Les propri�t�s dans l'ordre.
   */
  public PRPropriete[] getProprietes() {
    PRPropriete[] r= new PRPropriete[TT2dResource.nbPropsGlob];
    int ind;
    ind= TT2dResource.DENS_EAU;
    try {
      oldPrps_[ind]= Double.parseDouble(tfDensO.getText());
    } catch (NumberFormatException _exc) {}
    ind= TT2dResource.DENS_SEDI;
    try {
      oldPrps_[ind]= Double.parseDouble(tfDensSed.getText());
    } catch (NumberFormatException _exc) {}
    ind= TT2dResource.DIFFU_LONGI;
    try {
      oldPrps_[ind]= Double.parseDouble(tfDiffuLongi.getText());
    } catch (NumberFormatException _exc) {}
    ind= TT2dResource.DIFFU_TRAN;
    try {
      oldPrps_[ind]= Double.parseDouble(tfDiffuTrans.getText());
    } catch (NumberFormatException _exc) {}
    ind= TT2dResource.POROSITE;
    try {
      oldPrps_[ind]= Double.parseDouble(tfPorosite.getText());
    } catch (NumberFormatException _exc) {}
    ind= TT2dResource.SHIELDS;
    try {
      oldPrps_[ind]= Double.parseDouble(tfShields.getText());
    } catch (NumberFormatException _exc) {}
    ind= TT2dResource.COEF_PSI;
    try {
      oldPrps_[ind]= Double.parseDouble(tfPsi.getText());
    } catch (NumberFormatException _exc) {}
    ind= TT2dResource.VISCO_DYN;
    try {
      oldPrps_[ind]= Double.parseDouble(tfViscoDyn.getText());
    } catch (NumberFormatException _exc) {}
    for (int i= 0; i < oldPrps_.length; i++) {
      IEvolutionConstante evol=
        IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());
      evol.constante(oldPrps_[i]);
      r[i]= new PRPropriete(-1, evol, new Object[0]);
    }
    return r;
  }
}
