/*
 * @file         Definitions.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;

import org.fudaa.dodico.corba.mesure.IEvolutionConstante;

import org.fudaa.dodico.objet.UsineLib;

import org.fudaa.ebli.trace.TraceLigne;

/**
 * D�finition des problemes support�s. Chaque probl�me est d�fini (nombre, types des natures de fond, etc.)
 *
 * @version $Revision: 1.10 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author Bertrand Marchand
 */
public final class Definitions {
  
  private Definitions() {}
  // Probl�mes
  private final static String PjVT2D = "Courantologie 2D";
  private final static String PjVL2D = "Courantologie 2D + lmg";
  private final static String PjVT3D = "Courantologie 3D";
  private final static String PjSU2D = "Transport en suspension 2D";
  private final static String PjCH2D = "Transport total 2D";
  // Sch�mas
  private final static String ScSTAT = "Stationnaire";
  private final static String ScEULE = "Euler";
  private final static String ScKAWA = "Kawahara";
  private final static String ScLAXW = "Lax Wendroff";
  // M�thodes
  private final static String MtLI = "Lin�aire";
  private final static String MtNR = "Newton Raphson";
  private final static String MtNRBC = "Newton Raphson + bancs c/d";
  private final static String MtNRLM = "Newton Raphson + lmg";
  private final static String MtSL = "Selected Lumping";
  private final static String MtSLBC = "Selected Lumping + bancs c/d";
  private final static String MtLW = "Lax Wendroff";
  // Natures
  private final static String GpFD = "Fond 2D";
  private final static String GpFDLM = "Fond Lmg";
  private final static String GpFD3D = "Fond 3D";
  private final static String GpFDNE = "Fond non �rodable";
  private final static String GpBDO = "Bord ouvert";
  private final static String GpBDOQ = "Bord ouvert avec d�bit";
  private final static String GpBDOV = "Bord ouvert avec vitesse";
  private final static String GpBDF = "Bord ferm�";
  private final static String GpBDFF = "Bord ferm� avec frottement";
  private final static String GpBDQS = "Bord ouvert avec d�bit solide";
  private final static String GpBDOL = "Bord ouvert libre";
  private final static String GpBDFC = "Bord ouvert avec flux de conc.";
  // Propri�t�s
  private final static String PrRUGO = "Rugosit�";
  private final static String PrVISC = "Viscosit�";
  private final static String PrALLM = "Alpha longueur de m�lange";
  private final static String PrFTBD = "Frottement de paroi";
  private final static String PrPERT = "Perte de charge";
  private final static String PrBASE = "Base d'approximation";
  private final static String PrNBFT = "Nombre de termes de la base";
  private final static String PrD50 = "Grain D50";
  private final static String PrD90 = "Grain D90";
  // Conditions limites
  private final static String ClVITX = "Vitesse Un";
  private final static String ClVITY = "Vitesse Ut";
  private final static String ClHT = "Niveau d'eau";
  private final static String ClCONC = "Concentration";
  private final static String ClZF = "Cote de fond";
  // Sollicitations
  private final static String SlQ = "D�bit lin�ique";
  private final static String SlQS = "D�bit solide";
  private final static String SlFLUX = "Flux";
  // Coefficients de m�thodes
  private final static String CmRELA = "Relaxation";
  private final static String CmPNR = "Pr�cision Newton Raphson";
  private final static String CmPBCD = "Pr�cision Bancs Couvrants/D�couvrants";
  private final static String CmVISC = "Viscosit� des �l�ments de transit";
  private final static String CmNBIT = "Nombre d'it�rations maxi";
  // R�sultats
  private final static String RsX = "Coordonn�e X";
  private final static String RsY = "Coordonn�e Y";
  private final static String RsBATH = "Bathym�trie";
  private final static String RsVITX = "Vitesse X";
  private final static String RsVITY = "Vitesse Y";
  // private final static String RsNULL="<null>";
  private final static String RsNE = "Niveau d'eau";
  private final static String RsPROF = "Profondeur";
  private final static String RsNORM = "Norme vitesse";

  /**
   * Retourne les types de projets.
   */
  static public int[] getTypesProjet() {
    return new int[] { PRProjet.VIT_NU_CON, PRProjet.VIT_NU_LMG, PRProjet.TRANS_SUS_2D, PRProjet.TRANS_TOT_2D,
        PRProjet.VIT_3D };
  }

  /**
   * Retourne le string pour le type de projet donn�.
   */
  static public String projetToString(int _type) {
    String r = null;
    if (_type == PRProjet.VIT_NU_CON) r = PjVT2D;
    else if (_type == PRProjet.VIT_NU_LMG) r = PjVL2D;
    else if (_type == PRProjet.TRANS_SUS_2D) r = PjSU2D;
    else if (_type == PRProjet.TRANS_TOT_2D) r = PjCH2D;
    else if (_type == PRProjet.VIT_3D) r = PjVT3D;
    return r;
  }

  /**
   * Retourne le type de projet pour le string donn�.
   *
   * @param _string String �quivalent au type de projet.
   */
  static public int stringToProjet(String _string) {
    int r = 0;
    if (_string == PjVT2D) r = PRProjet.VIT_NU_CON;
    else if (_string == PjVL2D) r = PRProjet.VIT_NU_LMG;
    else if (_string == PjSU2D) r = PRProjet.TRANS_SUS_2D;
    else if (_string == PjCH2D) r = PRProjet.TRANS_TOT_2D;
    else if (_string == PjVT3D) r = PRProjet.VIT_3D;
    return r;
  }

  /**
   * Retourne le string pour le type de sch�ma donn�.
   *
   * @param _type Le type de sch�ma de r�solution.
   * @return Le string correspondant.
   */
  static public String schemaToString(int _type) {
    String r = null;
    if (_type == PRSchemaResolution.STATIONNAIRE) r = ScSTAT;
    else if (_type == PRSchemaResolution.EULER) r = ScEULE;
    else if (_type == PRSchemaResolution.KAWAHARA) r = ScKAWA;
    else if (_type == PRSchemaResolution.LAX_WENDROFF) r = ScLAXW;
    return r;
  }

  /**
   * Retourne le type de sch�ma pour le string donn�.
   *
   * @param _string String �quivalent au type de sch�ma.
   */
  public static int stringToSchema(String _string) {
    int r = 0;
    if (_string == ScEULE) r = PRSchemaResolution.EULER;
    else if (_string == ScKAWA) r = PRSchemaResolution.KAWAHARA;
    else if (_string == ScLAXW) r = PRSchemaResolution.LAX_WENDROFF;
    else if (_string == ScSTAT) r = PRSchemaResolution.STATIONNAIRE;
    return r;
  }

  /**
   * Retourne le string pour le type de m�thode donn�e.
   *
   * @param _type Le type de m�thode de r�solution.
   * @return Le string correspondant.
   */
  static public String methodeToString(int _type) {
    String r = null;
    if (_type == PRMethodeResolution.LINEAIRE) r = MtLI;
    else if (_type == PRMethodeResolution.NEWTON_RAPHSON) r = MtNR;
    else if (_type == PRMethodeResolution.NEWTON_RAPHSON_BCD) r = MtNRBC;
    else if (_type == PRMethodeResolution.NEWTON_RAPHSON_LMG) r = MtNRLM;
    else if (_type == PRMethodeResolution.SELECTED_LUMPING) r = MtSL;
    else if (_type == PRMethodeResolution.SELECTED_LUMPING_BCD) r = MtSLBC;
    else if (_type == PRMethodeResolution.LAX_WENDROFF) r = MtLW;
    return r;
  }

  /**
   * Retourne le type de methode pour le string donn�.
   *
   * @param _string String �quivalent au type de m�thode.
   * @return Le type correspondant.
   */
  public static int stringToMethode(String _string) {
    int r = 0;
    if (_string == MtLI) r = PRMethodeResolution.LINEAIRE;
    else if (_string == MtNR) r = PRMethodeResolution.NEWTON_RAPHSON;
    else if (_string == MtNRBC) r = PRMethodeResolution.NEWTON_RAPHSON_BCD;
    else if (_string == MtNRLM) r = PRMethodeResolution.NEWTON_RAPHSON_LMG;
    else if (_string == MtSL) r = PRMethodeResolution.SELECTED_LUMPING;
    else if (_string == MtSLBC) r = PRMethodeResolution.SELECTED_LUMPING_BCD;
    else if (_string == MtLW) r = PRMethodeResolution.LAX_WENDROFF;
    return r;
  }

  /**
   * Retourne le string pour le type de r�sultat donn�e.
   *
   * @param _type Le type de r�sultat.
   * @return Le string correspondant.
   */
  static public String resultToString(int _type) {
    String r = null;
    if (_type == PRResultats.COORD_X) r = RsX;
    else if (_type == PRResultats.COORD_Y) r = RsY;
    else if (_type == PRResultats.BATHYMETRIE) r = RsBATH;
    else if (_type == PRResultats.VITESSE_X) r = RsVITX;
    else if (_type == PRResultats.VITESSE_Y) r = RsVITY;
    else if (_type == PRResultats.NIVEAU_EAU) r = RsNE;
    else if (_type == PRResultats.PROFONDEUR) r = RsPROF;
    else if (_type == PRResultats.NORME) r = RsNORM;
    else
      r = "";
    return r;
  }

  /**
   * Retourne le type de resultat pour le string donn�.
   *
   * @param _string String �quivalent au type de r�sultat.
   * @return Le type correspondant.
   */
  public static int stringToResultat(String _string) {
    int r = 0;
    if (_string == RsX) r = PRResultats.COORD_X;
    else if (_string == RsY) r = PRResultats.COORD_Y;
    else if (_string == RsBATH) r = PRResultats.BATHYMETRIE;
    else if (_string == RsVITX) r = PRResultats.VITESSE_X;
    else if (_string == RsVITY) r = PRResultats.VITESSE_Y;
    else if (_string == RsNE) r = PRResultats.NIVEAU_EAU;
    else if (_string == RsPROF) r = PRResultats.PROFONDEUR;
    else if (_string == RsNORM) r = PRResultats.NORME;
    else
      r = PRResultats.UNDEFINED;
    return r;
  }

  /**
   * Retourne le string pour la nature donn�e.
   *
   * @param _tpNat Type de nature.
   * @return La chaine correspondante.
   */
  static public String natureToString(int _tpNat) {
    String r = null;
    if (_tpNat == PRNature.FOND) r = GpFD;
    else if (_tpNat == PRNature.FOND_LMG) r = GpFDLM;
    else if (_tpNat == PRNature.FOND_3D) r = GpFD3D;
    else if (_tpNat == PRNature.FOND_NON_ERODABLE) r = GpFDNE;
    else if (_tpNat == PRNature.BORD_OUVERT) r = GpBDO;
    else if (_tpNat == PRNature.BORD_OUVERT_DEBIT) r = GpBDOQ;
    else if (_tpNat == PRNature.BORD_OUVERT_VITESSE) r = GpBDOV;
    else if (_tpNat == PRNature.BORD_FERME) r = GpBDF;
    else if (_tpNat == PRNature.BORD_FERME_FROTTEMENT) r = GpBDFF;
    else if (_tpNat == PRNature.BORD_OUVERT_DEBIT_SOL) r = GpBDQS;
    else if (_tpNat == PRNature.BORD_OUVERT_LIBRE) r = GpBDOL;
    else if (_tpNat == PRNature.BORD_OUVERT_FLUX_CONC) r = GpBDFC;
    return r;
  }

  /**
   * Retourne la nature pour le string donn�.
   *
   * @param _string String �quivalent � la nature.
   * @return Le type correspondant.
   */
  static public int stringToNature(String _string) {
    int r = -1;
    if (_string == GpFD) r = PRNature.FOND;
    else if (_string == GpFDLM) r = PRNature.FOND_LMG;
    else if (_string == GpFD3D) r = PRNature.FOND_3D;
    else if (_string == GpFDNE) r = PRNature.FOND_NON_ERODABLE;
    else if (_string == GpBDO) r = PRNature.BORD_OUVERT;
    else if (_string == GpBDOQ) r = PRNature.BORD_OUVERT_DEBIT;
    else if (_string == GpBDOV) r = PRNature.BORD_OUVERT_VITESSE;
    else if (_string == GpBDF) r = PRNature.BORD_FERME;
    else if (_string == GpBDFF) r = PRNature.BORD_FERME_FROTTEMENT;
    else if (_string == GpBDQS) r = PRNature.BORD_OUVERT_DEBIT_SOL;
    else if (_string == GpBDOL) r = PRNature.BORD_OUVERT_LIBRE;
    else if (_string == GpBDFC) r = PRNature.BORD_OUVERT_FLUX_CONC;
    return r;
  }

  /**
   * Retourne le string pour la propri�t� donn�e.
   *
   * @param _tpPrp Type de propri�t�.
   */
  static public String proprieteToString(int _tpPrp) {
    String r = null;
    if (_tpPrp == PRPropriete.RUGOSITE) r = PrRUGO;
    else if (_tpPrp == PRPropriete.VISCOSITE) r = PrVISC;
    else if (_tpPrp == PRPropriete.ALPHA_LONGUEUR_MELANGE) r = PrALLM;
    else if (_tpPrp == PRPropriete.FROTTEMENT_PAROI) r = PrFTBD;
    else if (_tpPrp == PRPropriete.PERTE_CHARGE) r = PrPERT;
    else if (_tpPrp == PRPropriete.BASE) r = PrBASE;
    else if (_tpPrp == PRPropriete.NB_FCT) r = PrNBFT;
    else if (_tpPrp == PRPropriete.D50_GRAIN) r = PrD50;
    else if (_tpPrp == PRPropriete.D90_GRAIN) r = PrD90;
    return r;
  }

  /**
   * Retourne le type de propri�t� pour le string donn�.
   *
   * @param _string String donn�.
   */
  static public int stringToPropriete(String _string) {
    int r = 0;
    if (_string == PrRUGO) r = PRPropriete.RUGOSITE;
    else if (_string == PrVISC) r = PRPropriete.VISCOSITE;
    else if (_string == PrALLM) r = PRPropriete.ALPHA_LONGUEUR_MELANGE;
    else if (_string == PrFTBD) r = PRPropriete.FROTTEMENT_PAROI;
    else if (_string == PrPERT) r = PRPropriete.PERTE_CHARGE;
    else if (_string == PrBASE) r = PRPropriete.BASE;
    else if (_string == PrNBFT) r = PRPropriete.NB_FCT;
    else if (_string == PrD50) r = PRPropriete.D50_GRAIN;
    else if (_string == PrD90) r = PRPropriete.D90_GRAIN;
    return r;
  }

  /**
   * Retourne le string pour le type de condition donn�e.
   *
   * @param _tpCond Type de condition.
   * @return Le nom de la condition.
   */
  public static String conditionToString(int _tpCond) {
    String r = null;
    if (_tpCond == PRConditionLimite.VITESSE_X) r = ClVITX;
    else if (_tpCond == PRConditionLimite.VITESSE_Y) r = ClVITY;
    else if (_tpCond == PRConditionLimite.HAUTEUR) r = ClHT;
    else if (_tpCond == PRConditionLimite.CONCENTR) r = ClCONC;
    else if (_tpCond == PRConditionLimite.COTE_FOND) r = ClZF;
    return r;
  }

  /**
   * Retourne le string pour la sollicitation donn�e.
   *
   * @param _tpSol Type de sollicitation.
   * @return Le nom de la sollicitation.
   */
  public static String sollicitationToString(int _tpSol) {
    String r = null;
    if (_tpSol == PRSollicitation.DEBIT) r = SlQ;
    else if (_tpSol == PRSollicitation.DEBIT_SOLIDE) r = SlQS;
    else if (_tpSol == PRSollicitation.FLUX) r = SlFLUX;
    return r;
  }

  /**
   * Retourne les types de natures de fond pour le type de projet courant.
   *
   * @return Les types de natures.
   */
  public static int[] getTypesNaturesFond() {
    int[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
      r = new int[] { PRNature.FOND };
      break;
    case PRProjet.VIT_NU_LMG:
      r = new int[] { PRNature.FOND_LMG };
      break;
    case PRProjet.VIT_3D:
      r = new int[] { PRNature.FOND_3D };
      break;
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      r = new int[] { PRNature.FOND, PRNature.FOND_NON_ERODABLE };
      // r=new int[]{PRNature.FOND};
      break;
    }
    return r;
  }

  /**
   * Retourne les types de natures de bord pour le type de projet courant.
   *
   * @return Les types de natures.
   */
  public static int[] getTypesNaturesBord() {
    int[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
      r = new int[] { PRNature.BORD_OUVERT, PRNature.BORD_OUVERT_DEBIT, PRNature.BORD_OUVERT_VITESSE,
          PRNature.BORD_OUVERT_LIBRE, PRNature.BORD_FERME, PRNature.BORD_FERME_FROTTEMENT };
      break;
    case PRProjet.VIT_3D:
      r = new int[] { PRNature.BORD_OUVERT, PRNature.BORD_OUVERT_DEBIT, PRNature.BORD_OUVERT_VITESSE,
          PRNature.BORD_FERME, PRNature.BORD_FERME_FROTTEMENT };
      break;
    case PRProjet.TRANS_SUS_2D:
      r = new int[] { PRNature.BORD_OUVERT, PRNature.BORD_OUVERT_FLUX_CONC, PRNature.BORD_OUVERT_LIBRE,
          PRNature.BORD_FERME };
      break;
    case PRProjet.TRANS_TOT_2D:
      r = new int[] { PRNature.BORD_OUVERT, PRNature.BORD_OUVERT_DEBIT_SOL, PRNature.BORD_OUVERT_LIBRE,
          PRNature.BORD_FERME };
      break;
    }
    return r;
  }

  /**
   * Retourne les type de propri�t�s �l�mentaires par type de nature pour le type de projet courant.
   *
   * @param _tpNat Type de nature.
   * @return Les propri�t�s �l�mentaires
   */
  static public int[] getTypesProprietesElementaires(int _tpNat) {
    switch (_tpNat) {
    default:
      return new int[0];
    case PRNature.FOND:
      switch (RefluxResource.typeProjet) {
      default:
        return new int[] { PRPropriete.RUGOSITE, PRPropriete.VISCOSITE, PRPropriete.PERTE_CHARGE };
      case PRProjet.TRANS_SUS_2D:
      case PRProjet.TRANS_TOT_2D:
        return new int[] { PRPropriete.RUGOSITE, PRPropriete.D50_GRAIN, PRPropriete.D90_GRAIN };
      }
    case PRNature.FOND_LMG:
      return new int[] { PRPropriete.RUGOSITE, PRPropriete.VISCOSITE, PRPropriete.ALPHA_LONGUEUR_MELANGE,
          PRPropriete.PERTE_CHARGE };
    case PRNature.FOND_3D:
      return new int[] { PRPropriete.BASE, PRPropriete.NB_FCT };
    case PRNature.BORD_FERME_FROTTEMENT:
      switch (RefluxResource.typeProjet) {
      default:
        return new int[] { PRPropriete.FROTTEMENT_PAROI };
      case PRProjet.VIT_3D:
        return new int[0];
      }
    case PRNature.FOND_NON_ERODABLE:
      return new int[] { PRPropriete.RUGOSITE, PRPropriete.D50_GRAIN, PRPropriete.D90_GRAIN };
    }
  }

  /**
   * Retourne les composants de saisie de valeur des propri�t�s �l�mentaires du groupe. Le groupe est d�fini par le type
   * de nature.
   *
   * @param _tpNat Nature du groupe de propri�t�s.
   */
  public static PRPnSaisieValeurPE[] getComposantsSaisieValeurs(int _tpNat) {
    PRPnSaisieValeurPE[] r;
    int[] tpsPrp = getTypesProprietesElementaires(_tpNat);
    r = new PRPnSaisieValeurPE[tpsPrp.length];
    for (int i = 0; i < tpsPrp.length; i++) {
      r[i] = PRFabrique.getFabrique().creeCpSaisieValeurPE(tpsPrp[i]);
    }
    return r;
  }

  /**
   * Retourne le type de trait de repr�sentation de la nature donn�e.
   */
  static public int getTypeTrait(int _type) {
    int r = 0;
    if (_type == PRNature.BORD_FERME) r = TraceLigne.LISSE;
    else if (_type == PRNature.BORD_FERME_FROTTEMENT) r = TraceLigne.LISSE;
    else if (_type == PRNature.BORD_OUVERT) r = TraceLigne.TIRETE;
    else if (_type == PRNature.BORD_OUVERT_DEBIT) r = TraceLigne.TIRETE;
    else if (_type == PRNature.BORD_OUVERT_VITESSE) r = TraceLigne.TIRETE;
    else if (_type == PRNature.BORD_OUVERT_DEBIT_SOL) r = TraceLigne.TIRETE;
    else if (_type == PRNature.BORD_OUVERT_LIBRE) r = TraceLigne.POINTILLE;
    return r;
  }

  /**
   * Retourne les conditions limites possibles pour le projet courant.
   *
   * @return Les conditions limites.
   */
  public static int[] getTypesConditions() {
    int[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
    case PRProjet.VIT_3D:
      r = new int[] { PRConditionLimite.VITESSE_X, PRConditionLimite.VITESSE_Y, PRConditionLimite.HAUTEUR };
      break;
    case PRProjet.TRANS_SUS_2D:
      r = new int[] { PRConditionLimite.CONCENTR };
      break;
    case PRProjet.TRANS_TOT_2D:
      r = new int[] { PRConditionLimite.COTE_FOND };
      break;
    }
    return r;
  }

  /**
   * Retourne les sollicitations possibles pour le projet courant.
   *
   * @return Les sollicitations.
   */
  public static int[] getTypesSollicitations() {
    int[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
    case PRProjet.VIT_3D:
      r = new int[] { PRSollicitation.DEBIT };
      break;
    case PRProjet.TRANS_SUS_2D:
      r = new int[] { PRSollicitation.FLUX };
      break;
    case PRProjet.TRANS_TOT_2D:
      r = new int[] { PRSollicitation.DEBIT_SOLIDE };
      break;
    }
    return r;
  }

  /**
   * Retourne les solutions initiales possibles pour le projet courant.
   *
   * @return Les sollicitations.
   */
  public static int[] getTypesSolutionsInitiales() {
    int[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
    case PRProjet.VIT_3D:
      r = new int[] { PRConditionLimite.HAUTEUR };
      break;
    case PRProjet.TRANS_SUS_2D:
      r = new int[] { PRConditionLimite.CONCENTR };
      break;
    case PRProjet.TRANS_TOT_2D:
      // r=new int[] {PRConditionLimite.COTE_FOND};
      r = new int[0];
      break;
    }
    return r;
  }

  /**
   * Retourne les propri�t�s globales par d�faut pour le type de projet courant.
   *
   * @return Les propriet�s globales
   */
  public static PRPropriete[] getDefaultProprietesGlobales() {
    PRPropriete[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
      return new PRPropriete[0];
    case PRProjet.VIT_3D:
      r = new PRPropriete[7];
      for (int i = 0; i < r.length; i++) {
        r[i] = new PRPropriete(-1, UsineLib.findUsine().creeMesureEvolutionConstante()
        /* IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie()) */, new Object[0]);
      }
      return r;
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      IEvolutionConstante ec = null;
      r = new PRPropriete[8];
      ec = UsineLib.findUsine().creeMesureEvolutionConstante();
      ec.constante(1030);
      r[0] = new PRPropriete(-1, ec, new Object[0]);
      ec = UsineLib.findUsine().creeMesureEvolutionConstante();
      ec.constante(2665);
      r[1] = new PRPropriete(-1, ec, new Object[0]);
      ec = UsineLib.findUsine().creeMesureEvolutionConstante();
      r[2] = new PRPropriete(-1, ec, new Object[0]);
      ec = UsineLib.findUsine().creeMesureEvolutionConstante();
      r[3] = new PRPropriete(-1, ec, new Object[0]);
      ec = UsineLib.findUsine().creeMesureEvolutionConstante();
      r[4] = new PRPropriete(-1, ec, new Object[0]);
      ec = UsineLib.findUsine().creeMesureEvolutionConstante();
      r[5] = new PRPropriete(-1, ec, new Object[0]);
      ec = UsineLib.findUsine().creeMesureEvolutionConstante();
      r[6] = new PRPropriete(-1, ec, new Object[0]);
      ec = UsineLib.findUsine().creeMesureEvolutionConstante();
      ec.constante(1.e-6);
      r[7] = new PRPropriete(-1, ec, new Object[0]);
      return r;
    }
  }

  /**
   * Retourne parametres de calcul par d�faut pour le type de projet courant.
   *
   * @return Les parametres de calcul.
   */
  public static Object[] getDefaultParametresCalcul() {
    Object[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
      r = new Double[18];
      // Coefficients de pond�ration
      r[0] = new Double(1.0);
      r[1] = new Double(1.0);
      r[2] = new Double(1.0);
      r[3] = new Double(1.0);
      r[4] = new Double(1.0);
      r[5] = new Double(1.0);
      r[6] = new Double(0.0);
      r[7] = new Double(0.0);
      r[8] = new Double(0.0);
      // Indices d'impression
      r[9] = new Double(0);
      r[10] = new Double(0);
      r[11] = new Double(0);
      r[12] = new Double(0);
      r[13] = new Double(0);
      r[14] = new Double(0);
      r[15] = new Double(0);
      r[16] = new Double(0);
      r[17] = new Double(1);
      return r;
    case PRProjet.VIT_3D:
      r = new Double[11];
      // Options
      r[0] = new Double(0);
      r[1] = new Double(0);
      r[2] = new Double(0);
      r[3] = new Double(2);
      // G�n�ral
      r[4] = new Double(4);
      r[5] = new Double(0);
      r[6] = new Double(0);
      r[7] = new Double(0);
      r[8] = new Double(0);
      r[9] = new Double(0);
      r[10] = new Double(1);
      return r;
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      int nbParsCalcul = 0;
      final int CONVEC_ZF = nbParsCalcul++;
      final int CONVEC_C = nbParsCalcul++;
      final int DIFFUSION = nbParsCalcul++;
      final int DEBIT_REF = nbParsCalcul++;
      final int PROF_REF = nbParsCalcul++;
      final int CONC_REF = nbParsCalcul++;
      final int EFFET_PENTE = nbParsCalcul++;
      final int ND_ERODABLE = nbParsCalcul++;
      final int HMIN_CALCUL = nbParsCalcul++;
      final int FORMULE = nbParsCalcul++;
      final int PRJ_HOULE = nbParsCalcul++;
      final int CAS_CHAMP = nbParsCalcul++;
      final int CAS_MAREE = nbParsCalcul++;
      final int PERIODE = nbParsCalcul++;
      final int ICOURBE = nbParsCalcul++;
      final int PRJ_MAREE_45 = nbParsCalcul++;
      final int PRJ_MAREE_90 = nbParsCalcul++;
      final int T0_HYDRO = nbParsCalcul++;
      r = new Object[nbParsCalcul];
      // Options
      r[CONVEC_ZF] = new Integer(RefluxResource.typeProjet == PRProjet.TRANS_TOT_2D ? 1 : 0);
      r[CONVEC_C] = new Integer(RefluxResource.typeProjet == PRProjet.TRANS_SUS_2D ? 1 : 0);
      r[DIFFUSION] = new Integer(1);
      // G�n�ral
      r[DEBIT_REF] = new Double(0);
      r[PROF_REF] = new Double(0);
      r[CONC_REF] = new Double(0);
      r[EFFET_PENTE] = new Double(0);
      r[ND_ERODABLE] = new Double(0.1);
      r[HMIN_CALCUL] = new Double(0.1);
      r[FORMULE] = new Integer(0);
      r[PRJ_HOULE] = new File("");
      r[T0_HYDRO] = new Double(0);
      // Champ de courant
      r[CAS_CHAMP] = new Integer(0);
      r[CAS_MAREE] = new Integer(0);
      r[PERIODE] = new Double(0);
      r[ICOURBE] = new Integer(-1);
      r[PRJ_MAREE_45] = new File("");
      r[PRJ_MAREE_90] = new File("");
      return r;
    }
  }

  /**
   * Retourne le type de nature de fond par d�faut pour le projet courant.
   *
   * @return Le type.
   */
  public static int getDefautTypeFond() {
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
      return PRNature.FOND;
    case PRProjet.VIT_NU_LMG:
      return PRNature.FOND_LMG;
    case PRProjet.VIT_3D:
      return PRNature.FOND_3D;
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      return PRNature.FOND;
    }
  }

  /**
   * Retourne le type de nature de bord par d�faut pour le projet courant.
   *
   * @return Le type.
   */
  public static int getDefautTypeBord() {
    return PRNature.BORD_FERME;
  }

  /**
   * Retourne les types de sch�ma possibles pour le type de projet courant.
   *
   * @return Les types de sch�ma.
   */
  public static int[] getTypesSchema() {
    int[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
      r = new int[3];
      r[0] = PRSchemaResolution.STATIONNAIRE;
      r[1] = PRSchemaResolution.EULER;
      r[2] = PRSchemaResolution.KAWAHARA;
      return r;
    case PRProjet.VIT_3D:
      r = new int[3];
      r[0] = PRSchemaResolution.STATIONNAIRE;
      r[1] = PRSchemaResolution.LAX_WENDROFF;
      r[2] = PRSchemaResolution.EULER;
      return r;
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      r = new int[3];
      r[0] = PRSchemaResolution.STATIONNAIRE;
      r[1] = PRSchemaResolution.LAX_WENDROFF;
      r[2] = PRSchemaResolution.EULER;
      return r;
    }
  }

  /**
   * Retourne la valeur du coefficient par defaut pour le schema pour le type de projet courant.
   *
   * @param _type Le type de sch�ma.
   */
  public static double getDefaultCoefSchema(int _type) {
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      switch (_type) {
      default:
      case PRSchemaResolution.STATIONNAIRE:
        return 0;
      case PRSchemaResolution.EULER:
        return 0.5;
      case PRSchemaResolution.KAWAHARA:
        return 0.92;
      }
    case PRProjet.VIT_3D:
      return 0; // Le coefficient n'est pas utilis�.
    }
  }

  /**
   * Retourne les types de m�thodes possibles pour le type de projet courant. Les types sont retourn�s dans un ordre
   * quelconque.
   *
   * @return Les types de m�thodes.
   */
  public static int[] getTypesMethode() {
    int[] r;
    HashSet hTpsMeth = new HashSet();
    int[] tpsSch = getTypesSchema();
    // Integer[] iTpsMeth;
    for (int i = 0; i < tpsSch.length; i++) {
      int[] tpsMeth = getTypesMethode(tpsSch[i]);
      for (int j = 0; j < tpsMeth.length; j++) {
        hTpsMeth.add(new Integer(tpsMeth[j]));
      }
    }
    r = new int[hTpsMeth.size()];
    int k = 0;
    for (Iterator it = hTpsMeth.iterator(); it.hasNext();) {
      r[k++] = ((Integer) it.next()).intValue();
    }
    return r;
  }

  /**
   * Retourne les types de m�thodes possibles pour le sch�ma donn� pour le type de projet courant.
   *
   * @param _tpSch Type de sch�ma donn�.
   */
  public static int[] getTypesMethode(int _tpSch) {
    int[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
      switch (_tpSch) {
      default:
      case PRSchemaResolution.STATIONNAIRE:
        r = new int[] { PRMethodeResolution.LINEAIRE, PRMethodeResolution.NEWTON_RAPHSON,
            PRMethodeResolution.NEWTON_RAPHSON_BCD, PRMethodeResolution.SELECTED_LUMPING,
            PRMethodeResolution.SELECTED_LUMPING_BCD };
        return r;
      case PRSchemaResolution.EULER:
        r = new int[] { PRMethodeResolution.LINEAIRE, PRMethodeResolution.NEWTON_RAPHSON,
            PRMethodeResolution.NEWTON_RAPHSON_BCD };
        return r;
      case PRSchemaResolution.KAWAHARA:
        r = new int[] { PRMethodeResolution.SELECTED_LUMPING, PRMethodeResolution.SELECTED_LUMPING_BCD };
        return r;
      }
    case PRProjet.VIT_3D:
      switch (_tpSch) {
      default:
      case PRSchemaResolution.STATIONNAIRE:
        r = new int[] { PRMethodeResolution.NEWTON_RAPHSON };
        return r;
      case PRSchemaResolution.EULER:
        r = new int[] { PRMethodeResolution.NEWTON_RAPHSON };
        return r;
      case PRSchemaResolution.LAX_WENDROFF:
        r = new int[] { PRMethodeResolution.NEWTON_RAPHSON };
        return r;
      }
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      switch (_tpSch) {
      default:
      case PRSchemaResolution.STATIONNAIRE:
        r = new int[] { PRMethodeResolution.LINEAIRE, PRMethodeResolution.NEWTON_RAPHSON };
        return r;
      case PRSchemaResolution.EULER:
        r = new int[] { PRMethodeResolution.LINEAIRE, PRMethodeResolution.NEWTON_RAPHSON,
            PRMethodeResolution.LAX_WENDROFF };
        return r;
      case PRSchemaResolution.LAX_WENDROFF:
        r = new int[] { PRMethodeResolution.LINEAIRE, PRMethodeResolution.NEWTON_RAPHSON,
            PRMethodeResolution.LAX_WENDROFF };
        return r;
      }
    }
  }

  /**
   * Retourne la valeur des coefficients par defaut pour la m�thode pour le type de projet courant.
   *
   * @param _tpMeth Le type de m�thode.
   */
  public static double[] getDefaultCoefsMethode(int _tpMeth) {
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
      switch (_tpMeth) {
      default:
      case PRMethodeResolution.LINEAIRE:
      case PRMethodeResolution.SELECTED_LUMPING:
        return new double[] {};
      case PRMethodeResolution.NEWTON_RAPHSON:
        return new double[] { 1, 0.001, 5 };
      case PRMethodeResolution.NEWTON_RAPHSON_BCD:
        return new double[] { 1, 0.001, 0.0005, 5, 1000.0 };
      case PRMethodeResolution.SELECTED_LUMPING_BCD:
        return new double[] { 0.0005, 1000.0 };
      }
    case PRProjet.VIT_3D:
      switch (_tpMeth) {
      default:
      case PRMethodeResolution.NEWTON_RAPHSON:
        return new double[] { 0.001, 5 };
      }
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      switch (_tpMeth) {
      default:
      case PRMethodeResolution.LINEAIRE:
        return new double[] {};
      case PRMethodeResolution.NEWTON_RAPHSON:
      case PRMethodeResolution.LAX_WENDROFF:
        return new double[] { 1, 0.001, 5 };
      }
    }
  }

  /**
   * Retourne le nom des coefficients pour la m�thode pour le type de projet courant.
   *
   * @param _tpMeth Le type de m�thode.
   */
  public static String[] getNomsCoefsMethode(int _tpMeth) {
    // String[] r;
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
      switch (_tpMeth) {
      default:
      case PRMethodeResolution.LINEAIRE:
      case PRMethodeResolution.SELECTED_LUMPING:
        return new String[] {};
      case PRMethodeResolution.NEWTON_RAPHSON:
        return new String[] { CmRELA, CmPNR, CmNBIT };
      case PRMethodeResolution.NEWTON_RAPHSON_BCD:
        return new String[] { CmRELA, CmPNR, CmPBCD, CmNBIT, CmVISC };
      case PRMethodeResolution.SELECTED_LUMPING_BCD:
        return new String[] { CmPBCD, CmVISC };
      case PRMethodeResolution.LAX_WENDROFF:
        return new String[] { CmRELA, CmNBIT };
      }
    case PRProjet.VIT_3D:
      switch (_tpMeth) {
      default:
      case PRMethodeResolution.NEWTON_RAPHSON:
        return new String[] { CmPNR, CmNBIT };
      }
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      switch (_tpMeth) {
      default:
      case PRMethodeResolution.LINEAIRE:
        return new String[] {};
      case PRMethodeResolution.NEWTON_RAPHSON:
      case PRMethodeResolution.LAX_WENDROFF:
        return new String[] { CmRELA, CmPNR, CmNBIT };
      }
    }
  }

  /**
   * Retourne le nombre maxi de groupes de pas qui peuvent �tre cr��s pour le type de projet courant.
   *
   * @return Le nombre max.
   */
  public static int getNbMaxGroupesPT() {
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      return Integer.MAX_VALUE;
    case PRProjet.VIT_3D:
      return 1;
    }
  }

  /**
   * Le calcul transitoire peut-il d�marrer � une valeur sup�rieur � t0>0 ?. Autrement dit, peut-on �diter la valeur de
   * t0 ?
   *
   * @return <i>true</i> La valeur est �ditable. <i>false</i>t0 vaut toujours 0.
   */
  public static boolean estT0Editable() {
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      return true;
    case PRProjet.VIT_3D:
      return false;
    }
  }

  /**
   * Le plan de r�f�rence peut-il �tre utilis� pour d�finir les solutions initiales ?
   *
   * @return <i>true</i> Le plan de r�f�rence est op�rationnel. <i>false</i> Le plan de r�f�rence n'est pas
   *         op�rationnel.
   */
  public static boolean estPlanReferenceOK() {
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
    case PRProjet.VIT_3D:
      return true;
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      return false;
    }
  }

  /**
   * Retourne les extensions possibles de fichiers de r�sultats pour le type de projet.
   */
  public static String[] getExtFichiersResultats() {
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
      return new String[] { "sov", "sol" };
    case PRProjet.VIT_3D:
      return new String[] { "vno" };
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      return new String[] { "sot" };
    }
  }

  /**
   * Retourne les extensions possibles de fichiers de donn�es pour le type de projet.
   */
  public static String[] getExtFichiersDonnees() {
    switch (RefluxResource.typeProjet) {
    default:
    case PRProjet.VIT_NU_CON:
    case PRProjet.VIT_NU_LMG:
      return new String[] { "inp", "siv", "clv", "pnv" };
    case PRProjet.VIT_3D:
      return new String[] { "inp", "ini", "cl" };
    case PRProjet.TRANS_SUS_2D:
    case PRProjet.TRANS_TOT_2D:
      return new String[] { "inp", "sit", "clt", "pnt", "phy" };
    }
  }
}
