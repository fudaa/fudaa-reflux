/*
 * @file         HY3dProjet.java
 * @creation     2001-01-08
 * @modification $Date: 2007-01-19 13:14:34 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d3;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import com.memoire.bu.BuDialogError;

import org.fudaa.dodico.corba.mesure.IEvolution;
import org.fudaa.dodico.corba.mesure.IEvolutionConstante;
import org.fudaa.dodico.corba.mesure.IEvolutionSerieIrreguliere;
import org.fudaa.dodico.corba.planification.ISegmentationTemps;

import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.objet.UsineLib;

import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;

import org.fudaa.fudaa.commun.conversion.FudaaMaillageCORELEBTH;
import org.fudaa.fudaa.reflux.*;
/**
 * Un projet Reflux 3D courantologie.
 *
 * @version      $Revision: 1.12 $ $Date: 2007-01-19 13:14:34 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class HY3dProjet extends PRProjet {
  /**
   * Le serveur de calcul associ� au projet.
   */
  private static HY3dInterfaceCalcul interface_= null;
  /**
   * Initialisation du projet depuis les fichiers .bth/.cor/.ele./[.crb] de
   * nom sp�cifi�.
   *
   * @param _racine Le nom de la racine des fichiers sans extension.
   * @param _params Param�tres de cr�ation du projet. Non utilis� dans ce type
   *                de projet.
   *
   * @exception FileNotFoundException Un fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   */
  public void initialiser(String _racine, Object[] _params)
    throws IOException {
    RefluxMaillage maillage;
    // Racine des fichiers
    racineFichiers(_racine);
    // Lecture du maillage
    maillage= lireMaillage(_racine);
    maillage(maillage);
    // Tentative de lecture du fichier des courbes s'il y en a un
    try {
      lireFichierCRB(_racine + ".crb");
    } catch (FileNotFoundException _exc) {}
    // Points d�finissant le plan des solutions initiales
//    {
//      GrBoite boite= new GrBoite();
//      GrNoeud[] noeuds= maillage().noeuds();
//      GrPoint[] ptsSI= new GrPoint[3];
//      GrPoint pmin;
//      GrPoint pmax;
//      for (int i= 0; i < noeuds.length; i++)
//        boite.ajuste(noeuds[i].point);
//      pmin= boite.o;
//      pmax= boite.e;
//      ptsSI[0]= new GrPoint(pmin.x, pmin.y, pmax.z + 0.2);
//      ptsSI[1]= new GrPoint(pmax.x, pmin.y, pmax.z + 0.2);
//      ptsSI[2]= new GrPoint(pmax.x, pmax.y, pmax.z + 0.2);
//      modeleCalcul().pointsSI(ptsSI);
//    }
//    // Solutions initiales (a partir du plan)
//    initialiserSolutionsInitiales();
    {
      GrNoeud[] noeuds= maillage().noeuds();
      double z=Double.NEGATIVE_INFINITY;
      for (int i= 0; i < noeuds.length; i++) z=Math.max(z,noeuds[i].point_.z_);
      initialiserSolutionsInitiales(new double[]{z+0.2});
    }
    // Normales en chaque noeuds des contours
    initialiserNormales();
    //Nature des bords
    initialiserNatureBords();
    //Nature des fonds
    initialiserNatureFonds();
  }
  /**
   * Lecture d'un maillage depuis les fichiers noeuds, bathy et �l�ments de
   * maillage.
   * @param _fichier Nom du fichier maillage. Le maillage est contenu
   *                 dans les _fichier.cor, _fichier.bth et _fichier.ele
   * @exception FileNotFoundException Un fichier de maillage n'est pas trouv�
   * @exception IOException Une erreur de lecture s'est produite
   * @return L'objet maillage
   */
  public static RefluxMaillage lireMaillage(String _fichier)
    throws IOException {
    RefluxMaillage r= null;
    try {
      GrMaillageElement mail= FudaaMaillageCORELEBTH.lire(new File(_fichier));
      r= new RefluxMaillage(mail.elements(), mail.noeuds());
    } catch (FileNotFoundException _exc) {
      throw new FileNotFoundException(
        "Erreur d'ouverture de " + _exc.getMessage());
    } catch (IOException _exc) {
      throw new IOException("Erreur de lecture sur " + _exc.getMessage());
    }
    // Controle des �lements lus. Ils doivent tous etre T6.
    GrElement[] els= r.elements();
    for (int i= 0; i < els.length; i++)
      if (els[i].type_ != GrElement.T6)
        throw new IOException("Seuls les �l�ments T6 sont autoris�s pour ce type de projet");
    return r;
  }
  /**
   * Lecture des informations contenues dans le fichier .crb des courbes transitoires
   * de nom specifie et stockage dans les objets IEvolutionSerieIrreguliere
   *
   * @param _nomFichier Nom du fichier a lire
   * @exception FileNotFoundException Le fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   */
  private void lireFichierCRB(String _nomFichier)
    throws FileNotFoundException, IOException {
    int[] fmt;
    String nomCourbe; // Nom de courbe
    int nbPoints; // Nombre de points de courbe
    long[] x; // Abscisses de la courbe
    double[] y; // Ordonn�es de la courbe
    IEvolutionSerieIrreguliere evolution; // Evolution en cours de cr�ation
    IEvolutionSerieIrreguliere[] evolutions; // Evolutions lues
    Vector evolTmp; // Vecteur temporaire des �volutions
    FortranReader file= null;
    try {
      // Ouverture du fichier
      file= new FortranReader(new FileReader(_nomFichier));
      // Num�ro de version (seules les versions > 4.0 sont autoris�es)
      fmt= new int[] { 12, 10 };
      file.readFields(fmt);
      if (file.stringField(1).compareTo("4.0") < 0) {
        throw new Exception(
          "Erreur de lecture sur "
            + _nomFichier
            + "\nLe format du fichier est de version"
            + file.stringField(1)
            + ".\nSeules les versions > � 4.0 sont autoris�es");
      }
      // Boucle jusqu'� fin de fichier
      evolTmp= new Vector();
      while (file.ready()) {
        fmt= new int[] { 20, 1, 5 };
        try {
          file.readFields(fmt);
        } catch (EOFException exc) {
          break;
        }
        if (file.stringField(0).substring(0, 1).equals("*"))
          continue;
        // Nom de la courbe
        nomCourbe= file.stringField(0);
        // Nombre de points de la courbe
        nbPoints= file.intField(2);
        x= new long[nbPoints];
        y= new double[nbPoints];
        // Boucle sur les points
        fmt= new int[] { 10, 1, 10 };
        for (int i= 0; i < nbPoints;) {
          file.readFields(fmt);
          if (file.stringField(0).substring(0, 1).equals("*"))
            continue;
          x[i]= (long)file.doubleField(0);
          y[i]= file.doubleField(2);
          i++;
        }
        // Cr�ation de l'�volution s�rie irr�guli�re
        evolution=UsineLib.findUsine().creeMesureEvolutionSerieIrreguliere();
/*          IEvolutionSerieIrreguliereHelper.narrow(
            new DEvolutionSerieIrreguliere().tie());*/
        ISegmentationTemps st=UsineLib.findUsine().creePlanificationSegmentationTemps();
/*          ISegmentationTempsHelper.narrow(new DSegmentationTemps().tie());*/
        st.modifieInstants(x);
        evolution.instants(st);
        evolution.serie(y);
        associe(evolution, nomCourbe);
        evolTmp.addElement(evolution);
      }
    } catch (FileNotFoundException exc) {
      throw new FileNotFoundException(_nomFichier);
    } catch (IOException exc) {
      throw new IOException("Erreur de lecture sur " + _nomFichier);
    } catch (NumberFormatException exc) {
      throw new IOException("Erreur de lecture sur " + _nomFichier);
    } catch (Exception _exc) {
      throw new IOException(_exc.getMessage());
    } finally {
      // Fermeture du fichier
      if (file != null)
        file.close();
    }
    evolutions= new IEvolutionSerieIrreguliere[evolTmp.size()];
    evolTmp.copyInto(evolutions);
    evolutions(evolutions);
  }
  /**
   * Chargement du projet depuis les fichiers .pre/.bth/.cor/.ele./[.crb]
   * @param _racine Le nom de la racine des fichiers sans extension.
   */
  public void charger(String _racine) throws IOException {
    Reader rf= null;
    FortranReader file= null;
    int ival;
    String nmFcPre= _racine + ".pre";
    RefluxMaillage maillage;
    GrElement[] els;
    GrNoeud[] nds;
    GrNoeud[][] ndsBds;
    GrElement[][] elsBds;
    // Racine des fichiers
    racineFichiers(_racine);
    // Lecture du maillage
    maillage= lireMaillage(_racine);
    maillage(maillage);
    // Lecture du fichier .crb (s'il existe)
    try {
      lireFichierCRB(_racine + ".crb");
    }
    // FileNotFoundException => On controlera + tard la coh�rence .pre/.crb
    catch (FileNotFoundException _exc) {}
    els= maillage().elements();
    nds= maillage().noeuds();
    elsBds= maillage().aretesContours();
    ndsBds= maillage().noeudsContours();
    // Correspondances
    // Table Noeud->Arete
    Hashtable hnd2Bd= new Hashtable();
    for (int i= 0; i < elsBds.length; i++)
      for (int j= 0; j < elsBds[i].length; j++)
        hnd2Bd.put(elsBds[i][j].noeuds_[0], elsBds[i][j]);
    // Table Num�ro->Element
    Hashtable hnum2El=new Hashtable();
    for (int i=0; i<els.length; i++) hnum2El.put(new Integer(i),els[i]);
    //--------------------------------------------------------------------------
    //--- Lecture des donn�es  -------------------------------------------------
    //--------------------------------------------------------------------------
    try {
      // Ouverture du fichier
      file= new FortranReader(rf= new FileReader(nmFcPre));
      file.setBlankZero(true);
      // Num�ro de version (pas utilis� pour l'instant)
      file.readFields(new int[]{ 12, 10 });
      String version=file.stringField(1);
      System.out.println(nmFcPre+" version : "+version);
      // Type de probleme => Doit �tre=3
      file.readFields();
      ival= file.intField(0);
      if (ival != 3)
        throw new Exception(
          "Erreur de lecture sur "
            + nmFcPre
            + "\nLe probl�me n'est pas de type courantologique 3D.");
      //------------------------------------------------------------------------
      // Solutions initiales
      //------------------------------------------------------------------------
      GrPoint[] ptsSI= new GrPoint[3];
      PRSolutionsInitiales[] sis= new PRSolutionsInitiales[nds.length];
      int[] numGrpElements=null;
      int[][] numElements=null;
      int[] numMultiplans=null;
      int[] numMultiplansGrp=null;
      int[][] numMultiplansLignes=null;
      double[][][] p1MultiplansLignes=null;
      double[][][] p2MultiplansLignes=null;
      int[][] autoMultiplansLignes=null;
      double[][] valMultiplansLignes=null; // Si auto, num plan associ�.

      if (version.compareTo("5.15")<0) {
        // Commentaire
        file.readFields();
        // Points de d�finition de la surface.
        for (int i = 0; i < 3; i++) {
          ptsSI[i] = new GrPoint();
          file.readFields();
          ptsSI[i].x_ = file.doubleField(0);
          ptsSI[i].y_ = file.doubleField(1);
          ptsSI[i].z_ = file.doubleField(2);
        }

        // 1 groupe pour tous les �lements, 1 multiplan recalcul�.

        numGrpElements=new int[]{1};
        numElements=new int[1][els.length];
        for (int i=0; i<els.length; i++) numElements[0][i]=i+1;

        numMultiplans=new int[]{1};
        numMultiplansGrp=new int[]{1};
        numMultiplansLignes=new int[][]{{1,2}};
        p1MultiplansLignes=new double[1][2][2];
        p2MultiplansLignes=new double[1][2][2];
        autoMultiplansLignes=new int[][]{{0,0}};
        valMultiplansLignes=new double[1][2];

        // Calcul des points du plan pour en avoir 2 � m�me h.
        int imil=-1;
        int imax=-1;
        int imin=-1;
        boolean bcal=false;
        if (ptsSI[0].z_==ptsSI[1].z_) { imil=2; imin=0; imax=1; }
        if (ptsSI[1].z_==ptsSI[2].z_) { imil=0; imin=1; imax=2; }
        if (ptsSI[2].z_==ptsSI[0].z_) { imil=1; imin=2; imax=0; }
        if (ptsSI[0].z_>ptsSI[1].z_ && ptsSI[1].z_>ptsSI[2].z_) { imil=1; imin=2; imax=0; bcal=true; }
        if (ptsSI[1].z_>ptsSI[2].z_ && ptsSI[2].z_>ptsSI[0].z_) { imil=2; imin=0; imax=1; bcal=true; }
        if (ptsSI[2].z_>ptsSI[0].z_ && ptsSI[0].z_>ptsSI[1].z_) { imil=0; imin=1; imax=2; bcal=true; }
        if (ptsSI[0].z_<ptsSI[1].z_ && ptsSI[1].z_<ptsSI[2].z_) { imil=1; imin=0; imax=2; bcal=true; }
        if (ptsSI[1].z_<ptsSI[2].z_ && ptsSI[2].z_<ptsSI[0].z_) { imil=2; imin=1; imax=0; bcal=true; }
        if (ptsSI[2].z_<ptsSI[0].z_ && ptsSI[0].z_<ptsSI[1].z_) { imil=0; imin=2; imax=1; bcal=true; }

        if (bcal) { // Calcul de la position du point �quivalent au z imil.
          double x=ptsSI[imin].x_+(ptsSI[imil].z_-ptsSI[imin].z_)*(ptsSI[imax].x_-ptsSI[imin].x_)/(ptsSI[imax].z_-ptsSI[imin].z_);
          double y=ptsSI[imin].y_+(ptsSI[imil].z_-ptsSI[imin].z_)*(ptsSI[imax].y_-ptsSI[imin].y_)/(ptsSI[imax].z_-ptsSI[imin].z_);
          double z=ptsSI[imil].z_;
          p1MultiplansLignes[0][0][0]=ptsSI[imil].x_;
          p1MultiplansLignes[0][0][1]=ptsSI[imil].y_;
          p2MultiplansLignes[0][0][0]=x;
          p2MultiplansLignes[0][0][1]=y;
          valMultiplansLignes[0][0]=z;

          if (z>(ptsSI[imin].z_+ptsSI[imax].z_)/2) imil=imin;
          else                                   imil=imax;
        }
        else {
          p1MultiplansLignes[0][0][0]=ptsSI[imin].x_;
          p1MultiplansLignes[0][0][1]=ptsSI[imin].y_;
          p2MultiplansLignes[0][0][0]=ptsSI[imax].x_;
          p2MultiplansLignes[0][0][1]=ptsSI[imax].y_;
          valMultiplansLignes[0][0]=ptsSI[imin].z_;
        }

        // Les 2 points formant la 2ieme ligne sont confondus.
        p1MultiplansLignes[0][1][0]=ptsSI[imil].x_;
        p1MultiplansLignes[0][1][1]=ptsSI[imil].y_;
        p2MultiplansLignes[0][1][0]=ptsSI[imil].x_;
        p2MultiplansLignes[0][1][1]=ptsSI[imil].y_;
        valMultiplansLignes[0][1]=ptsSI[imil].z_;
      }

      // Commentaire
      file.readFields();
      // VXi,VYi,VZi,HT en chaque noeud
      for (int i= 0; i < sis.length; i++) {
        file.readFields();
        int nbFields= file.getNumberOfFields();
        double[] vals= new double[nbFields - 1];
        //        double[] u=new double[(nbFields-2)/3];
        //        double[] v=new double[(nbFields-2)/3];
        //        double[] w=new double[(nbFields-2)/3];
        //        double h;
        ival= file.intField(0);
        for (int j= 0; j < vals.length; j++)
          vals[j]= file.doubleField(j + 1);
        //        for (int j=0; j<u.length; j++) u[j]=file.doubleField(j+1);
        //        for (int j=0; j<v.length; j++) v[j]=file.doubleField(j+1+u.length);
        //        for (int j=0; j<w.length; j++) w[j]=file.doubleField(j+1+2*u.length);
        //        h=file.doubleField(3*u.length+1);
        sis[i]= new PRSolutionsInitiales(nds[ival], vals);
        //        sis[i]=new PRSolutionsInitiales(nds[ival],u,v,w,h);
      }
      modeleCalcul().solutionsInitiales(sis);
      //------------------------------------------------------------------------
      // Mod�le de propri�t�s
      //------------------------------------------------------------------------
      // Propri�t�s de bord.
      int[] tpsBds;
      PRPropriete[][] prpsBds;
      Vector vnatBds= new Vector();
      Vector vprpsBds= new Vector();
      // Commentaire
      file.readFields();
      // Nombre de groupes de propri�t�s d'ar�tes
      file.readFields();
      ival= file.intField(0);
      // Groupes de propri�t�s d'ar�tes => Type des bords et propri�t�s
      // d'ar�tes
      tpsBds= new int[ival];
      prpsBds= new PRPropriete[ival][];
      for (int i= 0; i < tpsBds.length; i++) {
        file.readFields();
        tpsBds[i]= file.intField(1);
        int[] tpsPrps= Definitions.getTypesProprietesElementaires(tpsBds[i]);
        prpsBds[i]= new PRPropriete[tpsPrps.length];
        for (int j= 0; j < tpsPrps.length; j++) {
          int code= file.intField(j * 2 + 2);
          double val= file.doubleField(j * 2 + 3);
          IEvolution crb;
          if (code == 0)
            continue;
          else if (code == 1) {
            crb=UsineLib.findUsine().creeMesureEvolutionConstante();
/*              IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
            ((IEvolutionConstante)crb).constante(val);
          } else {
            crb= evolutions()[(int)val];
          }
          prpsBds[i][j]= new PRPropriete(tpsPrps[j], crb, new Vector());
          vprpsBds.add(prpsBds[i][j]);
        }
      }
      // Commentaire
      file.readFields();
      // Num�ro de groupe de propri�t�s d'ar�tes par noeud de bord.
      // => Cr�ation des natures de bord et affectation des propri�t�s d'aretes.
      for (int i= 0; i < elsBds.length; i++) {
        for (int j= 0; j < elsBds[i].length; j++) {
          int iNd;
          int iGrp;
          GrElement elBd;
          file.readFields();
          iNd= file.intField(0);
          iGrp= file.intField(1);
          elBd= (GrElement)hnd2Bd.get(nds[iNd]);
          vnatBds.add(new PRNature(tpsBds[iGrp], new Object[] { elBd }));
          for (int k= 0; k < prpsBds[iGrp].length; k++)
            if (prpsBds[iGrp][k] != null)
              prpsBds[iGrp][k].supports().add(elBd);
        }
      }
      mdlPrp_.naturesBords(vnatBds);
      mdlPrp_.proprietesAretes(
        (PRPropriete[])vprpsBds.toArray(new PRPropriete[0]));
      // Propri�t�s de fond.
      int[] tpsFds;
      PRPropriete[][] prpsFds;
      Vector vnatFds= new Vector();
      Vector vprpsFds= new Vector();
      // Commentaire
      file.readFields();
      // Nombre de groupes de propri�t�s de fond
      file.readFields();
      ival= file.intField(0);
      // Groupes de propri�t�s de fond => Type des fonds et propri�t�s
      // d'ar�tes
      tpsFds= new int[ival];
      prpsFds= new PRPropriete[ival][];
      for (int i= 0; i < tpsFds.length; i++) {
        file.readFields();
        tpsFds[i]= file.intField(1);
        int[] tpsPrps= Definitions.getTypesProprietesElementaires(tpsFds[i]);
        prpsFds[i]= new PRPropriete[tpsPrps.length];
        for (int j= 0; j < tpsPrps.length; j++) {
          int code= file.intField(j * 2 + 2);
          double val= file.doubleField(j * 2 + 3);
          IEvolution crb;
          if (code == 0)
            continue;
          else if (code == 1) {
            crb=UsineLib.findUsine().creeMesureEvolutionConstante();
/*              IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
            ((IEvolutionConstante)crb).constante(val);
          } else {
            crb= evolutions()[(int)val];
          }
          prpsFds[i][j]= new PRPropriete(tpsPrps[j], crb, new Vector());
          vprpsFds.add(prpsFds[i][j]);
        }
      }
      // Commentaire
      file.readFields();
      // Num�ro de groupe de propri�t�s de fond par �lement.
      // => Cr�ation des natures de fond et affectation des propri�t�s de fond.
      for (int i= 0; i < els.length; i++) {
        int iEl;
        int iGrp;
        file.readFields();
        iEl= file.intField(0);
        iGrp= file.intField(1);
        vnatFds.add(new PRNature(tpsFds[iGrp], new Object[] { els[iEl] }));
        for (int k= 0; k < prpsFds[iGrp].length; k++)
          if (prpsFds[iGrp][k] != null)
            prpsFds[iGrp][k].supports().add(els[iEl]);
      }
      mdlPrp_.naturesFonds(vnatFds);
      mdlPrp_.proprietesElements(
        (PRPropriete[])vprpsFds.toArray(new PRPropriete[0]));
      // Propri�t�s globales
      PRPropriete[] prpsGlobs= mdlPrp_.proprietesGlobales();
      // Commentaire
      file.readFields();
      file.readFields();
      for (int i= 0; i < prpsGlobs.length; i++) {
        double val= file.doubleField(i);
        ((IEvolutionConstante)prpsGlobs[i].evolution()).constante(val);
      }
      //------------------------------------------------------------------------
      // Conditions aux limites
      //------------------------------------------------------------------------
      PRConditionLimite[] cls;
      // Commentaire
      file.readFields();
      // Nombre de conditions aux limites
      file.readFields();
      ival= file.intField(0);
      cls= new PRConditionLimite[ival];
      // Conditions aux limites
      for (int i= 0; i < cls.length; i++) {
        int type;
        int code;
        double val;
        int nbNds;
        Vector vnds= new Vector();
        IEvolution crb;
        file.readFields();
        type= file.intField(0);
        code= file.intField(1);
        val= file.doubleField(2);
        nbNds= file.intField(3);
        if (code == 0)
          continue;
        else if (code == 1) {
          crb=UsineLib.findUsine().creeMesureEvolutionConstante();
/*            IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
          ((IEvolutionConstante)crb).constante(val);
        } else {
          crb= evolutions()[(int)val];
        }
        cls[i]= new PRConditionLimite(type, crb, vnds);
        for (int j= 0; j < nbNds; j++) {
          int iNd;
          file.readFields();
          iNd= file.intField(0);
          vnds.add(nds[iNd]);
        }
      }
      conditionsLimites(cls);
      //------------------------------------------------------------------------
      // Sollicitations
      //------------------------------------------------------------------------
      PRSollicitation[] sos;
      // Commentaire
      file.readFields();
      // Nombre de sollicitations
      file.readFields();
      ival= file.intField(0);
      sos= new PRSollicitation[ival];
      // Sollicitations
      for (int i= 0; i < sos.length; i++) {
        int type;
        int code;
        double val;
        int nbNds;
        Vector vnds= new Vector();
        IEvolution crb;
        file.readFields();
        type= file.intField(0);
        code= file.intField(1);
        val= file.doubleField(2);
        nbNds= file.intField(3);
        if (code == 0)
          continue;
        else if (code == 1) {
          crb=UsineLib.findUsine().creeMesureEvolutionConstante();
/*            IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
          ((IEvolutionConstante)crb).constante(val);
        } else {
          crb= evolutions()[(int)val];
        }
        sos[i]= new PRSollicitation(type, crb, vnds);
        for (int j= 0; j < nbNds; j++) {
          int iNd;
          file.readFields();
          iNd= file.intField(0);
          vnds.add(nds[iNd]);
        }
      }
      sollicitations(sos);
      //------------------------------------------------------------------------
      // Normales
      //------------------------------------------------------------------------
      Vector vnormales= new Vector();
      // Commentaire
      file.readFields();
      for (int i= 0; i < ndsBds.length; i++) {
        for (int j= 0; j < ndsBds[i].length; j++) {
          int iNd;
          double val;
          file.readFields();
          iNd= file.intField(0);
          val= file.doubleField(1);
          vnormales.add(new PRNormale(nds[iNd], val));
        }
      }
      mdlPrp_.normales((PRNormale[])vnormales.toArray(new PRNormale[0]));
      //------------------------------------------------------------------------
      // Mod�le de calcul
      //------------------------------------------------------------------------
      PRGroupePT[] grpPTs;
      Object[] paramsCal;
      // Groupes de pas de temps
      // Commentaire
      file.readFields();
      // Nombre de groupes de pas de temps
      file.readFields();
      ival= file.intField(0);
      grpPTs= new PRGroupePT[ival];
      // Groupes de pas de temps
      for (int i= 0; i < grpPTs.length; i++) {
        double deb;
        double fin;
        int nbPas;
        int tpSch;
        double coefSch;
        int tpMth;
        double[] coefsMth;
        int freq;
        file.readFields();
        deb= file.doubleField(0);
        fin= file.doubleField(1);
        nbPas= file.intField(2);
        PRBoucleTemps tps= new PRBoucleTemps(deb, fin, nbPas);
        tpSch= file.intField(3);
        coefSch= file.doubleField(4);
        PRSchemaResolution sch= new PRSchemaResolution(tpSch, coefSch);
        tpMth= file.intField(5);
        PRMethodeResolution mth= new PRMethodeResolution(tpMth);
        coefsMth= mth.coefficients();
        for (int j= 0; j < coefsMth.length; j++)
          coefsMth[j]= file.doubleField(6 + j);
        freq= file.intField(6 + coefsMth.length);
        grpPTs[i]= new PRGroupePT(tps, sch, mth, freq);
      }
      mdlCal_.groupesPT(grpPTs);
      // Commentaire
      file.readFields();
      // Parametres du calcul
      paramsCal= mdlCal_.parametresCalcul();
      file.readFields();
      for (int i= 0; i < paramsCal.length; i++)
        paramsCal[i]= new Double(file.doubleField(i));

      if (version.compareTo("5.15")>=0) {
        // Commentaire
        file.readFields();
        // Nombre de groupes d'�l�ments
        file.readFields();
        numGrpElements=new int[file.intField(0)];
        numElements=new int[numGrpElements.length][];
        // Num�ros des �l�ments (16 par ligne)
        for (int i=0; i<numGrpElements.length; i++) {
          file.readFields();
          numGrpElements[i]=file.intField(0);
          numElements[i]=new int[file.intField(1)];
          int j=0;
          while (j<numElements[i].length) {
            if (j%16==0) file.readFields();
            numElements[i][j]=file.intField(j%16);
            j++;
          }
        }

        // Commentaire
        file.readFields();
        // Nombre de multiplans
        file.readFields();
        numMultiplans=new int[file.intField(0)];
        numMultiplansGrp=new int[file.intField(0)];
        numMultiplansLignes=new int[file.intField(0)][];
        p1MultiplansLignes=new double[file.intField(0)][][];
        p2MultiplansLignes=new double[file.intField(0)][][];
        autoMultiplansLignes=new int[file.intField(0)][];
        valMultiplansLignes=new double[file.intField(0)][];
        for (int i=0; i<numMultiplans.length; i++) {
          file.readFields();
          numMultiplans[i]=file.intField(0);
          numMultiplansLignes[i]=new int[file.intField(1)];
          p1MultiplansLignes[i]=new double[file.intField(1)][2];
          p2MultiplansLignes[i]=new double[file.intField(1)][2];
          autoMultiplansLignes[i]=new int[file.intField(1)];
          valMultiplansLignes[i]=new double[file.intField(1)];
          numMultiplansGrp[i]=file.intField(2);
          for (int j=0; j<numMultiplansLignes[i].length; j++) {
            file.readFields();
            numMultiplansLignes[i][j]=file.intField(0);
            p1MultiplansLignes[i][j][0]=file.doubleField(1);
            p1MultiplansLignes[i][j][1]=file.doubleField(2);
            p2MultiplansLignes[i][j][0]=file.doubleField(3);
            p2MultiplansLignes[i][j][1]=file.doubleField(4);
            autoMultiplansLignes[i][j]=file.intField(5);
            valMultiplansLignes[i][j]=file.doubleField(6);
          }
        }
      }

      //--------------------------------------------------------------------------
      // Groupes d'�l�ments pour multiplans
      //--------------------------------------------------------------------------
      getGroupes().clear();
      for (int i=0; i<numGrpElements.length; i++) {
        Object[] elsGrp=new Object[numElements[i].length];
        for (int j=0; j<numElements[i].length; j++) {
          elsGrp[j]=(GrElement)hnum2El.get(new Integer(numElements[i][j]-1));
        }
        RefluxGroupe gp=new RefluxGroupe(elsGrp);
        gp.setNum(numGrpElements[i]);
        addGroupe(gp);
      }

      //--------------------------------------------------------------------------
      // Multiplans
      //--------------------------------------------------------------------------
      {
        getPlans().clear();
        getSegments().clear();
        for (int i=0; i<numMultiplans.length; i++) {
          RefluxSegment[] sgs=new RefluxSegment[numMultiplansLignes[i].length];
          for (int j=0; j<numMultiplansLignes[i].length; j++) {
            sgs[j]=new RefluxSegment(p1MultiplansLignes[i][j],p2MultiplansLignes[i][j]);
            sgs[j].setNum(numMultiplansLignes[i][j]);
            sgs[j].setHAutomatique(autoMultiplansLignes[i][j]==1);
            sgs[j].setHauteur(valMultiplansLignes[i][j]);
            addSegment(sgs[j]);
          }
          RefluxPlan pl=new RefluxPlan(sgs);
          pl.setNum(numMultiplans[i]);
          for (int j=0; j<groupes_.size(); j++) {
            RefluxGroupe gp=(RefluxGroupe)groupes_.get(j);
            if (gp.getNum()==numMultiplansGrp[i]) {
              pl.setGroupe(gp);
              break;
            }
          }
          addPlan(pl);
        }

        // Mise a jour des plans de d�pendance.
        Vector plans=getPlans();
        for (int i=0; i<plans.size(); i++) {
          RefluxPlan pl=(RefluxPlan)plans.get(i);
          if (pl.isHAutomatiqueAmont()) {
            int numpl=(int)pl.getSegments()[0].getHauteur();
            for (int j=0; j<plans.size(); j++) {
              RefluxPlan pldep=(RefluxPlan)plans.get(j);
              if (pldep.getNum()==numpl) {
                pl.setPlanAmont(pldep);
                break;
              }
            }
          }
          if (pl.isHAutomatiqueAval()) {
            int numpl=(int)pl.getSegments()[pl.getSegments().length-1].getHauteur();
            for (int j=0; j<plans.size(); j++) {
              RefluxPlan pldep=(RefluxPlan)plans.get(j);
              if (pldep.getNum()==numpl) {
                pl.setPlanAval(pldep);
                break;
              }
            }
          }
        }
      }


    } catch (NumberFormatException _exc) {
      _exc.printStackTrace();
      throw new IOException(
        "Erreur de lecture sur " + nmFcPre + " ligne " + file.getLineNumber());
    } catch (FileNotFoundException _exc) {
      throw new IOException("Erreur d'ouverture de " + nmFcPre);
    } catch (IOException _exc) {
      throw new IOException(
        "Erreur de lecture sur " + nmFcPre + " ligne " + file.getLineNumber());
    } catch (Exception _exc) {
      throw new IOException(_exc.getMessage());
    } finally {
      if (rf != null)
        rf.close();
    }
  }
  /**
   * Enregistrement du projet sur fichier .pre
   * @param _nomFichier Nom du fichier de sauvegarde.
   */
  public void enregistrer(String _nomFichier) throws IOException {
    class GroupePE {
      int type;
      int[] codes;
      double[] vals;
    }
    GrElement[] els= maillage().elements();
    GrNoeud[] nds= maillage().noeuds();
    IEvolution[] crbs= evolutions();
    // Correspondances
    // Table Noeud->Num�ro
    Hashtable hnd2Num= new Hashtable();
    for (int i= 0; i < nds.length; i++)
      hnd2Num.put(nds[i], new Integer(i));
    // Table El�ment->Num�ro
    Hashtable hel2Num= new Hashtable();
    for (int i= 0; i < els.length; i++)
      hel2Num.put(els[i], new Integer(i));
    // Table Courbe->Num�ro
    Hashtable hcrb2Num= new Hashtable();
    for (int i= 0; i < crbs.length; i++)
      hcrb2Num.put(crbs[i], new Integer(i));
    //--------------------------------------------------------------------------
    //--- Formattage des donn�es  ----------------------------------------------
    //--------------------------------------------------------------------------
    // Cr�ation des groupes de propri�t�s par �l�ments de fond.
    Vector vgpes= new Vector();
    int[] igpes= new int[els.length];
    GroupePE[] grpPEs= new GroupePE[els.length];
    for (int i= 0; i < grpPEs.length; i++)
      grpPEs[i]= new GroupePE();
    // Initialisation du type des groupes
    {
      Vector vntsFds= mdlPrp_.naturesFonds();
      for (int i= 0; i < vntsFds.size(); i++) {
        PRNature nat= (PRNature)vntsFds.get(i);
        int tpNat= nat.type();
        int[] tpPrps= Definitions.getTypesProprietesElementaires(tpNat);
        Vector vels= nat.supports();
        for (int j= 0; j < vels.size(); j++) {
          int iEl= ((Integer)hel2Num.get(vels.get(j))).intValue();
          grpPEs[iEl].type= tpNat;
          grpPEs[iEl].codes= new int[tpPrps.length];
          grpPEs[iEl].vals= new double[tpPrps.length];
          for (int k= 0; k < tpPrps.length; k++)
            grpPEs[iEl].codes[k]= 0;
        }
      }
    }
    // Initialisation des propri�t�s des groupes.
    {
      PRPropriete[] prpsEls= mdlPrp_.proprietesElements();
      for (int i= 0; i < prpsEls.length; i++) {
        int tpPrp= prpsEls[i].type();
        Vector vels= prpsEls[i].supports();
        int code;
        double val;
        if (prpsEls[i].evolution() instanceof IEvolutionConstante) {
          code= 1;
          val= ((IEvolutionConstante)prpsEls[i].evolution()).constante();
        } else {
          code= 2;
          val= ((Integer)hcrb2Num.get(prpsEls[i].evolution())).intValue();
        }
        for (int j= 0; j < vels.size(); j++) {
          int iEl= ((Integer)hel2Num.get(vels.get(j))).intValue();
          int tpNat= grpPEs[iEl].type;
          int[] tpPrps= Definitions.getTypesProprietesElementaires(tpNat);
          int iPE;
          for (iPE= 0; iPE < tpPrps.length; iPE++)
            if (tpPrps[iPE] == tpPrp)
              break;
          grpPEs[iEl].codes[iPE]= code;
          grpPEs[iEl].vals[iPE]= val;
        }
      }
    }
    // Num�ro de groupe par �l�ment.
    NEXT_ELE : for (int i= 0; i < els.length; i++) {
      int iEl= ((Integer)hel2Num.get(els[i])).intValue();
      NEXT_GPE : for (int j= 0; j < vgpes.size(); j++) {
        GroupePE gp= (GroupePE)vgpes.get(j);
        if (gp.type != grpPEs[i].type)
          continue NEXT_GPE;
        for (int k= 0; k < gp.codes.length; k++)
          if (gp.codes[k] != grpPEs[i].codes[k])
            continue NEXT_GPE;
        for (int k= 0; k < gp.vals.length; k++)
          if (gp.vals[k] != grpPEs[i].vals[k])
            continue NEXT_GPE;
        igpes[iEl]= j;
        continue NEXT_ELE;
      }
      vgpes.add(grpPEs[i]);
      igpes[iEl]= vgpes.size() - 1;
    }
    // Cr�ation des groupes de propri�t�s par �l�ments de bord. Chaque bord est
    // rep�r� par le num�ro de son premier noeud.
    Vector vgpas= new Vector();
    int[] igpas= new int[nds.length];
    GroupePE[] grpPAs= new GroupePE[nds.length];
    // Initialisation du type des groupes pour chaque bord. Chaque nature ne
    // pointe que sur un bord.
    {
      Vector vntsBds= mdlPrp_.naturesBords();
      for (int i= 0; i < vntsBds.size(); i++) {
        PRNature nat= (PRNature)vntsBds.get(i);
        int tpNat= nat.type();
        int[] tpPrps= Definitions.getTypesProprietesElementaires(tpNat);
        Vector vels= nat.supports();
        for (int j= 0; j < vels.size(); j++) {
          int iNd=
            ((Integer)hnd2Num.get(((GrElement)vels.get(j)).noeuds_[0]))
              .intValue();
          grpPAs[iNd]= new GroupePE();
          grpPAs[iNd].type= tpNat;
          grpPAs[iNd].codes= new int[tpPrps.length];
          grpPAs[iNd].vals= new double[tpPrps.length];
          for (int k= 0; k < tpPrps.length; k++)
            grpPAs[iNd].codes[k]= 0;
        }
      }
    }
    // Initialisation des propri�t�s des groupes de bord. Chaque propri�t� peut
    // pointer sur plusieurs groupes.
    {
      PRPropriete[] prpsBds= mdlPrp_.proprietesAretes();
      for (int i= 0; i < prpsBds.length; i++) {
        int tpPrp= prpsBds[i].type();
        Vector vels= prpsBds[i].supports();
        int code;
        double val;
        if (prpsBds[i].evolution() instanceof IEvolutionConstante) {
          code= 1;
          val= ((IEvolutionConstante)prpsBds[i].evolution()).constante();
        } else {
          code= 2;
          val= ((Integer)hcrb2Num.get(prpsBds[i].evolution())).intValue();
        }
        for (int j= 0; j < vels.size(); j++) {
          int iNd=
            ((Integer)hnd2Num.get(((GrElement)vels.get(j)).noeuds_[0]))
              .intValue();
          int tpNat= grpPAs[iNd].type;
          int[] tpPrps= Definitions.getTypesProprietesElementaires(tpNat);
          int iPA;
          for (iPA= 0; iPA < tpPrps.length; iPA++)
            if (tpPrps[iPA] == tpPrp)
              break;
          grpPAs[iNd].codes[iPA]= code;
          grpPAs[iNd].vals[iPA]= val;
        }
      }
    }
    // Num�ro de groupe par bord.
    NEXT_ELE : for (int i= 0; i < nds.length; i++) {
      if (grpPAs[i] == null) {
        igpas[i]= -1;
        continue;
      }
      NEXT_GPE : for (int j= 0; j < vgpas.size(); j++) {
        GroupePE gp= (GroupePE)vgpas.get(j);
        if (gp.type != grpPAs[i].type)
          continue NEXT_GPE;
        for (int k= 0; k < gp.codes.length; k++)
          if (gp.codes[k] != grpPAs[i].codes[k])
            continue NEXT_GPE;
        for (int k= 0; k < gp.vals.length; k++)
          if (gp.vals[k] != grpPAs[i].vals[k])
            continue NEXT_GPE;
        igpas[i]= j;
        continue NEXT_ELE;
      }
      vgpas.add(grpPAs[i]);
      igpas[i]= vgpas.size() - 1;
    }
    //--------------------------------------------------------------------------
    //--- Transfert sur fichier  -----------------------------------------------
    //--------------------------------------------------------------------------
    PrintWriter file= null;
    try {
      // Ouverture du fichier
      file= new PrintWriter(new FileWriter(_nomFichier));
      // Num�ro de version courante
      String version= RefluxImplementation.informationsSoftware().version;
      file.println("*!$ Version " + version);
      // Type de probleme (3: Courantologie 3D)
      file.println("    3");
      //------------------------------------------------------------------------
      // Solutions initiales
      //------------------------------------------------------------------------
//      GrPoint[] ptsSI= modeleCalcul().pointsSI();
//      // Nombre de points de la surface
//      file.println("* POINTS DE SOLUTIONS INITIALES");
//      // Points de d�finition de la surface.
//      for (int i= 0; i < ptsSI.length; i++) {
//        file.println(ptsSI[i].x + " " + ptsSI[i].y + " " + ptsSI[i].z);
//      }
      // Solutions en chaque noeud
      PRSolutionsInitiales[] sis= modeleCalcul().solutionsInitiales();
      file.println("* SOL. INITIALES (VXi,VYi,VZi,HT) SUR LES NOEUDS");
      for (int i= 0; i < sis.length; i++) {
        double[] vals= sis[i].valeurs();
        file.print(i);
        for (int j= 0; j < vals.length; j++)
          file.print(" " + vals[j]);
        file.println();
      }
      //------------------------------------------------------------------------
      // Mod�le de propri�t�s
      //------------------------------------------------------------------------
      // Groupes de propri�t�s d'ar�tes
      file.println("* GROUPES DE PROPRIETES D'ARETES");
      file.println(vgpas.size());
      // Groupes de propri�t�s d'ar�tes
      for (int i= 0; i < vgpas.size(); i++) {
        GroupePE gp= (GroupePE)vgpas.get(i);
        file.print(i + " ");
        file.print(gp.type + " ");
        for (int j= 0; j < gp.codes.length; j++) {
          file.print(gp.codes[j] + " " + gp.vals[j] + " ");
        }
        file.println();
      }
      // Num�ro de groupe de propri�t�s d'ar�tes par noeud de bord.
      file.println("* NUMEROS DE GROUPES DE PROPRIETES D'ARETES");
      for (int i= 0; i < igpas.length; i++)
        if (igpas[i] != -1)
          file.println(i + " " + igpas[i]);
      // Nombre de groupes de propri�t�s de fond
      file.println("* GROUPES DE PROPRIETES DE FOND");
      file.println(vgpes.size());
      // Groupes de propri�t�s de fond
      for (int i= 0; i < vgpes.size(); i++) {
        GroupePE gp= (GroupePE)vgpes.get(i);
        file.print(i + " ");
        file.print(gp.type + " ");
        for (int j= 0; j < gp.codes.length; j++) {
          file.print(gp.codes[j] + " " + gp.vals[j] + " ");
        }
        file.println();
      }
      // Num�ro de groupe de propri�t�s de fond par �l�ment
      file.println("* NUMEROS DE GROUPES DE PROPRIETES DE FOND");
      for (int i= 0; i < igpes.length; i++)
        file.println(i + " " + igpes[i]);
      // Propri�t�s globales
      file.println("* PROPRIETES GLOBALES");
      PRPropriete[] prpsGlobs= mdlPrp_.proprietesGlobales();
      for (int i= 0; i < prpsGlobs.length; i++) {
        file.print(
          ((IEvolutionConstante)prpsGlobs[i].evolution()).constante() + " ");
      }
      file.println();
      //------------------------------------------------------------------------
      // Conditions aux limites
      //------------------------------------------------------------------------
      PRConditionLimite[] cls= conditionsLimites();
      // Nombre de conditions aux limites
      file.println("* CONDITIONS AUX LIMITES");
      file.println(cls.length);
      // Conditions aux limites
      for (int i= 0; i < cls.length; i++) {
        int code;
        double val;
        Vector vnds;
        if (cls[i].evolution() instanceof IEvolutionConstante) {
          code= 1;
          val= ((IEvolutionConstante)cls[i].evolution()).constante();
        } else {
          code= 2;
          val= ((Integer)hcrb2Num.get(cls[i].evolution())).intValue();
        }
        vnds= cls[i].supports();
        file.println(
          cls[i].type() + " " + code + " " + val + " " + vnds.size());
        for (int j= 0; j < vnds.size(); j++)
          file.println(((Integer)hnd2Num.get(vnds.get(j))).intValue());
      }
      //------------------------------------------------------------------------
      // Sollicitations
      //------------------------------------------------------------------------
      PRSollicitation[] sos= sollicitations();
      // Nombre de sollicitations
      file.println("* SOLLICITATIONS");
      file.println(sos.length);
      // Sollicitations
      for (int i= 0; i < sos.length; i++) {
        int code;
        double val;
        Vector vnds;
        if (sos[i].evolution() instanceof IEvolutionConstante) {
          code= 1;
          val= ((IEvolutionConstante)sos[i].evolution()).constante();
        } else {
          code= 2;
          val= ((Integer)hcrb2Num.get(sos[i].evolution())).intValue();
        }
        vnds= sos[i].supports();
        file.println(
          sos[i].type() + " " + code + " " + val + " " + vnds.size());
        for (int j= 0; j < vnds.size(); j++)
          file.println(((Integer)hnd2Num.get(vnds.get(j))).intValue());
      }
      //------------------------------------------------------------------------
      // Normales
      //------------------------------------------------------------------------
      file.println("* NORMALES SUR LES NOEUDS");
      PRNormale[] normales= mdlPrp_.normales();
      for (int i= 0; i < normales.length; i++) {
        int iNd= ((Integer)hnd2Num.get(normales[i].support())).intValue();
        file.println(iNd + " " + normales[i].valeur());
      }
      //------------------------------------------------------------------------
      // Mod�le de calcul
      //------------------------------------------------------------------------
      PRGroupePT[] grpPTs= mdlCal_.groupesPT();
      Object[] paramsCal= mdlCal_.parametresCalcul();
      // Groupes de pas de temps
      // Nombre de groupes de pas de temps
      file.println("* GROUPES DE PAS DE TEMPS");
      file.println(grpPTs.length);
      // Groupes de pas de temps
      for (int i= 0; i < grpPTs.length; i++) {
        PRBoucleTemps tps= grpPTs[i].temps();
        PRSchemaResolution sch= grpPTs[i].schema();
        PRMethodeResolution mth= grpPTs[i].methode();
        double[] coefs= mth.coefficients();
        file.print(tps.debut() + " " + tps.fin() + " " + tps.nbPas() + " ");
        file.print(sch.type() + " " + sch.coefficient() + " ");
        file.print(mth.type() + " ");
        for (int j= 0; j < coefs.length; j++)
          file.print(coefs[j] + " ");
        file.println(grpPTs[i].frequenceStockage());
      }
      // Parametres du calcul
      file.println("* PARAMETRES DU CALCUL");
      for (int i= 0; i < paramsCal.length; i++)
        file.print(paramsCal[i] + " ");
      file.println();

      //--------------------------------------------------------------------------
      // Groupes d'�l�ments
      //--------------------------------------------------------------------------
      {
        Vector grps=getGroupes();
        int[] numGrpElements=new int[grps.size()];
        int[][] numElements=new int[grps.size()][];
        for (int i=0; i<grps.size(); i++) {
          RefluxGroupe gp=(RefluxGroupe)grps.get(i);
          numGrpElements[i]=gp.getNum();
          Object[] elsGrp=gp.getObjects();
          numElements[i]=new int[els.length];
          for (int j=0; j<elsGrp.length; j++) {
            numElements[i][j]=((Integer)hel2Num.get(elsGrp[j])).intValue()+1;
          }
        }

        // Commentaire
        file.println("* GROUPES D'ELEMENTS POUR LES SOLUTIONS INITIALES");
        // <Nombre de groupes>
        file.println(numGrpElements.length);
        // Boucle sur les groupes
        for (int i=0; i<numGrpElements.length; i++) {
          // <Num�ro de groupe>,<nombre d'�lements>
          file.println(numGrpElements[i]+" "+numElements[i].length);

          // <Num�ros des �l�ments (16 par ligne)>
          int j=0;
          while (j<numElements[i].length) {
            file.print(numElements[i][j]+" ");
            if ((j+1)%16==0 || j==numElements[i].length-1) file.println();
            j++;
          }
        }
      }

      //--------------------------------------------------------------------------
      // Multiplans
      //--------------------------------------------------------------------------
      {
        Vector plans=getPlans();
        int[] numMultiplans=new int[plans.size()];
        int[] numMultiplansGrp=new int[plans.size()];
        int[][] numMultiplansLignes=new int[plans.size()][];
        double[][][] p1MultiplansLignes=new double[plans.size()][][];
        double[][][] p2MultiplansLignes=new double[plans.size()][][];
        int[][] autoMultiplansLignes=new int[plans.size()][];
        double[][] valMultiplansLignes=new double[plans.size()][];

        for (int i=0; i<plans.size(); i++) {
          RefluxPlan pl=(RefluxPlan)plans.get(i);
          numMultiplans[i]=pl.getNum();
          numMultiplansGrp[i]=pl.getGroupe()==null ? 0:pl.getGroupe().getNum();

          RefluxSegment[] sgs=pl.getSegments();
          numMultiplansLignes[i]=new int[sgs.length];
          p1MultiplansLignes[i]=new double[sgs.length][2];
          p2MultiplansLignes[i]=new double[sgs.length][2];
          autoMultiplansLignes[i]=new int[sgs.length];
          valMultiplansLignes[i]=new double[sgs.length];
          for (int j=0; j<sgs.length; j++) {
            numMultiplansLignes[i][j]=sgs[j].getNum();
            p1MultiplansLignes[i][j]=sgs[j].o;
            p2MultiplansLignes[i][j]=sgs[j].e;
            autoMultiplansLignes[i][j]=sgs[j].isHAutomatique()? 1:0;
            if (!sgs[j].isHAutomatique())
              valMultiplansLignes[i][j]=sgs[j].getHauteur();
            else if (j==0)              // Num�ro du plan amont associ�
              valMultiplansLignes[i][j]=pl.getPlanAmont()==null ? 0:pl.getPlanAmont().getNum();
            else if (j==sgs.length-1)   // Num�ro du plan aval associ�
              valMultiplansLignes[i][j]=pl.getPlanAval()==null ? 0:pl.getPlanAval().getNum();
          }
        }
        // Commentaire
        file.println("* MULTIPLANS POUR LES SOLUTIONS INITIALES");
        // <nombre de multiplans>
        file.println(numMultiplans.length);

        // Boucle sur les multiplans
        for (int i=0; i<numMultiplans.length; i++) {
          // <numero de multiplan>,<nb lignes>,<num groupe>
          file.print(numMultiplans[i]+" ");
          file.print(numMultiplansLignes[i].length+" ");
          file.println(numMultiplansGrp[i]);

          // Boucle sur chaque segment
          for (int j=0; j<numMultiplansLignes[i].length; j++) {
            // <num segment>,<xo>,<yo>,<xe>,<ye>,<h; 0:Fix�, 1:Auto>,<valeur h ou num plan si auto>
            file.print(numMultiplansLignes[i][j]+" ");
            file.print(p1MultiplansLignes[i][j][0]+" ");
            file.print(p1MultiplansLignes[i][j][1]+" ");
            file.print(p2MultiplansLignes[i][j][0]+" ");
            file.print(p2MultiplansLignes[i][j][1]+" ");
            file.print(autoMultiplansLignes[i][j]+" ");
            file.println(valMultiplansLignes[i][j]);
          }
        }
      }
    } catch (IOException exc) {
      throw new IOException("Erreur d'�criture sur " + _nomFichier);
    } catch (NumberFormatException exc) {
      throw new IOException("Erreur d'�criture sur " + _nomFichier);
    } finally {
      // Fermeture du fichier
      file.close();
    }
  }
  /**
   * Retourne la classe interface de calcul associ� au projet.
   *
   * @return L'interface de calcul.
   */
  public PRInterfaceCalcul getInterfaceCalcul() {
    if (interface_ == null) {
      interface_= new HY3dInterfaceCalcul();
    }
    return interface_;
  }
  /**
   * Cr�e les solutions initiales aux noeuds � partir des tableaux pass�s.
   * @param _vx La vitesse x en chaque noeud dans l'ordre des noeuds.
   * @param _vy La vitesse y en chaque noeud dans l'ordre des noeuds.
   * @param _ne Le niveau d'eau en chaque noeud dans l'ordre des noeuds.
   */
  public void creeSolutionsInitiales(
    double[] _vx,
    double[] _vy,
    double[] _ne) {
    PRSolutionsInitiales[] sis;
    int[] nbFctsNds= nbFonctionsNoeuds();
    GrNoeud[] nds= maillage().noeuds();
    sis= new PRSolutionsInitiales[nds.length];
    for (int i= 0; i < nds.length; i++) {
      //      sis[i]=new PRSolutionsInitiales(nds[i],new double[]{_vx[i]},
      //                                             new double[]{_vy[i]},
      //                                             new double[]{0.},
      //                                             _ne[i]);
      sis[i]=
        new PRSolutionsInitiales(
          nds[i],
          new double[] { _vx[i], _vy[i], 0., _ne[i] });
      changeNbTermes(sis[i], nbFctsNds[i]);
    }
    mdlCal_.solutionsInitiales(sis);
  }
  /**
   * Controle et mise � jour des solutions initiales si le nombre de termes
   * u,v,w a chang�.
   */
  public void controleNbTermesNoeuds() {
    PRSolutionsInitiales[] sis= modeleCalcul().solutionsInitiales();
    int[] nbFctsNds= nbFonctionsNoeuds();
    for (int i= 0; i < nbFctsNds.length; i++) {
      int nbFcts= (sis[i].valeurs().length - 1) / 3;
      if (nbFcts != nbFctsNds[i])
        changeNbTermes(sis[i], nbFctsNds[i]);
    }
  }
  /**
   * Initialise les solutions initiales en chaque noeud avec des valeurs
   * globales.
   *
   * @param _valGlobs Valeurs globales.
   *        _valGlobs[0]: Niveau d'eau global.
   */
  public void initialiserSolutionsInitiales(double[] _valGlobs) {
    double[] hts;
    double[] vx;
    double[] vy;
    GrNoeud[] nds= maillage().noeuds();
    hts=new double[nds.length];
    vx=new double[nds.length];
    vy=new double[nds.length];
    for (int i= 0; i < nds.length; i++) {
      GrPoint pt= nds[i].point_;
      hts[i]= _valGlobs[0] - pt.z_;
      vx[i]=0;
      vy[i]=0;
    }
//    initialiserSolutionsInitiales(sx, sy, hts);
    creeSolutionsInitiales(vx,vy,hts);
  }
  /**
   * Initialise les solutions initiales en chaque noeud � partir du plan d�finit
   * par 3 points.
   */
  public void initialiserSolutionsInitiales() {
    // Controle que les multiplans sont correctement d�finis.
    Vector pls=getPlans();
    for (int i=0; i<pls.size(); i++) {
      RefluxPlan pl=(RefluxPlan)pls.get(i);
      if (pl.getGroupe()==null) {
        new BuDialogError(null, RefluxImplementation.informationsSoftware(),
                          "Le plan n� "+pl.getNum()+
                          " n'est associ� � aucun groupe d'�l�ment").activate();
        return;
      }
      if (pl.isHAutomatiqueAmont() && pl.getPlanAmont()==null ||
          pl.isHAutomatiqueAval()  && pl.getPlanAval()==null) {
        new BuDialogError(null, RefluxImplementation.informationsSoftware(),
                          "Le plan n� "+pl.getNum()+
                          " n'est pas enti�rement d�finit").activate();
        return;
      }
    }

    Vector plsTri=null;
    try {
      plsTri=RefluxPlan.triPlans(getPlans());
    }
    catch (IllegalArgumentException _exc) {
      new BuDialogError(null,RefluxImplementation.informationsSoftware(),_exc.getMessage()).activate();
      return;
    }

    for (int i=0; i<plsTri.size(); i++) {
      RefluxPlan pl=(RefluxPlan)plsTri.get(i);
      if (pl.isHAutomatiqueAmont()) {
        RefluxPlan plDep=pl.getPlanAmont();
        RefluxSegment sg=pl.getSegmentAmont();
        double xmil=(sg.o[0]+sg.e[0])/2;
        double ymil=(sg.o[1]+sg.e[1])/2;
        double h=plDep.calculeHauteur(xmil,ymil);  // A priori si le point est bien entre 2 segments
        pl.setHAmont(h);
      }
      if (pl.isHAutomatiqueAval()) {
        RefluxPlan plDep=pl.getPlanAval();
        RefluxSegment sg=pl.getSegmentAval();
        double xmil=(sg.o[0]+sg.e[0])/2;
        double ymil=(sg.o[1]+sg.e[1])/2;
        double h=plDep.calculeHauteur(xmil,ymil);  // A priori si le point est bien entre 2 segments
        pl.setHAval(h);
      }
      pl.calculeHIntermediaires(); // Pour obtenir un h partout sur le multiplan
    }

    // Initialisation des solutions initiales (par d�faut h � bathym�trie, vx=0, vy=0).
    GrNoeud[] nds=maillage().noeuds();
    double[] hts=new double[nds.length];
    double[] vx=new double[nds.length];
    double[] vy=new double[nds.length];
    for (int i=0; i<hts.length; i++) hts[i]=0;

    Hashtable hnd2Ind=new Hashtable();
    Hashtable hnd2Pl=new Hashtable();   // Stockage du plan dans lequel est le noeud
    Hashtable hnd2Iseg=new Hashtable(); // Stockage du segment du plan dans lequel est le noeud.
    for (int i=0; i<nds.length; i++) hnd2Ind.put(nds[i],new Integer(i));

    // Calcul des hauteurs totales des noeuds pour chaque multiplan.
    for (int i=0; i<plsTri.size(); i++) {
      RefluxPlan pl=(RefluxPlan)plsTri.get(i);
      RefluxGroupe gp=pl.getGroupe();
      Object[] els=gp.getObjects();
      // Regroupement de tous les noeuds de tous les �l�ments concern�s par le plan
      HashSet hnds=new HashSet();
      for (int j=0; j<els.length; j++) {
        GrNoeud[] ndsEle=((GrElement)els[j]).noeuds_;
        for (int k=0; k<ndsEle.length; k++) {
          hnds.add(ndsEle[k]);
        }
      }
      GrNoeud[] ndsPl=(GrNoeud[])hnds.toArray(new GrNoeud[0]);
      for (int j=0; j<ndsPl.length; j++) {
        GrPoint pt=ndsPl[j].point_;
        int iseg=pl.getIndiceSegment(pt.x_,pt.y_);
        double h=pl.calculeHauteur(iseg,pt.x_,pt.y_);
        hnd2Pl.put(ndsPl[j],pl);
        hnd2Iseg.put(ndsPl[j],new Integer(iseg));
        hts[((Integer)hnd2Ind.get(ndsPl[j])).intValue()]=h-pt.z_;
      }
    }

    // Noeuds de transit (calcul�s globalement au maillage)
    boolean[] ntrs=getNoeudsTransit(hts);

    // Attention : Le calcul des vitesses implique de connaitre le plan dans lequel
    // est le ht du noeud. Donc � nouveau recherche des noeuds du plan � consid�rer
    // pour retrouver dans quel plan, etc. Peut �tre stocker plutot la pente pour
    // chaque noeud...

    // Pour chaque noeud concern�, calcul des vitesses.
    for (Enumeration e=hnd2Pl.keys(); e.hasMoreElements();) {
      GrNoeud nd=(GrNoeud)e.nextElement();
      RefluxPlan pl=(RefluxPlan)hnd2Pl.get(nd);
      int iseg=((Integer)hnd2Iseg.get(nd)).intValue();
      int ind=((Integer)hnd2Ind.get(nd)).intValue();
      double[] vxy=pl.calculeVitesses(iseg,hts[ind],nd.point_.x_,nd.point_.y_,ntrs[ind]);
      vx[ind]=vxy[0];
      vy[ind]=vxy[1];
    }

    creeSolutionsInitiales(vx,vy,hts);
  }
  /**
   * Initialise les solutions initiales � partir de la pente suivant x et y et
   * des hauteurs.
   */
//  private void initialiserSolutionsInitiales(
//    double _sx,
//    double _sy,
//    double[] _hts) {
//    // Noeuds de transit
//    boolean[] nTrs= getNoeudsTransit(_hts);
//    // Calcul des vitesses pour chaque noeud (On prend un chezy=50)
//    double[][] vxy= calculVitesses(_hts, nTrs, _sx, _sy);
//    creeSolutionsInitiales(vxy[0], vxy[1], _hts);
//  }
  /**
   * Retourne le nombre de fonctions aux noeuds pour chaque noeud. Ceci est
   * fonction du nombre de fonctions sur les �l�ments.
   *
   * @return Le nb de fonctions aux noeuds dans l'ordre des noeuds du maillage.
   *         Si le noeud appartient � un �l�ment dont le nombre de fonctions
   *         n'est pas fix�, son nombre de fonctions=0.
   */
  public int[] nbFonctionsNoeuds() {
    int[] r;
    GrNoeud[] nds= maillage().noeuds();
    GrElement[] els= maillage().elements();
    PRPropriete[] prpsEls= modeleProprietes().proprietesElements();
    // Table de correspondance Noeud->Indice
    Hashtable hnd2Ind= new Hashtable();
    for (int i= 0; i < nds.length; i++)
      hnd2Ind.put(nds[i], new Integer(i));
    // Table de correspondance El�ment->Indice
    Hashtable hel2Ind= new Hashtable();
    for (int i= 0; i < els.length; i++)
      hel2Ind.put(els[i], new Integer(i));
    r= new int[nds.length];
    for (int i= 0; i < nds.length; i++)
      r[i]= Integer.MAX_VALUE;
    for (int i= 0; i < prpsEls.length; i++) {
      Vector supports;
      int ival;
      if (prpsEls[i].type() != PRPropriete.NB_FCT)
        continue;
      supports= prpsEls[i].supports();
      for (int j= 0; j < supports.size(); j++) {
        int iEl= ((Integer)hel2Ind.get(supports.get(j))).intValue();
        GrNoeud[] ndsEl= els[iEl].noeuds_;
        ival= (int) ((IEvolutionConstante)prpsEls[i].evolution()).constante();
        for (int k= 0; k < ndsEl.length; k++) {
          int iNd= ((Integer)hnd2Ind.get(ndsEl[k])).intValue();
          r[iNd]= Math.min(r[iNd], ival);
        }
      }
    }
    for (int i= 0; i < nds.length; i++)
      if (r[i] == Integer.MAX_VALUE)
        r[i]= 0;
    return r;
  }
  /**
   * Change le nombre de termes pour la solution initiale.
   * Ce nombre � une influence sur les vitesses.
   *
   * @param _si Solution initiale.
   * @param _nb Le nombre de termes. S'il est inf�rieur � 1, ce nombre est mis
   * � 1 pour garder les valeurs de vitesses.
   */
  public void changeNbTermes(PRSolutionsInitiales _si, int _nb) {
    _nb= Math.max(_nb, 1);
    double[] oldVals= _si.valeurs();
    int oldNb= (oldVals.length - 1) / 3;
    double[] vals= new double[_nb * 3 + 1];
    for (int i= 0; i < vals.length; i++)
      vals[i]= 0;
    for (int i= 0; i < Math.min(_nb, oldNb); i++) {
      vals[i]= oldVals[i];
      vals[i + _nb]= oldVals[i + oldNb];
      vals[i + 2 * _nb]= oldVals[i + 2 * oldNb];
    }
    vals[vals.length - 1]= oldVals[oldVals.length - 1];
//    _si.valeurs(oldVals);
    _si.valeurs(vals);
  }
}
