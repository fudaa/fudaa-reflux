/*
 * @file         PRSollicitation.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.util.Arrays;
import java.util.Vector;

import org.fudaa.dodico.corba.mesure.IEvolution;
/**
 * Une sollicitation.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRSollicitation {
  public static final int DEBIT= 0;
  public static final int VENT= 1;
  public static final int CONTRAINTE_RADIATION= 2;
  public static final int FLUX= 3;
  public static final int DEBIT_SOLIDE= 4;
  // Type de la sollicitation
  private int type_;
  // Evolution associee
  private IEvolution evolution_;
  // Supports geometriques associes
  private Vector supports_;
  /**
   * Construit une sollicitation sans attributs. Les attributs doivent dans ce cas
   * etre OBLIGATOIREMENT specifies par les methodes appropriees avant toute
   * manipulation de la sollicitation.
   */
  public PRSollicitation() {
    type(-1);
    evolution(null);
    supports(new Vector());
  }
  /**
   * Construit une sollicitation.
   * @param type le type de la sollicitation
   * @param evolution l'evolution associee
   * @param supports les supports geometriques associes
   */
  public PRSollicitation(int type, IEvolution evolution, Vector supports) {
    type(type);
    evolution(evolution);
    supports(supports);
  }
  /**
   * Construit une sollicitation.
   * @param type le type de la sollicitation
   * @param evolution l'evolution associee
   * @param supports les supports geometriques associes
   */
  public PRSollicitation(int type, IEvolution evolution, Object[] supports) {
    type(type);
    evolution(evolution);
    supports(supports);
  }
  /**
   * Retourne le type de cette sollicitation.
   * @return le type
   */
  public int type() {
    return type_;
  }
  /**
   * Modifie le type de cette sollicitation.
   * @param _type le type de la sollicitation
   */
  public void type(int _type) {
    type_= _type;
  }
  /**
   * Retourne l'evolution associee a cette sollicitation.
   * @return l'evolution associee (= null si pas d'evolution)
   */
  public IEvolution evolution() {
    return evolution_;
  }
  /**
   * Modifie l'evolution associee a cette sollicitation.
   * @param _evolution l'evolution associee
   */
  public void evolution(IEvolution _evolution) {
    evolution_= _evolution;
  }
  /**
   * Retourne les supports geometriques associes a cette sollicitation.
   * @return les supports geometriques (= null si pas de supports)
   */
  public Vector supports() {
    return supports_;
  }
  /**
   * Remplace les supports geometriques associes a cette sollicitation.
   * @param _supports les supports associes
   */
  public void supports(Vector _supports) {
    supports_= _supports;
  }
  /**
   * Remplace les supports geometriques associes a cette sollicitation.
   * @param _supports les supports associes
   */
  public void supports(Object[] _supports) {
    supports_= new Vector(Arrays.asList(_supports));
  }
}
