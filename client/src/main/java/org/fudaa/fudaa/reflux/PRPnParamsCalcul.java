/*
 * @file         PRPnParamsCalcul.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import javax.swing.JPanel;
/**
 * Un panel comportant les parametres de calcul.
 * Tous les parametres de calcul sont actuellement cens�s �tre des
 * <i>double</i>. Les <i>boolean</i> sont donc mis en double, par exemple en
 * d�cidant que 0 <=> <i>false</i> et 1 <=> <i>true</i>, les <i>int</i> aussi.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public abstract class PRPnParamsCalcul extends JPanel {
  /**
   * Affectation du projet courant pour mise � jour du panneau.
   *
   * @param _prj Le projet courant.
   */
  public abstract void setProjet(PRProjet _prj);
  /**
   * Initialisation avec les parametres de calcul.
   *
   * @param _params Les parametres de calcul.
   */
  public abstract void setParametres(Object[] _params);
  /*
   * Retourne les parametres de calcul.
   * @return Les parametres dans l'ordre.
   */
  public abstract Object[] getParametres();
}
