/*
 * @file         PRPreferences.java
 * @creation     2001-03-07
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import com.memoire.bu.BuPreferences;
/**
 * Une classe de preferences pour Reflux.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRPreferences extends BuPreferences {
  public final static PRPreferences REFLUX= new PRPreferences();
  public PRPreferences() {
    super();
    // taille des normales en pixel.
    //    if (getStringProperty("fudaa_reflux.taille_normales")==null)
    //     putStringProperty("fudaa_reflux.taille_normales","15");
    putIntegerProperty(
      "fudaa_reflux.taille_normales",
      getIntegerProperty("fudaa_reflux.taille_normales", 15));
  }
  public void applyOn(Object _o) {
    if (!(_o instanceof RefluxImplementation))
      throw new RuntimeException("" + _o + " is not a RefluxImplementation.");
    RefluxImplementation _app= (RefluxImplementation)_o;
    // Taille des normales
    _app.fnCalques_.cqNormale.setTailleNormales(tailleNormales());
    _app.fnCalques_.cqNormale.repaint();
  }
  public int tailleNormales() {
    return Math.max(5, getIntegerProperty("fudaa_reflux.taille_normales"));
  }
}
