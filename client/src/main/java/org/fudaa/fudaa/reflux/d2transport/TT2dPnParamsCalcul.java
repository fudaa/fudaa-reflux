/*
 * @file         TT2dPnParamsCalcul.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d2transport;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuGridLayout;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.mesure.IEvolution;

import org.fudaa.fudaa.reflux.PRPnParamsCalcul;
import org.fudaa.fudaa.reflux.PRProjet;
/**
 * Un panel Reflux 2D transport comportant les parametres de calcul.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class TT2dPnParamsCalcul extends PRPnParamsCalcul {
  //public class TT2dPnParamsCalcul extends JPanel {
  JCheckBox cbConvectionZf= new JCheckBox();
  JCheckBox cbConvectionC= new JCheckBox();
  JCheckBox cbDiffusion= new JCheckBox();
  JTextField tfDebitRef= new JTextField();
  JTextField tfProfRef= new JTextField();
  JTextField tfConcRef= new JTextField();
  JTextField tfEffetPente= new JTextField();
  JTextField tfNdErodable= new JTextField();
  JTextField tfHminCalcul= new JTextField();
  JComboBox coFormule= new JComboBox();
  JTextField tfPrjHoule= new JTextField();
  JLabel lbPrjHoule= new JLabel();
  JButton btPrjHoule= new JButton();
  JFileChooser diFcPrjHoule_;
  JFileChooser diFcPrjHydro_;
  BuFileFilter fltPrjHydro_;
  BuFileFilter fltFicHydro_;
  BuGridLayout lyCourant= new BuGridLayout();
  JPanel pnCas= new JPanel();
  JRadioButton rbMaree= new JRadioButton();
  JRadioButton rbCrue= new JRadioButton();
  JRadioButton rbConst= new JRadioButton();
  JPanel pnChamp= new JPanel();
  CardLayout lyChamp= new CardLayout();
  JPanel pnMaree= new JPanel();
  BuGridLayout lyMaree= new BuGridLayout();
  JRadioButton rbMareeCal= new JRadioButton();
  JRadioButton rbMareeInt= new JRadioButton();
  JLabel lbDummy1= new JLabel();
  JLabel lbPeriode= new JLabel();
  JTextField tfPeriode= new JTextField();
  JButton btPrjMaree45= new JButton();
  JLabel lbDummy2= new JLabel();
  JLabel lbPrjMaree45= new JLabel();
  JTextField tfPrjMaree45= new JTextField();
  JLabel lbPrjMaree90= new JLabel();
  JTextField tfPrjMaree90= new JTextField();
  JButton btPrjMaree90= new JButton();
  JLabel lbCrbCoefs= new JLabel();
  JComboBox coCrbCoefs= new JComboBox();
  BuGridLayout lyCrue= new BuGridLayout();
  JButton btPrjCrue= new JButton();
  JLabel lbDureeCrue= new JLabel();
  JTextField tfDureeCrue= new JTextField();
  JPanel pnCrue= new JPanel();
  JTextField tfPrjCrue= new JTextField();
  JLabel lbPrjCrue= new JLabel();
  BuGridLayout lyConst= new BuGridLayout();
  JButton btPrjConst= new JButton();
  JPanel pnConst= new JPanel();
  JTextField tfPrjConst= new JTextField();
  JLabel lbPrjConst= new JLabel();
  JTextField tfT0Hydro= new JTextField();
  JLabel lbFormule= new JLabel();
  /**
   * Cr�ation du panel.
   */
  public TT2dPnParamsCalcul() {
    super();
    try {
      jbInit();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  /**
   * M�thode d'impl�mentation de l'interface.
   */
  private void jbInit() throws Exception {
    JLabel lbDummy4= new JLabel();
    JLabel lbDummy5= new JLabel();
    JLabel lbDummy6= new JLabel();
    JLabel lbDummy7= new JLabel();
    JLabel lbDummy8= new JLabel();
    JLabel lbDummy9= new JLabel();
    JLabel lbDummy10= new JLabel();
    JLabel lbDebitRef= new JLabel();
    JLabel lbProfRef= new JLabel();
    JLabel lbConcRef= new JLabel();
    JLabel lbEffetPente= new JLabel();
    JLabel lbNdErodable= new JLabel();
    JLabel lbHminCalcul= new JLabel();
    JLabel lbT0Hydro= new JLabel();
    BuGridLayout lyOptions= new BuGridLayout();
    BuGridLayout lyGene= new BuGridLayout();
    JPanel pnOptions= new JPanel();
    JPanel pnGene= new JPanel();
    JPanel pnCourant= new JPanel();
    JTabbedPane tpMain= new JTabbedPane();
    BorderLayout lyThis= new BorderLayout();
    JSeparator seCourant= new JSeparator();
    ButtonGroup bgCasCourant= new ButtonGroup();
    ButtonGroup bgCasMaree= new ButtonGroup();
    BuFileFilter flt1;
    //    BuFileFilter flt2;
    // Panneau options
    cbConvectionZf.setText("Prise en compte de la convection sur Zf");
    cbConvectionC.setText("Prise en compte de la convection sur C");
    cbDiffusion.setText("Prise en compte de la diffusion");
    lyOptions.setColumns(1);
    lyOptions.setHgap(5);
    lyOptions.setVgap(5);
    lyOptions.setCfilled(false);
    pnOptions.setLayout(lyOptions);
    pnOptions.setBorder(new EmptyBorder(5, 5, 5, 5));
    pnOptions.add(cbConvectionZf);
    pnOptions.add(cbConvectionC);
    pnOptions.add(cbDiffusion);
    // Panneau g�n�ral
    lbDebitRef.setText("D�bit liquide de r�f�rence:");
    lbProfRef.setText("Profondeur de r�f�rence:");
    lbConcRef.setText("Concentration de r�f�rence:");
    lbEffetPente.setText("Coef. d'effet de pente:");
    lbNdErodable.setText("Crit�re de noeud �rodable:");
    lbHminCalcul.setText("Hauteur de d�pot min. calculable:");
    lbFormule.setText("Formulation:");
    lbPrjHoule.setText("Projet houle associ�:");
    lbT0Hydro.setText("t0 s�diment par rapport � t0 hydro:");
    lbDebitRef.setHorizontalAlignment(SwingConstants.RIGHT);
    lbProfRef.setHorizontalAlignment(SwingConstants.RIGHT);
    lbConcRef.setHorizontalAlignment(SwingConstants.RIGHT);
    lbEffetPente.setHorizontalAlignment(SwingConstants.RIGHT);
    lbNdErodable.setHorizontalAlignment(SwingConstants.RIGHT);
    lbHminCalcul.setHorizontalAlignment(SwingConstants.RIGHT);
    lbFormule.setHorizontalAlignment(SwingConstants.RIGHT);
    lbPrjHoule.setHorizontalAlignment(SwingConstants.RIGHT);
    lbT0Hydro.setHorizontalAlignment(SwingConstants.RIGHT);
    tfDebitRef.setPreferredSize(new Dimension(100, 21));
    tfProfRef.setPreferredSize(new Dimension(100, 21));
    tfConcRef.setPreferredSize(new Dimension(100, 21));
    tfEffetPente.setPreferredSize(new Dimension(100, 21));
    tfNdErodable.setPreferredSize(new Dimension(100, 21));
    tfHminCalcul.setPreferredSize(new Dimension(100, 21));
    tfPrjHoule.setPreferredSize(new Dimension(100, 21));
    tfT0Hydro.setPreferredSize(new Dimension(100, 21));
    coFormule.addItem("Meyer Peter M�ller");
    coFormule.addItem("Einstein");
    coFormule.addItem("Engelund Hansen");
    coFormule.addItem("Bijker");
    coFormule.addItem("Van Rijn");
    coFormule.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        coFormule_itemStateChanged(_evt);
      }
    });
    coFormule.setSelectedIndex(-1);
    btPrjHoule.setText("...");
    btPrjHoule.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        btPrjHoule_actionPerformed(_evt);
      }
    });
    lyGene.setColumns(3);
    lyGene.setHgap(5);
    lyGene.setVgap(5);
    lyGene.setVfilled(false);
    lyGene.setCfilled(false);
    pnGene.setLayout(lyGene);
    pnGene.setBorder(new EmptyBorder(5, 5, 5, 5));
    pnGene.add(lbDebitRef);
    pnGene.add(tfDebitRef);
    pnGene.add(lbDummy4);
    pnGene.add(lbProfRef);
    pnGene.add(tfProfRef);
    pnGene.add(lbDummy5);
    pnGene.add(lbConcRef);
    pnGene.add(tfConcRef);
    pnGene.add(lbDummy6);
    pnGene.add(lbEffetPente);
    pnGene.add(tfEffetPente);
    pnGene.add(lbDummy7);
    pnGene.add(lbNdErodable);
    pnGene.add(tfNdErodable);
    pnGene.add(lbDummy8);
    pnGene.add(lbHminCalcul);
    pnGene.add(tfHminCalcul);
    pnGene.add(lbDummy9);
    pnGene.add(lbFormule);
    pnGene.add(coFormule);
    pnGene.add(lbDummy10);
    pnGene.add(lbPrjHoule);
    pnGene.add(tfPrjHoule);
    pnGene.add(btPrjHoule);
    pnGene.add(lbT0Hydro);
    pnGene.add(tfT0Hydro);
    // Panneau champ de courant
    // Cas de champ de courant
    rbConst.setText("Constant");
    rbConst.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbCas_itemStateChanged(_evt);
      }
    });
    bgCasCourant.add(rbConst);
    rbCrue.setText("Crue");
    rbCrue.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbCas_itemStateChanged(_evt);
      }
    });
    bgCasCourant.add(rbCrue);
    rbMaree.setText("Mar�e");
    rbMaree.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbCas_itemStateChanged(_evt);
      }
    });
    bgCasCourant.add(rbMaree);
    pnCas.add(rbConst);
    pnCas.add(rbCrue);
    pnCas.add(rbMaree);
    // Panneau champ de courant constant
    lbPrjConst.setText("Projet de courantologie:");
    lbPrjConst.setHorizontalAlignment(SwingConstants.RIGHT);
    tfPrjConst.setPreferredSize(new Dimension(150, 21));
    btPrjConst.setText("...");
    btPrjConst.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        btPrjHydro_actionPerformed(_evt);
      }
    });
    lyConst.setVgap(5);
    lyConst.setHgap(5);
    lyConst.setColumns(3);
    lyConst.setCfilled(false);
    lyConst.setVfilled(false);
    pnConst.setLayout(lyConst);
    pnConst.add(lbPrjConst);
    pnConst.add(tfPrjConst);
    pnConst.add(btPrjConst);
    // Panneau champ de courant crue
    lbPrjCrue.setText("Projet de courantologie:");
    lbPrjCrue.setHorizontalAlignment(SwingConstants.RIGHT);
    tfPrjCrue.setPreferredSize(new Dimension(150, 21));
    btPrjCrue.setText("...");
    btPrjCrue.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        btPrjHydro_actionPerformed(_evt);
      }
    });
    lbDureeCrue.setText("Dur�e de la crue:");
    lbDureeCrue.setHorizontalAlignment(SwingConstants.RIGHT);
    tfDureeCrue.setPreferredSize(new Dimension(150, 21));
    lyCrue.setVgap(5);
    lyCrue.setHgap(5);
    lyCrue.setColumns(3);
    lyCrue.setCfilled(false);
    lyCrue.setVfilled(false);
    pnCrue.setLayout(lyCrue);
    pnCrue.add(lbPrjCrue);
    pnCrue.add(tfPrjCrue);
    pnCrue.add(btPrjCrue);
    pnCrue.add(lbDureeCrue);
    pnCrue.add(tfDureeCrue);
    // Panneau champ de courant mar�e
    rbMareeCal.setText("Mar�e calcul�e");
    rbMareeCal.setHorizontalAlignment(SwingConstants.RIGHT);
    rbMareeCal.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbMaree_itemStateChanged(_evt);
      }
    });
    bgCasMaree.add(rbMareeCal);
    rbMareeInt.setText("Mar�e interpol�e");
    rbMareeInt.setHorizontalAlignment(SwingConstants.RIGHT);
    rbMareeInt.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbMaree_itemStateChanged(_evt);
      }
    });
    bgCasMaree.add(rbMareeInt);
    lbPeriode.setText("P�riode de la mar�e:");
    lbPeriode.setHorizontalAlignment(SwingConstants.RIGHT);
    tfPeriode.setPreferredSize(new Dimension(150, 21));
    lbPrjMaree45.setText("Projet mar�e coef. 45:");
    lbPrjMaree45.setHorizontalAlignment(SwingConstants.RIGHT);
    tfPrjMaree45.setPreferredSize(new Dimension(150, 21));
    btPrjMaree45.setText("...");
    btPrjMaree45.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        btPrjHydro_actionPerformed(_evt);
      }
    });
    lbPrjMaree90.setText("Projet mar�e coef. 90:");
    lbPrjMaree90.setHorizontalAlignment(SwingConstants.RIGHT);
    tfPrjMaree90.setPreferredSize(new Dimension(150, 21));
    btPrjMaree90.setText("...");
    btPrjMaree90.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        btPrjHydro_actionPerformed(_evt);
      }
    });
    lbCrbCoefs.setText("Courbe des coef. de mar�e:");
    lbCrbCoefs.setHorizontalAlignment(SwingConstants.RIGHT);
    coCrbCoefs.setPreferredSize(new Dimension(150, 21));
    lyMaree.setVfilled(false);
    lyMaree.setCfilled(false);
    lyMaree.setColumns(3);
    lyMaree.setHgap(5);
    lyMaree.setVgap(5);
    pnMaree.setLayout(lyMaree);
    pnMaree.add(rbMareeCal);
    pnMaree.add(rbMareeInt);
    pnMaree.add(lbDummy1);
    pnMaree.add(lbPeriode);
    pnMaree.add(tfPeriode);
    pnMaree.add(lbDummy2);
    pnMaree.add(lbPrjMaree45);
    pnMaree.add(tfPrjMaree45);
    pnMaree.add(btPrjMaree45);
    pnMaree.add(lbPrjMaree90);
    pnMaree.add(tfPrjMaree90);
    pnMaree.add(btPrjMaree90);
    pnMaree.add(lbCrbCoefs);
    pnMaree.add(coCrbCoefs);
    pnChamp.setLayout(lyChamp);
    pnChamp.add(pnConst, rbConst.getText());
    pnChamp.add(pnCrue, rbCrue.getText());
    pnChamp.add(pnMaree, rbMaree.getText());
    lyCourant.setColumns(1);
    lyCourant.setHgap(5);
    lyCourant.setVgap(5);
    pnCourant.setLayout(lyCourant);
    pnCourant.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnCourant.add(pnCas);
    pnCourant.add(seCourant);
    pnCourant.add(pnChamp);
    tpMain.addTab(
      "Champ de courant",
      null,
      pnCourant,
      "Param�tres de courantologie");
    tpMain.addTab("G�n�ral", null, pnGene, "Valeurs g�n�rales");
    tpMain.addTab("Options", null, pnOptions, "Options du calcul");
    this.setLayout(lyThis);
    this.add(tpMain, "Center");
    // Dialogue de saisie du nom du projet de houle.
    flt1= new BuFileFilter("prf", "Projet refonde");
    diFcPrjHoule_= new JFileChooser();
    diFcPrjHoule_.setFileHidingEnabled(true);
    diFcPrjHoule_.setCurrentDirectory(new File(System.getProperty("user.dir")));
    diFcPrjHoule_.setMultiSelectionEnabled(false);
    diFcPrjHoule_.addChoosableFileFilter(flt1);
    diFcPrjHoule_.setFileFilter(flt1);
    // Dialogue de saisie du nom du projet de courantologie.
    fltPrjHydro_= new BuFileFilter("pre", "Projet courantologie");
    fltFicHydro_=
      new BuFileFilter(
        new String[] { "cor", "ele", "bth", "sov" },
        "Fichiers courantologie");
    diFcPrjHydro_= new JFileChooser();
    diFcPrjHydro_.setFileHidingEnabled(true);
    diFcPrjHydro_.setCurrentDirectory(new File(System.getProperty("user.dir")));
    diFcPrjHydro_.setMultiSelectionEnabled(false);
    diFcPrjHydro_.addChoosableFileFilter(fltPrjHydro_);
    diFcPrjHydro_.addChoosableFileFilter(fltFicHydro_);
    diFcPrjHydro_.setFileFilter(fltPrjHydro_);
  }
  /**
   * Affectation du projet courant pour mise � jour du panneau. Permet
   * d'affecter le nom des courbes de coefficients de mar�e.
   *
   * @param _prj Le projet courant.
   */
  public void setProjet(PRProjet _prj) {
    IEvolution[] crbs= _prj.evolutions();
    boolean bprjTT= _prj.type() == PRProjet.TRANS_TOT_2D;
    // Nom des courbes de coefficients de mar�e
    coCrbCoefs.removeAllItems();
    for (int i= 0; i < crbs.length; i++)
      coCrbCoefs.addItem(_prj.getName(crbs[i]));
    // Activation de la formulation (uniquement pour le transport solide)
    lbFormule.setEnabled(bprjTT);
    coFormule.setEnabled(bprjTT);
    // Activation des termes de convection
    cbConvectionC.setEnabled(!bprjTT);
    cbConvectionZf.setEnabled(bprjTT);
  }
  /**
   * Initialisation avec les parametres de calcul. Ils sont dans cet ordre :<p>
   *
   * <pre>
   *  Options:
   *    [] Integer : Convection Zf                  {0: sans, 1: avec}
   *    [] Integer : Convection C                   {0: sans, 1: avec}
   *    [] Integer : Diffusion                      {0: sans, 1: avec}
   *
   *  G�n�ral:
   *    [] Double  : D�bit liquide de r�f�rence
   *    [] Double  : Profondeur de r�f�rence
   *    [] Double  : Concentration de r�f�rence
   *    [] Double  : Coef. d'effet de pente
   *    [] Double  : Crit�re de noeud �rodable
   *    [] Double  : Haut. de d�pot min. calculable
   *    [] Integer : Formulation                    {0,1,2,3,4}
   *    [] File    : Racine du projet de houle
   *    [] Double  : t0 s�diment par rapport � t0 hydro
   *
   *  Courantologie:
   *   [] Integer : Cas de champ                   {0: constant, 1: crue, 2: mar�e}
   *   [] Integer : Cas de mar�e                   {0: calcul�e, 1: interpol�e}
   *   [] Double  : Dur�e crue/P�riode de mar�e
   *   [] Integer : Indice de courbe des coefs dans la table des courbes du projet.
   *   [] File    : Racine du projet crue/mar�e 45
   *   [] File    : Racine du projet mar�e coef. 90
   * </pre>
   *
   * @param _pars Les parametres de calcul.
   */
  public void setParametres(Object[] _pars) {
    Integer convecZf= (Integer)_pars[TT2dResource.CONVEC_ZF];
    Integer convecC= (Integer)_pars[TT2dResource.CONVEC_C];
    Integer diffusion= (Integer)_pars[TT2dResource.DIFFUSION];
    Double debitRef= (Double)_pars[TT2dResource.DEBIT_REF];
    Double profRef= (Double)_pars[TT2dResource.PROF_REF];
    Double concRef= (Double)_pars[TT2dResource.CONC_REF];
    Double effetPente= (Double)_pars[TT2dResource.EFFET_PENTE];
    Double ndErodable= (Double)_pars[TT2dResource.ND_ERODABLE];
    Double hminCalcul= (Double)_pars[TT2dResource.HMIN_CALCUL];
    Integer formule= (Integer)_pars[TT2dResource.FORMULE];
    File prjHoule= (File)_pars[TT2dResource.PRJ_HOULE];
    Integer casChamp= (Integer)_pars[TT2dResource.CAS_CHAMP];
    Integer casMaree= (Integer)_pars[TT2dResource.CAS_MAREE];
    Double periode= (Double)_pars[TT2dResource.PERIODE];
    Integer icrb= (Integer)_pars[TT2dResource.ICOURBE];
    File prjMaree45= (File)_pars[TT2dResource.PRJ_MAREE_45];
    File prjMaree90= (File)_pars[TT2dResource.PRJ_MAREE_90];
    Double t0Hydro= (Double)_pars[TT2dResource.T0_HYDRO];
    // Options
    cbConvectionZf.setSelected(convecZf.intValue() == 1);
    cbConvectionC.setSelected(convecC.intValue() == 1);
    cbDiffusion.setSelected(diffusion.intValue() == 1);
    // G�n�ral
    tfDebitRef.setText("" + debitRef);
    tfProfRef.setText("" + profRef);
    tfConcRef.setText("" + concRef);
    tfEffetPente.setText("" + effetPente);
    tfNdErodable.setText("" + ndErodable);
    tfHminCalcul.setText("" + hminCalcul);
    coFormule.setSelectedIndex(formule.intValue());
    tfPrjHoule.setText(prjHoule.getPath());
    tfT0Hydro.setText("" + t0Hydro);
    // Champ de courant
    rbConst.setSelected(casChamp.intValue() == 0);
    rbCrue.setSelected(casChamp.intValue() == 1);
    rbMaree.setSelected(casChamp.intValue() == 2);
    rbMareeCal.setSelected(casMaree.intValue() == 0);
    rbMareeInt.setSelected(casMaree.intValue() == 1);
    tfPeriode.setText("" + periode);
    tfDureeCrue.setText("" + periode);
    coCrbCoefs.setSelectedIndex(icrb.intValue());
    tfPrjConst.setText(prjMaree45.getPath());
    tfPrjCrue.setText(prjMaree45.getPath());
    tfPrjMaree45.setText(prjMaree45.getPath());
    tfPrjMaree90.setText(prjMaree90.getPath());
  }
  /*
   * Retourne les parametres de calcul.
   * @return Les parametres dans l'ordre.
   */
  public Object[] getParametres() {
    Object[] r= new Object[TT2dResource.nbParsCalcul];
    int casChamp;
    if (rbConst.isSelected())
      casChamp= 0;
    else if (rbCrue.isSelected())
      casChamp= 1;
    else
      casChamp= 2;
    // Options
    r[TT2dResource.CONVEC_ZF]= new Integer(cbConvectionZf.isSelected() ? 1 : 0);
    r[TT2dResource.CONVEC_C]= new Integer(cbConvectionC.isSelected() ? 1 : 0);
    r[TT2dResource.DIFFUSION]= new Integer(cbDiffusion.isSelected() ? 1 : 0);
    // G�n�ral
    try {
      r[TT2dResource.DEBIT_REF]= new Double(tfDebitRef.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.PROF_REF]= new Double(tfProfRef.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.CONC_REF]= new Double(tfConcRef.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.EFFET_PENTE]= new Double(tfEffetPente.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.ND_ERODABLE]= new Double(tfNdErodable.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.HMIN_CALCUL]= new Double(tfHminCalcul.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.FORMULE]= new Integer(coFormule.getSelectedIndex());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.PRJ_HOULE]= new File(tfPrjHoule.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.T0_HYDRO]= new Double(tfT0Hydro.getText());
    } catch (NumberFormatException _exc) {};
    // Champ de courant
    r[TT2dResource.CAS_CHAMP]= new Integer(casChamp);
    r[TT2dResource.CAS_MAREE]= new Integer(rbMareeInt.isSelected() ? 1 : 0);
    try {
      if (casChamp == 0)
        r[TT2dResource.PERIODE]= new Double(0);
      else if (casChamp == 1)
        r[TT2dResource.PERIODE]= new Double(tfDureeCrue.getText());
      else
        r[TT2dResource.PERIODE]= new Double(tfPeriode.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.ICOURBE]= new Integer(coCrbCoefs.getSelectedIndex());
    } catch (NumberFormatException _exc) {};
    try {
      if (casChamp == 0)
        r[TT2dResource.PRJ_MAREE_45]= new File(tfPrjConst.getText());
      else if (casChamp == 1)
        r[TT2dResource.PRJ_MAREE_45]= new File(tfPrjCrue.getText());
      else
        r[TT2dResource.PRJ_MAREE_45]= new File(tfPrjMaree45.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[TT2dResource.PRJ_MAREE_90]= new File(tfPrjMaree90.getText());
    } catch (NumberFormatException _exc) {};
    return r;
  }
  /**
   * Retourne la racine d'un fichier s�lectionn�.
   *
   * @param _fich Nom du fichier s�lectionn�.
   * @param _exts Les extensions possibles sans les ".".
   * @return La racine du fichier sans extension.
   */
  private String getRacineFichier(String _fich, String[] _exts) {
    String r= _fich;
    for (int i= 0; i < _exts.length; i++) {
      if (r.endsWith(CtuluLibString.DOT + _exts[i])) {
        r= r.substring(0, r.lastIndexOf(CtuluLibString.DOT  + _exts[i]));
        break;
      }
    }
    return r;
  }
  //----------------------------------------------------------------------------
  // Ev�nements
  //----------------------------------------------------------------------------
  /**
   * M�thode activ�e quand l'item de coFormule est modifi�.
   */
  void coFormule_itemStateChanged(ItemEvent _evt) {
    boolean active=
      _evt.getStateChange() == ItemEvent.SELECTED
        && _evt.getItem().equals("Bijker");
    lbPrjHoule.setEnabled(active);
    tfPrjHoule.setEnabled(active);
    btPrjHoule.setEnabled(active);
  }
  /**
   * M�thode activ�e quand le bouton btPrjHoule est actionn�.
   */
  void btPrjHoule_actionPerformed(ActionEvent _evt) {
    diFcPrjHoule_.setSelectedFile(new File(tfPrjHoule.getText()));
    int ret= diFcPrjHoule_.showOpenDialog(this);
    if (ret == JFileChooser.APPROVE_OPTION) {
      tfPrjHoule.setText("" + diFcPrjHoule_.getSelectedFile());
    }
  }
  /**
   * D�clench� quand le bouton de s�lection du nom du fichier projet
   * courantologie est activ�.
   */
  void btPrjHydro_actionPerformed(ActionEvent _evt) {
    JTextField tf;
    if (_evt.getSource() == btPrjConst)
      tf= tfPrjConst;
    else if (_evt.getSource() == btPrjCrue)
      tf= tfPrjCrue;
    else if (_evt.getSource() == btPrjMaree45)
      tf= tfPrjMaree45;
    else
      tf= tfPrjMaree90;
    if (tf.getText().endsWith(".pre"))
      diFcPrjHydro_.setFileFilter(fltPrjHydro_);
    else
      diFcPrjHydro_.setFileFilter(fltFicHydro_);
    diFcPrjHydro_.setSelectedFile(new File(tf.getText()));
    int ret= diFcPrjHydro_.showOpenDialog(this);
    if (ret == JFileChooser.APPROVE_OPTION) {
      String fich=
        getRacineFichier(
          diFcPrjHydro_.getSelectedFile().getPath(),
          new String[] { "cor", "ele", "bth", "sov" });
      tf.setText("" + fich);
    }
  }
  /**
   * D�clench� quand un radio bouton du panneau Cas est
   * s�lectionn�.
   */
  void rbCas_itemStateChanged(ItemEvent _evt) {
    if (_evt.getStateChange() == ItemEvent.SELECTED)
      lyChamp.show(pnChamp, ((JRadioButton)_evt.getSource()).getText());
  }
  /**
   * D�clench� quand un radio bouton du choix de mar�e est
   * s�lectionn�.
   */
  void rbMaree_itemStateChanged(ItemEvent _evt) {
    if (_evt.getStateChange() == ItemEvent.SELECTED) {
      boolean b= _evt.getSource() == rbMareeInt;
      // Pas de passage en mode interpol� si pas de courbes.
      if (b && coCrbCoefs.getItemCount() == 0) {
        new BuDialogError(
          null,
          null,
          "Le projet courant ne contient aucune courbe.\n"
            + "Vous ne pouvez pas passer en mode 'Mar�e interpol�e'.")
          .activate();
        rbMareeCal.setSelected(true);
        return;
      }
      lbPrjMaree90.setEnabled(b);
      tfPrjMaree90.setEnabled(b);
      btPrjMaree90.setEnabled(b);
      lbCrbCoefs.setEnabled(b);
      coCrbCoefs.setEnabled(b);
      lbPrjMaree45.setText(b ? "Projet mar�e coef. 45:" : "Projet mar�e:");
    }
  }
}
