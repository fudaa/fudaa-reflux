/*
 * @file         PRResultats.java
 * @creation     2001-01-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.util.Arrays;
import java.util.Vector;
/**
 * Classe de résultats du projet. Contient les résultats aux noeuds pour le
 * projet.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRResultats {
  public static final int UNDEFINED= -1;
  public static final int COORD_X= 0;
  public static final int COORD_Y= 1;
  public static final int BATHYMETRIE= 2;
  public static final int VITESSE_X= 3;
  public static final int VITESSE_Y= 4;
  public static final int NIVEAU_EAU= 5;
  public static final int PROFONDEUR= 6;
  public static final int NORME= 7;
  /** Vector of steps. */
  private Vector vsteps_= new Vector();
  /** Project associatted with results. */
  private PRProjet prj_;
  public PRResultats() {}
  public PRResultats(Etape[] _steps) {
    vsteps_.addAll(Arrays.asList(_steps));
  }
  /**
   * Assign the associated project .
   */
  public void setProject(PRProjet _prj) {
    prj_= _prj;
  }
  public Etape[] getSteps() {
    return (Etape[])vsteps_.toArray(new Etape[0]);
  }
  public void addStep(Etape _step) {
    //    System.out.println("Step added");
    vsteps_.add(_step);
    if (prj_ != null)
      prj_.fireDataChange(
        new RefluxDataChangeEvent(this, RefluxDataChangeEvent.RESULT_ADDED));
  }
  /**
   * Remove all steps.
   */
  public void removeAll() {
    vsteps_.clear();
    if (prj_ != null)
      prj_.fireDataChange(
        new RefluxDataChangeEvent(this, RefluxDataChangeEvent.RESULTS_CLEARED));
  }
  public Etape getStep(int _ind) {
    if (_ind < 0 || _ind >= vsteps_.size())
      return null;
    return (Etape)vsteps_.get(_ind);
  }
  /**
   * Get step at time _t.
   * @return Step. If no step at this time, <code>null</code> is returned.
   */
  public Etape getStepAtTime(double _t) {
    for (int i= 0; i < vsteps_.size(); i++)
      if (((Etape)vsteps_.get(i)).t == _t)
        return (Etape)vsteps_.get(i);
    return null;
  }
  public int getNbSteps() {
    return vsteps_.size();
  }
  /**
   * A class representing a step of results.
   */
  public static class Etape {
    public double t;
    private Colonne[] cols_;
    public Etape(Colonne[] _cols) {
      this(0, _cols);
    }
    public Etape(double _t, Colonne[] _cols) {
      cols_= _cols;
      if (cols_ == null)
        cols_= new Colonne[0];
      t= _t;
    }
    public Colonne getColonne(int _ind) {
      if (_ind < 0 || _ind > cols_.length)
        return null;
      return cols_[_ind];
    }
    /**
     * Return first column of type _type.
     * @return Column or <code>null</code> if no column of this type.
     */
    public Colonne getColonneOfType(int _type) {
      for (int i= 0; i < cols_.length; i++)
        if (cols_[i].type == _type)
          return cols_[i];
      return null;
    }
    public void replace(int _ind, Colonne _col) {
      if (_ind < 0 || _ind > cols_.length)
        return;
      cols_[_ind]= _col;
    }
    /**
     * Add a column to end of results.
     * @param _col Column.
     */
    public void add(Colonne _col) {
      Colonne[] newCols_= new Colonne[cols_.length + 1];
      System.arraycopy(cols_, 0, newCols_, 0, cols_.length);
      newCols_[cols_.length]= _col;
      cols_= newCols_;
    }
    public int getNbColonnes() {
      return cols_.length;
    }
    public static class Colonne {
      /** Type de résultat. */
      public int type;
      /** Valeurs. */
      public double[] valeurs;
      public Colonne(double[] _vals) {
        this(UNDEFINED, _vals);
      }
      public Colonne(int _type, double[] _vals) {
        type= _type;
        valeurs= _vals;
      }
    }
  }
}