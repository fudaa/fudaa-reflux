/*
 * @file         PRModeleCalcul.java
 * @creation     2001-01-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;

import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;
/**
 * Classe mod�le de calcul du projet. Le mod�le de calcul contient toutes les
 * sp�cificit�s calcul, cad les impressions, les coefficients, les groupes de
 * pas de temps.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRModeleCalcul {
  /**
   * Groupes de pas de temps du mod�le.
   */
  private Vector grpPdt_= new Vector();
  /**
   * Parametres du calcul (coefs de pond�ration, indices d'impression, etc.).
   */
  private Object[] paramsCalcul_;
  /**
   * Solutions initiales calcul�es ou r�cup�r�es d'un pr�cedent calcul, dans
   * l'ordre des noeuds du maillage.
   */
  private PRSolutionsInitiales[] solInit_= new PRSolutionsInitiales[0];
  /**
   * Hashtable des noeuds pointants sur les solutions initiales.
   */
  private Hashtable hnd2Si= new Hashtable();
  /**
   * Points du plan de r�f�rence des solutions initiales.
   */
  private GrPoint[] ptsSI_;
  /**
   * Cr�ation d'un mod�le de calcul.
   */
  public PRModeleCalcul() {
    paramsCalcul_= Definitions.getDefaultParametresCalcul();
    grpPdt_.add(new PRGroupePT());
    // Valeur des points de solutions initiales
    ptsSI_= new GrPoint[3];
    ptsSI_[0]= new GrPoint(0., 0., 0.);
    ptsSI_[1]= new GrPoint(1., 0., 0.);
    ptsSI_[2]= new GrPoint(1., 1., 0.);
  }
  /**
   * Affecte les groupes de pas de temps au mod�le.
   * @param _grps Les groupes.
   */
  public void groupesPT(PRGroupePT[] _grps) {
    grpPdt_= new Vector(Arrays.asList(_grps));
  }
  /**
   * Retourne les groupes de pas de temps du mod�le.
   * @return Les groupes.
   */
  public PRGroupePT[] groupesPT() {
    return (PRGroupePT[])grpPdt_.toArray(new PRGroupePT[0]);
  }
  /**
   * Affecte les parametres du calcul.
   * @param _params Les parametres.
   */
  public void parametresCalcul(Object[] _params) {
    paramsCalcul_= _params;
  }
  /**
   * Retourne les parametres du calcul.
   * @return Les parametres.
   */
  public Object[] parametresCalcul() {
    return paramsCalcul_;
  }
  /**
   * Affecte les solutions initiales.
   * @param _sol Les solutions initiales pour tous les noeuds.
   */
  public void solutionsInitiales(PRSolutionsInitiales[] _sol) {
    solInit_= _sol;
    hnd2Si.clear();
    for (int i= 0; i < solInit_.length; i++) {
      hnd2Si.put(solInit_[i].support(), solInit_[i]);
    }
  }
  /**
   * Retourne les solutions initiales.
   * @return Les solutions initiales pour tous les noeuds.
   */
  public PRSolutionsInitiales[] solutionsInitiales() {
    return solInit_;
  }
  /**
   * Retourne la solution intiale pour un noeud donn�.
   *
   * @param _nd
   * @return La solution initiale.
   */
  public PRSolutionsInitiales solutionInitiale(GrNoeud _nd) {
    return (PRSolutionsInitiales)hnd2Si.get(_nd);
  }
  /**
   * Affecte les points du plan de r�f�rence des solutions initiales.
   * @param _pts Les points.
   */
  public void pointsSI(GrPoint[] _pts) {
    ptsSI_= _pts;
  }
  /**
   * Retourne les  points du plan de r�f�rence des solutions initiales.
   */
  public GrPoint[] pointsSI() {
    return ptsSI_;
  }
  /**
   * Calcul de la pente suivant x et y pour le plan de r�f�rence des solutions
   * initiales.
   *
   * @return r[0]: pente suivant x. r[1]: pente suivant y.
   */
  public double[] getPenteXYPlanSI() {
    double[] r= new double[2];
    r[0]=
      - (
        (ptsSI_[1].y_ - ptsSI_[0].y_) * (ptsSI_[2].z_ - ptsSI_[0].z_)
          - (ptsSI_[1].z_ - ptsSI_[0].z_) * (ptsSI_[2].y_ - ptsSI_[0].y_))
        / ((ptsSI_[1].x_ - ptsSI_[0].x_) * (ptsSI_[2].y_ - ptsSI_[0].y_)
          - (ptsSI_[1].y_ - ptsSI_[0].y_) * (ptsSI_[2].x_ - ptsSI_[0].x_));
    r[1]=
      ((ptsSI_[1].x_ - ptsSI_[0].x_) * (ptsSI_[2].z_ - ptsSI_[0].z_)
        - (ptsSI_[1].z_ - ptsSI_[0].z_) * (ptsSI_[2].x_ - ptsSI_[0].x_))
        / ((ptsSI_[1].x_ - ptsSI_[0].x_) * (ptsSI_[2].y_ - ptsSI_[0].y_)
          - (ptsSI_[1].y_ - ptsSI_[0].y_) * (ptsSI_[2].x_ - ptsSI_[0].x_));
    return r;
  }
}