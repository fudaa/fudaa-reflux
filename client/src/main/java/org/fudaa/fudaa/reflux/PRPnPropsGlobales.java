/*
 * @file         PRPnPropsGlobales.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import javax.swing.JPanel;
/**
 * Un panel comportant les propriétés physiques globales.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public abstract class PRPnPropsGlobales extends JPanel {
  /**
   * Initialisation avec les propriétés globales.
   *
   * @param _prps Les propriétés.
   */
  public abstract void setProprietes(PRPropriete[] _prps);
  /*
   * Retourne les propriétés physiques globales.
   * @return Les propriétés dans l'ordre.
   */
  public abstract PRPropriete[] getProprietes();
}