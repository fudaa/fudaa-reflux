/*
 * @file         PRPnSaisieValeurPEDefaut.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.BorderLayout;

import javax.swing.JTextField;
/**
 * Un composant standard de saisie de valeur de propri�t�. Ce composant est
 * un JTextField.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRPnSaisieValeurPEDefaut extends PRPnSaisieValeurPE {
  /**
   * Le composant contenu dans le panneau.
   */
  private JTextField tf_= new JTextField();
  /**
   * Sauvegarde de la valeur de propri�t�.
   */
  private double oldPrps_;
  /**
   * Cr�ation du composant.
   */
  public PRPnSaisieValeurPEDefaut() {
    super();
    tf_.setText("0.0");
    setLayout(new BorderLayout());
    add(tf_, "Center");
  }
  /**
   * Initialisation avec la valeur de propri�t�.
   * @param _val La valeur de la propri�t�.
   */
  public void setValeur(double _val) {
    oldPrps_= _val;
    if (Double.isNaN(oldPrps_))
      tf_.setText("");
    else
      tf_.setText("" + oldPrps_);
  }
  /**
   * Retourne la valeur de la propri�t�. La valeur peut �tre Double.NaN, c'est
   * � dire mixte.
   *
   * @return La valeur.
   */
  public double getValeur() {
    try {
      oldPrps_= Double.parseDouble(tf_.getText());
    } catch (NumberFormatException _exc) {}
    return oldPrps_;
  }
  /**
   * Pour rendre actif/non les composants du panneau.
   */
  public void setEnabled(boolean _b) {
    super.setEnabled(_b);
    tf_.setEnabled(_b);
  }
}