/*
 * @file         RefluxVariables.java
 * @creation     1999-01-11
 * @modification $Date: 2003-11-25 10:14:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
/**
 * Classe des variables globales � Reflux.
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:14:06 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class RefluxVariables {
  // Flag de d�bug (false : Pas de d�bug, true : D�bug)
  public static boolean debug= false;
}