/*
 * @file         PRDialogNormale.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.fudaa.ebli.geometrie.GrNoeud;
/**
 * Une boite de dialogue pour la modification des normales.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRDialogNormale extends RefluxDialog {
  PRProjet projet_;
  JPanel pnAngle= new JPanel();
  JLabel laAngle= new JLabel();
  JTextField tfAngle= new JTextField();
  FlowLayout lyAffichage= new FlowLayout();
  public PRDialogNormale() {
    this(null);
  }
  public PRDialogNormale(Frame _parent) {
    // Le frame principal et le panel des boutons est cr�� et g�r� par la classe
    // m�re. On ne cr�e que le panel principal.
    super(_parent, OK_CANCEL_APPLY_OPTION);
    /**Jbuilder**/
    jbInit();
  }
  public void jbInit() { /**JBuilder**/
    laAngle.setText("Angle des normales");
    tfAngle.setPreferredSize(new Dimension(70, 19));
    tfAngle.setMinimumSize(new Dimension(70, 19));
    tfAngle.setMaximumSize(new Dimension(70, 19));
    pnAffichage.setLayout(lyAffichage);
    pnAffichage.add(laAngle);
    pnAffichage.add(tfAngle);
    setTitle("Modification de l'angle des normales");
    pack();
    setSize(getPreferredSize());
    setNoSelection();
    setModal(false);
    setResizable(false);
  }
  /**
   * Association du projet.
   */
  public void setProjet(PRProjet _projet) {
    projet_= _projet;
  }
  /**
   * Mise a jour de la boite de dialogue avec les objets s�lectionn�s.
   */
  public void setSelection(Object[] _selects) {
    if (_selects == null
      || _selects.length == 0
      || !(_selects[0] instanceof GrNoeud)) {
      setNoSelection();
      return;
    }
    Double valeur= null;
    PRNormale normale;
    boolean notSet= true;
    for (int i= 0; i < _selects.length; i++) {
      normale= projet_.modeleProprietes().normale((GrNoeud)_selects[i]);
      if (normale != null) {
        if (notSet) {
          notSet= false;
          valeur= new Double(normale.valeur());
        } else if (valeur != null && valeur.doubleValue() != normale.valeur())
          valeur= null;
      }
    }
    laAngle.setEnabled(true);
    tfAngle.setEnabled(true);
    APPLY_BUTTON.setEnabled(true);
    OK_BUTTON.setEnabled(true);
    setAngle(valeur);
  }
  /**
   * Mise a jour de la boite de dialogue avec aucune s�lection.
   */
  public void setNoSelection() {
    laAngle.setEnabled(false);
    tfAngle.setEnabled(false);
    APPLY_BUTTON.setEnabled(false);
    OK_BUTTON.setEnabled(false);
    setAngle(new Double(0));
  }
  /**
   * Affectation de la valeur de l'angle (en degr�s).
   * @param _angle Si angle est <I>null</I>, alors la valeur est consid�r�e
   * comme �tant mixte. Elle est repr�sent�e par un texte vide
   */
  public void setAngle(Double _angle) {
    if (_angle == null)
      tfAngle.setText("");
    else {
      NumberFormat nf= NumberFormat.getInstance(Locale.US);
      nf.setMaximumFractionDigits(3);
      tfAngle.setText("" + nf.format(_angle.doubleValue()));
    }
  }
  /**
   * Retourne la valeur de l'angle (en degr�s).
   * @return l'angle. Si angle est <I>null</I>, alors la valeur est consid�r�e
   * comme �tant mixte.
   */
  public Double getAngle() {
    Double angle= null;
    if (!tfAngle.getText().equals("")) {
      try {
        angle= new Double(tfAngle.getText());
      } // Afficher une boite de dialogue si valeur non valide
      catch (NumberFormatException _exc) {}
    }
    return angle;
  }
}
