/*
 * @file         HY2dPnParamsCalcul.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d2;
import java.awt.BorderLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuGridLayout;

import org.fudaa.fudaa.reflux.PRPnParamsCalcul;
import org.fudaa.fudaa.reflux.PRProjet;
/**
 * Un panel Reflux 2D comportant les parametres de calcul. Il s'agit des
 * coefficients de pond�ration et des indices d'impression.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:56 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class HY2dPnParamsCalcul extends PRPnParamsCalcul {
  private JTextField tfConvection;
  private JTextField tfDiffusion;
  private JTextField tfContLineaire;
  private JTextField tfContNonLineaire;
  private JTextField tfGravitation;
  private JTextField tfFrottement;
  private JTextField tfCoriolis;
  private JTextField tfContRadiation;
  private JTextField tfVent;
  private JCheckBox cbDonnees;
  private JCheckBox cbPointeurs;
  private JCheckBox cbTablesElem;
  private JCheckBox cbTablesGlob;
  private JCheckBox cbCiel;
  private JCheckBox cbInit;
  private JCheckBox cbMiseAJour;
  private JCheckBox cbResolution;
  private JCheckBox cbIteration;
  //  JTabbedPane pnParams = new JTabbedPane();
  /**
   * Cr�ation du panel.
   */
  public HY2dPnParamsCalcul() {
    super();
    try {
      jbInit();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  /**
   * M�thode d'impl�mentation de l'interface.
   */
  private void jbInit() throws Exception {
    JTabbedPane tpMain;
    BorderLayout lyThis= new BorderLayout();
    JPanel pnCoefs= new JPanel();
    BuGridLayout lyCoefs= new BuGridLayout();
    lyCoefs.setColumns(2);
    lyCoefs.setHgap(5);
    lyCoefs.setVgap(5);
    pnCoefs.setLayout(lyCoefs);
    pnCoefs.setBorder(new EmptyBorder(5, 5, 5, 5));
    int n= 0;
    tfConvection= new JTextField();
    tfDiffusion= new JTextField();
    tfContLineaire= new JTextField();
    tfContNonLineaire= new JTextField();
    tfGravitation= new JTextField();
    tfFrottement= new JTextField();
    tfCoriolis= new JTextField();
    tfContRadiation= new JTextField();
    tfVent= new JTextField();
    tfConvection.setName("tfCONVECTION");
    tfDiffusion.setName("tfDIFFUSION");
    tfContLineaire.setName("tfCONTLINEAIRE");
    tfContNonLineaire.setName("tfCONTNONLINEAIRE");
    tfGravitation.setName("tfGRAVITATION");
    tfFrottement.setName("tfFROTTEMENT");
    tfCoriolis.setName("tfCORIOLIS");
    tfContRadiation.setName("tfCONTRADIATION");
    tfVent.setName("tfVENT");
    tfConvection.setColumns(8);
    tfDiffusion.setColumns(8);
    tfContLineaire.setColumns(8);
    tfContNonLineaire.setColumns(8);
    tfGravitation.setColumns(8);
    tfFrottement.setColumns(8);
    tfCoriolis.setColumns(8);
    tfContRadiation.setColumns(8);
    tfVent.setColumns(8);
    // Valeurs par d�faut
    tfConvection.setText("1.0");
    tfDiffusion.setText("1.0");
    tfContLineaire.setText("1.0");
    tfContNonLineaire.setText("1.0");
    tfGravitation.setText("1.0");
    tfFrottement.setText("1.0");
    tfCoriolis.setText("0.0");
    tfContRadiation.setText("0.0");
    tfVent.setText("0.0");
    pnCoefs.add(new JLabel("Convection :", SwingConstants.RIGHT), n++);
    pnCoefs.add(tfConvection, n++);
    pnCoefs.add(new JLabel("Diffusion :", SwingConstants.RIGHT), n++);
    pnCoefs.add(tfDiffusion, n++);
    pnCoefs.add(new JLabel("Continuit� lin�aire :", SwingConstants.RIGHT), n++);
    pnCoefs.add(tfContLineaire, n++);
    pnCoefs.add(new JLabel("Continuit� non lin�aire :", SwingConstants.RIGHT), n++);
    pnCoefs.add(tfContNonLineaire, n++);
    pnCoefs.add(new JLabel("Gravitation :", SwingConstants.RIGHT), n++);
    pnCoefs.add(tfGravitation, n++);
    pnCoefs.add(new JLabel("Frottement :", SwingConstants.RIGHT), n++);
    pnCoefs.add(tfFrottement, n++);
    pnCoefs.add(new JLabel("Coriolis :", SwingConstants.RIGHT), n++);
    pnCoefs.add(tfCoriolis, n++);
    pnCoefs.add(new JLabel("Contraintes de radiation :", SwingConstants.RIGHT), n++);
    pnCoefs.add(tfContRadiation, n++);
    pnCoefs.add(new JLabel("Forces du vent :", SwingConstants.RIGHT), n++);
    pnCoefs.add(tfVent, n++);
    pnCoefs.setLayout(lyCoefs);
    pnCoefs.setBorder(new EmptyBorder(5, 5, 5, 5));
    // Indices d'impression.
    JPanel pnImp= new JPanel();
    BuGridLayout lyImp= new BuGridLayout();
    lyImp.setColumns(1);
    lyImp.setHgap(5);
    lyImp.setVgap(2);
    pnImp.setLayout(lyImp);
    pnImp.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    cbDonnees= new JCheckBox();
    cbPointeurs= new JCheckBox();
    cbTablesElem= new JCheckBox();
    cbTablesGlob= new JCheckBox();
    cbCiel= new JCheckBox();
    cbInit= new JCheckBox();
    cbMiseAJour= new JCheckBox();
    cbResolution= new JCheckBox();
    cbIteration= new JCheckBox();
    cbDonnees.setName("cbDONNEES");
    cbPointeurs.setName("cbPOINTEURS");
    cbTablesElem.setName("cbTABLESELEM");
    cbTablesGlob.setName("cbTABLESGLOB");
    cbCiel.setName("cbCIEL");
    cbInit.setName("cbINIT");
    cbMiseAJour.setName("cbMISEAJOUR");
    cbResolution.setName("cbRESOLUTION");
    cbIteration.setName("cbITERATION");
    cbDonnees.setText("DONNEES :");
    cbPointeurs.setText("POINTEURS :");
    cbTablesElem.setText("TABLESELEM :");
    cbTablesGlob.setText("TABLESGLOB :");
    cbCiel.setText("CIEL :");
    cbInit.setText("INITIALISATION :");
    cbMiseAJour.setText("MISE A JOUR :");
    cbResolution.setText("RESOLUTION :");
    cbIteration.setText("ITERATION :");
    cbDonnees.setHorizontalTextPosition(SwingConstants.LEFT);
    cbPointeurs.setHorizontalTextPosition(SwingConstants.LEFT);
    cbTablesElem.setHorizontalTextPosition(SwingConstants.LEFT);
    cbTablesGlob.setHorizontalTextPosition(SwingConstants.LEFT);
    cbCiel.setHorizontalTextPosition(SwingConstants.LEFT);
    cbInit.setHorizontalTextPosition(SwingConstants.LEFT);
    cbMiseAJour.setHorizontalTextPosition(SwingConstants.LEFT);
    cbResolution.setHorizontalTextPosition(SwingConstants.LEFT);
    cbIteration.setHorizontalTextPosition(SwingConstants.LEFT);
    cbDonnees.setHorizontalAlignment(SwingConstants.RIGHT);
    cbPointeurs.setHorizontalAlignment(SwingConstants.RIGHT);
    cbTablesElem.setHorizontalAlignment(SwingConstants.RIGHT);
    cbTablesGlob.setHorizontalAlignment(SwingConstants.RIGHT);
    cbCiel.setHorizontalAlignment(SwingConstants.RIGHT);
    cbInit.setHorizontalAlignment(SwingConstants.RIGHT);
    cbMiseAJour.setHorizontalAlignment(SwingConstants.RIGHT);
    cbResolution.setHorizontalAlignment(SwingConstants.RIGHT);
    cbIteration.setHorizontalAlignment(SwingConstants.RIGHT);
    // Etat par d�faut
    cbDonnees.setSelected(true);
    cbPointeurs.setSelected(false);
    cbTablesElem.setSelected(false);
    cbTablesGlob.setSelected(false);
    cbCiel.setSelected(false);
    cbInit.setSelected(true);
    cbMiseAJour.setSelected(false);
    cbResolution.setSelected(true);
    cbIteration.setSelected(true);
    pnImp.add(cbDonnees, n++);
    pnImp.add(cbPointeurs, n++);
    pnImp.add(cbTablesElem, n++);
    pnImp.add(cbTablesGlob, n++);
    pnImp.add(cbCiel, n++);
    pnImp.add(cbInit, n++);
    pnImp.add(cbMiseAJour, n++);
    pnImp.add(cbResolution, n++);
    pnImp.add(cbIteration, n++);
    //    pnParams.addTab("Coefficients",null,pnCoefs,"Coefficients de pond�ration");
    //    pnParams.addTab("Impressions",null,pnImp,"Informations imprim�es dans le fichier .out");
    tpMain= new JTabbedPane();
    tpMain.setName("tpPARAMETRES");
    //    JPanel cdCoefs=new JPanel();
    //    cdCoefs.setLayout(new BorderLayout());
    //    pnCoefs_=PRFabrique.getFabrique().creePnCoefsCalcul();
    //    cdCoefs.add("North",pnCoefs_);
    tpMain.addTab("Coefficients", null, pnCoefs, "Coefficients de pond�ration");
    //    JPanel cdImp=new JPanel();
    //    cdImp.setLayout(new BorderLayout());
    //    cdImp.add("North",pnImp);
    tpMain.addTab(
      "Impressions",
      null,
      pnImp,
      "Informations imprim�es dans le fichier .out");
    this.setLayout(lyThis);
    this.add(tpMain, BorderLayout.CENTER);
  }
  /**
   * Affectation du projet courant pour mise � jour du panneau. Inutilis�.
   *
   * @param _prj Le projet courant.
   */
  public void setProjet(PRProjet _prj) {}
  /**
   * Initialisation avec les parametres de calcul. Ils sont dans cet ordre :<p>
   *
   * <pre>
   *  Coefficients de pond�rations :
   *    [ 0] : Convection       [0,1]
   *    [ 1] : Diffusion        [0,1]
   *    [ 2] : ContLineaire     [0,1]
   *    [ 3] : ContNonLineaire  [0,1]
   *    [ 4] : Gravitation      [0,1]
   *    [ 5] : Frottement       [0,1]
   *    [ 6] : Coriolis         [0,1]
   *    [ 7] : ContRadiation    [0,1]
   *    [ 8] : Vent             [0,1]
   *
   *  Indices d'impression:
   *    [ 9] : Donn�es          {0,1}
   *    [10] : Pointeurs        {0,1}
   *    [11] : TablesElem       {0,1}
   *    [12] : TablesGlob       {0,1}
   *    [13] : Ciel             {0,1}
   *    [14] : Init             {0,1}
   *    [15] : MiseAJour        {0,1}
   *    [16] : Resolution       {0,1}
   *    [17] : Iteration        {0,1}
   * </pre>
   *
   * @param _params Les parametres de calcul.
   */
  public void setParametres(Object[] _params) {
    // Coefficients de pond�ration.
    tfConvection.setText("" + _params[0]);
    tfDiffusion.setText("" + _params[1]);
    tfContLineaire.setText("" + _params[2]);
    tfContNonLineaire.setText("" + _params[3]);
    tfGravitation.setText("" + _params[4]);
    tfFrottement.setText("" + _params[5]);
    tfCoriolis.setText("" + _params[6]);
    tfContRadiation.setText("" + _params[7]);
    tfVent.setText("" + _params[8]);
    // Indices d'impression.
    cbDonnees.setSelected(((Double)_params[9]).doubleValue() == 1);
    cbPointeurs.setSelected(((Double)_params[10]).doubleValue() == 1);
    cbTablesElem.setSelected(((Double)_params[11]).doubleValue() == 1);
    cbTablesGlob.setSelected(((Double)_params[12]).doubleValue() == 1);
    cbCiel.setSelected(((Double)_params[13]).doubleValue() == 1);
    cbInit.setSelected(((Double)_params[14]).doubleValue() == 1);
    cbMiseAJour.setSelected(((Double)_params[15]).doubleValue() == 1);
    cbResolution.setSelected(((Double)_params[16]).doubleValue() == 1);
    cbIteration.setSelected(((Double)_params[17]).doubleValue() == 1);
  }
  /*
   * Retourne les parametres du calcul.
   * @return Les parametres dans l'ordre.
   */
  public Object[] getParametres() {
    Object[] r= new Object[18];
    // Coefficients de pond�ration.
    try {
      r[0]= new Double(tfConvection.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[1]= new Double(tfDiffusion.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[2]= new Double(tfContLineaire.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[3]= new Double(tfContNonLineaire.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[4]= new Double(tfGravitation.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[5]= new Double(tfFrottement.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[6]= new Double(tfCoriolis.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[7]= new Double(tfContRadiation.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[8]= new Double(tfVent.getText());
    } catch (NumberFormatException _exc) {};
    // Indices d'impression.
    r[9]= new Double(cbDonnees.isSelected() ? 1 : 0);
    r[10]= new Double(cbPointeurs.isSelected() ? 1 : 0);
    r[11]= new Double(cbTablesElem.isSelected() ? 1 : 0);
    r[12]= new Double(cbTablesGlob.isSelected() ? 1 : 0);
    r[13]= new Double(cbCiel.isSelected() ? 1 : 0);
    r[14]= new Double(cbInit.isSelected() ? 1 : 0);
    r[15]= new Double(cbMiseAJour.isSelected() ? 1 : 0);
    r[16]= new Double(cbResolution.isSelected() ? 1 : 0);
    r[17]= new Double(cbIteration.isSelected() ? 1 : 0);
    return r;
  }
}