package org.fudaa.fudaa.reflux;
import java.util.EventListener;
public interface RefluxDataChangeListener extends EventListener {
  public void dataChanged(RefluxDataChangeEvent _evt);
}