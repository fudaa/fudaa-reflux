package org.fudaa.fudaa.reflux;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;

import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.ZModeleMaillage;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrSymbole;
import org.fudaa.ebli.trace.TraceGeometrie;

import org.fudaa.fudaa.commun.trace2d.BPaletteCouleurPlage;
import org.fudaa.fudaa.commun.trace2d.Symbole;
import org.fudaa.fudaa.commun.trace2d.ZModeleChangeEvent;
import org.fudaa.fudaa.commun.trace2d.ZModeleChangeListener;
/**
 * Un calque de trace de vecteurs aux noeuds suivant une longueur et une couleur
 * d�pendante de la norme.
 *
 * @version      $Id: ZCalqueVecteur.java,v 1.12 2006-09-19 15:11:53 deniger Exp $
 * @author       Bertrand Marchand , Bertrand Marchand
 */
public class ZCalqueVecteur
  extends BCalqueAffichage
  implements ZModeleChangeListener {
  BPaletteCouleurPlage pal_;
  private GrBoite boite_;
  /** Le modele de maillage. */
  private ZModeleMaillage mdMaillage_;
  /** Le mod�le de vecteurs. */
  private ZModeleVecteur mdVecteur_;
  /** Le trac� ou non du maillage. */
  private boolean isMeshVisible_;
  /**
   * Contructeur du calque.
   */
  public ZCalqueVecteur() {
    super();
    pal_= new BPaletteCouleurPlage();
    pal_.setSymboleDefaut(new Symbole(Symbole.FLECHE, -90));
    pal_.setTitre("");
    pal_.setSousTitre("");
    pal_.setNbPlages(10);
    pal_.setTaillePlage(0, 8);
    pal_.setTaillePlage(9, 15);
  }
  /**
   * Affecte la palette au calque et au calque de l�gende associ�.
   * @param _pal Palette de niveaux de couleurs. Une palette <code>null</code>
   *             n'est pas autoris�e.
   */
  public void setPalette(BPaletteCouleurPlage _pal) {
    if (_pal == null)
      throw new IllegalArgumentException("Palette can't be null");
    pal_= _pal;
    construitLegende();
  }
  /**
   * Retourne la palette de niveaux de couleur.
   * @return La palette.
   */
  public BPaletteCouleurPlage getPalette() {
    return pal_;
  }
  /**
   * D�finition de la visibilit� du maillage.
   * @param _b <code>true</code> ou <code>false</code>
   */
  public void setMaillageVisible(boolean _b) {
    isMeshVisible_= _b;
  }
  /**
   * Le maillage est-il visible ?
   * @return <code>true</code> ou <code>false</code>
   */
  public boolean isMaillageVisible() {
    return isMeshVisible_;
  }
  /**
   * D�finition de la taille maxi en pixels des vecteurs.
   * @param _taille Taille maxi des vecteurs en pixels
   */
  public void setTailleMax(int _taille) {
    pal_.setTaillePlage(pal_.getNbPlages() - 1, _taille);
  }
  /**
   * Retoune la taille maxi des normales en pixels.
   */
  public int getTailleMax() {
    return pal_.getTaillePlage(pal_.getNbPlages() - 1);
  }
  /**
   * D�finition de la taille mini en pixels des vecteurs.
   * @param _taille Taille mini des vecteurs en pixels
   */
  public void setTailleMin(int _taille) {
    pal_.setTaillePlage(0, _taille);
  }
  /**
   * Retoune la taille maxi des normales en pixels.
   */
  public int getTailleMin() {
    return pal_.getTaillePlage(0);
  }
  /**
   * Affectation du modele de maillage.
   * @param _modele Le modele.
   */
  public void setModeleMaillage(ZModeleMaillage _modele) {
    ZModeleMaillage vp= mdMaillage_;
    mdMaillage_= _modele;
    boite_= null;
    firePropertyChange("maillage", vp, mdMaillage_);
    repaint();
  }
  /**
   * Retourne le modele de maillage.
   * @return Le modele.
   */
  public ZModeleMaillage getModeleMaillage() {
    return mdMaillage_;
  }
  /**
   * Affectation du modele de vecteur.
   * @param _modele Le modele
   */
  public void setModeleVecteur(ZModeleVecteur _modele) {
    // ZModeleVecteur vp=mdVecteur_;
    //    if (mdVecteur_!=null) mdVecteur_.removeModelChangeListener(this);
    mdVecteur_= _modele;
    //    if (mdVecteur_!=null) mdVecteur_.addModelChangeListener(this);
    modelChanged(null);
  }
  /**
   * Retourne le modele de vecteur.
   * @return Le modele.
   */
  public ZModeleVecteur getModeleVecteur() {
    return mdVecteur_;
  }
  // >>> ZModeleChangeListener  ------------------------------------------------
  public void modelChanged(ZModeleChangeEvent _evt) {
    // Mise � jour de la palette en fonction des valeurs (d�sactiv� pour le chgt de pas de temps).
    pal_.setMinPalette(mdVecteur_.getMinNorme());
    pal_.setMaxPalette(mdVecteur_.getMaxNorme());
    pal_.ajustePlages();
    pal_.ajusteLegendes();
    construitLegende();
  }
  // <<< ZModeleChangeListener  ------------------------------------------------
  /**
   * Construction de la legende. En fait, affecte la legende du calque au calque
   * d'affichage des legendes.
   */
  protected void construitLegende() {
    BCalqueLegende cqLg= getLegende();
    if (cqLg == null)
      return;
    cqLg.enleve(this);
    //    p.setOpaque(false);
    //    cqLg.ajoute(this, paletteLeg, p);
    cqLg.ajoute(this, pal_);
  }
  // Paint
  public void paintComponent(Graphics _g) {
    boolean attenue= isAttenue();
    boolean rapide= isRapide();
    int i;
    TraceGeometrie tg= new TraceGeometrie( getVersEcran());
    //    TraceSymbole ts=new TraceSymbole((Graphics2D)_g);
    if (boite_ == null)
      boite_= getModeleMaillage().getDomaine();
    GrMorphisme versEcran= getVersEcran();
    Polygon pecr= boite_.enPolygoneXY().applique(versEcran).polygon();
    Rectangle clip= _g.getClipBounds();
    if (clip == null)
      clip= new Rectangle(0, 0, getWidth(), getHeight());
    if (clip.intersects(pecr.getBounds())) {
      if (rapide) {
        Color c;
        c= pal_.getCouleurAutres();
        if (attenue)
          c= attenueCouleur(c);
        _g.setColor(c);
        _g.drawPolygon(pecr);
      } else {
        Color fg= getForeground();
        Color bg= getBackground();
        if (attenue)
          fg= attenueCouleur(fg);
        if (attenue)
          bg= attenueCouleur(bg);
        int n= mdMaillage_.nombreNoeuds();
        if (isMeshVisible_) {
          _g.setColor(fg);
          for (i= 0; i < mdMaillage_.nombrePolygones(); i++) {
            Polygon p= mdMaillage_.polygone(i).applique(versEcran).polygon();
            //            if (!clip.intersects(p.getBounds())) continue;
            _g.drawPolygon(p);
          }
        }
        //        double minNorm=mdVecteur_.getMinNorme();
        //        double maxNorm=mdVecteur_.getMaxNorme();
        double minPal= pal_.getMinPalette();
        double maxPal= pal_.getMaxPalette();
        int minSize= pal_.getTaillePlage(0);
        int maxSize= pal_.getTaillePlage(pal_.getNbPlages() - 1);
        GrSymbole s= new GrSymbole(GrSymbole.FLECHE, null, 0, 1.);
        for (i= 0; i < n; i++) {
          Point p= mdMaillage_.point(i).applique(versEcran).point();
          if (!clip.contains(p))
            continue;
          double norme= mdVecteur_.getNorme(i);
          if (norme < minPal || norme > maxPal)
            continue; // If norm outside palette limits, no trace.
          s.position_= mdMaillage_.point(i);
          s.direction_.x_= -mdVecteur_.getValeurY(i);
          // For including symbol initial -90� rotation
          s.direction_.y_= mdVecteur_.getValeurX(i);
          s.echelle_=
            minSize
              + (norme - minPal) / (maxPal - minPal) * (maxSize - minSize);
          Color c= pal_.couleur(norme);
          if (c != null) { // if null, "other" values mustn't be drawn.
            tg.setForeground(pal_.couleur(norme));
            tg.dessineSymbole( (Graphics2D) _g,s, false, rapide);
          }
        }
        /*        Symbole s=new Symbole(Symbole.FLECHE,0);

                GrVecteur v=new GrVecteur();
                for (i=0;i<n;i++) {
                  Point p=mdMaillage_.point(i).applique(versEcran).point();
                  if (!clip.contains(p)) continue;

                  v.x=mdVecteur_.getValeurX(i);
                  v.y=mdVecteur_.getValeurY(i);
                  v=v.applique(versEcran);
                  s.rotationZ=Math.atan2(-v.y,v.x)*180/Math.PI;
                  int taille=minSize+(int)((mdVecteur_.getNorme(i)-minNorm)/(maxNorm-minNorm)*(maxSize-minSize));

                  ts.setCouleur(pal_.couleur(mdVecteur_.getNorme(i)));
                  ts.setTaille(taille);
                  ts.dessineSymboleAncre(s,p.x,p.y);
                }*/
      }
    }
  }
  public GrBoite getDomaine() {
    GrBoite r= super.getDomaine();
    if (mdMaillage_ != null) {
      GrBoite b= mdMaillage_.getDomaine();
      if (r == null)
        r= b;
      else
        r= r.union(b);
    }
    return r;
  }
}
