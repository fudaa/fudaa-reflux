/*
 * @file         PRInterfaceCalcul.java
 * @creation     2001-01-20
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.io.File;
import java.io.IOException;
/**
 * Classe d'interfacage entre les objets du serveur de calcul et les objets
 * Reflux.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public abstract class PRInterfaceCalcul {
  /**
   * Lancement du calcul pour le projet.
   */
  public abstract void calculer(PRProjet _prj);
  /**
   * Transfert des donn�es du projet vers les fichiers d'entr�es de Reflux.
   *
   * @param _prj Projet � transf�rer sur fichiers d'entr�e.
   * @param _rac Racine des fichiers avec le chemin.
   */
  public abstract void versFichiersEntree(PRProjet _prj, File _rac)
    throws IOException;
  /**
   * R�cup�ration des solutions finales. Ces solutions finales sont �quivalentes
   * aux solutions utilis�es comme solutions initiales.
   *
   * @param _prj Le projet.
   * @return Les solutions finales.
   */
  public abstract PRSolutionsInitiales[] getSolutionsFinales(
    PRProjet _prj,
    File _rac)
    throws IOException;
  /**
   * R�cup�ration des solutions finales depuis le serveur.
   *
   * @param _prj Le projet.
   * @return Les solutions finales.
   */
  public abstract PRSolutionsInitiales[] getSolutionsFinales(PRProjet _prj)
    throws IOException;
  /**
   * R�cup�ration des r�sultats nodaux sur un fichier.
   *
   * @param _prj Le projet.
   * @param _rac La racine des fihciers du projet.
   * @return Les r�sultats nodaux.
   */
  public abstract PRResultats getResultats(PRProjet _prj, File _rac)
    throws IOException;
  /**
   * Retourne si le serveur a pu �tre joint et est op�rationnel.
   * @return <i>true</i> Le serveur est pr�sent.
   *         <i>false</i> Le serveur est inexistant
   */
  public abstract boolean serveurExiste();
  /**
   * Retourne si le calcul s'est bien d�roul�.
   * @return <i>true</i> : Ok, <i>false</i> : Probl�me.
   */
  public abstract boolean calculEstOK();
  /**
   * Retourne la trace d'ex�cution du calcul.
   * @return La chaine d'ex�cution.
   */
  public abstract String calculTraceExecution();
}
