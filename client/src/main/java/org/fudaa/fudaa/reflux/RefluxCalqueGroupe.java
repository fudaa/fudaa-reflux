/*g
 * @file         RefluxCalqueElement.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Rectangle;

import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.VecteurGrContour;
/**
 * Un calque d'affichage de groupes.
 *
 * @version      $Revision: 1.4 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxCalqueGroupe extends BCalqueAffichage {
  private VecteurGrContour groupes_;
  private GrBoite boite_;

  public RefluxCalqueGroupe() {
    super();
    setDestructible(false);
    groupes_= new VecteurGrContour();
    boite_= null;
  }
  /**
   * Trac� des groupes.
   */
  public void paintComponent(Graphics _g) {
    if (groupes_.nombre() > 0) {
      int i;
      Polygon p;
      GrElement ele;
      GrMorphisme versEcran= getVersEcran();
      Polygon pecr= getDomaine().enPolygoneXY().applique(versEcran).polygon();
      Rectangle clip= _g.getClipBounds();
      if (clip == null)
        clip= new Rectangle(0, 0, getWidth(), getHeight());
      if (clip.intersects(pecr.getBounds())) {
        if (isRapide()) {
          Color c= Color.black;
          _g.setColor(isAttenue() ? attenueCouleur(c) : c);
          _g.drawPolygon(pecr);
        }

        // Affichage des groupes
        else {
          for (int j=0; j<groupes_.nombre(); j++) {
            RefluxGroupe grp = ((RefluxGrGroupe)groupes_.renvoie(j)).grp_;
            Object[] objs=grp.getObjects();

            for (i=0; i<objs.length; i++) {
              if (objs[i] instanceof GrElement) {
                ele= (GrElement)objs[i];
                p= ele.applique(versEcran).polygon();

                if (clip.intersects(p.getBounds())) {
                  Color c= Color.cyan;
                  _g.setColor(isAttenue() ? attenueCouleur(c) : c);
                  _g.drawPolygon(p);
                }
              }
            }
          }
        }
      }
    }
    super.paintComponent(_g);
  }

  /**
   * Ajout d'un <I>GrElement</I> au calque.
   */
  public void add(RefluxGroupe _grp) {
    groupes_.ajoute(new RefluxGrGroupe(_grp));
    boite_= null;
  }
  /**
   * Suppression d'un <I>GrElement</I> du calque.
   */
  public void remove(RefluxGroupe _grp) {
    for (int i=0; i<groupes_.nombre(); i++)
      if (((RefluxGrGroupe)groupes_.renvoie(i)).grp_==_grp) {
        groupes_.enleve(i);
        break;
      }
    boite_= null;
  }
  /**
   * Suppression de tous les <I>GrElement</I>.
   */
  public void removeAll() {
    groupes_.vide();
    boite_= null;
  }

  /**
   * Liste des �l�ments s�lectionnables.
   */
  public VecteurGrContour contours() {
    return groupes_;
  }
  /**
   * Boite englobante des objets contenus dans le calque.
   * @return Boite englobante. Si le calque est vide,
   * la boite englobante retourn�e est <I>null</I>
   */
  public GrBoite getDomaine() {
    GrBoite boiteGroupe;
    if (boite_ == null && groupes_.nombre() > 0) {
      boite_= new GrBoite();
      for (int i= 0; i < groupes_.nombre(); i++) {
        boiteGroupe= ((RefluxGrGroupe)groupes_.renvoie(i)).boite();
        boite_.ajuste(boiteGroupe.o_);
        boite_.ajuste(boiteGroupe.e_);
      }
    }
    return boite_;
  }
}
