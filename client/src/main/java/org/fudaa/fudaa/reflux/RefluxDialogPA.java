/*
 * @file         RefluxDialogPA.java
 * @creation     1998-06-17
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuCheckBox3States;
import com.memoire.bu.BuGridLayout;

import org.fudaa.dodico.corba.mesure.IEvolution;
import org.fudaa.dodico.corba.mesure.IEvolutionConstante;

import org.fudaa.dodico.objet.UsineLib;
/**
 * Une boite de dialogue permettant d'afficher les propri�t�s + nature des
 * ar�tes.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxDialogPA extends RefluxDialog {
  JComboBox coNature;
  JPanel pnProprietes;
  private PRProjet probleme_;
  Hashtable bordToNature;
  Object[] iSelect_;
  int[] typesPE;
  String[] nomCourbes_;
  int PEImposeDefaut= BuCheckBox3States.STATE_DESELECTED;
  int PEStatDefaut= BuCheckBox3States.STATE_DESELECTED;
  Double PEValeurDefaut= new Double(0.0);
  public RefluxDialogPA() {
    this(null);
  }
  public RefluxDialogPA(Frame _parent) {
    super(_parent, OK_CANCEL_APPLY_OPTION);
    nomCourbes_= null;
    /**Jbuilder**/
    jbInit();
  }
  public void jbInit() { /**JBuilder**/
    JPanel pnNature= new JPanel();
    FlowLayout lyNature= new FlowLayout();
    JLabel laNature= new JLabel();
    TitledBorder tiProprietes= new TitledBorder("");
    CardLayout lyProprietes= new CardLayout();
    coNature= new JComboBox();
    pnProprietes= new JPanel();
    laNature.setText("Nature");
    pnNature.setLayout(lyNature);
    pnNature.add(laNature, null);
    pnNature.add(coNature, null);
    tiProprietes.setTitle("Propri�t�s");
    pnProprietes.setLayout(lyProprietes);
    pnProprietes.setBorder(tiProprietes);
    pnAffichage.add(pnProprietes, BorderLayout.CENTER);
    pnAffichage.add(pnNature, BorderLayout.NORTH);
    setModal(true);
    setTitle("Propri�t�s des bords");
    pack();
    setSize(getPreferredSize());
  }
  /**
   * Initialisation du dialog pour le probleme donn� pour les objets selectionn�s.
   */
  public void initialise(PRProjet _probleme, Object[] _iSelect) {
    probleme_= _probleme;
    iSelect_= _iSelect;
    PRNature nature= null;
    Vector supports;
    String nomTpNat;
    int[] PEImpose= null;
    int[] PEStat= null;
    Double[] PEValeur= null; // un PEValeur[i] == null repr�sente un �tat mixte
    IEvolution[] PECourbe= null;
    // un PECourbe[i] == null repr�sente un �tat mixte
    Vector natures= probleme_.modeleProprietes().naturesBords();
    IEvolution[] courbes= probleme_.evolutions();
    // Nom des courbes
    nomCourbes_= new String[courbes.length];
    for (int i= 0; i < courbes.length; i++)
      nomCourbes_[i]= probleme_.getName(courbes[i]);
    // Construction de la table bord->nature
    bordToNature= new Hashtable(natures.size() > 0 ? natures.size() : 1);
    for (int i= 0; i < natures.size(); i++) {
      bordToNature.put(
        ((PRNature)natures.elementAt(i)).supports().get(0),
        natures.elementAt(i));
    }
    // Recherche du type de nature pour tous les bords s�lectionn�s (on consid�re
    // que tous les bords ont une nature)
    for (int i= 0; i < _iSelect.length; i++) {
      if (i == 0)
        nature= (PRNature)bordToNature.get(_iSelect[i]);
      else {
        if (((PRNature)bordToNature.get(_iSelect[i])).type()
          != nature.type()) {
          nature= null;
          break;
        }
      }
    }
    if (nature == null) {
      nomTpNat= "<mixte>";
      typesPE= new int[0];
    }
    // Nature semblable => Recherche des propri�t�s communes
    else {
      nomTpNat= Definitions.natureToString(nature.type());
      typesPE= Definitions.getTypesProprietesElementaires(nature.type());
      PRPropriete[] iPE= probleme_.modeleProprietes().proprietesAretes();
      PRPropriete iPECourante;
      // Cr�ation des tables bord->PE
      Hashtable[] bordToPE= new Hashtable[typesPE.length];
      for (int i= 0; i < typesPE.length; i++)
        bordToPE[i]= new Hashtable();
      for (int i= 0; i < iPE.length; i++) {
        for (int j= 0; j < typesPE.length; j++) {
          if (iPE[i].type() == typesPE[j]) {
            supports= iPE[i].supports();
            for (int k= 0; k < supports.size(); k++)
              bordToPE[j].put(supports.get(k), iPE[i]);
            break;
          }
        }
      }
      // Recherche du niveau d'�quivalence des propri�t�s pour les bords
      // s�lectionn�s
      int impose= PEImposeDefaut;
      int stationnaire= PEStatDefaut;
      Double valeur= PEValeurDefaut;
      IEvolution courbe= null;
      PEImpose= new int[typesPE.length];
      PEStat= new int[typesPE.length];
      PEValeur= new Double[typesPE.length];
      PECourbe= new IEvolution[typesPE.length];
      for (int j= 0; j < typesPE.length; j++) {
        for (int i= 0; i < _iSelect.length; i++) {
          iPECourante= (PRPropriete)bordToPE[j].get(_iSelect[i]);
          if (iPECourante == null) {
            impose= BuCheckBox3States.STATE_DESELECTED;
          } else {
            impose= BuCheckBox3States.STATE_SELECTED;
            if (iPECourante.evolution() instanceof IEvolutionConstante) {
              stationnaire= BuCheckBox3States.STATE_DESELECTED;
              valeur=
                new Double(
                  ((IEvolutionConstante)iPECourante.evolution()).constante());
            } else {
              stationnaire= BuCheckBox3States.STATE_SELECTED;
              courbe= iPECourante.evolution();
            }
          }
          // Premier �l�ment
          if (i == 0) {
            PEImpose[j]= impose;
            PEStat[j]= stationnaire;
            PEValeur[j]= valeur;
            PECourbe[j]= courbe;
          }
          // Les autres
          else {
            if (PEImpose[j] != impose) {
              PEImpose[j]= BuCheckBox3States.STATE_MIXED;
            } else if (PEImpose[j] == BuCheckBox3States.STATE_SELECTED) {
              if (PEStat[j] != stationnaire) {
                PEStat[j]= BuCheckBox3States.STATE_MIXED;
              } else if (PEStat[j] == BuCheckBox3States.STATE_SELECTED) {
                if (PECourbe[j] != courbe) {
                  PECourbe[j]= null; // Etat mixte
                }
              } else {
                if (PEValeur[j] != null && !PEValeur[j].equals(valeur)) {
                  PEValeur[j]= null; // Etat mixte
                }
              }
            }
          }
        }
      }
    }
    // Initialisation de l'interface
    initInterface();
    // S�lection de la nature
    coNature.setSelectedItem(nomTpNat);
    //    changeNature(coNature, nomTpNat);
    // S�lection des autres composants (propri�t�s)
    {
      JPanel sspn=
        (JPanel)pnProprietes.getComponent(coNature.getSelectedIndex());
      JPanel pnValeur;
      Component[] cp= sspn.getComponents();
      for (int i= 0; i < typesPE.length; i++) {
        ((BuCheckBox3States)cp[i * 3]).setState(PEImpose[i]);
        //        changePE((BuCheckBox3States)cp[i*3],PEImpose[i]);
        if (PEImpose[i] != BuCheckBox3States.STATE_MIXED) {
          ((BuCheckBox3States)cp[i * 3 + 1]).setState(PEStat[i]);
          //          changeCas((BuCheckBox3States)cp[i*3+1],PEStat[i]);
          pnValeur= (JPanel)cp[i * 3 + 2];
          if (PEStat[i] == BuCheckBox3States.STATE_SELECTED) {
            changeCourbe((JComboBox)pnValeur.getComponent(1), PECourbe[i]);
          } else if (PEStat[i] == BuCheckBox3States.STATE_DESELECTED) {
            changeValeur((JTextField)pnValeur.getComponent(0), PEValeur[i]);
          }
        }
      }
    }
  }
  // ---------------------------------------------------------------------------
  // Initialisation de l'interface
  // ---------------------------------------------------------------------------
  private void initInterface() {
    int[] typesNat;
    //int[] typesPE;
    JPanel sspnProprietes;
    JPanel pnValeur;
    JTextField tfValeur;
    JComboBox coCourbe;
    BuCheckBox3States cbImpose;
    BuCheckBox3States cbTransitoire;
    typesNat= Definitions.getTypesNaturesBord();
    coNature.removeAllItems();
    coNature.addItem("<mixte>");
    for (int i= 0; i < typesNat.length; i++)
      coNature.addItem(Definitions.natureToString(typesNat[i]));
    coNature.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        nature_itemStateChanged(_evt);
      }
    });
    pnProprietes.removeAll();
    pnProprietes.add(new JPanel(), new Integer(0).toString());
    // Construction de tous les panels proprietes pour chaque nature
    for (int i= 0; i < typesNat.length; i++) {
      sspnProprietes= new JPanel();
      sspnProprietes.setLayout(new BuGridLayout(3, 5, 5, false, false));
      pnProprietes.add(sspnProprietes, new Integer(i + 1).toString());
      typesPE= Definitions.getTypesProprietesElementaires(typesNat[i]);
      for (int j= 0; j < typesPE.length; j++) {
        cbImpose= new BuCheckBox3States();
        cbImpose.setText(Definitions.proprieteToString(typesPE[j]));
        cbImpose.setState(BuCheckBox3States.STATE_DESELECTED);
        cbImpose.addItemListener(new ItemListener() {
          public void itemStateChanged(ItemEvent _evt) {
            propriete_itemStateChanged(_evt);
          }
        });
        sspnProprietes.add(cbImpose);
        cbTransitoire= new BuCheckBox3States();
        cbTransitoire.setText("Transitoire");
        cbTransitoire.setState(BuCheckBox3States.STATE_DESELECTED);
        cbTransitoire.setEnabled(false);
        cbTransitoire.addItemListener(new ItemListener() {
          public void itemStateChanged(ItemEvent _evt) {
            cas_itemStateChanged(_evt);
          }
        });
        sspnProprietes.add(cbTransitoire);
        tfValeur= new JTextField("0.0");
        tfValeur.setPreferredSize(new Dimension(70, 19));
        tfValeur.setEnabled(false);
        coCourbe= new JComboBox();
        coCourbe.setEnabled(false);
        pnValeur= new JPanel();
        pnValeur.setLayout(new CardLayout());
        pnValeur.add(
          tfValeur,
          new Integer(BuCheckBox3States.STATE_DESELECTED).toString());
        pnValeur.add(
          coCourbe,
          new Integer(BuCheckBox3States.STATE_SELECTED).toString());
        sspnProprietes.add(pnValeur);
      }
    }
    initListesCourbes();
    pack();
    setSize(getPreferredSize());
  }
  private void initListesCourbes() {
    if (nomCourbes_.length == 0)
      return;
    Component[] cp;
    Component[] sspn= pnProprietes.getComponents();
    JComboBox coCourbe;
    for (int i= 0; i < sspn.length; i++) {
      cp= ((JPanel)sspn[i]).getComponents();
      for (int j= 0; j < cp.length; j += 3) {
        coCourbe= (JComboBox) ((JPanel)cp[j + 2]).getComponent(1);
        coCourbe.removeAllItems();
        coCourbe.addItem("<mixte>");
        for (int k= 0; k < nomCourbes_.length; k++)
          coCourbe.addItem(nomCourbes_[k]);
        coCourbe.addItemListener(new ItemListener() {
          public void itemStateChanged(ItemEvent _evt) {
            courbe_itemStateChanged(_evt);
          }
        });
      }
    }
  }
  // ---------------------------------------------------------------------------
  // Changement de nature
  // ---------------------------------------------------------------------------
  private void changeNature(JComboBox _coNature, String _nomTpNat) {
    String card= new Integer(coNature.getSelectedIndex()).toString();
    ((CardLayout)pnProprietes.getLayout()).show(pnProprietes, card);
  }
  // ---------------------------------------------------------------------------
  // Changement d'�tat d'une propri�t� (Libre/Impos�/Mixte)
  // ---------------------------------------------------------------------------
  private void changePE(BuCheckBox3States _cbImpose, int _state) {
    JPanel pn= (JPanel)_cbImpose.getParent();
    Component[] cp= pn.getComponents();
    Component[] sscp;
    int i;
    for (i= 0; i < cp.length; i += 3)
      if (_cbImpose == cp[i])
        break;
    sscp= ((JPanel)cp[i + 2]).getComponents();
    switch (_state) {
      // Cas Impos�
      case BuCheckBox3States.STATE_SELECTED :
        cp[i + 1].setEnabled(true);
        sscp[0].setEnabled(true);
        sscp[1].setEnabled(true);
        break;
        // Cas libre/mixte => Pas de modification possible de la propri�t�
      case BuCheckBox3States.STATE_DESELECTED :
      case BuCheckBox3States.STATE_MIXED :
        cp[i + 1].setEnabled(false);
        sscp[0].setEnabled(false);
        sscp[1].setEnabled(false);
        break;
    }
  }
  // ---------------------------------------------------------------------------
  // Changement d'�tat du cas (Stationnaire/Transitoire/Mixte)
  // ---------------------------------------------------------------------------
  private void changeCas(BuCheckBox3States _cbTransitoire, int _state) {
    // Pas de courbes, pas de changement d'�tat
    if (_state == BuCheckBox3States.STATE_SELECTED
      && probleme_.evolutions().length == 0) {
      _cbTransitoire.setState(BuCheckBox3States.STATE_DESELECTED);
      System.out.println(
        "Erreur : Pas de courbes dans la base de donnees."
          + " Impossible de definir une PA transitoire");
      return;
    }
    JPanel pn= (JPanel)_cbTransitoire.getParent();
    Component[] cp= pn.getComponents();
    Component[] sscp;
    int i;
    for (i= 1; i < cp.length; i += 3)
      if (_cbTransitoire == cp[i])
        break;
    sscp= ((JPanel)cp[i + 1]).getComponents();
    switch (_state) {
      // Cas transitoire/ Cas non stationnaire
      case BuCheckBox3States.STATE_SELECTED :
      case BuCheckBox3States.STATE_DESELECTED :
        CardLayout ly= ((CardLayout) ((JPanel)cp[i + 1]).getLayout());
        ly.show((JPanel)cp[i + 1], new Integer(_state).toString());
        sscp[0].setEnabled(true);
        sscp[1].setEnabled(true);
        break;
        // Cas mixte => On ne change rien
      case BuCheckBox3States.STATE_MIXED :
        sscp[0].setEnabled(false);
        sscp[1].setEnabled(false);
        break;
    }
  }
  // ---------------------------------------------------------------------------
  // Changement de la valeur d'une propri�t�
  // ---------------------------------------------------------------------------
  private void changeValeur(JTextField _tfValeur, Double _valeur) {
    // Valeur donn�e
    if (_valeur != null)
      _tfValeur.setText(_valeur.toString());
    // Valeur nulle => Cas mixte : Texte vide
    else
      _tfValeur.setText("");
  }
  // ---------------------------------------------------------------------------
  // Changement de la courbe associ�e � une propri�t�
  // ---------------------------------------------------------------------------
  private void changeCourbe(JComboBox _coCourbe, IEvolution _courbe) {
    // Courbe donn�e
    if (_courbe != null)
      _coCourbe.setSelectedItem(probleme_.getName(_courbe));
    // Valeur nulle => Cas mixte
    else
      _coCourbe.setSelectedItem("<mixte>");
  }
  void nature_itemStateChanged(ItemEvent _evt) {
    if (coNature.getSelectedItem() == _evt.getItem()) {
      /*      if (_evt.getItem() != "<mixte>") {
              for (int i=0; i<listeNature.getItemCount(); i++) {
                if (listeNature.getItemAt(i) == "<mixte>") listeNature.removeItemAt(i);
              }
            } */
      changeNature(coNature, (String)coNature.getSelectedItem());
      System.out.println("Nature modifi�e");
    }
  }
  // ---------------------------------------------------------------------------
  // Changement d'�tat de propri�t�
  // ---------------------------------------------------------------------------
  void propriete_itemStateChanged(ItemEvent _evt) {
    BuCheckBox3States cbImpose= (BuCheckBox3States)_evt.getSource();
    int state= cbImpose.getState();
    // On n'autorise pas le passage � mixte
    /*    if (state == BuCheckBox3States.STATE_MIXED) {
          checkBoxPE.setState((state+1)%3);
          return;
        } */
    changePE(cbImpose, state);
    System.out.println("Propri�t� modifi�e");
  }
  void cas_itemStateChanged(ItemEvent _evt) {
    BuCheckBox3States cbTransitoire= (BuCheckBox3States)_evt.getSource();
    int state= cbTransitoire.getState();
    // On n'autorise pas le passage � mixte
    /*    if (state == BuCheckBox3States.STATE_MIXED) {
          checkBoxCas.setState((state+1)%3);
          return;
        } */
    changeCas(cbTransitoire, state);
    System.out.println("Cas modifi�");
  }
  // ---------------------------------------------------------------------------
  // Changement de courbe
  // ---------------------------------------------------------------------------
  void courbe_itemStateChanged(ItemEvent _evt) {
    JComboBox coCourbe= (JComboBox)_evt.getSource();
    if (coCourbe.getSelectedItem() == _evt.getItem()) {
      /*      if (_evt.getItem() != "<mixte>") {
              for (int i=0; i<cb.getItemCount(); i++) {
                if (cb.getItemAt(i) == "<mixte>") cb.removeItemAt(i);
              }
            } */
      //      changeCourbe(listeNature, (String) listeNature.getSelectedItem());
      System.out.println("Courbe modifi�e");
    }
  }
  /**
   * Retourne le type de nature selectionn�.
   * @return le type de nature. <I>null</I> si le type est mixte.
   */
  public int getTypeNature() {
    return Definitions.stringToNature((String)coNature.getSelectedItem());
  }
  /**
   * Retourne les propri�tes �l�mentaires affect�es.
   * @return les propri�tes �l�mentaires affect�es. Si aucune propri�t� n'a
   *         �t� affect�e, le tableau en retour est vide
   */
  public PRPropriete[] getProprietes() {
    BuCheckBox3States[] cbImpose;
    BuCheckBox3States[] cbTransitoire;
    JTextField[] tfValeur;
    JComboBox[] coCourbe;
    JPanel sspn;
    JPanel pnValeur;
    int typeNat;
    //int[] typesPE;
    IEvolution[] courbes= probleme_.evolutions();
    Vector vctPE= new Vector();
    IEvolution evolution;
    typeNat= Definitions.stringToNature((String)coNature.getSelectedItem());
    typesPE= Definitions.getTypesProprietesElementaires(typeNat);
    sspn= (JPanel)pnProprietes.getComponent(coNature.getSelectedIndex());
    cbImpose= new BuCheckBox3States[typesPE.length];
    cbTransitoire= new BuCheckBox3States[typesPE.length];
    tfValeur= new JTextField[typesPE.length];
    coCourbe= new JComboBox[typesPE.length];
    for (int i= 0; i < typesPE.length; i++) {
      cbImpose[i]= (BuCheckBox3States)sspn.getComponent(i * 3);
      cbTransitoire[i]= (BuCheckBox3States)sspn.getComponent(i * 3 + 1);
      pnValeur= (JPanel)sspn.getComponent(i * 3 + 2);
      tfValeur[i]= (JTextField)pnValeur.getComponent(0);
      coCourbe[i]= (JComboBox)pnValeur.getComponent(1);
    }
    for (int i= 0; i < typesPE.length; i++) {
      // On passe cette propri�t� si elle n'est pas s�lectionn�e ou mixte
      if (cbImpose[i].getState() != BuCheckBox3States.STATE_SELECTED)
        continue;
      if (cbTransitoire[i].getState() == BuCheckBox3States.STATE_MIXED)
        continue;
      if (cbTransitoire[i].getState() == BuCheckBox3States.STATE_DESELECTED
        && tfValeur[i].getText().equals(""))
        continue;
      if (cbTransitoire[i].getState() == BuCheckBox3States.STATE_SELECTED
        && coCourbe[i].getSelectedIndex() == 0)
        continue;
      // Cr�ation de la propri�t�
      if (cbTransitoire[i].getState() == BuCheckBox3States.STATE_SELECTED) {
        evolution= courbes[coCourbe[i].getSelectedIndex() - 1];
      } else {
        evolution=UsineLib.findUsine().creeMesureEvolutionComposee();
/*          IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());*/
        double constante= new Double(tfValeur[i].getText()).doubleValue();
        ((IEvolutionConstante)evolution).constante(constante);
      }
      vctPE.addElement(new PRPropriete(typesPE[i], evolution, new Vector()));
    }
    PRPropriete[] tabPE= new PRPropriete[vctPE.size()];
    vctPE.copyInto(tabPE);
    return tabPE;
  }
}
