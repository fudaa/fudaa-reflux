/*
 * @file         RefluxTacheOperation.java
 * @creation     1999-01-11
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuTaskOperation;
/**
 * Une classe permettant d'�x�cuter une tache et d'afficher l'�tat
 * d'avancement de cette tache en m�me temps qu'elle se d�roule.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxTacheOperation extends BuTaskOperation {
  //  public String operation  ="";
  //  public int    progression=0;
  BuCommonInterface app_;
  public RefluxTacheOperation(BuCommonInterface _app, String _nom) {
    super(_app, _nom);
    app_= _app;
    new ThreadMessage(_app, this).start();
  }
  public void setOperation(String _operation) {
    //    operation=_operation;
    app_.getMainPanel().setMessage(_operation);
  }
  //  public void   setProgression(int _progression) {
  //    progression=_progression;
  //    app_.getMainPanel().setProgression(_progression);
  //  }
  public void setProgres(int _progression) {
    //    progression=_progression;
    app_.getMainPanel().setProgression(_progression);
  }
  class ThreadMessage extends Thread {
    RefluxTacheOperation op_;
    //    BuCommonInterface    app_;
    public ThreadMessage(BuCommonInterface _app, RefluxTacheOperation _op) {
      super();
      op_= _op;
      //      app_=_app;
    }
    public void run() {
      while (!op_.isAlive())
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {}
      while (op_.isAlive()) {
        //        app_.getMainPanel().setMessage(op_.operation);
        //        app_.getMainPanel().setProgression(op_.progression);
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {}
      }
      app_.getMainPanel().setMessage("");
      app_.getMainPanel().setProgression(0);
    }
  }
}
