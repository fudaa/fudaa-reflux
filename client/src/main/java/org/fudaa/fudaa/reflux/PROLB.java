/*
 * @file         PROLB.java
 * @creation     2001-03-15
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.util.Hashtable;

import org.fudaa.dodico.corba.olb.ICalculOlb;
import org.fudaa.dodico.corba.olb.ICalculOlbHelper;
import org.fudaa.dodico.corba.olb.IParametresOlb;
import org.fudaa.dodico.corba.olb.IParametresOlbHelper;
import org.fudaa.dodico.corba.olb.IResultatsOlb;
import org.fudaa.dodico.corba.olb.IResultatsOlbHelper;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;

import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrNoeud;

import org.fudaa.fudaa.commun.conversion.FudaaMaillageElement;
/**
 * Optimisation de largeur de bande pour un maillage.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public final class PROLB {
  
  private PROLB() {}
  /**
   * Serveur d'optimisation de largeur de bande.
   */
  private static ICalculOlb olbCal_;
  /**
   * Recherche ou non d'un serveur distant.
   */
  public static boolean distant;
  /**
   * Initialisation du serveur de calcul.
   * @param _distant
   *  <i>true</i>Recherche un serveur de calcul distant, et cr�e un serveur
   *             local en cas d'�chec.
   *  <i>false</i>Cr�e un serveur local sans recherche d'un distant.
   */
  public static void initialiser(boolean _distant) {
    if (_distant) {
      System.out.println(
        "Recherche d'un serveur d'optimisation de largeur de "
          + "bande distant...");
      olbCal_=
        ICalculOlbHelper.narrow(
          CDodico.findServerByInterface("::olb::ICalculOLB", 4000));
    }
    if (olbCal_ == null) {
      System.out.println(
        "Cr�ation d'un serveur d'optimisation de largeur de "
          + "bande local...");
      olbCal_= UsineLib.findUsine().creeOlbCalculOlb()/*ICalculOlbHelper.narrow(new DCalculOlb().tie())*/;
    }
  }
  /**
   * Optimisation de la largeur de bande pour le maillage donn�. La m�thode
   * retourne un tableau des num�ros des noeuds apr�s optimisation.
   *
   * @return Le tableau d'indices ordonn�es
   */
  public static int[] optimiser(GrMaillageElement _mail) {
    int[] numOpts= null;
    GrNoeud[] nds;
    GrElement[] els;
    //    GrMaillageElement mailInit;
    //    GrNoeud[]         ndsInit;
    //    GrElement[]       elsInit;
    GrMaillageElement mailFinal;
    GrNoeud[] ndsFinal;
    GrElement[] elsFinal;
    Hashtable hndFinal2Num;
    Hashtable hnd2Num;
    //    RefondeNoeudData ndData;
    int tpMail;
    IResultatsOlb olbRes;
    IParametresOlb olbPar;
    nds= _mail.noeuds();
    els= _mail.elements();
    // Optimisation uniquement si tous les �lements T3 ou T6
    tpMail= els[0].type_;
    for (int i= 0; i < els.length; i++) {
      if (els[i].type_ != GrElement.T3 && els[i].type_ != GrElement.T6)
        throw new IllegalArgumentException("Le maillage � optimiser ne peut contenir que des T6 ou des T3");
      if (els[i].type_ != tpMail)
        throw new IllegalArgumentException("Les �l�ments du maillage � optimiser doivent tous �tre de m�me type");
    }
    // Controle d'existence du serveur d'OLB et initialisation si inexistant
    //    if (olbCal_==null) initialiser(distant);
    if (olbCal_ == null)
      olbCal_= Reflux.SERVEUR_OLB;
    //    RefondeImplementation.statusBar.setProgression(20);
    // Noeud maillage => Indice dans table des noeuds
    hnd2Num= new Hashtable(nds.length);
    for (int i= 0; i < nds.length; i++)
      hnd2Num.put(nds[i], new Integer(i));
    if (nds.length > 0) {
      olbPar= IParametresOlbHelper.narrow(olbCal_.parametres(Reflux.CONNEXION_OLB));
      olbPar.maillage(FudaaMaillageElement.gr2S(_mail));
      //      RefondeImplementation.statusBar.setProgression(60);
      olbCal_.calcul(Reflux.CONNEXION_OLB);
      //      RefondeImplementation.statusBar.setProgression(70);
      olbRes= IResultatsOlbHelper.narrow(olbCal_.resultats(Reflux.CONNEXION_OLB));
      mailFinal= FudaaMaillageElement.s2Gr(olbRes.maillage());
      elsFinal= mailFinal.elements();
      ndsFinal= mailFinal.noeuds();
      //      RefondeImplementation.statusBar.setProgression(80);
      // Noeud maillage final => Indice dans table des noeuds (num�ro optimis�)
      hndFinal2Num= new Hashtable(ndsFinal.length);
      for (int i= 0; i < ndsFinal.length; i++)
        hndFinal2Num.put(ndsFinal[i], new Integer(i));
      // Num�ro optimis� pour chaque noeud du maillage initial
      numOpts= new int[nds.length];
      for (int i= 0; i < els.length; i++) {
        GrNoeud[] ndsEle= els[i].noeuds_;
        GrNoeud[] ndsEleFinal= elsFinal[i].noeuds_;
        for (int j= 0; j < ndsEle.length; j++) {
          numOpts[((Integer)hnd2Num.get(ndsEle[j])).intValue()]=
            ((Integer)hndFinal2Num.get(ndsEleFinal[j])).intValue();
          //          ndData=(RefondeNoeudData)ndsEleInit[j].data();
          //          if (ndData.numero==0)
          //           ndData.numero=((Integer)nd2Num.get(ndsEleFinal[j])).intValue();
        }
      }
    }
    //    RefondeImplementation.statusBar.setProgression(85);
    // Mise � jour des num�ro de noeuds non rattach�s � un �l�ment T3
    //    {
    //      int nbOpt=ndsInit.length;
    //      for (int i=0; i<nds.length; i++) {
    //        ndData=(RefondeNoeudData)nds[i].data();
    //        if (ndData.numero==0) ndData.numero=++nbOpt;
    //      }
    //    }
    // Mise a jour du num�ro des �l�ments
    //    for (int i=0; i<els.length; i++)
    //     ((RefondeElementData)els[i].data()).numero=i+1;
    //    RefondeImplementation.statusBar.setProgression(90);
    return numOpts;
  }
}
