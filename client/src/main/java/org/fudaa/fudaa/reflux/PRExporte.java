/*
 * @file         PRExporte.java
 * @creation     1998-12-15
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;

import com.memoire.bu.BuPanel;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.mesure.IEvolution;
import org.fudaa.dodico.corba.mesure.IEvolutionSerieIrreguliere;

import org.fudaa.dodico.fortran.FortranWriter;

import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;
/**
 * Une classe pour l'exportation du projet ou partie du projet vers d'autres
 * format.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRExporte {
  private PRProjet projet_;
  private GrMaillageElement maillageT3_;
  //  private BuPanel    panel_;
  /**
   * Cr�ation de la classe avec le projet.
   */
  public PRExporte(BuPanel _panel, PRProjet _projet) {
    projet_= _projet;
    //    panel_     =_panel;
    maillageT3_= null;
  }
  /**
   * Exportation du projet vers des fichiers de maillage Opthyca (noeuds,
   * �lements, fronti�re). Ces fichiers ont respectivement pour extension .opn,
   * .ope, .opf.
   * @param _file Fichiers de stockage des donn�es du probl�me. _file doit avoir
   *              pour extension, ou avoir comme extension .opn, .ope, .opf.
   */
  public void opthycaMaillage(File _file)
    throws IOException {
    if (maillageT3_ == null)
      maillageT3_= projet_.maillage().versLineaire(1);
    String racine;
    String path= _file.getPath();
    //---  Controle de l'extension pass�e  -------------------------------------
    if (path.endsWith(".opn")
      || path.endsWith(".ope")
      || path.endsWith(".opf"))
      racine= path.substring(0, path.lastIndexOf(CtuluLibString.DOT));
    else
      racine= path;
    opthycaNoeuds(new File(racine + ".opn"));
    opthycaElements(new File(racine + ".ope"));
    opthycaFrontiere2D(new File(racine + ".opf"));
  }
  /**
   * Exportation du projet vers le fichier des noeuds d'Opthyca.
   * @param _file Fichier de stockage des noeuds du probl�me.
   */
  public void opthycaNoeuds(File _file)
    throws IOException {
    if (maillageT3_ == null)
      maillageT3_= projet_.maillage().versLineaire(1);
    String version= RefluxImplementation.informationsSoftware().version;
    GrNoeud[] noeuds= maillageT3_.noeuds();
    GrPoint pt;
    PrintWriter out= new PrintWriter(new FileWriter(_file));
    try {
      // Entete
      out.println("<NOEUD>");
      // Descriptif
      out.println(
        "/*======================================"
          + "============================= G.H.N. ===");
      out.println("/* Description : Fichier des noeuds OPTHYCA");
      out.println("/* Cree par    : REFLUX " + version);
      out.println(
        "/*======================================"
          + "========================================");
      // Coordonn�es des noeuds
      for (int i= 0; i < noeuds.length; i++) {
        pt= noeuds[i].point_;
        out.println((i + 1) + "; " + pt.x_ + "; " + pt.y_ + "; " + pt.z_);
      }
    } finally {
      if (out != null)
        out.close();
    }
  }
  /**
   * Exportation du projet vers le fichier des �l�ments d'Opthyca.
   * @param _file Fichier de stockage des �l�ments du probl�me.
   */
  public void opthycaElements(File _file)
    throws IOException {
    if (maillageT3_ == null)
      maillageT3_= projet_.maillage().versLineaire(1);
    String version= RefluxImplementation.informationsSoftware().version;
    GrElement[] elements= maillageT3_.elements();
    GrNoeud[] noeuds;
    PrintWriter out= new PrintWriter(new FileWriter(_file));
    Hashtable nd2Num= new Hashtable();
    {
      GrNoeud[] nds= maillageT3_.noeuds();
      for (int i= 0; i < nds.length; i++)
        nd2Num.put(nds[i], new Integer(i));
    }
    try {
      // Entete
      out.println("<ELEMENT>");
      // Descriptif
      out.println(
        "/*======================================"
          + "============================= G.H.N. ===");
      out.println("/* Description : Fichier des elements OPTHYCA");
      out.println("/* Cree par    : REFLUX " + version);
      out.println(
        "/*======================================"
          + "========================================");
      // Connectivit� des �lements
      for (int i= 0; i < elements.length; i++) {
        noeuds= elements[i].noeuds_;
        out.print(i + 1);
        for (int j= 0; j < noeuds.length; j++)
          out.print("; " + (((Integer)nd2Num.get(noeuds[j])).intValue() + 1));
        out.println("");
      }
    } finally {
      if (out != null)
        out.close();
    }
  }
  /**
   * Exportation du projet vers le fichier des fronti�res 2D d'Opthyca.
   * @param _file Fichier de stockage des fronti�res 2D du probl�me.
   */
  public void opthycaFrontiere2D(File _file)
    throws IOException {
    if (maillageT3_ == null)
      maillageT3_= projet_.maillage().versLineaire(1);
    String version= RefluxImplementation.informationsSoftware().version;
    GrNoeud[][] noeuds= maillageT3_.noeudsContours();
    PrintWriter out= new PrintWriter(new FileWriter(_file));
    Hashtable nd2Num= new Hashtable();
    {
      GrNoeud[] nds= maillageT3_.noeuds();
      for (int i= 0; i < nds.length; i++)
        nd2Num.put(nds[i], new Integer(i));
    }
    try {
      // Entete
      out.println("<FRONTIERE2D>");
      // Descriptif
      out.println(
        "/*======================================"
          + "============================= G.H.N. ===");
      out.println("/* Description : Fichier des frontieres 2D OPTHYCA");
      out.println("/* Cree par    : REFLUX " + version);
      out.println(
        "/*======================================"
          + "========================================");
      // Fronti�res 2D
      for (int i= 0; i < noeuds.length; i++) {
        out.println("\"" + i + 1 + "\"; \"----\"; \"L\"");
        for (int j= 0; j < noeuds[i].length; j++)
          out.println(((Integer)nd2Num.get(noeuds[i][j])).intValue() + 1);
        out.println("END");
      }
      out.println("END");
    } finally {
      if (out != null)
        out.close();
    }
  }
  /**
   * Exportation du projet vers les fichiers de calcul Reflux. Ces fichiers
   * ont pour extension .inp, .siv, .clv, .pnv.
   * @param _file Fichiers de stockage des donn�es du probl�me. _file doit avoir
   *              pour extension, ou avoir comme extension .inp, .siv, .clv, .pnv.
   */
  public void refluxDonnees(File _file)
    throws IOException {
    projet_.getInterfaceCalcul().versFichiersEntree(projet_, _file);
  }
  /**
   * Exportation des courbes en format de fichier .crb 4.0.
   * @param _file Fichier de stockage des courbes du projet.
   * @exception FileNotFoundException Le fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   */
  public void refluxCourbes(File _file)
    throws IOException {
    int[] fmt;
    FortranWriter file= null;
    try {
      // Ouverture du fichier
      file= new FortranWriter(new FileWriter(_file));
      // Num�ro de version courante
      String version= RefluxImplementation.informationsSoftware().version;
      fmt= new int[] { 12, version.length()};
      file.stringField(0, "*!$ Version ");
      file.stringField(1, version);
      file.writeFields(fmt);
      // Boucle sur toutes les courbes cr��es
      IEvolution[] courbes= projet_.evolutions();
      for (int i= 0; i < courbes.length; i++) {
        String nmCourbe= projet_.getName(courbes[i]);
        double[] y= ((IEvolutionSerieIrreguliere)courbes[i]).serie();
        long[] x=
          ((IEvolutionSerieIrreguliere)courbes[i]).instants().instants();
        int nbPoints= y.length;
        // Nom de la courbe
        fmt= new int[] { 20, 1, 5 };
        file.stringField(0, nmCourbe);
        // Nombre de points de la courbe
        file.intField(2, nbPoints);
        file.writeFields(fmt);
        // Pour tous les points
        fmt= new int[] { 10, 1, 10 };
        for (int j= 0; j < nbPoints; j++) {
          file.doubleField(0, x[j]);
          file.doubleField(2, y[j]);
          file.writeFields(fmt);
        }
      }
    } catch (FileNotFoundException exc) {
      throw new FileNotFoundException(_file.getName());
    } catch (IOException exc) {
      throw new IOException("Erreur d'�criture sur " + _file.getName());
    } catch (NumberFormatException exc) {
      throw new IOException("Erreur d'�criture sur " + _file.getName());
    } catch (Exception _exc) {
      throw new IOException(_exc.getMessage());
    } finally {
      // Fermeture du fichier
      if (file != null)
        file.close();
    }
  }
}
