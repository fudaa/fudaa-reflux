package org.fudaa.fudaa.reflux;

import java.util.HashSet;
import java.util.Iterator;

import org.fudaa.fudaa.commun.trace2d.ZModeleChangeEvent;
import org.fudaa.fudaa.commun.trace2d.ZModeleChangeListener;

/**
 * Un modele de vecteurs bas� sur les r�sultats avec changement possible du pas
 * de temps s�lectionn�.
 */
class RefluxModeleResultatsVecteur implements ZModeleVecteur {
  private PRResultats results_;
  private int xtype_= PRResultats.VITESSE_X;
  private int ytype_= PRResultats.VITESSE_Y;
  private Double t_;
  private PRResultats.Etape.Colonne xcol_;
  private PRResultats.Etape.Colonne ycol_;
  private Double minNorm_;
  private Double maxNorm_;
  /** Listeners for modifications in modele. */
  protected HashSet listeners_= new HashSet();
  /**
   * Constructor for speed vector based on x and y speeds.
   */
  public RefluxModeleResultatsVecteur(PRResultats _results) {
    results_= _results;
  }
  /**
   * Pour changer le pas de temps sur le modele.
   */
  public void setSelectedStep(Double _t) {
    t_= _t;
    // Clear cache tables/values
    xcol_= null;
    ycol_= null;
    minNorm_= null;
    maxNorm_= null;
    fireModelChange(
      new ZModeleChangeEvent(this, ZModeleChangeEvent.VALUES_CHANGED));
  }
  /**
   * Retourne le pas de temps s�lectionn�.
   */
  public Double getSelectedStep() {
    return t_;
  }
  /**
   * Retourne l'index du pas.
   */
  public int getIndexOfStep(Double _t) {
    if (_t == null)
      return -1;
    for (int i= 0; i < results_.getNbSteps(); i++)
      if (results_.getStep(i).t == _t.doubleValue())
        return i;
    return -1;
  }
  protected void fireModelChange(ZModeleChangeEvent _evt) {
    for (Iterator i= listeners_.iterator(); i.hasNext();) {
      ((ZModeleChangeListener)i.next()).modelChanged(_evt);
    }
  }
  private PRResultats.Etape.Colonne getXCol() {
    if (xcol_ == null) {
      int ind= getIndexOfStep(t_);
      if (ind == -1)
        return null;
      xcol_= results_.getStep(ind).getColonneOfType(xtype_);
    }
    return xcol_;
  }
  private PRResultats.Etape.Colonne getYCol() {
    if (ycol_ == null) {
      int ind= getIndexOfStep(t_);
      if (ind == -1)
        return null;
      ycol_= results_.getStep(ind).getColonneOfType(ytype_);
    }
    return ycol_;
  }
  // >>> ZModeleVecteur  -------------------------------------------------------
  /**
   * Valeur x pour l'index i.
   * @return La valeur ou Double.NaN si aucune valeur pour l'index donn�.
   */
  public double getValeurX(int i) {
    if (getXCol() == null)
      return Double.NaN;
    if (xcol_.valeurs.length <= i || i < 0)
      return Double.NaN;
    return xcol_.valeurs[i];
  }
  /**
   * Valeur y pour l'index i.
   * @return La valeur ou Double.NaN si aucune valeur pour l'index donn�.
   */
  public double getValeurY(int i) {
    if (getYCol() == null)
      return Double.NaN;
    if (ycol_.valeurs.length <= i || i < 0)
      return Double.NaN;
    return ycol_.valeurs[i];
  }
  /**
   * Norme pour l'index i.
   * @return La valeur ou Double.NaN si aucune valeur pour l'index donn�.
   */
  public double getNorme(int i) {
    double x= getValeurX(i);
    double y= getValeurY(i);
    if (x == Double.NaN || y == Double.NaN)
      return Double.NaN;
    return Math.sqrt(x * x + y * y);
  }
  /**
   * Nombre de vecteurs du modele.
   * @return Le nombre de valeurs. 0 si aucune valeur
   */
  public int getNbVecteurs() {
    if (getXCol() == null)
      return 0;
    return xcol_.valeurs.length;
  }
  /**
   * Norme min des vecteurs.
   * @return Le min ou Double.NaN si aucune valeur.
   */
  public double getMinNorme() {
    if (minNorm_ == null) {
      double r= Double.NaN;
      if (getNbVecteurs() > 0)
        r= Double.POSITIVE_INFINITY;
      for (int i= 0; i < getNbVecteurs(); i++)
        r= Math.min(r, getNorme(i));
      minNorm_= new Double(r);
    }
    return minNorm_.doubleValue();
  }
  /**
   * Norme max des vecteurs.
   * @return Le max ou Double.NaN si aucune valeur.
   */
  public double getMaxNorme() {
    if (maxNorm_ == null) {
      double r= Double.NaN;
      if (getNbVecteurs() > 0)
        r= Double.NEGATIVE_INFINITY;
      for (int i= 0; i < getNbVecteurs(); i++)
        r= Math.max(r, getNorme(i));
      maxNorm_= new Double(r);
    }
    return maxNorm_.doubleValue();
  }
  public void addModelChangeListener(ZModeleChangeListener _listener) {
    listeners_.add(_listener);
  }
  public void removeModelChangeListener(ZModeleChangeListener _listener) {
    listeners_.remove(_listener);
  }
  // <<< ZModeleVecteur  -------------------------------------------------------
}
