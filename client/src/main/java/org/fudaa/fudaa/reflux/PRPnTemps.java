/*
 * @file         PRPnTemps.java
 * @creation     1999-06-25
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.memoire.bu.BuGridLayout;
/**
 * Un dialogue destin� � afficher les groupes de pas de temps.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRPnTemps extends JPanel {
  JRadioButton rbCasStat= new JRadioButton();
  JRadioButton rbCasTran= new JRadioButton();
  ButtonGroup bgCas= new ButtonGroup();
  JButton btModifier= new JButton();
  JButton btSupprimer= new JButton();
  JPanel pnCas= new JPanel();
  TitledBorder bdpnCasStat= new TitledBorder(" ");
  TitledBorder bdpnCasTran= new TitledBorder(" ");
  DefaultTableModel tbModelStat= new ListeTableModel();
  DefaultTableModel tbModelTran= new ListeTableModel();
  JPanel pnChoixCas= new JPanel();
  BuGridLayout lyChoixCas= new BuGridLayout();
  JPanel pnActions= new JPanel();
  BuGridLayout lyActions= new BuGridLayout();
  JButton btAjouter= new JButton();
  JPanel pnCasStat= new JPanel();
  JScrollPane pnStat= new JScrollPane();
  JTable tbStat= new JTable();
  BorderLayout lyCasStat= new BorderLayout();
  CardLayout lyCas= new CardLayout();
  JScrollPane pnTran= new JScrollPane();
  JTable tbTran= new JTable();
  BorderLayout lyCasTran= new BorderLayout();
  JPanel pnCasTran= new JPanel();
  BorderLayout lyThis= new BorderLayout();
  Border bdPnActions;
  Border bdPnCas;
  boolean casStationnaire_;
  Dialog parent_;
  /**
   * Cr�ation d'un dialogue sans initialisation.
   */
  public PRPnTemps() {
    jbInit();
  }
  /**
   * D�finition de l'interface graphique.
   */
  private void jbInit() {
    bdPnActions= BorderFactory.createEmptyBorder(5, 5, 5, 5);
    bdPnCas= BorderFactory.createEmptyBorder(0, 5, 0, 5);
    this.setPreferredSize(new Dimension(500, 200));
    rbCasStat.setText("Stationnaire");
    rbCasStat.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbCas_itemStateChanged(_evt);
      }
    });
    rbCasTran.setText("Transitoire");
    rbCasTran.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent _evt) {
        rbCas_itemStateChanged(_evt);
      }
    });
    btModifier.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btModifier_actionPerformed(e);
      }
    });
    btModifier.setText("Modifier...");
    btSupprimer.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btSupprimer_actionPerformed(e);
      }
    });
    btSupprimer.setText("Supprimer");
    pnCas.setLayout(lyCas);
    this.setLayout(lyThis);
    pnChoixCas.setLayout(lyChoixCas);
    lyChoixCas.setColumns(2);
    lyChoixCas.setHgap(5);
    lyChoixCas.setCfilled(false);
    pnActions.setLayout(lyActions);
    lyActions.setColumns(3);
    lyActions.setCfilled(false);
    btAjouter.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btAjouter_actionPerformed(e);
      }
    });
    btAjouter.setText("Ajouter...");
    pnStat.setBorder(null);
    tbStat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    tbStat.setModel(tbModelStat);
    pnCasStat.setLayout(lyCasStat);
    pnCasStat.setBorder(bdpnCasStat);
    pnTran.setBorder(null);
    tbTran.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    tbTran.setModel(tbModelTran);
    pnCasTran.setLayout(lyCasTran);
    pnCasTran.setBorder(bdpnCasTran);
    bdpnCasStat.setTitle(" M�thode de r�solution");
    bdpnCasTran.setTitle(" Groupes de pas de temps");
    pnActions.setBorder(bdPnActions);
    pnCas.setBorder(bdPnCas);
    pnChoixCas.setBorder(bdPnActions);
    this.add(pnChoixCas, BorderLayout.NORTH);
    pnChoixCas.add(rbCasStat, null);
    pnChoixCas.add(rbCasTran, null);
    this.add(pnCas, BorderLayout.CENTER);
    pnCas.add(pnCasStat, "pnCasStat");
    pnCasStat.add(pnStat, BorderLayout.CENTER);
    pnCas.add(pnCasTran, "pnCasTran");
    pnCasTran.add(pnTran, BorderLayout.CENTER);
    pnTran.getViewport().add(tbTran, null);
    pnStat.getViewport().add(tbStat, null);
    this.add(pnActions, BorderLayout.SOUTH);
    pnActions.add(btAjouter, null);
    pnActions.add(btSupprimer, null);
    pnActions.add(btModifier, null);
    bgCas.add(rbCasStat);
    bgCas.add(rbCasTran);
    rbCasTran.setEnabled(true);
    //    pack();
    tbStat.setDefaultRenderer(
      new double[0].getClass(),
      new DoubleArrayTableCellRenderer());
    tbStat.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tbTran.setDefaultRenderer(
      new double[0].getClass(),
      new DoubleArrayTableCellRenderer());
    tbTran.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }
  /**
   * D�finit le parent du Panel. Le parent doit �tre un dialogue.
   * @param _parent La fenetre parent.
   */
  public void setParent(Dialog _parent) {
    parent_= _parent;
  }
  /**
   * Initialisation de la boite de dialogue avec les groupes de PT du projet
   * Si le calcul est stationnaire, le nombre de groupes de pas de temps doit
   * �tre �gal � 1.
   *
   * @param _grps Les groupes de pas de temps.
   */
  public void setGroupesPT(PRGroupePT[] _grps) {
    String[] nmColsS= { "M�thode", "Coefficients de la m�thode" };
    String[] nmColsT=
      {
        "Temps de d�but",
        "Temps de fin",
        "Nombre de pas",
        "Valeur du pas",
        "Fr�quence de stockage",
        "Sch�ma",
        "coefficient du sch�ma",
        "M�thode",
        "Coefficients de la m�thode" };
    Object[][] dtS= new Object[1][nmColsS.length];
    Object[][] dtT= new Object[_grps.length][nmColsT.length];
    casStationnaire_=
      (_grps[0].schema().type() == PRSchemaResolution.STATIONNAIRE);
    // Le cas est stationnaire.
    if (casStationnaire_) {
      dtS[0][0]= Definitions.methodeToString(_grps[0].methode().type());
      dtS[0][1]= _grps[0].methode().coefficients();
      // D�termination d'un cas transitoire.
      PRBoucleTemps tpsDef= new PRBoucleTemps();
      int tpSch;
      double coefSch;
      int tpMth;
      double[] coefsMth;
      int[] tpsSch= Definitions.getTypesSchema();
      for (tpSch= 0; tpSch < tpsSch.length; tpSch++)
        if (tpsSch[tpSch] != PRSchemaResolution.STATIONNAIRE)
          break;
      coefSch= Definitions.getDefaultCoefSchema(tpSch);
      tpMth= Definitions.getTypesMethode(tpSch)[0];
      coefsMth= Definitions.getDefaultCoefsMethode(tpMth);
      dtT[0][0]= new Double(tpsDef.debut());
      dtT[0][1]= new Double(tpsDef.fin());
      dtT[0][2]= new Integer(tpsDef.nbPas());
      dtT[0][3]= new Double(tpsDef.pas());
      dtT[0][4]= new Integer(1);
      dtT[0][5]= Definitions.schemaToString(tpSch);
      dtT[0][6]= new Double(coefSch);
      dtT[0][7]= Definitions.methodeToString(tpMth);
      dtT[0][8]= coefsMth;
    }
    // Le cas est transitoire.
    else {
      for (int i= 0; i < _grps.length; i++) {
        dtT[i][0]= new Double(_grps[i].temps().debut());
        dtT[i][1]= new Double(_grps[i].temps().fin());
        dtT[i][2]= new Integer(_grps[i].temps().nbPas());
        dtT[i][3]= new Double(_grps[i].temps().pas());
        dtT[i][4]= new Integer(_grps[i].frequenceStockage());
        dtT[i][5]= Definitions.schemaToString(_grps[i].schema().type());
        dtT[i][6]= new Double(_grps[i].schema().coefficient());
        dtT[i][7]= Definitions.methodeToString(_grps[i].methode().type());
        dtT[i][8]= _grps[i].methode().coefficients();
      }
      // D�termination d'un cas stationnaire.
      int tpMth=
        Definitions.getTypesMethode(PRSchemaResolution.STATIONNAIRE)[0];
      double[] coefsMth= Definitions.getDefaultCoefsMethode(tpMth);
      dtS[0][0]= Definitions.methodeToString(tpMth);
      dtS[0][1]= coefsMth;
    }
    tbModelStat.setDataVector(dtS, nmColsS);
    tbModelTran.setDataVector(dtT, nmColsT);
    // Choix du cas
    if (casStationnaire_)
      rbCasStat.setSelected(true);
    else
      rbCasTran.setSelected(true);
  }
  /**
   * Retourne les groupes de pas de temps de la table.
   * @return Les groupes de pas de temps.
   */
  public PRGroupePT[] getGroupesPT() {
    PRGroupePT[] r= null;
    // Cas stationnaire : 1 seul groupe de pas de temps, 1 seul pas de temps.
    if (casStationnaire_) {
      int tpMth= 0;
      double[] coefsMth= null;
      r= new PRGroupePT[1];
      tpMth= Definitions.stringToMethode((String)tbModelStat.getValueAt(0, 0));
      coefsMth= (double[])tbModelStat.getValueAt(0, 1);
      r[0]= new PRGroupePT();
      r[0].temps().debut(0);
      r[0].temps().fin(1);
      r[0].temps().nbPas(1);
      r[0].schema().type(PRSchemaResolution.STATIONNAIRE);
      r[0].methode().type(tpMth);
      r[0].methode().coefficients(coefsMth);
    }
    // Cas transitoire
    else {
      double debut;
      double fin;
      int nbPas;
      int tpSch;
      double coefSch;
      int tpMth;
      double[] coefsMth;
      int freq;
      r= new PRGroupePT[tbModelTran.getRowCount()];
      for (int i= 0; i < r.length; i++) {
        debut= ((Double)tbTran.getValueAt(i, 0)).doubleValue();
        fin= ((Double)tbTran.getValueAt(i, 1)).doubleValue();
        nbPas= ((Integer)tbTran.getValueAt(i, 2)).intValue();
        freq= ((Integer)tbTran.getValueAt(i, 4)).intValue();
        tpSch= Definitions.stringToSchema((String)tbModelTran.getValueAt(i, 5));
        coefSch= ((Double)tbTran.getValueAt(i, 6)).doubleValue();
        tpMth=
          Definitions.stringToMethode((String)tbModelTran.getValueAt(i, 7));
        coefsMth= (double[])tbModelTran.getValueAt(i, 8);
        r[i]= new PRGroupePT();
        r[i].temps().debut(debut);
        r[i].temps().fin(fin);
        r[i].temps().nbPas(nbPas);
        r[i].frequenceStockage(freq);
        r[i].schema().type(tpSch);
        r[i].schema().coefficient(coefSch);
        r[i].methode().type(tpMth);
        r[i].methode().coefficients(coefsMth);
      }
    }
    return r;
  }
  /**
   * Coh�rence des temps de la table transitoire apres modification.
   */
  void mjjTableTransitoire() {
    double fin;
    for (int i= 0; i < tbModelTran.getRowCount(); i++) {
      // Temps d�but
      if (i != 0)
        tbModelTran.setValueAt(tbModelTran.getValueAt(i - 1, 1), i, 0);
      // Temps fin
      fin=
        ((Double)tbModelTran.getValueAt(i, 0)).doubleValue()
          + ((Integer)tbModelTran.getValueAt(i, 2)).intValue()
            * ((Double)tbModelTran.getValueAt(i, 3)).doubleValue();
      tbModelTran.setValueAt(new Double(fin), i, 1);
    }
  }
  //----------------------------------------------------------------------------
  // Ev�nements
  //----------------------------------------------------------------------------
  /**
   * Bouton Ajouter activ� (seulement en transitoire)
   * => Ajout d'un groupe de pas de temps.
   */
  void btAjouter_actionPerformed(ActionEvent e) {
    Vector row= new Vector();
    // Numero de ligne s�lectionn�e
    int numRow= tbTran.getSelectedRow();
    if (numRow == -1)
      return;
    if (numRow == 0)
      numRow= tbModelTran.getRowCount();
    // Rajout d'une ligne avec les valeurs pr�c�dentes
    for (int i= 0; i < tbModelTran.getColumnCount(); i++)
      row.addElement(tbModelTran.getValueAt(numRow - 1, i));
    tbModelTran.insertRow(numRow, row);
    // Coh�rence des temps
    mjjTableTransitoire();
    //    mjBDGroupesPT();
    btSupprimer.setEnabled(tbModelTran.getRowCount() > 1);
  }
  /**
   * Bouton Supprimer activ� (seulement en transitoire)
   * => Suppression d'un groupe de pas de temps.
   */
  void btSupprimer_actionPerformed(ActionEvent e) {
    // Numero de ligne s�lectionn�e
    int numRow= tbTran.getSelectedRow();
    if (numRow == -1)
      return;
    tbModelTran.removeRow(numRow);
    // Coh�rence des temps
    mjjTableTransitoire();
    //    mjBDGroupesPT();
    btSupprimer.setEnabled(tbModelTran.getRowCount() > 1);
  }
  /**
   * Bouton Modifier activ� => Modification d'une ligne de liste.
   */
  void btModifier_actionPerformed(ActionEvent e) {
    String titre=
      (casStationnaire_
        ? "D�finition de la m�thode"
        : "D�finition du groupe de pas de temps");
    int numRow= (casStationnaire_ ? 0 : tbTran.getSelectedRow());
    boolean t0Editable= (numRow == 0 && Definitions.estT0Editable());
    if (numRow >= 0) {
      PRGroupePT[] gpsPT= getGroupesPT();
      PRPnGroupePT pn= new PRPnGroupePT();
      pn.setGroupePT(gpsPT[numRow], t0Editable);
      PRDialogPanneau diPn= new PRDialogPanneau(parent_, pn, titre) {
        public void actionApply() {
          PRGroupePT gp= ((PRPnGroupePT)getPanneauPrincipal()).getGroupePT();
          if (casStationnaire_) {
            tbModelStat.setValueAt(
              Definitions.methodeToString(gp.methode().type()),
              0,
              0);
            tbModelStat.setValueAt(gp.methode().coefficients(), 0, 1);
          } else {
            int n= tbTran.getSelectedRow();
            tbModelTran.setValueAt(new Double(gp.temps().debut()), n, 0);
            tbModelTran.setValueAt(new Double(gp.temps().fin()), n, 1);
            tbModelTran.setValueAt(new Integer(gp.temps().nbPas()), n, 2);
            tbModelTran.setValueAt(new Double(gp.temps().pas()), n, 3);
            tbModelTran.setValueAt(new Integer(gp.frequenceStockage()), n, 4);
            tbModelTran.setValueAt(
              Definitions.schemaToString(gp.schema().type()),
              n,
              5);
            tbModelTran.setValueAt(new Double(gp.schema().coefficient()), n, 6);
            tbModelTran.setValueAt(
              Definitions.methodeToString(gp.methode().type()),
              n,
              7);
            tbModelTran.setValueAt(gp.methode().coefficients(), n, 8);
            mjjTableTransitoire();
          }
          //          mjBDGroupesPT();
        }
      };
      diPn.show();
    }
  }
  /**
   * M�thode appel�e quand rbCasStationnaire ou rbCasTransitoire sont actionn�s.
   */
  void rbCas_itemStateChanged(ItemEvent _evt) {
    if (_evt.getStateChange() == ItemEvent.DESELECTED)
      return;
    casStationnaire_= rbCasStat.isSelected();
    lyCas.show(pnCas, casStationnaire_ ? "pnCasStat" : "pnCasTran");
    btAjouter.setEnabled(!casStationnaire_);
    btSupprimer.setEnabled(!casStationnaire_ && tbModelTran.getRowCount() > 1);
  }
  //----------------------------------------------------------------------------
  // Classes internes
  //----------------------------------------------------------------------------
  /**
   * Classe de rendu des cellules commportant des double[] de la table.
   */
  private class DoubleArrayTableCellRenderer extends DefaultTableCellRenderer {
    public Component getTableCellRendererComponent(
      JTable table,
      Object doubleArray,
      boolean isSelected,
      boolean hasFocus,
      int row,
      int column) {
      super.getTableCellRendererComponent(
        table,
        doubleArray,
        isSelected,
        hasFocus,
        row,
        column);
      // Attention, getTableCellRendererComponent peut �tre appel� pour cr�ation
      // d'un Component, m�me avec doubleArray=null
      // => Retour d'un Component
      if (doubleArray == null)
        return this;
      double[] coefficients= (double[])doubleArray;
      String label= "";
      for (int j= 0; j < coefficients.length; j++)
        label += coefficients[j] + ";";
      this.setText(label);
      return this;
    }
  }
  /**
   * Mod�le de tables.
   */
  private class ListeTableModel extends DefaultTableModel {
    // Necessaire pour l'affichage de cells de type double[]
    public Class getColumnClass(int c) {
      return getValueAt(0, c).getClass();
    }
    public boolean isCellEditable(int row, int col) {
      return false;
    }
  }
}
