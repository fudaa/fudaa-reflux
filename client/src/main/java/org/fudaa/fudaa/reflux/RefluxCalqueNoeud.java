/*
 * @file         RefluxCalqueNoeud.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.Hashtable;

import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TracePoint;
/**
 * Un calque d'affichage des noeuds.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class RefluxCalqueNoeud extends BCalqueAffichage {
  private VecteurGrContour noeuds_; // Liste des noeuds affich�s
  private GrBoite boite_; // Boite englobante des noeuds du calque
  private int typePoint_; // Type de repr�sentation des noeuds
  private Hashtable nd2Num_;
  private boolean visibleNumber_;
  /**
   * Cr�ation d'un calque d'affichage des noeuds. Les m�thodes <I>add</I> et <I>remove</I>
   * permettent l'ajout ou la suppression d'un noeud.
   */
  public RefluxCalqueNoeud() {
    super();
    setDestructible(false);
    noeuds_= new VecteurGrContour();
    boite_= null;
    typePoint_= TracePoint.CROIX;
    visibleNumber_= false;
    nd2Num_= null;
  }
  /**
   * Trac� des noeuds.
   */
  public void paintComponent(Graphics _g) {
    if (noeuds_.nombre() > 0) {
      GrMorphisme versEcran= getVersEcran();
      TraceGeometrie tg= new TraceGeometrie(versEcran);
      GrPoint pt;
      GrNoeud nd;
      Color c= new Color(0, 200, 0);
      int incr;
      int num;
      if (nd2Num_ == null) {
        nd2Num_= new Hashtable();
        GrNoeud[] nds= RefluxImplementation.projet.maillage().noeuds();
        for (int i= 0; i < nds.length; i++)
          nd2Num_.put(nds[i], new Integer(i));
      }
      Polygon pecr= getDomaine().enPolygoneXY().applique(versEcran).polygon();
      Rectangle clip= _g.getClipBounds();
      if (clip == null)
        clip= new Rectangle(0, 0, getWidth(), getHeight());
      if (clip.intersects(pecr.getBounds())) {
        tg.setTypePoint(typePoint_);
        tg.setForeground(isAttenue() ? attenueCouleur(c) : c);
        incr= (isRapide() ? noeuds_.nombre() / 1000 + 1 : 1);
        for (int i= 0; i < noeuds_.nombre(); i += incr) {
          nd= (GrNoeud)noeuds_.renvoie(i);
          pt= nd.point_;
          tg.dessinePoint( (Graphics2D) _g,pt, isRapide());
          // Visibilit� des num�ros
          if (!isRapide() && visibleNumber_) {
            num= ((Integer)nd2Num_.get(nd)).intValue() + 1;
            tg.dessineTexte( (Graphics2D) _g,"" + num, pt, isRapide());
          }
        }
      }
    }
    super.paintComponent(_g);
  }
  /**
   * Affectation du type de point pour tracer les noeuds (CERCLE, POINT, DISQUE,
   * etc.). Par d�faut = TracePoint.POINT
   *
   * @see org.fudaa.ebli.trace.TracePoint
   */
  public void setPointType(int _type) {
    typePoint_= _type;
  }
  /**
   * Retour du type de point utilis� pour tracer les noeuds.
   *
   * @see org.fudaa.ebli.trace.TracePoint
   */
  public int getPointType() {
    return typePoint_;
  }
  /**
   * Ajout d'un <I>GrNoeud</I> au calque.
   */
  public void add(GrNoeud _noeud) {
    noeuds_.ajoute(_noeud);
    boite_= null;
    nd2Num_= null;
  }
  /**
   * Suppression d'un <I>GrNoeud</I> du calque.
   */
  public void remove(GrNoeud _noeud) {
    noeuds_.enleve(_noeud);
    boite_= null;
    nd2Num_= null;
  }
  /**
   * Suppression de tous les <I>GrNoeud</I>.
   */
  public void removeAll() {
    noeuds_.vide();
    boite_= null;
    nd2Num_= null;
  }
  /**
   * Visibilit� des num�ros d'objets.
   */
  public void setVisibleNumber(boolean _visible) {
    visibleNumber_= _visible;
  }
  /**
   * Liste des �l�ments s�lectionnables.
   */
  public VecteurGrContour contours() {
    return noeuds_;
  }
  /**
   * Boite englobante des objets contenus dans le calque.
   * @return Boite englobante. Si le calque est vide,
   * la boite englobante retourn�e est <I>null</I>
   */
  public GrBoite getDomaine() {
    if (boite_ == null && noeuds_.nombre() > 0) {
      boite_= new GrBoite();
      for (int i= 0; i < noeuds_.nombre(); i++) {
        boite_.ajuste(((GrNoeud)noeuds_.renvoie(i)).point_);
      }
    }
    return boite_;
  }
}
