/*
 * @file         RefluxCalqueBord.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;

import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.trace.TraceGeometrie;
/**
 * Un calque d'affichage des bords.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class RefluxCalqueBord extends BCalqueAffichage {
  private VecteurGrContour bords_;
  private GrBoite boite_;
  public RefluxCalqueBord() {
    super();
    setDestructible(false);
    bords_= new VecteurGrContour();
    boite_= null;
  }
  /**
   * Trac� des bords.
   */
  public void paintComponent(Graphics _g) {
    if (bords_.nombre() > 0) {
      GrMorphisme versEcran= getVersEcran();
      TraceGeometrie tg= new TraceGeometrie( versEcran);
      //GrPolyligne pl;
      RefluxGrBord bord;
      Polygon pecr= getDomaine().enPolygoneXY().applique(versEcran).polygon();
      Rectangle clip= _g.getClipBounds();
      if (clip == null)
        clip= new Rectangle(0, 0, getWidth(), getHeight());
      if (clip.intersects(pecr.getBounds())) {
        for (int i= 0; i < bords_.nombre(); i++) {
          bord= ((RefluxGrBord)bords_.renvoie(i));
          //pl= bord.ligne;
          tg.setTypeTrait(bord.getTypeTrait());
          tg.setForeground(
            isAttenue()
              ? attenueCouleur(bord.getCouleur())
              : bord.getCouleur());
          tg.dessinePolyligne((Graphics2D) _g,bord.ligne, isRapide());
        }
      }
    }
    super.paintComponent(_g);
  }
  /**
   * Reinitialise la liste des bords.
   */
  public void reinitialise() {
    bords_.vide();
    boite_= null;
  }
  /**
   * Ajoute un bord a la liste.
   */
  public void ajoute(RefluxGrBord _bord) {
    bords_.ajoute(_bord);
    boite_= null;
  }
  /**
   * Retire un bord de la liste.
   */
  public void enleve(RefluxGrBord _bord) {
    bords_.enleve(_bord);
    boite_= null;
  }
  /**
   * Liste des bords s�lectionnables.
   */
  public VecteurGrContour contours() {
    return bords_;
  }
  /**
   * Boite englobante des objets contenus dans le calque.
   * @return Boite englobante. Si le calque est vide,
   * la boite englobante retourn�e est <I>null</I>
   */
  public GrBoite getDomaine() {
    GrBoite boiteBord;
    if (boite_ == null && bords_.nombre() > 0) {
      boite_= new GrBoite();
      for (int i= 0; i < bords_.nombre(); i++) {
        boiteBord= ((RefluxGrBord)bords_.renvoie(i)).ligne.boite();
        boite_.ajuste(boiteBord.o_);
        boite_.ajuste(boiteBord.e_);
      }
    }
    return boite_;
  }
}
