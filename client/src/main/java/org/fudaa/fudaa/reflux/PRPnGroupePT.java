/*
 * @file         PRPnGroupePT.java
 * @creation     1999-06-25
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;

import cz.autel.dmi.HIGConstraints;
import cz.autel.dmi.HIGLayout;
/**
 * Un panneau pour la saisie du groupe de pas de temps.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRPnGroupePT extends JPanel {
  //  private PRGroupePT groupePT_=null;
  private boolean casStationnaire_;
  private JComboBox cbSchema= new JComboBox();
  private BuLabel laSchema= new BuLabel();
  private EtchedBorder bdPanel= new EtchedBorder();
  private JTextField tfStockage= new JTextField();
  private JLabel laStockage= new JLabel();
  private JLabel laDebut= new JLabel();
  private JTextField tfDebut= new JTextField();
  private JLabel laFin= new JLabel();
  private JTextField tfFin= new JTextField();
  private JLabel laNbPT= new JLabel();
  private JTextField tfNbPT= new JTextField();
  private JLabel laValeurPT= new JLabel();
  private JTextField tfValeurPT= new JTextField();
  private JPanel pnTemps= new JPanel();
  private JPanel pnSchema= new JPanel();
  private PRPnMethode pnMethode= new PRPnMethode();
  private HIGLayout lyTemps=
    new HIGLayout(
      new int[] { 5, 0, 5, -8, 5, 0, 5, -4, 5 },
      new int[] { 5, 0, 5, 0, 5, 0, 5 });
  private BuLabel laCoefSchema= new BuLabel();
  private JTextField tfCoefSchema= new JTextField();
  private FlowLayout lySchema= new FlowLayout();
  private BuGridLayout lyThis= new BuGridLayout();
  /**
   * Cr�ation d'un panneau.
   */
  public PRPnGroupePT() {
    super();
    try {
      jbInit();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  /**
   * D�finition de l'interface graphique.
   */
  private void jbInit() {
    lyThis.setVgap(5);
    lyThis.setHgap(5);
    lyThis.setColumns(1);
    setLayout(lyThis);
    laNbPT.setText("Nombre de pas de temps");
    tfNbPT.setPreferredSize(new Dimension(70, 19));
    laDebut.setText("D�but");
    tfDebut.setPreferredSize(new Dimension(70, 19));
    //    tfDebut.setEditable(false);
    laValeurPT.setText("Valeur du pas de temps");
    tfValeurPT.setPreferredSize(new Dimension(70, 19));
    laFin.setText("Fin");
    tfFin.setPreferredSize(new Dimension(70, 19));
    tfFin.setEditable(false);
    laStockage.setText("Fr�quence de stockage des r�sultats");
    tfStockage.setToolTipText("En nombre de pas");
    tfStockage.setPreferredSize(new Dimension(70, 19));
    pnTemps.setLayout(lyTemps);
    pnTemps.setBorder(bdPanel);
    HIGConstraints cont= new HIGConstraints();
    cont.xywh(2, 2, 1, 1);
    pnTemps.add(laNbPT, cont);
    cont.xywh(4, 2, 1, 1);
    pnTemps.add(tfNbPT, cont);
    cont.xywh(6, 2, 1, 1);
    pnTemps.add(laDebut, cont);
    cont.xywh(8, 2, 1, 1);
    pnTemps.add(tfDebut, cont);
    cont.xywh(2, 4, 1, 1);
    pnTemps.add(laValeurPT, cont);
    cont.xywh(4, 4, 1, 1);
    pnTemps.add(tfValeurPT, cont);
    cont.xywh(6, 4, 1, 1);
    pnTemps.add(laFin, cont);
    cont.xywh(8, 4, 1, 1);
    pnTemps.add(tfFin, cont);
    cont.xywh(2, 6, 5, 1);
    pnTemps.add(laStockage, cont);
    cont.xywh(8, 6, 1, 1);
    pnTemps.add(tfStockage, cont);
    laSchema.setText("Sch�ma");
    cbSchema.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        cbSchema_itemStateChanged(e);
      }
    });
    laCoefSchema.setText("Coefficient");
    tfCoefSchema.setPreferredSize(new Dimension(70, 19));
    tfCoefSchema.setToolTipText("Valeur dans l'intervalle [0,1]");
    pnSchema.setLayout(lySchema);
    pnSchema.setBorder(bdPanel);
    pnSchema.add(laSchema, null);
    pnSchema.add(cbSchema, null);
    pnSchema.add(laCoefSchema, null);
    pnSchema.add(tfCoefSchema, null);
    //    setTitle("Groupe de pas de temps");
    //    setModal(true);
  }
  /**
   * Initialisation avec le groupe de pas de temps. Si le groupe de pas est
   * de sch�ma stationnaire, alors lla boite de dialogue ne fait pas apparaitre
   * l'int�gration en temps.
   *
   * @param _gp Le groupe de pas de temps.
   * @param _t0EstEditable <i>true</i>t0 est �ditable.<i>false</i>sinon
   */
  public void setGroupePT(PRGroupePT _gp, boolean _t0EstEditable) {
    PRBoucleTemps temps= _gp.temps();
    PRMethodeResolution methode= _gp.methode();
    PRSchemaResolution schema= _gp.schema();
    int stockage= _gp.frequenceStockage();
    int[] tpsSchema= Definitions.getTypesSchema();
    casStationnaire_= (schema.type() == PRSchemaResolution.STATIONNAIRE);
    // Cas stationnaire
    if (casStationnaire_) {
      cbSchema.removeAllItems();
      cbSchema.addItem(
        Definitions.schemaToString(PRSchemaResolution.STATIONNAIRE));
      removeAll();
      add(pnMethode);
    }
    // Cas transitoire
    else {
      tfDebut.setEditable(_t0EstEditable);
      tfNbPT.setText("" + temps.nbPas());
      tfValeurPT.setText("" + temps.pas());
      tfDebut.setText("" + temps.debut());
      tfFin.setText("" + temps.fin());
      tfStockage.setText("" + stockage);
      // Mise � jour des sch�mas possibles.
      cbSchema.removeAllItems();
      for (int i= 0; i < tpsSchema.length; i++)
        if (tpsSchema[i] != PRSchemaResolution.STATIONNAIRE)
          cbSchema.addItem(Definitions.schemaToString(tpsSchema[i]));
      removeAll();
      add(pnTemps);
      add(pnSchema);
      add(pnMethode);
    }
    cbSchema.setSelectedItem(Definitions.schemaToString(schema.type()));
    tfCoefSchema.setText("" + schema.coefficient());
    pnMethode.setMethode(methode);
  }
  /**
   * Retourne le groupe de pas de temps.
   * @return Le groupe
   */
  public PRGroupePT getGroupePT() {
    PRGroupePT r= new PRGroupePT();
    PRSchemaResolution schema= r.schema();
    if (!casStationnaire_) {
      PRBoucleTemps temps= r.temps();
      // Temps + frequence
      double debut= Double.parseDouble(tfDebut.getText());
      double pas= Double.parseDouble(tfValeurPT.getText());
      int nbPas= Integer.parseInt(tfNbPT.getText());
      temps.debut(debut);
      temps.nbPas(nbPas);
      temps.fin(debut + pas * nbPas);
      r.frequenceStockage(Integer.parseInt(tfStockage.getText()));
    }
    schema.type(Definitions.stringToSchema((String)cbSchema.getSelectedItem()));
    schema.coefficient(Double.parseDouble(tfCoefSchema.getText()));
    r.methode(pnMethode.getMethode());
    return r;
  }
  //  /**
  //   * Edite un groupe de pas de temps.
  //   * @return true Modification accept�e<BR>false Modification rejet�e
  //   * @deprecated Utiliser plut�t setGroupePT()
  //   */
  //  public boolean edite(PRGroupePT _groupePT, boolean _isFirstGroup) {
  //    groupePT_=_groupePT;
  //
  //    setGroupePT(groupePT_,_isFirstGroup);
  //
  //    if (casStationnaire_) setTitle("D�finition de la m�thode");
  //    else                  setTitle("D�finition du groupe de pas de temps");
  //
  //    pack();
  //    show();
  //
  //    return !abort;
  //  }
  //  /**
  //   * Bouton "Annuler" press�, effacage du dialog, traitement dans le listener du dialog
  //   */
  //  protected void CANCEL_BUTTON_actionPerformed(ActionEvent _evt) {
  //    abort=true;
  //    super.OK_BUTTON_actionPerformed(_evt);
  //  }
  //  /**
  //   * Bouton "Ok" press�, effacage du dialog, traitement dans le listener du dialog
  //   */
  //  protected void OK_BUTTON_actionPerformed(ActionEvent _evt) {
  //    majBDD();
  //    abort=false;
  //    super.OK_BUTTON_actionPerformed(_evt);
  //  }
  //  /**
  //   * Mise � jour du groupe de pas.
  //   * @deprecated Utiliser plut�t setGroupe()/getGroupePT().
  //   */
  //  private void majBDD() {
  //    PRSchemaResolution  schema  =groupePT_.schema();
  //
  //    if (!casStationnaire_) {
  //      PRBoucleTemps temps=groupePT_.temps();
  //
  //      // Temps + frequence
  //      double debut=Double.parseDouble(tfDebut   .getText());
  //      double pas  =Double.parseDouble(tfValeurPT.getText());
  //      int nbPas   =Integer.parseInt(tfNbPT.getText());
  //      temps.debut(debut);
  //      temps.nbPas(nbPas);
  //      temps.fin(debut+pas*nbPas);
  //      groupePT_.frequenceStockage(Integer.parseInt(tfStockage.getText()));
  //    }
  //
  //    schema.type(Definitions.stringToSchema((String)cbSchema.getSelectedItem()));
  //    schema.coefficient(Double.parseDouble(tfCoefSchema.getText()));
  //    groupePT_.methode(pnMethode.getMethode());
  //  }
  //
  /**
   * M�thode appel�e quand on change d'item sur la liste des sch�mas.
   */
  void cbSchema_itemStateChanged(ItemEvent _evt) {
    String nomSchema= (String)_evt.getItem();
    pnMethode.setTypeSchema(Definitions.stringToSchema(nomSchema));
  }
}