package org.fudaa.fudaa.reflux;
import java.util.Hashtable;

import com.memoire.bu.BuTable;

import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleMaillage;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
public final class RefluxMaillage
  extends GrMaillageElement
  implements ZModeleMaillage {
  /** Hashtable pour faire l'association Noeud -> Indice pour la connectivit�. */
  private Hashtable nd2Ind_= new Hashtable();
  /**
   * Cr�ation d'un maillage � partir des �l�ments et de la table des noeuds
   * associ�e.
   * <p>
   * Seule ce constructeur est autoris�, car l'ordre des noeuds
   * doit �tre respect� pour repondre � l'interface ZModeleMaillage.
   * Le maillage est d�finitif et ne doit pas �tre modifi�.
   */
  public RefluxMaillage(GrElement[] _elements, GrNoeud[] _noeuds) {
    super(_elements, _noeuds);
    // Remplissage de la table pour r�cup�rer rapidement la connectivit�.
    for (int i= 0; i < _noeuds.length; i++) {
      nd2Ind_.put(_noeuds[i], new Integer(i));
    }
  }
  
  
  public BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer){
    return null;
  }


  public void fillWithInfo(InfoData _d,ZCalqueAffichageDonneesInterface _layer){}


  public boolean isValuesTableAvailable(){
    return false;
  }


  public  int nombrePolygones() {
    return getNombre();
  }
  public boolean isDonneesBoiteAvailable() {
    return false;
  }
  public boolean isDonneesBoiteTimeAvailable() {
    return false;
  }
  public  int nombreNoeuds() {
    return noeuds().length;
  }
  public  GrBoite getDomaine() {
    return boite();
  }
  public  void getDomaine(GrBoite _bt) {
    return;
    //    throw new RuntimeException("RefluxMaillage.domaine(GrBoite _bt) not yet implemented");
  }
  public  GrPolygone polygone(int _i) {
    return element(_i).polygone();
  }
  public  GrPoint point(int _i) {
    return noeud(_i).point_;
  }
  public  Object getObject(int _i) {
    return noeud(_i);
  }
  public  int[] connectivites(int _i) {
    GrNoeud[] nds= element(_i).noeuds_;
    int[] conns= new int[nds.length];
    for (int j= 0; j < nds.length; j++) {
      conns[j]= ((Integer)nd2Ind_.get(nds[j])).intValue();
    }
    return conns;
  }

}
