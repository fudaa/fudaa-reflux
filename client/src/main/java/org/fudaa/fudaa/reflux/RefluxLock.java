/*
 * @file         RefluxLock.java
 * @creation     1999-01-11
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.util.GregorianCalendar;
/**
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public final class RefluxLock {
  
  private RefluxLock() {}
  public static boolean isTrue() {
    return (
      System.currentTimeMillis()
        > new GregorianCalendar(1999, 11, 00, 00, 00).getTime().getTime());
  }
}
