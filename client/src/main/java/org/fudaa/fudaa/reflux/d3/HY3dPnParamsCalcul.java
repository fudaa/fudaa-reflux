/*
 * @file         HY3dPnParamsCalcul.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d3;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuGridLayout;

import org.fudaa.fudaa.reflux.PRPnParamsCalcul;
import org.fudaa.fudaa.reflux.PRProjet;
/**
 * Un panel Reflux 3D courantologie comportant les parametres de calcul.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class HY3dPnParamsCalcul extends PRPnParamsCalcul {
  JCheckBox cbConvection= new JCheckBox();
  JCheckBox cbFrottement= new JCheckBox();
  JCheckBox cbPression= new JCheckBox();
  JComboBox coMatrice= new JComboBox();
  JTextField tfNbNoeudsZ= new JTextField();
  JTextField tfAmplitude= new JTextField();
  JTextField tfLgOnde= new JTextField();
  JTextField tfPeriode= new JTextField();
  JTextField tfCelerite= new JTextField();
  JTextField tfHmoy= new JTextField();
  JTextField tfVent= new JTextField();
  /**
   * Cr�ation du panel.
   */
  public HY3dPnParamsCalcul() {
    super();
    try {
      jbInit();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  /**
   * M�thode d'impl�mentation de l'interface.
   */
  private void jbInit() throws Exception {
    JLabel lbDummy1= new JLabel();
    JLabel lbDummy2= new JLabel();
    JLabel lbDummy3= new JLabel();
    JLabel lbMatrice= new JLabel();
    JLabel lbNbNoeudsZ= new JLabel();
    JLabel lbAmplitude= new JLabel();
    JLabel lbLgOnde= new JLabel();
    JLabel lbPeriode= new JLabel();
    JLabel lbCelerite= new JLabel();
    JLabel lbHmoy= new JLabel();
    JLabel lbVent= new JLabel();
    BuGridLayout lyOptions= new BuGridLayout();
    BuGridLayout lyGene= new BuGridLayout();
    JPanel pnOptions= new JPanel();
    JPanel pnGene= new JPanel();
    JTabbedPane tpMain= new JTabbedPane();
    BorderLayout lyThis= new BorderLayout();
    String sMatriceIter= "A chaque it�ration";
    String sMatricePdt= "A chaque pas de temps";
    cbConvection.setText("Prise en compte de la convection");
    cbFrottement.setText("Prise en compte du frottement");
    cbPression.setText("Pression non hydrostatique");
    lbMatrice.setText("Calcul de la matrice tangente");
    coMatrice.addItem(sMatriceIter);
    coMatrice.addItem(sMatricePdt);
    lbDummy1.setVisible(false);
    lbDummy2.setVisible(false);
    lbDummy3.setVisible(false);
    lyOptions.setColumns(2);
    lyOptions.setHgap(5);
    lyOptions.setVgap(5);
    lyOptions.setCfilled(false);
    pnOptions.setLayout(lyOptions);
    pnOptions.setBorder(new EmptyBorder(5, 5, 5, 5));
    pnOptions.add(cbConvection);
    pnOptions.add(lbDummy1);
    pnOptions.add(cbFrottement);
    pnOptions.add(lbDummy2);
    pnOptions.add(cbPression);
    pnOptions.add(lbDummy3);
    pnOptions.add(lbMatrice);
    pnOptions.add(coMatrice);
    lbNbNoeudsZ.setText("Nb noeuds sur la verticale :");
    lbAmplitude.setText("Amplitude :");
    lbLgOnde.setText("Longueur d'onde :");
    lbPeriode.setText("P�riode :");
    lbCelerite.setText("C�l�rit� :");
    lbHmoy.setText("Hauteur moyenne :");
    lbVent.setText("Force du vent :");
    lbNbNoeudsZ.setHorizontalAlignment(SwingConstants.RIGHT);
    lbAmplitude.setHorizontalAlignment(SwingConstants.RIGHT);
    lbLgOnde.setHorizontalAlignment(SwingConstants.RIGHT);
    lbPeriode.setHorizontalAlignment(SwingConstants.RIGHT);
    lbCelerite.setHorizontalAlignment(SwingConstants.RIGHT);
    lbHmoy.setHorizontalAlignment(SwingConstants.RIGHT);
    lbVent.setHorizontalAlignment(SwingConstants.RIGHT);
    tfNbNoeudsZ.setPreferredSize(new Dimension(100, 21));
    tfAmplitude.setPreferredSize(new Dimension(100, 21));
    tfLgOnde.setPreferredSize(new Dimension(100, 21));
    tfPeriode.setPreferredSize(new Dimension(100, 21));
    tfCelerite.setPreferredSize(new Dimension(100, 21));
    tfHmoy.setPreferredSize(new Dimension(100, 21));
    tfVent.setPreferredSize(new Dimension(100, 21));
    lyGene.setColumns(2);
    lyGene.setHgap(5);
    lyGene.setVgap(5);
    lyGene.setCfilled(false);
    pnGene.setLayout(lyGene);
    pnGene.setBorder(new EmptyBorder(5, 5, 5, 5));
    pnGene.add(lbNbNoeudsZ);
    pnGene.add(tfNbNoeudsZ);
    pnGene.add(lbAmplitude);
    pnGene.add(tfAmplitude);
    pnGene.add(lbLgOnde);
    pnGene.add(tfLgOnde);
    pnGene.add(lbPeriode);
    pnGene.add(tfPeriode);
    pnGene.add(lbCelerite);
    pnGene.add(tfCelerite);
    pnGene.add(lbHmoy);
    pnGene.add(tfHmoy);
    pnGene.add(lbVent);
    pnGene.add(tfVent);
    tpMain.addTab("G�n�ral", null, pnGene, "Valeurs g�n�rales");
    tpMain.addTab("Options", null, pnOptions, "Options du calcul");
    this.setLayout(lyThis);
    this.add(tpMain, "Center");
  }
  /**
   * Affectation du projet courant pour mise � jour du panneau. Inutilis�.
   *
   * @param _prj Le projet courant.
   */
  public void setProjet(PRProjet _prj) {}
  /**
   * Initialisation avec les parametres de calcul. Ils sont dans cet ordre :<p>
   *
   * <pre>
   *  Options :
   *    [0] : Convection     {0,1}
   *    [1] : Frottement     {0,1}
   *    [2] : Pression       {0,1}
   *    [3] : Calcul matrice {1:iteration,2:pdt}
   *
   *  G�n�ral :
   *    [4] : Nombre de noeuds sur la verticale
   *    [5] : Amplitude
   *    [6] : Longueur d'onde
   *    [7] : P�riode
   *    [8] : C�l�rit�
   *    [9] : Hauteur moyenne
   *    [10] : Force du vent
   * </pre>
   *
   * @param _params Les parametres de calcul.
   */
  public void setParametres(Object[] _params) {
    // Options
    cbConvection.setSelected(((Double)_params[0]).doubleValue() == 1);
    cbFrottement.setSelected(((Double)_params[1]).doubleValue() == 1);
    cbPression.setSelected(((Double)_params[2]).doubleValue() == 1);
    coMatrice.setSelectedIndex(((Double)_params[3]).intValue() - 1);
    // G�n�ral
    tfNbNoeudsZ.setText("" + _params[4]);
    tfAmplitude.setText("" + _params[5]);
    tfLgOnde.setText("" + _params[6]);
    tfPeriode.setText("" + _params[7]);
    tfCelerite.setText("" + _params[8]);
    tfHmoy.setText("" + _params[9]);
    tfVent.setText("" + _params[10]);
  }
  /*
   * Retourne les parametres de calcul.
   * @return Les parametres dans l'ordre.
   */
  public Object[] getParametres() {
    Object[] r= new Object[11];
    // Options
    r[0]= new Double(cbConvection.isSelected() ? 1 : 0);
    r[1]= new Double(cbFrottement.isSelected() ? 1 : 0);
    r[2]= new Double(cbPression.isSelected() ? 1 : 0);
    r[3]= new Double(coMatrice.getSelectedIndex() + 1);
    // G�n�ral
    try {
      r[4]= new Double(tfNbNoeudsZ.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[5]= new Double(tfAmplitude.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[6]= new Double(tfLgOnde.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[7]= new Double(tfPeriode.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[8]= new Double(tfCelerite.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[9]= new Double(tfHmoy.getText());
    } catch (NumberFormatException _exc) {};
    try {
      r[10]= new Double(tfVent.getText());
    } catch (NumberFormatException _exc) {};
    return r;
  }
}