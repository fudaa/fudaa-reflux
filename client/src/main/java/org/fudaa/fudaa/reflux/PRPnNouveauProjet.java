/*
 * @file         PRPnNouveauProjet.java
 * @creation     2001-03-22
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * Un panneau pour la saisie d'un nouveau projet.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRPnNouveauProjet extends JPanel {
  private JComboBox coType_= new JComboBox();
  private JPanel pnParams_= new JPanel();
  private CardLayout lyParams_= new CardLayout();
  /**
   * Pour retrouver le panel de parametres � partir de son type de projet.
   */
  private Hashtable hTp2PnParams_;
  /**
   * Cr�ation du panneau de saisie de m�thode.
   */
  public PRPnNouveauProjet() {
    super();
    try {
      jbInit();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  /**
   * D�finition de l'interface graphique.
   */
  private void jbInit() throws Exception {
    JLabel laType= new JLabel();
    BorderLayout lyThis= new BorderLayout();
    JPanel pnType= new JPanel();
    FlowLayout lyType= new FlowLayout();
    int[] tpsPrj= Definitions.getTypesProjet();
    hTp2PnParams_= new Hashtable(tpsPrj.length);
    laType.setToolTipText("");
    laType.setText("Type de projet:");
    coType_.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        coType__itemStateChanged(e);
      }
    });
    lyParams_.setHgap(3);
    lyParams_.setVgap(3);
    pnParams_.setLayout(lyParams_);
    pnParams_.setBorder(BorderFactory.createRaisedBevelBorder());
    // Panneaux de coefficients. On construit les panneaux de tous les types de
    // projets possibles pour que la m�thode panneau.getPreferredSize()
    // retourne d'embl�e la taille maxi pour tous les composants.
    int tpPrjCur= RefluxResource.typeProjet;
    for (int i= 0; i < tpsPrj.length; i++) {
      String nmType= Definitions.projetToString(tpsPrj[i]);
      RefluxResource.typeProjet= tpsPrj[i];
      JPanel pnPars= PRFabrique.getFabrique().creePnParamsNouveauProjet();
      pnParams_.add(pnPars, nmType);
      coType_.addItem(nmType);
      hTp2PnParams_.put(new Integer(tpsPrj[i]), pnPars);
    }
    RefluxResource.typeProjet= tpPrjCur;
    pnType.setLayout(lyType);
    pnType.add(laType, null);
    pnType.add(coType_, null);
    this.setLayout(lyThis);
    this.add(pnType, BorderLayout.NORTH);
    this.add(pnParams_, BorderLayout.CENTER);
  }
  /**
   * Retourne le type de projet s�lectionn�.
   * @return Le type
   * @see PRProjet
   */
  public int getTypeProjet() {
    int r;
    r= Definitions.stringToProjet((String)coType_.getSelectedItem());
    return r;
  }
  /**
   * Retourne la racine du projet.
   * @return La racine du projet.
   */
  public String getRacineProjet() {
    PRPnParamsNouveauProjet pn;
    pn=
      (PRPnParamsNouveauProjet)hTp2PnParams_.get(new Integer(getTypeProjet()));
    return pn.getRacineProjet();
  }
  /**
   * Retourne les parametres s�lectionn�s sous forme d'objets.
   */
  public Object[] getParametres() {
    PRPnParamsNouveauProjet pn;
    pn=
      (PRPnParamsNouveauProjet)hTp2PnParams_.get(new Integer(getTypeProjet()));
    return pn.getParametres();
  }
  /**
   * M�thode appel�e lors d'un �venement ItemEvent sur coMethode_.
   */
  void coType__itemStateChanged(ItemEvent _evt) {
    lyParams_.show(pnParams_, (String)_evt.getItem());
  }
}
