/*
 * @file         HY3dInterfaceCalcul.java
 * @creation     2001-01-08
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d3;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.calcul.ICalcul;
import org.fudaa.dodico.corba.mesure.IEvolutionConstante;
import org.fudaa.dodico.corba.mesure.IEvolutionSerieIrreguliere;
import org.fudaa.dodico.corba.reflux3d.*;

import org.fudaa.dodico.reflux3d.DParametresReflux3d;
import org.fudaa.dodico.reflux3d.DResultatsReflux3d;

import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;

import org.fudaa.fudaa.reflux.*;

/**
 * Transfert du projet vers les structures Reflux 3d courantologie et calcul.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author Bertrand Marchand
 */
public class HY3dInterfaceCalcul extends PRInterfaceCalcul {
  /**
   * Serveur de calcul.
   */
  private static ICalcul serveur_ ;

  /**
   * Cr�ation d'une interface de calcul.
   */
  public HY3dInterfaceCalcul() {
    if (serveur_ == null) serveur_ = Reflux.SERVEUR_REFLUX3D;
  }

  /**
   * Appel au serveur de calcul Reflux 3D.
   * 
   * @param _prj Projet.
   * @exception IllegalArgumentException Erreur de l'application. Des donn�es sont incoh�rentes ou non initialis�es
   */
  public void calculer(PRProjet _prj) {
    // Transfert vers l'objet parametres
    versParametresReflux(_prj);
    // Calcul
    serveur_.calcul(Reflux.CONNEXION_REFLUX3D);
  }

  /**
   * Transfert des donn�es du projet vers les fichiers d'entr�es de Reflux.
   * 
   * @param _prj Projet � transf�rer sur fichiers d'entr�e.
   * @param _rac Racine des fichiers avec le chemin.
   */
  public void versFichiersEntree(PRProjet _prj, File _rac) throws IOException {
    IParametresReflux3d par = IParametresReflux3dHelper.narrow(serveur_.parametres(Reflux.CONNEXION_REFLUX3D));
    String racine;
    String path = _rac.getPath();
    String[] exts = Definitions.getExtFichiersDonnees();
    // Controle de l'extension pass�e
    racine = path;
    for (int i = 0; i < exts.length; i++)
      if (path.endsWith(exts[i])) {
        racine = path.substring(0, path.lastIndexOf(CtuluLibString.DOT + exts[i]));
        break;
      }
    // Transfert des donn�es vers un objet adapt�
    versParametresReflux(_prj);
    // par.fichier(new File(racine).getName());
    // Ecriture des fichiers d'entr�e
    DParametresReflux3d.ecritSurFichiers(racine, par);
  }

  /**
   * R�cup�ration des solutions finales depuis un fichier local. Les solutions finales sont les solutions du dernier pas
   * de temps.
   * 
   * @param _prj Le projet.
   * @param _file Fichier des solutions finales.
   * @return Les solutions finales.
   */
  public PRSolutionsInitiales[] getSolutionsFinales(PRProjet _prj, File _file) throws IOException {
    PRSolutionsInitiales[] r;
    SResultatsR3dVNO res;
    SResultatsEtapeR3dVNO pasRes;
    int nbPas;
    GrMaillageElement mail = _prj.maillage();
    GrNoeud[] nds = mail.noeuds();
    int[] nbFctsNds = ((HY3dProjet) _prj).nbFonctionsNoeuds();
    res = DResultatsReflux3d.litResultatsReflux3dVNO(_file, nds.length, nbFctsNds);
    if (res == null || res.etapes == null || res.etapes.length <= 0) throw new IOException(
        "Le fichier r�sultat n'est pas compatible avec le projet courant");
    nbPas = res.etapes.length;
    pasRes = res.etapes[nbPas - 1];
    if (pasRes.lignes.length != nds.length || pasRes.lignes[0].valeurs.length < 8) throw new IOException(
        "Le fichier r�sultat n'est pas compatible avec le projet courant");
    r = new PRSolutionsInitiales[nds.length];
    for (int i = 0; i < pasRes.lignes.length; i++) {
      SResultatsLigneR3dVNO ndRes = pasRes.lignes[i];
      int nbFcts = (ndRes.valeurs.length - 5) / 3;
      double[] vals = new double[nbFcts * 3 + 1];
      // double[] u=new double[nbFcts];
      // double[] v=new double[nbFcts];
      // double[] w=new double[nbFcts];
      // double h;
      vals[vals.length - 1] = ndRes.valeurs[3];
      for (int j = 0; j < nbFcts * 3; j++)
        vals[j] = ndRes.valeurs[j + 5];
      // h=ndRes.valeurs[3];
      // for (int j=0; j<nbFcts; j++) u[j]=ndRes.valeurs[j+5];
      // for (int j=0; j<nbFcts; j++) v[j]=ndRes.valeurs[j+nbFcts+5];
      // for (int j=0; j<nbFcts; j++) w[j]=ndRes.valeurs[j+nbFcts*2+5];
      r[i] = new PRSolutionsInitiales(nds[i], vals);
      // r[i]=new PRSolutionsInitiales(nds[i],u,v,w,h);
    }
    return r;
  }

  /**
   * R�cup�ration des solutions finales sur le serveur. Les solutions finales sont les solutions du dernier pas de
   * temps.
   * 
   * @param _prj Le projet.
   * @return Les solutions finales.
   */
  public PRSolutionsInitiales[] getSolutionsFinales(PRProjet _prj) throws IOException {
    PRSolutionsInitiales[] r;
    SResultatsR3dVNO res;
    SResultatsEtapeR3dVNO pasRes;
    int nbPas;
    GrMaillageElement mail = _prj.maillage();
    GrNoeud[] nds = mail.noeuds();
    IParametresReflux3d par = IParametresReflux3dHelper.narrow(serveur_.parametres(Reflux.CONNEXION_REFLUX3D));
    IResultatsReflux3d ires = IResultatsReflux3dHelper.narrow(serveur_.resultats(Reflux.CONNEXION_REFLUX3D));
    par.setRacine(new File(_prj.racineFichiers()).getName(), ires);
    res = ires.resultatsVNO();
    if (res == null || res.etapes == null || res.etapes.length <= 0) throw new IOException(
        "Serveur de calcul : Un des fichiers r�sultats est manquant\n"
            + "ou n'est pas compatible avec le projet courant");
    nbPas = res.etapes.length;
    pasRes = res.etapes[nbPas - 1];
    if (pasRes.lignes.length != nds.length || pasRes.lignes[0].valeurs.length < 8) throw new IOException(
        "Serveur de calcul : Un des fichiers r�sultats est manquant\n"
            + "ou n'est pas compatible avec le projet courant");
    r = new PRSolutionsInitiales[nds.length];
    for (int i = 0; i < pasRes.lignes.length; i++) {
      SResultatsLigneR3dVNO ndRes = pasRes.lignes[i];
      int nbFcts = (ndRes.valeurs.length - 5) / 3;
      double[] vals = new double[nbFcts * 3 + 1];
      // double[] u=new double[nbFcts];
      // double[] v=new double[nbFcts];
      // double[] w=new double[nbFcts];
      // double h;
      vals[vals.length - 1] = ndRes.valeurs[3];
      for (int j = 0; j < nbFcts * 3; j++)
        vals[j] = ndRes.valeurs[j + 5];
      // h=ndRes.valeurs[3];
      // for (int j=0; j<nbFcts; j++) u[j]=ndRes.valeurs[j+5];
      // for (int j=0; j<nbFcts; j++) v[j]=ndRes.valeurs[j+nbFcts+5];
      // for (int j=0; j<nbFcts; j++) w[j]=ndRes.valeurs[j+nbFcts*2+5];
      r[i] = new PRSolutionsInitiales(nds[i], vals);
      // r[i]=new PRSolutionsInitiales(nds[i],u,v,w,h);
    }
    return r;
  }

  /**
   * Retourne les r�sultats nodaux. <strong>ATTENTION : Cette methode ne fonctionne pas du fait que le nombre de
   * colonnes par noeud est variable</strong>. Probablement faudrait-il utiliser le fichier .SOL � la place du vno.
   * 
   * @param _prj Le projet.
   * @param _racine La racine des fichiers du projet.
   * @return Les r�sultats nodaux.
   */
  public PRResultats getResultats(PRProjet _prj, File _racine) throws IOException {
    SResultatsR3dVNO res;
    SResultatsEtapeR3dVNO pasRes;
    int nbPas;
    int nbRes;
    int nbCols;
    GrMaillageElement mail = _prj.maillage();
    GrNoeud[] nds = mail.noeuds();
    int[] nbFctsNds = ((HY3dProjet) _prj).nbFonctionsNoeuds();
    File file = new File(_racine.getPath() + ".vno");
    res = DResultatsReflux3d.litResultatsReflux3dVNO(file, nds.length, nbFctsNds);
    nbPas = res.etapes.length;
    pasRes = res.etapes[nbPas - 1];
    if (pasRes.lignes.length != nds.length || (nbRes = pasRes.lignes[0].valeurs.length) < 8) throw new IOException(
        "Le fichier " + file + " n'est pas compatible avec le projet courant");
    // Stockage des valeurs dans le format ad�quat.
    PRResultats.Etape[] etapes = new PRResultats.Etape[res.etapes.length];
    for (int i = 0; i < res.etapes.length; i++) {
      PRResultats.Etape.Colonne[] cols = new PRResultats.Etape.Colonne[7];
      nbCols = 0;
      for (int j = 0; j < 9; j++) {
        double[] vals = new double[res.etapes[i].lignes.length];
        for (int k = 0; k < vals.length; k++)
          vals[k] = res.etapes[i].lignes[k].valeurs[j];
        cols[nbCols++] = new PRResultats.Etape.Colonne(vals);
      }
      etapes[i] = new PRResultats.Etape(res.etapes[i].instant, cols);
    }
    return new PRResultats(etapes);
  }

  /**
   * Retourne si le serveur a pu �tre joint.
   * 
   * @return <i>true</i> Le serveur est pr�sent. <i>false</i> Le serveur est inexistant
   */
  public boolean serveurExiste() {
    return serveur_ != null && ((ICalculReflux3d) serveur_).estOperationnel();
  }

  /**
   * Retourne si le calcul s'est bien d�roul�.
   * 
   * @return <i>true</i> : Ok, <i>false</i> : Probl�me.
   */
  public boolean calculEstOK() {
    return serveurExiste() ? ((ICalculReflux3d) serveur_).estOK() : false;
  }

  /**
   * Retourne la trace d'ex�cution du calcul.
   * 
   * @return La chaine d'ex�cution.
   */
  public String calculTraceExecution() {
    return serveurExiste() ? ((ICalculReflux3d) serveur_).traceExecution() : null;
  }

  /**
   * Transfert du projet vers un objet parametres adapt� au calcul.
   * 
   * @param _prj Le projet
   */
  private void versParametresReflux(PRProjet _prj) throws IllegalArgumentException {
    boolean bclTransitoires;
    IParametresReflux3d par = IParametresReflux3dHelper.narrow(serveur_.parametres(Reflux.CONNEXION_REFLUX3D));
    IResultatsReflux3d res = IResultatsReflux3dHelper.narrow(serveur_.resultats(Reflux.CONNEXION_REFLUX3D));
    // Racine des fichiers calcul
    // par.fichier((new File(_prj.racineFichiers())).getName());
    par.setRacine((new File(_prj.racineFichiers())).getName(), res);
    // Parametres INP
    SParametresR3dINP pINP = new SParametresR3dINP();
    bclTransitoires = versParametresRefluxINP(pINP, _prj);
    par.parametresINP(pINP);
    // Parametres INI
    SParametresR3dINI pINI = new SParametresR3dINI();
    versParametresRefluxINI(pINI, _prj);
    par.parametresINI(pINI);
    // Parametres CL
    if (bclTransitoires) {
      SParametresR3dCL pCL = new SParametresR3dCL();
      versParametresRefluxCL(pCL, pINP, _prj);
      par.parametresCL(pCL);
    }
  }

  /**
   * Transfert du projet dans un format des parametres INP serveur.
   * 
   * @param _params La structure contenant les informations en format serveur.
   * @param _projet Le projet.
   * @return <i>true</i> Le projet contient des conditions limites transitoires. <i>false</i> sinon.
   */
  private boolean versParametresRefluxINP(SParametresR3dINP _params, PRProjet _prj) {
    boolean bclTransitoires = false;
    GrMaillageElement mail = _prj.maillage();
    PRModeleCalcul mdlCal = _prj.modeleCalcul();
    PRModeleProprietes mdlPrp = _prj.modeleProprietes();
    PRSollicitation[] sos = _prj.sollicitations();
    PRConditionLimite[] cls = _prj.conditionsLimites();
    GrNoeud[] nds = mail.noeuds();
    GrElement[] els = mail.elements();
    PRNormale[] norms = mdlPrp.normales();
    PRPropriete[] prpsEls = mdlPrp.proprietesElements();
    Vector vnatBds = mdlPrp.naturesBords();
    PRPropriete[] prpsGlobs = mdlPrp.proprietesGlobales();
    PRGroupePT[] gpsPT = mdlCal.groupesPT();
    Object[] parsCal = mdlCal.parametresCalcul();
    // Table de correspondance Noeud->Indice
    Hashtable hnd2Ind = new Hashtable();
    for (int i = 0; i < nds.length; i++)
      hnd2Ind.put(nds[i], new Integer(i));
    // Table de correspondance El�ment->Indice
    Hashtable hel2Ind = new Hashtable();
    for (int i = 0; i < els.length; i++)
      hel2Ind.put(els[i], new Integer(i));
    // D�termination de la dur�e du calcul pour controle de l'�tendue des
    // courbes non stationnaires
    double calDeb = Double.POSITIVE_INFINITY;
    double calFin = Double.NEGATIVE_INFINITY;
    if (gpsPT[0].schema().type() != PRSchemaResolution.STATIONNAIRE) {
      calDeb = gpsPT[0].temps().debut() + gpsPT[0].temps().pas();
      calFin = gpsPT[gpsPT.length - 1].temps().fin();
    }
    // --- Coordonn�es des noeuds ---------------------------------------------
    SParametresR3dCoordonnees[] reCoors = new SParametresR3dCoordonnees[nds.length];
    for (int i = 0; i < nds.length; i++) {
      GrPoint pt = nds[i].point_;
      reCoors[i] = new SParametresR3dCoordonnees(pt.x_, pt.y_, pt.z_);
    }
    _params.coordonnees = reCoors;
    // --- Nombre de noeuds sur la verticale ----------------------------------
    _params.nbNoeudsVerticale = ((Double) parsCal[4]).intValue();
    // --- Nombre de degr�s de libert� par noeuds -----------------------------
    int[] reNbsDLN = new int[nds.length];
    for (int i = 0; i < els.length; i++) {
      GrNoeud[] ndsSommets;
      GrNoeud[] ndsMilieux;
      ndsSommets = els[i].noeudsSommets();
      for (int j = 0; j < ndsSommets.length; j++)
        reNbsDLN[((Integer) hnd2Ind.get(ndsSommets[j])).intValue()] = 3;
      ndsMilieux = els[i].noeudsMilieux();
      for (int j = 0; j < ndsMilieux.length; j++)
        reNbsDLN[((Integer) hnd2Ind.get(ndsMilieux[j])).intValue()] = 2;
    }
    _params.nbsDLNoeuds = reNbsDLN;
    // --- Propri�t�s nodales -------------------------------------------------
    SParametresR3dProprietesNoeud[] rePrpsNds = new SParametresR3dProprietesNoeud[nds.length];
    for (int i = 0; i < nds.length; i++)
      rePrpsNds[i] = new SParametresR3dProprietesNoeud(new double[] { 0., 0. });
    // Initialisation de la 1�re propri�t� nodale (Valeur de la normale)
    for (int i = 0; i < norms.length; i++) {
      int iNd = ((Integer) hnd2Ind.get(norms[i].support())).intValue();
      rePrpsNds[iNd].valeurs[0] = norms[i].valeur();
    }
    // Initialisation de la 2�me propri�t� nodale (Valeur de d�bit lin�ique)
    for (int i = 0; i < sos.length; i++) {
      Vector supports;
      int iNd;
      // On passe les sollicitations qui ne sont pas de type debit
      if (sos[i].type() != PRSollicitation.DEBIT) continue;
      supports = sos[i].supports();
      for (int j = 0; j < supports.size(); j++) {
        // On ne traite que les supports de type GrNoeud
        if (!(supports.get(j) instanceof GrNoeud)) continue;
        iNd = ((Integer) hnd2Ind.get(supports.get(j))).intValue();
        if (!(sos[i].evolution() instanceof IEvolutionConstante)) throw new IllegalArgumentException("Noeud "
            + (iNd + 1) + ": Les d�bits lin�iques transitoires ne sont pas support�s");
        rePrpsNds[iNd].valeurs[0] = ((IEvolutionConstante) sos[i].evolution()).constante();
      }
    }
    _params.propsNoeuds = rePrpsNds;
    // --- Nombre de fonctions aux noeuds par noeud ---------------------------
    // Ces informations ne sont n�cessaires que pour le relecture des fichiers
    _params.nbFonctionsNoeuds = ((HY3dProjet) _prj).nbFonctionsNoeuds();
    // --- Nombre maxi de noeuds par element ----------------------------------
    int reNbMaxNds = 0;
    for (int i = 0; i < els.length; i++)
      reNbMaxNds = Math.max(reNbMaxNds, els[i].noeuds_.length);
    _params.nbMaxiNoeudsElement = reNbMaxNds;
    // --- Conditions limites -------------------------------------------------
    Vector vreCls = new Vector();
    for (int i = 0; i < cls.length; i++) {
      Vector supports;
      double val;
      int code;
      int[] codes;
      double[] vals;
      int[] tmpISupports;
      int[] iSupports;
      int nbSupports = 0;
      // Controle de validit� des supports pour cette c.l.
      supports = cls[i].supports();
      tmpISupports = new int[supports.size()];
      for (int j = 0; j < supports.size(); j++) {
        int iNd;
        // On ne traite que les supports de type GrNoeud
        if (!(supports.get(j) instanceof GrNoeud)) continue;
        iNd = ((Integer) hnd2Ind.get(supports.get(j))).intValue();
        // Cas d'une condition limite hauteur => On passe les noeuds milieux
        if (cls[i].type() == PRConditionLimite.HAUTEUR && _params.nbsDLNoeuds[iNd] == 2) continue;
        tmpISupports[nbSupports++] = iNd;
      }
      // Si aucun noeud support => On passe la c.l.
      if (nbSupports == 0) continue;
      iSupports = new int[nbSupports];
      System.arraycopy(tmpISupports, 0, iSupports, 0, nbSupports);
      // Codes et valeurs
      // Stationnaires
      if (cls[i].evolution() instanceof IEvolutionConstante) {
        code = 2;
        val = ((IEvolutionConstante) cls[i].evolution()).constante();
      }
      // Transitoires si la condition n'est pas de type hauteur.
      else if (cls[i].type() != PRConditionLimite.HAUTEUR) {
        throw new IllegalArgumentException("Les conditions limites transitoires de type vitesse ne sont pas "
            + "support�es");
      }
      // Transitoires
      else {
        bclTransitoires = true;
        code = 1;
        val = 0.;
        double crbDeb = cls[i].evolution().debut();
        double crbFin = cls[i].evolution().fin();
        if (crbDeb > calDeb || crbFin < calFin) throw new IllegalArgumentException("La courbe '"
            + _prj.getName(cls[i].evolution()) + "' n'est pas assez �tendue\n" + "Courbe: {" + crbDeb + ";" + crbFin
            + "} ; Calcul: {" + calDeb + ";" + calFin + "}");
      }
      if (cls[i].type() == PRConditionLimite.VITESSE_X) {
        codes = new int[] { code, 0, 0 };
        vals = new double[] { val, 0, 0 };
      } else if (cls[i].type() == PRConditionLimite.VITESSE_Y) {
        codes = new int[] { 0, code, 0 };
        vals = new double[] { 0, val, 0 };
      } else {
        codes = new int[] { 0, 0, code };
        vals = new double[] { 0, 0, val };
      }
      // Condition limite Reflux
      vreCls.add(new SParametresR3dConditionLimite(codes, vals, iSupports));
    }
    // Transfert vers structures Reflux
    _params.conditionsLimites = new SParametresR3dConditionLimite[vreCls.size()];
    vreCls.toArray(_params.conditionsLimites);
    // --- Connectivit� des �l�ments + types -----------------------------------
    Vector vreConns = new Vector();
    Vector vreTypes = new Vector();
    // El�ments de fond
    for (int i = 0; i < els.length; i++) {
      GrNoeud[] ndsEl = els[i].noeuds_;
      int[] reConns = new int[ndsEl.length];
      for (int j = 0; j < ndsEl.length; j++)
        reConns[j] = ((Integer) hnd2Ind.get(ndsEl[j])).intValue();
      vreConns.add(new SParametresR3dConnectivite(reConns));
      vreTypes.add(new Integer(5));
    }
    // El�ments de bord
    for (int i = 0; i < vnatBds.size(); i++) {
      PRNature nat = (PRNature) vnatBds.get(i);
      GrNoeud[] ndsEl = ((GrElement) nat.supports().get(0)).noeuds_;
      int[] reConns = new int[ndsEl.length];
      // El�ment de type vitesse
      if (nat.type() == PRNature.BORD_OUVERT_VITESSE) vreTypes.add(new Integer(6));
      // El�ment de type d�bit
      else if (nat.type() == PRNature.BORD_OUVERT_DEBIT) vreTypes.add(new Integer(7));
      // El�ment de type frottement de paroi
      else if (nat.type() == PRNature.BORD_FERME_FROTTEMENT) vreTypes.add(new Integer(8));
      // Nature d'un autre type => Pas de cr�ation d'�l�ment.
      else
        continue;
      // Connectivit�
      for (int j = 0; j < ndsEl.length; j++)
        reConns[j] = ((Integer) hnd2Ind.get(ndsEl[j])).intValue();
      vreConns.add(new SParametresR3dConnectivite(reConns));
    }
    // Transfert vers structures Reflux
    _params.connectivites = new SParametresR3dConnectivite[vreConns.size()];
    vreConns.toArray(_params.connectivites);
    _params.typesElements = new int[vreTypes.size()];
    for (int i = 0; i < vreTypes.size(); i++)
      _params.typesElements[i] = ((Integer) vreTypes.get(i)).intValue();
    // --- Fonctions aux noeuds par �lement + Nombre maxi de fonctions --------
    // Pour les �l�ments de bord, la base et le nombre de fonctions valent 0.
    SParametresR3dFonctionNoeud[] reFcts = new SParametresR3dFonctionNoeud[_params.connectivites.length];
    // El�ments de fond
    for (int i = 0; i < els.length; i++)
      reFcts[i] = new SParametresR3dFonctionNoeud(-1, -1);
    // El�ments de bord
    for (int i = els.length; i < _params.connectivites.length; i++)
      reFcts[i] = new SParametresR3dFonctionNoeud(0, 0);
    int reNbMaxFct = 0;
    // Remplissage des structures
    for (int i = 0; i < prpsEls.length; i++) {
      Vector supports;
      double val;
      supports = prpsEls[i].supports();
      for (int j = 0; j < supports.size(); j++) {
        int iEl = ((Integer) hel2Ind.get(supports.get(j))).intValue();
        if (!(prpsEls[i].evolution() instanceof IEvolutionConstante)) throw new IllegalArgumentException("El�ment "
            + (iEl + 1) + ": " + "Les propri�t�s �l�mentaires transitoires ne sont pas support�es");
        val = ((IEvolutionConstante) prpsEls[i].evolution()).constante();
        switch (prpsEls[i].type()) {
        default:
        case PRPropriete.NB_FCT:
          if (val < 1) throw new IllegalArgumentException("El�ment " + (iEl + 1) + ": "
              + "Le nombre de termes de la base doit �tre sup�rieur ou �gal � 1");
          reFcts[iEl].nbFonctions = (int) val;
          break;
        case PRPropriete.BASE:
          reFcts[iEl].iBase = (int) val;
          break;
        }
      }
    }
    // Controle que tous les champs sont bien affect�s + nb max fct
    for (int i = 0; i < reFcts.length; i++) {
      if (reFcts[i].iBase == -1 || reFcts[i].nbFonctions == -1) throw new IllegalArgumentException("El�ment " + (i + 1)
          + ": " + "Une propri�t� �l�mentaire n'est pas affect�e");
      reNbMaxFct = Math.max(reNbMaxFct, reFcts[i].nbFonctions);
    }
    _params.nbMaxiFonctions = reNbMaxFct;
    _params.fonctionsNoeuds = reFcts;
    // --- Groupe de propri�t�s �l�mentaires ----------------------------------
    SParametresR3dGroupePE reGpe = new SParametresR3dGroupePE(new double[13]);
    for (int i = 0; i < prpsGlobs.length; i++)
      reGpe.valeurs[i] = ((IEvolutionConstante) prpsGlobs[i].evolution()).constante();
    for (int i = 0; i < 6; i++)
      reGpe.valeurs[prpsGlobs.length + i] = ((Double) parsCal[5 + i]).doubleValue();
    _params.groupePE = reGpe;
    // --- Groupes de pas de temps --------------------------------------------
    SParametresR3dGroupePT reGpt = new SParametresR3dGroupePT();
    PRSchemaResolution sch = gpsPT[0].schema();
    PRMethodeResolution mth = gpsPT[0].methode();
    PRBoucleTemps temps = gpsPT[0].temps();
    double[] coefsMth = mth.coefficients();
    reGpt.convection = ((Double) parsCal[0]).intValue();
    reGpt.frottement = ((Double) parsCal[1]).intValue();
    reGpt.pression = ((Double) parsCal[2]).intValue();
    reGpt.calculMatrice = ((Double) parsCal[3]).intValue();
    switch (sch.type()) {
    case PRSchemaResolution.LAX_WENDROFF:
      reGpt.schemaResolution = 1;
      break;
    default:
      reGpt.schemaResolution = 2;
      break;
    }
    reGpt.precisionNR = coefsMth[0];
    reGpt.nbMaxIterations = (int) coefsMth[1];
    // Cas du sch�ma stationnaire: On prend un sch�ma euler avec un seul pas
    // de temps et un pas de temps=1.e8
    if (sch.type() == PRSchemaResolution.STATIONNAIRE) {
      reGpt.nbPasDeTemps = 1;
      reGpt.valeurPasDeTemps = 1.e8;
    } else {
      reGpt.nbPasDeTemps = temps.nbPas();
      reGpt.valeurPasDeTemps = temps.pas();
    }
    reGpt.pasImpression = gpsPT[0].frequenceStockage();
    _params.groupePT = reGpt;
    return bclTransitoires;
  }

  /**
   * Transfert du projet dans un format des parametres INI serveur.
   * 
   * @param _params La structure contenant les informations en format serveur.
   * @param _projet Le projet.
   */
  private void versParametresRefluxINI(SParametresR3dINI _params, PRProjet _prj) {
    PRModeleCalcul mdlCal = _prj.modeleCalcul();
    PRSolutionsInitiales[] sis = mdlCal.solutionsInitiales();
    GrNoeud[] nds = _prj.maillage().noeuds();
    boolean[] nTrs;
    double[] ne = new double[nds.length];
    double[] hts = new double[sis.length];
    // Noeuds de transit
    for (int i = 0; i < sis.length; i++) {
      double[] vals = sis[i].valeurs();
      hts[i] = vals[vals.length - 1];
    }
    // for (int i=0; i<sis.length; i++) hts[i]=sis[i].hauteur();
    nTrs = _prj.getNoeudsTransit(hts);
    // --- Calcul des niveaux d'eau ---------------------------------------------
    for (int i = 0; i < nds.length; i++) {
      // Noeud sec => niveau d'eau = bathy
      if (!nTrs[i] && hts[i] < 0.) {
        ne[i] = nds[i].point_.z_;
      }
      // Noeud de transit ou noeud mouille
      else {
        ne[i] = nds[i].point_.z_ + hts[i];
      }
    }
    // --- Remplissage de la structure Reflux -----------------------------------
    _params.u = new double[nds.length][];
    _params.v = new double[nds.length][];
    _params.w = new double[nds.length][];
    _params.hs = new double[nds.length];
    for (int i = 0; i < sis.length; i++) {
      double[] vals = sis[i].valeurs();
      int nbFcts = (vals.length - 1) / 3;
      _params.u[i] = new double[nbFcts];
      _params.v[i] = new double[nbFcts];
      _params.w[i] = new double[nbFcts];
      System.arraycopy(vals, 0, _params.u[i], 0, nbFcts);
      System.arraycopy(vals, nbFcts, _params.v[i], 0, nbFcts);
      System.arraycopy(vals, nbFcts * 2, _params.w[i], 0, nbFcts);
      // _params.u[i]=sis[i].u();
      // _params.v[i]=sis[i].v();
      // _params.w[i]=sis[i].w();
      _params.hs[i] = ne[i];
    }
  }

  /**
   * Transfert du projet dans un format des parametres CL serveur.
   * 
   * @param _params La structure contenant les informations en format serveur.
   * @param _parINP La structure pour le fichier .inp
   * @param _projet Le projet.
   */
  private void versParametresRefluxCL(SParametresR3dCL _params, SParametresR3dINP _parINP, PRProjet _prj) {
    GrMaillageElement mail = _prj.maillage();
    PRModeleProprietes mdlPrp = _prj.modeleProprietes();
    PRModeleCalcul mdlCal = _prj.modeleCalcul();
    PRConditionLimite[] cls = _prj.conditionsLimites();
    GrNoeud[] nds = mail.noeuds();
    GrElement[] els = mail.elements();
    // PRPropriete[] prpsEls=mdlPrp.proprietesElements();
    PRGroupePT[] gpsPT = mdlCal.groupesPT();
    PRBoucleTemps temps = gpsPT[0].temps();
    IEvolutionSerieIrreguliere[] crbs = new IEvolutionSerieIrreguliere[cls.length];
    Hashtable[] hnbFct2Vnds = new Hashtable[cls.length];
    int nbCls = 0;
    // Table de correspondance Noeud->Indice
    Hashtable hnd2Ind = new Hashtable();
    for (int i = 0; i < nds.length; i++)
      hnd2Ind.put(nds[i], new Integer(i));
    // Table de correspondance El�ment->Indice
    Hashtable hel2Ind = new Hashtable();
    for (int i = 0; i < els.length; i++)
      hel2Ind.put(els[i], new Integer(i));
    // --------------------------------------------------------------------------
    // --- Pour chaque c.l. => Stockage de la courbe, groupement --------------
    // --- des noeuds par nombre de termes. -----------------------------------
    // --------------------------------------------------------------------------
    for (int i = 0; i < cls.length; i++) {
      Vector supports = cls[i].supports();
      // On ne traite que les conditions limites transitoires
      if (!(cls[i].evolution() instanceof IEvolutionSerieIrreguliere)) continue;
      hnbFct2Vnds[i] = new Hashtable();
      // Controle de validit� des supports pour cette c.l.
      for (int j = 0; j < supports.size(); j++) {
        int iNd;
        Vector vnds;
        int nbFcts;
        // On ne traite que les supports de type GrNoeud
        if (!(supports.get(j) instanceof GrNoeud)) continue;
        iNd = ((Integer) hnd2Ind.get(supports.get(j))).intValue();
        // Cas d'une condition limite hauteur => On passe les noeuds milieux
        if (cls[i].type() == PRConditionLimite.HAUTEUR && _parINP.nbsDLNoeuds[iNd] == 2) continue;
        // Stockage du noeud dans le vecteur contenant d�j� des noeuds � m�me
        // nb de fonctions
        nbFcts = _parINP.nbFonctionsNoeuds[iNd];
        vnds = (Vector) hnbFct2Vnds[i].get(new Integer(nbFcts));
        if (vnds == null) {
          vnds = new Vector();
          hnbFct2Vnds[i].put(new Integer(nbFcts), vnds);
          nbCls++;
        }
        vnds.add(new Integer(iNd));
      }
      // Si aucun noeud support => On passe la c.l.
      if (hnbFct2Vnds[i].size() == 0) continue;
      crbs[i] = (IEvolutionSerieIrreguliere) cls[i].evolution();
    }
    // --------------------------------------------------------------------------
    // --- Transfert vers les structures Reflux -------------------------------
    // --------------------------------------------------------------------------
    int nbPas = temps.nbPas();
    double pas = temps.pas();
    SParametresR3dConditionLimite[][] reCls = new SParametresR3dConditionLimite[nbPas][nbCls];
    // Pour chaque condition limite
    nbCls = 0;
    for (int i = 0; i < cls.length; i++) {
      if (crbs[i] == null) continue;
      // Pour chaque groupe de noeuds
      for (Enumeration e = hnbFct2Vnds[i].keys(); e.hasMoreElements();) {
        double[] vals;
        int[] codes;
        int[] iNds;
        double vpdt = 0;
        int nbFctsNd = ((Integer) e.nextElement()).intValue();
        Vector vnds = (Vector) hnbFct2Vnds[i].get(new Integer(nbFctsNd));
        iNds = new int[vnds.size()];
        for (int j = 0; j < vnds.size(); j++)
          iNds[j] = ((Integer) vnds.get(j)).intValue();
        codes = new int[nbFctsNd * 2 + 1];
        for (int j = 0; j < codes.length - 1; j++)
          codes[j] = 0;
        codes[codes.length - 1] = 1;
        vals = new double[nbFctsNd * 2 + 1];
        for (int j = 0; j < codes.length - 1; j++)
          vals[j] = 0;
        // Pour chaque pas de temps
        for (int j = 0; j < nbPas; j++) {
          double[] vals2 = new double[vals.length];
          int[] iNds2 = new int[iNds.length];
          int[] codes2 = new int[codes.length];
          System.arraycopy(iNds, 0, iNds2, 0, iNds.length);
          System.arraycopy(codes, 0, codes2, 0, codes.length);
          System.arraycopy(vals, 0, vals2, 0, vals.length);
          vpdt += pas;
          vals2[vals2.length - 1] = crbs[i].valeur((int) (vpdt));
          reCls[j][nbCls] = new SParametresR3dConditionLimite(codes2, vals2, iNds2);
        }
        nbCls++;
      }
    }
    _params.conditionsLimites = reCls;
  }
}
