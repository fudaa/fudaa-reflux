package org.fudaa.fudaa.reflux;

import org.fudaa.fudaa.commun.trace2d.ZModeleChangeListener;

/**
 * Un modele de vecteur. Utilis� pour le trac� des vecteurs repr�sentant des
 * r�sultats.
 */
public interface ZModeleVecteur {
  /**
   * Valeur x pour l'index i.
   * @return La valeur ou Double.NaN si aucune valeur pour l'index donn�.
   */
  double getValeurX(int i);
  /**
   * Valeur y pour l'index i.
   * @return La valeur ou Double.NaN si aucune valeur pour l'index donn�.
   */
  double getValeurY(int i);
  /**
   * Norme pour l'index i.
   * @return La valeur ou Double.NaN si aucune valeur pour l'index donn�.
   */
  double getNorme(int i);
  /**
   * Nombre de vecteurs du modele.
   * @return Le nombre de valeurs. 0 si aucune valeur
   */
  int getNbVecteurs();
  /**
   * Norme min des vecteurs.
   * @return Le min ou Double.NaN si aucune valeur.
   */
  double getMinNorme();
  /**
   * Norme max des vecteurs.
   * @return Le max ou Double.NaN si aucune valeur.
   */
  double getMaxNorme();
  /**
   * Add Model listener.
   */
  void addModelChangeListener(ZModeleChangeListener _listener);
  /**
   * Remove model listener.
   */
  void removeModelChangeListener(ZModeleChangeListener _listener);
}
