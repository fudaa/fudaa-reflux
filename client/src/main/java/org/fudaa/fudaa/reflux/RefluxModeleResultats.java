package org.fudaa.fudaa.reflux;
import java.util.HashSet;
import java.util.Iterator;

import org.fudaa.fudaa.commun.trace2d.ZModeleChangeEvent;
import org.fudaa.fudaa.commun.trace2d.ZModeleChangeListener;
import org.fudaa.fudaa.commun.trace2d.ZModeleValeur;
/**
 * Un modele de valeur bas� sur les r�sultats avec changement possible du pas
 * de temps s�lectionn� pour la colonne donn�e.
 */
class RefluxModeleResultats implements ZModeleValeur {
  private PRResultats results_;
  private int type_= PRResultats.UNDEFINED;
  private Double t_;
  private PRResultats.Etape.Colonne col_;
  /** Listeners for modifications in modele. */
  protected HashSet listeners_= new HashSet();
  public RefluxModeleResultats(PRResultats _results, int _type) {
    results_= _results;
    type_= _type;
  }
  /**
   * Pour changer le pas de temps sur le modele.
   */
  public void setSelectedStep(Double _t) {
    t_= _t;
    col_= null;
    fireModelChange(
      new ZModeleChangeEvent(this, ZModeleChangeEvent.VALUES_CHANGED));
  }
  /**
   * Retourne le pas de temps s�lectionn�.
   */
  public Double getSelectedStep() {
    return t_;
  }
  /**
   * Retourne l'index du pas.
   */
  public int getIndexOfStep(Double _t) {
    if (_t == null)
      return -1;
    for (int i= 0; i < results_.getNbSteps(); i++)
      if (results_.getStep(i).t == _t.doubleValue())
        return i;
    return -1;
  }
  protected void fireModelChange(ZModeleChangeEvent _evt) {
    for (Iterator i= listeners_.iterator(); i.hasNext();) {
      ((ZModeleChangeListener)i.next()).modelChanged(_evt);
    }
  }
  public double valeur(int i) {
    if (col_ == null) {
      int ind= getIndexOfStep(t_);
      if (ind == -1)
        return Double.NaN;
      col_= results_.getStep(ind).getColonneOfType(type_);
      if (col_ == null)
        return Double.NaN;
    }
    if (col_.valeurs.length <= i || i < 0)
      return Double.NaN;
    return col_.valeurs[i];
  }
  public int nbValeurs() {
    if (col_ == null) {
      int ind= getIndexOfStep(t_);
      if (ind == -1)
        return 0;
      col_= results_.getStep(ind).getColonneOfType(type_);
      if (col_ == null)
        return 0;
    }
    return col_.valeurs.length;
  }
  public double getMin() {
    double r= Double.NaN;
    if (nbValeurs() > 0)
      r= Double.POSITIVE_INFINITY;
    for (int i= 0; i < nbValeurs(); i++)
      r= Math.min(r, valeur(i));
    return r;
  }
  public double getMax() {
    double r= Double.NaN;
    if (nbValeurs() > 0)
      r= Double.NEGATIVE_INFINITY;
    for (int i= 0; i < nbValeurs(); i++)
      r= Math.max(r, valeur(i));
    return r;
  }
  public void addModelChangeListener(ZModeleChangeListener _listener) {
    listeners_.add(_listener);
  }
  public void removeModelChangeListener(ZModeleChangeListener _listener) {
    listeners_.remove(_listener);
  }
  // <<< ZmodeleValeur  --------------------------------------------------------
}
