/*
 * @file         PRNormale.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import org.fudaa.ebli.geometrie.GrNoeud;
/**
 * Une classe d�finissant une normale � un support (noeud, point, g�ometrie).
 * Une normale d�finie en un noeud est prioritaire sur les autres.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class PRNormale {
  private GrNoeud support_;
  private double valeur_;
  public PRNormale(GrNoeud _support, double _valeur) {
    this.support(_support);
    this.valeur(_valeur);
  }
  public void support(GrNoeud _support) {
    this.support_= _support;
  }
  public GrNoeud support() {
    return this.support_;
  }
  public void valeur(double _valeur) {
    this.valeur_= _valeur;
  }
  public double valeur() {
    return this.valeur_;
  }
}
