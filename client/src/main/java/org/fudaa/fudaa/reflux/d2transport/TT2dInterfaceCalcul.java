/*
 * @file         TT2dInterfaceCalcul.java
 * @creation     2001-01-08
 * @modification $Date: 2007-01-19 13:14:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d2transport;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.calcul.ICalcul;
import org.fudaa.dodico.corba.mesure.IEvolution;
import org.fudaa.dodico.corba.mesure.IEvolutionConstante;
import org.fudaa.dodico.corba.mesure.IEvolutionSerieIrreguliere;
import org.fudaa.dodico.corba.reflux.*;

import org.fudaa.dodico.reflux.DParametresReflux;
import org.fudaa.dodico.reflux.DResultatsReflux;

import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;

import org.fudaa.fudaa.commun.conversion.FudaaInterpolateurMaillage;
import org.fudaa.fudaa.reflux.*;
import org.fudaa.fudaa.refonde.MessageEvent;
import org.fudaa.fudaa.refonde.MessageListener;
import org.fudaa.fudaa.refonde.RefondeImplementation;
import org.fudaa.fudaa.refonde.RefondeMaillage;
import org.fudaa.fudaa.refonde.RefondeNoeudData;
import org.fudaa.fudaa.refonde.RefondeProjet;
import org.fudaa.fudaa.refonde.RefondeResultats;
import org.fudaa.fudaa.refonde.RefondeTacheInterruptionException;
/**
 * Transfert du projet vers les structures Reflux et calcul.
 *
 * @version      $Revision: 1.9 $ $Date: 2007-01-19 13:14:37 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class TT2dInterfaceCalcul extends PRInterfaceCalcul {
  /**
   * Serveur de calcul.
   */
  private static ICalcul serveur_;
  /**
   * Cr�ation d'une interface de calcul.
   */
  public TT2dInterfaceCalcul() {
    if (serveur_ == null)
      serveur_= Reflux.SERVEUR_REFLUX;
    // Refonde a besoin d'�tre initialis� avec les connexions serveurs dunes pour
    // relecture d'un projet. La connexion au serveur se fait dans Reflux,
    // mais est pass�e � Refonde.
    if (RefondeImplementation.SERVEUR_DUNES == null) {
      RefondeImplementation.SERVEUR_DUNES= Reflux.SERVEUR_DUNES;
      RefondeImplementation.CONNEXION_DUNES= Reflux.CONNEXION_DUNES;
    }
  }
  /**
   * Appel au serveur de calcul Reflux 2D.
   *
   * @param _prj Projet.
   * @exception IllegalArgumentException Erreur de l'application. Des donn�es
   *            sont incoh�rentes ou non initialis�es
   */
  public void calculer(PRProjet _prj) {
    // Transfert vers l'objet parametres
    versParametresReflux(_prj);
    message("Ecriture des fichiers d'entr�e � Reflux...", 100);
    // Calcul
    serveur_.calcul(Reflux.CONNEXION_REFLUX);
  }
  /**
   * Transfert des donn�es du projet vers les fichiers d'entr�es de Reflux.
   *
   * @param _prj Projet � transf�rer sur fichiers d'entr�e.
   * @param _rac Racine des fichiers avec le chemin.
   */
  public void versFichiersEntree(PRProjet _prj, File _rac) throws IOException {
    IParametresReflux par=
      IParametresRefluxHelper.narrow(
        serveur_.parametres(Reflux.CONNEXION_REFLUX));
    String racine;
    String path= _rac.getPath();
    String[] exts= Definitions.getExtFichiersDonnees();
    // Controle de l'extension pass�e
    racine= path;
    for (int i= 0; i < exts.length; i++)
      if (path.endsWith(exts[i])) {
        racine= path.substring(0, path.lastIndexOf(CtuluLibString.DOT + exts[i]));
        break;
      }
    // Transfert des donn�es vers un objet adapt�
    versParametresReflux(_prj);
    // Ecriture du fichier des parametres
    message("Ecriture des fichiers d'entr�e � Reflux...", 100);
    DParametresReflux.ecritSurFichiers(racine, par);
  }
  /**
   * R�cup�ration des solutions finales depuis un fichier local. Les solutions
   * finales sont les solutions du dernier pas de temps.
   *
   * @param _prj Le projet.
   * @param _file Fichier des solutions finales.
   * @return Les solutions finales.
   */
  public PRSolutionsInitiales[] getSolutionsFinales(PRProjet _prj, File _file)
    throws IOException {
    PRSolutionsInitiales[] r;
    SResultatsReflux res;
    SResultatsEtapeReflux pasRes;
    int nbPas;
    GrMaillageElement mail= _prj.maillage();
    GrNoeud[] nds= mail.noeuds();
    res= DResultatsReflux.litResultatsReflux(_file, nds.length);
    if (res == null || res.etapes == null || res.etapes.length <= 0)
      throw new IOException("Le fichier r�sultat n'est pas compatible avec le projet courant");
    nbPas= res.etapes.length;
    pasRes= res.etapes[nbPas - 1];
    if (pasRes.lignes.length != nds.length
      || pasRes.lignes[0].valeurs.length < 8)
      throw new IOException(
        "Le fichier " + _file + " n'est pas compatible avec le projet courant");
    r= new PRSolutionsInitiales[nds.length];
    for (int i= 0; i < pasRes.lignes.length; i++) {
      SResultatsLigneReflux ndRes= pasRes.lignes[i];
      double c= ndRes.valeurs[6];
      double zf= ndRes.valeurs[7];
      r[i]= new PRSolutionsInitiales(nds[i], new double[] { c, zf });
    }
    //    for (int i=0; i<r.length; i++) {
    //      System.out.println("Noeud : "+i+" "+r[i].valeur()+" "+_prj.conditionsInitiales()[i].valeur());
    //    }
    return r;
  }
  /**
   * R�cup�ration des solutions finales sur le serveur. Les solutions finales
   * sont les solutions du dernier pas de temps.
   *
   * @param _prj Le projet.
   * @return Les solutions finales.
   */
  public PRSolutionsInitiales[] getSolutionsFinales(PRProjet _prj)
    throws IOException {
    PRSolutionsInitiales[] r;
    SResultatsReflux res;
    SResultatsEtapeReflux pasRes;
    int nbPas;
    GrMaillageElement mail= _prj.maillage();
    GrNoeud[] nds= mail.noeuds();
    IParametresReflux par=
      IParametresRefluxHelper.narrow(
        serveur_.parametres(Reflux.CONNEXION_REFLUX));
    IResultatsReflux ires=
      IResultatsRefluxHelper.narrow(
        serveur_.resultats(Reflux.CONNEXION_REFLUX));
    par.setRacine(new File(_prj.racineFichiers()).getName(), ires);
    res= ires.resultatsReflux();
    if (res == null || res.etapes == null || res.etapes.length <= 0)
      throw new IOException(
        "Serveur de calcul : Un des fichiers r�sultats est manquant\n"
          + "ou n'est pas compatible avec le projet courant");
    nbPas= res.etapes.length;
    pasRes= res.etapes[nbPas - 1];
    if (pasRes.lignes.length != nds.length
      || pasRes.lignes[0].valeurs.length < 8)
      throw new IOException(
        "Serveur de calcul : Un des fichiers r�sultats est manquant\n"
          + "ou n'est pas compatible avec le projet courant");
    r= new PRSolutionsInitiales[nds.length];
    for (int i= 0; i < pasRes.lignes.length; i++) {
      SResultatsLigneReflux ndRes= pasRes.lignes[i];
      double c= ndRes.valeurs[6];
      double zf= ndRes.valeurs[7];
      r[i]= new PRSolutionsInitiales(nds[i], new double[] { c, zf });
    }
    return r;
  }
  /**
   * Retourne les r�sultats nodaux.
   *
   * @param _prj Le projet.
   * @param _racine La racine des fichiers du projet.
   * @return Les r�sultats nodaux.
   */
  public PRResultats getResultats(PRProjet _prj, File _racine)
    throws IOException {
    SResultatsReflux res;
    SResultatsEtapeReflux pasRes;
    int nbPas;
    //int nbRes;
    int nbCols;
    final int[] tpResults=
      {
        PRResultats.COORD_X,
        PRResultats.COORD_Y,
        PRResultats.BATHYMETRIE,
        PRResultats.VITESSE_X,
        PRResultats.VITESSE_Y,
        -1,
        PRResultats.NIVEAU_EAU,
        PRResultats.PROFONDEUR,
        -1 };
    GrMaillageElement mail= _prj.maillage();
    GrNoeud[] nds= mail.noeuds();
    File file= new File(_racine.getPath() + ".sov");
    res= DResultatsReflux.litResultatsReflux(file, nds.length);
    nbPas= res.etapes.length;
    pasRes= res.etapes[nbPas - 1];
    if (pasRes.lignes.length != nds.length
      || (/*nbRes= */pasRes.lignes[0].valeurs.length) < 8)
      throw new IOException(
        "Le fichier " + file + " n'est pas compatible avec le projet courant");
    // Stockage des valeurs dans le format ad�quat.
    PRResultats.Etape[] etapes= new PRResultats.Etape[res.etapes.length];
    for (int i= 0; i < res.etapes.length; i++) {
      PRResultats.Etape.Colonne[] cols= new PRResultats.Etape.Colonne[7];
      nbCols= 0;
      for (int j= 0; j < 9; j++) {
        if (tpResults[j] == -1)
          continue; // On passe les colonnes sans r�sultats.
        double[] vals= new double[res.etapes[i].lignes.length];
        for (int k= 0; k < vals.length; k++)
          vals[k]= res.etapes[i].lignes[k].valeurs[j];
        cols[nbCols++]= new PRResultats.Etape.Colonne(tpResults[j], vals);
      }
      etapes[i]= new PRResultats.Etape(res.etapes[i].instant, cols);
      // Add vitesse norm
      PRResultats.Etape.Colonne vx=
        etapes[i].getColonneOfType(PRResultats.VITESSE_X);
      PRResultats.Etape.Colonne vy=
        etapes[i].getColonneOfType(PRResultats.VITESSE_Y);
      PRResultats.Etape.Colonne norm=
        new PRResultats.Etape.Colonne(
          PRResultats.NORME,
          new double[vx.valeurs.length]);
      for (int j= 0; j < norm.valeurs.length; j++) {
        norm.valeurs[j]=
          Math.sqrt(
            vx.valeurs[j] * vx.valeurs[j] + vy.valeurs[j] * vy.valeurs[j]);
      }
      etapes[i].add(norm);
    }
    return new PRResultats(etapes);
  }
  /**
   * Retourne si le serveur a pu �tre joint et est op�rationnel.
   * @return <i>true</i> Le serveur est pr�sent.
   *         <i>false</i> Le serveur est inexistant
   */
  public boolean serveurExiste() {
    return serveur_ != null && ((ICalculReflux)serveur_).estOperationnel();
  }
  /**
   * Retourne si le calcul s'est bien d�roul�.
   * @return <i>true</i> : Ok, <i>false</i> : Probl�me.
   */
  public boolean calculEstOK() {
    return serveurExiste() ? ((ICalculReflux)serveur_).estOK() : false;
  }
  /**
   * Retourne la trace d'ex�cution du calcul.
   * @return La chaine d'ex�cution.
   */
  public String calculTraceExecution() {
    return serveurExiste() ? ((ICalculReflux)serveur_).traceExecution() : null;
  }
  /**
   * Transfert du projet vers un objet parametres adapt� au calcul.
   * @param _prj Le projet
   */
  private void versParametresReflux(PRProjet _prj)
    throws IllegalArgumentException {
    IParametresReflux par=
      IParametresRefluxHelper.narrow(
        serveur_.parametres(Reflux.CONNEXION_REFLUX));
    IResultatsReflux res=
      IResultatsRefluxHelper.narrow(
        serveur_.resultats(Reflux.CONNEXION_REFLUX));
    // Racine des fichiers calcul
    par.setRacine((new File(_prj.racineFichiers())).getName(), res);
    // Parametres INP
    SParametresRefluxINP pINP= new SParametresRefluxINP();
    versParametresRefluxINP(pINP, _prj);
    par.parametresINP(pINP);
    // Parametres PHY
    SParametresRefluxPHY pPHY= new SParametresRefluxPHY();
    versParametresRefluxPHY(pPHY, pINP, _prj);
    par.parametresPHY(pPHY);
    // Parametres SI
    SParametresRefluxSI pSI= new SParametresRefluxSI();
    versParametresRefluxSI(pSI, pINP, _prj);
    par.parametresSI(pSI);
    // Parametres CL
    if (pINP.bCLTransitoires) {
      SParametresRefluxCL pCL= new SParametresRefluxCL();
      versParametresRefluxCL(pCL, pINP, _prj);
      par.parametresCL(pCL);
    }
    // Parametres PN
    if (pINP.bPNTransitoires) {
      SParametresRefluxPN pPN= new SParametresRefluxPN();
      versParametresRefluxPN(pPN, pINP, _prj);
      par.parametresPN(pPN);
    }
  }
  /**
   * Transfert du projet dans un format des parametres INP serveur.
   *
   * @param _params La structure contenant les informations en format serveur.
   * @param _projet Le projet.
   */
  private void versParametresRefluxINP(
    SParametresRefluxINP _params,
    PRProjet _prj) {
    GrMaillageElement mail= _prj.maillage();
    PRModeleCalcul mdlCal= _prj.modeleCalcul();
    PRModeleProprietes mdlPrp= _prj.modeleProprietes();
    PRSollicitation[] sos= _prj.sollicitations();
    GrElement[] els= mail.elements();
    GrNoeud[] nds= mail.noeuds();
    Object[] parCal= mdlCal.parametresCalcul();
    PRPropriete[] prpsGlob= mdlPrp.proprietesGlobales();
    // Nombre de propri�t�s �l�mentaires pour les �l�ments de fond
    int nbPEFond= 3;
    // Valeur du diam�tre de grain D50.
    double d50= 0;
    double calDeb;
    double calFin;
    Hashtable hnd2Num= new Hashtable();
    for (int i= 0; i < nds.length; i++)
      hnd2Num.put(nds[i], new Integer(i));
    Hashtable hel2Num= new Hashtable();
    for (int i= 0; i < els.length; i++)
      hel2Num.put(els[i], new Integer(i));
    PRGroupePT[] grpPTs= mdlCal.groupesPT();
    //--------------------------------------------------------------------------
    //
    //      Transfert des informations du probleme en parametres REFLUX
    //
    //--------------------------------------------------------------------------
    //---  D�termination de la dur�e du calcul pour controle de l'�tendue des
    //     courbes non stationnaires
    calDeb= Double.POSITIVE_INFINITY;
    calFin= Double.NEGATIVE_INFINITY;
    if (grpPTs[0].schema().type() != PRSchemaResolution.STATIONNAIRE) {
      calDeb= grpPTs[0].temps().debut() + grpPTs[0].temps().pas();
      calFin= grpPTs[grpPTs.length - 1].temps().fin();
    }
    //--------------------------------------------------------------------------
    //---  Definition des groupes de PREL / Numeros de groupes  ----------------
    //---  de PREL par �l�ment / Type des �l�ments / connectivit�s  ------------
    //--------------------------------------------------------------------------
    {
      GrElement[][] bords= mail.aretesContours();
      //      PRPropriete[] gPE;                 // Groupe de P.E. courant
      double[] gpes;
      // Table d'indexage direct element => Propri�t�s �l�mentaires
      //      PRPropriete[][] elementsPE=new PRPropriete[els.length][nbPEFond];
      int[] connectivite;
      //      double[] valeurs;                         // Valeurs des P.E.
      int cpt; // Compteur
      Vector natsBds= mdlPrp.naturesBords();
      PRPropriete[] prpsBds= mdlPrp.proprietesAretes();
      Vector rePREL= new Vector(); // Groupes de P.E.
      Vector reCONN= new Vector(els.length); // Connectivit� des �lements
      Vector reNGPE= new Vector(els.length);
      // Num�ros de groupe de P.E. par �l�ment
      Vector reTPEL= new Vector(els.length); // Types des �l�ments
      PRNature nature;
      //IEvolutionConstante courbe;
      Hashtable hbd2Nat; // bord->Nature
      Hashtable hbd2PA; // bord->Propriete d'arete
      PRPropriete pA; // Propri�t� d'ar�te
      Object[] supports;
      Hashtable hel2PE;
      //---  Elements de fond  -------------------------------------------------
      //--- Indexation element->Vecteur de propri�t�s de fond
      {
        PRPropriete[] pes= mdlPrp.proprietesElements();
        hel2PE= new Hashtable(els.length);
        for (int i= 0; i < pes.length; i++) {
          Vector vsups= pes[i].supports();
          for (int j= 0; j < vsups.size(); j++) {
            Vector vpes;
            Object el= vsups.get(j);
            if ((vpes= (Vector)hel2PE.get(el)) == null) {
              vpes= new Vector(nbPEFond);
              hel2PE.put(el, vpes);
            }
            vpes.add(pes[i]);
          }
        }
      }
      //--- Remplissage de la table d'indexage direct
      //      {
      //        PRPropriete[] pe=mdlPrp.proprietesElements();
      //
      //        for (int i=0; i<pe.length; i++) {
      //          supports=pe[i].supports().toArray();
      //          for (int j=0; j<supports.length; j++) {
      //            int ind;
      //
      //            switch (pe[i].type()) {
      //             default:
      //             case PRPropriete.RUGOSITE:  ind=0; break;
      //             case PRPropriete.D50_GRAIN: ind=1; break;
      //             case PRPropriete.D90_GRAIN: ind=2; break;
      //            }
      //
      //            // Remplissage de la table
      //            elementsPE[((Integer)hel2Num.get(supports[j])).intValue()][ind]=pe[i];
      //          }
      //        }
      //      }
      //--- Cr�ation d'une propri�t� �l�mentaire bidon = 0
      //    pour le bon d�roulement de la suite des op�rations
      //      {
      //        IEvolutionConstante crb=new CEvolutionConstante(); crb.constante(0);
      //        PRPropriete peZero=new PRPropriete(PRPropriete.PERTE_CHARGE,crb,new Vector());
      //
      //        for (int i=0; i<els.length; i++) {
      //          if (_prj.type()==PRProjet.VIT_NU_CON) elementsPE[i][2]=peZero;
      //          if (elementsPE[i][3]==null) elementsPE[i][3]=peZero;
      //        }
      //      }
      //--- Controle que tous les �l�ments ont bien leurs propri�t�s affect�es
      for (int i= 0; i < els.length; i++) {
        Vector vpes= (Vector)hel2PE.get(els[i]);
        //        for (int j=0; j<nbPEFond; j++) {
        //          if (elementsPE[i][j]==null)
        if (vpes == null || vpes.size() != nbPEFond)
          throw new IllegalArgumentException(
            "L'�l�ment "
              + (i + 1)
              + " a une propri�t� �l�mentaire non affect�e");
        //        }
      }
      //--- Cr�ation des groupes de propri�t�s
      ELEMENTS : for (int i= 0; i < els.length; i++) {
        double[] gpe= new double[11];
        for (int j= 0; j < gpe.length; j++)
          gpe[j]= 0;
        Vector vpes= (Vector)hel2PE.get(els[i]);
        for (int j= 0; j < vpes.size(); j++) {
          PRPropriete pe= (PRPropriete)vpes.get(j);
          int ind;
          switch (pe.type()) {
            default :
            case PRPropriete.RUGOSITE :
              ind= 2;
              break;
            case PRPropriete.D50_GRAIN :
              ind= 3;
              break;
            case PRPropriete.D90_GRAIN :
              ind= 4;
              break;
          }
          gpe[ind]= ((IEvolutionConstante)pe.evolution()).constante();
        }
        reNGPE.add(new Integer(getNumGpe(rePREL, gpe)));
        //        // Recherche d'un groupe de P.E. existant
        //        GPE: for (int j=0; j<rePREL.size(); j++) {
        //          gPE = (PRPropriete[]) rePREL.elementAt(j);
        //          for (int k=0; k<nbPEFond; k++) {
        //            if (((IEvolutionConstante)gPE[k].evolution()).constante()!=
        //                ((IEvolutionConstante)elementsPE[i][k].evolution()).constante()) continue GPE;
        //          }
        //
        //          // Groupe trouv� => On affecte son num�ro � l'�l�ment
        //          reNGPE.addElement(new Integer(j+1));
        //          continue ELEMENTS;
        //        }
        //
        //        // Groupe inexistant => Nouveau groupe
        //        gpes=new double[11];
        //        gPE = new PRPropriete[nbPEFond];
        //        for (int k=0; k<nbPEFond; k++) gPE[k] = elementsPE[i][k];
        //        rePREL.addElement(gPE);
        //        // Affectation du num�ro de groupe de P.E.
        //        reNGPE.addElement(new Integer(rePREL.size()));
      }
      //--- Types des �l�ments de fond
      for (int i= 0; i < els.length; i++) {
        if (els[i].type_ == GrElement.T3) {
          reTPEL.addElement(new Integer(33));
        } else
          throw new IllegalArgumentException(
            "Le type d'�l�ment "
              + els[i].type_
              + "est invalide pour un projet de type "
              + Definitions.projetToString(_prj.type()));
      }
      //---  Connectivit�s
      for (int i= 0; i < els.length; i++) {
        GrNoeud[] ndsEl= els[i].noeuds_;
        connectivite= new int[ndsEl.length];
        for (int j= 0; j < ndsEl.length; j++)
          connectivite[j]= ((Integer)hnd2Num.get(ndsEl[j])).intValue();
        reCONN.addElement(connectivite);
      }
      //---  Elements de bord  -------------------------------------------------
      // Bord->Nature
      hbd2Nat= new Hashtable(natsBds.size() > 0 ? natsBds.size() : 1);
      for (Enumeration e= natsBds.elements(); e.hasMoreElements();) {
        nature= (PRNature)e.nextElement();
        hbd2Nat.put(nature.supports().get(0), nature);
      }
      // Bord->Propri�t� d'ar�te
      hbd2PA= new Hashtable();
      for (int i= 0; i < prpsBds.length; i++) {
        pA= prpsBds[i];
        supports= pA.supports().toArray();
        for (int j= 0; j < supports.length; j++)
          hbd2PA.put(supports[j], pA);
      }
      //---  R�cup�ration des informations
      for (int i= 0; i < bords.length; i++) {
        for (int j= 0; j < bords[i].length; j++) {
          nature= (PRNature)hbd2Nat.get(bords[i][j]);
          GrNoeud[] ndsEl= null;
          // Element de type fronti�re ouverte libre: L'�l�ment de fronti�re
          // ouverte libre est un �lement T3, dont la connectivit� commence par
          // le premier noeud d'ar�te.
          if (nature.type() == PRNature.BORD_OUVERT_LIBRE) {
            ndsEl= bords[i][j].elementsSupport_[0].noeuds_;
            reTPEL.addElement(new Integer(34));
            reNGPE.addElement(new Integer(0));
            int decal;
            for (decal= 0; decal < ndsEl.length; decal++)
              if (ndsEl[decal] == bords[i][j].noeuds_[0])
                break;
            connectivite= new int[ndsEl.length];
            for (int k= 0; k < ndsEl.length; k++)
              connectivite[k]=
                ((Integer)hnd2Num.get(ndsEl[(k + decal) % ndsEl.length]))
                  .intValue();
            reCONN.addElement(connectivite);
          }
          // Element de type d�bit solide
          else if (nature.type() == PRNature.BORD_OUVERT_DEBIT_SOL) {
            ndsEl= bords[i][j].noeuds_;
            reTPEL.addElement(new Integer(35));
            reNGPE.addElement(new Integer(0));
            connectivite= new int[ndsEl.length];
            for (int k= 0; k < ndsEl.length; k++)
              connectivite[k]= ((Integer)hnd2Num.get(ndsEl[k])).intValue();
            reCONN.addElement(connectivite);
          }
          // Element de type flux de concentration
          else if (nature.type() == PRNature.BORD_OUVERT_FLUX_CONC) {
            ndsEl= bords[i][j].noeuds_;
            reTPEL.addElement(new Integer(36));
            reNGPE.addElement(new Integer(0));
            connectivite= new int[ndsEl.length];
            for (int k= 0; k < ndsEl.length; k++)
              connectivite[k]= ((Integer)hnd2Num.get(ndsEl[k])).intValue();
            reCONN.addElement(connectivite);
          }
        }
      }
      // Controle que tous les �l�ments ont bien la m�me valeur de grain D50.
      // Ceci s'av�re n�cessaire car le bloc TRAN prend une valeur de grain
      // globale.
      d50= ((double[])rePREL.get(0))[3];
      for (int i= 1; i < rePREL.size(); i++) {
        if (d50 != ((double[])rePREL.get(i))[3])
          throw new IllegalArgumentException(
            "La valeur de la propri�t� \""
              + Definitions.proprieteToString(PRPropriete.D50_GRAIN)
              + "\" doit �tre �gale pour tous les �l�ments.");
      }
      //---  Propri�t�s �l�mentaires globales  ---------------------------------
      for (int i= 0; i < rePREL.size(); i++) {
        double[] gpe= (double[])rePREL.get(i);
        gpe[0]=
          ((IEvolutionConstante)prpsGlob[TT2dResource.DIFFU_LONGI].evolution())
            .constante();
        gpe[1]=
          ((IEvolutionConstante)prpsGlob[TT2dResource.DIFFU_TRAN].evolution())
            .constante();
        // Porosit� (formulation sysiphe)=1/(1-porosit�) (formulation Reflux)
        // => Porosit� Reflux=1-1/porosit� sysiphe
        gpe[5]=
          1
            - 1
              / ((IEvolutionConstante)prpsGlob[TT2dResource
                .POROSITE]
                .evolution())
                .constante();
        gpe[6]=
          ((IEvolutionConstante)prpsGlob[TT2dResource.SHIELDS].evolution())
            .constante();
        gpe[7]=
          ((IEvolutionConstante)prpsGlob[TT2dResource.COEF_PSI].evolution())
            .constante();
        gpe[8]= ((Double)parCal[TT2dResource.DEBIT_REF]).doubleValue();
        gpe[9]= ((Double)parCal[TT2dResource.PROF_REF]).doubleValue();
        gpe[10]= ((Double)parCal[TT2dResource.CONC_REF]).doubleValue();
      }
      //---  Propri�t� �l�mentaire suppl�mentaire pour les bancs  --------------
      //     couvrants/d�couvrants
      //      for (int i=0; i<grpPTs.length; i++) {
      //        PRMethodeResolution meth=grpPTs[i].methode();
      //
      //        if (meth.type()==PRMethodeResolution.NEWTON_RAPHSON_BCD ||
      //            meth.type()==PRMethodeResolution.SELECTED_LUMPING_BCD) {
      //
      //          double[] coefs=meth.coefficients();
      //          CEvolutionConstante evol=new CEvolutionConstante();
      //          PRPropriete pe=new PRPropriete();
      //
      //          if (meth.type()==PRMethodeResolution.NEWTON_RAPHSON_BCD)
      //           evol.constante(coefs[4]);
      //          else
      //           evol.constante(coefs[1]);
      //
      //          pe.type(PRPropriete.VISCOSITE);
      //          pe.evolution(evol);
      //          rePREL.add(new PRPropriete[]{pe});
      //          break;
      //        }
      //      }
      //---  Remplissage de la structure  --------------------------------------
      // Groupes de PE
      cpt= 0;
      _params.groupesPE= new SParametresRefluxGroupePE[rePREL.size()];
      for (int i= 0; i < rePREL.size(); i++) {
        gpes= (double[])rePREL.get(i);
        _params.groupesPE[cpt++]= new SParametresRefluxGroupePE(gpes);
        //        gPE=(PRPropriete[])e.nextElement();
        //        valeurs=new double[gPE.length];
        //        for (int j=0; j<gPE.length; j++)
        //         valeurs[j]=((IEvolutionConstante)gPE[j].evolution()).constante();
        //        _params.groupesPE[cpt++]=new SParametresRefluxGroupePE(valeurs);
      }
      // Numero de groupes de PE
      cpt= 0;
      _params.numeroGroupePE= new int[reNGPE.size()];
      for (Enumeration e= reNGPE.elements(); e.hasMoreElements();)
        _params.numeroGroupePE[cpt++]= ((Integer)e.nextElement()).intValue();
      // Types des �l�ments
      cpt= 0;
      _params.typesElement= new int[reTPEL.size()];
      for (Enumeration e= reTPEL.elements(); e.hasMoreElements();)
        _params.typesElement[cpt++]= ((Integer)e.nextElement()).intValue();
      // Connectivit�s
      cpt= 0;
      _params.connectivites= new SParametresRefluxConnectivite[reCONN.size()];
      for (Enumeration e= reCONN.elements(); e.hasMoreElements();)
        _params.connectivites[cpt++]=
          new SParametresRefluxConnectivite((int[])e.nextElement());
    }
    // Type de projet s�dimentologie
    _params.typeProbleme= 2;
    // Coordonn�es des noeuds. La renum�rotation/optimisation doit avoir �t�
    // effectu�e si on ne veut pas de d�passement de tableau.
    // B.M. Il serait peut �tre judicieux de controler si ca a �t� fait.
    {
      //double[] coordonnees=new double[3];
      _params.coordonnees= new SParametresRefluxCoordonnees[nds.length];
      for (int i= 0; i < nds.length; i++) {
        GrPoint pt= nds[i].point_;
        //        _params.coordonnees[nds[i].numero()] =
        _params.coordonnees[i]=
          new SParametresRefluxCoordonnees(pt.x_, pt.y_, pt.z_);
      }
    }
    // Nombre de degr�s de libert� par noeuds (2 pour tous, pas de noeuds milieux)
    {
      int[] nombresDLN= new int[nds.length];
      for (int i= 0; i < nds.length; i++)
        nombresDLN[i]= 2;
      _params.nombresDLN= nombresDLN;
    }
    //--- Conditions limites ---------------------------------------------------
    {
      PRConditionLimite[] cLs= _prj.conditionsLimites();
      SParametresRefluxConditionLimite[] refluxCLs=
        new SParametresRefluxConditionLimite[cLs.length];
      int[] codes= null;
      int code= 0;
      double[] valeurs= null;
      double valeur= 0;
      Vector supports= null;
      int[] tmpNumSupports= null;
      int[] numSupports= null;
      GrNoeud noeud= null;
      int nbSupports;
      int nbCLs= 0;
      // Pr�sence de conditions limites transitoires
      _params.bCLTransitoires= false;
      // Pour chaque C.L.
      for (int i= 0; i < cLs.length; i++) {
        // Pour chaque support
        supports= cLs[i].supports();
        tmpNumSupports= new int[supports.size()];
        nbSupports= 0;
        for (int j= 0; j < supports.size(); j++) {
          // Le support n'est pas un noeud => On le passe
          if (!(supports.get(j) instanceof GrNoeud))
            continue;
          noeud= (GrNoeud)supports.get(j);
          // Dans le cas d'une condition limite hauteur
          //          if (cLs[i].type()==PRConditionLimite.HAUTEUR) {
          //            // Noeud milieu => On le passe
          //            if (_params.nombresDLN[((Integer)hnd2Num.get(noeud)).intValue()]==2)
          //             continue;
          //          }
          tmpNumSupports[nbSupports]= ((Integer)hnd2Num.get(noeud)).intValue();
          nbSupports++;
        }
        // Si aucun noeud support => On passe
        if (nbSupports == 0)
          continue;
        numSupports= new int[nbSupports];
        System.arraycopy(tmpNumSupports, 0, numSupports, 0, nbSupports);
        // Codes / valeurs stationnaires
        if (cLs[i].evolution() instanceof IEvolutionConstante) {
          code= 2;
          valeur= ((IEvolutionConstante)cLs[i].evolution()).constante();
        }
        // Codes / valeurs transitoires
        else {
          code= 1;
          valeur= 0.;
          double crbDeb= cLs[i].evolution().debut();
          double crbFin= cLs[i].evolution().fin();
          if (crbDeb > calDeb || crbFin < calFin)
            throw new IllegalArgumentException(
              "La courbe '"
                + _prj.getName(cLs[i].evolution())
                + "' n'est pas assez �tendue\n"
                + "Courbe: {"
                + crbDeb
                + ";"
                + crbFin
                + "} ; Calcul: {"
                + calDeb
                + ";"
                + calFin
                + "}");
          _params.bCLTransitoires= true;
        }
        if (cLs[i].type() == PRConditionLimite.CONCENTR) {
          codes= new int[] { code, 0, 0 };
          valeurs= new double[] { valeur, 0., 0. };
        } else if (cLs[i].type() == PRConditionLimite.COTE_FOND) {
          codes= new int[] { 0, code, 0 };
          valeurs= new double[] { 0., valeur, 0. };
        } else {
          throw new IllegalArgumentException("Ce type de condition limite n'est pas autoris�");
        }
        // Condition limite Reflux
        refluxCLs[nbCLs]=
          new SParametresRefluxConditionLimite(codes, valeurs, numSupports);
        nbCLs++;
      }
      // Remplissage du tableau des conditions limites
      _params.conditionsLimites= new SParametresRefluxConditionLimite[nbCLs];
      System.arraycopy(refluxCLs, 0, _params.conditionsLimites, 0, nbCLs);
    }
    //--- Propri�t�s nodales ---------------------------------------------------
    {
      int code= 0;
      double val= 0;
      int ind;
      _params.proprietesNodales=
        new SParametresRefluxProprieteNodale[nds.length];
      GrNoeud support= null;
      Vector supports= null;
      PRNormale[] normales= mdlPrp.normales();
      // Pr�sence de propri�t�s nodales transitoires
      _params.bPNTransitoires= false;
      // Cr�ation des propri�t�s
      for (int i= 0; i < nds.length; i++) {
        _params.proprietesNodales[i]=
          new SParametresRefluxProprieteNodale(
            new int[] { 0, 0, 0 },
            new double[] { 0, 0, 0 });
      }
      // Initialisation de la 1�re propri�t� nodale (normale)
      for (int i= 0; i < normales.length; i++) {
        support= normales[i].support();
        int num= ((Integer)hnd2Num.get(support)).intValue();
        _params.proprietesNodales[num].code[0]= 2;
        _params.proprietesNodales[num].valeur[0]= normales[i].valeur();
      }
      // Initialisation des 2i�me et 3ieme propri�t� nodale
      // (flux de concentration,d�bit solide)
      for (int i= 0; i < sos.length; i++) {
        // Codes / valeurs stationnaires
        if (sos[i].evolution() instanceof IEvolutionConstante) {
          code= 2;
          val= ((IEvolutionConstante)sos[i].evolution()).constante();
        }
        // Codes / valeurs transitoires
        else {
          code= 1;
          val= 0.;
          double crbDeb= sos[i].evolution().debut();
          double crbFin= sos[i].evolution().fin();
          if (crbDeb > calDeb || crbFin < calFin)
            throw new IllegalArgumentException(
              "La courbe '"
                + _prj.getName(sos[i].evolution())
                + "' n'est pas assez �tendue\n"
                + "Courbe: {"
                + crbDeb
                + ";"
                + crbFin
                + "} ; Calcul: {"
                + calDeb
                + ";"
                + calFin
                + "}");
          _params.bPNTransitoires= true;
        }
        if (sos[i].type() == PRSollicitation.FLUX)
          ind= 1;
        else if (sos[i].type() == PRSollicitation.DEBIT_SOLIDE)
          ind= 2;
        else
          throw new IllegalArgumentException("Ce type de sollicitation n'est pas autoris�");
        supports= sos[i].supports();
        for (int j= 0; j < supports.size(); j++) {
          // On ne traite que les supports de type GrNoeud
          if (!(supports.get(j) instanceof GrNoeud))
            continue;
          support= (GrNoeud)supports.get(j);
          int num= ((Integer)hnd2Num.get(support)).intValue();
          _params.proprietesNodales[num].code[ind]= code;
          _params.proprietesNodales[num].valeur[ind]= val;
        }
      }
    }
    //--- Nombre maxi de noeuds par element ------------------------------------
    _params.nombreMaxiNoeudsElement= 0;
    for (int i= 0; i < _params.connectivites.length; i++) {
      _params.nombreMaxiNoeudsElement=
        Math.max(
          _params.nombreMaxiNoeudsElement,
          _params.connectivites[i].numeroNoeud.length);
    }
    //--- Sollicitations r�parties ---------------------------------------------
    _params.bSollicitationsReparties= false;
    for (int i= 0; i < sos.length; i++) {
      if (sos[i].type() == PRSollicitation.DEBIT_SOLIDE
        || sos[i].type() == PRSollicitation.FLUX) {
        _params.bSollicitationsReparties= true;
        break;
      }
    }
    //--- Bloc Transport -------------------------------------------------------
    {
      int formule= ((Integer)parCal[TT2dResource.FORMULE]).intValue();
      double periode= ((Double)parCal[TT2dResource.PERIODE]).doubleValue();
      int casHydro= ((Integer)parCal[TT2dResource.CAS_CHAMP]).intValue();
      int casMaree= ((Integer)parCal[TT2dResource.CAS_MAREE]).intValue();
      double hminCal= ((Double)parCal[TT2dResource.HMIN_CALCUL]).doubleValue();
      double coefPent= ((Double)parCal[TT2dResource.EFFET_PENTE]).doubleValue();
      double ndErod= ((Double)parCal[TT2dResource.ND_ERODABLE]).doubleValue();
      double t0Hydro= ((Double)parCal[TT2dResource.T0_HYDRO]).doubleValue();
      int icrb= ((Integer)parCal[TT2dResource.ICOURBE]).intValue();
      double densEau=
        ((IEvolutionConstante)prpsGlob[TT2dResource.DENS_EAU].evolution())
          .constante();
      double densSedi=
        ((IEvolutionConstante)prpsGlob[TT2dResource.DENS_SEDI].evolution())
          .constante();
      double porosite=
        ((IEvolutionConstante)prpsGlob[TT2dResource.POROSITE].evolution())
          .constante();
      double shields=
        ((IEvolutionConstante)prpsGlob[TT2dResource.SHIELDS].evolution())
          .constante();
      double visocDyn=
        ((IEvolutionConstante)prpsGlob[TT2dResource.VISCO_DYN].evolution())
          .constante();
      _params.blocTRAN= new SParametresRefluxTRAN();
      // Sous type
      if (_prj.type() == PRProjet.TRANS_TOT_2D)
        _params.blocTRAN.sousType= 0;
      else
        _params.blocTRAN.sousType= 1;
      // Propri�t�s
      _params.blocTRAN.proprietes= new double[10];
      _params.blocTRAN.proprietes[0]= densEau;
      _params.blocTRAN.proprietes[1]= densSedi;
      _params.blocTRAN.proprietes[2]= d50;
      // Le contr�le que D50 est �gal pour tous les �l�ments � �t� fait plus haut.
      _params.blocTRAN.proprietes[3]= porosite;
      _params.blocTRAN.proprietes[4]= shields;
      _params.blocTRAN.proprietes[5]= visocDyn;
      _params.blocTRAN.proprietes[6]= hminCal;
      _params.blocTRAN.proprietes[7]= coefPent;
      _params.blocTRAN.proprietes[8]= 0;
      _params.blocTRAN.proprietes[9]= ndErod;
      // Formule
      _params.blocTRAN.iFormule= formule;
      // Cas de vitesses
      if (casHydro == 2 && casMaree == 0)
        casHydro= 1;
      _params.blocTRAN.casHydro= casHydro;
      // P�riode de la mar�e
      _params.blocTRAN.periodeMaree= periode;
      // t0 s�dimentologie % t0 hydro
      _params.blocTRAN.t0surHydro= t0Hydro;
      // Courbe des coefficients de mar�e.
      if (casHydro == 2) {
        IEvolutionSerieIrreguliere crb=
          (IEvolutionSerieIrreguliere)_prj.evolutions()[icrb];
        long[] x= crb.instants().instants();
        double[] y= crb.serie();
        _params.blocTRAN.xcoefsMaree= new double[x.length];
        for (int i= 0; i < x.length; i++)
          _params.blocTRAN.xcoefsMaree[i]= x[i];
        _params.blocTRAN.ycoefsMaree= y;
      }
    }
    //--- Groupes de pas de temps ----------------------------------------------
    {
      int reNbPT; // Nombre de pas de temps
      double rePas; // Valeur du pas de temps
      int reSch; // Type du sch�ma
      double reCoefSch; // Coefficient du sch�ma
      int reMth; // Type de m�thode
      double reMthRelaxation; // Coefficient de relaxation
      double reMthPrecisionNR; // Coefficient de pr�cision NW/R
      double reMthPrecisionBCD; // Coefficient de pr�cision BCD
      int reMthNbMaxIterations; // Nombre max d'it�rations
      double[] reCoefCont; // Termes pris en comptes
      int reFreq; // Fr�quence de stockage
      double[] coefs;
      _params.groupesPT= new SParametresRefluxGroupePT[grpPTs.length];
      // Termes pris en compte.
      reCoefCont= new double[3];
      // Convection C et Zf
      if (((Integer)parCal[TT2dResource.CONVEC_C]).intValue() == 0) {
        if (((Integer)parCal[TT2dResource.CONVEC_ZF]).intValue() == 0)
          reCoefCont[0]= 0;
        else
          reCoefCont[0]= 4;
      } else {
        if (((Integer)parCal[TT2dResource.CONVEC_ZF]).intValue() == 0)
          reCoefCont[0]= 1;
        else
          reCoefCont[0]= 2;
      }
      // Diffusion
      reCoefCont[1]= ((Integer)parCal[TT2dResource.DIFFUSION]).intValue();
      // Source & puit
      reCoefCont[2]= _prj.type() == PRProjet.TRANS_SUS_2D ? 3 : 0;
      for (int i= 0; i < grpPTs.length; i++) {
        if (grpPTs[i].schema().type() == PRSchemaResolution.STATIONNAIRE) {
          reNbPT= 1;
          rePas= 1;
        } else {
          reNbPT= grpPTs[i].temps().nbPas();
          rePas= grpPTs[i].temps().pas();
        }
        reSch= grpPTs[i].schema().type();
        reCoefSch= grpPTs[i].schema().coefficient();
        reMth= grpPTs[i].methode().type();
        reFreq= grpPTs[i].frequenceStockage();
        // Param�tres de la m�thode
        coefs= grpPTs[i].methode().coefficients();
        if (grpPTs[i].methode().type() == PRMethodeResolution.NEWTON_RAPHSON) {
          reMthRelaxation= coefs[0];
          reMthPrecisionNR= coefs[1];
          // N'est pas utile, mais doit �tre initialis�>0 (sinon erreur Reflux)
          reMthPrecisionBCD= 0.005;
          reMthNbMaxIterations= (int)coefs[2];
        } else if (
          grpPTs[i].methode().type() == PRMethodeResolution.LAX_WENDROFF) {
          reMthRelaxation= coefs[0];
          reMthPrecisionNR= coefs[1];
          // N'est pas utile, mais doit �tre initialis�>0 (sinon erreur Reflux)
          reMthPrecisionBCD= 0.005;
          reMthNbMaxIterations= (int)coefs[2];
        } else {
          reMthRelaxation= 0;
          reMthPrecisionNR= 0;
          // N'est pas utile, mais doit �tre initialis�>0 (sinon erreur Reflux)
          reMthPrecisionBCD= 0.05;
          reMthNbMaxIterations= 0;
        }
        // Initialisation du groupe
        _params.groupesPT[i]= new SParametresRefluxGroupePT();
        _params.groupesPT[i].nombrePasDeTemps= reNbPT;
        _params.groupesPT[i].valeurPasDeTemps= rePas;
        _params.groupesPT[i].schemaResolution= reSch;
        _params.groupesPT[i].coefficientSchema= reCoefSch;
        _params.groupesPT[i].methodeResolution= reMth;
        _params.groupesPT[i].relaxation= reMthRelaxation;
        _params.groupesPT[i].precisionNR= reMthPrecisionNR;
        _params.groupesPT[i].precisionBCD= reMthPrecisionBCD;
        _params.groupesPT[i].nombreMaxIterations= reMthNbMaxIterations;
        _params.groupesPT[i].coefficientContribution= reCoefCont;
        _params.groupesPT[i].frequenceImpression= reFreq;
      }
      _params.t0= grpPTs[0].temps().debut();
    }
    //--- Pr�sence des bloc VENT, CRAD, TRAN -----------------------------------
    _params.bVent= false;
    _params.bCntRadiations= false;
    _params.bTransport= true;
    //--- Indices d'impression (fixes) -----------------------------------------
    {
      boolean[] impres= new boolean[9];
      for (int i= 0; i < impres.length; i++)
        impres[i]= false;
      impres[8]= true; // Impression des it�rations
      _params.impression= impres;
    }
  }
  /**
   * Transfert du projet dans un format des parametres SI serveur.
   *
   * @param _params La structure contenant les informations en format serveur.
   * @param _parINP La structure pour le fichier .inp
   * @param _projet Le projet.
   */
  private void versParametresRefluxSI(
    SParametresRefluxSI _params,
    SParametresRefluxINP _paramINP,
    PRProjet _prj) {
    GrMaillageElement mail= _prj.maillage();
    PRSolutionsInitiales[] sis= _prj.modeleCalcul().solutionsInitiales();
    GrNoeud[] nds= mail.noeuds();
    double[] vals= new double[nds.length * 2];
    //--------------------------------------------------------------------------
    //-------  Remplissage de la table des solutions initiales  ----------------
    //--------------------------------------------------------------------------
    for (int i= 0; i < nds.length; i++) {
      double[] v= sis[i].valeurs();
      vals[i * 2]= v[TT2dResource.CONCENTR];
      vals[i * 2 + 1]= v[TT2dResource.COTE_Z];
    }
    _params.valeurs= vals;
  }
  /**
   * Transfert du projet dans un format des parametres CL serveur.
   *
   * @param _params La structure contenant les informations en format serveur.
   * @param _parINP La structure pour le fichier .inp
   * @param _projet Le projet.
   */
  private void versParametresRefluxCL(
    SParametresRefluxCL _params,
    SParametresRefluxINP _paramINP,
    PRProjet _prj) {
    PRConditionLimite[] cls= _prj.conditionsLimites();
    GrMaillageElement mail= _prj.maillage();
    Object[] supports;
    Vector dDLTransitoire= new Vector();
    int nbPTs;
    int nbDDLs;
    int nddl;
    SParametresRefluxLigneTransitoire[] pTs;
    double valeurPT;
    double dtReel; // Decalage de la valeur du pas de temps du fait
    // que Reflux ne sait pas prendre en compte un t0.
    IEvolutionSerieIrreguliere courbe;
    int nbMaxDDL= 2; // Concentration, Cote Zf
    Hashtable hnd2Num= new Hashtable();
    {
      GrNoeud[] nds= mail.noeuds();
      for (int i= 0; i < nds.length; i++)
        hnd2Num.put(nds[i], new Integer(i));
    }
    IEvolution[][] dlts= new IEvolution[mail.noeuds().length][nbMaxDDL];
    for (int i= 0; i < dlts.length; i++)
      for (int j= 0; j < nbMaxDDL; j++)
        dlts[i][j]= null;
    //--------------------------------------------------------------------------
    //---  Stockage de la courbe pour chaque DDL transitoires  -----------------
    //--------------------------------------------------------------------------
    for (int i= 0; i < cls.length; i++) {
      if (cls[i].evolution() instanceof IEvolutionConstante)
        continue;
      // Pour chaque support
      supports= cls[i].supports().toArray();
      for (int j= 0; j < supports.length; j++) {
        // Le support n'est pas un noeud => On le passe
        if (!(supports[j] instanceof GrNoeud))
          continue;
        int num= ((Integer)hnd2Num.get(supports[j])).intValue();
        // Dans le cas d'une condition limite hauteur
        //        if (cls[i].type()==PRConditionLimite.HAUTEUR) {
        //          // Noeud milieu => On le passe
        //          if (_paramINP.nombresDLN[num]==2) continue;
        //        }
        //
        nddl= 0;
        if (cls[i].type() == PRConditionLimite.CONCENTR)
          nddl= 0;
        else if (cls[i].type() == PRConditionLimite.COTE_FOND)
          nddl= 1;
        dlts[num][nddl]= cls[i].evolution();
        //        dDLTransitoire.addElement(cls[i].evolution());
      }
    }
    // Stockage des courbes dans l'ordre des noeuds
    for (int i= 0; i < dlts.length; i++)
      for (int j= 0; j < nbMaxDDL; j++)
        if (dlts[i][j] != null)
          dDLTransitoire.add(dlts[i][j]);
    //--------------------------------------------------------------------------
    //---  Remplissage des valeurs de DDL pour chaque pas de temps  ------------
    //--------------------------------------------------------------------------
    nbPTs= 0;
    for (int i= 0; i < _paramINP.groupesPT.length; i++)
      nbPTs += _paramINP.groupesPT[i].nombrePasDeTemps;
    pTs= new SParametresRefluxLigneTransitoire[nbPTs];
    nbPTs= 0;
    valeurPT= 0;
    dtReel= _paramINP.t0;
    for (int i= 0; i < _paramINP.groupesPT.length; i++) {
      for (int j= 0; j < _paramINP.groupesPT[i].nombrePasDeTemps; j++) {
        pTs[nbPTs]= new SParametresRefluxLigneTransitoire();
        valeurPT += _paramINP.groupesPT[i].valeurPasDeTemps;
        pTs[nbPTs].valeurPasDeTemps= valeurPT;
        pTs[nbPTs].valeursDegresLibertes= new double[dDLTransitoire.size()];
        // Interpolation sur la courbe au temps t=valeurPT+dtReel
        nbDDLs= 0;
        for (Enumeration e= dDLTransitoire.elements(); e.hasMoreElements();) {
          courbe= (IEvolutionSerieIrreguliere)e.nextElement();
          pTs[nbPTs].valeursDegresLibertes[nbDDLs]=
            courbe.valeur((int) (valeurPT + dtReel));
          nbDDLs++;
        }
        nbPTs++;
      }
    }
    _params.pasDeTemps= pTs;
  }
  /**
   * Transfert du projet dans un format des parametres PN serveur.
   *
   * @param _params La structure contenant les informations en format serveur.
   * @param _parINP La structure pour le fichier .inp
   * @param _projet Le projet.
   */
  private void versParametresRefluxPN(
    SParametresRefluxPN _params,
    SParametresRefluxINP _paramINP,
    PRProjet _prj) {
    PRSollicitation[] sos= _prj.sollicitations();
    GrMaillageElement mail= _prj.maillage();
    Object[] supports;
    /*GrNoeud noeud;
    GrNoeud[] noeuds= _prj.maillage().noeuds();*/
    //    Hashtable          dDLTransitoire=new Hashtable();
    Vector dDLTransitoire= new Vector();
    int nbPTs;
    int nbDDLs;
    int nddl;
    SParametresRefluxLigneTransitoire[] pTs;
    double valeurPT;
    double dtReel; // Decalage de la valeur du pas de temps du fait
    // que Reflux ne sait pas prendre en compte un t0.
    IEvolutionSerieIrreguliere courbe;
    int nbMaxDDL= 2; // Flux, D�bit solide
    Hashtable hnd2Num= new Hashtable();
    {
      GrNoeud[] nds= mail.noeuds();
      for (int i= 0; i < nds.length; i++)
        hnd2Num.put(nds[i], new Integer(i));
    }
    IEvolution[][] dlts= new IEvolution[mail.noeuds().length][nbMaxDDL];
    for (int i= 0; i < dlts.length; i++)
      for (int j= 0; j < nbMaxDDL; j++)
        dlts[i][j]= null;
    //--------------------------------------------------------------------------
    //---  Stockage de la courbe pour chaque DDL transitoires  -----------------
    //--------------------------------------------------------------------------
    for (int i= 0; i < sos.length; i++) {
      if (sos[i].type() != PRSollicitation.FLUX
        && sos[i].type() != PRSollicitation.DEBIT_SOLIDE)
        continue;
      if (sos[i].evolution() instanceof IEvolutionConstante)
        continue;
      // Pour chaque support
      supports= sos[i].supports().toArray();
      for (int j= 0; j < supports.length; j++) {
        // Le support n'est pas un noeud => On le passe
        if (!(supports[j] instanceof GrNoeud))
          continue;
        int num= ((Integer)hnd2Num.get(supports[j])).intValue();
        nddl= 0;
        if (sos[i].type() == PRSollicitation.FLUX)
          nddl= 0;
        else if (sos[i].type() == PRSollicitation.DEBIT_SOLIDE)
          nddl= 1;
        //noeud= (GrNoeud)supports[j];
        dlts[num][nddl]= sos[i].evolution();
        //        dDLTransitoire.put(noeud,sos[i].evolution());
      }
    }
    // Stockage des courbes dans l'ordre des noeuds
    for (int i= 0; i < dlts.length; i++)
      for (int j= 0; j < nbMaxDDL; j++)
        if (dlts[i][j] != null)
          dDLTransitoire.add(dlts[i][j]);
    //--------------------------------------------------------------------------
    //---  Remplissage des valeurs de DDL pour chaque pas de temps  ------------
    //--------------------------------------------------------------------------
    nbPTs= 0;
    for (int i= 0; i < _paramINP.groupesPT.length; i++)
      nbPTs += _paramINP.groupesPT[i].nombrePasDeTemps;
    pTs= new SParametresRefluxLigneTransitoire[nbPTs];
    nbPTs= 0;
    valeurPT= 0;
    dtReel= _paramINP.t0;
    for (int i= 0; i < _paramINP.groupesPT.length; i++) {
      for (int j= 0; j < _paramINP.groupesPT[i].nombrePasDeTemps; j++) {
        pTs[nbPTs]= new SParametresRefluxLigneTransitoire();
        valeurPT += _paramINP.groupesPT[i].valeurPasDeTemps;
        pTs[nbPTs].valeurPasDeTemps= valeurPT;
        pTs[nbPTs].valeursDegresLibertes= new double[dDLTransitoire.size()];
        // Interpolation sur la courbe au temps t=valeurPT+dtReel
        nbDDLs= 0;
        for (Enumeration e= dDLTransitoire.elements(); e.hasMoreElements();) {
          //        for (int k=0; k<noeuds.length; k++) {
          //          courbe=(IEvolutionSerieIrreguliere)dDLTransitoire.get(noeuds[k]);
          courbe= (IEvolutionSerieIrreguliere)e.nextElement();
          //          if (courbe!=null) {
          pTs[nbPTs].valeursDegresLibertes[nbDDLs]=
            courbe.valeur((int) (valeurPT + dtReel));
          nbDDLs++;
          //          }
        }
        nbPTs++;
      }
    }
    _params.pasDeTemps= pTs;
  }
  /**
   * Transfert du projet dans un format des parametres PHY serveur.
   *
   * @param _params La structure contenant les informations en format serveur.
   * @param _parINP La structure pour le fichier .inp
   * @param _projet Le projet.
   */
  private void versParametresRefluxPHY(
    SParametresRefluxPHY _params,
    SParametresRefluxINP _paramINP,
    PRProjet _prj) {
    GrMaillageElement mail= _prj.maillage();
    PRModeleCalcul mdlCal= _prj.modeleCalcul();
    Vector gpesEls= _prj.modeleProprietes().naturesFonds();
    Object[] parCal= mdlCal.parametresCalcul();
    int casChamp= ((Integer)parCal[TT2dResource.CAS_CHAMP]).intValue();
    int casMaree= ((Integer)parCal[TT2dResource.CAS_MAREE]).intValue();
    String prjRac45= ((File)parCal[TT2dResource.PRJ_MAREE_45]).getPath();
    String prjRac90= ((File)parCal[TT2dResource.PRJ_MAREE_90]).getPath();
    int formule= ((Integer)parCal[TT2dResource.FORMULE]).intValue();
    File prjRacHoule= ((File)parCal[TT2dResource.PRJ_HOULE]);
    GrNoeud[] nds= mail.noeuds();
    GrPoint[] pts= new GrPoint[nds.length];
    for (int i= 0; i < pts.length; i++)
      pts[i]= nds[i].point_;
    // Noeud->Ind (indice sur la table des noeuds du maillage ou sur les
    // tables du serveur)
    Hashtable hnd2Ind= new Hashtable(nds.length);
    for (int i= 0; i < nds.length; i++)
      hnd2Ind.put(nds[i], new Integer(i));
    // Table associant un noeud � un �l�ment pour les propri�t�s �l�mentaires
    // donn�es aux noeuds dans le fichier .phy
    //Hashtable hnd2El=new Hashtable(nds.length);
    int nbValPHY= 7;
    SParametresRefluxEtapePHY[] etapes= null;
    try {
      //------------------------------------------------------------------------
      //--- Traitement projet courantologie mar�e/constant/crue ou mar�e 45 ----
      //------------------------------------------------------------------------
      {
        PRProjet prjCourant45;
        PRResultats resHydro45= null; // R�sultats aux noeuds hydro 45
        GrMaillageElement mailHydro45= null; // Maillage T3 hydro 45
        PRResultats res;
        //--- Lecture du projet courantologie 45 -------------------------------
        message(
          "Lecture du projet de courantologie 45 depuis " + prjRac45 + "...",
          10);
        if (prjRac45.endsWith(".pre"))
          prjCourant45= PRProjet.ouvrir(prjRac45);
        else
          prjCourant45= PRProjet.nouveau(PRProjet.VIT_NU_CON, prjRac45, null);
        message(null, 30);
        res= prjCourant45.resultats();
        message(null, 80);
        mailHydro45= prjCourant45.maillage().versLineaire(2);
        // Recup�ration et stockage des seuls qx, qy, ne.
        resHydro45= new PRResultats();
        for (int i= 0; i < res.getNbSteps(); i++) {
          PRResultats.Etape.Colonne vx= res.getStep(i).getColonne(3);
          PRResultats.Etape.Colonne vy= res.getStep(i).getColonne(4);
          PRResultats.Etape.Colonne ne= res.getStep(i).getColonne(6);
          PRResultats.Etape.Colonne ht= res.getStep(i).getColonne(7);
          PRResultats.Etape.Colonne qx;
          PRResultats.Etape.Colonne qy;
          qx= new PRResultats.Etape.Colonne(new double[vx.valeurs.length]);
          qy= new PRResultats.Etape.Colonne(new double[vy.valeurs.length]);
          for (int j= 0; j < qx.valeurs.length; j++)
            qx.valeurs[j]= vx.valeurs[j] * ht.valeurs[j];
          for (int j= 0; j < qy.valeurs.length; j++)
            qy.valeurs[j]= vy.valeurs[j] * ht.valeurs[j];
          resHydro45.addStep(
            new PRResultats.Etape(
              res.getStep(i).t,
              new PRResultats.Etape.Colonne[] { qx, qy, ne }));
        }
        message(null, 90);
        //--- Construction des �tapes bloc 45 ----------------------------------
        System.out.print("Construction des �tapes des structures Reflux...");
        etapes= new SParametresRefluxEtapePHY[resHydro45.getNbSteps()];
        for (int i= 0; i < etapes.length; i++) {
          SParametresRefluxLignePHY[] bloc45= null;
          //          SParametresRefluxLignePHY[] bloc90=null;
          double t= resHydro45.getStep(i).t;
          bloc45= new SParametresRefluxLignePHY[nds.length];
          for (int j= 0; j < bloc45.length; j++)
            bloc45[j]= new SParametresRefluxLignePHY(new double[nbValPHY]);
          //          if (casChamp==2 && casMaree==1) {
          //            bloc90=new SParametresRefluxLignePHY[nds.length];
          //            for (int j=0; j<bloc90.length; j++)
          //             bloc90[j]=new SParametresRefluxLignePHY(new double[nbValPHY]);
          //          }
          etapes[i]= new SParametresRefluxEtapePHY(t, bloc45, null);
        }
        System.out.println(" OK");
        message(null, 100);
        //--- Interpolation des valeurs hydro 45 sur les noeuds s�dimentologie -
        message("Interpolation des valeurs de courantologie 45...", 10);
        System.out.print("Interpolation des valeurs de courantologie 45...");
        GrNoeud[] nds45= mailHydro45.noeuds();
        FudaaInterpolateurMaillage it= new FudaaInterpolateurMaillage();
        it.maillage(mailHydro45);
        message(null, 30);
        // Pour tous les pas de temps, et pour les valeurs qx, qy, ne.
        for (int i= 0; i < resHydro45.getNbSteps(); i++) {
          message(null, 30 + 70 * (i + 1) / resHydro45.getNbSteps());
          for (int j= 0; j < resHydro45.getStep(i).getNbColonnes(); j++) {
            GrPoint[] points;
            PRResultats.Etape.Colonne col= resHydro45.getStep(i).getColonne(j);
            for (int k= 0; k < col.valeurs.length; k++)
              nds45[k].point_.z_= col.valeurs[k];
            points= it.interpolePoints(pts);
            for (int k= 0; k < etapes[i].bloc45.length; k++) {
              etapes[i].bloc45[k].valeurs[j]= points[k].z_;
            }
          }
        }
        System.out.println(" OK");
        message(null, 100);
      }
      //------------------------------------------------------------------------
      //--- Traitement projet courantologie mar�e 90 ---------------------------
      //------------------------------------------------------------------------
      if (casChamp == 2 && casMaree == 1) {
        PRProjet prjCourant90;
        PRResultats resHydro90= null; // R�sultats aux noeuds hydro 90
        GrMaillageElement mailHydro90= null; // Maillage T3 hydro 90
        PRResultats res;
        //--- Lecture du projet courant mar�e 90 -------------------------------
        message(
          "Lecture du projet de courantologie 90 depuis " + prjRac90 + "...",
          10);
        System.out.println("Racine projet mar�e 90: " + prjRac90);
        System.out.print("Chargement du projet courantologie 90...");
        if (prjRac90.endsWith(".pre"))
          prjCourant90= PRProjet.ouvrir(prjRac90);
        else
          prjCourant90= PRProjet.nouveau(PRProjet.VIT_NU_CON, prjRac90, null);
        message(null, 30);
        System.out.println(" OK");
        System.out.println("Chargement des r�sultats courantologie 90...");
        res= prjCourant90.resultats();
        message(null, 80);
        System.out.println("R�sultats charg�s.");
        // Controle que le nb de pas est bien le m�me que pour 45 et que les
        // instants sont les m�mes.
        if (res.getNbSteps() != etapes.length)
          throw new IllegalArgumentException(
            "Le nombre de pas de temps sur les deux projets mar�e n'est pas "
              + "identique");
        for (int i= 0; i < res.getNbSteps(); i++)
          if (res.getStep(i).t != etapes[i].instant)
            throw new IllegalArgumentException(
              "Les valeurs de pas de temps sur les deux projets mar�e ne sont "
                + "pas identiques");
        mailHydro90= prjCourant90.maillage().versLineaire(2);
        // Recup�ration et stockage des seuls qx, qy, ne.
        resHydro90= new PRResultats();
        for (int i= 0; i < res.getNbSteps(); i++) {
          PRResultats.Etape.Colonne vx= res.getStep(i).getColonne(3);
          PRResultats.Etape.Colonne vy= res.getStep(i).getColonne(4);
          PRResultats.Etape.Colonne ne= res.getStep(i).getColonne(6);
          PRResultats.Etape.Colonne ht= res.getStep(i).getColonne(7);
          PRResultats.Etape.Colonne qx;
          PRResultats.Etape.Colonne qy;
          qx= new PRResultats.Etape.Colonne(new double[vx.valeurs.length]);
          qy= new PRResultats.Etape.Colonne(new double[vy.valeurs.length]);
          for (int j= 0; j < qx.valeurs.length; j++)
            qx.valeurs[j]= vx.valeurs[j] * ht.valeurs[j];
          for (int j= 0; j < qy.valeurs.length; j++)
            qy.valeurs[j]= vy.valeurs[j] * ht.valeurs[j];
          resHydro90.addStep(
            new PRResultats.Etape(
              res.getStep(i).t,
              new PRResultats.Etape.Colonne[] { qx, qy, ne }));
        }
        message(null, 90);
        //--- Construction des �tapes bloc 90 ----------------------------------
        System.out.print("Construction des �tapes des structures Reflux...");
        for (int i= 0; i < etapes.length; i++) {
          SParametresRefluxLignePHY[] bloc90= null;
          bloc90= new SParametresRefluxLignePHY[nds.length];
          for (int j= 0; j < bloc90.length; j++)
            bloc90[j]= new SParametresRefluxLignePHY(new double[nbValPHY]);
          etapes[i].bloc90= bloc90;
        }
        System.out.println(" OK");
        message(null, 100);
        //--- Interpolation des valeurs hydro 90 sur les noeuds s�dimentologie -
        message("Interpolation des valeurs de courantologie 90...", 10);
        System.out.print("Interpolation des valeurs de courantologie 90...");
        GrNoeud[] nds90= mailHydro90.noeuds();
        FudaaInterpolateurMaillage it= new FudaaInterpolateurMaillage();
        it.maillage(mailHydro90);
        message(null, 30);
        // Pour tous les pas de temps, et pour les valeurs qx, qy, ne.
        for (int i= 0; i < resHydro90.getNbSteps(); i++) {
          message(null, 30 + 70 * (i + 1) / resHydro90.getNbSteps());
          for (int j= 0; j < resHydro90.getStep(i).getNbColonnes(); j++) {
            GrPoint[] points;
            PRResultats.Etape.Colonne col= resHydro90.getStep(i).getColonne(j);
            for (int k= 0; k < col.valeurs.length; k++)
              nds90[k].point_.z_= col.valeurs[k];
            points= it.interpolePoints(pts);
            for (int k= 0; k < etapes[i].bloc90.length; k++) {
              etapes[i].bloc90[k].valeurs[j]= points[k].z_;
            }
          }
        }
        System.out.println(" OK");
        message(null, 100);
      }
      _params.etapes= etapes;
    } catch (IOException _exc) {
      throw new IllegalArgumentException(_exc.getLocalizedMessage());
    } finally {
      // R�initialisation avec le type de projet courant.
      RefluxResource.typeProjet= _prj.type();
    }
    //--------------------------------------------------------------------------
    //--- Traitement pour la formulation de bijker -----------------------------
    //--------------------------------------------------------------------------
    if (formule == 3) {
      RefondeProjet prjHoule= null;
      GrMaillageElement mailHoule= null;
      RefondeResultats resHoule= null;
      GrPoint[] points;
      double periode= 0;
      message("Lecture du projet de houle depuis " + prjRacHoule + "...", 10);
      System.out.println("Lecture du projet de houle...");
      try {
        prjHoule= new RefondeProjet();
        prjHoule.addMessageListener(new MessageListener() {
          public void messageEnvoye(MessageEvent _evt) {
            System.out.println(
              _evt.getMessage() + " %" + _evt.getProgression());
          }
        });
        try {
          prjHoule.ouvrir(prjRacHoule);
        } catch (RefondeTacheInterruptionException _exc) {} // Interruption depuis l'interface, non possible => Pas de traitement
        message(null, 40);
        if (!prjHoule.hasResultats())
          throw new IllegalArgumentException("Pas de r�sultats pour le projet de houle");
        mailHoule= RefondeMaillage.creeSuperMaillage(prjHoule);
        message(null, 50);
        resHoule= prjHoule.getResultats();
        message(null, 80);
        periode= prjHoule.getModeleCalcul().periodeHoule();
      } catch (IOException _exc) {
        throw new IllegalArgumentException(_exc.getMessage());
      }
      // Initialisation des valeurs de p�riode de houle.
      System.out.println(
        "Initialisation pour une p�riode de houle de: " + periode);
      for (int i= 0; i < etapes.length; i++) {
        for (int j= 0; j < etapes[i].bloc45.length; j++)
          etapes[i].bloc45[j].valeurs[4]= periode;
        if (etapes[i].bloc90 != null)
          for (int j= 0; j < etapes[i].bloc90.length; j++)
            etapes[i].bloc90[j].valeurs[4]= periode;
      }
      message(null, 100);
      // Interpolation des valeurs de hauteur de houle.
      message("Interpolation des valeurs de hauteur de houle...", 10);
      System.out.print("Interpolation des valeurs de hauteur de houle...");
      GrNoeud[] ndsHoule= mailHoule.noeuds();
      message(null, 20);
      FudaaInterpolateurMaillage it= new FudaaInterpolateurMaillage();
      it.maillage(mailHoule);
      message(null, 50);
      // Initialisation de la cote Z de l'interpolateur avec les valeurs de
      // houle en chaque noeud
      double[] htHoule=
        resHoule.getResultat(resHoule.nomResultats[resHoule.HAUTEUR_HOULE]);
      for (int i= 0; i < ndsHoule.length; i++) {
        RefondeNoeudData data= (RefondeNoeudData)ndsHoule[i].data();
        //        ndsHoule[i].point.z=resHoule.resultats_.lignes[data.numero-1].moduleHauteur;
        ndsHoule[i].point_.z_= htHoule[data.numero - 1];
      }
      // Interpolation des points du maillage s�dimentologie.
      points= it.interpolePoints(pts);
      message(null, 60);
      // Pour tous les noeuds du maillage s�dimentologie.
      for (int i= 0; i < etapes.length; i++) {
        for (int j= 0; j < etapes[i].bloc45.length; j++)
          etapes[i].bloc45[j].valeurs[3]= points[j].z_;
        if (etapes[i].bloc90 != null)
          for (int j= 0; j < etapes[i].bloc90.length; j++)
            etapes[i].bloc90[j].valeurs[3]= points[j].z_;
      }
      System.out.println(" OK");
      message(null, 100);
    }
    //--- Propri�t�s �l�mentaires transf�r�es aux noeuds -----------------------
    System.out.print("Propri�t�s �l�mentaires transf�r�es aux noeuds...");
    // Fond erodable ou non
    for (int i= 0; i < gpesEls.size(); i++) {
      PRNature gpe= (PRNature)gpesEls.get(i);
      if (gpe.type() == PRNature.FOND) {
        Vector vels= gpe.supports();
        for (int j= 0; j < vels.size(); j++) {
          GrElement el= (GrElement)vels.get(j);
          for (int k= 0; k < el.noeuds_.length; k++) {
            int ind= ((Integer)hnd2Ind.get(el.noeuds_[k])).intValue();
            for (int l= 0; l < etapes.length; l++) {
              etapes[l].bloc45[ind].valeurs[6]= 1;
              if (etapes[l].bloc90 != null)
                etapes[l].bloc90[ind].valeurs[6]= 1;
            }
          }
        }
      }
    }
    // Rugosit�
    for (int i= 0; i < _paramINP.numeroGroupePE.length; i++) {
      if (_paramINP.numeroGroupePE[i] == 0)
        continue;
      double rugo=
        _paramINP.groupesPE[_paramINP.numeroGroupePE[i] - 1].valeur[2];
      for (int j= 0; j < _paramINP.connectivites[i].numeroNoeud.length; j++) {
        int ind= _paramINP.connectivites[i].numeroNoeud[j];
        for (int l= 0; l < etapes.length; l++) {
          etapes[l].bloc45[ind].valeurs[5]= rugo;
          if (etapes[l].bloc90 != null)
            etapes[l].bloc90[ind].valeurs[5]= rugo;
        }
      }
    }
    //    // Remise � 0 des rugosit�s sur les �lements
    //
    //    for (int i=0; i<_paramINP.groupesPE.length; i++) {
    //      _paramINP.groupesPE[i].valeur[2]=0;
    //    }
    System.out.println(" OK");
    //--- Initialisation des valeurs du bloc TRAN non encore initialis�es ------
    // Dur�e du calcul hydro
    _paramINP.blocTRAN.dureeCalculHydro=
      etapes[etapes.length - 1].instant - etapes[0].instant;
    // Nombre de valeurs du fichier .phy�
    _paramINP.blocTRAN.nbValPHY= nbValPHY;
  }
  /*
   * Retourne le nouveau num�ro de groupe de propri�t� ou un ancien si
   * existant
   */
  private static int getNumGpe(Vector _vgpes, double[] _gpe) {
    NEXT_GPE : for (int k= 0; k < _vgpes.size(); k++) {
      double[] gpec= (double[])_vgpes.get(k);
      if (gpec.length != _gpe.length)
        continue;
      for (int l= 0; l < gpec.length; l++)
        if (gpec[l] != _gpe[l])
          continue NEXT_GPE;
      return k + 1;
    }
    _vgpes.add(_gpe);
    return _vgpes.size();
  }
  /**
   * Permet un retour d'information vers la barre des t�ches si le Thread
   * courant est de type RefluxTacheOperation.
   */
  private void message(String _mes, int _prog) {
    if (Thread.currentThread() instanceof RefluxTacheOperation) {
      RefluxTacheOperation thread= (RefluxTacheOperation)Thread.currentThread();
      if (_mes != null)
        thread.setOperation(_mes);
      if (_prog != -1)
        thread.setProgres(_prog);
    }
  }
}
