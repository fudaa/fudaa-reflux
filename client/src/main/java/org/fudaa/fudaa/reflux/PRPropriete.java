/*
 * @file         PRPropriete.java
 * @creation     1999-06-28
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;
import java.util.Arrays;
import java.util.Vector;

import org.fudaa.dodico.corba.mesure.IEvolution;
/**
 * Une classe propri�t�. Cette propri�t� peut �tre �lementaire, nodale, globale,
 * etc.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class PRPropriete {
  public static final int RUGOSITE= 0;
  public static final int VISCOSITE= 1;
  public static final int FROTTEMENT_PAROI= 2;
  public static final int ALPHA_LONGUEUR_MELANGE= 3;
  public static final int PERTE_CHARGE= 4;
  public static final int BASE= 5;
  public static final int NB_FCT= 6;
  public static final int D50_GRAIN= 7;
  public static final int D90_GRAIN= 8;
  // Type de la propriete
  private int type_;
  // Evolution associee
  private IEvolution evolution_;
  // Supports geometriques associes
  private Vector supports_;
  /**
   * Construit une propriete sans attributs. Les attributs doivent dans ce cas
   * etre OBLIGATOIREMENT specifies par les methodes appropriees avant toute
   * manipulation de la propriete.
   */
  public PRPropriete() {
    type(-1);
    evolution(null);
    supports(new Vector());
  }
  /**
   * Construit une propriete.
   * @param type le type de la propriete
   * @param evolution l'evolution associee
   * @param supports les supports geometriques associes
   */
  public PRPropriete(int type, IEvolution evolution, Vector supports) {
    type(type);
    evolution(evolution);
    supports(supports);
  }
  /**
   * Construit une propriete.
   * @param type le type de la propriete
   * @param evolution l'evolution associee
   * @param supports les supports geometriques associes
   */
  public PRPropriete(int type, IEvolution evolution, Object[] supports) {
    type(type);
    evolution(evolution);
    supports(supports);
  }
  /**
   * Retourne le type de cette propriete.
   * @return le type
   */
  public int type() {
    return type_;
  }
  /**
   * Modifie le type de cette propriete.
   * @param _type le type de la propriete
   */
  public void type(int _type) {
    type_= _type;
  }
  /**
   * Retourne l'evolution associee a cette propriete.
   * @return l'evolution associee (= null si pas d'evolution)
   */
  public IEvolution evolution() {
    return evolution_;
  }
  /**
   * Modifie l'evolution associee a cette propriete.
   * @param _evolution l'evolution associee
   */
  public void evolution(IEvolution _evolution) {
    evolution_= _evolution;
  }
  /**
   * Retourne les supports geometriques associes a cette propriete.
   * @return les supports geometriques (= null si pas de supports)
   */
  public Vector supports() {
    return supports_;
  }
  /**
   * Remplace les supports geometriques associes a cette propriete.
   * @param _supports les supports associes
   */
  public void supports(Vector _supports) {
    supports_= _supports;
  }
  /**
   * Remplace les supports geometriques associes a cette propriete.
   * @param _supports les supports associes
   */
  public void supports(Object[] _supports) {
    supports_= new Vector(Arrays.asList(_supports));
  }
}
