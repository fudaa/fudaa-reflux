/*
 * @file         TT2dProjet.java
 * @creation     2001-01-08
 * @modification $Date: 2007-01-19 13:14:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux.d2transport;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.Hashtable;
import java.util.Vector;

import org.fudaa.ctulu.CtuluLibFile;

import org.fudaa.dodico.corba.mesure.IEvolution;
import org.fudaa.dodico.corba.mesure.IEvolutionConstante;
import org.fudaa.dodico.corba.mesure.IEvolutionSerieIrreguliere;
import org.fudaa.dodico.corba.planification.ISegmentationTemps;
import org.fudaa.dodico.corba.planification.ISegmentationTempsHelper;

import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.objet.UsineLib;

import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;

import org.fudaa.fudaa.commun.conversion.FudaaMaillageCORELEBTH;
import org.fudaa.fudaa.reflux.*;
/**
 * Un projet Reflux 2D transport.
 *
 * @version      $Revision: 1.11 $ $Date: 2007-01-19 13:14:37 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class TT2dProjet extends PRProjet {
  /**
   * Interface de calcul.
   */
  private static TT2dInterfaceCalcul interface_= null;
  /**
   * Modification du maillage (implique la sauvegarde lors de la sauvegarde du
   * projet).
   */
  private boolean modMail= false;
  /**
   * Initialisation du projet depuis les fichiers .bth/.cor/.ele./[.crb] de
   * nom sp�cifi�.
   *
   * @param _racine Le nom de la racine des fichiers sans extension.
   * @param _params Param�tres de cr�ation du projet.
   * <pre>
   *   0 : String   Racine du projet courantologie ou du maillage T3 existant
   *   1 : Integer  0 : Cr�ation du maillage transport depuis le maillage
   *                courantologie, 1 : Existant
   *   2 : Integer  Mode pour la cr�ation 0 : 1T6->1T3, 1 : 1T6->4T3
   * </pre>
   *
   * @exception FileNotFoundException Un fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   */
  public void initialiser(String _racine, Object[] _params)
    throws IOException {
    RefluxMaillage maillage;
    Object[] parCal= mdlCal_.parametresCalcul();
    String prjLecRac= (String)_params[0];
    boolean prjTransExist= ((Integer)_params[1]).intValue() == 1;
    int mode= ((Integer)_params[2]).intValue();
    // Racine des fichiers
    racineFichiers(_racine);
    // Maillage T3 existant.
    if (prjTransExist) {
      maillage= lireMaillage(prjLecRac);
    }
    // Cr�ation du projet transport par transformation du maillage courantologie
    // en maillage T3.
    else {
      GrMaillageElement mail;
      {
        PRProjet prjCourant;
        prjCourant= PRProjet.nouveau(PRProjet.VIT_NU_CON, prjLecRac, null);
        //      prjCourant.resultats();
        System.out.println("OK projet courantologie");
        mail= prjCourant.maillage().versLineaire(mode + 1);
        // R�initialisation du type de projet courant suite � la lecture du
        // projet courantologie.
        RefluxResource.typeProjet= type();
      }
      // Optimisation du maillage
      int[] numOpts= PROLB.optimiser(mail);
      GrElement[] els= mail.elements();
      GrNoeud[] nds= mail.noeuds();
      GrNoeud[] ndsOpt= new GrNoeud[nds.length];
      for (int i= 0; i < ndsOpt.length; i++)
        ndsOpt[numOpts[i]]= nds[i];
      maillage= new RefluxMaillage(els, ndsOpt);
      modMail= true;
    }
    maillage(maillage);
    // Tentative de lecture du fichier des courbes s'il y en a un
    try {
      //      _operation.operation="Lecture des courbes";
      lireFichierCRB(prjLecRac + ".crb");
    } catch (FileNotFoundException _exc) {}
    // Points d�finissant le plan des solutions initiales
    //    getMainPanel().setProgression(50);
    {
      GrBoite boite= new GrBoite();
      GrNoeud[] noeuds= maillage().noeuds();
      GrPoint[] ptsSI= new GrPoint[3];
      GrPoint pmin;
      GrPoint pmax;
      for (int i= 0; i < noeuds.length; i++)
        boite.ajuste(noeuds[i].point_);
      pmin= boite.o_;
      pmax= boite.e_;
      ptsSI[0]= new GrPoint(pmin.x_, pmin.y_, pmax.z_ + 0.2);
      ptsSI[1]= new GrPoint(pmax.x_, pmin.y_, pmax.z_ + 0.2);
      ptsSI[2]= new GrPoint(pmax.x_, pmax.y_, pmax.z_ + 0.2);
      modeleCalcul().pointsSI(ptsSI);
    }
    // Nom des projets de courantologie pour les valeurs de champ de vitesse
    parCal[TT2dResource.PRJ_MAREE_45]= new File(prjLecRac + ".pre");
    parCal[TT2dResource.PRJ_MAREE_90]= new File(prjLecRac + ".pre");
    // Solutions initiales
    initialiserSolutionsInitiales();
    // Normales en chaque noeuds des contours
    initialiserNormales();
    //Nature des bords
    initialiserNatureBords();
    //Nature des fonds
    initialiserNatureFonds();
  }
  /**
   * Lecture d'un maillage depuis les fichiers noeuds, bathy et �l�ments de
   * maillage.
   * @param _fichier Nom du fichier maillage. Le maillage est contenu
   *                 dans les _fichier.cor, _fichier.bth et _fichier.ele
   * @exception FileNotFoundException Un fichier de maillage n'est pas trouv�
   * @exception IOException Une erreur de lecture s'est produite
   * @return L'objet maillage
   */
  public static RefluxMaillage lireMaillage(String _fichier)
    throws IOException {
    RefluxMaillage r= null;
    try {
      GrMaillageElement mail= FudaaMaillageCORELEBTH.lire(new File(_fichier));
      r= new RefluxMaillage(mail.elements(), mail.noeuds());
    } catch (FileNotFoundException _exc) {
      throw new FileNotFoundException(
        "Erreur d'ouverture de " + _exc.getMessage());
    } catch (IOException _exc) {
      throw new IOException("Erreur de lecture sur " + _exc.getMessage());
    }
    // Controle des �lements lus. Ils doivent tous etre T3.
    GrElement[] els= r.elements();
    for (int i= 0; i < els.length; i++)
      if (els[i].type_ != GrElement.T3)
        throw new IOException("Seuls les �l�ments T3 sont autoris�s pour ce type de projet");
    return r;
  }
  /**
   * Ecriture du mailalge sur fichiers .cor/.ele/.bth
   *
   * @param _racine Racine des noms des fichiers .cor/.ele/.bth
   */
  public void ecrireMaillage(String _racine) throws IOException {
    FudaaMaillageCORELEBTH.enregistrer(maillage(), new File(_racine));
  }
  /**
   * Lecture des informations contenues dans le fichier .crb des courbes transitoires
   * de nom specifie et stockage dans les objets IEvolutionSerieIrreguliere
   *
   * @param _nomFichier Nom du fichier a lire
   * @exception FileNotFoundException Le fichier est introuvable
   * @exception IOException Une erreur de lecture s'est produite
   */
  private void lireFichierCRB(String _nomFichier)
    throws FileNotFoundException, IOException {
    int[] fmt;
    String nomCourbe; // Nom de courbe
    int nbPoints; // Nombre de points de courbe
    long[] x; // Abscisses de la courbe
    double[] y; // Ordonn�es de la courbe
    IEvolutionSerieIrreguliere evolution; // Evolution en cours de cr�ation
    IEvolutionSerieIrreguliere[] evolutions; // Evolutions lues
    Vector evolTmp; // Vecteur temporaire des �volutions
    FortranReader file= null;
    try {
      // Ouverture du fichier
      file= new FortranReader(new FileReader(_nomFichier));
      // Num�ro de version (seules les versions > 4.0 sont autoris�es)
      fmt= new int[] { 12, 10 };
      file.readFields(fmt);
      if (file.stringField(1).compareTo("4.0") < 0) {
        throw new Exception(
          "Erreur de lecture sur "
            + _nomFichier
            + "\nLe format du fichier est de version"
            + file.stringField(1)
            + ".\nSeules les versions > � 4.0 sont autoris�es");
      }
      // Boucle jusqu'� fin de fichier
      evolTmp= new Vector();
      while (file.ready()) {
        fmt= new int[] { 20, 1, 5 };
        try {
          file.readFields(fmt);
        } catch (EOFException exc) {
          break;
        }
        if (file.stringField(0).substring(0, 1).equals("*"))
          continue;
        // Nom de la courbe
        nomCourbe= file.stringField(0);
        // Nombre de points de la courbe
        nbPoints= file.intField(2);
        x= new long[nbPoints];
        y= new double[nbPoints];
        // Boucle sur les points
        fmt= new int[] { 10, 1, 10 };
        for (int i= 0; i < nbPoints;) {
          file.readFields(fmt);
          if (file.stringField(0).substring(0, 1).equals("*"))
            continue;
          x[i]= (long)file.doubleField(0);
          y[i]= file.doubleField(2);
          i++;
        }
        // Cr�ation de l'�volution s�rie irr�guli�re
        evolution=UsineLib.findUsine().creeMesureEvolutionSerieIrreguliere();
  /*        IEvolutionSerieIrreguliereHelper.narrow(
            new DEvolutionSerieIrreguliere().tie());*/
        ISegmentationTemps st= UsineLib.findUsine().creePlanificationSegmentationTemps();
//        ISegmentationTemps st=
//          ISegmentationTempsHelper.narrow(new DSegmentationTemps().tie());
        st.modifieInstants(x);
        evolution.instants(ISegmentationTempsHelper.narrow(st));
        evolution.serie(y);
        associe(evolution, nomCourbe);
        evolTmp.addElement(evolution);
      }
    } catch (FileNotFoundException exc) {
      throw new FileNotFoundException(_nomFichier);
    } catch (IOException exc) {
      throw new IOException("Erreur de lecture sur " + _nomFichier);
    } catch (NumberFormatException exc) {
      throw new IOException("Erreur de lecture sur " + _nomFichier);
    } catch (Exception _exc) {
      throw new IOException(_exc.getMessage());
    } finally {
      // Fermeture du fichier
      if (file != null)
        file.close();
    }
    evolutions= new IEvolutionSerieIrreguliere[evolTmp.size()];
    evolTmp.copyInto(evolutions);
    evolutions(evolutions);
  }
  /**
   * Chargement du projet depuis les fichiers .pre/.bth/.cor/.ele./[.crb]
   * @param _racine Le nom de la racine des fichiers sans extension.
   */
  public void charger(String _racine) throws IOException {
    Reader rf= null;
    FortranReader file= null;
    int ival;
    String nmFcPre= _racine + ".pre";
    RefluxMaillage maillage;
    GrElement[] els;
    GrNoeud[] nds;
    GrNoeud[][] ndsBds;
    GrElement[][] elsBds;
    // Racine des fichiers
    racineFichiers(_racine);
    // Lecture du maillage
    maillage= lireMaillage(_racine);
    maillage(maillage);
    // Lecture du fichier .crb (s'il existe)
    try {
      lireFichierCRB(_racine + ".crb");
    }
    // FileNotFoundException => On controlera + tard la coh�rence .pre/.crb
    catch (FileNotFoundException _exc) {}
    els= maillage().elements();
    nds= maillage().noeuds();
    elsBds= maillage().aretesContours();
    ndsBds= maillage().noeudsContours();
    // Correspondances
    // Table Noeud->Arete
    Hashtable hnd2Bd= new Hashtable();
    for (int i= 0; i < elsBds.length; i++)
      for (int j= 0; j < elsBds[i].length; j++)
        hnd2Bd.put(elsBds[i][j].noeuds_[0], elsBds[i][j]);
    //--------------------------------------------------------------------------
    //--- Lecture des donn�es  -------------------------------------------------
    //--------------------------------------------------------------------------
    try {
      // Ouverture du fichier
      file= new FortranReader(rf= new FileReader(nmFcPre));
      file.setBlankZero(true);
      // Num�ro de version (pas utilis� pour l'instant)
      file.readFields();
      // Type de probleme => Doit �tre=4 ou 5
      file.readFields();
      ival= file.intField(0);
      if (ival != type() + 1)
        throw new Exception(
          "Erreur de lecture sur "
            + nmFcPre
            + "\nLe probl�me n'est pas de type transport 2D.");
      //------------------------------------------------------------------------
      // Solutions initiales
      //------------------------------------------------------------------------
      GrPoint[] ptsSI= new GrPoint[3];
      PRSolutionsInitiales[] sis= new PRSolutionsInitiales[nds.length];
      // Les points sont initialis�s, m�me si cette  n'a pas de
      // sens pour la s�dimentologie.
      {
        GrBoite boite= new GrBoite();
        GrPoint pmin;
        GrPoint pmax;
        for (int i= 0; i < nds.length; i++)
          boite.ajuste(nds[i].point_);
        pmin= boite.o_;
        pmax= boite.e_;
        ptsSI[0]= new GrPoint(pmin.x_, pmin.y_, pmax.z_ + 0.2);
        ptsSI[1]= new GrPoint(pmax.x_, pmin.y_, pmax.z_ + 0.2);
        ptsSI[2]= new GrPoint(pmax.x_, pmax.y_, pmax.z_ + 0.2);
        modeleCalcul().pointsSI(ptsSI);
      }
      //      // Commentaire
      //      file.readFields();
      //
      //      // Points de d�finition de la surface.
      //      for (int i=0; i<3; i++) {
      //        ptsSI[i]=new GrPoint();
      //
      //        file.readFields();
      //        ptsSI[i].x=file.doubleField(0);
      //        ptsSI[i].y=file.doubleField(1);
      //        ptsSI[i].z=file.doubleField(2);
      //      }
      //      modeleCalcul().pointsSI(ptsSI);
      // Commentaire
      file.readFields();
      // Solutions en chaque noeud
      for (int i= 0; i < sis.length; i++) {
        file.readFields();
        int nbFields= file.getNumberOfFields();
        double[] vals= new double[nbFields - 1];
        ival= file.intField(0);
        for (int j= 0; j < vals.length; j++)
          vals[j]= file.doubleField(j + 1);
        sis[i]= new PRSolutionsInitiales(nds[ival], vals);
      }
      modeleCalcul().solutionsInitiales(sis);
      //------------------------------------------------------------------------
      // Mod�le de propri�t�s
      //------------------------------------------------------------------------
      // Propri�t�s de bord.
      int[] tpsBds;
      PRPropriete[][] prpsBds;
      Vector vnatBds= new Vector();
      Vector vprpsBds= new Vector();
      // Commentaire
      file.readFields();
      // Nombre de groupes de propri�t�s d'ar�tes
      file.readFields();
      ival= file.intField(0);
      // Groupes de propri�t�s d'ar�tes => Type des bords et propri�t�s
      // d'ar�tes
      tpsBds= new int[ival];
      prpsBds= new PRPropriete[ival][];
      for (int i= 0; i < tpsBds.length; i++) {
        file.readFields();
        tpsBds[i]= file.intField(1);
        int[] tpsPrps= Definitions.getTypesProprietesElementaires(tpsBds[i]);
        prpsBds[i]= new PRPropriete[tpsPrps.length];
        for (int j= 0; j < tpsPrps.length; j++) {
          int code= file.intField(j * 2 + 2);
          double val= file.doubleField(j * 2 + 3);
          IEvolution crb;
          if (code == 0)
            continue;
          else if (code == 1) {
            crb=
//              IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());
              UsineLib.findUsine().creeMesureEvolutionConstante();
            ((IEvolutionConstante)crb).constante(val);
          } else {
            crb= evolutions()[(int)val];
          }
          prpsBds[i][j]= new PRPropriete(tpsPrps[j], crb, new Vector());
          vprpsBds.add(prpsBds[i][j]);
        }
      }
      // Commentaire
      file.readFields();
      // Num�ro de groupe de propri�t�s d'ar�tes par noeud de bord.
      // => Cr�ation des natures de bord et affectation des propri�t�s d'aretes.
      for (int i= 0; i < elsBds.length; i++) {
        for (int j= 0; j < elsBds[i].length; j++) {
          int iNd;
          int iGrp;
          GrElement elBd;
          //          PRNature nat;
          file.readFields();
          iNd= file.intField(0);
          iGrp= file.intField(1);
          elBd= (GrElement)hnd2Bd.get(nds[iNd]);
          vnatBds.add(new PRNature(tpsBds[iGrp], new Object[] { elBd }));
          for (int k= 0; k < prpsBds[iGrp].length; k++)
            if (prpsBds[iGrp][k] != null)
              prpsBds[iGrp][k].supports().add(elBd);
        }
      }
      mdlPrp_.naturesBords(vnatBds);
      mdlPrp_.proprietesAretes(
        (PRPropriete[])vprpsBds.toArray(new PRPropriete[0]));
      // Propri�t�s de fond.
      int[] tpsFds;
      PRPropriete[][] prpsFds;
      Vector vnatFds= new Vector();
      Vector vprpsFds= new Vector();
      // Commentaire
      file.readFields();
      // Nombre de groupes de propri�t�s de fond
      file.readFields();
      ival= file.intField(0);
      // Groupes de propri�t�s de fond => Type des fonds et propri�t�s
      // d'ar�tes
      tpsFds= new int[ival];
      prpsFds= new PRPropriete[ival][];
      for (int i= 0; i < tpsFds.length; i++) {
        file.readFields();
        tpsFds[i]= file.intField(1);
        int[] tpsPrps= Definitions.getTypesProprietesElementaires(tpsFds[i]);
        prpsFds[i]= new PRPropriete[tpsPrps.length];
        for (int j= 0; j < tpsPrps.length; j++) {
          int code= file.intField(j * 2 + 2);
          double val= file.doubleField(j * 2 + 3);
          IEvolution crb;
          if (code == 0)
            continue;
          else if (code == 1) {
            crb=
//              IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());
              UsineLib.findUsine().creeMesureEvolutionConstante();
              ((IEvolutionConstante)crb).constante(val);
          } else {
            crb= evolutions()[(int)val];
          }
          prpsFds[i][j]= new PRPropriete(tpsPrps[j], crb, new Vector());
          vprpsFds.add(prpsFds[i][j]);
        }
      }
      // Commentaire
      file.readFields();
      // Num�ro de groupe de propri�t�s de fond par �lement.
      // => Cr�ation des natures de fond et affectation des propri�t�s de fond.
      for (int i= 0; i < els.length; i++) {
        int iEl;
        int iGrp;
        file.readFields();
        iEl= file.intField(0);
        iGrp= file.intField(1);
        vnatFds.add(new PRNature(tpsFds[iGrp], new Object[] { els[iEl] }));
        for (int k= 0; k < prpsFds[iGrp].length; k++)
          if (prpsFds[iGrp][k] != null)
            prpsFds[iGrp][k].supports().add(els[iEl]);
      }
      mdlPrp_.naturesFonds(vnatFds);
      mdlPrp_.proprietesElements(
        (PRPropriete[])vprpsFds.toArray(new PRPropriete[0]));
      // Propri�t�s globales
      PRPropriete[] prpsGlobs= mdlPrp_.proprietesGlobales();
      // Commentaire
      file.readFields();
      file.readFields();
      for (int i= 0; i < prpsGlobs.length; i++) {
        double val= file.doubleField(i);
        ((IEvolutionConstante)prpsGlobs[i].evolution()).constante(val);
      }
      //------------------------------------------------------------------------
      // Conditions aux limites
      //------------------------------------------------------------------------
      PRConditionLimite[] cls;
      // Commentaire
      file.readFields();
      // Nombre de conditions aux limites
      file.readFields();
      ival= file.intField(0);
      cls= new PRConditionLimite[ival];
      // Conditions aux limites
      for (int i= 0; i < cls.length; i++) {
        int type;
        int code;
        double val;
        int nbNds;
        Vector vnds= new Vector();
        IEvolution crb;
        file.readFields();
        type= file.intField(0);
        code= file.intField(1);
        val= file.doubleField(2);
        nbNds= file.intField(3);
        if (code == 0)
          continue;
        else if (code == 1) {
          crb=
//            IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());
            UsineLib.findUsine().creeMesureEvolutionConstante();
            ((IEvolutionConstante)crb).constante(val);
        } else {
          crb= evolutions()[(int)val];
        }
        cls[i]= new PRConditionLimite(type, crb, vnds);
        for (int j= 0; j < nbNds; j++) {
          int iNd;
          file.readFields();
          iNd= file.intField(0);
          vnds.add(nds[iNd]);
        }
      }
      conditionsLimites(cls);
      //------------------------------------------------------------------------
      // Sollicitations
      //------------------------------------------------------------------------
      PRSollicitation[] sos;
      // Commentaire
      file.readFields();
      // Nombre de sollicitations
      file.readFields();
      ival= file.intField(0);
      sos= new PRSollicitation[ival];
      // Sollicitations
      for (int i= 0; i < sos.length; i++) {
        int type;
        int code;
        double val;
        int nbNds;
        Vector vnds= new Vector();
        IEvolution crb;
        file.readFields();
        type= file.intField(0);
        code= file.intField(1);
        val= file.doubleField(2);
        nbNds= file.intField(3);
        if (code == 0)
          continue;
        else if (code == 1) {
          crb=
//            IEvolutionConstanteHelper.narrow(new DEvolutionConstante().tie());
            UsineLib.findUsine().creeMesureEvolutionConstante();
            ((IEvolutionConstante)crb).constante(val);
        } else {
          crb= evolutions()[(int)val];
        }
        sos[i]= new PRSollicitation(type, crb, vnds);
        for (int j= 0; j < nbNds; j++) {
          int iNd;
          file.readFields();
          iNd= file.intField(0);
          vnds.add(nds[iNd]);
        }
      }
      sollicitations(sos);
      //------------------------------------------------------------------------
      // Normales
      //------------------------------------------------------------------------
      Vector vnormales= new Vector();
      // Commentaire
      file.readFields();
      for (int i= 0; i < ndsBds.length; i++) {
        for (int j= 0; j < ndsBds[i].length; j++) {
          int iNd;
          double val;
          file.readFields();
          iNd= file.intField(0);
          val= file.doubleField(1);
          vnormales.add(new PRNormale(nds[iNd], val));
        }
      }
      mdlPrp_.normales((PRNormale[])vnormales.toArray(new PRNormale[0]));
      //------------------------------------------------------------------------
      // Mod�le de calcul
      //------------------------------------------------------------------------
      PRGroupePT[] grpPTs;
      Object[] parCal;
      // Groupes de pas de temps
      // Commentaire
      file.readFields();
      // Nombre de groupes de pas de temps
      file.readFields();
      ival= file.intField(0);
      grpPTs= new PRGroupePT[ival];
      // Groupes de pas de temps
      for (int i= 0; i < grpPTs.length; i++) {
        double deb;
        double fin;
        int nbPas;
        int tpSch;
        double coefSch;
        int tpMth;
        double[] coefsMth;
        int freq;
        file.readFields();
        deb= file.doubleField(0);
        fin= file.doubleField(1);
        nbPas= file.intField(2);
        PRBoucleTemps tps= new PRBoucleTemps(deb, fin, nbPas);
        tpSch= file.intField(3);
        coefSch= file.doubleField(4);
        PRSchemaResolution sch= new PRSchemaResolution(tpSch, coefSch);
        tpMth= file.intField(5);
        PRMethodeResolution mth= new PRMethodeResolution(tpMth);
        coefsMth= mth.coefficients();
        for (int j= 0; j < coefsMth.length; j++)
          coefsMth[j]= file.doubleField(6 + j);
        freq= file.intField(6 + coefsMth.length);
        grpPTs[i]= new PRGroupePT(tps, sch, mth, freq);
      }
      mdlCal_.groupesPT(grpPTs);
      // Commentaire
      file.readFields();
      // Parametres du calcul
      parCal= mdlCal_.parametresCalcul();
      file.readFields();
      for (int i= 0; i < parCal.length; i++) {
        if (parCal[i] instanceof Double)
          parCal[i]= new Double(file.doubleField(i));
        else if (parCal[i] instanceof Integer)
          parCal[i]= new Integer(file.intField(i));
        else if (parCal[i] instanceof String)
          parCal[i]= new String(file.stringField(i));
        // Traitement sp�cial pour les File: On r�tablit le chemin absolu issu
        // du chemin du fichier de sauvegarde.
        else if (parCal[i] instanceof File) {
          File ftmp= new File(file.stringField(i));
          ftmp= CtuluLibFile.getAbsolutePathnameTo(new File(_racine), ftmp);
          parCal[i]= ftmp;
        }
      }
    } catch (NumberFormatException _exc) {
      _exc.printStackTrace();
      throw new IOException(
        "Erreur de lecture sur " + nmFcPre + " ligne " + file.getLineNumber());
    } catch (FileNotFoundException _exc) {
      throw new IOException("Erreur d'ouverture de " + nmFcPre);
    } catch (IOException _exc) {
      throw new IOException(
        "Erreur de lecture sur " + nmFcPre + " ligne " + file.getLineNumber());
    } catch (Exception _exc) {
      throw new IOException(_exc.getMessage());
    } finally {
      if (rf != null)
        rf.close();
    }
  }
  /**
   * Enregistrement du projet sur fichier .pre
   * @param _nomFichier Nom du fichier de sauvegarde.
   */
  public void enregistrer(String _nomFichier) throws IOException {
    class GroupePE {
      int type;
      int[] codes;
      double[] vals;
    }
    GrElement[] els= maillage().elements();
    GrNoeud[] nds= maillage().noeuds();
    IEvolution[] crbs= evolutions();
    // Correspondances
    // Table Noeud->Num�ro
    Hashtable hnd2Num= new Hashtable();
    for (int i= 0; i < nds.length; i++)
      hnd2Num.put(nds[i], new Integer(i));
    // Table El�ment->Num�ro
    Hashtable hel2Num= new Hashtable();
    for (int i= 0; i < els.length; i++)
      hel2Num.put(els[i], new Integer(i));
    // Table Courbe->Num�ro
    Hashtable hcrb2Num= new Hashtable();
    for (int i= 0; i < crbs.length; i++)
      hcrb2Num.put(crbs[i], new Integer(i));
    // Enregistrement du maillage s'il est nouveau.
    if (modMail)
      ecrireMaillage(_nomFichier.substring(0, _nomFichier.lastIndexOf(".pre")));
    modMail= false;
    //--------------------------------------------------------------------------
    //--- Formattage des donn�es  ----------------------------------------------
    //--------------------------------------------------------------------------
    // Cr�ation des groupes de propri�t�s par �l�ments de fond.
    Vector vgpes= new Vector();
    int[] igpes= new int[els.length];
    GroupePE[] grpPEs= new GroupePE[els.length];
    for (int i= 0; i < grpPEs.length; i++)
      grpPEs[i]= new GroupePE();
    // Initialisation du type des groupes
    {
      Vector vntsFds= mdlPrp_.naturesFonds();
      for (int i= 0; i < vntsFds.size(); i++) {
        PRNature nat= (PRNature)vntsFds.get(i);
        int tpNat= nat.type();
        int[] tpPrps= Definitions.getTypesProprietesElementaires(tpNat);
        Vector vels= nat.supports();
        for (int j= 0; j < vels.size(); j++) {
          int iEl= ((Integer)hel2Num.get(vels.get(j))).intValue();
          grpPEs[iEl].type= tpNat;
          grpPEs[iEl].codes= new int[tpPrps.length];
          grpPEs[iEl].vals= new double[tpPrps.length];
          for (int k= 0; k < tpPrps.length; k++)
            grpPEs[iEl].codes[k]= 0;
        }
      }
    }
    // Initialisation des propri�t�s des groupes.
    {
      PRPropriete[] prpsEls= mdlPrp_.proprietesElements();
      for (int i= 0; i < prpsEls.length; i++) {
        int tpPrp= prpsEls[i].type();
        Vector vels= prpsEls[i].supports();
        int code;
        double val;
        if (prpsEls[i].evolution() instanceof IEvolutionConstante) {
          code= 1;
          val= ((IEvolutionConstante)prpsEls[i].evolution()).constante();
        } else {
          code= 2;
          val= ((Integer)hcrb2Num.get(prpsEls[i].evolution())).intValue();
        }
        for (int j= 0; j < vels.size(); j++) {
          int iEl= ((Integer)hel2Num.get(vels.get(j))).intValue();
          int tpNat= grpPEs[iEl].type;
          int[] tpPrps= Definitions.getTypesProprietesElementaires(tpNat);
          int iPE;
          for (iPE= 0; iPE < tpPrps.length; iPE++)
            if (tpPrps[iPE] == tpPrp)
              break;
          grpPEs[iEl].codes[iPE]= code;
          grpPEs[iEl].vals[iPE]= val;
        }
      }
    }
    // Num�ro de groupe par �l�ment.
    NEXT_ELE : for (int i= 0; i < els.length; i++) {
      int iEl= ((Integer)hel2Num.get(els[i])).intValue();
      NEXT_GPE : for (int j= 0; j < vgpes.size(); j++) {
        GroupePE gp= (GroupePE)vgpes.get(j);
        if (gp.type != grpPEs[i].type)
          continue NEXT_GPE;
        for (int k= 0; k < gp.codes.length; k++)
          if (gp.codes[k] != grpPEs[i].codes[k])
            continue NEXT_GPE;
        for (int k= 0; k < gp.vals.length; k++)
          if (gp.vals[k] != grpPEs[i].vals[k])
            continue NEXT_GPE;
        igpes[iEl]= j;
        continue NEXT_ELE;
      }
      vgpes.add(grpPEs[i]);
      igpes[iEl]= vgpes.size() - 1;
    }
    // Cr�ation des groupes de propri�t�s par �l�ments de bord. Chaque bord est
    // rep�r� par le num�ro de son premier noeud.
    Vector vgpas= new Vector();
    int[] igpas= new int[nds.length];
    GroupePE[] grpPAs= new GroupePE[nds.length];
    // Initialisation du type des groupes pour chaque bord. Chaque nature ne
    // pointe que sur un bord.
    {
      Vector vntsBds= mdlPrp_.naturesBords();
      for (int i= 0; i < vntsBds.size(); i++) {
        PRNature nat= (PRNature)vntsBds.get(i);
        int tpNat= nat.type();
        int[] tpPrps= Definitions.getTypesProprietesElementaires(tpNat);
        Vector vels= nat.supports();
        for (int j= 0; j < vels.size(); j++) {
          int iNd=
            ((Integer)hnd2Num.get(((GrElement)vels.get(j)).noeuds_[0]))
              .intValue();
          grpPAs[iNd]= new GroupePE();
          grpPAs[iNd].type= tpNat;
          grpPAs[iNd].codes= new int[tpPrps.length];
          grpPAs[iNd].vals= new double[tpPrps.length];
          for (int k= 0; k < tpPrps.length; k++)
            grpPAs[iNd].codes[k]= 0;
        }
      }
    }
    // Initialisation des propri�t�s des groupes de bord. Chaque propri�t� peut
    // pointer sur plusieurs groupes.
    {
      PRPropriete[] prpsBds= mdlPrp_.proprietesAretes();
      for (int i= 0; i < prpsBds.length; i++) {
        int tpPrp= prpsBds[i].type();
        Vector vels= prpsBds[i].supports();
        int code;
        double val;
        if (prpsBds[i].evolution() instanceof IEvolutionConstante) {
          code= 1;
          val= ((IEvolutionConstante)prpsBds[i].evolution()).constante();
        } else {
          code= 2;
          val= ((Integer)hcrb2Num.get(prpsBds[i].evolution())).intValue();
        }
        for (int j= 0; j < vels.size(); j++) {
          int iNd=
            ((Integer)hnd2Num.get(((GrElement)vels.get(j)).noeuds_[0]))
              .intValue();
          int tpNat= grpPAs[iNd].type;
          int[] tpPrps= Definitions.getTypesProprietesElementaires(tpNat);
          int iPA;
          for (iPA= 0; iPA < tpPrps.length; iPA++)
            if (tpPrps[iPA] == tpPrp)
              break;
          grpPAs[iNd].codes[iPA]= code;
          grpPAs[iNd].vals[iPA]= val;
        }
      }
    }
    // Num�ro de groupe par bord.
    NEXT_ELE : for (int i= 0; i < nds.length; i++) {
      if (grpPAs[i] == null) {
        igpas[i]= -1;
        continue;
      }
      NEXT_GPE : for (int j= 0; j < vgpas.size(); j++) {
        GroupePE gp= (GroupePE)vgpas.get(j);
        if (gp.type != grpPAs[i].type)
          continue NEXT_GPE;
        for (int k= 0; k < gp.codes.length; k++)
          if (gp.codes[k] != grpPAs[i].codes[k])
            continue NEXT_GPE;
        for (int k= 0; k < gp.vals.length; k++)
          if (gp.vals[k] != grpPAs[i].vals[k])
            continue NEXT_GPE;
        igpas[i]= j;
        continue NEXT_ELE;
      }
      vgpas.add(grpPAs[i]);
      igpas[i]= vgpas.size() - 1;
    }
    //--------------------------------------------------------------------------
    //--- Transfert sur fichier  -----------------------------------------------
    //--------------------------------------------------------------------------
    PrintWriter file= null;
    try {
      // Ouverture du fichier
      file= new PrintWriter(new FileWriter(_nomFichier));
      // Num�ro de version courante
      String version= RefluxImplementation.informationsSoftware().version;
      file.println("*!$ Version " + version);
      // Type de probleme (4: transport total 2D, 5: suspension 2D)
      file.println("    " + (type() + 1));
      //------------------------------------------------------------------------
      // Solutions initiales
      //------------------------------------------------------------------------
      //      GrPoint[] ptsSI=modeleCalcul().pointsSI();
      PRSolutionsInitiales[] sis= modeleCalcul().solutionsInitiales();
      //      // Nombre de points de la surface
      //      file.println("* POINTS DE SOLUTIONS INITIALES");
      //
      //      // Points de d�finition de la surface.
      //      for (int i=0; i<ptsSI.length; i++) {
      //        file.println(ptsSI[i].x+" "+ptsSI[i].y+" "+ptsSI[i].z);
      //      }
      // Solutions en chaque noeud
      file.println("* SOL. INITIALES (C, ZF) SUR LES NOEUDS");
      for (int i= 0; i < sis.length; i++) {
        double[] vals= sis[i].valeurs();
        file.print(i);
        for (int j= 0; j < vals.length; j++)
          file.print(" " + vals[j]);
        file.println();
      }
      //------------------------------------------------------------------------
      // Mod�le de propri�t�s
      //------------------------------------------------------------------------
      // Groupes de propri�t�s d'ar�tes
      file.println("* GROUPES DE PROPRIETES D'ARETES");
      file.println(vgpas.size());
      // Groupes de propri�t�s d'ar�tes
      for (int i= 0; i < vgpas.size(); i++) {
        GroupePE gp= (GroupePE)vgpas.get(i);
        file.print(i + " ");
        file.print(gp.type + " ");
        for (int j= 0; j < gp.codes.length; j++) {
          file.print(gp.codes[j] + " " + gp.vals[j] + " ");
        }
        file.println();
      }
      // Num�ro de groupe de propri�t�s d'ar�tes par noeud de bord.
      file.println("* NUMEROS DE GROUPES DE PROPRIETES D'ARETES");
      for (int i= 0; i < igpas.length; i++)
        if (igpas[i] != -1)
          file.println(i + " " + igpas[i]);
      // Nombre de groupes de propri�t�s de fond
      file.println("* GROUPES DE PROPRIETES DE FOND");
      file.println(vgpes.size());
      // Groupes de propri�t�s de fond
      for (int i= 0; i < vgpes.size(); i++) {
        GroupePE gp= (GroupePE)vgpes.get(i);
        file.print(i + " ");
        file.print(gp.type + " ");
        for (int j= 0; j < gp.codes.length; j++) {
          file.print(gp.codes[j] + " " + gp.vals[j] + " ");
        }
        file.println();
      }
      // Num�ro de groupe de propri�t�s de fond par �l�ment
      file.println("* NUMEROS DE GROUPES DE PROPRIETES DE FOND");
      for (int i= 0; i < igpes.length; i++)
        file.println(i + " " + igpes[i]);
      // Propri�t�s globales
      file.println("* PROPRIETES GLOBALES");
      PRPropriete[] prpsGlobs= mdlPrp_.proprietesGlobales();
      for (int i= 0; i < prpsGlobs.length; i++) {
        file.print(
          ((IEvolutionConstante)prpsGlobs[i].evolution()).constante() + " ");
      }
      file.println();
      //------------------------------------------------------------------------
      // Conditions aux limites
      //------------------------------------------------------------------------
      PRConditionLimite[] cls= conditionsLimites();
      // Nombre de conditions aux limites
      file.println("* CONDITIONS AUX LIMITES");
      file.println(cls.length);
      // Conditions aux limites
      for (int i= 0; i < cls.length; i++) {
        int code;
        double val;
        Vector vnds;
        if (cls[i].evolution() instanceof IEvolutionConstante) {
          code= 1;
          val= ((IEvolutionConstante)cls[i].evolution()).constante();
        } else {
          code= 2;
          val= ((Integer)hcrb2Num.get(cls[i].evolution())).intValue();
        }
        vnds= cls[i].supports();
        file.println(
          cls[i].type() + " " + code + " " + val + " " + vnds.size());
        for (int j= 0; j < vnds.size(); j++)
          file.println(((Integer)hnd2Num.get(vnds.get(j))).intValue());
      }
      //------------------------------------------------------------------------
      // Sollicitations
      //------------------------------------------------------------------------
      PRSollicitation[] sos= sollicitations();
      // Nombre de sollicitations
      file.println("* SOLLICITATIONS");
      file.println(sos.length);
      // Sollicitations
      for (int i= 0; i < sos.length; i++) {
        int code;
        double val;
        Vector vnds;
        if (sos[i].evolution() instanceof IEvolutionConstante) {
          code= 1;
          val= ((IEvolutionConstante)sos[i].evolution()).constante();
        } else {
          code= 2;
          val= ((Integer)hcrb2Num.get(sos[i].evolution())).intValue();
        }
        vnds= sos[i].supports();
        file.println(
          sos[i].type() + " " + code + " " + val + " " + vnds.size());
        for (int j= 0; j < vnds.size(); j++)
          file.println(((Integer)hnd2Num.get(vnds.get(j))).intValue());
      }
      //------------------------------------------------------------------------
      // Normales
      //------------------------------------------------------------------------
      file.println("* NORMALES SUR LES NOEUDS");
      PRNormale[] normales= mdlPrp_.normales();
      for (int i= 0; i < normales.length; i++) {
        int iNd= ((Integer)hnd2Num.get(normales[i].support())).intValue();
        file.println(iNd + " " + normales[i].valeur());
      }
      //------------------------------------------------------------------------
      // Mod�le de calcul
      //------------------------------------------------------------------------
      PRGroupePT[] grpPTs= mdlCal_.groupesPT();
      Object[] parCal= mdlCal_.parametresCalcul();
      // Groupes de pas de temps
      // Nombre de groupes de pas de temps
      file.println("* GROUPES DE PAS DE TEMPS");
      file.println(grpPTs.length);
      // Groupes de pas de temps
      for (int i= 0; i < grpPTs.length; i++) {
        PRBoucleTemps tps= grpPTs[i].temps();
        PRSchemaResolution sch= grpPTs[i].schema();
        PRMethodeResolution mth= grpPTs[i].methode();
        double[] coefs= mth.coefficients();
        file.print(tps.debut() + " " + tps.fin() + " " + tps.nbPas() + " ");
        file.print(sch.type() + " " + sch.coefficient() + " ");
        file.print(mth.type() + " ");
        for (int j= 0; j < coefs.length; j++)
          file.print(coefs[j] + " ");
        file.println(grpPTs[i].frequenceStockage());
      }
      // Parametres du calcul
      file.println("* PARAMETRES DU CALCUL");
      for (int i= 0; i < parCal.length; i++) {
        // Traitement sp�cial pour les File: On enregistre un chemin relatif au
        // nom de fichier de sauvegarde.
        if (parCal[i] instanceof File) {
          File ftmp= (File)parCal[i];
          ftmp= CtuluLibFile.getRelativePathnameTo(new File(_nomFichier), ftmp);
          file.print(ftmp + " ");
        } else
          file.print(parCal[i] + " ");
      }
      file.println();
    } catch (IOException exc) {
      throw new IOException("Erreur d'�criture sur " + _nomFichier);
    } catch (NumberFormatException exc) {
      throw new IOException("Erreur d'�criture sur " + _nomFichier);
    } finally {
      // Fermeture du fichier
      file.close();
    }
  }
  /**
   * Retourne la classe interface de calcul associ� au projet.
   *
   * @return L'interface de calcul.
   */
  public PRInterfaceCalcul getInterfaceCalcul() {
    if (interface_ == null) {
      interface_= new TT2dInterfaceCalcul();
    }
    return interface_;
  }
  /**
   * Cr�e les solutions initiales aux noeuds � partir des tableaux pass�s.
   * @param _vx La vitesse x en chaque noeud dans l'ordre des noeuds.
   * @param _vy La vitesse y en chaque noeud dans l'ordre des noeuds.
   * @param _ne Le niveau d'eau en chaque noeud dans l'ordre des noeuds.
   */
  public void creeSolutionsInitiales(
    double[] _vx,
    double[] _vy,
    double[] _ne) {
    //    PRSolutionsInitiales[] sis;
    //
    //    GrNoeud[] nds=maillage().noeuds();
    //    sis=new PRSolutionsInitiales[nds.length];
    //
    //    for (int i=0; i<nds.length; i++) {
    //      sis[i]=new PRSolutionsInitiales(nds[i],_vx[i],_vy[i],_ne[i]);
    //    }
    //
    //    mdlCal_.solutionsInitiales(sis);
  }
  /**
   * Controle et mise � jour des solutions initiales si le nombre de termes
   * u,v,w a chang�. Pas d'action.
   */
  public void controleNbTermesNoeuds() {}
  /**
   * Initialise les solutions initiales en chaque noeud avec des valeurs
   * globales.
   *
   * @param _valGlobs Valeurs globales.
   *        _valGlobs[0] : Concentration globale.
   */
  public void initialiserSolutionsInitiales(double[] _valGlobs) {
    // Ne fonctionne que pour la suspension (initialisation de la concentration)
    if (type() != PRProjet.TRANS_SUS_2D)
      return;
    PRSolutionsInitiales[] sis;
    GrNoeud[] nds= maillage().noeuds();
    sis= new PRSolutionsInitiales[nds.length];
    // Solutions initiales : Concentration, cote_z
    for (int i= 0; i < nds.length; i++) {
      sis[i]=
        new PRSolutionsInitiales(
          nds[i],
          new double[] { _valGlobs[0], nds[i].point_.z_ });
    }
    mdlCal_.solutionsInitiales(sis);
  }
  /**
   * Initialise les solutions initiales du projet.
   */
  public void initialiserSolutionsInitiales() {
    PRSolutionsInitiales[] sis;
    GrNoeud[] nds= maillage().noeuds();
    sis= new PRSolutionsInitiales[nds.length];
    // Solutions initiales : Concentration, cote_z
    for (int i= 0; i < nds.length; i++) {
      sis[i]=
        new PRSolutionsInitiales(nds[i], new double[] { 0, nds[i].point_.z_ });
    }
    mdlCal_.solutionsInitiales(sis);
  }
}
