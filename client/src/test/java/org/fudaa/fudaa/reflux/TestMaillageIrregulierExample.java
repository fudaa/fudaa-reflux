/*
 * @file         TestMaillageIrregulier.java
 * @creation
 * @modification $Date: 2006-09-29 09:10:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.SpotLight;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.WindowConstants;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.image.TextureLoader;

import org.fudaa.dodico.corba.ef.IElement;
import org.fudaa.dodico.corba.ef.IElementHelper;
import org.fudaa.dodico.corba.ef.IMaillage;
import org.fudaa.dodico.corba.ef.INoeud;
import org.fudaa.dodico.corba.geometrie.LTypeElement;
import org.fudaa.dodico.corba.reflux.SResultatsReflux;

import org.fudaa.dodico.ef.DElement;
import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.reflux.DResultatsReflux;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.volume.BGrilleIrreguliere;
import org.fudaa.ebli.volume.BUnivers;
import org.fudaa.ebli.volume.controles.BControleVolume;
import org.fudaa.ebli.volume.controles.BUniversInteraction;

/**
 * @version $Revision: 1.8 $ $Date: 2006-09-29 09:10:26 $ by $Author: deniger $
 * @author
 */
public final class TestMaillageIrregulierExample {
  
  private TestMaillageIrregulierExample() {}
  public static void main(final String[] _args) {
    new GrilleIrreguliere();
  }
}

class GrilleIrreguliere {
  public BGrilleIrreguliere irr, sol_irr;
  public JCheckBox irr_visible, sol_visible;
  public boolean[] calcule = new boolean[] { false, false };
  public BUnivers u;
  IMaillage maille;
  int nbnoeuds;
  int nbtriangles;
  Point3d[] noeuds, noeuds_sol;
  int[] triangles;
  IElement[] ielements;
  INoeud[] inoeuds;

  public GrilleIrreguliere() {
    try {
      maille = MaillageCORELEBTH.lire(new File("maillage/compiegne/compiegne"));
    } catch (final IOException e) {
      System.err.println("Fichiers compiegne.* introuvables");
      System.exit(-1);
    }
    inoeuds = maille.noeuds();
    ielements = maille.elements();
    if (ielements[0].type() == LTypeElement.T6) {
      createT6();
    }
    nbnoeuds = inoeuds.length;
    nbtriangles = ielements.length;
    System.out.println("nombre de triangles : " + nbtriangles);
    // fabrication des noeuds format Java3D
    noeuds = new Point3d[nbnoeuds];
    for (int i = 0; i < nbnoeuds; i++) {
      noeuds[inoeuds[i].numero()] = new Point3d(inoeuds[i].point().coordonnees());
    }
    // fabrication des elements (triangles) format Java3D
    triangles = new int[nbtriangles * 3];
    for (int i = 0; i < nbtriangles; i++) {
      for (int j = 0; j < 3; j++) {
        triangles[3 * i + j] = ielements[i].noeuds()[j].numero();
      }
    }
    u = new BUnivers();
    final DirectionalLight l = new DirectionalLight(new Color3f(1f, 1f, 1f), new Vector3f(1, 1, 0));
    l.setInfluencingBounds(new BoundingSphere(new Point3d(), 100000));
    final AmbientLight l2 = new AmbientLight(new Color3f(1f, 1f, 1f));
    l2.setInfluencingBounds(new BoundingSphere(new Point3d(), 100000));
    final DirectionalLight l3 = new DirectionalLight(new Color3f(1f, 1f, 1f), new Vector3f(-1, 1, 0));
    l3.setInfluencingBounds(new BoundingSphere(new Point3d(), 100000));
    final SpotLight l4 = new SpotLight();
    l4.setInfluencingBounds(new BoundingSphere(new Point3d(), 100000));
    l4.setPosition(0, 100000, 0);
    l4.setDirection(0, -1, 0);
    final BranchGroup lightbg = new BranchGroup();
    lightbg.addChild(l);
    // lightbg.addChild(l2);
    lightbg.addChild(l3);
    // lightbg.addChild(l4);
    u.addBranchGraph(lightbg);
    irr = new BGrilleIrreguliere();
    double minx = noeuds[0].x, miny = noeuds[0].y, minz = noeuds[0].z, maxx = noeuds[0].x, maxy = noeuds[0].y, maxz = noeuds[0].z;
    for (int i = 1; i < nbnoeuds; i++) {
      if (minx > noeuds[i].x) {
        minx = noeuds[i].x;
      }
      if (miny > noeuds[i].y) {
        miny = noeuds[i].y;
      }
      if (minz > noeuds[i].z) {
        minz = noeuds[i].z;
      }
      if (maxx < noeuds[i].x) {
        maxx = noeuds[i].x;
      }
      if (maxy < noeuds[i].y) {
        maxy = noeuds[i].y;
      }
      if (maxz < noeuds[i].z) {
        maxz = noeuds[i].z;
      }
    }
    final JFrame frame = new JFrame();
    irr.setBoite(new GrPoint(minx, miny, minz), new GrPoint(maxx, maxy, maxz));
    irr.setGeometrie(nbnoeuds, noeuds, nbtriangles * 3, triangles);
    irr.setCouleur(Color.red);
    irr.setTransparence(0f);
    irr.setTexture(new TextureLoader("map5.gif", frame).getTexture());
    u.addObjet(irr);
    irr.setFilaire(true);
    irr.setVisible(true);
    calcule[0] = true;
    // SResultatsReflux sol=CResultatsReflux.litResultatsReflux("compiegne",nbnoeuds);
    /*
     * double[] z=new double[nbnoeuds]; for (int i=0;i<nbnoeuds;i++) z[i]=sol.etapes[0].lignes[i].valeurs[7];
     * noeuds_sol=new Point3d[nbnoeuds]; for (int i=0;i<nbnoeuds;i++) noeuds_sol[inoeuds[i].numero()]=new
     * Point3d(inoeuds[i].point().coordonnees()[0],inoeuds[i].point().coordonnees()[1],z[i]); sol_irr=new
     * BGrilleIrreguliere(); minz=z[0]; maxz=z[0]; for (int i=1;i<nbnoeuds;i++) { if (minz>z[i]) minz=z[i]; if (maxz<z[i])
     * maxz=z[i]; } sol_irr.setBoite(new GrPoint(minx,miny,minz),new GrPoint(maxx,maxy,maxz));
     * sol_irr.setGeometrie(nbnoeuds,noeuds_sol,nbtriangles*3,triangles); sol_irr.setCouleur(Color.blue);
     * sol_irr.setTransparence(.5f); u.addObjet(sol_irr); sol_irr.setVisible(false);
     */
    final JPanel jp = new JPanel();
    jp.setLayout(new FlowLayout());
    final JButton button = new JButton("Init");
    final BControleVolume cv = new BControleVolume(u);
    cv.addRepereEventListener(new BUniversInteraction(u));
    jp.add(cv);
    frame.addWindowListener(new WindowAdapter() {
      public void windowClosing(final WindowEvent _evt) {
        System.exit(0);
      }
    });
    frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    frame.setSize(new Dimension(400, 400));
    frame.getContentPane().setLayout(new BorderLayout());
    final JPanel panelEst = new JPanel();
    panelEst.setLayout(new FlowLayout());
    panelEst.add(button);
    irr_visible = new JCheckBox("Bat");
    irr_visible.setSelected(true);
    irr_visible.addItemListener(new CBListener());
    panelEst.add(irr_visible);
    sol_visible = new JCheckBox("Solution");
    sol_visible.setSelected(false);
    sol_visible.addItemListener(new CBListener());
    panelEst.add(sol_visible);
    final ButtonGroup bg = new ButtonGroup();
    final JRadioButton point = new JRadioButton("Point");
    point.setActionCommand("Point");
    final JRadioButton filaire = new JRadioButton("Filaire");
    filaire.setActionCommand("Filaire");
    filaire.setSelected(true);
    final JRadioButton polygone = new JRadioButton("Polygone");
    polygone.setActionCommand("Polygone");
    // polygone.setSelected(true);
    bg.add(point);
    bg.add(filaire);
    bg.add(polygone);
    final RadioListener radio = new RadioListener();
    point.addActionListener(radio);
    filaire.addActionListener(radio);
    polygone.addActionListener(radio);
    final JPanel jp2 = new JPanel();
    jp2.setLayout(new FlowLayout());
    jp2.add(point);
    jp2.add(filaire);
    jp2.add(polygone);
    frame.getContentPane().add("Center", u.getCanvas3D());
    frame.getContentPane().add("North", jp2);
    frame.getContentPane().add("South", jp);
    frame.getContentPane().add("East", panelEst);
    frame.setVisible(true);
    u.getCanvas3D().freeze(false);
    button.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent _e) {
        u.init();

      }

    });
    // u.init();
    // irr.setPoint();
    cv.setCoef(15f);
  }
  private void createT6() {
    final IElement[] eT3 = new IElement[ielements.length * 4];
    for (int i = 0; i < ielements.length * 4; i += 4) {
      eT3[i] = UsineLib.findUsine().creeEfElement();
      eT3[i].noeuds(new INoeud[] { ielements[i / 4].noeuds()[0], ielements[i / 4].noeuds()[1],
          ielements[i / 4].noeuds()[5] });
      eT3[i].type(LTypeElement.T3);
      eT3[i].numero(3);
      eT3[i + 1] = IElementHelper.narrow(new DElement().tie());
      eT3[i + 1].noeuds(new INoeud[] { ielements[i / 4].noeuds()[1], ielements[i / 4].noeuds()[5],
          ielements[i / 4].noeuds()[3] });
      eT3[i + 1].type(LTypeElement.T3);
      eT3[i + 1].numero(3);
      eT3[i + 2] = UsineLib.findUsine().creeEfElement();
      eT3[i + 2].noeuds(new INoeud[] { ielements[i / 4].noeuds()[5], ielements[i / 4].noeuds()[3],
          ielements[i / 4].noeuds()[4] });
      eT3[i + 2].type(LTypeElement.T3);
      eT3[i + 2].numero(3);
      eT3[i + 3] = UsineLib.findUsine().creeEfElement();
      eT3[i + 3].noeuds(new INoeud[] { ielements[i / 4].noeuds()[3], ielements[i / 4].noeuds()[1],
          ielements[i / 4].noeuds()[2] });
      eT3[i + 3].type(LTypeElement.T3);
      eT3[i + 3].numero(3);
    }
    ielements = eT3;
  }
  class RadioListener implements ActionListener {
    public void actionPerformed(final ActionEvent _evt) {
      try {
        final Method m = irr.getClass().getMethod("set" + _evt.getActionCommand(), new Class[0]);
        m.invoke(irr, new Object[0]);
        m.invoke(sol_irr, new Object[0]);
      } catch (final Exception ex) {
        System.out.println(ex);
      }
    }
  }
  class CBListener implements ItemListener {
    public void itemStateChanged(final ItemEvent _evt) {
      final Object source = _evt.getItemSelectable();
      boolean valeur;
      if (_evt.getStateChange() == ItemEvent.DESELECTED) {
        valeur = false;
        if (source == irr_visible) {
          irr.setVisible(valeur);
        } else {
          sol_irr.setVisible(valeur);
        }
      } else {
        valeur = true;
        if (source == irr_visible) {
          if (!calcule[0]) {
            if (ielements[0].type() == LTypeElement.T6) {
              final IElement[] eT3 = new IElement[ielements.length * 4];
              for (int i = 0; i < ielements.length * 4; i += 4) {
                eT3[i] = UsineLib.findUsine().creeEfElement();
                eT3[i].noeuds(new INoeud[] { ielements[i / 4].noeuds()[0], ielements[i / 4].noeuds()[1],
                    ielements[i / 4].noeuds()[5] });
                eT3[i].type(LTypeElement.T3);
                eT3[i].numero(3);
                eT3[i + 1] = UsineLib.findUsine().creeEfElement();
                eT3[i + 1].noeuds(new INoeud[] { ielements[i / 4].noeuds()[1], ielements[i / 4].noeuds()[5],
                    ielements[i / 4].noeuds()[3] });
                eT3[i + 1].type(LTypeElement.T3);
                eT3[i + 1].numero(3);
                eT3[i + 2] = UsineLib.findUsine().creeEfElement();
                eT3[i + 2].noeuds(new INoeud[] { ielements[i / 4].noeuds()[5], ielements[i / 4].noeuds()[3],
                    ielements[i / 4].noeuds()[4] });
                eT3[i + 2].type(LTypeElement.T3);
                eT3[i + 2].numero(3);
                eT3[i + 3] = UsineLib.findUsine().creeEfElement();
                eT3[i + 3].noeuds(new INoeud[] { ielements[i / 4].noeuds()[3], ielements[i / 4].noeuds()[1],
                    ielements[i / 4].noeuds()[2] });
                eT3[i + 3].type(LTypeElement.T3);
                eT3[i + 3].numero(3);
              }
              ielements = eT3;
            }
            // fabrication des noeuds format Java3D
            noeuds = new Point3d[nbnoeuds];
            for (int i = 0; i < nbnoeuds; i++) {
              noeuds[inoeuds[i].numero()] = new Point3d(inoeuds[i].point().coordonnees());
            }
            double minx = noeuds[0].x, miny = noeuds[0].y, minz = noeuds[0].z, maxx = noeuds[0].x, maxy = noeuds[0].y, maxz = noeuds[0].z;
            for (int i = 1; i < nbnoeuds; i++) {
              if (minx > noeuds[i].x) {
                minx = noeuds[i].x;
              }
              if (miny > noeuds[i].y) {
                miny = noeuds[i].y;
              }
              if (minz > noeuds[i].z) {
                minz = noeuds[i].z;
              }
              if (maxx < noeuds[i].x) {
                maxx = noeuds[i].x;
              }
              if (maxy < noeuds[i].y) {
                maxy = noeuds[i].y;
              }
              if (maxz < noeuds[i].z) {
                maxz = noeuds[i].z;
              }
            }
            // fabrication des elements (triangles) format Java3D
            triangles = new int[nbtriangles * 3];
            for (int i = 0; i < nbtriangles; i++) {
              for (int j = 0; j < 3; j++) {
                triangles[3 * i + j] = ielements[i].noeuds()[j].numero();
              }
            }
            calcule[0] = true;
            irr = new BGrilleIrreguliere();
            irr.setBoite(new GrPoint(minx, miny, minz), new GrPoint(maxx, maxy, maxz));
            irr.setGeometrie(nbnoeuds, noeuds, nbtriangles * 3, triangles);
            irr.setCouleur(Color.red);
            irr.setTransparence(0f);
            // irr.setTexture(new TextureLoader("map5.gif",frame).getTexture());
            u.addObjet(irr);
            irr.setVisible(false);
          }
          irr.setVisible(valeur);
        } else {
          if (!calcule[1]) {
            SResultatsReflux sol = null;
            try {
              sol = DResultatsReflux.litResultatsReflux(new File("maillage/calais/calais45.sol"), nbnoeuds);
            } catch (final IOException _exc) {
              _exc.printStackTrace();
            }
            final double[] z = new double[nbnoeuds];
            for (int i = 0; i < nbnoeuds; i++) {
              z[i] = sol.etapes[0].lignes[i].valeurs[7];
            }
            double minx = noeuds[0].x, miny = noeuds[0].y, minz = z[0], maxx = noeuds[0].x, maxy = noeuds[0].y, maxz = z[0];
            for (int i = 1; i < nbnoeuds; i++) {
              if (minx > noeuds[i].x) {
                minx = noeuds[i].x;
              }
              if (miny > noeuds[i].y) {
                miny = noeuds[i].y;
              }
              if (minz > z[i]) {
                minz = z[i];
              }
              if (maxx < noeuds[i].x) {
                maxx = noeuds[i].x;
              }
              if (maxy < noeuds[i].y) {
                maxy = noeuds[i].y;
              }
              if (maxz < z[i]) {
                maxz = z[i];
              }
            }
            noeuds_sol = new Point3d[nbnoeuds];
            for (int i = 0; i < nbnoeuds; i++) {
              noeuds_sol[inoeuds[i].numero()] = new Point3d(inoeuds[i].point().coordonnees()[0], inoeuds[i].point()
                  .coordonnees()[1], z[i]);
            }
            sol_irr = new BGrilleIrreguliere();
            sol_irr.setBoite(new GrPoint(minx, miny, minz), new GrPoint(maxx, maxy, maxz));
            sol_irr.setGeometrie(nbnoeuds, noeuds_sol, nbtriangles * 3, triangles);
            sol_irr.setCouleur(Color.blue);
            sol_irr.setTransparence(.5f);
            u.addObjet(sol_irr);
            sol_irr.setFilaire(true);
            calcule[1] = true;
          }
          sol_irr.setVisible(valeur);
        }
        /*
         * ((BGrilleIrreguliere)source).setVisible(false); else ((BGrilleIrreguliere)source).setVisible(true);
         */
      }
    }
  }
}
