/*
 * @file         TestIrregulier.java
 * @creation
 * @modification $Date: 2006-09-22 15:47:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.reflux;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

import javax.media.j3d.Texture;
import javax.vecmath.Point3d;

import com.sun.j3d.utils.image.TextureLoader;

import org.fudaa.dodico.corba.ef.IElement;
import org.fudaa.dodico.corba.ef.IElementHelper;
import org.fudaa.dodico.corba.ef.IMaillage;
import org.fudaa.dodico.corba.ef.INoeud;
import org.fudaa.dodico.corba.geometrie.LTypeElement;
import org.fudaa.dodico.corba.reflux.SResultatsReflux;

import org.fudaa.dodico.ef.DElement;
import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.reflux.DResultatsReflux;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.volume.BGrilleIrreguliere;
import org.fudaa.ebli.volume.FrameVolumeExample;

/**
 * @version $Revision: 1.5 $ $Date: 2006-09-22 15:47:11 $ by $Author: deniger $
 * @author
 */
public final class TestIrregulierExample {
  private TestIrregulierExample() {}

  public static void main(final String[] _args) {
    BGrilleIrreguliere irr, sol_irr;
    IMaillage maille = null;
    int nbnoeuds;
    int nbtriangles;
    Point3d[] noeuds, noeudsSol;
    int[] triangles;
    IElement[] ielements;
    INoeud[] inoeuds;
    try {
      maille = MaillageCORELEBTH.lire(new File("maillage/compiegne/compiegne"));
    } catch (final IOException e) {
      System.err.println("Fichiers compiegne.* introuvables");
      System.exit(-1);
    }
    inoeuds = maille.noeuds();
    ielements = maille.elements();
    if (ielements[0].type() == LTypeElement.T6) {
      final IElement[] e_t3 = new IElement[ielements.length * 4];
      for (int i = 0; i < ielements.length * 4; i += 4) {
        e_t3[i] = UsineLib.findUsine().creeEfElement()/* IElementHelper.narrow(new DElement().tie()) */;
        e_t3[i].noeuds(new INoeud[] { ielements[i / 4].noeuds()[0], ielements[i / 4].noeuds()[1],
            ielements[i / 4].noeuds()[5] });
        e_t3[i].type(LTypeElement.T3);
        e_t3[i].numero(3);
        e_t3[i + 1] = UsineLib.findUsine().creeEfElement();
        e_t3[i + 1].noeuds(new INoeud[] { ielements[i / 4].noeuds()[1], ielements[i / 4].noeuds()[5],
            ielements[i / 4].noeuds()[3] });
        e_t3[i + 1].type(LTypeElement.T3);
        e_t3[i + 1].numero(3);
        e_t3[i + 2] = IElementHelper.narrow(new DElement().tie());
        e_t3[i + 2].noeuds(new INoeud[] { ielements[i / 4].noeuds()[5], ielements[i / 4].noeuds()[3],
            ielements[i / 4].noeuds()[4] });
        e_t3[i + 2].type(LTypeElement.T3);
        e_t3[i + 2].numero(3);
        e_t3[i + 3] = UsineLib.findUsine().creeEfElement();
        e_t3[i + 3].noeuds(new INoeud[] { ielements[i / 4].noeuds()[1], ielements[i / 4].noeuds()[2],
            ielements[i / 4].noeuds()[3] });
        e_t3[i + 3].type(LTypeElement.T3);
        e_t3[i + 3].numero(3);
      }
      ielements = e_t3;
    }
    nbnoeuds = inoeuds.length;
    nbtriangles = ielements.length;
    System.out.println("nombre de triangles : " + nbtriangles);
    // fabrication des noeuds format Java3D
    noeuds = new Point3d[nbnoeuds];
    for (int i = 0; i < nbnoeuds; i++) {
      noeuds[inoeuds[i].numero()] = new Point3d(inoeuds[i].point().coordonnees());
    }
    // fabrication des elements (triangles) format Java3D
    triangles = new int[nbtriangles * 3];
    for (int i = 0; i < nbtriangles; i++) {
      for (int j = 0; j < 3; j++) {
        triangles[3 * i + j] = ielements[i].noeuds()[j].numero();
      }
    }
    final FrameVolumeExample frame = new FrameVolumeExample();
    irr = new BGrilleIrreguliere("Sol");
    double minx = noeuds[0].x, miny = noeuds[0].y, minz = noeuds[0].z, maxx = noeuds[0].x, maxy = noeuds[0].y, maxz = noeuds[0].z;
    for (int i = 1; i < nbnoeuds; i++) {
      if (minx > noeuds[i].x) {
        minx = noeuds[i].x;
      }
      if (miny > noeuds[i].y) {
        miny = noeuds[i].y;
      }
      if (minz > noeuds[i].z) {
        minz = noeuds[i].z;
      }
      if (maxx < noeuds[i].x) {
        maxx = noeuds[i].x;
      }
      if (maxy < noeuds[i].y) {
        maxy = noeuds[i].y;
      }
      if (maxz < noeuds[i].z) {
        maxz = noeuds[i].z;
      }
    }
    frame.addEchelleListener(irr);
    double[] z = new double[nbnoeuds];
    for (int i = 0; i < nbnoeuds; i++) {
      z[i] = (noeuds[i].z - minz) / (maxz - minz);
    }
    frame.addPaletteListener(irr, z);
    irr.setBoite(new GrPoint(minx, miny, minz), new GrPoint(maxx, maxy, maxz));
    irr.setGeometrie(nbnoeuds, noeuds, nbtriangles * 3, triangles);
    irr.setCouleur(Color.red);
    irr.setTransparence(0f);
    final Texture t = new TextureLoader("map5.gif", frame).getTexture();
    t.setCapability(Texture.ALLOW_ENABLE_WRITE);
    t.setBoundaryColor(0f, .9f, 0f, 0f);
    t.setEnable(true);
    // irr.setTexture(t);
    SResultatsReflux sol = null;
    try {
      sol = DResultatsReflux.litResultatsReflux(new File("maillage/compiegne/compiegne.sol"), nbnoeuds);
    } catch (final IOException _exc) {
      _exc.printStackTrace();
    }
    z = new double[nbnoeuds];
    for (int i = 0; i < nbnoeuds; i++) {
      z[i] = sol.etapes[0].lignes[i].valeurs[7];
    }
    minx = noeuds[0].x;
    miny = noeuds[0].y;
    minz = z[0];
    maxx = noeuds[0].x;
    maxy = noeuds[0].y;
    maxz = z[0];
    for (int i = 1; i < nbnoeuds; i++) {
      if (minx > noeuds[i].x) {
        minx = noeuds[i].x;
      }
      if (miny > noeuds[i].y) {
        miny = noeuds[i].y;
      }
      if (minz > z[i]) {
        minz = z[i];
      }
      if (maxx < noeuds[i].x) {
        maxx = noeuds[i].x;
      }
      if (maxy < noeuds[i].y) {
        maxy = noeuds[i].y;
      }
      if (maxz < z[i]) {
        maxz = z[i];
      }
    }
    noeudsSol = new Point3d[nbnoeuds];
    for (int i = 0; i < nbnoeuds; i++) {
      noeudsSol[inoeuds[i].numero()] = new Point3d(inoeuds[i].point().coordonnees()[0], inoeuds[i].point()
          .coordonnees()[1], z[i]);
    }
    sol_irr = new BGrilleIrreguliere();
    sol_irr.setBoite(new GrPoint(minx, miny, minz), new GrPoint(maxx, maxy, maxz));
    sol_irr.setGeometrie(nbnoeuds, noeudsSol, nbtriangles * 3, triangles);
    sol_irr.setCouleur(Color.blue);
    sol_irr.setTransparence(.5f);
    frame.getUnivers().addBranchGraph(irr);
    frame.getUnivers().addBranchGraph(sol_irr);
    irr.setVisible(false);
    sol_irr.setVisible(true);
    irr.setRapide(false);
    sol_irr.setRapide(false);
    frame.getUnivers().init();
    frame.setVisible(true);
  }
}
